<?php 
/**
* @Name: lang about
* @version : 1.0
* @date upgrade : 29/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	about	 => 	'Giới thiệu' ,
	advertise	 => 	'Quảng cáo' ,
	bntReset	 => 	'Kết quả' ,
	bntVote	 => 	'Bình chọn' ,
	cat_about	 => 	'Giới thiệu' ,
	document	 => 	'Tài liệu' ,
	f_navation	 => 	'Giới thiệu' ,
	mess_no_video	 => 	'Chưa có video nào' ,
	no_have_about	 => 	'Chưa có nội dung' ,
	other_video	 => 	'Các video khác' ,
	poll	 => 	'Thăm dò ý kiến' ,
	sub_about	 => 	'Nội dung khác' ,
	view_detail	 => 	'Xem chi tiết' ,
); 
?>
