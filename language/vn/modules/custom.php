<?php 
/**
* @Name: lang product vn
* @version : 1.0
* @date upgrade : 29/12/2007 by Thai son
**/
if ( !defined('IN_vnT') )	{ die('Access denied');	} 
$lang = array ( 
	accurate	 => 	'Tìm chính xác cụm từ' ,
	add_custom	 => 	'Thêm ý tưởng thiết kế mới' ,
	add_news	 => 	'Đăng tin tức' ,
	add_product	 => 	'Đăng tin bất động sản' ,
	add_success	 => 	'Đăng ý tưởng thiết kế thành công ! Vui lòng đợi xét duyệt nội dung' ,
	address	 => 	'Địa chỉ' ,
	advanced_search	 => 	'Tìm kiếm nâng cao' ,
	all_custom	 => 	'Tất cả ý tưởng thiết kế' ,
	all_project	 => 	'CÁC CÔNG TRÌNH ĐÃ THIẾT KẾ' ,
	are_you_sure_cancel	 => 	'Bạn có chắc không ?' ,
	are_you_sure_del	 => 	'Bạn chắc muốn Xóa không ?' ,
	area	 => 	'Diện tích' ,
	btnSend	 => 	'Gửi' ,
	btn_close	 => 	'Đóng' ,
	btn_print	 => 	'In trang này' ,
	btn_reset	 => 	'Hủy bỏ' ,
	btn_search	 => 	'Tìm' ,
	btn_send	 => 	'Gửi' ,
	btn_send_comment	 => 	'Gửi' ,
	btn_update	 => 	'Cập nhật' ,
	cat_filter	 => 	'Theo danh mục' ,
	category	 => 	'Danh mục' ,
	color	 => 	'Màu sắc:' ,
	comment_default	 => 	'Nhập ý kiến hoặc câu hỏi của bạn tại đây' ,
	compose_mail	 => 	'Soạn tin nhắn' ,
	consultant	 => 	'Liên hệ tư vấn' ,
	custom	 => 	'Ý tưởng thiết kế' ,
	custom_info	 => 	'Thông tin' ,
	custom_information	 => 	'THÔNG TIN khách hàng' ,
	custom_title	 => 	'Tên Customization' ,
	date_asc	 => 	'Ngày tăng dần' ,
	date_desc	 => 	'Ngày giảm dần' ,
	date_post	 => 	'Ngày đăng' ,
	del_custom	 => 	'Xóa Customization' ,
	del_failt	 => 	'Xóa thất bại' ,
	del_pic_failt	 => 	'Xóa hình thất bại' ,
	del_pic_success	 => 	'Xóa hình thành công' ,
	del_success	 => 	'Xóa Customization thành công' ,
	description	 => 	'Mô tả Customization' ,
	display_filter	 => 	'Hiển thị' ,
	edit_custom	 => 	'Cập nhật Customization' ,
	edit_picture	 => 	'Cập nhật hình ảnh' ,
	edit_success	 => 	'Cập nhật thành công ! Vui lòng đợi xét duyệt lại nội dung' ,
	email	 => 	'Email' ,
	email_receiver	 => 	'Email người nhận' ,
	email_sender	 => 	'Email người gửi' ,
	enter_code	 => 	'Nhập mã bảo vệ' ,
	enter_info_comment	 => 	'Nhập thông tin của bạn' ,
	err_area_empty	 => 	'Vui lòng nhập diện tích' ,
	err_color_empty	 => 	'Vui lòng nhập màu sắc' ,
	err_conntent_maxchar	 => 	'Nội dung đánh giá không vượt quá 200 ký tự, cám ơn!' ,
	err_conntent_minchar	 => 	'Nội dung đánh giá phải có ít nhất là 10 ký tự!' ,
	err_email_empty	 => 	'Vui lòng nhập email' ,
	err_email_invalid	 => 	'Email không đúng định dạng' ,
	err_empty_categoty	 => 	'Vui lòng chọn danh mục' ,
	err_empty_short	 => 	'Vui lòng nhập mô tả ngắn' ,
	err_empty_title	 => 	'Vui lòng nhập tên Customization' ,
	err_material_empty	 => 	'Vui lòng chọn chất liệu' ,
	err_max_pic_upload	 => 	'Bạn chỉ có thể upload 1 lần tối đa 5 hình .' ,
	err_min_pic_upload	 => 	'Không thể xóa hết .' ,
	err_name_empty	 => 	'Vui lòng nhập tên' ,
	err_phone_empty	 => 	'Vui lòng nhập số điện thoại' ,
	err_phone_invalid	 => 	'Số điện thoại không hợp lệ' ,
	err_product_empty	 => 	'Vui lòng chọn loại sản phẩm' ,
	err_project_empty	 => 	'Vui lòng chọn loại công trình' ,
	err_security_code_empty	 => 	'Vui lòng nhập mã bảo vệ' ,
	err_style_empty	 => 	'Vui lòng chọn phong cách' ,
	err_title_empty	 => 	'Vui lòng nhập tiêu đề' ,
	error_address_empty	 => 	'Vui lòng nhập địa chỉ' ,
	error_comment_failt	 => 	'Viết nhận xét thất bại . Thử lại lần nữa' ,
	error_email_invalid	 => 	'Email không hợp lệ' ,
	error_empty_c_name	 => 	'Vui lòng nhập tên người nhận' ,
	error_empty_email_receiver	 => 	'Vui lòng nhập email người nhận' ,
	error_empty_email_sender	 => 	'Vui lòng nhập email người gửi' ,
	error_empty_fullname	 => 	'Vui lòng nhập tên người đặt' ,
	error_empty_message	 => 	'Vui lòng nhập lời nhắn' ,
	error_empty_name_receiver	 => 	'Vui lòng nhập tên người nhận' ,
	error_empty_sender	 => 	'Vui lòng nhập tên người gửi' ,
	error_only_member	 => 	'Chỉ dành cho thành viên' ,
	error_ordet_id_empty	 => 	'Vui lòng nhập mã hóa đơn cần tìm' ,
	error_phone_empty	 => 	'Vui lòng nhập điện thoại' ,
	error_security_code	 => 	'Mã bảo vệ không chính xác' ,
	estore	 => 	'Công ty địa ốc' ,
	estore_name	 => 	'Cty địa ốc' ,
	f_booking	 => 	'đăng ký nhận báo giá & E-catalogue' ,
	f_booking_success	 => 	'Đăng ký thành công' ,
	f_comment	 => 	'Ý kiến của bạn' ,
	f_intro_to_friend	 => 	'GIỚI THIỆU Customization CHO BẠN BÈ' ,
	f_navation	 => 	'Customization' ,
	f_search	 => 	'Kết quả tìm kiếm' ,
	fax	 => 	'Fax' ,
	forgot_your_password	 => 	'Bạn đã quên mật khẩu ?' ,
	full_name	 => 	'Họ và tên' ,
	inbox	 => 	'Tin nhắn nhận được' ,
	key_search_empty	 => 	'Vui lòng nhập từ khóa' ,
	key_search_invalid	 => 	'Từ khóa tới thiểu là 1 ký tự' ,
	keyword	 => 	'Từ khóa' ,
	keyword_default	 => 	'Từ khóa' ,
	list_picture_product	 => 	'Dang sách các hình phụ cho tin rao này :' ,
	login_to_account	 => 	'Đăng nhập tài khoản' ,
	logout_account	 => 	'Thoát khỏi tài khoản' ,
	mail_new	 => 	'Tin nhắn mới' ,
	mailbox	 => 	'Tin nhắn' ,
	manage_estore	 => 	'Thông tin E-Store' ,
	map	 => 	'Vị trí Customization' ,
	maso	 => 	'Mã số' ,
	material_type	 => 	'Chất liệu:' ,
	mess_error_post	 => 	'Gửi bình luận không thành công, vui lòng thử lại' ,
	message	 => 	'Lời nhắn' ,
	message_email_post	 => 	'Thành viên post : <b>{poster}</b><p>Tên Customization : <b>{title}</b></p><p> Link chi tiết : {link_pro}</p>' ,
	my_bds	 => 	'Siêu thị địa ốc' ,
	my_custom	 => 	'Danh sách Customization' ,
	my_news	 => 	'Danh sách tin tức' ,
	my_product	 => 	'Danh sách tin đăng' ,
	no_accurate	 => 	'Tìm gần đúng' ,
	no_have_360	 => 	'Customization này chưa cập nhật thông tin về hình ảnh 360' ,
	no_have_custom	 => 	'Chưa có Customization nào' ,
	no_have_picture	 => 	'Chưa có hình nào' ,
	no_have_result	 => 	'Không tìm thấy kết quả nào' ,
	not_found	 => 	'Không tìm thấy Customization' ,
	note	 => 	'Nội dung' ,
	note_booking	 => 	'Chia sẻ với Bluesea điều quý khách mong muốn, chúng tôi sẽ đem đến những thiết kế phù hợp nhất!  ' ,
	note_key_search	 => 	'Kết quả tìm kiếm <b class=font_err>tất cả</b> với từ khóa  {keyword}' ,
	note_keyword	 => 	'Kết quả tìm kiếm với từ khóa  {keyword}' ,
	note_result	 => 	'Tổng cộng tìm thấy {totals} sản phẩm trong {num_pages} trang' ,
	note_success	 => 	'Cảm ơn bạn đã tham gia khảo sát của Bluesea. Chúng tôi đã ghi nhận thông tin của bạn. Chúng tôi sẽ xem xét yêu cầu của bạn và sẽ liên lạc lại với bạn trong thời gian sớm nhất. Nếu có bất cứ yêu cầu đặc biệt nào, vui lòng liên hệ trực tiếp hotline: 1900 9042  để được hỗ trợ tốt nhất.' ,
	other_custom	 => 	'Các Customization khác' ,
	persional_information	 => 	'Thông tin cá nhân' ,
	phone	 => 	'Điện thoại' ,
	picture	 => 	'Hình' ,
	please_chose_item	 => 	'Vui lòng chọn Item' ,
	please_login_member	 => 	'Vui lòng đăng nhập thành viên' ,
	price	 => 	'Giá trị tham khảo' ,
	price_default	 => 	'Liên hệ' ,
	prints	 => 	'In bài viết' ,
	product_name	 => 	'Tên sản phẩm' ,
	product_type	 => 	'Loại sản phẩm:' ,
	project_type	 => 	'Thực hiện cho công trình:' ,
	receiver	 => 	'Tên người nhận' ,
	register_estore	 => 	'Đăng ký E-Store' ,
	reply	 => 	'Trả lời' ,
	reply_default	 => 	'Nhập câu trả lời của bạn' ,
	request	 => 	'Yêu cầu' ,
	s_form	 => 	'từ' ,
	s_to	 => 	'đến' ,
	save	 => 	'Lưu tin' ,
	save_favorite	 => 	'Lưu lại tin này' ,
	search	 => 	'Tìm kiếm' ,
	search_by	 => 	'Tên kiếm theo' ,
	search_date	 => 	'Giới hạn tin theo ngày' ,
	security_code	 => 	'Mã bảo vệ' ,
	security_code_invalid	 => 	'Mã bảo vệ không đúng' ,
	select_category	 => 	'Chọn danh mục' ,
	select_custom	 => 	'--- Chọn Ý tưởng thiết kế ---' ,
	select_material	 => 	'-- Chọn chất liệu --' ,
	select_product	 => 	'-- Chọn loại sản phẩm --' ,
	select_project	 => 	'-- Chọn loại công trình --' ,
	select_status	 => 	'Chọn trạng thái' ,
	select_style	 => 	'-- Chọn phong cách --' ,
	send_comment_success	 => 	'Viết đánh giá thành công . Vui lòng đợi xét duyệt của BQT' ,
	send_email_success	 => 	'Gửi email thành công' ,
	send_reply_success	 => 	'Gửi câu trả lời thành công . Vui lòng đợi xét duyệt của BQT' ,
	sendbox	 => 	'Tin nhắn đã gửi' ,
	sender	 => 	'Tên người gửi' ,
	short	 => 	'Mô tả ngắn' ,
	sort_default	 => 	'Mặc định' ,
	sort_filter	 => 	'Phân loại theo:' ,
	sort_for	 => 	'Sắp xếp theo' ,
	sort_new	 => 	'Ý tưởng thiết kế mới' ,
	sort_old	 => 	'Ý tưởng thiết kế cũ' ,
	status	 => 	'Trạng thái' ,
	style_type	 => 	'Phong cách:' ,
	subject	 => 	'Tiêu đề' ,
	subject_booking	 => 	'Đơn đăng ký nhận báo giá mới' ,
	subject_email_post	 => 	'Có thành viên post Ý tưởng thiết kế mới từ' ,
	subject_email_tellfriend	 => 	'Giới thiệu Ý tưởng thiết kế tại {host}' ,
	survey_information	 => 	'Thông tin khảo sát' ,
	tellfriend	 => 	'Gửi cho bạn bè' ,
	template	 => 	'Mẫu nhà tham khảo' ,
	templates	 => 	'Một số mẫu nhà tham khảo của Customization' ,
	unsubscribe_email_list	 => 	'Hủy E-mail của Bạn ra khỏi hệ thống nhận maillist' ,
	update_account	 => 	'Thay đổi thông tin cá nhân và mật khẩu' ,
	view_asc	 => 	'Lượt xem tăng dần' ,
	view_desc	 => 	'Lượt xem giảm dần' ,
	view_detail	 => 	'[ Chi tiết ]' ,
	view_more	 => 	'xem thêm' ,
	views	 => 	'Lượt xem' ,
	your_comment	 => 	'Đánh giá của bạn' ,
); 
?>
