<!-- BEGIN: modules -->
<section class="banner mb_hidden"><img class="w-100 mb-2" src="{data.src}"></section>
    <div class="container mb_hidden">
      <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
           {data.navation}
      </nav>
    </div>
<style>

    @media (max-width: 767px){
        .mb_hidden {
            display: none;
        }
    }

</style>
  {data.main}
<!-- END: modules -->

<!-- BEGIN: item_about -->
<!-- <div class="about">
  <div class="grid">
    <div class="col">
      <div class="img">
        <a href="{data.link}" title="{data.title}"><img src="{data.src}" alt="{data.title}" /></a>
      </div>
    </div>
    <div class="col">
      <div class="caption">
        <div class="tend"><h3><a href="{data.link}" title="{data.title}">{data.title}</a></h3></div>
        <div class="des">{data.short}</div>
        <div class="link"><a href="{data.link}"><span>{LANG.about.view_detail}</span></a></div>
      </div>
    </div>
  </div>
</div>

<div class="col-12 col-lg-4"><a href="{data.link}">
    <div class="items-news-event">{data.pic}
      <h3 class="name-news-event fs-12 font-weight-bold">{data.title}</h3>
      <p class="fs-9 short-description-news">{data.short}</p>
    </div></a>
</div> -->



      <div class="col-12 col-lg-6 item-sp text-center mb-6">
                <div class="item-sp__image"><img src="{data.src}" alt="{data.title}" width="100%" /></div>
                <h5 class="fs-16 font-weight-medium">{data.title}</h5><a class="fs-10 text-center text-blue" href="{data.link}">{LANG.about.view_detail}</a>
              </div>
<!-- END: item_about -->