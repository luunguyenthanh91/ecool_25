<?php
/*================================================================================*\
|| 							Name code : about.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "about";

  function sMain (){
    global $vnT, $input;
    include ("function_".$this->module.".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE."/".$this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/".$this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/".$this->module . ".js");
		$vnT->setting['menu_active'] = $this->module;
		$itemID = (int) $input['itemID'];	
	 	if ($itemID) {
			$where = " AND display=1 AND n.aid=".$itemID;
			$result= $vnT->DB->query("SELECT * FROM about n,about_desc nd
																WHERE n.aid=nd.aid AND nd.lang='$vnT->lang_name' $where ");
			if($row = $vnT->DB->fetch_row($result)){
				if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
				if ($row['metakey'])	$vnT->conf['meta_keyword'] = $row['metakey'];
				$vnT->conf['indextitle'] =  $row['friendly_title'];
				$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL;
				$link_seo .= $row['friendly_url'].".html";
        $data['src'] = MOD_DIR_UPLOAD.'/'.$row['picture'];
				$data['main'] = $this->do_Detail($row);

        $navation = '<ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="'.$vnT->link_root.'">'.$vnT->lang['global']['homepage'].'</a></li>
          <li class="breadcrumb-item"><a href="'.$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['about'].'.html">'.$vnT->lang['about']['about'].'</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="#">'.$row['title'].'</a></li>
        </ol>';
			}else{
				@header("Location: ".$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['about'].".html");
				echo "<meta http-equiv='refresh' content='0; url=".LINK_MOD.".html'/>";
			}
		}else{
			if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
			if ($vnT->setting['metadesc']) $vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
			if ($vnT->setting['friendly_title']){
				$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];
			}
			$link_seo = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['about'].'.html';
			$data['main'] = $this->Do_ListAbout();
			$navation = '<ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="'.$vnT->link_root.'">'.$vnT->lang['global']['homepage'].'</a></li>
          <li class="breadcrumb-item"><a href="'.$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['about'].'.html">'.$vnT->lang['about']['about'].'</a></li>
        </ol>';
		}
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$data['navation'] = $vnT->lib->box_navation($navation);
		$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function Do_ListAbout(){
  	global $vnT, $input, $DB;
  	$text = '';
  	$result = $DB->query("SELECT * FROM about a, about_desc ad 
                          WHERE a.aid = ad.aid AND display = 1 AND lang = '$vnT->lang_name'
                          ORDER BY display_order ASC, date_post DESC");
    if($num = $DB->num_rows($result)){
    	$i = 1;
    	$text = '<div class="container pb-5 mb-3">
        <div class="row">';
    	while ($row = $DB->fetch_row($result)) {
    		$data['link'] = create_link($row['aid'],$row['friendly_url']);
    		$data['src'] = MOD_DIR_UPLOAD.'/'.$row['picture'];
    		$data['title'] = $vnT->func->HTML($row['title']);
    		$data['short'] = $row['short'];
	    	$this->skin->reset("item_about");
		    $this->skin->assign("data", $data);
		    $this->skin->parse("item_about");
		    $text .= $this->skin->text("item_about");
     		$i++;
    	}
    	$text .= '</div></div>';
    }
    $video = '';
    if($vnT->setting['video']){
      $bg = MOD_DIR_UPLOAD.'/'.$vnT->setting['video_bg'];
      $video = '<div class="aboutVideo" style="background-image:url('.$bg.')">
                  <div class="t1">'.$vnT->setting['video_title'].'</div>
                  <div class="t2">'.$vnT->setting['slogan'].'</div>
                  <div class="img"><img src="'.DIR_MOD_IMAGE.'/about/per.png"/></div>
                  <a href="'.$vnT->setting['video'].'" class="link fancybox.iframe"></a>
                </div>';
    }

    return  $text;
  
  }
  function do_Detail ($info){
  //   global $vnT, $input, $DB, $func;
		// $nd['f_title'] = '<h1>'.$func->HTML($info['title']).'</h1>';
  //   $nd['more'] = $vnT->func->HTML($info['slogan']);
		// $nd['content'] = '<div class="the_content desc">'.$info['content'].'</div>';
  //   return $vnT->skin_box->parse_box("box_middle", $nd);
    return $info['content'];
  }
}
?>