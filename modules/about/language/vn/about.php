<?php
/**
 * @Name: lang about
 * @version : 1.0
 * @date upgrade : 29/12/2007 by Thai son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$lang = array(
  about => 'Giới thiệu' , 
  cat_about => 'Giới thiệu về bệnh viện' , 
  sub_about => 'Nội dung khác' , 
  no_have_about => 'Chưa có nội dung' , 
  bntVote => 'Bình chọn' , 
  bntReset => 'Kết quả' , 
  poll => 'Thăm dò ý kiến' , 
  advertise => 'Quảng cáo' , 
  document => 'Tài liệu' , 
  mess_no_video => 'Chưa có video nào' , 
  other_video => 'Các video khác');
?>
