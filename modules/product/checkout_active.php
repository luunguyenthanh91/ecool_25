<?
if ( !defined('IN_vnT') )	{ die('Access denied');	}
$nts = new sMain();
class sMain{
var $output="";
var $skin="";

// Start func
function sMain(){
global $vnT,$input,$cart,$session,$conf,$func,$DB;
	require_once("includes/functions_shopping.php");
	// khoi tai session  do_finished
	$_SESSION['do_finished']=0;

	$err=$vnT->lang['mess_order_active'];
	if (isset($input['code'])) {
		$result = $vnT->DB->query("select * from tmp_order_sum where order_id='".$input['code']."'");
		if ($cot = $vnT->DB->fetch_row($result)){
			// insert
			$cot['order_id'] ="";
			$ok=$vnT->DB->do_insert("order_sum",$cot);
			if ($ok){
				$order_id = $DB->insertid();
				// insert detail
				$sql_cart = "select * from tmp_order_detail where order_id='".$input['code']."'";
				$result_cart = $vnT->DB->query ($sql_cart);
				$x = 0;	
				while ($cot1 = $vnT->DB->fetch_row($result_cart) ){
				   $p_id = $cot1["p_id"];
				   $sp[$x] = $p_id;
				   $cot1['order_id'] =$order_id;
				   $vnT->DB->do_insert("order_detail",$cot1);
				   $x++;
				}
			}
			
			 //san pham kem theo
			 for ($i=0;$i<count($sp);$i++){
			 	$pro_follow="";
				for ($j=0;$j<count($sp);$j++){
					if ($sp[$i]!=$sp[$j]){
						$pro_follow .= $sp[$j].",";
					}
				}

				$result= $DB->query("select pro_follow,p_id from products where p_id=".$sp[$i]);
				if ($row = $vnT->DB->fetch_row($result)){
					$pro_follow = substr($pro_follow,0,-1);
					if ($row['pro_follow']){
						$pro_follow = $pro_follow.",".$row['pro_follow'];
						$vnT->DB->query("UPDATE products set pro_follow='{$pro_follow}' where p_id=".$sp[$i]);
					}else {
						$vnT->DB->query("UPDATE products set pro_follow='{$pro_follow}' where p_id=".$sp[$i]);
					}
				}// end row
			 }// end for i

			//del cot tmp
			$DB->query("Delete from tmp_order_sum where order_id='".$input['code']."' ");
			$DB->query("Delete from tmp_order_detail where order_id='".$input['code']."' ");
			
			$mess = $vnT->lang['mess_active_order_success'];
			$url ="?soc=act:checkout_finished|order_id:".$vnT->func->NTS_encode($order_id);
			$out = $vnT->func->html_redirect($url,$mess);
			flush();
			echo $out;
			exit();
		}else{
			$err = $func->html_err($vnT->lang['err_active_code_incorrect']);
		}
	}
	
	$data['err'] = $err;
	$data['f_title'] = $vnT->lang['f_order_active'];
	$data['content'] = $this->html_main($data);
	$vnT->output.= $vnT->skin->html_box_mid($data);
	
}
// end func

//===================== html main
function html_main($data){
global $vnT,$input,$conf;
return<<<EOF
<br />
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<form action="?soc=act:checkout_active"  name="" method="post" >
				  
				  <tr>
					<td  align="center" class="font_err">{$data['err']}</td>
				  </tr>
				  <tr>
				    <td  align="center"><input name="code" id="code" type="text"  size="30" value="{$input['code']}"/>&nbsp;<input name="btnSubmit" type="submit" value="{$vnT->lang['btn_active']}" class="button" /></td>
			      </tr>
				</form>
		  </table>

<br />
<table  border="0" align="center" cellpadding="0" cellspacing="0">
				  <tr>
					<td>&nbsp;</td>
					<td height="25"  valign="middle"  colspan="3"><div align="center"> <strong>{$vnT->lang['order_progress']}</strong> <br />
					</div></td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td><img src="images/prog_left.gif"  /></td>
					<td><img src="images/orange.gif"  /></td>
					 <td><img src="images/orange.gif" /></td>
					<td><img src="images/orange.gif" /></td>
					<td><img src="images/prog_right_grey.gif"  /></td>
				  </tr>
				  <tr>
					<td height="25">0% </td>
					<td  colspan="3"><div align="center">50%</div></td>
					<td><div align="right">100%</div></td>
				  </tr>
</table>
<br>
EOF;
}
// end class
}
?>