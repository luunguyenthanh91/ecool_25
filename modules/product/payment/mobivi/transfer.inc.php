﻿<?php
 	
	function fixedVars ($info)
	{
		global $vnT, $conf, $input;
		$hiddenVars = "";
		return $hiddenVars;
	}
	
	
	function formAction($info)
	{
		global $vnT, $conf,$input ,$cart;
		
		// 1. Import Mobivi Checkout class
		$file_mobivi = $conf['rootpath']."modules/product/payment/mobivi/clsMobiviCheckout.php";
		if(!file_exists($file_mobivi)) die("Không tìm thấy File Mobivi Checkout của mobivi.vn ");
		include ($file_mobivi);
			
		// 2. Create Mobivi Checkout object and read merchant private key
		$payment_url = $vnT->module['payment_url'] ; 
		$mbv = new MobiviCheckout($payment_url);
		$read_key_result = $mbv->read_private_key($conf['rootpath']."modules/product/payment/mobivi/keys/beesnext_pri.pem");
		
		// 3. Fill the request with your cart information (the amount customer needs to
		// pay, cart items, etc.) then send request
 
		$tax = 0;
		$mbvRequest = new MobiviCheckoutRequest();
		$mbvRequest->SerialID = uniqid();
		$mbvRequest->From = $vnT->module['merchant_name'];
		$mbvRequest->To = $info['d_name'] ;
		
		$mbvRequest->Description = "Hóa đơn mua hàng ID : #".$vnT->module['order_code'];
		$mbvRequest->Amount = $vnT->module['total_price'];
		$mbvRequest->Tax = $tax;
		
		$cart_order_id =  base64_encode($vnT->module['order_code']); 
		 
		$mbvRequest->ReturnURL = $conf['rooturl']."san-pham/checkout_cancel.html/?order_code=". $cart_order_id ; 
		$mbvRequest->DoneURL =  $conf['rooturl']."san-pham/checkout_finished.html/?order_code=" . $cart_order_id ; 
 		
		$contents = $cart->display_contents($cart->session);
		if($cart->num_items($cart->session))  { 
			$x = 0;
			while($x < $cart->num_items($cart->session))
			{
				 if ($contents["price"][$x]){ 
						$price= $contents["price"][$x];
				 }else $price=0;
				 
 				 $maso =  $contents["p_id"][$x] ;
				 $mbvRequest->add_invoice_item(html_entity_decode($contents["p_name"][$x], ENT_NOQUOTES, "UTF-8"), $price, $contents["quantity"][$x], false, $maso, html_entity_decode("", ENT_NOQUOTES, "UTF-8"));				 
				 
				 $x++;
			}// ends loop for each product
		}
		
		//shipping
		if($vnT->module['s_price']) {
			 $mbvRequest->add_invoice_item(html_entity_decode("Giá vận chuyển (Shipping)", ENT_NOQUOTES, "UTF-8"), $vnT->module['s_price'], 1, false, "Shipping" , html_entity_decode("", ENT_NOQUOTES, "UTF-8"));
		}		 
			 
		$send = $mbv->send($vnT->module['merchant_username'], $mbvRequest, $redirect_url);
		$mbv->dispose();		
		 
		// 4. Redirect customer to Mobivi Checkout page (URL in request's response)
		if ($send) {
			$formAction = $redirect_url;
		} else {
			die($redirect_url);
		}
 
		return $formAction;			
	}
	 
	//========
 

$is_mobivi = 1;
$is_online = 1;
$is_insert = "yes"

?>