﻿<?
include_once ("lib/Config.php");
include_once ('lib/PayooNotify.php');
/*
include_once('lib/Config.php');
include_once('lib/PayooNotify.php');*/
try {
  // Make sure the user is posting
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Read the input from stdin
    $NotifyMessage = stripcslashes($_POST["NotifyData"]);
    if ($NotifyMessage == null || '' === $NotifyMessage)
      return;
    $listener = new PayooNotify($NotifyMessage);
    if ($listener->CheckNotifyMessage()) {
      $invoice = $listener->GetPaymentNotify();
      if ($listener->ConfirmToPayoo()) {
        //Check the invoice state is PAYMENT_RECEIVED
        //Check that payment amount is correct
        //Update database and process the invoice
        $out = fopen('tempdir/invoice.txt', 'w');
        fwrite($out, "OrderNo: " . $invoice->getOrderNo());
        fwrite($out, "\r\nOrderCashAmount: " . $invoice->getOrderCashAmount());
        fwrite($out, "\r\nState: " . $invoice->getState());
        //... so on...
        fclose($out);
      } else {
        //ConfirmToPayoo fail. Log for manual investigation.
        $log = fopen('tempdir/log.txt', 'w');
        fwrite($log, 'ConfirmToPayoo fail. OrderNo => ' . $invoice->getOrderNo());
        fclose($log);
      }
    } else {
      //Verify digital signature fail. Log for manual investigation.
      $log = fopen('tempdir/log.txt', 'w');
      fwrite($log, 'Verify digital signature fail');
      fclose($log);
    }
  }
} catch (Exception $ex) {
  throw $ex;
}
?>