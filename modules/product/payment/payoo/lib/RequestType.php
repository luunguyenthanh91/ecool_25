<?php

abstract class RequestType
{
  public $DigitalSignature;
}

class GetOrderInformationRequestType extends RequestType
{
  public $OrderID;
  public $ShopID;

  function GetXml ()
  {
    $xml = '<?xml version="1.0"?>
<GetOrderInformationRequestType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <OrderID xmlns="BusinessAPI">' . $this->OrderID . '</OrderID>
  <ShopID xmlns="BusinessAPI">' . $this->ShopID . '</ShopID>
</GetOrderInformationRequestType>';
    return $xml;
  }
}

class SendOrderRequestType extends RequestType
{
  public $BusinessID;
  public $ShopID;
  public $OrderID;
  public $CashAmount;
  public $StartShippingDate;
  public $ShippingDays;
  public $OrderDescription;
  public $HttpReference;
  public $ShopBackUrl;

  public function GetXml ()
  {
    $xml = '<?xml version="1.0"?>
<SendOrderRequestType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <BusinessID xmlns="BusinessAPI">' . $this->BusinessID . '</BusinessID>
  <ShopID xmlns="BusinessAPI">' . $this->ShopID . '</ShopID>
  <OrderID xmlns="BusinessAPI">' . $this->OrderID . '</OrderID>
  <CashAmount xmlns="BusinessAPI">' . $this->CashAmount . '</CashAmount>
  <StartShippingDate xmlns="BusinessAPI">' . $this->StartShippingDate . '</StartShippingDate>
  <ShippingDays xmlns="BusinessAPI">' . $this->ShippingDays . '</ShippingDays>
  <OrderDescription xmlns="BusinessAPI">' . htmlspecialchars($this->OrderDescription, ENT_NOQUOTES) . '</OrderDescription>
  <HttpReference xmlns="BusinessAPI">' . $this->HttpReference . '</HttpReference>
  <ShopBackUrl xmlns="BusinessAPI">' . $this->ShopBackUrl . '</ShopBackUrl>
</SendOrderRequestType>';
    return $xml;
  }
}

class UpdateOrderStatusRequestType extends RequestType
{
  public $ShopID;
  public $OrderID;
  public $NewStatus;
  public $UpdateLog;

  function GetXml ()
  {
    $xml = '<?xml version="1.0"?>
<UpdateOrderStatusRequestType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ShopID xmlns="BusinessAPI">' . $this->ShopID . '</ShopID>
  <OrderID xmlns="BusinessAPI">' . $this->OrderID . '</OrderID>
  <NewStatus xmlns="BusinessAPI">' . $this->NewStatus . '</NewStatus>
  <UpdateLog xmlns="BusinessAPI">' . $this->UpdateLog . '</UpdateLog>
</UpdateOrderStatusRequestType>';
    return $xml;
  }
}

class ConfirmNotifyInformationRequestType extends RequestType
{
  public $NotifyData;
  public $PayooSessionID;
  public $NotifyCommand;

  function GetXml ()
  {
    $xml = '<?xml version="1.0"?>
<ConfirmNotifyInformationRequestType xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <NotifyData xmlns="BusinessAPI">' . $this->NotifyData . '</NotifyData>
  <PayooSessionID xmlns="BusinessAPI">' . $this->PayooSessionID . '</PayooSessionID>
  <NotifyCommand xmlns="BusinessAPI">' . $this->NotifyCommand . '</NotifyCommand>
</ConfirmNotifyInformationRequestType>';
    return $xml;
  }
}
?>