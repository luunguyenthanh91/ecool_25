<?php
/****************************************
 *
 * Please edit the options below to reflect
 * your system configuration. If they are
 * incorrect, this program may not work as
 * expected.
 *
 ****************************************/
define('_BusinessUsername', $vnT->module['BusinessUsername']); //Username of business account
define('_ShopID', $vnT->module['ShopID']);
define('_ShopTitle', $vnT->module['ShopTitle']);
define('_ShopDomain', $vnT->module['ShopDomain']);
define('_ShopBackUrl', $conf['rooturl'] . $vnT->cmd . "=mod:product|act:payoo_result|do:back");
define('_NotifyUrl', $conf['rooturl'] .'modules/product/payment/payoo/NotifyListener.php');
//define('_NotifyUrl',$conf['rooturl'].$vnT->cmd."=mod:product|act:NotifyListener");
define('_PayooPaymentUrl', $vnT->module['PayooPaymentUrl']);
//Callerservices Config
define('_BusinessAPIUrl', $vnT->module['BusinessAPIUrl']);
define('_APIUsername', $vnT->module['APIUsername']);
define('_APIPassword', $vnT->module['APIPassword']);
define('_APISignature', $vnT->module['APISignature']);

/*define('_BusinessAPIUrl',$vnT->module['BusinessAPIUrl']);
		define('_APIUsername',$vnT->module['APIUsername']);
		define('_APIPassword',$vnT->module['APIPassword']);
		define('_APISignature',$vnT->module['APISignature']);*/
//Base Directory - Base directory where all files will be stored
//Base Directory - Base directory where all files will be stored
		define ('_PayooPath', realpath (dirname (__FILE__).'/').'/../');
		
		//Certificate Names - Names of all the certificates required
		//Your Private Key Filename
		define('_PrivateKey',_PayooPath.'certificate/muahangtragop_private_key.pem');
		//Your Public Certificate Filename
		define('_PublicCertificate',_PayooPath.'certificate/muahangtragop_public_cert.pem');
		//Payoo's Public Certificate Filename
		define('_PayooPublicCertificate',_PayooPath.'certificate/payoo_public_cert.pem');
		
//Temporary Directory - Where temporary files are stored regarding
//the transaction. This should be under the base directory OR outside
//the webroot, and only readable by you/the web server. Files from this
//directory are automatically removed after use. The trailing slash is REQUIRED
//Thu muc temp, co quyen ghi cho he thong.
define('_TempDirectory', _PayooPath . 'tempdir');
include_once (_PayooPath . 'lib/DigitalSignature.php');
?>