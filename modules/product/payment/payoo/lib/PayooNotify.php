﻿<?
include_once ('CallerServices.php');

class PayooNotify
{
  private $NotifyData = "";
  private $Signature = "";
  private $PayooSessionID = "";

  public function PayooNotify ($NotifyData)
  {
    $doc = new DOMDocument();
    $doc->loadXML($NotifyData);
    $this->NotifyData = $this->ReadNodeValue($doc, "Data");
    $this->Signature = $this->ReadNodeValue($doc, "Signature");
    $this->PayooSessionID = $this->ReadNodeValue($doc, "PayooSessionID");
  }

  public function CheckNotifyMessage ()
  {
    try {
      if (trim($this->NotifyData) == "" || trim($this->Signature) == "") {
        throw new Exception('Invalid NotifyData');
      }
      $crypt = new DigitalSignature();
      $crypt->LoadPrivateKey(_PrivateKey);
      $crypt->LoadPublicCertificate(_PublicCertificate);
      $crypt->SetPartnerPubCertPath(_PayooPublicCertificate);
      $crypt->SetTempDir(_TempDirectory);
      return $crypt->Verify($this->NotifyData, $this->Signature);
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  public function ConfirmToPayoo ()
  {
    try {
      if (trim($this->NotifyData) == "" || trim($this->PayooSessionID) == "") {
        throw new Exception('Invalid NotifyData');
      }
      $caller = new CallerServices();
      $request = new ConfirmNotifyInformationRequestType();
      $request->NotifyData = $this->NotifyData;
      $request->PayooSessionID = $this->PayooSessionID;
      $request->NotifyCommand = "NOTIFY_VALIDATE";
      $response = $caller->Call("ConfirmNotifyInformation", $request);
      if ($response->Ack == "Success" || $response->Ack == "SuccessWithWarning") {
        if ("VERIFIED" == strtoupper($response->ConfirmResult))
          return TRUE;
        else
          return FALSE;
      } else {
        return FALSE;
      }
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  public function GetPaymentNotify ()
  {
    try {
      if (trim($this->NotifyData) == "") {
        throw new Exception('Invalid NotifyData');
      }
      $Data = base64_decode($this->NotifyData);
      $invoice = new PaymentNotification();
      $doc = new DOMDocument();
      $doc->loadXML($Data);
      $invoice->setSession($this->ReadNodeValue($doc, "session"));
      $invoice->setBusinessUsername($this->ReadNodeValue($doc, "username"));
      $invoice->setShopID($this->ReadNodeValue($doc, "shop_id"));
      $invoice->setShopTitle($this->ReadNodeValue($doc, "shop_title"));
      $invoice->setShopDomain($this->ReadNodeValue($doc, "shop_domain"));
      $invoice->setShopBackUrl($this->ReadNodeValue($doc, "shop_back_url"));
      $invoice->setOrderNo($this->ReadNodeValue($doc, "order_no"));
      $invoice->setOrderCashAmount($this->ReadNodeValue($doc, "order_cash_amount"));
      $invoice->setStartShippingDate($this->ReadNodeValue($doc, "order_ship_date"));
      $invoice->setShippingDays($this->ReadNodeValue($doc, "order_ship_days"));
      $invoice->setOrderDescription(urldecode(($this->ReadNodeValue($doc, "order_description"))));
      $invoice->setNotifyUrl($this->ReadNodeValue($doc, "notify_url"));
      $invoice->setState($this->ReadNodeValue($doc, "State"));
      return $invoice;
    } catch (Exception $ex) {
      throw ex;
    }
  }

  private function ReadNodeValue ($Doc, $TagName)
  {
    $nodeList = $Doc->getElementsByTagName($TagName);
    return $nodeList->item(0)->nodeValue;
    ;
  }
}

class PaymentNotification
{
  private $Session = "";
  private $BusinessUsername = "";
  private $ShopID = 0;
  private $ShopTitle = "";
  private $ShopDomain = "";
  private $ShopBackUrl = "";
  private $OrderNo = "";
  private $OrderCashAmount = 0;
  private $StartShippingDate = ""; //Format: dd/mm/yyyy
  private $ShippingDays = - 1;
  private $OrderDescription = "";
  private $NotifyUrl = "";
  private $State = "";

  public function getSession ()
  {
    return $this->Session;
  }

  public function getBusinessUsername ()
  {
    return $this->BusinessUsername;
  }

  public function getShopID ()
  {
    return $this->ShopID;
  }

  public function getShopTitle ()
  {
    return $this->ShopTitle;
  }

  public function getShopDomain ()
  {
    return $this->ShopDomain;
  }

  public function getShopBackUrl ()
  {
    return $this->ShopBackUrl;
  }

  public function getOrderNo ()
  {
    return $this->OrderNo;
  }

  public function getOrderCashAmount ()
  {
    return $this->OrderCashAmount;
  }

  public function getStartShippingDate ()
  {
    return $this->StartShippingDate;
  }

  public function getShippingDays ()
  {
    return $this->ShippingDays;
  }

  public function getOrderDescription ()
  {
    return $this->OrderDescription;
  }

  public function getNotifyUrl ()
  {
    return $this->NotifyUrl;
  }

  public function getState ()
  {
    return $this->State;
  }

  public function setSession ($Session)
  {
    $this->Session = $Session;
  }

  public function setBusinessUsername ($BusinessUsername)
  {
    $this->BusinessUsername = $BusinessUsername;
  }

  public function setShopID ($ShopID)
  {
    $this->ShopID = $ShopID;
  }

  public function setShopTitle ($ShopTitle)
  {
    $this->ShopTitle = $ShopTitle;
  }

  public function setShopDomain ($ShopDomain)
  {
    $this->ShopDomain = $ShopDomain;
  }

  public function setShopBackUrl ($ShopBackUrl)
  {
    $this->ShopBackUrl = $ShopBackUrl;
  }

  public function setOrderNo ($OrderNo)
  {
    $this->OrderNo = $OrderNo;
  }

  public function setOrderCashAmount ($OrderCashAmount)
  {
    $this->OrderCashAmount = $OrderCashAmount;
  }

  public function setStartShippingDate ($StartShippingDate)
  {
    $this->StartShippingDate = $StartShippingDate;
  }

  public function setShippingDays ($ShippingDays)
  {
    $this->ShippingDays = $ShippingDays;
  }

  public function setOrderDescription ($OrderDescription)
  {
    $this->OrderDescription = $OrderDescription;
  }

  public function setNotifyUrl ($NotifyUrl)
  {
    $this->NotifyUrl = $NotifyUrl;
  }

  public function setState ($State)
  {
    $this->State = $State;
  }
}
?>