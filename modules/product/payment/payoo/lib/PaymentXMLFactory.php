<?php

class PaymentXMLFactory
{
  public $BusinessUsername = '';
  public $ShopID = 0;
  public $ShopTitle = '';
  public $ShopDomain = '';
  public $ShopBackUrl = '';
  public $Sign;
  public $PayooPaymentUrl = '';
  public $Session = '';
  public $OrderNo = '';
  public $CashAmount = 0;
  public $StartShippingDate = '';
  public $ShippingDays = - 1;
  public $OrderDescription = '';
  public $NotifyUrl = '';
  private $IsEncrypted = TRUE;

  function PaymentXMLFactory ($Encrypted = TRUE)
  {
    try {
      $this->IsEncrypted = $Encrypted;
      if ($Encrypted == TRUE) {
        $this->Sign = new DigitalSignature();
        $this->Sign->LoadPrivateKey(_PrivateKey);
        $this->Sign->LoadPublicCertificate(_PublicCertificate);
        $this->Sign->SetPartnerPubCertPath(_PayooPublicCertificate);
        $this->Sign->SetTempDir(_TempDirectory);
      }
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  private function ValidateData ()
  {
    if (trim($this->PayooPaymentUrl) == '')
      return 'PayooPaymentUrl is not set.';
    if (trim($this->BusinessUsername) == '')
      return 'BusinessUsername is not set.';
    if (trim($this->ShopID) == 0)
      return 'ShopID is not set.';
    if (trim($this->ShopTitle) == '')
      return 'ShopTitle is not set.';
    if (trim($this->ShopDomain) == '')
      return 'ShopDomain is not set.';
    if (trim($this->ShopBackUrl) == '')
      return 'ShopBackUrl is not set.';
    if (trim($this->OrderNo) == '')
      return 'OrderNo is not set.';
    if ($this->CashAmount <= 0)
      return 'CashAmount must not be a negative number or zero.';
    if (trim($this->StartShippingDate) == '' || strlen(trim($this->StartShippingDate)) < 10)
      return 'StartShippingDate is not set or invalid format (dd/mm/yyyy).';
    if ($this->ShippingDays < 0)
      return 'ShippingDays must not be a negative number.';
    if (trim($this->OrderDescription) == '')
      return 'OrderDescription is not set.';
    return '';
  }

  function GetPaymentXml ()
  {
    try {
      $xml = $this->BuildPaymentXml();
      if ($this->IsEncrypted == TRUE)
        return $this->Sign->SignAndEncrypt($xml);
      return $xml;
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  function BuildPaymentXml ()
  {
    $Message = $this->ValidateData();
    if ($Message != '')
      throw new Exception($Message);
    $xml = '
		  <shops>
			  <shop>
			    <session>' . $this->Session . '</session>
  			    <username>' . $this->BusinessUsername . '</username>
  			    <shop_id>' . $this->ShopID . '</shop_id>
  			    <shop_title>' . $this->ShopTitle . '</shop_title>
			    <shop_domain>' . $this->ShopDomain . '</shop_domain>
			    <shop_back_url>' . urlencode($this->ShopBackUrl) . '</shop_back_url>
			    <order_no>' . $this->OrderNo . '</order_no>
			    <order_cash_amount>' . $this->CashAmount . '</order_cash_amount>
			    <order_ship_date>' . $this->StartShippingDate . '</order_ship_date>
			    <order_ship_days>' . $this->ShippingDays . '</order_ship_days>
			    <order_description>' . urlencode($this->OrderDescription) . '</order_description>
			    <notify_url>' . urlencode($this->NotifyUrl) . '</notify_url>
			  </shop>
		  </shops>';
    return $xml;
  }

  function PayNow ()
  {
    try {
      ?>
<html>
<head>
<title></title>
</head>
<body>
<form name="frmPaynow" style='margin-top: 50px; text-align: center;'
	action='<?=$this->PayooPaymentUrl?>' method="post">
<noscript><input type='submit' value='Click if not redirected' /></noscript>
<div id='ContinueButton' style='display: none;'><input type='submit'
	value='Click if not redirected' /></div>
<input type='hidden' name='OrdersForPayoo'
	value='<?=$this->GetPaymentXml()?>' /></form>
<script type='text/javascript'>
			window.onload = function()
			{document.forms[0].submit();setTimeout(function() {document.getElementById('ContinueButton').style.display = '';}, 1000);}
			</script>
</body>
</html>
<?php
    } catch (Exception $ex) {
      throw $ex;
    }
  }
}
?>