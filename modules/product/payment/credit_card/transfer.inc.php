<?php


//========
function fixedVars ($info)
{
  global $vnT, $DB, $conf, $input, $session, $cart;
  
	$amount = $vnT->module['total_price'] ;
	$order_desc =  $vnT->lang['product']['order_desc']." (#".$vnT->module['order_code'].")";
	$paymentType = ($vnT->module['paymentType']) ? $vnT->module['paymentType'] : "Sale";
	$creditCardType = $input['creditCardType'];
	$ccnum = $input['ccnum'];
	$cc_code = $input['cc_cvv2'];
	$ccexp = $input['ccexpm'].$input['ccexpy'];
	
	// Arrange the data into name/value pairs ready to send
		$an_values = array (
			 
			"paymentType" => $paymentType,
			"firstName" => $info['d_fname'],
			"lastName" => $info['d_lname'],
			"creditCardType" => $creditCardType,			
			"creditCardNumber"		=>  $ccnum,			
			"expDateMonth"		=>  $input['ccexpm'], 
			"expDateYear"			=> $input['ccexpy'],
			"cvv2Number"	=>	$cc_code,
			'address1'			=> $info['d_address'],
			"address2"			=> $info['d_address2'],
			"city"				=> $info['d_city'],
			"state"				=> $info['d_state'],
			"zip"			=> $info['d_zipcode'],
			"amount"			=> $amount
		);
		
		$hiddenVars ='';
		foreach($an_values as $k=>$v) {
		
			$hiddenVars .="<input type='hidden' name='".$k."' value='" . $v . "' />"."\n";
		}
		
  return $hiddenVars;
}

 

//========
function ProcessPaymentForm($info)
{
	global $vnT, $DB, $func, $conf, $input,  $cart , $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header, $subject, $AUTH_token,$AUTH_signature,$AUTH_timestamp;
	
	$file_paypal = $vnT->conf['rootpath']."modules/product/payment/paypal/CallerService.php";
	if (! file_exists($file_paypal)) die("Not found File Paypal ");
	include ($file_paypal);
 	$arr_return = array();
 	
	 
	$amount = $vnT->module['total_price'] ;	
	$paymentType = ($vnT->module['paymentType']) ? $vnT->module['paymentType'] : "Sale";
	
	$firstName =urlencode( $info['d_fname']);
	$lastName =urlencode( $info['d_lname']);
	$creditCardType =urlencode( $_POST['creditCardType']);
	$creditCardNumber = urlencode($_POST['ccnum']);
	$expDateMonth =urlencode( $_POST['ccexpm']);
	
	// Month must be padded with leading zero
	$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
	
	$expDateYear =urlencode( $_POST['ccexpy']);
	$cvv2Number = urlencode($_POST['cc_cvv2']);
	$address1 = urlencode($info['d_address']);

	$city = urlencode($info['d_city']);
	$state =urlencode( $info['d_state']);
	$zip = urlencode($info['d_zipcode']);
	$amount = urlencode($amount); 
	$currencyCode="USD"; 
	$customer_email = urlencode($info['d_email']); 
	/* Construct the request string that will be sent to PayPal.
		 The variable $nvpstr contains all the variables and is a
		 name value pair string with & as a delimiter */
	$nvpstr="&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=".         $padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address1&CITY=$city&STATE=$state".
	"&ZIP=$zip&COUNTRYCODE=US&CURRENCYCODE=$currencyCode&L_EMAIL0=".$customer_email; 
		 
		$resArray = hash_call("doDirectPayment",$nvpstr);
		
		$ack = strtoupper($resArray["ACK"]);

		if($ack!="SUCCESS")  {
			$arr_return['ok'] =0;
			$arr_return['error'] = $resArray['L_LONGMESSAGE0'];
		}else{

			$dup['status'] = $vnT->module['status_payment'] ;			 
 			$vnT->DB->do_update("order_sum",$dup," order_code='".$vnT->module['order_code']."' ") ;
		 	
			$arr_return['ok'] =1;
			$arr_return['error'] = '';
			
		}
		/*echo " <table width = 400>";
			foreach($resArray as $key => $value) {    			
    		echo "<tr><td> $key:</td><td>$value</td>";
			}	
		echo " </table>";
 		*/ 
 	
	return $arr_return ;
	
}


function formAction ()
{
  global $vnT, $conf, $input;
 	 
	if ($vnT->module['testMode'] == 1) {
		$link_action = "https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=";		 
	} else {
		$link_action = "https://www.paypal.com/webscr&cmd=_express-checkout&token="; 
	} 
  return $link_action ;
}

function formPayment ($info)
{
  global $vnT, $conf, $input;
 	
	$data['ccname'] = ($input['ccname']) ? $input['ccname'] : $info['d_name'];

	$list_ccexpm = '<select id="ccexpm" name="ccexpm" size=1 class="select"  style="width:100px;">' ; 
	
	for($i = 1; $i <= 12; $i++) {
		$stamp = mktime(0, 0, 0, $i, 15, date("Y"));
		$val = str_pad($i, 2, "0", STR_PAD_LEFT);

		if (@$_POST['ccexpm'] == $i) {
			$sel = 'selected="selected"';
		} else {
			$sel = "";
		}

		$list_ccexpm .= sprintf("<option %s value='%s'>%s</option>", $sel, $i,$val);
	}
	$list_ccexpm .= '</select>' ;
	$data['list_ccexpm'] = $list_ccexpm;
	
	
	$list_ccexpy = '<select id="ccexpy" name="ccexpy" size=1 class="select" style="width:100px;">' ; 
	for($i = date("Y"); $i < date("Y")+10; $i++) {
		if (@$_POST['ccexpy'] == $i) {
			$sel = 'selected="selected"';
		} else {
			$sel = "";
		}
		$list_ccexpy .= sprintf("<option %s value='%s'>%s</option>", $sel, $i, $i);
	}
	$list_ccexpy .= '</select>' ;
	
	$data['list_ccexpy'] = $list_ccexpy;
	
	
	$list_credit_card_type =  vnT_HTML::selectbox("creditCardType", array(     'Visa' => 'Visa' , 'MasterCard' => 'MasterCard' , 'Discover' => 'Discover', 'Amex' => 'American Express'   ), $input['creditCardType'],"","style='width:200px'");
		
	$data['list_credit_card_type'] = $list_credit_card_type;
	$text = load_html("formPaymentPayPal",$data);
  return $text ;
}


///////////////////////////
// Other Vars
////////
 
$is_online = 1;
$is_insert = "yes"

?>