<?php 
	$vnT->setting['payment']['ok'] = 0;
	$vnT->setting['payment']['mess'] = '';
	
	global $API_Endpoint,$version,$API_UserName,$API_Password,$API_Signature,$nvp_Header, $subject, $AUTH_token,$AUTH_signature,$AUTH_timestamp;
	
	$path_payment =$vnT->conf['rootpath']."modules/product/payment/paypal/CallerService.php";
	if (! file_exists($path_payment)) die("Not found File Paypal ");
	include ($path_payment);
	
	$token =urlencode( $_REQUEST['token']);

	/* Build a second API request to PayPal, using the token as the
	ID to get the details on the payment authorization
	*/
	 $nvpstr="&TOKEN=".$token;
	 //echo "nvpstr = ".$nvpstr;
	 
	/* Make the API call and store the results in an array.  If the
	call was a success, show the authorization details, and provide
	an action to complete the payment.  If failed, show the error
	*/
	 $resArray=hash_call("GetExpressCheckoutDetails",$nvpstr);
	/* echo "<pre>";
	 print_r($resArray);
	 echo "</pre>";
	*/
	 $_SESSION['reshash']=$resArray;
	 $ack = strtoupper($resArray["ACK"]);
	
	if($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING'){
		$vnT->setting['payment']['ok'] = 1 ;	
	} else  {
		$vnT->setting['payment']['mess'] = '';
	}
	  
?>