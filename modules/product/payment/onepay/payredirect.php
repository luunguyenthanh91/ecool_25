<?php
$vnT->setting['payment']['ok'] = 0;
$vnT->setting['payment']['mess'] = '';

// If input is null, returns string "No Value Returned", else returns input
function null2unknown($data) {
    if ($data == "") {
        return "No Value Returned";
    } else {
        return $data;
    }
} 

$SECURE_SECRET = $vnT->module['SECURE_SECRET']; 
$vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
unset($_GET["vpc_SecureHash"]); 
 
$errorExists = false;

if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {

    $md5HashData = $SECURE_SECRET;

    // sort all the incoming vpc response fields and leave out any with no value
    foreach($_GET as $key => $value) {
        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
            $md5HashData .= $value;
        }
    }
     
    if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) {
        // Secure Hash validation succeeded, add a data field to be displayed
        // later.
        $hashValidated = "<FONT color='#00AA00'><strong>CORRECT</strong></FONT>";
    } else {
        // Secure Hash validation failed, add a data field to be displayed
        // later.
        $hashValidated = "<FONT color='#FF0066'><strong>INVALID HASH</strong></FONT>";
        $errorExists = true;
    }
} else {
    // Secure Hash was not validated, add a data field to be displayed later.
    $hashValidated = "<FONT color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></FONT>";
}


$amount          = null2unknown($_GET["vpc_Amount"]);
$locale          = null2unknown($_GET["vpc_Locale"]);
$command         = null2unknown($_GET["vpc_Command"]);
$version         = null2unknown($_GET["vpc_Version"]);
$orderInfo       = null2unknown($_GET["vpc_OrderInfo"]);
$merchantID      = null2unknown($_GET["vpc_Merchant"]);
$merchTxnRef     = null2unknown($_GET["vpc_MerchTxnRef"]);
$transactionNo   = null2unknown($_GET["vpc_TransactionNo"]);
$txnResponseCode = null2unknown($_GET["vpc_TxnResponseCode"]);
 

$errorTxt = "";

// Show this page as an error page if vpc_TxnResponseCode equals '7'
if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
    $errorTxt = "Error ";
}
     

 
	// Neu giao dich thanh cong
	if ( ($txnResponseCode==0)&&(strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) ) {
			// echo $message ;
			
			$sql_ck  = "select * from order_sum where order_code='".$HTTP_SESSION_VARS["order_id"]."' "; 
			$result = $vnT->DB->query($sql_ck); 
			if($vnT->DB->fetch_row($result)){ // kiem tra ton tai order 
				$vnT->setting['payment']['ok'] = 1;	
				 
			}else{
				$vnT->setting['payment']['ok'] = 0;
				$vnT->setting['payment']['mess'] = 'Khong ton tai don hang : '.$HTTP_SESSION_VARS["order_id"];
			}  
 
	}else{
		$vnT->setting['payment']['ok'] = 0;
		$vnT->setting['payment']['mess'] = " Qua trinh thanh toan khong thanh cong " . $hashValidated ;
	}
?>    

