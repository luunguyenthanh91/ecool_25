<!-- BEGIN: html_row_cart -->
<tr>
  <td>
    {data.item_title}
  </td>
  <td class="price">{data.price}</td>
  <td>
    <div class="choose-quantity">
      {data.quantity}
    </div>
  </td>
  <td class="price colorRed" id="ext_total_{data.id}">{data.total}</td>
  <td>
    <a class="btnclear" href="{data.link_remove}" title="{LANG.product.remove}"><i class="fa fa-trash"></i></a>
  </td>
</tr>
<!-- END: html_row_cart -->