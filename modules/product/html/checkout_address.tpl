<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/product/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>

<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.nav_shopping}
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          {data.form_login}
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          {data.customer_address}
        </div>
      </div>
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: html_from_login -->
<script src="https://apis.google.com/js/api:client.js"></script>
<div id="fb-root"></div>
<div class="form-login">
  {data.err}
  <form role="form" action="{data.link_action}" method="post" id="loginForm" name="loginForm" autocomplete="off">
    <div class="ftitle">{LANG.product.member_login}</div>
    <div class="fl-content">
      <div class="wrap_formLogin">
        <div class="row_input">
          <input type="text" class="form-control required" id="username" name="username" autocomplete="off" vk_1c584="subscribed" placeholder="{LANG.product.username}"/>
        </div>
        <div class="row_input">
          <input type="password" name="password" id="password" class="form-control required" placeholder="{LANG.product.password}" autocomplete="off" vk_1c584="subscribed"/>
        </div>
        <div class="remember_forget">
          <ul>
            <li><label>
              <input type="checkbox" name="ch_remember" id="ch_remember" value="1" checked="checked"/>
              {LANG.product.remember_login}
            </label></li>
            <li><a href="{data.link_lostpass}">{LANG.product.lostpass}</a></li>
          </ul>
        </div>
        <div class="row_input divButton">
          <div class="grid-button">
            <div class="col">
              <input type="hidden" name="do_login" value="1"/>
              <button id="btnLogin" name="btnLogin" type="submit" class="btnCart blood" value="1">
                <span>{LANG.product.btn_login}</span>
              </button>
            </div>
            <div class="col">
              <a href="{data.link_register}" class="btnCart">{LANG.product.btn_register}</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="login_social">
  <div class="ls_text">{LANG.product.social_login}</div>
  <div class="ls_button ls_facebook">
    <a href="javascript:void(0);" id="fbLogin"><span class="fa-facebook">{LANG.product.fblogin}</span></a>
  </div>
  <div class="ls_button ls_google">
    <a href="javascript:void(0);" id="gpLogin"><span class="fa-google">{LANG.product.gplogin}</span></a>
  </div>
  <span id="loading" style="display: none"><img src="{DIR_MOD}/images/loading.gif" alt="login loading"/></span>
  <script type="text/javascript">
    var facebook_appId = "{data.facebook_appId}";
    var google_appId = "{data.google_appId}";
    initLogin();
  </script>
  <div class="clear"></div>
</div>
<!-- END: html_from_login -->

<!-- BEGIN: customer_address -->
<div class="form-buy nobg">
  <form role="form" action="{data.link_action}" method="post" autocomplete="off">
    <div class="ftitle">{LANG.product.new_customer}</div>
    <div class="fb-content">
      {data.note_new_customer}
      <div class="divButton">
        <button type="submit" class="btnCart" name="btnConfirm" value="1">
          <span>{LANG.product.btn_continue_checkout}</span>
        </button>
      </div>
    </div>
  </form>
</div>
<!-- END: customer_address -->