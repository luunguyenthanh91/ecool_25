<!-- BEGIN: table_col -->
<div class="item">
  <div class="product" itemscope itemtype="http://schema.org/Product">
    <div class="img">
      <a href="{data.link}" itemprop="url" title="{data.p_name}">
        <img itemprop="image" src="{data.src}" alt="{data.p_name}" />
      </a>
    </div>
    <div class="tend">
      <h3 itemprop="name"><a href="{data.link}" title="{data.p_name}">{data.p_name} </a></h3>
    </div>
    <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <div class="txt">Giá :</div>
      <div class="red">{data.price_text}</div>
      {data.price_old_text}
      <meta itemprop="price" content="{data.price}"/>
      <meta itemprop="priceCurrency" content="VND"/>
    </div>
    <div class="cart"><a href="{data.link_cart}"><span>{LANG.product.buy_now}</span></a></div>
  </div>
</div>
<!-- END: table_col -->