<!-- BEGIN: modules -->
<div class="ecool-product">
      <div class="container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
           {data.navation}
        </nav>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-12 mb-5">
            <h1 class="font-weight-medium text-center fs-22 fs-lg-31 mb-4">{LANG.product.specool}</h1>
            <p class="fs-13 font-weight-medium products__description">{data.slogan}</p>
          </div>
          <div class="col-12 images-products"><img src="{DIR_IMAGE}/banner-product.png" alt=""></div>
        </div>
        <div class="row products-title">
          <div class="col-12">
            <h2 class="text-center fs-20 fs-lg-26">LỰA CHỌN MÁY ĐIỀU HÒA</h2>
          </div>
          <div class="col-12 text-center"><a class="text-center" href="#">XEM TẤT CẢ MÁY ĐIỀU HÒA ></a></div>
        </div>
        <div class="row products__list">

          
          {data.main}
          
        </div>


        {data.services}
      </div>
      <section class="ecool-app bg-gray">
        {data.ungdung_ecool}
      </section>
      <div class="container cachsudung">
       {data.sp_cachsudung}
      </div>
    </div>
<!-- END: modules -->


<!-- BEGIN: html_list -->
<div class="box_mid">
              
              <div class="mid-content">
                  <div class="mod-content row">
                      <div id="vnt-sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                          <div class="menuTab v2 hidden-lg hidden-md">
                              <div class="mc-menu">Select Menu</div>
                              {data.list_cat_mobile}
                          </div>
                          <div class="menuProduct hidden-sm hidden-xs">
                              {data.box_category1}
                          </div>
                          <div class="filter">
                              <div class="title">Trạng thái</div>
                              <div class="content">
                                  {data.status_filter}
                              </div>
                          </div>
                          <div class="filter">
                              <div class="title">{LANG.product.price_filter}</div>
                              <div class="content">
                                  {data.Price_filter}
                              </div>
                          </div>
                      </div>
                      <div id="vnt-main" class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

<div class="productTools">
    {data.Sort_Filter}
     
    {data.Display_Filter}
</div>
<div class="vnt-product">
    <div class="row">
        {data.row_product}
        
        
        
    </div>
</div>
<!--===BEGIN: PHAN TRANG===-->
<div class="pagination">
    {data.nav}
</div>
<!--===END: PHAN TRANG===-->
</div>
                  </div>
              </div>
          </div>
<!-- END: html_list -->

<!-- BEGIN: html_search -->
<div class="formSearch"> 
  <form name='modSearch' id="modSearch" method='get' action="{data.link_action}" >  
    <table width="90%" border="0" cellspacing="2" cellpadding="2" align="center" >
      <tr>
        <td class="col1" width="20%" nowrap="nowrap"><strong>{LANG.product.keyword}</strong></td>
        <td><input type="text" class="textfiled" name="keyword" id="keyword" value="{INPUT.keyword}" size="30"/></td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap" ><strong>{LANG.product.category}</strong></td>
        <td>{data.list_cat}</td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap">&nbsp;</td>
        <td>
          <button id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.product.btn_search}">
            <span>{LANG.product.btn_search}</span>
          </button>
        </td>
      </tr>
    </table>
  </form>
</div>
<p class="mess_result">{data.note_keyword} <br>{data.note_result}</p>
<div id="List_Product">{data.list_search}<br class="clear"></div> 
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: html_tags -->
<p class="mess_result">{data.note_result}</p>
<div id="List_Product">
    {data.row_product}
    <br class="clear">
</div> 
{data.nav}
<!-- END: html_tags -->
