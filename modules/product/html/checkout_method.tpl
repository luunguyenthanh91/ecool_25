<!-- BEGIN: modules -->
<div id="vnt-content">
    <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/product/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>

<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.nav_shopping}
      <form action="{data.link_action}" method="post" name="f_checkout" id="f_checkout">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            {data.delivery_address}
            {data.shipping_method}
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div id="tableCart">
              {data.info_cart}
            </div>
            {data.payment_method}
          </div>
        </div>
        <div class="divButton">
          <div class="fl"><a class="btnCart" href="{data.link_back}">{LANG.product.back_to_prev}</a></div>
          <div class="fr">
            <input name="do_process" type="hidden" value="1" />
            <button id="btnConfirm" name="btnConfirm" type="submit" class="btnCart blood" value="1">
              <span>{LANG.product.btn_continue}</span>
            </button>
          </div>
          <div class="clear"></div>
        </div>
      </form>
    </div>
  </div>
  <div id="flagEnd"></div>
</div>
</div>
<!-- END: modules -->

<!-- BEGIN: delivery_address -->
<script language="javascript">
  function checkform(f) {
    if (f.d_name.value == '' ) {
      alert("{LANG.product.err_full_name_empty}");
      f.d_name.focus();
      return false;
    }
    var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
    var d_email = f.d_email.value;
    if (d_email == ''){
      alert("{LANG.product.err_email_empty}");
      f.d_email.focus();
      return false;
    }
    if (d_email != '' && d_email.match(re)==null)   {
      alert("{LANG.product.err_email_invalid}");
      f.d_email.focus();
      return false;
    }
    if (f.d_phone.value == '' ) {
      alert("{LANG.product.err_phone_empty}");
      f.d_phone.focus();
      return false;
    }
    if (f.d_address.value == '' ) {
      alert("{LANG.product.err_address_empty}");
      f.d_address.focus();
      return false;
    }
    if (f.d_city.value == 0 || f.d_city.value == '' ) {
      alert("{LANG.product.err_city_empty}");
      f.d_city.focus();
      return false;
    }
    if (f.d_state.value == 0 || f.d_state.value == '' ) {
      alert("{LANG.product.err_state_empty}");
      f.d_state.focus();
      return false;
    }
    //if($("#ck-notSame").attr("checked")){
    if($("#ck-notSame").is( ":checked" )){
      if (f.c_name.value == '') {
        alert("{LANG.product.err_full_name_empty}");
        f.c_name.focus();
        return false;
      }
      if (f.c_phone.value == '') {
        alert("{LANG.product.err_phone_empty}");
        f.c_phone.focus();
        return false;
      }
      if (f.c_address.value == '') {
        alert("{LANG.product.err_address_empty}");
        f.c_address.focus();
        return false;
      }
      if (f.c_city.value == 0 || f.c_city.value == '' ) {
        alert("{LANG.product.err_city_empty}");
        f.c_city.focus();
        return false;
      }
      if (f.c_state.value == 0 || f.c_state.value == '' ) {
        alert("{LANG.product.err_state_empty}");
        f.c_state.focus();
        return false;
      }
    }
    if($("#ck-bill").attr("checked")){
      if (f.bill_company.value == '') {
        alert("{LANG.product.err_company_empty}");
        f.bill_company.focus();
        return false;
      }
      if (f.bill_address.value == '') {
        alert("{LANG.product.err_address_empty}");
        f.bill_address.focus();
        return false;
      }
      if (f.bill_mst.value == '') {
        alert("{LANG.product.err_mst_empty}");
        f.bill_mst.focus();
        return false;
      }
    }
    return true;
  }
  $(function(){
    $("#f_checkout").submit(function(){
      return checkform( $(this)[0] );
    });
  });
</script>
<div class="boxCart">
  <div class="title">{LANG.product.shipping_information}</div>
  <div class="content">
    <div class="cartField">
      <div class="form-group">
        <label for="d_name">{LANG.product.full_name} <span>*</span></label>
        <div class="colRight">
          <input type="text" class="required" id="d_name" name="d_name" value="{data.d_name}"/>
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="d_phone">{LANG.product.phone} <span>*</span></label>
        <div class="colRight">
          <input type="text" class="required" id="d_phone" name="d_phone" value="{data.d_phone}"/>
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="d_email">Email <span>*</span></label>
        <div class="colRight">
          <input type="text" class="required" id="d_email" name="d_email" value="{data.d_email}"/>
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="Email">{LANG.product.city} <span>*</span></label>
        <div class="colRight" id="ext_d_city">
          {data.list_d_city}
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="Email">{LANG.product.state} <span>*</span></label>
        <div class="colRight" id="ext_d_state">
          {data.list_d_state}
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="d_address">{LANG.product.address} <span>*</span></label>
        <div class="colRight">
          <input type="text" class="required" id="d_address" name="d_address" value="{data.d_address}"/>
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <label for="comment">{LANG.product.note}</label>
        <div class="colRight">
          <textarea name="comment" id="comment"></textarea>
        </div>
        <div class="clear"></div>
      </div>
      <div class="form-group">
        <div class="optionCart" id="dcGiaoOption">
          <input id="ck-notSame" name="notSame" type="checkbox"/>
          <span class="fa-truck">{LANG.product.check_not_same}</span>
        </div>
      </div>
      <div id="dcGiao" style="display: none;">
        <div class="form-group">
          <label for="c_name">{LANG.product.full_name} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="c_name" name="c_name" value="{data.c_name}"/>
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="c_phone">{LANG.product.phone} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="c_phone" name="c_phone" value="{data.c_phone}" />
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="Email">{LANG.product.city}<span>*</span></label>
          <div class="colRight" id="ext_c_city">
            {data.list_c_city}
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="Email">{LANG.product.state} <span>*</span></label>
          <div class="colRight" id="ext_c_state">
            {data.list_c_state}
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="c_address">{LANG.product.address} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="c_address" name="c_address" value="{data.c_address}"/>
          </div>
          <div class="clear"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="optionCart" id="dcBillOption">
          <input name="bill" id="ck-bill" type="checkbox">
          <span class="fa-building-o">{LANG.product.exbill}</span>
        </div>
      </div>
      <div id="dcBill" style="display: none;">
        <div class="form-group">
          <label for="bill_company">{LANG.product.company} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="bill_company" name="bill_company" value="{data.bill_company}"/>
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="bill_address">{LANG.product.address} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="bill_address" name="bill_address" value="{data.bill_address}" />
          </div>
          <div class="clear"></div>
        </div>
        <div class="form-group">
          <label for="bill_mst">{LANG.product.mst} <span>*</span></label>
          <div class="colRight">
            <input type="text" class="required" id="bill_mst" name="bill_mst" value="{data.bill_mst}" />
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END: delivery_address -->

<!-- BEGIN: shipping_method -->
<div class="boxCart">
  <div class="title">{LANG.product.shipping_method}</div>
  <div class="content">
    {data.list_shipping_method}
  </div>
</div>
<!-- END: shipping_method -->

<!-- BEGIN: payment_method -->
<div class="boxCart">
  <div class="title">{LANG.product.payment_method}</div>
  <div class="content">
    {data.list_payment_method}
  </div>
</div>
<!-- END: payment_method -->