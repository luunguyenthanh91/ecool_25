<!-- BEGIN: email_show_cart -->
<table width="100%" border="0" cellpadding="2" cellspacing="1" bgcolor="#FFFFFF">
	<tr bgcolor="#CCCCCC">
		<td align="left" style="padding-left:10px;"><b>{LANG.product.product}</b></td>
		<td width="10%" align="center"><b>{LANG.product.price}</b></td>
		<td width="10%" align="center"><b>{LANG.product.quantity}</b></td>
		<td width="10%" align="center"><b>{LANG.product.total}</b></td>
	</tr>
	{data.html_row_cart}
	<tr bgcolor="#EEEEEE">
		<td height="20" colspan="3" align="right"><b>{LANG.product.total_price} :</b> </td>
		<td height="20" align="center"><b>{data.total_price}</b></td>
	</tr>
</table>
<!-- END: email_show_cart -->
