<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (!defined('IN_vnT')) {
    die('Access denied');
}
$nts = new sMain();

class sMain
{
    var $output = "";
    var $skin = "";
    var $linkUrl = "";
    var $module = "product";
    var $action = "ajax";

    function sMain(){
        global $vnT, $input, $cart;
        include("function_" . $this->module . ".php");
        loadSetting();
        include("function_shopping.php");
        $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
        $this->skin->assign('DIR_MOD', DIR_MOD);
        $this->skin->assign('LANG', $vnT->lang);
        $this->skin->assign('INPUT', $input);
        $this->skin->assign('CONF', $vnT->conf);
        $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
        $this->skin->assign('DIR_STYLE', $vnT->dir_style);
        $this->skin->assign('DIR_JS', $vnT->dir_js);
        include(PATH_INCLUDE . "/JSON.php");
        switch ($input['do']) {
            case "popup" : $jsout = $this->do_popup(); break;
            case "shipping_price"  : $jsout = $this->do_shipping_price(); break;
            case "update_quantity"  : $jsout = $this->do_update_quantity(); break;
            case "wishlist"	 : $jsout = $this->do_Wishlist() ; break ;
            default : $jsout = "Error"; break;
        }

        flush();
        echo $jsout;
        exit();
    }
    function do_popup(){
        global $vnT,$DB;
        $textout = '';
        $result= $vnT->DB->query("SELECT * FROM advertise
                                  WHERE pos='main_popup' AND display=1 AND lang='$vnT->lang_name'
                                  ORDER BY l_order LIMIT 0,1");
        if ($row = $vnT->DB->fetch_row($result)){
            $title = $vnT->func->HTML($row['title']);
            $link = (! strstr($row['link'],"http://")) ? $vnT->link_root .$row['link'] : $row['link'];
            $src = ROOT_URL . "vnt_upload/weblink/" . $vnT->func->HTML($row['img']);
            $target = ($row['target']) ? $row['target'] : '_blank';
            $textout .= '<a href="'.$link.'" target="'.$target.'"><img style="max-width: 100%; max-height: 100%; margin: 0 auto; display: block;" src="'.$src.'" alt="'.$title.'"/></a>';
        }
        return $textout;
    }
    function do_shipping_price(){
        global $vnT, $input, $cart;
        $city = (int)$_POST['city'];
        $state = (int)$_POST['state'];
        $payment_method = $_POST['payment_method'];
        $shipping_method = $_POST['shipping_method'];
        //$res =$vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
        //$info = $vnT->DB->fetch_row($res);
        $cart_total = $cart->cart_total($cart->session);
        $s_price = 0;
        $res_s = $vnT->DB->query("SELECT * FROM shipping_method WHERE name='".$shipping_method."' ");
        if($row_s = $vnT->DB->fetch_row($res_s)) {
            if ($row_s['s_type'] == 0){
                $s_price = $row_s['price'];
            } else {
                $info_s['c_city'] = $city;
                $info_s['c_state'] = $state;
                $info_s['cart_total'] = $cart_total;
                $s_price = get_shipping_price($info_s);
            }
        }
        $total_price = $cart_total;
        $total_price += $s_price;
        $arr_json['s_price'] = '+'.get_price_pro($s_price);
        $arr_json['service_charge'] = get_price_pro($service_charge);
        $arr_json['total_price'] = get_price_pro($total_price);
        $json = new Services_JSON();
        $textout = $json->encode($arr_json);
        return $textout;
    }
    function do_update_quantity(){
        global $vnT, $input, $cart;
        $id = (int)$_POST['id'];
        $quantity = (int)$_POST['quantity'];
        $item_cart = get_item_cart($id);
        if($quantity==0){
            $vnT->DB->query( "DELETE FROM order_shopping WHERE session='".$cart->session."' AND id=".$id);
        }else{
            $onhand = check_onhand($item_cart['item_id'], $item_cart['subid']);
            $quantity = ($quantity > $onhand) ? $onhand : $quantity;
            $cart->modify_quantity($cart->session, $id, $quantity);
        }
        $contents = $cart->display_contents($cart->session);
        $x =0 ; $total_price = 0 ; $total_out =0 ;
        while ($x < $cart->num_items($cart->session)){
            $total = $contents['total'][$x];
            if($contents["id"][$x] == $id) {
                $total_out = $total;
            }
            $total_price += $total;
            $x++;
        }
        $arr_json['quantity'] = $quantity;
        $arr_json['total'] = get_price_pro($total_out);
        $arr_json['total_price'] = get_price_pro($cart->cart_total($cart->session));
        $json = new Services_JSON();
        $textout = $json->encode($arr_json);
        return $textout;
    }
    function do_Wishlist (){
        global $vnT, $input;
        $id = (int)$_POST['id'];
        $sub = $_POST['sub'];
        $mem_id = $vnT->user['mem_id'];
        $mess = "";
        if($vnT->user['mem_id']==0) {
            $arr_json['ok'] = "-1";
            $mess = 'Vui lòng đăng nhập thành viên';
        }else{
            //check sp
            $res_pro  = $vnT->DB->query("SELECT p_id FROM products WHERE p_id=".$id);
            if($vnT->DB->num_rows($res_pro)){
                if($sub=="del"){
                    //check
                    $res_ck = $vnT->DB->query("SELECT id FROM product_wishlist WHERE mem_id=".$mem_id." AND itemid=$id ");
                    if($vnT->DB->num_rows($res_ck)){
                        $vnT->DB->query("DELETE FROM product_wishlist WHERE mem_id=".$mem_id." AND itemid=$id ")	;
                        $arr_json['ok'] = 1;
                        $mess = 'Đã xóa tin ra khỏi danh sách ưa thích';
                    }else{
                        $arr_json['ok'] = 0;
                        $mess = 'Tin không tồn tại trong danh sách ưa thích';
                    }
                }else{
                    $res_ck = $vnT->DB->query("SELECT id FROM product_wishlist WHERE mem_id=".$mem_id." AND itemid=$id ");
                    if($vnT->DB->num_rows($res_ck)){
                        $arr_json['ok'] = 0;
                        $mess = 'Tin đã tồn tại trong danh sách ưa thích';
                    }else{
                        $cot['mem_id'] = $mem_id;
                        $cot['itemid'] = $id;
                        $cot['date_add'] = time();
                        $ok  = $vnT->DB->do_insert("product_wishlist",$cot);
                        $arr_json['ok'] = 1;
                        $mess = 'Đã thêm tin vào danh sách ưa thích';
                    }
                }
            }else{
                $arr_json['ok'] = 0;
                $mess = 'Tin không tồn tại ';
            }
        }
        $arr_json['num'] = $num;
        $arr_json['mess'] = $mess;
        $json = new Services_JSON( );
        $textout = $json->encode($arr_json);
        return $textout;
    }
}
?>