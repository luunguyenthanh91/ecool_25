<?php
	// no direct access
	if (!defined('IN_vnT')){
			 die('Hacking attempt!');
	}
	
	$seo_mod_name = $vnT->setting['seo_name'][$vnT->lang_name]['product'] ;	
	$ext_link= ''; 
	if( strstr($vnT->seo_url,".html/")){
		$url_last = $vnT->url_array[count($vnT->url_array)-1] ;	 
		$cmd_arr = @explode(",",$url_last)	;
		foreach ($cmd_arr as $value)
		{
			if (! empty($value))
			{
				$k = trim(substr($value, 0, strpos($value, "-")));
				$v = trim(substr($value, strpos($value, "-") + 1));
				$ext_link .= "|".$k.":".$v;
			}
		}
	}	
 	//product
	if( strstr($_SERVER['REQUEST_URI'],$seo_mod_name.".html" ))
	{	 
		$_GET[$vnT->conf['cmd']] = "mod:product".$ext_link;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:product".$ext_link;
	}
	
	if (in_array($seo_mod_name, $vnT->url_array)) 
	{
		$pos 				= array_search ($seo_mod_name, $vnT->url_array);
		//print_r($vnT->url_array);
		$mod 				= 'product';	
		$act				= $vnT->url_array[$pos+1];
		
		if(strstr($act,".html"))
		{
			$text_catid = $act;
			$catID = strtolower(substr($text_catid, strrpos($text_catid, "-") + 1));
			$catID = str_replace(".html","",$catID);
			$catID = (int) $catID ;
			
			if((int)$catID)
			{ 
				$vnT->setting['cur_friendly_url'] = substr($text_catid, 0, strrpos($text_catid, "-") );
				$_GET[$vnT->conf['cmd']] = "mod:$mod|act:category|catID:$catID".$ext_link ;
				$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:category|catID:$catID".$ext_link;		 
			}
		}
		
		$pID = $vnT->url_array[$pos+1];
		if((int)$pID && !strstr ($pID,".html") )
		{
			$vnT->setting['cur_friendly_url'] = str_replace(".html","",$vnT->url_array[$pos+2]);
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:detail|pID:$pID" ;
			$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:detail|pID:$pID" ; 		 
		}
		
		//status
		if( $act =="status" )
		{			 
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:status|sID:$sID".$ext_link;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:status|sID:$sID".$ext_link;
		} 
		 
		//search
		if( $act =="search" || $act =="search.html" )
		{ 			
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:search";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|do:search";
		}
 		//tags
		if( $act =="tags" || $act =="tags.html" ) {
			$tagID = $vnT->url_array[$pos + 2]; 
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:tags|tagID:$tagID" ;
			$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:tags|tagID:$tagID" ;
		}

		//ajax
		if($vnT->url_array[$pos+1]=="ajax" || $vnT->url_array[$pos+1]=="ajax.html")
		{
			$ajaxName = str_replace(".html","",$vnT->url_array[$pos+2]);
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:ajax|do:".$ajaxName ;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:ajax|do:".$ajaxName ;
		}

		if($vnT->url_array[$pos+1]=="popup" || $vnT->url_array[$pos+1] == "popup.html")
		{
			$popupName = str_replace(".html","",$vnT->url_array[$pos+2]);
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:popup|do:".$popupName ;
			$QUERY_STRING = $vnT->conf['cmd'] . "=mod:$mod|act:popup|do:".$popupName ;
		}

		//cart
		if($vnT->url_array[$pos+1]=="cart" || $vnT->url_array[$pos+1]=="cart.html")
		{
			$extra = $vnT->url_array[$pos+2] ;
			if($extra) $ext_link = "|".$extra;
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:cart".$ext_link;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:cart".$ext_link;
		}

		//checkout_address
		if($vnT->url_array[$pos+1]=="checkout_address" || $vnT->url_array[$pos+1]=="checkout_address.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_address";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_address";
		}

		//checkout_shipping
		if($vnT->url_array[$pos+1]=="checkout_shipping" || $vnT->url_array[$pos+1]=="checkout_shipping.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_shipping";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_shipping";
		}

		//checkout_payment
		if($vnT->url_array[$pos+1]=="checkout_payment" || $vnT->url_array[$pos+1]=="checkout_payment.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_payment";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_payment";
		}
		//checkout_method
		if($vnT->url_array[$pos+1]=="checkout_method" || $vnT->url_array[$pos+1]=="checkout_method.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_method";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_method";
		}
		//checkout_process
		if($vnT->url_array[$pos+1]=="checkout_process" || $vnT->url_array[$pos+1]=="checkout_process.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_process";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_process";
		}

		//checkout_confirmation
		if($vnT->url_array[$pos+1]=="checkout_confirmation" || $vnT->url_array[$pos+1]=="checkout_confirmation.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_confirmation";
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_confirmation";
		}

		//checkout_finished
		if($vnT->url_array[$pos+1]=="checkout_finished" || $vnT->url_array[$pos+1]=="checkout_finished.html")
		{
			$extra = $vnT->url_array[$pos+2] ;
			if($extra) $ext_link = "|".$extra;
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_finished".$ext_link;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_finished".$ext_link;
		}
		//checkout_finished
		if($vnT->url_array[$pos+1]=="checkout_cancel" || $vnT->url_array[$pos+1]=="checkout_cancel.html")
		{
			$_GET[$vnT->conf['cmd']] = "mod:$mod|act:checkout_cancel" ;
			$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:checkout_cancel" ;
		}

	}//end news
 

?>