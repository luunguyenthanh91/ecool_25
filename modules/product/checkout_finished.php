<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
	var $module = "product";
	var $action = "checkout_finished";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		include ("function_shopping.php");
		$this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$vnT->html->addStyleSheet( DIR_MOD."/css/cart.css");
		$vnT->html->addScript(DIR_MOD."/js/cart.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module; 
		$vnT->conf['indextitle'] = $vnT->lang['product']['f_order_finished'];
		$data['main'] = $this->do_Finished();
		$navation = get_navation (0,$vnT->lang['product']['f_order_finished']);
		$data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$this->skin->assign("data", $data);		
    $this->skin->parse("modules");
	  $vnT->output .=  $this->skin->text("modules");
	}
	function do_Finished(){
		global $vnT,$func,$DB,$input,$cart ,$conf;
		$order_code = trim(base64_decode($input['order_code']));
		$result = $DB->query("SELECT * FROM order_sum WHERE order_code='$order_code'");
		if($data = $DB->fetch_row($result)){
			$order_id = $data['order_id'];
			$mess_finished = $vnT->func->load_SiteDoc("checkout_finished");
			$_SESSION['do_finished'] = 0;
			if($_SESSION['do_finished']==0){
				//load payment
				$name_payment = trim($data['payment_method']);
				$vnT->module = fetch_module_payment($name_payment);
				$path_res = PATH_ROOT . "/modules/product/payment/" . $name_payment . "/payredirect.php";
				if(file_exists($path_res)){
					include ($path_res);
					if($vnT->setting['payment']['ok']==1){
						//update trang thai	 đã thanh toán
						$vnT->DB->query( "UPDATE order_sum SET status=3 WHERE order_code='".$order_code."' ");
						// Tính cộng diểm tích lũy cho TV và trừ diểm tích lũy mà TV dùng mua HĐ này nếu có
					}else{
						$mess_finished = $vnT->func->html_err($vnT->setting['payment']['mess']);
					}
				}
				//send email
				$payment_address = get_info_address ($data,"");
				$shipping_address = get_info_address ($data,"shipping");
				$info_shipping = $vnT->func->txt_unHTML($data['shipping_name']);
				$info_shipping .= "<br>".$vnT->lang['product']['s_price']." : <b class='price'>".get_price_pro($data['s_price'])."</b>";
				$info_payment = $vnT->func->HTML($data['payment_name']);
				$email = $data['d_email'];
				$email_toBQT = $vnT->func->load_MailTemp("OrdertoBQT");
				$email_toGuest = $vnT->func->load_MailTemp("OrdertoGuest");
				$qu_find = array(
					'{domain}',
					'{order_code}',
					'{payment_address}',
					'{shipping_address}',
					'{info_payment}',
					'{info_shipping}',
					'{table_cart}',
					'{comment}',
					'{date_order}',
				);
				$qu_replace = array(
					strtoupper($_SERVER['HTTP_HOST']),
					$order_code ,
					$payment_address,
					$shipping_address,
					$info_payment,
					$info_shipping,
					email_ShowCart($data),
					$func->HTML($data['comment']),
					@date("d/m/Y",$data['date_order'])
				);
				$email_OrdertoBQT = str_replace($qu_find, $qu_replace,$email_toBQT);				
				$email_OrdertoGuest = str_replace($qu_find, $qu_replace,$email_toGuest);
				$subject = str_replace("{domain}",$_SERVER['HTTP_HOST'],$vnT->lang['product']['subject_email_order']);
			  //--- gui den BQT 
				$vnT->func->doSendMail($vnT->conf['email'],$subject,$email_OrdertoBQT,$email);
		   	//--- gui den khach hang 
				$vnT->func->doSendMail($email,$subject,$email_OrdertoGuest,$vnT->conf['email']);
				// xoa cu
				$vnT->DB->query( "DELETE FROM order_shopping WHERE session='".$cart->session."' ");
				$vnT->DB->query( "DELETE FROM order_address WHERE session='".$cart->session."' ");
				//del session 
				$_SESSION['do_finished'] = 1;
			}
		}else{
			$link_ref= LINK_MOD.".html";
			$vnT->func->header_redirect($link_ref);
		}
		$data['order_code'] = $order_code;
		//$data['total_price'] = get_price_pro($data['total_price']);
		$data['mess_finished'] = str_replace("{order_code}",$order_code,$mess_finished);
		$data['nav_shopping'] = nav_shopping('checkout_finished');
		$data['link_back'] = $vnT->link_root;
		$this->skin->assign("data", $data);
    $this->skin->parse("checkout_finished");
    return $this->skin->text("checkout_finished");
	}
}
?>