<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')){
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";

  function sMain (){
    global $vnT, $input;
    include ("function_".$this->module.".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->setting['menu_active'] = $this->module;
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];	
		}
		$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
		$link_seo .= $vnT->setting['seo_name'][$vnT->lang_name]['product'].".html";			
		$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />';
		$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
    // $data['main'] = $this->list_product($data);
    $data['main'] = $this->list_category();
		$navation = get_navation($cat_id);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['slogan'] = $vnT->setting['slogan'];
     $data['services'] = $vnT->func->load_Sitedoc('service_product');
     $data['ungdung_ecool'] = $vnT->func->load_Sitedoc('ungdung_ecool');
     $data['sp_cachsudung'] = $vnT->func->load_Sitedoc('sp_cachsudung');
     
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
	function list_category () {
		global $vnT,$func,$DB,$input;
		$textout = $list_category = "";
		$sql  ="SELECT * FROM product_category n,  product_category_desc nd
						WHERE n.cat_id=nd.cat_id AND lang='$vnT->lang_name' AND display=1 AND parentid=0 						
						ORDER BY cat_order ASC, date_post DESC ";
		$result = $vnT->DB->query($sql);
		if ($vnT->DB->num_rows($result)){
			$text="";
			while ($row= $vnT->DB->fetch_row($result)){
				$cat_id = (int)$row['cat_id'];
				$data['link'] = create_link("category",$cat_id,$row['friendly_url']);
				$data['cat_name'] = $vnT->func->HTML($row['cat_name']);
				$data['src'] = MOD_DIR_UPLOAD.'/'.$row['picture'];
				$data['description'] = $row['description'];
				// $this->skin->reset("item_category");
				// $this->skin->assign("data", $data);
				// $this->skin->parse("item_category");
				// $list_category .= $this->skin->text("item_category");
        $text .= '<div class="col-6 col-md-3">
            <div class="item-product bg-gray text-center"><img class="item-product__img" src="'.$data['src'].'" alt="">
              <div class="item-product__content text-center">
                <h5 class="mb-1 mt-0 text-center">'.$data['cat_name'].'</h5>
                <p class="text-center mb-0">'.$row['slogan'].'</p>
              </div><a class="text-center" href="'.$data['link'].'">
                <div class="btn btn--product">'.$vnT->lang['product']['view_now'].'</div></a>
            </div>
          </div>';

			}
			
		}
	   return $text;
	}
  function list_product ($info){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $view = 1; $num_row=3; $where = "";
    $status = $input['status'];
    if($status){
    	$where .= " AND FIND_IN_SET('$status',status)<>0 ";
    	$ext_pag .= "&status=".$status;
    }
    $attr = $input['attr'];
    if($attr){
    	$where .= " AND FIND_IN_SET('$attr',attr_id)<>0 ";
    	$ext_pag .= "&attr=".$attr;
    }
    if($input['price']){
      $tmp_price = @explode("-",$input['price']);
      $min = (int)$tmp_price[0];
      $max = (int)$tmp_price[1];
      if ($tmp_price[0]== 0 ){
        $where .= " AND price < $max ";
      }else if (!$tmp_price[1]){
        $where .= " AND price > $min ";
      }else{
        $where .= " AND ( price BETWEEN $min AND $max )";
      }
      $ext_pag .= "&price=".$input['price'];
    }
    $sql_num = "SELECT p.p_id FROM products p, products_desc pd
								WHERE p.p_id=pd.p_id AND lang='$vnT->lang_name' AND display=1 $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
    if($input['display']){
      $n = ($input['display']);
      $ext_pag .= "&display=".$input['display'];
    }else{
      $n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 9;
    }
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){
			$root_link = LINK_MOD.".html";
			$nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
    switch ($input['sort']) {
      case 'new':
        $where .= " ORDER BY date_post DESC ";
        break;
      case 'old':
        $where .= " ORDER BY date_post ASC ";
        break;
      case 'low':
        $where .= " ORDER BY price ASC ";
        break;
      case 'hight':
        $where .= " ORDER BY price DESC ";
        break;
      default:
        $where .= " ORDER BY p_order DESC, date_post DESC ";
        break;
    }
    $data['row_product'] = row_product($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    $data['box_filter'] = box_filter();
    $data['Sort_Filter'] = Sort_Filter($input['sort']);
    $data['Display_Filter'] = Display_Filter($input['page']);
    $data['list_cat_mobile'] = List_category();
    $data['Price_filter'] = Price_filter();
    $data['status_filter'] = status_filter();
    $data['box_category1'] = box_category1($info['cat_id']);
     
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		$nd['content'] = $this->skin->text("html_list");
 		$nd['f_title'] = '<h1>'.$vnT->lang['product']['product'].'</h1>';
    $nd['more'] = $vnT->setting['slogan'];
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
		return $textout;
  }
}
?>