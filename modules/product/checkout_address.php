<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright � 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
	var $module = "product";
	var $action = "checkout_address";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		include ("function_shopping.php");
		$this->linkMod = $vnT->cmd . "=mod:".$this->module;
		$this->skin = new XiTemplate( DIR_MODULE."/".$this->module."/html/".$this->action.".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		
		$vnT->html->addScript(DIR_MOD."/js/cart.js");
		$vnT->html->addStyleSheet( DIR_MOD."/css/cart.css");
		$vnT->html->addScript(ROOT_URL . "modules/member/js/oAuthConnect.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module; 
		$vnT->conf['indextitle'] = $vnT->lang['product']['f_info_order'];
		//check empty
		if ($cart->num_items($cart->session)==0){
			$mess = $vnT->lang['product']['empty_cart'];
			$url = $this->linkMod;
			$vnT->func->html_redirect($url,$mess);
		}
		$res = $vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
		$info = $vnT->DB->fetch_row($res);
		if($vnT->user['mem_id'] > 0){
			if(empty($info['address_id'])){
				$cot['session'] = $cart->session;
				$cot['mem_id'] = $vnT->user['mem_id'];
				$cot['username'] = $vnT->user['username'];
				$cot['d_name'] = $vnT->user['full_name'];
				$cot['d_address'] = $vnT->user['address'];
				$cot['d_email'] = $vnT->user['email'];
				$cot['d_city'] = $vnT->user['city'];
				$cot['d_phone'] = $vnT->user['phone'];
				
				$cot['c_name'] = $vnT->user['full_name'];
				$cot['c_address'] =  $vnT->user['address'];
				$cot['c_city'] = $vnT->user['city'];
				$cot['c_phone'] =  $vnT->user['phone'];
				$cot['date_post'] = time();
				$ok = $vnT->DB->do_insert("order_address",$cot);					
			}
			$link_next= create_link_shopping("checkout_method");    
			$vnT->func->header_redirect($link_next);   
		}
		$err = "";
		if (isset($_POST['btnConfirm'])){
			$cot['d_name'] = $input['d_name'];
			$cot['d_company'] = $input['d_company'];
			$cot['d_address'] = $input['d_address'];
			$cot['d_email'] = $input['d_email'];
			$cot['d_city'] = $input['d_city'];
			$cot['d_phone'] = $input['d_phone'];
			
			$cot['c_name'] = $input['d_name'] ;
			$cot['c_address'] = $input['d_address'];
			$cot['c_city'] = $input['d_city'];
			$cot['c_phone'] =  $input['d_phone'];
				
			$cot['mem_id'] = $mem_id;
			$cot['username'] = $username;
			$cot['date_post'] = time();
			
			$res_ck = $DB->query("SELECT address_id FROM order_address WHERE session='".$cart->session."' ");
			if ($row_ck = $DB->fetch_row($res_ck)){
				$ok = $vnT->DB->do_update("order_address", $cot, "session='".$cart->session."' ");
			} else { 
				$cot['session'] = $cart->session;
				$ok = $vnT->DB->do_insert("order_address", $cot);
			}
			if ($ok){
				$link_ref = create_link_shopping("checkout_method");   
				$vnT->func->header_redirect($link_ref);  
			}else{
				$err = $func->html_err($DB->debug());
			}
		}
		$data['nav_shopping'] = nav_shopping('checkout_address');		
		$data['form_login'] = $this->form_login();
		$data['customer_address'] = $this->customer_address();
		$data['link_action'] = create_link_shopping("checkout_address");
		$navation = get_navation(0,$vnT->lang['product']['f_info_order']);
		$data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$data['err'] = $err;
		$this->skin->assign("data", $data);		
    $this->skin->parse("modules");
		$vnT->output .=  $this->skin->text("modules");
	}
	function form_login(){
		global $vnT,$input,$cart,$conf,$DB,$func;
		$data=$input;
		$ref = $vnT->func->base64url_encode($vnT->seo_url);
		$url = $vnT->func->base64url_encode($vnT->seo_url); 		
		$data['link_register'] = $vnT->link_root."member/register.html/?ref=".$ref;
		$data['link_action'] = $vnT->link_root."member/login.html/?ref=".$ref;
		$data['link_lostpass'] = $vnT->link_root."member/forget_pass.html/?ref=".$ref;
		$data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
		$data['google_appId'] = $vnT->setting['social_network_setting']['google_apikey'];
		$data['ref'] = $ref;
		$this->skin->assign("data", $data);
		$this->skin->parse("html_from_login");
		return $this->skin->text("html_from_login");	 
	}
	function customer_address(){
		global $vnT,$input,$cart,$conf,$DB,$func;
		$data=$input;
		$res_ck = $vnT->DB->query("SELECT * FROM order_address WHERE session='".$cart->session."' ");
		if($row_ck = $vnT->DB->fetch_row($res_ck)){
			$data = $row_ck;		
		}else{			
			$data['d_country'] = "VN";
			$data['d_city'] = "02"; 			
		}
		$data['list_city'] = $vnT->lib->List_City("VN", $vnT->user['city'], $vnT->lang['product']['select_choice']," ", "d_city");
		$data['link_action'] = create_link_shopping("checkout_address");   
		$data['link_back'] = create_link_shopping("cart");
		$data['link_checkout'] = create_link_shopping("checkout_method");
		$data['note_new_customer'] = $vnT->func->load_Sitedoc('note_new_customer');
		$this->skin->assign("data", $data);
		$this->skin->parse("customer_address");
		return $this->skin->text("customer_address");
	}
}
?>