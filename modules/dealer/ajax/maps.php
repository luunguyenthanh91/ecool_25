<?php
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	die('Hacking attempt!');
}
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);

require_once("../../../_config.php");
require_once ($conf['rootpath'] . 'includes' . DS . 'defines.php');
require_once($conf['rootpath']."includes/class_db.php");
$vnT->DB= $DB = new DB;
require_once ($conf['rootpath']."includes/class_functions.php");
$vnT->func = $func = new Func_Global();
$vnT->conf = $conf = $func->fetchDbConfig($conf);
require "../../../includes/JSON.php";

$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
$func->load_language('dealer');

$result = $DB->query("SELECT * FROM dealer_setting WHERE lang='$vnT->lang_name' ");
$setting = $DB->fetch_row($result);
foreach ($setting as $k => $v) {
	$vnT->setting[$k] = stripslashes($v);
}
$jsout = do_Maps() ;
flush();
echo $jsout;
exit();
//do_Maps
function do_Maps() {
	global $DB,$func,$conf,$vnT;
	$textout="";
	$cid =  $_POST['cid'];
	$keyword=  $_POST['keyword'];
	$cat_id =  $_POST['cate'];
	$city =  $_POST['city'];
	$state =  $_POST['state'];
	$utilities =  $_POST['utilities'];
	$sort = (int) $_POST['sort'];
	$page = 1 ;
	$pagesize = ((int) $_POST['pagesize']) ? $_POST['pagesize'] : 500 ;
	$total = 0;
	$item = array();
	$where ="";
	if($cat_id) {
		$where .=  ' AND cat_id ='.$cat_id;
	}
	if($city) {
		$where .= ' AND city ='.$city ;
	}
	if($state) {
		$where .= " AND state=".$state ;
	}
	if($utilities) {
		$arr_utilities = @explode(",",$utilities);
		$str_w = ''; $k=0;
		foreach($arr_utilities as $uid) {
			$k++;
			if($k>1){
				$str_w .= ' OR ' ;
			}
			$str_w .= "  FIND_IN_SET('".$uid."',utilities)<>0 " ;
		}
		$where .=  ' AND ('.$str_w.') ';
	}
	if($keyword){
		$keyword = str_replace("+"," ",$keyword);
		$key =  $vnT->func->get_keyword($keyword) ;
		$where .= " and (title  like '%" . $key . "%'  OR address like '%" . $key . "%' ) ";
	}
	$where .= " ORDER BY  " ;
	switch ($sort){
		default: $where .= " d_order ASC "; break;
	}
	$where .= " , date_post DESC ";
	$sql = "SELECT * FROM dealer n, dealer_desc nd
					WHERE n.did=nd.did AND display=1 AND lang='$vnT->lang_name' {$where}";
	$result = $vnT->DB->query($sql);
	if($total = $vnT->DB->num_rows($result)){
		$i=0; $w = 180 ;
		while($row = $vnT->DB->fetch_row($result)){
			$i++;
			$link = $row['did']."/". $row['friendly_url'].".html";
			$src = ($row['picture']) ?  $vnT->func->get_src_modules("dealer/".$row['picture'],$w,0,1,"1.5:1") : $vnT->conf['rooturl'] . "modules/dealer/images/nophoto.gif";
			$is_current = ($cid == $row['did']) ? 1 : 0;
			$item[] = array(
				"id" =>  $row['did'],
				"phone" => $row['phone'],
				"email" => $row['email'],
				"cat_id" =>  $row['cat_id'],
				"address_title" => $vnT->lang['dealer']['address'],
				"open" =>  $row['work_time'],
				"title" => $vnT->func->HTML($row['title']),
				"address" => $vnT->func->HTML($row['address']),
				"picture" => $src ,
				"date" => @date("d/m/Y",$row['date_post']),
				"detailLink"=> $link,
				"lat" => $row['map_lat'],
				"lon" => $row['map_lng'],
				"is_current" => $is_current
			);
		}
	}
	$arr_json['data'] = $item ;
	$arr_json['total'] = $total;
	$arr_json['page'] = $page;
	$arr_json['pagesize'] = $pagesize;
	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);
	return $textout;
}

$vnT->DB->close();
?>