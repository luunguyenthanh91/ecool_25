<?php
/*================================================================================*\
|| 							Name code : dealer.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{ 
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "dealer";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
		loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE."/".$this->module."/html/".$this->module.".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD."/css/".$this->module.".css");
    $vnT->html->addStyleSheet(ROOT_URL."modules/contact/css/contact.css");
    $vnT->html->addStyleSheet(DIR_MOD."/css/maps.css");
    $vnT->html->addScript(DIR_MOD."/js/".$this->module.".js");
    $vnT->html->addScript($vnT->dir_js . "/jquery_plugins/jquery.validate.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];	
		}
	  if((int)$input['itemID']) {
      $vnT->output = $this->do_Detail($input['itemID']);
    }else{
      $data['main'] = $this->do_List();
      $data['f_title'] = $vnT->lang['dealer']['dealer'];
      $navation = get_navation($input['catID']);
      $data['navation'] = $vnT->lib->box_navation($navation);
      $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
      $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
      $data['first_video'] = $this->first_video();
      $data['box_category'] = $this->box_category();
      
      $this->skin->assign("data", $data);
      $this->skin->parse("modules");
      $vnT->output .= $this->skin->text("modules");
    }
  }

  function box_category(){
    global $func, $DB, $conf, $vnT, $input;
    $text = '';
    $sql = "SELECT * FROM news_category n, news_category_desc nd
            WHERE n.cat_id = nd.cat_id AND lang = '$vnT->lang_name' AND parentid = 0 AND display = 1
            ORDER BY cat_order ASC, date_post DESC";
    $result = $DB->query($sql);
    if ($num = $DB->num_rows($result)) {
      $i = 0;
      $text = '';
      $active_main = (!$input['catID']) ? 'class="current"' : '';
      while ($row = $DB->fetch_row($result)) {
        $i ++;
        $active = ($row['cat_id']==$input['catID']) ?  "active" : "" ;
        $link = create_link("category", $row['cat_id'], $row['friendly_url']);
        $cat_name = $vnT->func->HTML($row['cat_name']);

        $text .= '<div class="col-6 col-lg-3 p-0"><a href="'.$link.'">
                  <p class="fs-16 font-weight-medium menu-sub-news-title '.$active.'">'.$cat_name.'</p></a></div>';
      }
    }
    return $text;
  }
  function first_video (){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM dealer n, dealer_desc nd    
          WHERE n.did = nd.did AND display=1 AND lang='$vnT->lang_name' $where 
          ORDER BY d_order ASC, date_post DESC  LIMIT 0,1";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)) {
    while ($row = $DB->fetch_row($result)) {
      $pic = get_picture($row['picture'], $w);
    $link = create_link("detail", $row['did'], $row['friendly_url']);    
    $text .= '<div class="row video-header">
                <div class="col-lg-8 col-12"><a href="'. $link .'">'.$pic.'</a></div>
                <div class="col-lg-4 col-12">
                  <p class="video-header-title fs-25"><a href="'. $link .'">'.$row['title'].'</a></p>
                  <p class="fs-10">'.$row['short'].'</p>
                </div>
              </div>';
    }
  }
  return $text;
}

  function do_List () {
    global $input, $conf, $vnT, $DB, $func;
    $text = "";
    $where = "";
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $n = (! empty($vnT->setting['n_list'])) ? $vnT->setting['n_list'] : 10;
    $num_row = 2;

    $view = 1;
    $sql_num = "SELECT n.did FROM dealer n, dealer_desc nd
                WHERE n.did = nd.did AND lang='$vnT->lang_name' AND display=1 $where ";


    $res_num = $DB->query($sql_num);
    $totals = $DB->num_rows($res_num);
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    if ($num_pages > 1) {
      $root_link = LINK_MOD.".html";
      $nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>";
    }

    $data['list_dealer'] = row_dealer($where, $start, $n ,$num_row,$view);
    $data['nav'] = $nav;

    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		return $this->skin->text("html_list");
  }

  function do_Detail ($did){
    global $input, $conf, $vnT, $DB, $func;
    $text = "";
    $vnT->DB->query("UPDATE dealer SET views=views+1 WHERE did=".$did);
    $sql = "SELECT * FROM dealer  n ,dealer_desc nd
						WHERE n.did=nd.did AND display=1 AND lang='$vnT->lang_name' AND n.did=$did";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)){
			if ($data['metadesc'])	$vnT->conf['meta_description'] = $data['metadesc'];
			if ($data['metakey'])	$vnT->conf['meta_keyword'] = $data['metakey'];
			if ($data['friendly_title']) $vnT->conf['extra_title'] =  $data['friendly_title']." - ";
			$input['catID']=$data['cat_id'];

      if(isset($data['link360'])){
          parse_str( parse_url( $data['link360'], PHP_URL_QUERY ), $my_array_of_vars );
          $code_v = $my_array_of_vars['v']; 
          $data['iframe_youtube'] = '<iframe width="100%" height="400"
          src="http://www.youtube.com/embed/'.$code_v.'?autoplay=1">
          </iframe>';
      }
      
    } else {
      $mess = $vnT->lang['dealer']['mess_no_dealer'];
      $url = LINK_MOD.".html";
      $vnT->func->html_redirect($url, $mess);
    }
    $this->skin->assign("data", $data);
    $this->skin->parse("html_detail");
    return $this->skin->text("html_detail");
  }
  function other_dealer($did){
    global $vnT, $DB;
    $text = "";
    $n = ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;
    $where .= " AND n.did<>$did";
    $sql = "SELECT * FROM dealer n, dealer_desc nd   
            WHERE n.did=nd.did AND display=1 AND lang='$vnT->lang_name' $where 
            ORDER BY d_order DESC , date_post DESC LIMIT 0,$n";
    $result = $vnT->DB->query($sql);
    if ($num = $DB->num_rows($result)) {
      $w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 370;
      $text .= '<div class="orther_dealer"><div class="od_title">'.$vnT->lang['dealer']['other_dealer'].'</div>
                <div class="od_content"><div class="slide_dealer">';
      while ($row = $DB->fetch_row($result)) {
        $data['link'] = create_link("detail", $row['did'], $row['friendly_url']);
        $data['title'] = $vnT->func->HTML($row['title']);
        $picture = ($row['picture']) ? "dealer/".$row['picture'] : "dealer/".$vnT->setting['pic_nophoto'];
        $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
        $info = '';
        if($row['address'])
          $info .= '<div class="before fa-home">'.$vnT->func->HTML($row['address']).'</div>';
        if($row['phone'])
          $info .= '<div class="before fa-phone">'.$row['phone'].'</div>';
        if($row['email'])
          $info .= '<div class="before fa-envelope-o">'.$row['email'].'</div>';
        $data['info'] = $info;
        $this->skin->reset("item_other");
        $this->skin->assign("data", $data);
        $this->skin->parse("item_other");
        $text .= $this->skin->text("item_other");
      }
      $text .= '</div></div></div>';
    }
    return $text;
  }
}
?>