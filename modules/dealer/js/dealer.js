$(document).ready(function(){
  $(".showroom a.map").fancybox({
  	width:"100%",
		maxWidth:"100%",
		height:"100%",
		maxHeight:"100%",
		autoSize: false,
		fitToView : false,
		padding:0,
		margin:0,
		type:'iframe',
		wrapCSS     : 'myDesignPopup',
  });
  $(".showroom .gallery a").fancybox({
  	padding:0,
  });
  $(".iframe360").css({'height': $(window).innerHeight()});
  $(window).resize(function(){
  	setTimeout(function(e){
			$(".iframe360").css({'height': $(window).innerHeight()});
  	},100);
  });
});