<!-- BEGIN: modules -->
    <section class="slider" id="home-slider">
      {data.banner}
    </section>
    <section class="slider" id="home-slider-mobile">
      {data.banner_mobile}
    </section>
    <!-- <link rel="stylesheet" type="text/css" href="{DIR_STYLE}/pages/index.css"> -->
    <section class="portfolio">
      <div class="container">
        <h2 class="section__title text-uppercase text-center font-weight-medium wow fadeInUpBig" data-wow-delay="1s">
          {LANG.global.mangthiennhien}</h2>

        <div class="function_1">
          <div class="row banner-child">
            {data.banner_child_home}
          </div>
        </div>

        <div class="row mb-banner-child">
          <div id="mb-banner-child">
            {data.banner_child_home}
          </div>
        </div>
      </div>
    </section>
    <section class="product-tab fadeInUp">
      {data.box_cat_pro}

    </section>
    <section class="video wow fadeInUp" data-wow-delay="0.5s" style="background-image:url({DIR_IMAGE}/bgvideo.png)">
      <div id="home-video-slider">

        {data.videos}


      </div>
    </section>


    {data.news_focus}


    <section class="product">
      <div class="container">

        <img class="w-100 img-banner-product" src="{data.banner_footer}" alt="">

        <div class="row">

          {data.ps_focus}
        </div>
      </div>
    </section>
<!-- END: modules -->



<!-- BEGIN: item_slide -->
<div class="item">
    <div class="img"><img src="{data.src}" alt="{data.title}" /></div>
    <!-- <div class="caption hidden-sm hidden-xs">
        <div class="t1">{data.title}</div>
        <div class="link"><a href="{data.link}"><span>Xem thêm</span></a></div>
    </div> -->
</div>
<!-- END: item_slide -->

<!-- BEGIN: html_collection -->
<div class="collecttionHome">
  <div class="wrapper">
    <div class="box_mid">
      <div class="mid-title">
        <div class="titleL"><h2>{LANG.main.collection}</h2></div>
        <div class="titleR">{LANG.main.collection_slogan}</div>
        <div class="clear"></div>
      </div>
      <div class="mid-content">
        <div class="gridS">
          <div class="col1">
            {data.slide_collection}
          </div>
          <div class="col2">
            {data.category}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END: html_collection -->

<!-- BEGIN: item_collection_category -->
<div class="itemCollect">
  <div class="img"><a href="{data.link}" title="{data.cat_name}">
    <img src="{data.src}" alt="{data.cat_name}" /></a>
  </div>
  <div class="caption">
    <div class="t1">{data.cat_name}</div>
    <div class="t2">{LANG.main.view_detail}</div>
  </div>
</div>
<!-- END: item_collection_category -->

<!-- BEGIN: html_project -->
<div class="productHome">
  <div class="boxHome">
      <div class="title">
          <h2><span>{LANG.main.project}</span></h2>
          <div class="des">{LANG.main.project_slogan}</div>
      </div>
      <div class="content">
          <div id="slideProduct" class="slick-init">
              {data.list_project}
          </div>
      </div>
  </div>
</div>
<!-- END: html_project -->

<!-- BEGIN: item_project -->
<div class="item">
    <div class="img"><a href="{data.link}"><img src="{data.src}" alt="{data.title}" /></a></div>
    <div class="tend"><h3>{data.title}</h3></div>
</div>
<!-- END: item_project -->

<!-- BEGIN: html_product_focus -->
<div class="vnt-product lazyloading">
  <div class="wrapper">
    <div class="title">{LANG.main.product}</div>
    <div class="slide_product">

        {data.list_product}

    </div>
  </div>
</div>
<!-- END: html_product_focus -->

<!-- BEGIN: item_product -->
  <div class="item">
    <div class="wrap_product" itemscope itemtype="http://schema.org/Product">
      <div class="img">
       <a itemprop="url" href="{data.link}" title="{data.p_name}">{data.pic}</a>
      </div>
      <div class="i-desc">
        <div class="i-title">
          <h3 itemprop="name"><a itemprop="url" href="dau-goi-saroma-tri-gau-450ml.html" title="Dầu Gội Saroma Trị Gàu 450ml">Dầu Gội Saroma Trị Gàu 450ml</a></h3>
        </div>
        <div class="i-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
          <span class="promotion">
            {data.price_text}
          </span>

        </div>
      </div>
    </div>
  </div>
<!-- END: item_product -->

<!-- BEGIN: html_news_focus -->


  <section class="news">
      <div class="container">
        <h2 class="font-weight-semi-bold">{LANG.global.news}</h2>
        <div class="slide-news position-relative">
          <div class="slide-news__content">
            {data.list_news}

          </div>
          <div class="arrow"><span class="arrow__left"><img src="{DIR_IMAGE}/arrowleft.png"></span><span class="arrow__right"><img src="{DIR_IMAGE}/arrowright.png"></span></div>
        </div>
        <div class="text-center news__viewall"><a class="fs-11 font-weight-semi-bold" href="">{LANG.global.xemtttt}<img src="{DIR_IMAGE}/arrowdown.png"></a></div>
      </div>
    </section>
<!-- END: html_news_focus -->

<!-- BEGIN: item_news -->

<div class="row items-slide news-slide">
    <div class="col-md-6">
      <div class="news__banner"><img src="{data.src}" alt="{data.title}">

      </div>
    </div>
    <div class="col-md-6">
      <div class="news__text">
        <h3 class="font-weight-semi-bold"><a itemprop="url" class="fs-15 font-weight-semi-bold" href="{data.link}">{data.title}</a></h3>
        {data.short}
      </div>
    </div>
  </div>

<!-- END: item_news -->

<!-- BEGIN: html_feeling -->
<div class="opinionHome">
  <div class="wrapping">
    <div class="wrapper">
      <div id="slideOpinion" class="slick-init">
        {data.list_feeling}
      </div>
    </div>
  </div>
</div>
<!-- END: html_feeling -->

<!-- BEGIN: item_feeling -->
<div class="item">
  <div class="img"><img src="{data.src}" alt="{data.title}" /></div>
  <div class="name">{data.title}</div>
  {data.description}
</div>
<!-- END: item_feeling -->
<!-- BEGIN: html_popup -->
<script>
  $(document).ready(function(){
    $.fancybox.open([{
      href : '{data.link_popup}',
      type: "ajax"
    }], {
      margin      : [95,20,20,20],
      padding     : 0,
      maxWidth    : '90%',
      maxHeight   : '90%',
      fitToView : false,
      autoSize  : true,
      autoHeight  : true,
      autoWidth   : true,
      closeClick  : false,
      wrapCSS     : 'csspopup_deals'
    });
  });
</script>
<!-- END: html_popup -->
