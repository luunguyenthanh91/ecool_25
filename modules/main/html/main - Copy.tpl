<!-- BEGIN: modules -->
<div id="vnt-content">
<div class="wrapCont">
  <div class="vnt-about lazyloading">
    <div class="wrapper">
      {data.intro_home}
    </div>
  </div>
  {data.product_focus}


  {data.box_cat_pro}
  
  
 
<div class="vnt-feeling lazyloading">
  <div class="wrapper">
    <div class="title">Cảm nhận của khách hàng</div>
    <div class="vf-content">
      <div class="slider_feeling">
        <div id="feeling_img">
          <div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling5.jpg" alt="Nguyễn Thanh Hoa"></div></div>
                      </div></div><div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling4.jpg" alt="Lê Thanh Trâm"></div></div>
                      </div></div><div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling3.jpg" alt="Trần Bảo Ngọc"></div></div>
                      </div></div><div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling2.jpg" alt="Lê Nguyễn Trúc Linh"></div></div>
                      </div></div><div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling1.jpg" alt="Trần Thị Ái Nga"></div></div>
                      </div></div><div class="item"><div class="i-bg">
                        <div class="i-img"><div><img src="http://mcwaybeauty.vn/vnt_upload/weblink/feeling2.jpg" alt="Bùi Thị Thu Hải"></div></div>
                      </div></div>
        </div>
        <div id="feeling_text">
          <div class="item">
                        <div class="i-name"><strong>Nguyễn Thanh Hoa</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div><div class="item">
                        <div class="i-name"><strong>Lê Thanh Trâm</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div><div class="item">
                        <div class="i-name"><strong>Trần Bảo Ngọc</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div><div class="item">
                        <div class="i-name"><strong>Lê Nguyễn Trúc Linh</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div><div class="item">
                        <div class="i-name"><strong>Trần Thị Ái Nga</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div><div class="item">
                        <div class="i-name"><strong>Bùi Thị Thu Hải</strong></div>
                        <div class="i-content">Berry SkinCare đã vượt quá sự mong đợi của tôi. Sản phẩm chất lượng tốt, đội ngũ nhân viên kỹ thuật giỏi, chuyên nghiệp, phong cách phục vụ tận tình, ân cần và chu đáo. Tôi rất hài lòng về sản phẩm và thái độ phục vụ của Berry SkinCare.</div>
                      </div>          
        </div>
      </div>
    </div>
  </div>
</div>


<div class="vnt-news w_effect_zoom">
                    <div class="wrapper">
                        <div class="title"><a href="/">TIN TỨC MỚI NHẤT</a></div>
                        <div class="slide_news">
                            {data.box_new_news}
                        </div>
                    </div>
                </div>
</div>
<!-- END: modules -->



<!-- BEGIN: item_slide -->
<div class="item">
    <div class="img"><img src="{data.src}" alt="{data.title}" /></div>
    <!-- <div class="caption hidden-sm hidden-xs">
        <div class="t1">{data.title}</div>
        <div class="link"><a href="{data.link}"><span>Xem thêm</span></a></div>
    </div> -->
</div>
<!-- END: item_slide -->

<!-- BEGIN: html_collection -->
<div class="collecttionHome">
  <div class="wrapper">
    <div class="box_mid">
      <div class="mid-title">
        <div class="titleL"><h2>{LANG.main.collection}</h2></div>
        <div class="titleR">{LANG.main.collection_slogan}</div>
        <div class="clear"></div>
      </div>
      <div class="mid-content">
        <div class="gridS">
          <div class="col1">
            {data.slide_collection}
          </div>
          <div class="col2">
            {data.category}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END: html_collection -->

<!-- BEGIN: item_collection_category -->
<div class="itemCollect">
  <div class="img"><a href="{data.link}" title="{data.cat_name}">
    <img src="{data.src}" alt="{data.cat_name}" /></a>
  </div>
  <div class="caption">
    <div class="t1">{data.cat_name}</div>
    <div class="t2">{LANG.main.view_detail}</div>
  </div>
</div>
<!-- END: item_collection_category -->

<!-- BEGIN: html_project -->
<div class="productHome">
  <div class="boxHome">
      <div class="title">
          <h2><span>{LANG.main.project}</span></h2>
          <div class="des">{LANG.main.project_slogan}</div>
      </div>
      <div class="content">
          <div id="slideProduct" class="slick-init">
              {data.list_project}
          </div>
      </div>
  </div>
</div>
<!-- END: html_project -->

<!-- BEGIN: item_project -->
<div class="item">
    <div class="img"><a href="{data.link}"><img src="{data.src}" alt="{data.title}" /></a></div>
    <div class="tend"><h3>{data.title}</h3></div>
</div>
<!-- END: item_project -->

<!-- BEGIN: html_product_focus -->
<div class="vnt-product lazyloading">
  <div class="wrapper">
    <div class="title">{LANG.main.product}</div>
    <div class="slide_product">
      
        

        {data.list_product}

    </div>
  </div>
</div>
<!-- END: html_product_focus -->

<!-- BEGIN: item_product -->
  <div class="item">
    <div class="wrap_product" itemscope itemtype="http://schema.org/Product">
      <div class="img">
       <a itemprop="url" href="{data.link}" title="{data.p_name}">{data.pic}</a>
      </div>
      <div class="i-desc">
        <div class="i-title">
          <h3 itemprop="name"><a itemprop="url" href="dau-goi-saroma-tri-gau-450ml.html" title="Dầu Gội Saroma Trị Gàu 450ml">Dầu Gội Saroma Trị Gàu 450ml</a></h3>
        </div>
        <div class="i-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
          <span class="promotion">
            {data.price_text}
          </span>
          
        </div>
      </div>
    </div>
  </div>
<!-- END: item_product -->

<!-- BEGIN: html_news_focus -->
<div class="newsHome">
      <div class="wrapper">
          <div class="boxHome">
              <div class="title">
                  <h2><span>{LANG.main.news}</span></h2>
                  <div class="des">{LANG.main.news_slogan}</div>
              </div>
              <div class="content">
                  <div id="slideNews" class="slick-init">
                      {data.list_news}
                  </div>
              </div>
          </div>
      </div>
  </div>
<!-- END: html_news_focus -->

<!-- BEGIN: item_news -->
<div class="item">
    <div class="news" itemscope itemtype="http://schema.org/Article">
        <div class="img" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
          <a href="{data.link}"><img itemprop="url" src="{data.src}" alt="{data.title}" /><meta itemprop="width" content="800"><meta itemprop="height" content="800"></a></div>
        <div class="wrap">
            <div class="caption">
                <div class="date">{data.date_post}</div>
                <div class="tend"><h3 itemprop="headline"><a itemprop="url" href="{data.link}">{data.title}</a></h3></div>
                <div class="des" itemprop="description">{data.short}</div>
            </div>
        </div>
        <meta itemprop="datePublished" content="data.date_post_meta}" />
        <meta itemprop="author" content="admin" />
        <meta itemprop="dateModified" content="{data.date_update_meta}" />
        <meta itemprop="mainEntityOfPage" content="123" />
        <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <meta itemprop="name" content="admin" />
            <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                <meta itemprop="url" content="http://www.mycorp.com/logo.jpg">
            </div>
        </div>
    </div>
</div>
<!-- END: item_news -->

<!-- BEGIN: html_feeling -->
<div class="opinionHome">
  <div class="wrapping">
    <div class="wrapper">
      <div id="slideOpinion" class="slick-init">
        {data.list_feeling}
      </div>
    </div>
  </div>
</div>
<!-- END: html_feeling -->

<!-- BEGIN: item_feeling -->
<div class="item">
  <div class="img"><img src="{data.src}" alt="{data.title}" /></div>
  <div class="name">{data.title}</div>
  {data.description}
</div>
<!-- END: item_feeling -->
<!-- BEGIN: html_popup -->
<script>
  $(document).ready(function(){
    $.fancybox.open([{
      href : '{data.link_popup}',
      type: "ajax"
    }], {
      margin      : [95,20,20,20],
      padding     : 0,
      maxWidth    : '90%',
      maxHeight   : '90%',
      fitToView : false,
      autoSize  : true,
      autoHeight  : true,
      autoWidth   : true,
      closeClick  : false,
      wrapCSS     : 'csspopup_deals'
    });
  });
</script>
<!-- END: html_popup -->
