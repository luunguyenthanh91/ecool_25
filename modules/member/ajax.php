<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
	var $action = "ajax";

  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input,$cart;
    include ("function_" . $this->module . ".php");
		loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images); 
		$this->skin->assign('DIR_STYLE', $vnT->dir_style); 
		$this->skin->assign('DIR_JS', $vnT->dir_js);
		include (PATH_INCLUDE."/JSON.php");
		switch ($input['do']){
			case "popup_login"	 : $jsout = $this->do_popupLogin() ; break ;
			case "ajax_logout" : $jsout = $this->do_ajax_logout() ; break;	
			case "ajax_register" : $jsout = $this->do_ajax_register() ; break;
			case "oAuthConnect" : $jsout = $this->do_oAuthConnect() ; break;
			case "edit_avatar" : $jsout = $this->do_EditAvatar() ;break;
			case "edit_cover" : $jsout = $this->do_EditCover() ;break;
			default : $jsout = "Error" ; break ;
		}
		flush();
		echo $jsout;
		exit(); 
  }
	function do_popupLogin() {
		global $DB,$func,$conf,$vnT;
		$arr_json = array(); 
		//$email = $_POST['email'] ;
		$user = str_replace("'", "",trim($_POST['user'])) ;
		$pass = $_POST['pass'] ;
		$save = $_POST['save'] ;
		$ok = 1;
		$mess = "";
		 
		// Check
		$password = $func->md10($pass);
		$ch_remember = ($save) ? 1 : "0";
		//$check_qr = $DB->query("SELECT * FROM members WHERE username='{$user}' AND password='{$password}' ");
		$check_qr = $DB->query("SELECT * FROM members WHERE username='{$user}' AND password='{$password}' ");
		//echo "SELECT * FROM members WHERE email='{$email}' AND password='{$password}' ";
		if ($info = $DB->fetch_row($check_qr)) 
		{
			
			if ($info['m_status'] == 0) 
			{
				$ok=0;
				if ($vnT->setting['active_method'] == 2) {
					$mess = $vnT->lang['member']['account_not_active_BQT'];
				} else {
					$link = LINK_MOD . '/activate.html';
					$mess = str_replace("{link}", $link, $vnT->lang['member']['account_not_active']);
				} 				
			}
			
			if ($info['m_status'] == 2) {
				$ok=0;
				$mess = $vnT->lang['member']['account_ban'];
			}
				 
		} else {
			$ok=0;
			$mess = $vnT->lang['member']['mess_login_failt'];
		}			
        // End check	
		if ($ok==1) 
		{	
			//echo "mem_id".$info['mem_id'];
			$vnT->func->vnt_set_member_cookie($info['mem_id'],$ch_remember);		 		
			$vnT->func->User_Login($info);		
			$vnT->DB->query("Update members set last_login=" . time() . " , num_login=num_login+1 where mem_id=" . $info['mem_id'] . " ");
			
		} 
		
		$arr_json['ok'] = $ok ;
		$arr_json['mess'] = $mess;		 		 
		$json = new Services_JSON( );
	  $textout = $json->encode($arr_json);

 		return $textout;
	}

	
	//do_ajax_logout
	function do_ajax_logout() {
		global $DB,$func,$conf,$vnT;
		$arr_json = array();  		
		$time = time();
		$mem_id = $vnT->user['mem_id']; 		 
    $vnT->DB->query("UPDATE sessions SET time='{$time}',mem_id=0 WHERE ( mem_id=".$mem_id." OR s_id='".$vnT->session->get("s_id")."' ) ");
		$vnT->func->vnt_clear_member_cookie();
   	$arr_json['ok'] = 1 ;	 	 		 
		$json = new Services_JSON( );
	  $textout = $json->encode($arr_json);

 		return $textout;
	}
	
	
	//do_ajax_register
	function do_ajax_register() 
	{
		global $DB,$func,$conf,$vnT;
		$arr_json = array(); 
		$email = $_POST['email'] ;
		$pass = $_POST['pass'] ;
		$full_name = $_POST['full_name'] ;
		$phone = $_POST['phone'] ;
		$sec_code = $_POST['sec_code'] ;
		
		$ok = 1;
		$mess = "";
  	 
		if ($_SESSION['sec_code'] == $sec_code)
		{	  
			$password = $func->md10($pass);
			$res_ck = $DB->query("select mem_id,email,username from members WHERE email='".$email."'  ");
			if ($row_ck = $DB->fetch_row($res_ck)) { 
				$ok = 0;
				$mess = "Email này đã có người sử dụng . Vui lòng nhập email khác";
			}else{
				
				$res = $DB->query("select * from member_setting WHERE lang='$vnT->lang_name' ");
				$setting = $DB->fetch_row($res);
								
				$activate_code = md5(uniqid(microtime()));
				$m_status = ($setting['active_method'] == 0) ? 1 : 0;
				$mempoint = $setting['money_default'];
				$cot = array(
					'mem_group' => 1 ,  
					'email' => $email , 
					'password' => $password , 
					'full_name' => $vnT->func->txt_HTML($full_name) , 					 
					'phone' => trim($phone) , 
					'date_join' => $func->getTimestamp() , 
					'activate_code' => $activate_code , 
					'mem_point' => $mempoint , 
					'newsletter' => 1 , 
					'm_status' => $m_status
					);
				
				$kq = $vnT->DB->do_insert("members", $cot);
				if ($kq) {
					$mem_id = $vnT->DB->insertid(); 
					//update  username
					$str_so ="";
					for ($n=strlen(strval($mem_id));$n<4;$n++){
						$str_so .="0";
					}
					$str_so.=$mem_id;
					$username = "KH".$str_so;
					$vnT->DB->query("Update members set username='" .$username. "' where mem_id=" . $mem_id . " ");
					
					if($mempoint){
						//inser gold_history
						$cot_h['mem_id'] = $mem_id;
						$cot_h['action'] = 'register'; 
						$cot_h['value'] = $mempoint;
						$cot_h['reason'] = "Thưởng đăng ký thành viên";
						$cot_h['id_reason'] = $mem_id; 
						$cot_h['code_reason'] = $mem_id; 																				
						$cot_h['notes'] = "Thưởng ".$vnT->func->format_number($mempoint)." đ vào Tài Khoản khi đăng ký thành viên";
						$cot_h['datesubmit'] = time();
						$vnT->DB->do_insert("money_history", $cot_h);
					}
					//load mailer
					$func->include_libraries('phpmailer.phpmailer');
					$vnT->mailer = new PHPMailer();

					$link = ROOT_URL."member/activate.html/" . $activate_code;						
					$content_email = $vnT->func->load_MailTemp("register" . $setting['active_method']);
					$qu_find = array(
						'{full_name}' , 
						'{username}' , 
						'{email}' , 
						'{realpass}' , 
						'{link}' , 
						'{host}');
					$qu_replace = array(
						$full_name , 
						$username , 
						$email , 
						$pass , 
						$link , 
						$_SERVER['HTTP_HOST']);
					$message = str_replace($qu_find, $qu_replace, $content_email);
					$subject = "Thong tin dang ky thanh vien tai ".$_SERVER['HTTP_HOST'];
					$sent = $vnT->func->doSendMail($email, $subject, $message, $vnT->conf['email']);
					//die($m_status);
					if ($m_status) {
						//login
						$info['mem_id'] = $mem_id;
						$info['username'] = $username;
						$kq_login = $func->User_Login($info);
						if ($kq_login) {
							$vnT->func->vnt_set_member_cookie($info['mem_id'],0);
							$vnT->DB->query("Update members set last_login=" . time() . " ,last_ip='".getIp()."' , num_login=num_login+1, last_ip='" . $_SERVER['REMOTE_ADDR'] . "' where mem_id=" . $info['mem_id'] . " ");
						}
						$mess = "Đăng ký thành viên thành công";		
					} else {						
						//$mess = "Đăng ký thành viên thành công . Vui lòng kiểm tra Email để kích hoạt tài khoản"; 	
						//$_SESSION['mess'] = $vnT->func->html_mess($mess) ;
						//$link_ref = ROOT_URL."member/popup.html/?do=activate" ;		
						
						$link_ref = ROOT_URL."member/popup.html/?do=register_success&email=".$email ;		
						$arr_json['link_ref'] = $link_ref ;
					}
					//inser maillist

					$check = $vnT->DB->query("select * from listmail where email='$email'");
					if (! $vnT->DB->num_rows($check)) {
						$cot_mail['email'] = $email;
						$cot_mail['cat_id'] = 1;
						$cot_mail['datesubmit'] = time();
						$DB->do_insert("listmail", $cot_mail);
					} 
					
					$ok = 1 ;						
				} else {
					$ok = 0 ;			
					$mess = "Có lỗi xảy ra . Vui lòng thử lại";
				}
			} 			
			
		}else{
			$ok = 0 ;
			$mess = "Mã bảo vệ không đúng";				
		}		 	
 		$arr_json['m_status'] = $m_status ;		
		$arr_json['ok'] = $ok ;
		$arr_json['mess'] = $mess;		 		 
		$json = new Services_JSON( );
	  $textout = $json->encode($arr_json);

 		return $textout;
	}
	function do_oAuthConnect() {
		global $DB,$func,$conf,$vnT;
		$arr_json = array();
		$ok = 0;
		$identifier = trim($_POST['identifier']);
		$email = trim($_POST['email']);
		$returnUrl = trim($_POST['ref']);

		$api_type = ($_POST['provider']) ? $_POST['provider'] : "facebook";

		$res_ck = $vnT->DB->query("SELECT * FROM members WHERE  email='".$email."' AND email<>'' " ) 		;
		if($row = $vnT->DB->fetch_row($res_ck)){
			$ok = 1 ;

			$vnT->func->vnt_set_member_cookie($row['mem_id'],0);
			$vnT->func->User_Login($row);
			$vnT->DB->query("Update members set last_login=" . time() . " ,last_ip='".$_SERVER['REMOTE_ADDR']."' , num_login=num_login+1 where mem_id=" . $row['mem_id'] . "");

			if($row['api_pass']){
				$link_ref = ($returnUrl) ? "http://".$_SERVER['HTTP_HOST'].$returnUrl : $vnT->conf['rooturl']."member.html";
			}else{
				$link_ref = $vnT->conf['rooturl']."member/api_changepass.html";
			}

		}else{
			$link_ref = $vnT->conf['rooturl']."member/api_changepass.html";

			$activate_code = md5(uniqid(microtime()));
			$m_status =  1 ;
			$ip = $_SERVER['REMOTE_ADDR'];
			$pass = $vnT->func->m_random_str(6);
			$password =  $vnT->func->md10($pass);

			$full_name = $vnT->func->HTML($_POST['name']);
			$gender = ($_POST['gender'] =="male" ) ? 1 : 2 ;
			$address = $vnT->func->HTML($_POST['address']);
			$phone = $vnT->func->HTML($_POST['phone']);

			$birthday = '';
			if($_POST['birthday']){
				$tmp = @explode("/",$_POST['birthday']);
				$birthday = $tmp[1]."/".$tmp[0]."/".$tmp[2];
			}
			$avatar ='';
			if ($api_type == "facebook") {
				$url = 'https://graph.facebook.com/' . $identifier . '/picture?type=large&redirect=false';
				$obj = json_decode(file_get_contents($url), TRUE);
				$url_img = $obj['data']['url'];
				$img = @file_get_contents($url_img);
				$avatar = $identifier . '.jpg';
			} else {
				if ($_POST['avatar']) {
					$img = @file_get_contents($_POST['avatar']);
					$avatar = $identifier . '.jpg';
				}
			}
			if($avatar) {
				$file = $vnT->conf['rootpath'].'vnt_upload/member/avatar/'.$avatar;
				@file_put_contents($file, $img);
			}
			$cot = array(
				'mem_group' => 1 ,
				'email' => $email ,
				'password' => $password ,
				'phone' => $phone ,
				'avatar' => $avatar ,
				'full_name' => $full_name ,
				'gender' => $gender ,
				'birthday' => $birthday,
				'address' => $address ,

				'date_join' => $vnT->func->getTimestamp() ,
				'activate_code' => $activate_code ,
				'newsletter' => 1 ,
				'last_login' => $vnT->func->getTimestamp() ,
				'm_status' => $m_status,
				'api_type' => $api_type,
				'api_user' => $identifier,
				'api_pass' => 0
			);
			$ok_insert = $vnT->DB->do_insert("members", $cot);
			if ($ok_insert) {
				$ok = 1 ;
				$mem_id = $vnT->DB->insertid();
				//update  username
				$so_next = $mem_id;
				$str_so ="";
				for ($n=strlen(strval($so_next));$n<4;$n++){
					$str_so .="0";
				}
				$str_so.=$so_next;
				$username = "KH".$str_so;
				$vnT->DB->query("Update members set username='" .$username. "' where mem_id=" . $mem_id . " ");

				//login
				$info['mem_id'] = $mem_id;
				$info['username'] = $username;
				$kq = $vnT->func->User_Login($info);
				if ($kq) {
					$vnT->func->vnt_set_member_cookie($info['mem_id'],0);
					$vnT->DB->query("Update members set last_login=" . time() . " ,last_ip='".$_SERVER['REMOTE_ADDR']."' , num_login=num_login+1 where mem_id=" . $mem_id . " ");
				}
			}
		}
		$arr_json['ok'] = $ok;
		$arr_json['mess'] = $mess ;
		$arr_json['link_ref'] = $link_ref;
		$json = new Services_JSON( );
		$textout = $json->encode($arr_json);
		return $textout;
	}
	function do_EditAvatar (){
		global $vnT, $input;
		$arr_json = array();
		$mem_id = (int)$vnT->user['mem_id'];
		//File
		include (PATH_INCLUDE . DS . 'class_files.php');
		$vnT->File  = new FileSystem;
		$mess='';
		$arr_json['ok'] = 0 ;
		$w_avatar = ($vnT->setting['w_avatar']) ? $vnT->setting['w_avatar'] : 180;
		$picture = $_POST['picture'] ;
		//upload
		if ($picture){
			$up['path'] = PATH_ROOT."/vnt_upload/member";
			$up['dir'] = "/avatar";
			$up['file'] = PATH_ROOT.'/vnt_upload/member/avatar/'.$picture;

			$up['w'] = $w_avatar ;
			$up['h'] = $w_avatar ;
			$up['rect_w']   = $_POST['w'];
			$up['rect_h']   = $_POST['h'];
			$up['posX']   = $_POST['x'];
			$up['posY']   = $_POST['y'];

			$result = $vnT->File->Upload_Crop($up);
			if (empty($result['err'])) {
				$avatar = $result['link'];
			} else {
				$mess = $result['err'];
			}
		}else{
			$mess = "Vui lòng chon hình đại diện";
		}

		if (empty($err)) {
			$cot['avatar'] = $avatar;

			$ok = $vnT->DB->do_update("members", $cot, "mem_id=".$vnT->user['mem_id']);
			if ($ok) {

				$arr_json['ok'] = 1 ;
				$mess = "Cập nhật Avatar thành công";
				$arr_json['html'] = ROOT_URI."vnt_upload/member/avatar/".$avatar ;

				//xoa file
				if(file_exists(PATH_ROOT . '/vnt_upload/member/avatar/'.$picture))
				{
					@unlink(PATH_ROOT.'/vnt_upload/member/avatar/'.$picture);
				}
			} else{
				$mess = "Có lỗi cập nhật ";
			}
		}

		$arr_json['mess'] = $mess ;
		$json = new Services_JSON( );
		$textout = $json->encode($arr_json);

		return $textout;
	}
	function do_EditCover ()
	{
		global $vnT, $input;
		$arr_json = array();
		//File
		include (PATH_INCLUDE . DS . 'class_files.php');
		$vnT->File  = new FileSystem ;

		$mem_id = (int)$vnT->user['mem_id'];
		$arr_json['ok'] = 0 ;

		$picture = $_POST['picture'] ;
		//upload
		if ($picture)
		{
			$up['path'] = PATH_ROOT."/vnt_upload/member";
			$up['dir'] = "/cover";
			$up['file'] = PATH_ROOT.'/vnt_upload/member/cover/'.$picture;

			$up['w'] = 1170 ;
			$up['h'] = 270 ;
			$up['rect_w']   = $_POST['w'];
			$up['rect_h']   = $_POST['h'];
			$up['posX']   = $_POST['x'];
			$up['posY']   = $_POST['y'];

			$result = $vnT->File->Upload_Crop($up);
			if (empty($result['err'])) {
				$photocover = $result['link'];
			} else {
				$mess = $result['err'];
			}
		}else{
			$mess = "Vui lòng chon  hình Cover";
		}

		if(empty($mess))
		{
			$cot['photocover'] = $photocover;

			$ok = $vnT->DB->do_update("members", $cot, "mem_id=".$vnT->user['mem_id']);
			if ($ok) {

				$arr_json['ok'] = 1 ;
				$mess = "Cập nhật hình Cover thành công";
				$arr_json['html'] = ROOT_URI."vnt_upload/member/cover/".$photocover ;

			} else{
				$mess = "Có lỗi cập nhật ";
			}
		}

		$arr_json['mess'] = $mess ;
		$json = new Services_JSON( );
		$textout = $json->encode($arr_json);

		return $textout;
	}

 	
	// end class
}
?>