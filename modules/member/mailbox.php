<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";  
  var $module = "member";
  var $action = "mailbox";
	var $linkMod = "";
  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_".$this->module.".php");
		loadSetting();
		include ("function_".$this->action.".php");
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css"); 
		$vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->action . ".css");		
		
		$vnT->html->addStyleSheet($vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");			
		$vnT->html->addScript($vnT->dir_js . "/jquery_plugins/jquery.validate.js"); 		 
		
		$vnT->html->addScriptDeclaration(" 
			$(function() {
				$('.dates').datepicker({							
					changeMonth: true,
					changeYear: true 
				});
			});
		
		");
		
		
		
		$this->linkMod = LINK_MOD."/mailbox.html";	
		
    //check login
		check_member_login();	
		
		$vnT->conf['indextitle'] = $vnT->lang['member']['mailbox']  ;
		$navation =$vnT->lang['member']['mailbox']  ;
    switch ($input['do']) 
		{	
			case 'sendbox' :  
				$data['main']= $this->do_Sendbox() ;
			break;
			
			case 'write' :
				$data['main'] = $this->do_Write();
			break;
			
			case 'view' : 
				$data['main'] = $this->do_View();
			break;
			
			case 'reply' :
				$data['main'] = $this->do_Reply();
			break;			
			default : 
				$input['do'] = "inbox";
				$data['main']= $this->do_Inbox() ;				
			break;
		} 
 		

		$data['navation'] = get_navation('<li>'.$navation.'</li>');
		$data['box_usercp'] = box_usercp();
		$data['box_sidebar'] = box_sidebar();
			
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  
  }
 
 

//================================ do_Inbox
function do_Inbox ()
{
  global $DB,$func,$input,$conf,$vnT;
	$textout="";
	$mem_id = $vnT->user['mem_id'];
 	$root_link = LINK_ACTION.".html" ;	
	 
	if (isset ($_GET['do_action']) &&  $_GET['do_action'] =="del")
	{		
		$id = (int) $input['id'];
		$ext = $input["ext"];
 		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($input["del_id"])){
			$ids = implode(',', $input["del_id"]);
		}
	 	$ok_del = 0; 
			
		$res_del = $vnT->DB->query("SELECT mail_id,sendbox FROM mailbox WHERE  mail_to=$mem_id AND mail_id IN (".$ids.") ");
		while($row_del = $vnT->DB->fetch_row($res_del))
		{
			if($row_del['sendbox']==0){
				$vnT->DB->query ( "DELETE FROM mailbox WHERE  mail_to=$mem_id AND mail_id=".$row_del['mail_id'] );	
			}else{
				$vnT->DB->query ( "UPDATE  mailbox SET inbox=0 WHERE mail_to=$mem_id  AND mail_id=".$row_del['mail_id']);	
			}
			$ok_del = 1; 			
		}
		
		//echo $sql;
		if ($ok_del){			 		
			 $mess = $vnT->func->html_mess( $vnT->lang['member']['del_success']," alert-autohide");
		}	else $mess = $vnT->func->html_mess($vnT->lang['member']['not_found']," alert-autohide");
		
		$ext_page = str_replace("|", "&", $ext);
		$link_ref = LINK_ACTION.".html/?do=inbox".$ext_page ;	
		$vnT->func->header_redirect($link_ref, $mess);
 		
	}// if
	
	
		
	$p  = ((int)$input['p']) ? (int)$input['p'] : 1 ;
	$key = (isset($input['key'])) ? $input['key'] : "" ; 
	$date_from = (isset($input['date_from'])) ?  $input['date_from'] : "";
	$date_to = (isset($input['date_to'])) ?  $input['date_to'] : ""; 
	
	$ext_pag = "";
	$ext ="";
	$where="";
	 
	$ext.="|p=$p"; 
	 
		
	if($key){ 
		$key_search =  $vnT->func->get_keyword($key) ;
		$where .= " and (mail_subject like '%".$key."%' or LOWER(mail_subject) like '%".$key_search."%' ) ";
			 
		$where_tab .= " AND mail_subject like '%".$key."%'  ";
		$ext_pag .= "&key=" . rawurlencode($key);	
		$ext.="|key=$key";		
	}
	
	if($date_from || $date_to )
	{
		$tmp1 = @explode("/", $date_from);
		$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
		
		$tmp2 = @explode("/", $date_to);
		$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
		
		$where.=" AND (date_update BETWEEN {$time_begin} AND {$time_end} ) ";
		$where_tab .=" AND (date_update BETWEEN {$time_begin} AND {$time_end} ) ";
		$ext_pag.= "&date_from=".$date_from."&date_to=".$date_to ;
		$ext.="|date_from=$date_from|date_to=$date_to";
	}
		
	
	$sql_num = "select mail_id from mailbox where mail_to=$mem_id  AND inbox=1 {$where} ";
	$result_num = $vnT->DB->query($sql_num); 
	$totals = (int)$vnT->DB->num_rows($result_num); 
	$n=20;
	
	$num_pages = ceil($totals/$n) ;
	if ($p > $num_pages) $p=$num_pages;
	if ($p < 1 ) $p=1;
	$start = ($p-1) * $n ; 
	
	if($num_pages>1) {			 
		$nav = "<div class=\"vnt-pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
	}
 	
 
		$sql = "select mb.* , m.full_name as sender_name  
		FROM mailbox mb LEFT JOIN members m ON mb.mail_from=m.mem_id 
		WHERE mail_to={$mem_id}
		AND inbox=1 
		{$where} 
		order by mail_id 
		DESC LIMIT $start,$n ";
		//print "sql =".$sql."<br>";
		$result = $vnT->DB->query ($sql);	
		
		$table['link_action'] = LINK_ACTION.".html" ;		
		$table['title'] = array (
			 'check_box'	=>	"<input type=\"checkbox\" name=\"ch_all\" id=\"ch_all\" value=\"checkbox\" onClick=\"vnTpost.check_all();\">|3%|center", 			 'mail_subject' => $vnT->lang['member']['mail_subject']."||left",
			 'sender' => $vnT->lang['member']['sender']."|15%|left", 
			 'mail_date' => $vnT->lang['member']['mail_date']."|15%|left",
			 'mail_status' => $vnT->lang['member']['mail_status']."|10%|left",
			 'delete' => $vnT->lang['member']['delete']."|5%|center",
		 );
		 
		 if ($vnT->DB->num_rows ($result)){
			 $row = $vnT->DB->get_array($result);
			 for ($i=0;$i<count($row);$i++) {
				$row[$i]['ext_page'] = $ext; 
				$row_info =  render_row($row[$i]);
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['mail_id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
	
		 }else{
			$table['row']=array();
			
		 }
		
		$data['table_list']= ShowTable($table);
		$data['nav'] = $nav;
		$data['totals'] = $totals;
		$data['key'] = $key;
		
		 
		
		$arr_tab = array("inbox" => $vnT->lang['member']['inbox'],"sendbox" => $vnT->lang['member']['sendbox']   );
		foreach ($arr_tab as $k => $val)
		{
			$class = ($k==$input['do']) ? " class='current'" : "";
			$link_tab = LINK_ACTION.".html/?do=".$k;
			$list_tab .= '<li '.$class.'><a href="'.$link_tab.'">'.$val.' </a></li>';	
		}
		$data['list_tab'] = $list_tab;
 				
		$data['err'] = $err;
		if($_SESSION['mess']!=''){ 
			$data['err'] = 	$_SESSION['mess'];
			$_SESSION['mess']='';
		} 
		
		$data['link_add'] = LINK_ACTION.".html/?do=write";
		$data['action_del'] = LINK_ACTION.".html/?do=inbox&do_action=del&ext=".$ext;	
		$data['link_action']  = LINK_ACTION.".html" ;		
		
		
	$this->skin->assign("data", $data);
	$this->skin->parse("html_list_email");
	$nd['content'] = $this->skin->text("html_list_email");
	$nd['f_title'] = $vnT->lang['member']['f_inbox'];
	return $vnT->skin_box->parse_box("box_middle", $nd);

	 
}

//================================ do_Inbox
function do_Sendbox ()
{
  global $DB,$func,$input,$conf,$vnT;
	$textout="";
	$mem_id = $vnT->user['mem_id'];
 	$root_link = LINK_ACTION.".html" ;	
	 
	if (isset ($_GET['do_action']) &&  $_GET['do_action'] =="del")
	{		
		$id = (int) $input['id'];
		$ext = $input["ext"];
 		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($input["del_id"])){
			$ids = implode(',', $input["del_id"]);
		}
	 	$ok_del = 0; 
			
		$res_del = $vnT->DB->query("SELECT mail_id,inbox FROM mailbox WHERE  mail_from=$mem_id AND mail_id IN (".$ids.") ");
		while($row_del = $vnT->DB->fetch_row($res_del))
		{
			if($row_del['inbox']==0){
				$vnT->DB->query ( "DELETE FROM mailbox WHERE  mail_from=$mem_id AND mail_id=".$row_del['mail_id'] );	
			}else{
				$vnT->DB->query ( "UPDATE  mailbox SET sendbox=0 WHERE mail_from=$mem_id  AND mail_id=".$row_del['mail_id']);	
			}
			$ok_del = 1; 			
		}
		
		//echo $sql;
		if ($ok_del){			 		
			 $mess = $vnT->func->html_mess( $vnT->lang['member']['del_success']," alert-autohide");
		}	else $mess = $vnT->func->html_mess($vnT->lang['member']['not_found']," alert-autohide");
		
		$ext_page = str_replace("|", "&", $ext);
		$link_ref = LINK_ACTION.".html/?do=sendbox".$ext_page ;	
		$vnT->func->header_redirect($link_ref, $mess);
 		
	}// if
	
	
		
	$p  = ((int)$input['p']) ? (int)$input['p'] : 1 ;
	$key = (isset($input['key'])) ? $input['key'] : "" ; 
	$date_from = (isset($input['date_from'])) ?  $input['date_from'] : "";
	$date_to = (isset($input['date_to'])) ?  $input['date_to'] : ""; 
	
	$ext_pag = "";
	$ext ="";
	$where="";
	
	$ext_pag .= "&do=sendbox";	
	$ext.="|p=$p"; 
	 
	 
	if($key){ 
		$key_search =  $vnT->func->get_keyword($key) ;
		$where .= " and (mail_subject like '%".$key."%' or LOWER(mail_subject) like '%".$key_search."%' ) ";
			 
		$where_tab .= " AND mail_subject like '%".$key."%'  ";
		$ext_pag .= "&key=" . rawurlencode($key);	
		$ext.="|key=$key";		
	}
	
	if($date_from || $date_to )
	{
		$tmp1 = @explode("/", $date_from);
		$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
		
		$tmp2 = @explode("/", $date_to);
		$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
		
		$where.=" AND (date_update BETWEEN {$time_begin} AND {$time_end} ) ";
		$where_tab .=" AND (date_update BETWEEN {$time_begin} AND {$time_end} ) ";
		$ext_pag.= "&date_from=".$date_from."&date_to=".$date_to ;
		$ext.="|date_from=$date_from|date_to=$date_to";
	}
		
	
	$sql_num = "select mail_id from mailbox where mail_from=$mem_id  AND sendbox=1 {$where} ";
	$result_num = $vnT->DB->query($sql_num); 
	$totals = $vnT->DB->num_rows($result_num); 
	$n=20;
	
	$num_pages = ceil($totals/$n) ;
	if ($p > $num_pages) $p=$num_pages;
	if ($p < 1 ) $p=1;
	$start = ($p-1) * $n ; 
	
	if($num_pages>1) {			 
		$nav = "<div class=\"vnt-pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
	}
 	
 
		$sql = "select mb.* , m.full_name as receiver_name 
		FROM mailbox mb LEFT JOIN members m ON mb.mail_to=m.mem_id 
		WHERE mail_from={$mem_id}
		AND sendbox=1 
		{$where} 
		order by mail_id 
		DESC LIMIT $start,$n ";
		//print "sql =".$sql."<br>";
		$result = $vnT->DB->query ($sql);	
		
		$table['link_action'] = LINK_ACTION.".html" ;		
		$table['title'] = array (
			 'check_box'	=>	"<input type=\"checkbox\" name=\"ch_all\" id=\"ch_all\" value=\"checkbox\" onClick=\"vnTpost.check_all();\">|3%|center", 			 'mail_subject' => $vnT->lang['member']['mail_subject']."||left",
			 'receiver' => $vnT->lang['member']['receiver']."|15%|left", 
			 'mail_date' => $vnT->lang['member']['mail_date']."|15%|left",
			 'mail_status' => $vnT->lang['member']['mail_status']."|10%|left",
			 'delete' => $vnT->lang['member']['delete']."|5%|center",
		 );
		 
		 if ($vnT->DB->num_rows ($result)){
			 $row = $vnT->DB->get_array($result);
			 for ($i=0;$i<count($row);$i++) {
				$row[$i]['sendbox'] = 1; 
				$row[$i]['ext_page'] = $ext; 
				$row_info =  render_row($row[$i]);
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['mail_id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
	
		 }else{
			$table['row']=array();
			
		 }
		
		$data['table_list']= ShowTable($table);
		$data['nav'] = $nav;
		$data['totals'] = $totals;
		$data['key'] = $key;
		
		$arr_tab = array("inbox" => $vnT->lang['member']['inbox'],"sendbox" => $vnT->lang['member']['sendbox']   );
		foreach ($arr_tab as $k => $val)
		{
			$class = ($k==$input['do']) ? " class='current'" : "";
			$link_tab = LINK_ACTION.".html/?do=".$k;
			$list_tab .= '<li '.$class.'><a href="'.$link_tab.'">'.$val.' </a></li>';	
		}
		$data['list_tab'] = $list_tab;
		
		
		$data['err'] = $err;
		if($_SESSION['mess']!=''){ 
			$data['err'] = 	$_SESSION['mess'];
			$_SESSION['mess']='';
		} 
		
		$data['link_add'] = LINK_ACTION.".html/?do=write";
		$data['action_del'] = LINK_ACTION.".html/?do=sendbox&do_action=del&ext=".$ext;	
		$data['link_action']  = LINK_ACTION.".html/?do=sendbox" ;		
		
		
	$this->skin->assign("data", $data);
	$this->skin->parse("html_list_email");
	$nd['content'] = $this->skin->text("html_list_email");
	$nd['f_title'] = $vnT->lang['member']['f_sendbox'];
	return $vnT->skin_box->parse_box("box_middle", $nd);

	 
}


//================================ do_Write
function do_Write ()
{
  global $DB,$func,$input,$conf,$vnT;
 	
	$mem_id = $vnT->user['mem_id'];
	
	if(isset($input['to'])) {
		$data['mail_to'] = $input['to'];
	}
	
	$data['content_lenght']=0;
	
	if(isset($input['reply_id']))
	{
		$res_reply = $vnT->DB->query("SELECT * FROM mailbox WHERE mail_id=".(int)$input['reply_id'])	;
		if($row_reply = $vnT->DB->fetch_row($res_reply))
		{
			$data['mail_to'] = $row_reply['from_username'];
			$data['mail_subject'] = "Re : ".$row_reply['mail_subject'];
			$data['mail_content'] = $vnT->lang['member']['original_message']."\n". $row_reply['mail_content']."\n-------------------------------------------------------------\n";
		}
	}
	
	if (isset ($_POST['do_submit']))
	{
		$data=$_POST;
		$data['content_lenght'] = strlen($data['mail_content']);
		
		$arr_mailTo = @explode(",",$input['mail_to']);
		$ok_send=1;
		
		//check
		foreach ($arr_mailTo as $key => $value )
		{
			$res_mail_to[$key] = get_mail_to(trim($value)) ;	
 			// Check
			if ($res_mail_to[$key]['mem_id']==$mem_id) $err.= $vnT->lang['member']['not_send_yourself']."<br>";
			//End check	
			if ($res_mail_to[$key]['mem_id']==0) $err.= str_replace("{text}",$value,$vnT->lang['member']['mail_to_not_found'])."<br>";
			
 			if ($err) {
				$ok_send = 0;
			}			
		}//end for
		
		if($ok_send)
		{			
			foreach ($arr_mailTo as $key => $value )
			{				
				$mail_subject = $func->txt_HTML($data['mail_subject']);
				$mail_content = $func->txt_HTML($data['mail_content']);  
				
				$cot['mail_from'] = $mem_id ; 
				$cot['from_name'] = $vnT->user['full_name'] ; 				
				$cot['mail_to'] = $res_mail_to[$key]['mem_id'] ; 				
				$cot['to_name'] = $res_mail_to[$key]['full_name'] ; 				
				$cot['mail_subject'] = $mail_subject ;
				$cot['mail_content'] = $mail_content ;
				$cot['mail_date'] = time();
				$DB->do_insert ("mailbox",$cot);		
				
			}//end for
 				
			$mess= $vnT->func->html_mess($vnT->lang['member']['send_success']);
			$link_ref = LINK_ACTION.".html/?do=sendbox";
      $vnT->func->header_redirect($link_ref,$mess); 
			
		}else{
			$err = $vnT->func->html_err($err);
		}
 	}

 	$data['err'] = $err;
	if($_SESSION['mess']!=''){ 
		$data['err'] = 	$_SESSION['mess'];
		$_SESSION['mess']='';
	}
	
	$data['link_search'] = LINK_ACTION.".html/?do=inbox";
	//$data['nav_menu'] = get_nav_menu($input['do']);	 
	
	$data['link_action'] = LINK_ACTION.".html/?do=write";
	
	$this->skin->assign("data", $data);
	$this->skin->parse("html_write");
	$nd['content'] = $this->skin->text("html_write");
	$nd['f_title'] = $vnT->lang['member']['compose_mail'];
	return $vnT->skin_box->parse_box("box_middle", $nd);
  	
}

 
//================================ do_Write
function do_View ()
{
  global $DB,$func,$input,$conf,$vnT;
	$id= ((int)$input['id']) ?  $input['id']  : 0;
	$mem_id = $vnT->user['mem_id'];
	$username = $vnT->user['username'];
	
	$sql ="select * from mailbox where mail_id=$id AND (mail_from=$mem_id OR mail_to=$mem_id) ";
	$result =$vnT->DB->query ($sql);
	if ($data=$vnT->DB->fetch_row($result)){
		$data['mail_subject'] = $vnT->func->HTML ($data['mail_subject']);
		$data['mail_date'] = date ("H:i A",$data['mail_date']).", ".date ("d/m/Y",$data['mail_date']) ;
		$data['mail_content'] =  $vnT->func->HTML ($data['mail_content']) ; 
	}else{
			
	}
	
 	
	$data['link_search'] = LINK_ACTION.".html/?do=inbox";
	//$data['nav_menu'] = get_nav_menu($input['do']);	
	
	$data['nav_control'] = '<div class="nav-control"><ul>';
	
	if ($mem_id ==$data['mail_to'] ){ 
		//update status
		if($data['is_marketing']==0){
			$DB->query ("Update mailbox SET  mail_status=1 where mail_id=$id AND (mail_from=$mem_id OR mail_to=$mem_id) ");
		}
 		
		if($data['mail_from']){
			$link_write = LINK_ACTION.".html/?do=write&to=".$data['from_username'];
			$data['nav_control'] .= '<li ><a  href="'.$link_write.'" class="write" >'.$vnT->lang['member']['write_to'].'</a></li>';
			
			$link_reply = LINK_ACTION.".html/?do=write&reply_id=".$id;
			$data['nav_control'] .= '<li ><a  href="'.$link_reply.'" class="reply" >'.$vnT->lang['member']['reply'].'</a></li>';
		} 
		$link_del = LINK_ACTION.".html/?do=inbox&do_action=del&id=".$id;
		$data['nav_control'] .= '<li ><a  href="'.$link_del.'" class="del" >'.$vnT->lang['member']['delete'].'</a></li>';	
 		
		$link_back = LINK_ACTION.".html/?do=inbox";
	}else{
		$link_del = LINK_ACTION.".html/?do=sendbox&do_action=del&id=".$id;
		$data['nav_control'] .= '<li ><a  href="'.$link_del.'" class="del" >'.$vnT->lang['member']['delete'].'</a></li>';
		$link_back = LINK_ACTION.".html/?do=sendbox";
	}
	
	$data['nav_control'] .= '</ul></div>';
	$data['link_add'] = LINK_ACTION.".html/?do=write";
	
	$this->skin->assign("data", $data);
	$this->skin->parse("html_view");
	$nd['content'] = $this->skin->text("html_view");
	$nd['f_title'] = $vnT->lang['member']['f_view'] ; 
	return $vnT->skin_box->parse_box("box_middle", $nd);
	
} 

// end class
}
?>