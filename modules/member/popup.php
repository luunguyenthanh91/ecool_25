<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "popup";

  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input; 
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('MOD_DIR_IMAGE', MOD_DIR_IMAGE);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);  
		$this->skin->assign('DIR_JS', $vnT->dir_js);
		$this->skin->assign("DIR_SKIN", $vnT->dir_skin);
		$this->skin->assign("DIR_STYLE", $vnT->dir_style);

		flush();		
		switch ($input['do'])
		{
			case "login": $data['main'] = $this->do_Login();   break;
      case 'avatar': $data['main'] = $this->do_Avatar(); break;
      case 'cover': $data['main'] = $this->do_Cover(); break;
			case "register":  
        $data['main'] = $this->do_Register();
      break;
			case "register_success":  
       $data['main'] = $this->do_Register_Success();
      break;
			case "activate":
        $data['main'] = $this->do_activate();
      break;
      case "send_code":
        $data['main'] = $this->do_send_code();
      break;
			
			default: $data['main'] = " Not Found" ;	break;
		} 
		
		if($input['no_iframe']) {
			echo $data['main'] ;
		}else{
			$this->skin->assign("data", $data);
    	$this->skin->parse("modules");
    	echo $this->skin->text("modules"); 
		}
		exit();
  }

  /**
   * function do_Login ()
   * Khoi tao 
   **/
  function do_Login ()
  {
    global $vnT, $input, $func, $cart, $DB, $conf;
    $data['ref'] = $input['ref'];   
    $data['link_lostpass'] = LINK_MOD . "/forget_pass.html";
    $data['link_register'] = LINK_MOD . "/register.html";
    $data['link_action'] = LINK_MOD . "/login.html/" . $url;
		$data['link_openid'] = ROOT_URL . "openidlogon.php" ;
		$data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
		$data['url_ajax'] = LINK_MOD."/ajax" ;
		
    $this->skin->assign("data", $data);
    $this->skin->parse("html_login"); 
    return $this->skin->text("html_login");
  }
	
	/**
   * function do_Register ()
   * Khoi tao 
   **/
  function do_Register ()
  {
    global $vnT, $input, $func, $cart, $DB, $conf; 
		$data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
		$data['link_dieu_khoan'] = $vnT->link_root."dieu-khoan-thoa-thuan.html"; 
		
		$sec_code = rand(100000, 999999);
		$_SESSION['sec_code'] = $sec_code;
		$data['sec_code'] = $sec_code;
		$data['ver_img'] = ROOT_URI . "includes/sec_image.php?code=".$vnT->func->NDK_encode($sec_code);
		 
		$link1 = $vnT->link_root."page/the-le.html";
		$link2 = $vnT->link_root."page/quy-dinh-hoan-tien.html";
		$data['term_of_register']  = str_replace(array("{link1}","{link2}"),array($link1,$link2),$vnT->lang['member']['term_of_register']);
		 
		$data['url_ajax'] = LINK_MOD."/ajax" ;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_register");
    return $this->skin->text("html_register");
  }
	
	
	/**
   * function do_Register_Success ()
   * Khoi tao 
   **/
  function do_Register_Success ()
  {
    global $vnT, $input ; 
		
		$email = $input['email'] ;
		$mess_register_success = $vnT->setting["mess_register_success"];
		$mess_register_success = str_replace("{email}",$email,$mess_register_success);
		
		$data['mess_register_success'] = $mess_register_success;		 
		
    $this->skin->assign("data", $data);
    $this->skin->parse("html_register_success");
    return $this->skin->text("html_register_success");
  }
	
	
	
	/**
   * function do_activate 
   * 
   **/
  function do_activate ()
  {
    global $input, $vnT, $conf, $DB, $func;
		// extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_active_member']  ;
		
    // check member 
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
			$vnT->func->header_redirect($link_ref); 
    }
		
    if (isset($input['code']) && ! empty($input['code'])) {
      $res_check = $vnT->DB->query("select mem_id,username,email,password,m_status from members where activate_code='{$input['code']}' ");
      if ($row = $vnT->DB->fetch_row($res_check)) {
				if($row['m_status']==0){
					$ok = $vnT->DB->query("Update members set m_status=1  where activate_code='{$input['code']}' ");
					if ($ok) {
						
						//login          
						$kq = $func->User_Login($row);
						if ($kq) {
							$vnT->func->vnt_set_member_cookie($row['mem_id'],0);	
							$DB->query("Update members set last_login=" . time() . " , num_login=num_login+1, last_ip='" . $_SERVER['REMOTE_ADDR'] . "' where mem_id=" . $info['mem_id'] . " ");
						}
						
						$link_ref = LINK_MOD . "/login.html";
						$vnT->func->html_redirect($link_ref, $vnT->lang['member']['mess_active_success']);
					}
				}else{
					$link_ref = LINK_MOD . "/login.html";
					$vnT->func->header_redirect($link_ref);  
				}
      } else {
        $link = LINK_MOD . "/send_code.html";				
        $mess = str_replace("{link}", $link, $vnT->lang['member']['active_code_not_exist']);			
        $err = $vnT->func->html_err($mess);
      }
    }
		
    $num = rand(100000, 999999);
    $data['code_num'] = $num;
    $scode = $vnT->func->NDK_encode($num);
    $data['ver_img'] = "<img src=\"" . ROOT_URI . "includes/sec_image.php?code=$scode\" align=\"absmiddle\" />";
    
		$data['err'] = $err;		
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
			$data['err'] = 	$_SESSION['mess'];
			unset($_SESSION['mess']);
		} 
		$data['f_title'] = $vnT->lang['member']['title_active_member'] ;
    $data['link_action'] = LINK_MOD . "/activate.html";
    
    $this->skin->assign("data", $data);
    $this->skin->parse("html_activate"); 
    return $this->skin->text("html_activate");
  }
	
	
  /**
   * function do_send_code
   * 
   **/
  function do_send_code ()
  {
    global $input, $vnT, $conf, $DB, $func;
		// extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_send_active'] ;
		
    // check da login 
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
      $vnT->func->header_redirect($link_ref);  
    } 

    if (isset($input['do_submit'])) 
		{
			 
			if ($vnT->user['sec_code'] == $input['security_code']) 
			{
				//reset sec_code
				$vnT->func->get_security_code();
				
				$email = $input['email'];
				$result = $DB->query("select mem_id,username,email,activate_code from members where email='{$email}'");
				if ($row = $DB->fetch_row($result)) {
					//send mail	 
					if ($vnT->func->is_muti_lang()) {
						 $link = ROOT_URL.$vnT->lang_name."/member/activate.html/" . $row['activate_code'];
					}else{
						 $link = ROOT_URL."member/activate.html/" . $row['activate_code'];
					}
					$content_email = $vnT->func->load_MailTemp("send_activate_code");
					$qu_find = array(
						'{username}' , 
						'{activate_code}' , 
						'{link}' , 
						'{host}');
					$qu_replace = array(
						$row['username'] , 
						$row['activate_code'] , 
						$link , 
						$_SERVER['HTTP_HOST']);
					$message = str_replace($qu_find, $qu_replace, $content_email);
					
					$subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_send_code']);
					$sent = $vnT->func->doSendMail($email, $subject, $message, $vnT->conf['email']);
					$mess = $vnT->lang['member']['send_active_code_success'];
					$url = LINK_MOD . "/activate.html";
					$vnT->func->html_redirect($url, $mess);
				} else{
					$err = $func->html_err($vnT->lang['member']['email_not_in_system']);
				}
			}else{
				 $err = $func->html_err($vnT->lang['member']['err_security_code_invalid']);	
			}
    } 
		
		$vnT->user['sec_code'] = $vnT->func->get_security_code();
    $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
    $data['ver_img'] = ROOT_URL . "includes/sec_image.php?code=$scode";
		 
    $data['err'] = $err;
		$data['f_title'] = $vnT->lang['member']['title_send_active'] ;
    $data['link_action'] = LINK_MOD . "/send_code.html";
    
    $this->skin->assign("data", $data);
    $this->skin->parse("html_send_activate"); 
    return $this->skin->text("html_send_activate");
  }

  /**
   * function do_Avatar
   *
   **/
  function do_Avatar()
  {
    global  $vnT, $func, $DB, $conf;
    $mem_id = (int)$vnT->user['mem_id'];

    $cur_avatar = ($vnT->user['avatar']) ? $vnT->user['avatar'] : "noavatar.gif" ;
    $data['cur_avatar'] = ROOT_URI."vnt_upload/member/avatar/".$cur_avatar;
    $data['folder_upload'] = $vnT->conf['rootpath'].'vnt_upload/member/avatar' ;
    $data['dir_upload'] =  '';
    $data['link_show_file'] = ROOT_URL.'vnt_upload/member/avatar';


    $data['err'] = $err;
    if($_SESSION['mess']!=''){
      $data['err'] = 	$_SESSION['mess'];
      $_SESSION['mess']='';
    }
    $data['link_action'] = $link_action;

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);

    $this->skin->parse("htm_avatar");
    return $this->skin->text("htm_avatar");
  }

  /**
   * function do_Cover
   *
   **/
  function do_Cover()
  {
    global  $vnT, $func, $DB, $conf;
    $mem_id = (int)$vnT->user['mem_id'];

    $cur_cover = ($vnT->user['photocover']) ? $vnT->user['photocover'] : "nocover.jpg" ;
    $data['cur_cover'] = ROOT_URI."vnt_upload/member/cover/".$cur_cover;
    $data['folder_upload'] = $vnT->conf['rootpath'].'vnt_upload/member/cover' ;
    $data['dir_upload'] =  '';
    $data['link_show_file'] = ROOT_URL.'vnt_upload/member/cover';


    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);

    $this->skin->parse("htm_cover");
    return $this->skin->text("htm_cover");
  }

  // end class
}
?>