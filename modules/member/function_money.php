<?php
/*================================================================================*\
|| 							Name code : function_member.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/ 
define("LINK_ACTION", LINK_MOD . '/money'); 
 
//====== get_price
function get_price ($price,$default="0 VNĐ"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $vnT->func->format_number($price)." VNĐ" ;		
	}else{
		$price = $default;
	}
	return $price;
}

function sub_navation_money($tab='history_add')
{
	global $vnT, $DB, $func;		
  	
	$Nav[0]['act'] = "history_add"; 
	$Nav[0]['title'] = 'Lịch sử nạp tiền';
	$Nav[0]['link'] = LINK_ACTION.".html/?tab=history_add";
	
	$Nav[1]['act'] = "history_apply"; 
	$Nav[1]['title'] = 'Lịch sử tiền thưởng';
	$Nav[1]['link'] =  LINK_ACTION.".html/?tab=history_apply";  
	
	$Nav[2]['act'] = "history_use"; 
	$Nav[2]['title'] = 'Lịch sử sử dụng tiền';
	$Nav[2]['link'] =  LINK_ACTION.".html/?tab=history_use";  
    
  $text = '<div  class="nav-tab" ><ul>';	 
	$num = count($Nav) ;
	$i=0;
	foreach ($Nav as $navItem)
  { 
		$i++; 
		$active = ($tab == $navItem['act']) ? " class='current' " : "";
		$text .= "<li {$active} ><a href=\"".$navItem['link']."\"  >".$navItem['title']."</a></li>";	 
	}	
	
	$text .= '</ul></div><div class="clear"></div>'; 
	
	return $text ;
}	


?>