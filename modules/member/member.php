<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "member";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->html->addScript($vnT->dir_js . "/jquery_plugins/jquery.validate.js");
    //active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO		
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];
		}
    switch ($input['do']) {
      case "update_account":
				$navation =  $vnT->lang['member']['update_account']  ;
        $data['main'] = $this->do_update_account(); 
      break;
			case "upgrade":
        $data['main'] = $this->do_upgrade();
			
      break;			
      case "activate":
        $data['main'] = $this->do_activate();
      break;
      case "send_code":
        $data['main'] = $this->do_send_code();
      break;
      case "changepass":
				$navation =  $vnT->lang['member']['f_changepass']  ;
        $data['main'] = $this->do_changepass();
      break;
			break;
			 case "api_changepass":
         $navation =  $vnT->lang['member']['f_changepass'];
        $data['main'] = $this->do_api_changepass();
      break;
      case "reset_pass":
        $navation =  $vnT->lang['member']['f_changepass'];
        $data['main'] = $this->do_reset_pass();
      break;

      case "forget_pass":
				$navation =  $vnT->lang['member']['forgot_your_password'];
        $data['main'] = $this->do_forget_pass();
      break;
      case "unsubscribe":
				$navation =  $vnT->lang['member']['unsubscribe_email_list'];
        $data['main'] = $this->do_unsubscribe();
      break;
      case "save_search":
				$navation =  "Tim kiếm đã lưu"  ;
        $data['main'] = $this->do_save_search();
      break;
      default:
        $navation =  $vnT->lang['member']['update_account']  ;
        $data['main'] = $this->do_update_account();
      break;
    }
    if($input['do']=="activate" || $input['do']=="send_code" || $input['do']=="forget_pass"){
      $data['box_usercp']='';
    }else{
      $data['box_usercp'] = box_usercp();
    }
    $nav_member = nav_member($input['do']);
		$navation = get_navation($navation);
    $data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child',$nav_member['mb']);
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Overview (){
    global $input, $vnT, $conf, $DB, $func;
    if ($vnT->user['mem_id'] == 0) {
      $linklogin = LINK_MOD . "/login.html" . $url;
      $vnT->func->header_redirect($linklogin);
    }
    $vnT->conf['indextitle'] = $vnT->lang['member']['overview'];
    $data = $vnT->user;
    $data['link_website'] = $vnT->conf['rooturl'].$vnT->setting['seo_name'][$vnT->lang_name]['estore']."/".$vnT->user['username'];
    $data['text_gender'] = $vnT->setting['arr_gender'][$data['gender']] ;
    $data['text_date_join'] = @date("d/m/Y",$data['date_join']);
    $data['text_last_login'] = @date("d/m/Y, H:i",$data['last_login']);
    $data['link_edit'] = LINK_MOD."/update_account.html";
    $this->skin->assign("data", $data);
    $this->skin->parse("html_overview");
    $textout = $this->skin->text("html_overview");
    return $textout;
  }
  function do_update_account (){
    global $input, $vnT, $conf, $DB, $func;
    //File
    include (PATH_INCLUDE . DS . 'class_files.php');
    $vnT->File  = new FileSystem;
    $limit = '1900:'.@date('Y');
    $vnT->html->addStyleSheet($vnT->dir_js."/datepicker/datepicker.css");
    $vnT->html->addScript($vnT->dir_js."/datepicker/datepicker.js");
    $vnT->html->addScriptDeclaration("
      $(document).ready(function(){
        $('#birthday').datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '".$limit."',
          dateFormat: 'dd/mm/yy'
        })
      });
    ");
    $vnT->conf['indextitle'] =  $vnT->lang['member']['update_account'];
    if ($_SERVER['REQUEST_URI']) {
      $url = "/" . $vnT->func->base64url_encode($vnT->seo_url);
    } else {
      $url = "";
    }
    if ($vnT->user['mem_id'] == 0) {
      $linklogin = LINK_MOD . "/login.html" . $url;
			$vnT->func->header_redirect($linklogin) ; 
    }
    $mem_id = $vnT->user['mem_id'];
    $w_avatar = ($vnT->setting['w_avatar']) ? $vnT->setting['w_avatar'] : 130;
    if ($input['btnEdit']){
      if ($input['chk_upload'] == 1) {
        if (! empty($_FILES['image']) && ($_FILES['image']['name'] != "")) {
          $up['path'] = PATH_UPLOAD;
          $up['dir'] = "/avatar";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = $w_avatar;
          $up['file_max_size'] = (100*1024);
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $avatar = $result['link'];
          } else {
            $err .= $result['err'];
          }
        }
      }
      if (empty($err)) {
        $description = $vnT->func->txt_HTML($_POST['description']);
        $full_address = $input['address'];
				// if($input['state']) $full_address .= ", ".$vnT->lib->get_state_name($input['state']) ;
				// if($input['city'])  $full_address .= ", ".$vnT->lib->get_city_name($input['city']) ;
				$cot = array( 
					'phone' => trim($input['phone']) , 
					'full_name' => $vnT->func->txt_HTML($_POST['full_name']) , 
					'gender' => trim($input['gender']) , 
					'birthday' => $input['birthday'],
					'address' => trim($input['address']) , 
					'city' => trim($input['city']) , 
					'state' => trim($input['state']) ,
          'address' => $full_address,
          'description' =>  $description
				);
        if($avatar){
          $cot['avatar'] = $avatar;
        }
        $ok = $DB->do_update("members", $cot, "mem_id=$mem_id");
        if ($ok) {
          $mess = $vnT->func->html_mess($vnT->lang['member']['update_account_success']);
          $url = LINK_MOD . "/update_account.html";
          $vnT->func->header_redirect($url, $mess);
        } else{
          $err = $vnT->func->html_err($vnT->lang['member']['mess_failt']);
        }
      }
    }
    $data = $vnT->user;
    $data = $vnT->user;
    if ($data['avatar']) {
      $src = MOD_DIR_UPLOAD . "/avatar/" . $data['avatar'];
      $pic = "<img src=\"{$src}\" class='img_border' >";
      $data['pic'] = $pic . "<br>";
      $data['pic'] .= "<input name=\"chk_upload\" type=\"hidden\" value=\"1\">";
    } else { 
      $src = MOD_DIR_UPLOAD . "/avatar/no_avatar.jpg";
      $data['pic'] = "<img src=\"{$src}\" class='img_border'>";
      $data['pic'] .= "<input name=\"chk_upload\" type=\"hidden\" value=\"1\">";
    }
    $data['description'] = $vnT->func->txt_unHTML($data['description']);
		$data['err'] = $err;
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
			$data['err'] = 	$_SESSION['mess'];
			unset($_SESSION['mess']);
		}
    $data['list_gender'] = list_gender($data['gender'],'select');
    $data['link_action'] = LINK_MOD . "/update_account.html";
    $data['nav_member'] = nav_member($input['do']);
    $this->skin->assign("data", $data);
    $this->skin->parse("html_profile");
    return $this->skin->text("html_profile");
    // $nd['f_title'] = $vnT->lang['member']['update_account'];
    // $textout = $vnT->skin_box->parse_box("box_middle", $nd);
    // return $textout; 
  }
  function do_upgrade (){
    global $input, $vnT, $conf, $DB, $func;
    if ($_SERVER['REQUEST_URI']) {
      $url = "/" . $vnT->func->NDK_encode($vnT->seo_url);
    } else {
      $url = "";
    }
    // check member 
    if ($vnT->user['mem_id'] == 0) {
      $linklogin = LINK_MOD . "/login.html" . $url;
      @header("Location: {$linklogin}");
      echo "<meta http-equiv='refresh' content='0; url={$linklogin}' />";
    }
    $mem_id = $vnT->user['mem_id'];
    if ($input['btnUpgrade']){
			$data = $input;
			$username = trim($input['user_name']);
			if (empty($err)){
 				$cot['mem_id'] = $mem_id;
				$cot['g_current'] = $vnT->user['mem_group'];
				$cot['g_upgrade'] = $input['mem_group'];
				$cot['status'] = 0;
				$cot['date_post'] = time(); 
				$cot['note'] = $vnT->func->HTML($_POST['note']);
        $ok = $DB->do_insert("member_upgrade", $cot);
        if ($ok) {
          $mess = $vnT->lang['member']['upgrade_account_success'];
          $url = LINK_MOD . ".html";
          $vnT->func->html_redirect($url, $mess);
        } else
          $err .= $vnT->func->html_err($DB->debug());
      } // if err
    }
    $data = $vnT->user;
    $mem_group = ($data['mem_group']>1) ? $data['mem_group'] : 2;
    $data['err'] = $err;
		$g_color = ($vnT->setting['g_name'][$data['g_color']])	? "#".$vnT->setting['g_name'][$data['g_color']] : "#ababab";
		$text_group = "<span class='fGroup' style='color:".$g_color."' >".$vnT->setting['g_name'][$data['mem_group']]."</span> ";
    $data['text_group'] .= $vnT->setting['img_group'][$data['mem_group']]."  <span class='fGroup' >" . getGroupName($data['mem_group']) . "</span>  ";
		$data['list_group_member'] = List_Mem_Group($mem_group," AND price>0 ");
    $data['link_action'] = LINK_MOD . "/upgrade.html";
    // extra title
    $vnT->conf['extra_title'] =  $vnT->lang['member']['upgrade_account']." - " ;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_upgrade");
	  return $this->skin->text("html_upgrade");  
    // $nd['f_title'] = $vnT->lang['member']['upgrade_account'];
    // $textout = $vnT->skin_box->parse_box("box_middle", $nd); 
    // return $textout;
  }
  function do_unsubscribe (){
    global $input, $vnT, $conf, $DB, $func;
    $url = ($_SERVER['REQUEST_URI']) ? "/" . $vnT->func->NDK_encode($vnT->seo_url) : "";
    // check member 
    if ($vnT->user['mem_id'] == 0) {
      $linklogin = LINK_MOD . "/login.html" . $url;
      @header("Location: {$linklogin}");
      echo "<meta http-equiv='refresh' content='0; url={$linklogin}' />";
    }
    if (isset($input['btnEdit'])) {
      $data = $input;
      // Check       
      if ($vnT->user['email'] == $input['email']) {
        $ok = $DB->query("UPDATE listmail set status=0 WHERE email='{$input['email']}' ");
        $nd['content'] = '<table width="100%" border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td class="fTitle">' . $vnT->lang['member']['mess_del_maillist_success'] . '</td>
									</tr>
									<tr>
										<td align="justify">' . $vnT->lang['member']['mess_unsubscribed'] . '</td>
									</tr>
								</table>';
      } else {
        // End check	
        $data['err'] = $func->html_err($vnT->lang['member']['err_not_your_email']) . "<br>";
        $data['link_action'] = LINK_MOD . "/unsubscribe.html";
        $this->skin->assign("data", $data);
        $this->skin->parse("html_unsubscribe");
        $nd['content'] = $this->skin->text("html_unsubscribe");
      }
    } else {
      $data['email'] = $vnT->user['email'];
      $data['link_action'] = LINK_MOD . "/unsubscribe.html";
      $this->skin->assign("data", $data);
      $this->skin->parse("html_unsubscribe");
      $nd['content'] = $this->skin->text("html_unsubscribe");
    }
    $nd['f_title'] = $vnT->lang['member']['unsubscribe_email_list'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_forget_pass (){
    global $input, $vnT, $conf, $DB, $func;
		// check member 
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
			$vnT->func->header_redirect($link_ref); 
    }
	
    // extra title
    $vnT->conf['indextitle'] =  $vnT->lang['member']['forgot_your_password']  ;
		
		$link_action = LINK_MOD . "/forget_pass.html";
		
		if ($input['do_forget']) 
		{
      if ($vnT->user['sec_code'] == $input['security_code'])
      {
        //reset sec_code
        $vnT->func->get_security_code();

        $email = $input['email'];
        // Check
        $check_qr = $DB->query("SELECT * FROM members WHERE email='{$email}' ");
        if ($exist = $DB->fetch_row($check_qr)) {
          $mem_id = $exist['mem_id'];
          $username = $exist['username'];
          $full_name = $exist['name'];
          $code = $exist['activate_code'];

          if ($vnT->func->is_muti_lang()) {
            $link_reset = ROOT_URL.$vnT->lang_name."/member/reset_pass.html/?code=".$code;
          }else{
            $link_reset = ROOT_URL."member/reset_pass.html/?code=".$code;
          }

          $content_email = $vnT->func->load_MailTemp("forget_pass");
          $qu_find = array(
            '{host}' ,
            '{full_name}' ,
            '{username}' ,
            '{email}' ,
            '{link}');
          $qu_replace = array(
            $_SERVER['HTTP_HOST'] ,
            $full_name ,
            $username ,
            $email ,
            $link_reset);
          $message = str_replace($qu_find, $qu_replace, $content_email);
          $subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_change_password']);

          //send mail
          $sent = $vnT->func->doSendMail($email, $subject, $message, $vnT->conf['email']);

          $vnT->func->header_redirect($link_action,$email);

        } else{
          $err = $func->html_err($vnT->lang['member']['err_email_not_exist']);
        }

      }else{
        $err = $func->html_err($vnT->lang['member']['err_security_code_invalid']);
      }
    } 
		
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){

			 $nd['content'] = '<table width="100%" border="0" cellspacing="2" cellpadding="2">								 
							<tr>
								<td align="justify" class="font_err">' . str_replace("{email}",$_SESSION['mess'] , $vnT->lang['member']['note_changepass_success']) . '</td>
							</tr>
						</table>'; 
			unset($_SESSION['mess']);			
			$nd['f_title'] = $vnT->lang['member']['mess_changepass_success'];
			return $vnT->skin_box->parse_box("box_middle", $nd);
		}

    $vnT->user['sec_code'] = $vnT->func->get_security_code();
    $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
    $data['ver_img'] = ROOT_URL . "includes/sec_image.php?code=$scode";

		$data['err'] = $err  ;
		$data['link_action'] = $link_action;
		$this->skin->assign("data", $data);
		$this->skin->parse("html_forgetpass");
		$nd['content'] = $this->skin->text("html_forgetpass");    
    $nd['f_title'] = $vnT->lang['member']['forgot_your_password'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_changepass (){
    global $input, $vnT, $conf, $DB, $func;
		$link_action =  LINK_MOD . "/changepass.html";
    $vnT->conf['indextitle'] = $vnT->lang['member']['f_changepass'];
    if ($vnT->user['mem_id'] == 0){
			$url = "/" . $vnT->func->base64url_encode($vnT->seo_url);
			$link_ref = LINK_MOD . "/login.html".$url;
			$vnT->func->header_redirect($link_ref);  
    }
    $mem_id = $vnT->user['mem_id'];
    $s_id = $vnT->user['session_id'];
    $time = time();
    if (isset($input['do_change'])){
      $old_pass = $func->md10($input['old_pass']);
      $sql_check = "SELECT mem_id,email FROM members WHERE mem_id=$mem_id AND password ='{$old_pass}' ";
      $check_qr = $DB->query($sql_check);
      if ($exist = $DB->fetch_row($check_qr)) {
        $dataup["password"] = $func->md10($input['new_pass']);
        $kq = $DB->do_update("members", $dataup, "mem_id='{$exist['mem_id']}'");
        if ($kq) {
					$vnT->func->vnt_set_member_cookie($vnT->user['mem_id']);
					//send email changepass 
					if ($vnT->func->is_muti_lang()) {
						 $link_login = ROOT_URL.$vnT->lang_name."/member/login.html" ;
					}else{
						 $link_login = ROOT_URL."member/login.html";
					}
          $content_email = $vnT->func->load_MailTemp("changepass");
          $qu_find = array(
            '{host}' , 
            '{full_name}' , 
            '{username}' , 
            '{email}' , 
            '{password}' , 
            '{link_login}');
          $qu_replace = array(
            $_SERVER['HTTP_HOST'] , 
            $vnT->user['full_name'] , 
            $vnT->user['username'] , 
            $vnT->user['email'] , 
            $input['new_pass'], 
            $link_login);
          $message = str_replace($qu_find, $qu_replace, $content_email); 
          $subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_change_password']);
					$sent = $vnT->func->doSendMail($vnT->user['email'], $subject, $message, $vnT->conf['email']);
 					
					$mess = $func->html_mess($vnT->lang['member']['mess_changepass_success']);
					$vnT->func->header_redirect($link_action,$mess);  
          
        } else
          $err = $func->html_err($vnT->lang['member']['mess_changepass_failt']);
      } else
        $err .= $func->html_err($vnT->lang['member']['password_not_exist']);
      // End check	
    }
    $data['err'] = $err;		
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
			$data['err'] = 	$_SESSION['mess'];
			unset($_SESSION['mess']);
		}
    $data['link_action'] = $link_action;
    $data['nav_member'] = nav_member($input['do']);
    $this->skin->assign("data", $data);
    $this->skin->parse("html_changepass");
    return $this->skin->text("html_changepass");
    $nd['f_title'] = $vnT->lang['member']['f_changepass'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_api_changepass (){
    global $input, $vnT, $conf, $DB, $func;
		$link_action =  LINK_MOD . "/api_changepass.html";
		// extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['f_changepass'] ;
		 
    // check member 
    if ($vnT->user['mem_id'] == 0) {
			$url = "/" . $vnT->func->base64url_encode($vnT->seo_url);
			$link_ref = LINK_MOD . "/login.html".$url;
			$vnT->func->header_redirect($link_ref);  
    }
		
    $mem_id = $vnT->user['mem_id'];  		

		if($vnT->user['api_pass']==1)
		{
			$vnT->func->header_redirect(LINK_MOD."/changepass.html");  			
		} 
		
    if (isset($_POST['do_change'])) 
		{
      $new_username =   trim($input['new_username']);
      // Check
      $res_ck = $DB->query("select mem_id from members WHERE username='".$new_username."'  ");
      if ($row_ck = $DB->fetch_row($res_ck)) {
        $err = $vnT->func->html_err($vnT->lang['member']['err_username_exist']);
      }

      if(empty($err)) {
        $dataup['username'] = $new_username;
        $dataup["password"] = $vnT->func->md10($input['new_pass']);
        $dataup["api_pass"] = 1;
        $dataup["phone"] = $input['phone'];
        $kq = $DB->do_update("members", $dataup, "mem_id=".$mem_id);
        if ($kq) {

          $vnT->func->vnt_set_member_cookie($vnT->user['mem_id']);

          //send email changepass
          /*if ($vnT->func->is_muti_lang()) {
            $link_login = ROOT_URL.$vnT->lang_name."/member/login.html" ;
          }else{
            $link_login = ROOT_URL."member/login.html";
          }

          $content_email = $vnT->func->load_MailTemp("changepass");
          $qu_find = array(
            '{host}' ,
            '{full_name}' ,
            '{username}' ,
            '{email}' ,
            '{password}' ,
            '{link_login}');
          $qu_replace = array(
            $_SERVER['HTTP_HOST'] ,
            $vnT->user['full_name'] ,
            $vnT->user['username'] ,
            $vnT->user['email'] ,
            $input['new_pass'],
            $link_login);
          $message = str_replace($qu_find, $qu_replace, $content_email);
          $subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_change_password']);
          $sent = $vnT->func->doSendMail($vnT->user['email'], $subject, $message, $vnT->conf['email']);
          */

          $link_ref = LINK_MOD.".html";
          $vnT->func->header_redirect($link_ref);
        } else {
          $err = $func->html_err($vnT->lang['member']['mess_changepass_failt']);
        }

      } //end err

      
    }

    $data['err'] = $err;		
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
			$data['err'] = 	$_SESSION['mess'];
			unset($_SESSION['mess']);
		} 
		
		$data['email'] = $vnT->user['email'];
		 
    $data['link_action'] = $link_action ;   
    $this->skin->assign("data", $data);
    $this->skin->parse("html_api_changepass");
    $nd['content'] = $this->skin->text("html_api_changepass");
    $nd['f_title'] = $vnT->lang['member']['f_changepass'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_reset_pass (){
    global $input, $vnT, $conf, $DB, $func;
    $link_action =  LINK_MOD . "/reset_pass.html";
    // extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['f_changepass'] ;

    // check member
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
      $vnT->func->header_redirect($link_ref);
    }

    $code = trim($input['code']);
    $res_ck = $vnT->DB->query("SELECT * FROM members WHERE activate_code='".$code."' ");
    if($row_ck = $vnT->DB->fetch_row($res_ck))
    {

      $s_id = $vnT->user['session_id'];
      $time = time();
      if (isset($input['do_change']))
      {
        $dataup["password"] = $func->md10($input['new_pass']);

        $kq = $DB->do_update("members", $dataup, "mem_id=".$row_ck['mem_id']);
        if ($kq) {
          //$do_update = $DB->query("UPDATE sessions SET time='{$time}',mem_id='0' WHERE s_id='{$s_id}'");
          //$mess = $vnT->lang['member']['mess_changepass_success'];
          //$url = LINK_MOD . "/login.html";
          //$vnT->func->html_redirect($url, $mess);
          $vnT->func->vnt_set_member_cookie($vnT->user['mem_id']);

          //send email changepass
          /*if ($vnT->func->is_muti_lang()) {
            $link_login = ROOT_URL.$vnT->lang_name."/member/login.html" ;
          }else{
            $link_login = ROOT_URL."member/login.html";
          }

          $content_email = $vnT->func->load_MailTemp("changepass");
          $qu_find = array(
            '{host}' ,
            '{full_name}' ,
            '{username}' ,
            '{email}' ,
            '{password}' ,
            '{link_login}');
          $qu_replace = array(
            $_SERVER['HTTP_HOST'] ,
            $vnT->user['full_name'] ,
            $vnT->user['username'] ,
            $vnT->user['email'] ,
            $input['new_pass'],
            $link_login);
          $message = str_replace($qu_find, $qu_replace, $content_email);
          $subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_change_password']);
          $sent = $vnT->func->doSendMail($vnT->user['email'], $subject, $message, $vnT->conf['email']);
          */

          $mess = $func->html_mess($vnT->lang['member']['mess_changepass_success']);
          $vnT->func->header_redirect(LINK_MOD."/login.html",$mess);
        } else {
          $err = $func->html_err($vnT->lang['member']['mess_changepass_failt']);
        }

      }

    }else{
      $link = LINK_MOD . "/forget_pass.html";
      $mess = str_replace("{link}", $link, $vnT->lang['member']['reset_code_not_exist']);
      $err = $vnT->func->html_err($mess);
    }


    $data['err'] = $err;
    if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
      $data['err'] = 	$_SESSION['mess'];
      unset($_SESSION['mess']);
    }

    $data['link_action'] = $link_action."/?code=".$code ;

    $this->skin->assign("data", $data);
    $this->skin->parse("html_reset_pass");
    $nd['content'] = $this->skin->text("html_reset_pass");
    $nd['f_title'] = $vnT->lang['member']['f_reset_pass'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_activate (){
    global $input, $vnT, $conf, $DB, $func;
		// extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_active_member']  ;
		
    // check member 
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
			$vnT->func->header_redirect($link_ref); 
    }
		
    if (isset($input['code']) && ! empty($input['code'])) {
      $res_check = $vnT->DB->query("select mem_id,m_status from members where activate_code='{$input['code']}' ");
      if ($row = $vnT->DB->fetch_row($res_check)) {
				if($row['m_status']==0){
					$ok = $vnT->DB->query("Update members set m_status=1  where activate_code='{$input['code']}' ");
					if ($ok) {
						$url = LINK_MOD . "/login.html";
						$vnT->func->html_redirect($url, $vnT->lang['member']['mess_active_success']);
					}
				}else{
					$link_ref = LINK_MOD . "/login.html";
					$vnT->func->header_redirect($link_ref);  
				}
      } else {
        $link = LINK_MOD . "/send_code.html";				
        $mess = str_replace("{link}", $link, $vnT->lang['member']['active_code_not_exist']);			
        $err = $vnT->func->html_err($mess);
      }
    }
		
    $num = rand(100000, 999999);
    $data['code_num'] = $num;
    $scode = $vnT->func->NDK_encode($num);
    $data['ver_img'] = "<img src=\"" . ROOT_URI . "includes/sec_image.php?code=$scode\" align=\"absmiddle\" />";
    
		$data['err'] = $err;		
		if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
			$data['err'] = 	$_SESSION['mess'];
			unset($_SESSION['mess']);
		} 
		
    $data['link_action'] = LINK_MOD . "/activate.html";
    
    $this->skin->assign("data", $data);
    $this->skin->parse("html_activate");
    $nd['content'] = $this->skin->text("html_activate");
    $nd['f_title'] = $vnT->lang['member']['title_active_member'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_send_code (){
    global $input, $vnT, $conf, $DB, $func;
		// extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_send_active'] ;
		
    // check da login 
    if ($vnT->user['mem_id'] > 0) {
      $link_ref = LINK_MOD . ".html";
      $vnT->func->header_redirect($link_ref);  
    } 

    if (isset($input['do_submit'])) 
		{
			 
			if ($vnT->user['sec_code'] == $input['security_code']) 
			{
				//reset sec_code
				$vnT->func->get_security_code();
				
				$email = $input['email'];
				$result = $DB->query("select mem_id,username,email,activate_code from members where email='{$email}'");
				if ($row = $DB->fetch_row($result)) {
					//send mail	 
					if ($vnT->func->is_muti_lang()) {
						 $link = ROOT_URL.$vnT->lang_name."/member/activate.html/" . $row['activate_code'];
					}else{
						 $link = ROOT_URL."member/activate.html/" . $row['activate_code'];
					}
					$content_email = $vnT->func->load_MailTemp("send_activate_code");
					$qu_find = array(
						'{username}' , 
						'{activate_code}' , 
						'{link}' , 
						'{host}');
					$qu_replace = array(
						$row['username'] , 
						$row['activate_code'] , 
						$link , 
						$_SERVER['HTTP_HOST']);
					$message = str_replace($qu_find, $qu_replace, $content_email);
					
					$subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_send_code']);
					$sent = $vnT->func->doSendMail($email, $subject, $message, $vnT->conf['email']);
					$mess = $vnT->lang['member']['send_active_code_success'];
					$url = LINK_MOD . "/activate.html";
					$vnT->func->html_redirect($url, $mess);
				} else{
					$err = $func->html_err($vnT->lang['member']['email_not_in_system']);
				}
			}else{
				 $err = $func->html_err($vnT->lang['member']['err_security_code_invalid']);	
			}
    } 
		
		$vnT->user['sec_code'] = $vnT->func->get_security_code();
    $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
    $data['ver_img'] = ROOT_URL . "includes/sec_image.php?code=$scode";
		 
    $data['err'] = $err;
    $data['link_action'] = LINK_MOD . "/send_code.html";
    
    $this->skin->assign("data", $data);
    $this->skin->parse("html_send_activate");
    $nd['content'] = $this->skin->text("html_send_activate");
    $nd['f_title'] = $vnT->lang['member']['title_send_active'];
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
  function do_save_search (){
    global $input, $vnT, $conf, $DB, $func;
    // extra title
    $vnT->conf['indextitle'] = "Tìm kiếm đã lưu" ;

    // check member
    if ($vnT->user['mem_id'] == 0) {
      $url = "/" . $vnT->func->base64url_encode($vnT->seo_url);
      $link_ref = LINK_MOD . "/login.html".$url;
      $vnT->func->header_redirect($link_ref);
    }


    if (isset ($_GET['do_action']) &&  $_GET['do_action'] =="del")
    {
      $id = (int) $input['id'];
      $ext = $input["ext"];
      $qr = "";

      if ($id != 0) {
        $ids = $id;
      }
      if (isset($input["del_id"])){
        $ids = implode(',', $input["del_id"]);
      }

      $ok_del = 0;
      $ok = $vnT->DB->query("DELETE FROM  member_search WHERE id in (".$ids.") AND  mem_id=".$vnT->user['mem_id']);
      if($ok){
        $ok_del = 1;
      }

      //echo $sql;
      if ($ok_del){
        $mess = $vnT->func->html_mess( $vnT->lang['member']['del_success']," alert-autohide");
      }	else $mess = $vnT->func->html_mess($vnT->lang['member']['not_found']," alert-autohide");

      $ext_page = str_replace("|", "&", $ext);
      $link_ref = LINK_MOD."/save_search.html".$ext_page ;
      $vnT->func->header_redirect($link_ref, $mess);

    }// if


    $p = ((int)$input['p']) ? (int)$input['p'] : 1 ;
    $root_link = LINK_ACTION.".html" ;
    $html_row="";
    $where ="";
    $ext_pag = "&do=hide";

    $sql_num = "SELECT id
						FROM member_search
						WHERE mem_id = ".$vnT->user['mem_id']."
						$where  ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);

    $n = 10;

    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;

    if($num_pages>1) {
      $nav = "<div class=\"pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
    }

    $sql = "SELECT *
						FROM member_search
						WHERE mem_id = ".$vnT->user['mem_id']."
						$where
						order by date_post DESC  LIMIT $start,$n ";
    //print "sql =".$sql."<br>";
    $result = $vnT->DB->query ($sql);

    //echo "<br>".$sql ;
    $result =  $vnT->DB->query($sql);
    if($num = $vnT->DB->num_rows($result))
    {
      $i=0; $stt=0;
      while($row = $vnT->DB->fetch_row($result))
      {
        $i++;
        $stt = ($start+$i);

        $id = $row["id"];
        $checkbox = '<input class="checkbox" type="checkbox" value="'.$id.'" onclick="vnTpost.select_row(\'row_'.$id.'\')"  name="del_id[]">';
        $title = $vnT->func->HTML($row['title']);
        $link = $vnT->link_root.$row['url_save'];
        $title ='<b><a href="' . $link . '" target="_blank">' . $title . '</a></b>';
        $url_save = $row['url_save'] ;


        $link_del	=  LINK_MOD."/save_search.html/?do_action=del&id=".$id   ;
        $action = '<a  href="'.$link.'" title="View" target="_blank" ><i class="fa fa-search"></i></a> ';
        $action .= '<a  href="javascript:;" onClick="vnTpost.del_item(\''.$link_del.'\')" title="Xóa "><i class="fa fa-times"></i></a> ';
        $class = ($i%2==0) ? " class='row1'" : " class='row2'";


        $text .= '<tr id="row_'.$id.'" '.$class.' >';
        $text .= '<td align="center">'.$checkbox.'</td>';
        $text .= "<td valign='top' class='td_title'>".$title.'</td>';
        $text .= "<td valign='top' class='td_link'>".$url_save.'</td>';
        $text .= "<td align='center'><div class='icon_action'>".$action."</div></td></tr>";
      }

    }
    else{
      $text = '<tr class="row1"><td colspan="4" align="center"><b class="font_err">Chưa lưu tìm kiếm nào</b></td></tr>';
    }

    $data['list_items'] = $text;
    $data['nav'] = $nav;
    $data['totals'] = $totals;


    $data['err'] = $err;
    if(isset($_SESSION['mess']) && $_SESSION['mess']!=''){
      $data['err'] = 	$_SESSION['mess'];
      unset($_SESSION['mess']);
    }

    $data['action_del'] = LINK_MOD."/save_search.html/?do_action=del";
    $data['link_action'] = LINK_MOD . "/save_search.html";

    $this->skin->assign("data", $data);
    $this->skin->parse("html_save_search");
    $nd['content'] = $this->skin->text("html_save_search");
    $nd['f_title'] = "Tìm kiếm đã lưu" ;
    return $vnT->skin_box->parse_box("box_middle", $nd);
  }
}
?>