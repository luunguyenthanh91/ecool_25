<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "money";

  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  { 
    global $vnT, $input, $func,  $DB, $conf;    
		include (DIR_MODULE."/". $this->module . "/function_" . $this->module . ".php");
		include (DIR_MODULE."/". $this->module . "/function_" . $this->action . ".php");
    loadSetting(); 

    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js"); 
  	$vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->action . ".css"); 
		
		 // check member 
		if ($vnT->user['mem_id'] == 0) {
			$url = "/?url=" . $vnT->func->base64url_encode($vnT->seo_url);
			$link_ref = LINK_MOD . "/login.html" . $url;
			@header("Location: {$link_ref}");
      echo "<meta http-equiv='refresh' content='0; url={$link_ref}' />";
		}
		
	 
		switch ($input['do']) {     	 
			case "add-money":				 
				$data['main'] = $this->do_Add();
			break;
			case "result":				 
				$data['main'] = $this->do_Result();
			break;			 
			default:				
				$data['main'] = $this->do_History();
			break;
		}



		$data['navation'] = get_navation('<li>'.$navation.'</li>');
		$data['box_usercp'] = box_usercp();
		$data['box_sidebar'] = box_sidebar();
		
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
	
	
		/**
   * function do_Add ()
   * 
   **/
  function do_Add ()
  {
    global $vnT, $func, $DB, $conf ,$input;

		if($vnT->setting['receiver']){

			require_once($vnT->conf['rootpath'].'modules/member/payment/nganluong/include/lib/nusoap.php');
      require_once($vnT->conf['rootpath'].'modules/member/payment/nganluong/include/nganluong.microcheckout.class.php');
 
			$return_url =  $conf['rooturl']."member/money/result.html/?mem_id=".$vnT->user['mem_id'];
			$cancel_url =  $conf['rooturl']."member/money/result.html/?mem_id=".$vnT->user['mem_id'];

			$inputs = array(
				'receiver'		=> $vnT->setting['receiver'], // địa chỉ email người nhận
				'order_code'	=> 'M'.$row['mem_id'].'-'.date('His-dmY'), // mã đơn hàng
				'return_url'	=> $return_url,  
				'cancel_url'	=> $cancel_url,  
				'language'		=> 'vn'  
			);
			$link_checkout = '';
			$obj = new NL_MicroCheckout( $vnT->setting['merchant_site_code'],  $vnT->setting['secure_pass'], $vnT->setting['nganluong_url']);
			$result = $obj->setExpressCheckoutDeposit($inputs);
			if ($result != false) {
				if ($result['result_code'] == '00') {
					$link_checkout = $result['link_checkout'];
				} else {
					$err = 'Mã lỗi '.$result['result_code'].' ('.$result['result_description'].') ' ;
				}
			} else {
				$err ='Lỗi kết nối tới cổng thanh toán Ngân Lượng';
			}

			/*	echo "<br> receiver =" . $vnT->setting['receiver'] ;
					echo "<br> merchant_site_code =" . $vnT->setting['merchant_site_code'] ;
					echo "<br> secure_pass =" . $vnT->setting['secure_pass'] ;
					echo "<br> link_checkout =" . $link_checkout ;*/

			$this->skin->assign("data", $data);
			$this->skin->parse("html_add.html_nganluong");
		} 
		
		
		//SMS
		if($vnT->setting['sms_key']){

			$data['sms_key']	 = $vnT->setting['sms_key'];
			$data['sms_port1']	 = $vnT->setting['sms_port1'];
			$data['sms_port2']	 = $vnT->setting['sms_port2'];
			$data['sms_port3']	 = $vnT->setting['sms_port3'];
			$data['sms_port1_value']	 = $vnT->func->format_number( $vnT->setting['sms_port1_value'])." VNĐ";
			$data['sms_port2_value']	 = $vnT->func->format_number( $vnT->setting['sms_port2_value'])." VNĐ";
			$data['sms_port3_value']	 = $vnT->func->format_number( $vnT->setting['sms_port3_value'])." VNĐ";

			$this->skin->assign("data", $data);
			$this->skin->parse("html_add.html_sms");
		}

		
		
 		$data['err'] = $err ;
		$data['link_checkout'] = $link_checkout ;
		
		$data['mem_id'] = $vnT->user['mem_id'];
		$data['username'] = $vnT->user['username'];
		$data['email'] = $vnT->user['email'];
		$data['total_money'] = $vnT->func->format_number($vnT->user['mem_point'])." đ";
		$data['current_money_expire'] = ($vnT->user['money_expire']) ? @date("d/m/Y , H:i",$vnT->user['money_expire']): 'None';

		$data['link_action'] = LINK_MOD."/money/add-money.html";
		$data['link_buy'] = LINK_MOD."/money/buy-package.html";
		
		$data['err'] = $err;
		$this->skin->assign("data", $data);
		$this->skin->parse("html_add"); 		  
		$textout = $this->skin->text("html_add");
		
		$nd['content'] = $textout ;
		$nd['f_title'] = "Nạp tiền vào tài khoản";	
		return $vnT->skin_box->parse_box("box_middle", $nd); 
  }
 
	/**
   * function do_Result ()
   * Khoi tao 
   **/
  function do_Result ()
  {
    global $vnT, $input, $func,  $DB, $conf;
 		 
		require_once($vnT->conf['rootpath'].'nganluong/include/lib/nusoap.php');
		require_once($vnT->conf['rootpath'].'nganluong/include/nganluong.microcheckout.class.php');
		$url_ws = "https://www.nganluong.vn/micro_checkout_api.php?wsdl" ;
		// kiểm tra tham số trả về
		$obj = new NL_MicroCheckout( $vnT->setting['merchant_site_code'],  $vnT->setting['secure_pass'], $url_ws);
  
		if ($obj->checkReturnUrlAuto()) {
			// gọi Webservice của Ngân Lượng để lấy thông tin đơn hàng
			$inputs = array(
				'token'		=> $obj->getTokenCode(), // mã token nhận được khi gửi yêu cầu thanh toán
			);
			$result = $obj->getExpressCheckout($inputs);
			if ($result != false) {
				if ($result['result_code'] != '00') {
					$mess = 'Mã lỗi '.$result['result_code'].' ('.$result['result_description'].') ';
				}
			} else {
				$err = 'Lỗi kết nối tới cổng thanh toán Ngân Lượng';
			}
		} else {
			$mess = 'Tham số truyền không đúng';
		}
	
		
		$text_result =  '<br><table border="1" cellpadding="3" cellspacing="0" style="border-collapse:collapse;" width="600">
		<tr><th colspan="2">Thông tin đơn hàng</th></tr>
		<tr><td width="200">Mã đơn hàng</td><td>'.$_GET['order_code'].'</td></tr>
		<tr><td>Mã giao dịch</td><td>'.$result['transaction_id'].'</td></tr>
		<tr><td>Loại giao dịch</td><td>'. ($result['transaction_type'] == 1 ? 'Thanh toán ngay' : 'Thanh toán tạm giữ') .'</td></tr>
		<tr><td>Trạng thái</td><td>'.$result['transaction_status'].'</td></tr>
		<tr><td>Số tiền</td><td>'.$result['amount'].'</td></tr>
		<tr><td>Hình thức thanh toán</td><td>'.$result['method_payment_name'].'</td></tr>
		<tr><td>Card type</td><td>'.$result['card_type'].'</td></tr>
		<tr><td>Card amount</td><td>'.$result['card_amount'].'</td></tr>		
		<tr><td>Tên người mua</td><td>'.$result['payer_name'].'</td></tr>
		<tr><td>Email người mua</td><td>'.$result['payer_email'].'</td></tr>
		<tr><td>Điện thoại người mua</td><td>'.$result['payer_mobile'].'</td></tr>
	</table>'; 
	
		if (isset($result) && !empty($result)) {
			if ($result['result_code'] == '00') {
 				
				if($result['transaction_type']==1)
				{
					$order_code = @$_GET['order_code'] ;
					$mem_id = ($vnT->user['mem_id']) ? $vnT->user['mem_id'] : @$_GET['mem_id'] ;
					$res_ck = $vnT->DB->query(" SELECT * FROM member_money WHERE order_code='$order_code' ");
					if(!$vnT->DB->num_rows($res_ck))
					{
						$price = $result['amount'] ;
						$status = ($result['transaction_status']==2 || $result['transaction_status']==4) ? 1 : 0 ;
						
						$cot['mem_id'] = $mem_id ;
						$cot['type'] = "nganluong" ;
						$cot['order_code'] = $order_code; 
						$cot['value'] = $price; 
						$cot['status'] = $status ;
						
						$cot['transaction_id'] = $result['transaction_id'] ;
						$cot['transaction_type'] = $result['transaction_type'] ;
						$cot['transaction_status'] = $result['transaction_status'] ; 
						
						$cot['code'] =  md5(uniqid(microtime()));
						$cot['date_post'] = time();
						
						$kq = $vnT->DB->do_insert("member_money", $cot);
						if($kq)
						{
							$id = $vnT->DB->insertid() ;
							
							//insert tien mem_point
							if($status==1){
								$vnT->DB->query("UPDATE members SET mem_point=mem_point+".$price." WHERE mem_id=".$mem_id." ");

								//inser gold_history
								$cot_h['mem_id'] = $mem_id;
								$cot_h['action'] = 'add'; 
								$cot_h['value'] = $price;
								$cot_h['reason'] = "Nạp từ Ngân Lượng ";
								$cot_h['id_reason'] = $id; 
								$cot_h['code_reason'] = $order_code; 								
								$cot_h['notes'] = "Nạp ".$vnT->func->format_number($price)." đ vào Tài Khoản từ Ngân Lượng";
								$cot_h['datesubmit'] = time();
								$vnT->DB->do_insert("money_history", $cot_h);
							}
							$mess = 'Nạp <b class="font_err">'.$vnT->func->format_number($price).' đ</b> vào tài khoản thành công' ;							
						} 
					
					}else{ 
						$mess = 'Nạp <b class="font_err">'.$vnT->func->format_number($result['amount']).' đ</b> vào tài khoản thành công' ;						
					} 		
				}
				
			} else {
				$mess = $result['result_description'];
			}
		}
		
		$mess .= $text_result ; 
		$data['f_title']  = 'Kết quả Nạp tiền vào tài khoản';
		$data['mess'] = $mess;
		$data['info_contact'] = $vnT->func->load_SiteDoc("info_contact");		 
		 
   	$this->skin->assign("data", $data);
		$this->skin->parse("html_result");  
		$nd['content'] = $this->skin->text("html_result");
    $nd['f_title'] = "Kết quả nạp tiền";
    return $vnT->skin_box->parse_box("box_middle", $nd);
		 
  }	
	
	 /**
   * function do_History ()
   * Khoi tao 
   **/
  function do_History ()
  {
    global $vnT, $input, $func, $cart, $DB, $conf;

		$mem_id = $vnT->user['mem_id'];
		$tab = ($input['tab']) ? $input['tab'] : "history_add";
		$ext_pag = "&tab=".$tab;
		$root_link = LINK_ACTION.".html";
		
		$money_add = 0;
		$money_apply = 0;
		$money_use = 0;
		
		$sql = "SELECT * FROM money_history WHERE mem_id=".$mem_id ;
		$result = $vnT->DB->query($sql);
		if($num=$vnT->DB->num_rows($result))
		{ 
			while($row = $vnT->DB->fetch_row($result))	
			{
				if($row['action']=="add")	{ $money_add+=$row['value'] ;}
				if($row['action']=="receive")	{ $money_add+=$row['value'] ;}
				
				if($row['action']=="register")	{ $money_apply+=$row['value'] ;}
				if($row['action']=="tellfriend")	{ $money_apply+=$row['value'] ;}
				if($row['action']=="complete_order")	{ $money_apply+=$row['value'] ;}
				
				if($row['action']=="send")	{ $money_use+=$row['value'] ;}				
				if($row['action']=="use")	{ $money_use+=$row['value'] ;}
				
			}
		}
		$data['total_money'] = get_price($vnT->user['mem_point']);
		$data['money_add'] = get_price($money_add);
		$data['money_apply'] = get_price($money_apply);
		$data['money_use'] = get_price($money_use);
		 
		
		switch ($tab) 
		{
			case 'history_add' :  	$where =  "AND (action='add' OR action='receive' ) ";	 $table_list =  $this->history_add() ;	  break ;
			case 'history_apply' : 	$where =  " AND (action='register' OR action='tellfriend' OR action='post'  ) "; $table_list =  $this->history_apply() ;		 break ;
			case 'history_use' : 	$where =  " AND (action='use' OR action='send' )"; $table_list =  $this->history_use() ;	 break ;
		}
		 
		$data['table_list']  = $table_list;
		
		 
		
		$data['username'] = $vnT->user['username'];
		$data['email'] = $vnT->user['email'];
		$data['total_money'] = $vnT->func->format_number($vnT->user['mem_point'])." VNĐ";
		$data['current_money_expire'] = ($vnT->user['money_expire']) ? @date("d/m/Y , H:i",$vnT->user['money_expire']): 'None';
		$data['nav_tab'] = sub_navation_money ($tab);
    $data['link_nap'] = LINK_MOD."/money/add-money.html"; 
		 
   	$this->skin->assign("data", $data);
		$this->skin->parse("html_history");  
		$nd['content'] = $this->skin->text("html_history");
    $nd['f_title'] = "Lịch sử tiền tài khoản";
    return $vnT->skin_box->parse_box("box_middle", $nd);
		
		 
  } 
	
	 /**
   * function history_add ()
   * Khoi tao 
   **/
  function history_add ()
  {
		global $vnT, $input, $func, $cart, $DB, $conf;
		$mem_id = $vnT->user['mem_id'];
		$root_link = LINK_ACTION.".html/?tab=".$tab;
		
		$where = " AND (action='add' OR action='receive' )";
		 //lay tong so
    $sql_num = "SELECT id
								FROM money_history
								WHERE mem_id=$mem_id
								{$where} ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		$n = 10;
    
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1)
		{ 
			$nav = "<div class=\"pagination\">".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
		
		
		$sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC LIMIT $start,$n ";
		//print "sql =".$sql."<br>";
		$result = $DB->query ($sql);	 
		
		$table['title'] = array ( 
			 'datesubmit' => "Ngày nạp|10%|center",
			 'code_reason' => "Mã nạp|10%|center",
			 'reason' => "Hình thức nạp tiền|15%|center",
			 'value' => "Số tiền|15%|center",
			 'notes' => "Ghi chú||left",
		 );
		 
		 if ($DB->num_rows ($result)){
			 $row = $DB->get_array($result);
			 for ($i=0;$i<count($row);$i++) {
				 
				$row_info['code_reason'] = $row[$i]['code_reason'];
				$row_info['reason'] = $row[$i]['reason'];
				$row_info['value'] = $vnT->func->format_number($row[$i]['value'])." đ";
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']); 
				$row_info['notes'] = $vnT->func->HTML($row[$i]['notes']);
				
				
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
	
		 }else{
			$table['row']=array();
			$nav= "<div align=center class=font_err >Chưa có lịch sử nào</div>";
		 }
		
		$textout = ShowTable($table);		
		$textout .= $nav;
		
		return $textout ;
	}
	
	 /**
   * function history_add ()
   * Khoi tao 
   **/
  function history_apply ()
  {
		global $vnT, $input, $func, $cart, $DB, $conf;
		$mem_id = $vnT->user['mem_id'];
		$root_link = LINK_ACTION.".html/?tab=history_apply";
		
		$where = " AND (action='add' OR action='register' OR action='post' )";
		 //lay tong so
    $sql_num = "SELECT id
								FROM money_history
								WHERE mem_id=$mem_id
								{$where} ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		$n = 10;
    
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1)
		{ 
			$nav = "<div class=\"pagination\">".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
		
		
		$sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC LIMIT $start,$n ";
		//print "sql =".$sql."<br>";
		$result = $DB->query ($sql);	 
		
		$table['title'] = array ( 
			 'datesubmit' => "Ngày nhận|10%|center",
			 'code_reason' => "Mã nhận|10%|center",
			 'reason' => "Hình thức nạp tiền|15%|center",
			 'value' => "Số tiền|15%|center",
			 'notes' => "Ghi chú||left",
		 );
		 
		 if ($DB->num_rows ($result)){
			 $row = $DB->get_array($result);
			 for ($i=0;$i<count($row);$i++) {
				 
				$row_info['code_reason'] = $row[$i]['code_reason'];
				$row_info['reason'] = $row[$i]['reason'];
				$row_info['value'] = $vnT->func->format_number($row[$i]['value'])." đ";
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']); 
				$row_info['notes'] = $vnT->func->HTML($row[$i]['notes']);
				
				
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
	
		 }else{
			$table['row']=array();
			$nav= "<div align=center class=font_err >Chưa có lịch sử nào</div>";
		 }
		
		$textout = ShowTable($table);		
		$textout .= $nav;
		
		return $textout ;
	}
	
	 /**
   * function history_use ()
   * Khoi tao 
   **/
  function history_use ()
  {
		global $vnT, $input, $func, $cart, $DB, $conf;
		$mem_id = $vnT->user['mem_id'];
		$root_link = LINK_ACTION.".html/?tab=history_use";
		
		$where = " AND (action='use' OR action='send' ) ";
		 //lay tong so
    $sql_num = "SELECT id
								FROM money_history
								WHERE mem_id=$mem_id
								{$where} ";
    //echo $sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		$n = 10;
    
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1)
		{ 
			$nav = "<div class=\"pagination\">".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
		
		
		$sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC LIMIT $start,$n ";
		//print "sql =".$sql."<br>";
		$result = $DB->query ($sql);	 
		
		$table['title'] = array ( 
			 'datesubmit' => "Ngày dùng|15%|center",
			 'code_reason' => "Mã dùng|10%|center",
			 'reason' => "Hình thức dùng tiền|15%|center",
			 'value' => "Số tiền|15%|center",
			 'notes' => "Ghi chú||left",
		 );
		 
		 if ($DB->num_rows ($result)){
			 $row = $DB->get_array($result);
			 for ($i=0;$i<count($row);$i++) {
				 
				$row_info['code_reason'] = $row[$i]['code_reason'];
				$row_info['reason'] = $row[$i]['reason'];
				$row_info['value'] = $vnT->func->format_number($row[$i]['value'])." đ";
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']); 
				$row_info['notes'] = $vnT->func->HTML($row[$i]['notes']);
				
				
				$row_field[$i]=$row_info ;
				$row_field[$i]['stt'] = ($i+1);
				$row_field[$i]['row_id'] = "row_".$row[$i]['id'];
				$row_field[$i]['ext'] ="";
			}
			$table['row'] = $row_field;
	
		 }else{
			$table['row']=array();
			$nav= "<div align=center class=font_err >Chưa có lịch sử nào</div>";
		 }
		
		$textout = ShowTable($table);		
		$textout .= $nav;
		
		return $textout ;
	}
	
	
		
 	 
	
  // end class
}
?>