<?php
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
require_once("../../../_config.php");
require_once ('../../../includes/defines.php');
require_once("../../../includes/class_db.php");

require_once("../../../includes/class_functions.php");
$vnT->DB = $DB = new DB;
$vnT->func = $func = new Func_Global;

$vnT->user = array();
$vnT->user['mem_id'] =0;
$arr_cookie_member = explode("|",$_COOKIE[MEMBER_COOKIE]);

$res_mem = $DB->query("SELECT * FROM members WHERE username='".$arr_cookie_member[0]."' ");
if($row_mem = $DB->fetch_row($res_mem))
{
	$mem_hash = md5($row_mem['mem_id'] . '|' . $row_mem['password']);
	if($arr_cookie_member[1] ==$mem_hash )	{
		$vnT->user  = $row_mem  ;
	}
}

require_once("../../../includes/JSON.php");
if($vnT->user['mem_id']==0) {
	die( "error" );
}

//------get_dir_upload
function get_dir_upload($folder_upload) {
	global $vnT,$conf;

	$dir =date("m_Y");
	$dir_thumbs = $dir."/thumbs";

	$path_dir = $folder_upload."/".$dir;
	$path_thumb_dir =  $folder_upload."/".$dir_thumbs;

	if (!file_exists($path_dir)){
		@mkdir($path_dir,0777);
		@exec("chmod 777 {$path_dir}");
		if (!file_exists($path_thumb_dir)){
			@mkdir($path_thumb_dir,0777);
			@exec("chmod 777 {$path_thumb_dir}");
		}
	}

	return $dir;
}

function file_safe_name($text)
{
	$text = str_replace(array(' ', '-'), array('_','_'), $text) ;
	$text = preg_replace('/[^A-Za-z0-9_]/', '', $text) ;
	return $text ;
}

$arr_json = array();
$folder_upload = $_POST['folder_upload'];
$dir = $_POST['dir_upload'];
$link_folder = $_POST['link_folder'];
$w = $_POST['w'];
$thumb = $_POST['thumb'];
$w_thumb = ($_POST['w_thumb']) ? $_POST['w_thumb'] : 100 ;
$crop = $_POST['crop'] ;

$filename = basename($_FILES['uploadfile']['name']) ;
$type = strtolower(substr($filename,strrpos($filename,".")+1));
$src_name = substr( $filename,0,strrpos($filename,".") );
$file_name =  file_safe_name($src_name).".".$type;

$link_file = ($dir) ? $folder_upload."/".$dir."/".$file_name : $folder_upload."/".$file_name;
$arr_json['link_file'] = $link_file;

if (file_exists($link_file)){
	$file_name = $src_name."_".time().".".$type;
	$link_file = ($dir) ? $folder_upload."/".$dir."/".$file_name : $folder_upload."/".$file_name;
}

if($w){
	if($crop){
		$func->thum ($_FILES["uploadfile"]["tmp_name"], $link_file, $w, $w,"1:1");
	}else{
		$func->thum ($_FILES["uploadfile"]["tmp_name"], $link_file, $w);
	}
	$ok_copy = 1;
}else{
	$ok_copy = copy($_FILES["uploadfile"]["tmp_name"],$link_file);
}
if ($ok_copy) {
	$picture = ($dir) ? $dir."/".$file_name : $file_name ;
	$file_show =    $link_folder."/".$picture;
	$info_size = @getimagesize($link_file);

	if($thumb==1)
	{
		$file_thumb = ($dir) ? $folder_upload."/".$dir."/thumbs/".$file_name : $folder_upload."/thumbs/" . $file_name   ;
		if($crop){
			$func->thum ($link_file, $file_thumb, $w_thumb, $w_thumb,"1:1");
		}else{
			$func->thum ($link_file, $file_thumb, $w_thumb);
		}
		$file_show =  ($dir) ? $link_folder."/".$dir."/thumbs/".$file_name :  $link_folder."/thumbs/" . $file_name ;
	}

	$arr_json['ok'] = "success";
	$arr_json['file_show'] = $file_show;
	$arr_json['picture'] = $picture;
	$arr_json['imagewidth'] = $info_size[0];
	$arr_json['imageheight'] = $info_size[1];

} else {
	$arr_json['ok'] = "error";
}

$json = new Services_JSON( );
$jsout = $json->encode($arr_json);

flush();
echo $jsout;
exit();


?>