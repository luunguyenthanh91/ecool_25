<!-- BEGIN: box_usercp -->
<div class="community-header">
	<div id="id-avatar" class="avatar-coverpf">{data.avatar}</div>
	<div id="id-cover" class="img-coverpf">{data.photocover}</div>

	<div class="cHeader">
		<div class="cHeader-l fl">{data.header_info}</div>
		<div class="cHeader-r fr">
			<div class="cHeader-tool">{data.header_tool} <div class="clear"></div> </div>
		</div>
		<div class="clear"></div>
	</div>

</div>
<!-- END: box_usercp -->