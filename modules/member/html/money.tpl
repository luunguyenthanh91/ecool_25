<!-- BEGIN: modules -->
<div class="wrapper">
    <div id="vnt-navation" class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><div class="navation">{data.navation}</div></div>
    <div class="mod-content">
        {data.box_usercp}
        <div class="row">
            <div class="col-xs-9">{data.main}</div>
            <aside  class="col-xs-3">{data.box_sidebar}</aside>
        </div>
    </div>
</div>
<!-- END: modules -->


<!-- BEGIN: html_add --> 
{data.err}
<script language=javascript>

function checkform(f) {			
	
	if ($("input[name='mem_group']:checked").val() == undefined )
	{
		alert("Vui lòng chọn gói giá cần mua");
		return false;	
	} 	 
  
	return true;
}
 

</script>

<div class="boxUpgrade">
    <form action="{data.link_action}" name="frmMember" id="frmMember" method="post"   onsubmit="return checkform(this);" >
  <div>
      <div>Tài khoản : <strong class="fUsername">{data.email}</strong></div>
      <div style="padding-top: 5px;">Tổng số tiền : <span class="fBig">{data.total_money}</span> </div>
    </div>

    <!-- BEGIN: html_sms -->
    <br />
    <div class="member_fTitle">NẠP TIỀN THÔNG QUA TIN NHẮN SMS </div>
    <div class="boxSMS">
        <div class="divSMS-left fl">
            <div class="sms-text">Soạn tin nhắn theo cú pháp</div>
            <div class="sms-cupthap">
                <div class="cuphap">{data.sms_key} {data.mem_id} <span>gửi đến</span> {data.sms_port1}</div>
                <div  class="cuphap_mid" >hoặc</div>
                <div class="cuphap">{data.sms_key} {data.mem_id} <span>gửi đến</span> {data.sms_port2}</div>
                <div  class="cuphap_mid" >hoặc</div>
                <div class="cuphap">{data.sms_key} {data.mem_id} <span>gửi đến</span> {data.sms_port3}</div>
            </div>

        </div>
        <div class="divSMS-right fr">
            <div class="sms-text"><strong>Bảng giá</strong></div>
            <div class="list-price">
                <table width="100%"  class="table table-bordered">
                    <thead >
                    <tr>
                        <th class="active" style="text-align: center" >Đầu số</th>
                        <th class="active" style="text-align: center" >Mệnh giá</th>
                    </tr>
                    </thead>
                    <tbody>


                    <tr align="center">
                        <td >{data.sms_port1}</td>
                        <td >{data.sms_port1_value}</td>
                    </tr>
                    <tr align="center">
                        <td >{data.sms_port2}</td>
                        <td >{data.sms_port2_value}</td>
                    </tr>
                    <tr align="center">
                        <td >{data.sms_port3}</td>
                        <td >{data.sms_port3_value}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="sms-note">
                <div class="sms-text"><strong>Lưu ý: </strong></div>
                - Chỉ chấp nhận tin nhắn từ Vinaphone, Mobifone, Viettel<br>
                - Vui lòng tải lại trang khi nhận tin báo thành công.<br>
                - Khi gặp sự cố, vui lòng gửi email về {CONF.email} để được hỗ trợ.
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <!-- END: html_sms -->

    <!-- BEGIN: html_nganluong -->
    <br />

<div class="member_fTitle">NẠP TIỀN THÔNG QUA NGÂN LƯỢNG </div>





   <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="260"><a href="https://www.nganluong.vn" target="_blank"><img src="{DIR_MOD}/images/nganluong_logo.png" /></a></td>
    <td style="padding-left:10px;"><div style="color:#F00" ><strong>Chú ý:</strong> Bạn phải có thẻ Visa/Master đã kích hoạt thanh toán online hoặc thẻ ATM đã đăng kí sử dụng internet banking với ngân hàng phát hành thẻ
Thanh toán Online được đảm bảo bởi <a href="https://www.nganluong.vn" target="_blank">Nganluong.vn</a></div></td>
  </tr>
</table>
<br />

<div><h3>Chọn hình thức nạp  :</h3></div>
 <table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr align="center">
     <td width="33%">
  	 <input type="radio" name="payment_option" value="nganluong" checked="checked" /> Tài khoản NgânLượng.vn
    </td>
     <td>
		<input type="radio" name="payment_option" value="mobile" />   Thẻ cào điện thoại

    </td>
     <td width="33%">
		<input type="radio" name="payment_option" value="bank" /> Tài khoản Ngân hàng

    </td>
  </tr>
 </table>
        <!-- END: html_nganluong -->

	 <div class="div-button"  style="text-align: center; padding: 10px;">
 		<button  id="btnAdd" name="btnAdd" type="button" class="btn btn-default" value="Nạp tiền" style="height:30px;   padding:5px 20px;" onclick="setClick();"  ><span style="height:30px;  font-weight:bold; font-size:16px;"  >Nạp tiền</span></button>
    <input type="hidden" id="do_submit" name="do_submit" value="1" />
   </div>




 </form>


</div>

<script language="javascript" src="{CONF.rooturl}nganluong/include/nganluong.apps.mcflow.js"></script>
<script language="javascript">
function setClick()
{
	var payment_options = document.getElementsByTagName('input');
	var payment_option = '';
	for (var i = 0; i < payment_options.length; i++) {
		if (payment_options[i].name == 'payment_option' && payment_options[i].checked) {
			payment_option = payment_options[i].value;
			break;
		}
	}
	var url = '{data.link_checkout}' + '&payment_option='+payment_option;
	var mc_flow = new NGANLUONG.apps.MCFlow();
	mc_flow.startFlow(url);
}
</script> 
<!-- END: html_add --> 


<!-- BEGIN: html_process -->
 
<script language="JavaScript" type="text/javascript">
	function submitDoc(formName) { 
      var obj;
        if (obj=findObj(formName)!=null) 
        {
            findObj(formName).submit(); 
        }
        else 
        {
            alert('The form you are attempting to submit called \'' + formName + '\' couldn\'t be found. Please make sure the submitDoc function has the correct id and name.');
        }
    
    }

  window.onload = Redirector;
	function Redirector() {
	document.f_confirm.submit();			
	}
</script>
<form action="{data.link_action}" method="post" name="f_confirm" id="f_confirm" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 
      <tr>
        <td align="center" height="100">Đang xử lý vui lòng đợi ...... <br />	<img src="{DIR_MOD}/images/progress.gif"  alt="" title="" onload="submitDoc('f_confirm')"  />
</td>
      </tr>
	  	  <tr>
		<td align="center">
		<input name="do_process" type="hidden" value="1" />
		&nbsp;&nbsp;
	
		</td>
	</tr>
    </table>
		</td>
      </tr>
    </table>
</form>
<br />
<!-- END: html_process -->


 
<!-- BEGIN: html_history --> 

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
        <div>Tài khoản : <strong class="fUsername">{data.email}</strong></div>
        <div style="padding-top: 5px;">Tổng số tiền : <span class="fBig">{data.total_money}</span> </div>
    </td>
    <td width="200" align="right" ><a href="{data.link_nap}" title="Nạp tiền"><img src="{DIR_MOD}/images/btnNapTien.gif" alt="Nap tien" /></a></td>
  </tr>
</table>
 <br />

<div class="boxGold">
<table class="table table-striped" id="table_list">
    <thead>
	<tr class="row_title">
	<th width="25%" class="row_tittle">Số tiền nạp</th>
  <th  class="row_tittle">Số tiền được thưởng</th>
  <th width="25%" class="row_tittle">Số tiền sử dụng</th> 
  <th width="25%" class="row_tittle">Tổng số tiền còn lại</th>   
  </tr>
</thead>
    <tbody>
  <tr align="center" class="row1" >
	<td ><span class="fBig">{data.money_add}</span></th>
  <td ><span class="fBig">{data.money_apply}</span></th>
  <td  ><span class="fBig">{data.money_use}</span></th>
  <td  ><span class="fBig">{data.total_money}</span></th>
  </tr>
    </tbody>
</table>
</div>
<br />
<div class="member_fTitle">Lịch sử giao dịch</div>
<div class="boxGold">
{data.err}
<div class="listgold">
<div class="nav-tool">{data.nav_tab}</div>
{data.table_list}
 
</div>
<br />
</div>
<!-- END: html_history --> 
 

 


<!-- BEGIN: html_result --> 
<div class="boxMoney">
<br /> 
<h1 class="messTitle" align="center">{data.f_title}</h1>
<div style="padding:10px 0px;font-size:14px;font-weight:bold;"  align="center" >{data.mess}</div>
 <br>
<div class="clear"></div> 
{data.info_contact} 
 <br />
</div> 
<!-- END: html_result --> 
 