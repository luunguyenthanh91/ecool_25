<!-- BEGIN: modules --><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="{DIR_JS}/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{DIR_JS}/jquery_alerts/jquery.alerts.css" rel="stylesheet" type="text/css" /> 
<link href="{DIR_SKIN}/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
<link href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css" />
<link href="{DIR_MOD}/css/popup.css" rel="stylesheet" type="text/css" />
<link href="{DIR_MOD}/css/member.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery-migrate.min.js"></script>
<script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="{DIR_JS}/core.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_alerts/jquery.alerts.js"></script> 

<script language="javascript">
var ROOT = "{CONF.rooturl}";
var DIR_IMAGE = "{DIR_IMAGE}";
var cmd = "{CONF.cmd}";  
var url_ajax = "{data.url_ajax}" ;
var js_lang=new Array();
js_lang['announce'] = "{LANG.global.announce}";
js_lang['error'] = "{LANG.global.error}";
js_lang['err_username_empty'] = "{LANG.member.err_username_empty}";
js_lang['err_email_empty'] = "{LANG.member.err_email_empty}";
js_lang['err_email_invalid'] = "{LANG.member.err_email_invalid}";
js_lang['err_password_empty'] = "{LANG.member.err_password_empty}"; 

</script>
<script type="text/javascript" src="{DIR_MOD}/js/member.js"></script>  

</head> 
<body>
<div class="divPopup">
{data.main} 
</div>
</body>
</html>
<!-- END: modules -->


<!-- BEGIN: html_login -->
<div id="fb-root"></div>
<script type="text/javascript">	 
 	var facebook_appId = "{data.facebook_appId}"; 
	initFacebook();  
	$(document).ready(function() {
		$("#login_email").focus();
	});
  </script>
<div class="popupTitle"><div class="title">{LANG.member.title_login}</div></div>  
<div class="popupMember">
	<div class="row">
  	<div class="col-xs-7 popupL">
      <div class="box-form-wrapper">
      <div class="title">{LANG.member.login_to_account}</div>
    	<div id="divError"></div>
   		<div class="formLogin">
      <form   role="form"  id="formLogin"  name="formLogin" method="post" action="" onsubmit="return vnTMember.loginPopup();">
        <input type="hidden" name="do_login" value="1"/>
        <input type="hidden" name="ref" id="ref" value="{data.ref}"/>


        <div class="form-group">
          <label class="control-label col-xs-4" for="login_user">{LANG.member.username}</label>
          <div  class="input-group col-xs-8">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input type="text" class="form-control"  id="login_user" name="login_user" value=""  />
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-xs-4" for="login_email">{LANG.member.password}</label>
          <div   class="input-group col-xs-8">
              <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
              <input  type="password" class="form-control" id="login_pass" name="login_pass" />
          </div>
        </div>

        <div class="form-group">
        	<label class="control-label col-xs-4"></label>
          <div   class="input-group col-xs-8">
            <input type="checkbox" name="ch_remember" id="ch_remember" value="1" checked="checked" />&nbsp; {LANG.member.remember_login}
            |  <a href="{data.link_lostpass}" class="forget_pass" title="{LANG.member.forget_password}" target="_top">{LANG.member.forget_password}</a>
          </div>
        </div>

        <div class="form-group">
        	<label class="control-label col-xs-4"></label>
          <div   class="input-group col-xs-8">
              <button  id="btnLogin" name="btnLogin" type="submit" class="btn btn-default" value="{LANG.member.btn_login}" ><span >{LANG.member.btn_login}</span></button>  <a   href="{data.link_register}"   class="aRegister" target="_top" >{LANG.member.btn_register}</a>
          </div>

        </div>



      </form>
		</div>



	  </div>
    </div>


    <div class="col-xs-5 popupR">

      <div class="box-social">
      <div class="title">{LANG.member.login_by_social}</div>
      	<ul>
        	<li><a href="javascript:void(0);" id="fbLogin" class="facebook social"><i class="fa fa-facebook"></i><span>{LANG.member.account_facebook}</span></a></li>
          <li><a href="javascript:void(0);" id="gpLogin" class="google social"><i class="fa fa-google-plus"></i><span>{LANG.member.account_google}</span></a></li>
        </ul>
        <span id="loading" style="display: none"> <img src="{DIR_MOD}/images/loading.gif" alt="login loading"/></span>
      </div>

    </div>
  </div>
	<div class="clear">  </div>
</div>
<!-- END: html_login -->


<!-- BEGIN: html_register -->
<div id="fb-root"></div>
<script type="text/javascript">	 
	js_lang['err_full_name_empty'] = "{LANG.member.err_full_name_empty}";
	js_lang['err_phone_empty'] = "{LANG.member.err_phone_empty}";
	js_lang['err_re_password_incorrect'] = "{LANG.member.err_re_password_incorrect}"; 
	js_lang['err_security_code_empty'] = "{LANG.member.err_security_code_empty}"; 
	js_lang['err_security_code_invalid'] = "{LANG.member.err_security_code_invalid}"; 
	js_lang['err_argee_empty'] = "{LANG.member.err_argee_empty}"; 

	var facebook_appId = "{data.facebook_appId}"; 
	initFacebook(); 
</script>
  
<div class="popupTitle"><div class="title">{LANG.member.title_register}</div></div>  
<div class="popupMember"> 
	<div class="row">
  	<div class="col-xs-7 popupL">    	
      <div class="box-form-wrapper"> 
      <div class="title">{LANG.member.register_acccount}</div>
      
      
  	<div class="boxRegister">    	
   <form class="form-horizontal" action="" name="fRegister" id="fRegister" method="post"  onsubmit="return vnTMember.doRegsiter();" > 
   
      <div class="form-group">
      	<label class="control-label col-xs-4" for="rEmail">{LANG.member.email} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="rEmail" name="rEmail"   autocomplete="off"  /></div>
      </div>
      
      
      <div class="form-group">
        <label class="control-label col-xs-4" for="rPassWord">{LANG.member.password} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="password" class="form-control required" id="rPassWord" name="rPassWord"   autocomplete="off"  /></div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-xs-4" for="rConfirm_PassWord">{LANG.member.re_password} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="password" class="form-control required" id="rConfirm_PassWord" name="rConfirm_PassWord"   autocomplete="off"  /></div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-xs-4" for="rFullName">{LANG.member.full_name} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="rFullName" name="rFullName"   autocomplete="off"  /></div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-xs-4" for="rPhone">{LANG.member.phone} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="rPhone" name="rPhone"   autocomplete="off"  /></div>
      </div>
      
     
      
      <div class="form-group">
        <label class="control-label col-xs-4" for="rEmail">{LANG.member.security_code} <span class="font_err">*</span></label>
        <div class="col-xs-8 ">
        	<div style="float:left;width:50%"><input type="text"  id="security_code" maxlength="6" name="security_code" class="form-control"  value="{data.security_code}"    autocomplete="off" /></div>
          <div style="float:right;width:45%; padding-top:2px; text-align:right;"> <img src="{data.ver_img}" align="absmiddle" /></div>
        </div>
       </div>
       
       <div class="form-group">
         	<label class="col-xs-4" >&nbsp;</label>
          <div class="col-xs-8 "> 
          <div class="note-term" ><input name="r_agree" id="r_agree" type="checkbox" value="1" align="absmiddle" />&nbsp;{data.term_of_register}</div>
          <button  id="btnRegister" name="btnRegister" type="submit" class="btn btn-default" value="{LANG.member.btn_register}" ><span >&nbsp; {LANG.member.btn_register} &nbsp; </span></button> </div>
            
        </div>   
         
         
     
      
  
 </form>
 	 </div>
   
  	 </div>
  
		</div> 
  	
    <div class="col-xs-5 popupR">
    	
      <div class="box-social">
      <div class="title">{LANG.member.register_by_social}</div>
      	<ul>
        	<li><a href="javascript:void(0);" id="fbLogin" class="facebook social"><i class="fa fa-facebook"></i><span>{LANG.member.account_facebook}</span></a></li>
          <li><a href="javascript:void(0);" id="gpLogin" class="google social"><i class="fa fa-google-plus"></i><span>{LANG.member.account_google}</span></a></li>
        </ul>
        <span id="loading" style="display: none"> <img src="{DIR_MOD}/images/loading.gif" alt="login loading"/></span> 
      </div>
      	
    </div>
	</div> 
</div>
</body>
</html>
<!-- END: html_register -->

<!-- BEGIN: html_register_success -->
<div class="popupTitle"><div class="title">{LANG.member.title_register}</div></div>  
<div class="popupMember"> 
 
  	<div class="boxRegister">
    	<div class="mess_register_success">{data.mess_register_success}</div>
		</div>

</div> 
<!-- END: html_register_success -->

<!-- BEGIN: html_activate -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link href="{DIR_MOD}/css/popup.css" rel="stylesheet" type="text/css" />
<link href="{DIR_JS}/jquery_alerts/jquery.alerts.css" rel="stylesheet" type="text/css" /> 
<link href="{DIR_MOD}/css/member.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/jquery.js"></script>
<script type="text/javascript" src="{DIR_JS}/core.js"></script>
<script type="text/javascript" src="{DIR_JS}/jquery_alerts/jquery.alerts.js"></script>
<script type="text/javascript" src="{DIR_JS}/FacebookJS.js"></script> 
<script type="text/javascript" src="{DIR_MOD}/js/member.js"></script>  	

</head>
<div>
<div id="fb-root"></div>
<script type="text/javascript">	 
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var cmd = "{CONF.cmd}";  
		var facebook_appId = "{CONF.facebook_appId}"; 
		initFacebook(); 
  </script>
<div class="popupMember"> 
	<div class="divPopupRegsiter"> 
     <br />

  <div class="mess_activate">{LANG.member.mess_register_success1}</div>
<br />
  
	</div> 
</div> 

<!-- END: html_activate -->

<!-- BEGIN: html_send_activate -->
<script language=javascript>
	function checkform(f) {			
	
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		if (f.email.value == '') {
			alert("{LANG.member.err_email_empty}");
			f.email.focus();
			return false;
		}
		
		if (f.email.value != '' && f.email.value.match(re)==null) 	{
			alert("{LANG.member.err_email_invalid}");
			f.email.focus();
			return false;
		}
		
		if (f.security_code.value == '') {
			alert("{LANG.member.err_security_code_empty}");
			f.security_code.focus();
			return false;
		}	
		return true;
	}
</script>

<div id="Member"  >
<div class="fTitle">{data.f_title}</div>
<br />
{data.err}
 <form id="f_change" name="f_change" method="post" action="{data.link_action}" onsubmit="return checkform(this)">
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="30%" align="right">{LANG.member.email}</td>
    <td><input name="email" id="email" type="text" class="textfiled" value="{INPUT.email}" style="width:300px;" /></td>
  </tr>
  <tr>
    <td align="right">{LANG.member.security_code}</td>
    <td><input name="security_code" type="text" id="security_code" size="10" maxlength="6" value="{data.security_code}" class="textfiled" />
            &nbsp;&nbsp; <img src="{data.ver_img}" align="absmiddle" /></td>
  </tr>

  <tr>
  	<td  align="right">&nbsp;</td>
		<td >
    <input type="hidden" name="do_submit" value="1" />
    <input type="hidden" name="h_code" value="{data.code_num}" />
    <button  id="btnSubmit" name="btnSubmit" type="submit" class="btn" value="{LANG.member.btn_send}" ><span >{LANG.member.btn_send}</span></button>  
  </td>
	</tr>
</table>
 
 </form>  
</div>

<!-- END: html_send_activate -->


<!-- BEGIN: htm_avatar -->
<script language="javascript">
  var ROOT = "{CONF.rooturl}"; ;
  var js_lang=new Array();
  js_lang['announce'] = "{LANG.global.announce}";
  js_lang['error'] = "{LANG.global.error}";
  js_lang['err_avatar_empty'] = "{LANG.member.err_avatar_empty}";

</script>


<script type="text/javascript" src="{DIR_JS}/ajaxupload.js"></script>
<script src="{DIR_JS}/jcrop/jcrop.js"></script>
<link rel="stylesheet" href="{DIR_JS}/jcrop/jcrop.css" type="text/css" />
<script language="javascript">
  $(document).ready(function() {


    var btnUpload=$('#image_upload');
    new AjaxUpload(btnUpload, {
      action: ROOT+'modules/member/ajax/upload-file.php',
      name: 'uploadfile',
      data: {folder_upload : "{data.folder_upload}", dir_upload : "{data.dir_upload}", link_folder :  "{data.link_show_file}", w :  "500"  },
      responseType : "json",
      onSubmit: function(file, ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
          // extension is not allowed
          jAlert('{LANG.member.err_file_invalid}','{LANG.global.error}');
          return false;
        }
        $("#span_image_action").hide();
        $("#upload_result").hide();
        $("#span_image_loading").show();

      },
      onComplete: function(file, response){
        //On completion clear the status
        $("#span_image_loading").hide();
        //Add uploaded file to list
        $("#span_image_action").show();
        $("#upload_result").show();

        if(response.ok==="success"){

          $("#image_url").val('');
          $("#picture").val(response.picture);

          var filenametmp =  response.file_show ;
          $('#tempfile').val(filenametmp);
          $("#image_container").show();
          var img = new Image();
          $(img).load(function ()
          {
            $imgpos.width  = response.imagewidth;
            $imgpos.height = response.imageheight;
            $("#cropbox").remove();
            $(".jcrop-holder").remove();
            $(this).attr('id','cropbox');
            $(this).hide();
            $('#image_container').append(this);

            $(this).fadeIn().Jcrop({
              onChange: showPreview,
              onSelect: showPreview,
              aspectRatio: 1,
              onSelect: updateCoords,
              setSelect: [ 0, 0, 180, 180 ],
              minSize: [ 180, 180 ],
              maxSize: [ 300, 300 ]
            });

            $("#preview").remove();
            var _imgprev = $(document.createElement('img')).attr('id','preview').attr('src',filenametmp);
            $('#preview_container').append(_imgprev);

            var html_buton = '' ;

            $('#divbutton').show();

          }).attr('src', filenametmp);


        } else{
          jAlert('Upload load file '+ file+' error  ','{LANG.global.error}');
        }
      }
    });

  });


  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };
  function checkCoords()
  {
    if (parseInt($('#w').val())){
      return true;
    }else{
      $('#x').val(0);
      $('#y').val(0);
      $('#w').val(180);
      $('#h').val(180);
    }
    return true;
  };

  function showPreview(coords)
  {
    if (parseInt(coords.w) > 0)
    {
      var rx = 180 / coords.w;
      var ry = 180 / coords.h;
      $('#preview').css({
        width: Math.round(rx * $imgpos.width) + 'px',
        height: Math.round(ry * $imgpos.height) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
      });
    }
  };

  $imgpos = {
    width	: '180',
    height	: '180'
  };


</script>
<div  class="divPopup" >


  {data.err}

  <form id="cropattrform" name="cropattrform" action="" method="post" onSubmit="return vnTMember.do_EditAvatar('save');">
    <div class="popup-avatar clearfix">
      <div class="div-preview fl">
        <div id="preview_container" class="preview_container">
          <img id="preview" src="{data.cur_avatar}" />
        </div>
      </div>

      <div class="div-upload fl">

        <h3>{LANG.member.title_upload}</h3>
        <div>{LANG.member.note_file_image_upload}</div>

        <div id="span_image_action" class="input-file"  style="width:350px;">
          <label><span >Browse...</span>
            <input type="file" id="image_upload" name="image_upload" />
          </label>
          <var><input name="image_url" id="image_url" value="{data.image_url}" readonly="readonly" type="text"><input name="picture" id="picture" value="{data.picture}"   type="hidden" ></var>
        </div>

<span id="span_image_loading" class="rightBlog" style="margin-top: 3px;display: none;">
    <img src="{DIR_IMAGE}/loading_small.gif" border="0" /> &nbsp;Uploadding ...
</span>
        <div id="upload_result" > </div>

        <div class="CropImage" id="CropImage">
          <div id="image_container" class="image_container" style="display:none;">
            <h3>{LANG.member.title_crop_avatar}</h3>
            <div id="inform-main">{LANG.member.note_crop_avatar}</div>
          </div>

          <input type="hidden" id="x" name="x" />
          <input type="hidden" id="y" name="y" />
          <input type="hidden" id="w" name="w" />
          <input type="hidden" id="h" name="h" />
          <input type="hidden" id="tempfile" name="tempfile" />

          <div class="div-button" id="divbutton" style="display:none;"><button  id="_crop_bttn" name="btnUpload" type="submit" class="btn btn-default" value="Save" ><span >Save</span></button> &nbsp; <button  id="_crop_cancel_bttn" name="btnCancel" type="submit" class="btn btn-default" value="Cancel" ><span >Cancel</span></button></div>

        </div>



      </div>
    </div>

  </form>

</div>
<!-- END: htm_avatar -->


<!-- BEGIN: htm_cover -->
<script type="text/javascript" src="{DIR_JS}/ajaxupload.js"></script>
<script src="{DIR_JS}/jcrop/jcrop.js"></script>
<link rel="stylesheet" href="{DIR_JS}/jcrop/jcrop.css" type="text/css" />
<script language="javascript">
  $(document).ready(function() {


    var btnUpload=$('#image_upload');
    new AjaxUpload(btnUpload, {
      action: ROOT+'modules/member/ajax/upload-file.php',
      name: 'uploadfile',
      data: {folder_upload : "{data.folder_upload}", dir_upload : "{data.dir_upload}", link_folder :  "{data.link_show_file}", w :  "1170"  },
      responseType : "json",
      onSubmit: function(file, ext){
        if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
          // extension is not allowed
          jAlert('Định dạng file không hợp lệ','Báo lỗi');
          return false;
        }
        $("#span_image_action").hide();
        $("#upload_result").hide();
        $("#span_image_loading").show();

      },
      onComplete: function(file, response){
        //On completion clear the status
        $("#span_image_loading").hide();
        //Add uploaded file to list
        $("#span_image_action").show();
        $("#upload_result").show();

        if(response.ok==="success"){

          $("#image_url").val('');
          $("#picture").val(response.picture);

          var filenametmp =  response.file_show ;
          $('#tempfile').val(filenametmp);
          $("#image_container").show();
          var img = new Image();
          $(img).load(function ()
          {
            $imgpos.width  = response.imagewidth;
            $imgpos.height = response.imageheight;
            $("#cropbox").remove();
            $(".jcrop-holder").remove();
            $(this).attr('id','cropbox');
            $(this).hide();
            $('#image_container').append(this);

            $(this).fadeIn().Jcrop({
              onSelect: updateCoords,
              allowResize: false,
              setSelect: [ 0, 0, 1170, 270 ],
              minSize: [ 1170, 270 ],
              maxSize: [ 1170, 270 ]
            });


            $('#divbutton').show();

          }).attr('src', filenametmp);


        } else{
          jAlert('Upload load file '+ file+' thất bại . Vui lòng thử lại','Báo lỗi');
        }
      }
    });

  });


  function updateCoords(c)
  {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };
  function checkCoords()
  {
    if (parseInt($('#w').val())){
      return true;
    }else{
      $('#x').val(0);
      $('#y').val(0);
      $('#w').val(1170);
      $('#h').val(270);
    }
    return true;
  };

  function showPreview(coords)
  {
    if (parseInt(coords.w) > 0)
    {
      var rx = 1170 / coords.w;
      var ry = 270 / coords.h;
      $('#preview').css({
        width: Math.round(rx * $imgpos.width) + 'px',
        height: Math.round(ry * $imgpos.height) + 'px',
        marginLeft: '-' + Math.round(rx * coords.x) + 'px',
        marginTop: '-' + Math.round(ry * coords.y) + 'px'
      });
    }
  };

  $imgpos = {
    width	: '1170',
    height	: '270'
  };



</script>
<div class="popupCover" >
{data.err}
<br />

<form id="cropattrform" name="cropattrform" action="" method="post" onSubmit="return vnTMember.do_EditCover('save');">



  <h3>{LANG.member.title_upload}</h3>
  <div>{LANG.member.note_file_image_upload}</div>

        <div id="span_image_action" class="input-file"  style="width:350px; margin: 10px auto">
          <label><span >Browse...</span>
            <input type="file" id="image_upload" name="image_upload" />
          </label>
          <var><input name="image_url" id="image_url" value="{data.image_url}" readonly="readonly" type="text"><input name="picture" id="picture" value="{data.picture}"   type="hidden" ></var>
        </div>

<span id="span_image_loading" class="rightBlog" style="margin-top: 3px; text-align: center; display: none;">
    <img src="{DIR_IMAGE}/loading_small.gif" border="0" /> &nbsp;Đang upload ...
</span>
        <div id="upload_result" style="padding-top:10px; text-align: center;" > </div>



        <div  id="CropImage" style="text-align: center">
          <div id="image_container" class="image_container" style="display:none;">
            <h3>Chỉnh sửa hình Cover</h3>
            <div id="inform-main">
              Chọn vị trí ảnh Cover của bạn. Bạn có thể mở rộng ảnh bằng cách kéo các ô vuông của khung.
            </div>
          </div>

          <input type="hidden" id="x" name="x" />
          <input type="hidden" id="y" name="y" />
          <input type="hidden" id="w" name="w" />
          <input type="hidden" id="h" name="h" />
          <input type="hidden" id="tempfile" name="tempfile" />



          <div class="div-button" id="divbutton" style="display:none;"><button  id="_crop_bttn" name="btnUpload" type="submit" class="btn btn-default" value="Save" ><span >Save</span></button> &nbsp; <button  id="_crop_cancel_bttn" name="btnCancel" type="submit" class="btn btn-default" value="Cancel" ><span >Cancel</span></button></div>

        </div>

  <br />

</form>
</div>
<!-- END: htm_cover -->
