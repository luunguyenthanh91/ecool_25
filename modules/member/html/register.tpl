<!-- BEGIN: modules -->
<div id="vnt-content">
  <!--===MAIN TOP===-->
  <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: register -->
<script src="https://apis.google.com/js/api:client.js"></script>
<div id="fb-root"></div>
<div class="boxMember">
  {data.err}
  <form action="{data.link_action}" name="fRegister" id="fRegister" method="post" class="validate">
    <div class="formMember bg1">
      <div class="form-group">
        <div class="formFa fa-lock">
          <input type="text" class="required" id="rUserName" name="rUserName" value="{data.rUserName}" placeholder="{LANG.member.username}" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-key">
          <input type="password" class="required" id="rPassWord" name="rPassWord" value="{data.rPassWord}" autocomplete="off" placeholder="{LANG.member.password}" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-key">
          <input type="password" class="form-control required" id="rConfirm_PassWord" name="rConfirm_PassWord" value="{data.rConfirm_PassWord}" autocomplete="off" placeholder="{LANG.member.re_password}" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-envelope">
          <input type="text" class="required" id="rEmail" name="rEmail" value="{data.rEmail}" placeholder="Email" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-user">
          <input type="text" class="form-control required" id="full_name" name="full_name" value="{data.full_name}" placeholder="{LANG.member.full_name}" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-home">
          <input name="address" id="address" type="text" placeholder="{LANG.member.address}">
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-phone">
          <input type="text" class="form-control required" id="phone" name="phone" value="{data.phone}" placeholder="{LANG.member.phone}" />
        </div>
      </div>
      <div class="form-group">
        <div class="formFa fa-calendar">
          <input name="birthday" id="birthday" type="text" placeholder="{LANG.member.birthday}">
        </div>
      </div>
      <div class="form-group">
        <div class="input-group">
          <div class="formFa fa-shield">
            <input type="text" placeholder="{LANG.member.security_code}" class="required" value="{data.security_code}" id="security_code" name="security_code" autocomplete="off"/>
          </div>
          <span class="input-group-img">
            <img class="security_ver" src="{data.ver_img}" align="absmiddle">
          </span>
        </div>
      </div>
      <div class="checkbox">
        <label for="">
          <input type="checkbox" name="r_agree" id="r_agree" checked="checked" />
          {data.TermOfRegister}
        </label>
      </div>
      <div class="form-group">
        <button id="btnRegister" name="btnRegister" type="submit" value="1">
          <span>{LANG.member.btn_register}</span>
        </button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var v = $("#fRegister").validate({
      rules: {
        rUserName: {
          required: true,
          remote: ROOT+"modules/member/ajax/check_register.php?do=check_username&lang="+lang,
          minlength: 4
        },
        rEmail: {
          required: true,
          email: true,
          remote: ROOT+"modules/member/ajax/check_register.php?do=check_email&lang="+lang
        },
        rPassWord: {
          required: true,
          minlength: 6
        },
        rConfirm_PassWord: {
          required: true,
          equalTo: "#rPassWord"
        },
        r_agree: "required"
      },
      messages: {
          rUserName: {
              required: "{LANG.member.err_username_empty}",
              remote: "{LANG.member.err_username_invalid}",
              minlength: "{LANG.member.err_username_4}"
          },
          rEmail: {
              required: "{LANG.member.err_email_empty}",
              email: "{LANG.member.err_email_invalid}",
              remote: "{LANG.member.err_email_exist}"
          },
          rPassWord: {
              required: "{LANG.member.err_password_empty}",
              minlength: "{LANG.member.err_password_min_char}"
          },

          rConfirm_PassWord: {
              required: "{LANG.member.err_re_password_empty}",
              equalTo: "{LANG.member.err_re_password_incorrect}"
          },
          r_agree: ""
      }
    });
  });
</script>
<!-- END: register -->




