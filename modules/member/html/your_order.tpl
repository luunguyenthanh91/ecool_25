<!-- BEGIN: modules -->
<div id="vnt-content">
  <!--===MAIN TOP===-->
  <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: html_edit -->
<section class="boxOrder">
  <div class="order-code-status">
    <span class="order-code">Đơn hàng <b class="code">#{data.order_code}</b></span> </div
  </div>
</section>
<form action="{data.link_action}" method="post" name="fCart" id="fCart">
  {data.err}
  <section class="boxOrder info_cart">
    <div class="orderTitle">{LANG.member.f_info_cart}</div>
    <div class="box-cart">
      <table width="100%" border="0" cellpadding="1" cellspacing="1" class="border_cart">
        <tr>
          <td class="row_title_cart" align="left" style="padding-left:10px;"><b>{LANG.member.product}</b></td>
            <td class="row_title_cart" width="20%" align="center"><b>Giá </b></td>
            <td class="row_title_cart" width="15%" align="center"><b>Số lượng</b></td>
          <td class="row_title_cart" width="20%" align="center"><b>{LANG.member.total}</b></td>

        </tr>
        <!-- BEGIN: row_cart -->
        <tr  class="row_item_cart" >
          <td style="padding-left:5px" align="left">{row.item_title}</td>
            <td align="center" >{row.price}</td>
          <td align="center">{row.quantity}</td>
          <td align="center"  ><span id="ext_total_{row.id}" class="price">{row.total}</span></td>
        </tr>
        <!-- END: row_cart -->

      </table>

      <div class="div-cart-total">

        <div class="row_cart_total clearfix"><div class="text">{LANG.member.total_price}</div> <span class="price" id="total_price">{data.total_cart}</span> </div>
      </div>
    </div>
  </section>
  <div class="div-button" style="text-align: center">
    <input type="hidden" name="btnUpdate" id="btnUpdate" value="1"/>
    <input type="hidden" name="num_update" id="num_update" value="{data.num_update}">
    <button  id="btnUpdate" name="btnUpdate" type="submit" class="btn btn-default" value="Cập nhật"   ><span >{LANG.member.btn_edit_order}</span></button>
  </div>
</form>
<!-- END: html_edit -->

<!-- BEGIN: html_detail -->
<div class="boxTetu">
  <div class="t1">{LANG.member.order_title} #{data.order_code} - <span>{data.text_status}</span></div>
  <div class="t2">{LANG.member.date_order}: {data.text_date_order}</div>
</div>
<div class="row">
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div id="tableCart">
      <table class="addtocart2">
        <thead>
          <tr>
            <td>{LANG.member.product}</td>
            <td>SL</td>
            <td>Đơn giá</td>
            <td>{LANG.member.total}</td>
          </tr>
        </thead>
        <tbody>
          <!-- BEGIN: row_cart -->
          <tr>
            <td>{row.item_title}</td>
            <td>{row.quantity}</td>
            <td><div class="price_simple">{row.price}</div></td>
            <td><div class="price_total">{row.total}</div></td>
          </tr>
          <!-- END: row_cart -->
        </tbody>
      </table>
      <div class="coupon">
        <div class="totalPrice">
          <ul>
            <li>
              <div class="p">Tổng cộng</div>
              <div class="l">{data.total_cart}</div>
              <div class="clear"></div>
            </li>
            {data.other_price}
            <li>
              <div class="p">{LANG.member.s_price}</div>
              <div class="l">+{data.s_price}</div>
              <div class="clear"></div>
            </li>
            <li>
              <div class="p">{LANG.member.total_price}</div>
              <div class="l v">{data.total_price}</div>
              <div class="clear"></div>
            </li>
          </ul>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <div class="boxCart">
      <div class="title">{LANG.member.payment_address}</div>
      <div class="content">
        {data.payment_address}
      </div>
    </div>
    <div class="boxCart">
      <div class="title">{LANG.member.shipping_address}</div>
      <div class="content">
        {data.shipping_address}
      </div>
    </div>
    <div class="boxCart">
      <div class="title">{LANG.member.shipping_method}</div>
      <div class="content">
        {data.info_shipping}
      </div>
    </div>
    <div class="boxCart">
      <div class="title">{LANG.member.payment_method}</div>
      <div class="content">
        {data.info_payment}
      </div>
    </div>
  </div>
</div>
<div class="divButton">
  <div class="fl"><a class="btnCart" href="javascript:;" onclick="history.go(-1)">{LANG.member.back_to_prev_page}</a></div>
  <div class="clear"></div>
</div>
<!-- END: html_detail -->
 

<!-- BEGIN: html_manage --> 
<script type="text/javascript">
  js_lang['please_chose_item'] = "{LANG.member.please_chose_item}";
  js_lang['error_only_member'] = "{LANG.member.error_only_member}";
  js_lang['are_you_sure'] = "{LANG.member.are_you_sure}";
  js_lang['are_you_sure_del'] = "{LANG.member.are_you_sure_del}";
</script>
{data.nav_member.pc}
<div class="boxMember pp">
  <div class="formSearch">
    {data.err}
    <form action="{data.fsearch}" method="post" name="f_Search" id="f_Search">
      <div class="gridS">
        <div class="col">
          {data.list_status_order}
        </div>
        <div class="col">
          <input name="date_from" id="date_from" type="text" class="dates date" placeholder="{LANG.member.date_order_from}" value="{INPUT.date_from}"/>
        </div>
        <div class="col">
          <input name="date_to" id="date_to" type="text" class="dates date" placeholder="{LANG.member.date_order_to}" value="{INPUT.date_to}"/>
        </div>
        <div class="col">
          <input name="key" id="key" type="text" class="idorder" placeholder="{LANG.member.order_code}" value="{data.keyword}"/>
        </div>
        <div class="col">
          <button id="btnSearch" name="btnSearch" type="submit" value="1">
            <span>{LANG.member.btn_search}</span>
          </button>
        </div>
      </div>
    </form>
  </div>
  <table class="tableManager">
    <thead>
      <tr>
        <th>{LANG.member.order_code}</th>
        <th>{LANG.member.date_order}</th>
        <th>{LANG.member.product}</th>
        <th>{LANG.member.total_price}</th>
        <th>{LANG.member.status}</th>
      </tr>
    </thead>
    <tbody>
      {data.table_list}
    </tbody>
  </table>
  {data.nav}
</div>
<!-- END: html_manage -->