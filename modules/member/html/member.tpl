<!-- BEGIN: modules -->
<div id="vnt-content">
  <!--===MAIN TOP===-->
  <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapper">
    {data.navation}
  </div>
</div>
<div class="wrapping">
	<div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: html_profile -->
<script language="javascript">
	$(document).ready(function() {
		var validator = $("#frmMember").validate({
			rules: {
				full_name: {
					required: true
				}
			},
			messages: {
				full_name: {
					required: " "
				}
			}
		});
	});
</script>
{data.nav_member.pc}
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post" enctype="multipart/form-data" class=" validate">
<div class="boxMember">
  <div class="formMember Profile">
  	{data.err}
    <div class="changeAvatar">
      <div class="img">{data.pic}</div>
      <div class="btnAvatar">
        <input name="image" type="file" id="image" size="30" maxlength="250" style="width: 0.1px;height: 0.1px;overflow: hidden;">
        <button type="button"><span><label for="image">{LANG.member.change_avatar}</label></span></button>
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-user">
      	<input type="text" class="required" id="full_name" name="full_name" value="{data.full_name}"/>
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-home">
      	<input type="text" class="required" id="address" name="address" value="{data.address}" placeholder="{LANG.member.address}" />
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-phone">
      	<input type="text" class="required" id="phone" name="phone" value="{data.phone}" placeholder="{LANG.member.phone}"/>
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-envelope">
      	<input type="text" id="email" name="email" value="{data.email}" disabled="disabled"/>
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-calendar">
        <input name="birthday" id="birthday" type="text" value="{data.birthday}">
      </div>
    </div>
    <div class="form-group">
      <div class="formFa fa-user">
        {data.list_gender}
      </div>
    </div>
    <div class="form-group">
    	<button id="btnEdit" name="btnEdit" type="submit" value="1">
    		<span>{LANG.member.btn_update}</span>
    	</button>
    </div>
  </div>
</div>
<!-- END: html_profile -->

<!-- BEGIN: html_api_changepass --> 
<script language="javascript">
	$(document).ready(function() {
		var validator = $("#frmMember").validate({
			rules: { 
				re_new_pass: {
					required: true, 
					equalTo: "#new_pass"
				} 
			},
			messages: {	 			 
				re_new_pass: {
					required: "{LANG.member.err_re_newpass_empty} ",
					equalTo: "{LANG.member.err_re_password_incorrect}"
				} 
			}
		});	  
	});
</script>
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post" class="validate">
	<div class="boxMember">
	  <div class="formMember bg2">
	  	{data.err}
	  	<div class="note" style="margin-bottom: 10px;color:#f00;">{LANG.member.note_api}</div>
      <div class="form-group">
        <label for="old_pass">{LANG.member.email}</label>
        <div class="formFa fa-key">
        	<input type="text" value="{data.email}" disabled="disabled" readonly="readonly" />
        </div>
      </div>
      <div class="form-group">
        <label for="new_username">{LANG.member.username}</label>
        <div class="formFa fa-key">
        	<input type="text" class="required" id="new_username" name="new_username" title="{LANG.member.err_username_empty}"/>
        </div>
      </div>
      <div class="form-group">
        <label for="new_pass">{LANG.member.new_password}</label>
        <div class="formFa fa-key">
        	<input type="password" class="required" id="new_pass" name="new_pass" title="{LANG.member.err_password_empty}"/>
        </div>
      </div>
      <div class="form-group">
        <label for="re_new_pass">{LANG.member.re_new_password}</label>
        <div class="formFa fa-key">
        	<input type="password" class="required" id="re_new_pass" name="re_new_pass"/>
        </div>
      </div>
      <div class="form-group">
      	<input type="hidden" name="do_change" value="1"/>
        <button id="btnSubmit" name="btnSubmit" type="submit" value="1">
        	<span>{LANG.member.btn_update}</span>
        </button>
      </div>
	  </div>
	</div>
</form>
<!-- END: html_api_changepass -->
 
<!-- BEGIN: html_changepass -->
<script language="javascript">
	$(document).ready(function() {
		var validator = $("#frmMember").validate({
			rules: {
				re_new_pass: {
					required: true, 
					equalTo: "#new_pass"
				}
			},
			messages: {
				re_new_pass: {
					required: " ",
					equalTo: "{LANG.member.err_re_password_incorrect}"
				}
			}
		});	  
	});
</script>
{data.nav_member.pc}
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post" class="validate">
	<div class="boxMember">
	  <div class="formMember bg2">
	  	{data.err}
      <div class="form-group">
        <label for="old_pass">{LANG.member.old_password}</label>
        <div class="formFa fa-key">
        	<input type="password" class="required" id="old_pass" name="old_pass"/>
        </div>
      </div>
      <div class="form-group">
        <label for="">{LANG.member.new_password}</label>
        <div class="formFa fa-key">
        	<input type="password" class="required" id="new_pass" name="new_pass"/>
        </div>
      </div>
      <div class="form-group">
        <label for="">{LANG.member.re_new_password}</label>
        <div class="formFa fa-key">
        	<input type="password" class="required" id="re_new_pass" name="re_new_pass"/>
        </div>
      </div>
      <div class="form-group">
      	<input type="hidden" name="do_change" value="1"/>
        <button id="btnSubmit" name="btnSubmit" type="submit" value="1">
        	<span>{LANG.member.btn_update}</span>
        </button>
      </div>
	  </div>
	</div>
</form>
<!-- END: html_changepass -->

<!-- BEGIN: html_forgetpass -->
<script language=javascript>
	function checkform(f) {
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		if (f.email.value == '') {
			alert("{LANG.member.err_email_empty}");
			f.email.focus();
			return false;
		}
		if (f.email.value != '' && f.email.value.match(re)==null) 	{
			alert("{LANG.member.err_email_invalid}");
			f.email.focus();
			return false;
		}
		return true;
	}
</script>
<div id="Member" class="Member"  >
<div class="boxForm">
	<div class="note">{LANG.member.note_reset_pass}</div>
   {data.err}
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post"   class="form-horizontal "  onsubmit="return checkform(this)">
      <div class="form-group">
      	<label class="control-label col-xs-4" for="email">{LANG.member.email_address} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="email" name="email"  value="{INPUT.email}"    /></div>
      </div>
	<div class="form-group">
		<label class="control-label col-xs-4" for="security_code">{LANG.member.security_code} <span class="font_err">*</span></label>
		<div class="col-xs-8 "><input type="text" class="form-control required" id="security_code" name="security_code"  style="width:150px ; display:inline"   />  &nbsp;&nbsp; <img src="{data.ver_img}" align="absmiddle" /></div>
	</div>
	<div class="form-group">
      	<label class="control-label col-xs-4"></label>
        <div class="col-xs-8 ">
        	<input type="hidden" name="do_forget" value="1"/>  
          <button  id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default" value="{LANG.member.btn_send_pass}" ><span >{LANG.member.btn_send_pass}</span></button>           
        </div>         	 
      </div>   
   
</form>
 </div>
 
 
</div>
<!-- END: html_forgetpass -->

<!-- BEGIN: html_reset_pass -->
<div id="Member" class="Member"  >

	<div class="boxForm">
		<script language="javascript">
			$(document).ready(function() {
				// validate signup form on keyup and submit
				var validator = $("#frmMember").validate({
					rules: {
						re_new_pass: {
							required: true,
							equalTo: "#new_pass"
						}
					},
					messages: {
						re_new_pass: {
							required: "{LANG.member.err_re_newpass_empty}",
							equalTo: "{LANG.member.err_re_password_incorrect}"
						}
					}

				});
			});

		</script>
		<form action="{data.link_action}" name="frmMember" id="frmMember" method="post"   enctype="multipart/form-data" class="form-horizontal validate" >
			{data.err}



			<div class="form-group">
				<label class="control-label col-xs-4" for="old_password">{LANG.member.new_password} <span class="font_err">*</span></label>
				<div class="col-xs-8 "><input type="password" class="form-control required" id="new_pass" name="new_pass"  title="{LANG.member.err_newpass_empty}"   /></div>
			</div>

			<div class="form-group">
				<label class="control-label col-xs-4" for="old_password">{LANG.member.re_new_password} <span class="font_err">*</span></label>
				<div class="col-xs-8 "><input type="password" class="form-control required" id="re_new_pass" name="re_new_pass"    /></div>
			</div>

			<div class="form-group">
				<label class="control-label col-xs-4"></label>
				<div class="col-xs-8 ">
					<input type="hidden" name="do_change" value="1"/>
					<button  id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default" value="{LANG.member.btn_update}" ><span >{LANG.member.btn_update}</span></button>
					<button  id="reset" name="reset" type="reset" class="btn btn-default" value="{LANG.member.btn_reset}" ><span >{LANG.member.btn_reset}</span></button>
				</div>
			</div>

		</form>
	</div>

</div>

<!-- END: html_reset_pass -->


<!-- BEGIN: html_activate -->
<script language=javascript>
	function checkform(f) {			
 
		if (f.code.value == '') {
			alert("{LANG.member.err_active_code_empty}");
			f.code.focus();
			return false;
		}
	
		if (f.security_code.value != f.h_code.value) {
			alert("{LANG.member.err_security_code_invalid}");
			f.security_code.focus();
			return false;
		}		
		return true;
	}
</script>

<div id="Member" class="Member"  >

<div class="boxForm">
   {data.err}
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post"   class="form-horizontal "  onsubmit="return checkform(this)">
     	
      
      <div class="form-group">
      	<label class="control-label col-xs-4" for="code">{LANG.member.active_code} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="code" name="code"    /></div>
      </div>  
      
      <div class="form-group">
      	<label class="control-label col-xs-4" for="security_code">{LANG.member.security_code} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="security_code" name="security_code"  style="width:150px ; display:inline"   />  &nbsp;&nbsp; {data.ver_img}</div>
      </div>
      
      <div class="form-group">
      	<label class="control-label col-xs-4"></label>
        <div class="col-xs-8 ">
        	<input type="hidden" name="do_submit" value="1"/> <input type="hidden" name="h_code" value="{data.code_num}" />
          <button  id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default" value="{LANG.member.btn_active}" ><span >{LANG.member.btn_active}</span></button>           
        </div>         	 
      </div>   
   
</form>
 </div>
  
</div>

<!-- END: html_activate -->

<!-- BEGIN: html_send_activate -->
<script language=javascript>
	function checkform(f) {			
	
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		if (f.email.value == '') {
			alert("{LANG.member.err_email_empty}");
			f.email.focus();
			return false;
		}
		
		if (f.email.value != '' && f.email.value.match(re)==null) 	{
			alert("{LANG.member.err_email_invalid}");
			f.email.focus();
			return false;
		}
		 
		if (f.security_code.value == '') {
			alert("{LANG.member.err_security_code_empty}");
			f.security_code.focus();
			return false;
		}		
		return true;
	}
</script>

<div id="Member" class="Member"  >
<div class="boxForm">
   {data.err}
<form action="{data.link_action}" name="frmMember" id="frmMember" method="post"   class="form-horizontal "  onsubmit="return checkform(this)">
     	
      
      <div class="form-group">
      	<label class="control-label col-xs-4" for="email">{LANG.member.email} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="email" name="email"  value="{INPUT.email}"    /></div>
      </div>  
      
      <div class="form-group">
      	<label class="control-label col-xs-4" for="security_code">{LANG.member.security_code} <span class="font_err">*</span></label>
        <div class="col-xs-8 "><input type="text" class="form-control required" id="security_code" name="security_code"  style="width:150px ; display:inline"   />  &nbsp;&nbsp; <img src="{data.ver_img}" align="absmiddle" /></div>
      </div>
      
      <div class="form-group">
      	<label class="control-label col-xs-4"></label>
        <div class="col-xs-8 ">
        	<input type="hidden" name="do_submit" value="1"/> <input type="hidden" name="h_code" value="{data.code_num}" />
          <button  id="btnSubmit" name="btnSubmit" type="submit" class="btn btn-default" value="{LANG.member.btn_send}" ><span >{LANG.member.btn_send}</span></button>           
        </div>         	 
      </div>   
   
</form>
 </div>
 
    
</div>

<!-- END: html_send_activate -->


<!-- BEGIN: html_save_search -->
<script type="text/javascript">
	var js_lang = new Array();
	js_lang['please_chose_item']   	= "{LANG.member.please_chose_item}";
	js_lang['error_only_member']   	= "{LANG.member.error_only_member}";
	js_lang['are_you_sure_del']   	= "{LANG.member.are_you_sure_del}";

</script>
<script type="text/javascript" src="{DIR_MOD}/js/post.js"></script>
{data.err}
<div class="nav-tool row">
	<div class="col-md-12">
		<ul class="nav_action">
			<li class="arr_down" >&nbsp;</li> <li><a href="javascript:;" onclick="vnTpost.del_selected('{data.action_del}');" class="btn-del">Xóa tin đã chọn </a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>

<div class="div-list">
	<form id="manage" name="manage" method="post" action="{data.link_action}">
		<table class="table table-striped" id="table_list">
			<thead>
			<tr class="row_title">
				<th align="center" width="20"><input type="checkbox" onclick="vnTpost.check_all();" value="checkbox" id="ch_all" name="ch_all"></th>

				<th width="15%" ><strong>Tiêu đề</strong></th>
				<th align="center"><strong>Link Save</strong></th>
				<th width="60" align="center"><strong>Xóa</strong></th>
			</tr>
			</thead>
			<tbody>

			{data.list_items}
			</tbody>
		</table>
		<input type="hidden" value="" id="do_action" name="do_action">
	</form>
</div>


<br />

{data.nav}
<br>
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">

		</div>
	</div>
</div>

<!-- END: html_save_search -->

