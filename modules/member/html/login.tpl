<!-- BEGIN: modules -->
<div id="vnt-content">
  <!--===MAIN TOP===-->
  <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: login -->
<script src="https://apis.google.com/js/api:client.js"></script>
<div id="fb-root"></div>
<script language="javascript">
    function checkform(f) {
        var username = f.username.value;
        if (username == '') {
            alert("{LANG.member.err_username_empty}");
            f.username.focus();
            return false;
        }
        if (f.password.value == '') {
            alert("{LANG.member.err_password_empty}");
            f.password.focus();
            return false;
        }
        return true;
    }
    $(document).ready(function () {
        $("#login_email").focus();
    });
</script>
<div class="boxMember">
    {data.err}
    <form id="fLogin" name="fLogin" method="post" action="{data.link_action}" onsubmit="return checkform(this)">
        <div class="formMember bg1">
            <div class="form-group">
                <div class="formFa fa-user">
                    <input type="text" name="username" id="username" class="required" placeholder="{LANG.member.username}"/>
                </div>
            </div>
            <div class="form-group">
                <div class="formFa fa-key">
                    <input type="password" name="password" id="password" class="required" placeholder="{LANG.member.password}" autocomplete="off" />
                </div>
            </div>
            <div class="checkbox">
                <label for="">
                    <input type="checkbox" name="ch_remember" id="ch_remember" value="1" checked="checked" />
                    {LANG.member.remember_login}
                </label>
            </div>
            <div class="form-group">
                <input type="hidden" name="do_login" value="1"/>
                <input type="hidden" name="ref" id="ref" value="{data.ref}"/>
                <button id="btnLogin" name="btnLogin" type="submit" value="1">
                    <span>{LANG.member.btn_login}</span>
                </button>
            </div>
        </div>
    </form>
</div>
<!-- END: login -->