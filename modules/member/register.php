<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "register";

  function sMain (){
    global $vnT, $input;
    global $vnT, $input, $func, $cart, $DB, $conf;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/" . $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->html->addScript($vnT->dir_js . "/jquery_plugins/jquery.validate.js");
    // extra title
    $vnT->conf['indextitle'] = $vnT->lang['member']['title_register'];
    if ($vnT->user['mem_id'] != 0) {
      $linkref = LINK_MOD . ".html";
      @header("Location: {$linkref}");
      echo "<meta http-equiv='refresh' content='0; url={$linkref}' />";
    }
    if($input['do']=="complete"){
			$data['main'] = $this->do_Complete();
		}else{
			$data['main'] = $this->do_Register();
		}
    $navation = get_navation($vnT->lang['member']['title_register']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    //$vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Register (){
    global $vnT, $input, $func, $cart, $DB, $conf;
    $limit = '1900:'.@date('Y');
    $vnT->html->addStyleSheet($vnT->dir_js."/datepicker/datepicker.css");
    $vnT->html->addScript($vnT->dir_js."/datepicker/datepicker.js");
    $vnT->html->addScriptDeclaration("
      $(document).ready(function(){
        $('#birthday').datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: '".$limit."',
          dateFormat: 'dd/mm/yy'
        })
      });
    ");
    if ($input['btnRegister']){
      $data = $input;
			if ($_SESSION['sec_code'] == $input['security_code']){
				$_SESSION['sec_code'] = '';
				$username = trim($input['rUserName']);
        $email = $input['rEmail'];
        $password = $func->md10($input['rPassWord']);
        $mem_group = get_group_default();
				if (strlen($username)<4) {
					$err = $vnT->func->html_err($vnT->lang['member']['err_username_invalid']); 
				}
				if (!preg_match('/^[a-z0-9-_]+$/', $username)) { 
					$err = $vnT->func->html_err($vnT->lang['member']['err_username_invalid']);       
				}
				$res_ck= $vnT->DB->query("SELECT mem_id,email,username FROM members 
                                  WHERE mem_id>0 AND (username='{$username}' OR email='{$email}' ");
				if ($row_ck = $DB->fetch_row($res_ck)){ 
					if ($row_ck['username'] == $username){					
						$err = $vnT->func->html_err($vnT->lang['member']['err_username_exist']);
					}
					if ($row_ck['email'] == $email){					 					
						$err = $vnT->func->html_err($vnT->lang['member']['err_email_exist']);
					}
				}
				$address = $input['address']; 				
        $activate_code = md5(uniqid(microtime()));
        $m_status = ($vnT->setting['active_method'] == 0) ? 1 : 0;
        $mem_point = $vnT->setting['money_default'];
        $cot = array(
  				'mem_group' => $mem_group , 
  				'username' => $username , 
  				'email' => $email , 
  				'password' => $password ,
  				'full_name' => trim($input['full_name']),
          'gender' => trim($input['gender']) ,
          'birthday' => $input['birthday'],
  				'phone' => trim($input['phone']) , 					
  				'address' => trim($input['address']) ,
  				
   				'date_join' => $func->getTimestamp() , 
  				'activate_code' => $activate_code , 
  				'newsletter' => 1 ,
          'mem_point' => $mem_point ,
          'm_status' => $m_status
				);
        if (empty($err)){
          $ok = $vnT->DB->do_insert("members", $cot);		 
          if ($ok) {
            $mem_id = $vnT->DB->insertid();
            if($mem_point){
              $cot_h['mem_id'] = $mem_id;
              $cot_h['action'] = 'register';
              $cot_h['value'] = $mem_point;
              $cot_h['reason'] = "Thưởng đăng ký thành viên";
              $cot_h['id_reason'] = $mem_id;
              $cot_h['code_reason'] = $mem_id;
              $cot_h['notes'] = "Thưởng ".$vnT->func->format_number($mem_point)." đ khi đăng ký thành viên";
              $cot_h['datesubmit'] = time();
              $vnT->DB->do_insert("money_history", $cot_h);
            }
            if ($vnT->func->is_muti_lang()) {
						  $link = ROOT_URL.$vnT->lang_name."/member/activate.html/" . $activate_code;
						}else{
						  $link = ROOT_URL."member/activate.html/" . $activate_code;
						}
            $content_email = $vnT->func->load_MailTemp("register" . $vnT->setting['active_method']);
            $qu_find = array(
              '{full_name}' , 
              '{username}' , 
              '{email}' , 
              '{realpass}' , 
              '{link}' , 
              '{host}'
            );
            $qu_replace = array(
              $input['full_name'] , 
              $username , 
              $email , 
              $input['rPassWord'] , 
              $link , 
              $_SERVER['HTTP_HOST']
            );
            $message = str_replace($qu_find, $qu_replace, $content_email);
            $subject = str_replace("{host}", $_SERVER['HTTP_HOST'], $vnT->lang['member']['subject_register']);
						if ($m_status) {
              $info['mem_id'] = $mem_id;
              $info['username'] = $username;
              $kq = $vnT->func->User_Login($info);
              if ($kq) {
								$vnT->func->vnt_set_member_cookie($info['mem_id'],0);
                $vnT->DB->query("Update members set last_login=" . time() . " , num_login=num_login+1, last_ip='" . $_SERVER['REMOTE_ADDR'] . "' where mem_id=" . $info['mem_id'] . " ");
              }
            }
						$sent = $vnT->func->doSendMail($email, $subject, $message, $vnT->conf['email']);
            $check = $vnT->DB->query("select * from listmail where email='$email'");
            if (! $vnT->DB->num_rows($check)) {
              $cot_mail['email'] = $email;
              $cot_mail['cat_id'] = 1;
              $cot_mail['datesubmit'] = time();
              $DB->do_insert("listmail", $cot_mail);
            }
            $mess_success = "mess_register_success" . $vnT->setting['active_method'];
            $mess = $vnT->lang['member'][$mess_success];
						$linkref = LINK_MOD. "/register.html/?do=complete";
						$vnT->func->header_redirect($linkref);
          } else{
            $err = $func->html_err($vnT->lang['member']['mess_register_failt'] );
					}//end if ok
        } //end if err
      } else { //end if security_code
        $err = $func->html_err($vnT->lang['member']['err_security_code_invalid']);
      }
    }
    $data['err'] = $err;
    //if (empty($data['country']))  $data['country'] = "VN";		
    //$data['list_country'] = $vnT->lib->List_Country($data['country'] );
    //$data['list_gender'] = List_Gender($data['gender'],"select");
		$sec_code = rand(100000, 999999);
		$_SESSION['sec_code'] = $sec_code;
		$scode = $vnT->func->NDK_encode($sec_code);
    $data['ver_img'] = ROOT_URL . "includes/sec_image.php?code=$scode&h=40";
    $data['TermOfRegister'] = $vnT->setting["term_of_register"];
    //$data['facebook_appId'] = $vnT->setting['social_network_setting']['facebook_appId'];
    //$data['google_appId'] = $vnT->setting['social_network_setting']['google_apikey'];
	  $data['link_login'] = LINK_MOD.'/login.html';
    $data['link_action'] = LINK_MOD . "/register.html";
    $this->skin->assign("data", $data);
    $this->skin->parse("register");
    $nd['content'] = $this->skin->text("register");
    $nd['f_title'] = '<h1>'.$vnT->lang['member']['title_register'].'</h1>';
    return $vnT->skin_box->parse_box("box_middle2", $nd);
  }
	function do_Complete (){
		global $vnT, $input, $func, $cart, $DB, $conf;
		$mess_success = "mess_register_success" . $vnT->setting['active_method']; 
		$nd['content'] ='<div class="boxMember"><div class="mess_complete">'.$vnT->lang['member'][$mess_success].'</div></div>';    
    $nd['f_title'] = '<h1>'.$vnT->lang['member']['f_register_complete'].'</h1>';
    $textout = $vnT->skin_box->parse_box("box_middle2", $nd);	
		return $textout ;
	}
  // end class
}
?>