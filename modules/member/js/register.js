﻿/* #################################### xu ly email #############################*/
var curEmail = '';
var curDomain = '';
var chkEmailErr = '';
function checkEmail_(){
	$("#rEmail_note").hide();
	
	var mailName = $("#rEmail").val();
	if(mailName == '')
		return false;

	 
	
	if(mailName == '')
	{	
				
		chkEmailErr = 0;
		hideError(this);
		$('#rEmail_wait').hide();
		return false;
	}
	
	var fullName = mailName;
	if(!CheckEmail(mailName,2,5,100)){
		chkEmailErr = 1;
		$('#rEmail_wait').hide();
		return showError('#rEmail',Email_Error[0]);
	}else{
		getEmail();			
	}
	
	chkEmailErr = 0;
		
//	hideError( $("#rEmail"));
//	return checkValid("#rEmail_icon");	
	return true;
}

$("#rEmail").blur( function() {	
	return checkEmail_();	
} );
 

/* #################################### xu ly username #############################*/
var chkAccErr = 0;
var curAcc = '';

$("#rUserName").keyup( function() {
	if(!acc_checkCharBegin(this.value)){
		chkAccErr = 1;
		return showError(this,UserName_Error[0]);
	}	
	if(!acc_checkCharContinuous(this.value)){
		chkAccErr = 1;
		return showError(this,UserName_Error[2]);
	}
	if(!acc_checkSpecialChars(this.value)){
		chkAccErr = 1;
		return showError(this,UserName_Error[3]);
	}
	if($("#rUserName_note").css('display') == 'none'){
		chkAccErr = 0;
		hideError(this);
	}	
	
} );
$("#rUserName").blur( function(){						   	
	if(!acc_checkCharEnd(this.value)){
		chkAccErr = 1;
		return showError(this,UserName_Error[1]);
	}
	if(chkAccErr==1){
		return false;
	}	
	if(!acc_checkLen(this.value)){
		return showError(this,UserName_Error[4]);	
	}	
	if(this.value!=''){
		if(checkStringIsNum(this.value)){
			chkAccErr = 1;
			return showError(this,UserName_Error[5]);
		}	
		getAccountName();
	}	
	else{
		//closeSuggest();	
	}	
	//return checkValid("#rUserName_icon");	
} );
/////////////
var chkPassErr = 0;
var chkConfirmPassErr = 0;
function resetMeta(){
	$('#meta1').removeClass('meta');	
	$('#meta2').removeClass('meta');		
	$('#meta3').removeClass('meta');		
	$('#meta4').removeClass('meta');	
}
$("#rPassWord").keyup( function() {

	$("#rConfirm_PassWord").val('');
	if(!checkVietNam(this.value)){
		chkPassErr = 1;
//		resetMeta();
		return showError(this,Password_Error[0]);
	}else{
		if($("#rPassWord_note").css('display') == 'none'){
			chkPassErr = 0;
			hideError(this);
		}
	}
	if(this.value.length < 6){
//		resetMeta();
		return false;
	}	
//	show_Meta(this.value);	
} );
$("#rPassWord").blur( function(){
	
	if(this.value == ''){
		return false;
	}	
	if(chkPassErr==1){
		$("#rConfirm_PassWord").focus();
		return false;
	}	
	if(!pass_checkLen(this.value)){
		chkPassErr=1;
		return showError(this,Password_Error[1]);	
		return false;
	}	
	chkPassErr=0;
	$("#rPassWord_tr_note").removeClass();
	$("#rPassWord_note").hide();
	//show_Meta(this.value);
	return checkValid("#rPassWord_icon");	
} );
$("#rConfirm_PassWord").keyup( function() {	
				 
	if(this.value.length == $("#rPassWord").val().length){
		if($("#rPassWord").val() == $("#rConfirm_PassWord").val()){
			chkConfirmPassErr = 0;
			hideError(this);
			return checkValid("#rConfirm_PassWord_icon");				
		}
	}
	if(this.value.length >= $("#rPassWord").val().length){
		if($("#rPassWord").val() != $("#rConfirm_PassWord").val()){
			chkConfirmPassErr = 1;
			return showError(this,Password_Error[2]);
			return false;
		}
	}
} );

$("#rConfirm_PassWord").blur( function(){
								
	if($("#rConfirm_PassWord").val()=='' || $("#rPassWord").val()=='' || chkPassErr==1)
		return false;
	if($("#rPassWord").val() != $("#rConfirm_PassWord").val()){
		chkConfirmPassErr = 1;
		return showError(this,Password_Error[2]);
		return false;
	}
	chkConfirmPassErr = 0;
	hideError(this);
	return checkValid("#rConfirm_PassWord_icon");
} );



/* #################################### xu ly password #############################*/
function show_Meta(field_pass){

	if(field_pass < 6)
		return false;
	var level = password_level(field_pass);
	if(level){
		//level 1
		if((level[0] && !level[1] && !level[2] && !level[3])
			 || (!level[0] && level[1] && !level[2] && !level[3])
			 || (!level[0] && !level[1] && level[2] && !level[3])
			 || (!level[0] && !level[1] && !level[2] && level[3])){			
			$('#meta1').addClass("meta");			
		}
		//level 2
		if((level[0] && level[1] && !level[2] && !level[3]) 
			|| (level[0] && !level[1] && level[2] && !level[3])
			|| (level[0] && !level[1] && !level[2] && level[3])
			
			|| (!level[0] && level[1] && level[2] && !level[3])
			|| (!level[0] && level[1] && !level[2] && level[3])
			
			|| (!level[0] && !level[1] && level[2] && level[3])){
			$('#meta1').addClass("meta");
			$('#meta2').addClass("meta");			
		}else{
			$('#meta2').removeClass("meta");			
		}
		//level 3			
		if((level[0] && level[1] && level[2] && !level[3]) 
			|| (level[0] && level[1] && !level[2] && level[3])
			|| (level[0] && !level[1] && level[2] && level[3])			
			|| (!level[0] && level[1] && level[2] && level[3])){
			$('#meta1').addClass("meta");
			$('#meta2').addClass("meta");		
			$('#meta3').addClass("meta");			
		}
		else{
			$('#meta3').removeClass("meta");			
		}
		//level 4			
		if(level[0] && level[1] && level[2] && level[3]){
			$('#meta1').addClass("meta");
			$('#meta2').addClass("meta");		
			$('#meta3').addClass("meta");		
			$('#meta4').addClass("meta");			
		}else{
			$('#meta4').removeClass("meta");
		}	
	}
}


$("#full_name").blur( function(){
								
	if($("#full_name").val()=='')
	{
		return showError(this,Fullname_Error);	
		return false;
	}
	hideError(this);
	return checkValid("#full_name_icon");
} );