<?php
// no direct access
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
//news
if (strstr($_SERVER['REQUEST_URI'], "member.html")) {
  $_GET[$vnT->conf['cmd']] = "mod:member";
  $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member";
}
if (in_array('member', $vnT->url_array)) {
  $pos = array_search('member', $vnT->url_array);
  //		print_r($vnT->url_array);
  $mod = "member";
  $act = $vnT->url_array[$pos + 1];
  //login
  if ($act == "login" || $act == "login.html") {
    $url = $vnT->url_array[$pos + 2];
    if ($url) {
      $extra_link = "|url:" . $url;
    }
    $_GET[$vnT->conf['cmd']] = "mod:member|act:login" . $extra_link;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:login" . $extra_link;
  }
	

  //logout
  if ($act == "logout" || $act == "logout.html") {
    $url = $vnT->url_array[$pos + 2];
    if ($url) {
      $extra_link = "|url:" . $url;
    }
    $_GET[$vnT->conf['cmd']] = "mod:member|act:logout" . $extra_link;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:logout" . $extra_link;
  }
  //forget_pass
  if ($act == "forget_pass" || $act == "forget_pass.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:forget_pass";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:forget_pass";
  }
	
  //update_account
  if ($act == "update_account" || $act == "update_account.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:update_account";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:update_account";
  }
	//upgrade_account
  if ($act == "upgrade" || $act == "upgrade.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:upgrade";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:upgrade";
  }
  //unsubscribe
  if ($act == "unsubscribe" || $act == "unsubscribe.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:unsubscribe";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:unsubscribe";
  }
  //changepass
  if ($act == "changepass" || $act == "changepass.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:changepass";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:changepass";
  }
  //reset_pass
  if ($act == "reset_pass" || $act == "reset_pass.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:reset_pass";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:reset_pass";
  }


  //activate
  if ($act == "activate" || $act == "activate.html") {
    $code = $vnT->url_array[$pos + 2];
    if ($code) {
      $extra_link = "|code:" . $code;
    }
    $_GET[$vnT->conf['cmd']] = "mod:member|do:activate" . $extra_link;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:activate" . $extra_link;
  }
  //send_code
  if ($act == "send_code" || $act == "send_code.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:send_code";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:send_code";
  }
  //your_order
  if ($act == "your_order" || $act == "your_order.html") {
    $extra = $vnT->url_array[$pos + 2];
    if ($extra)
      $extra_link = "|" . $extra;
    $_GET[$vnT->conf['cmd']] = "mod:member|act:your_order" . $extra_link;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:your_order" . $extra_link;
  }
  //register
  if ($act == "register" || $act == "register.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|act:register";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:register";
  }

    
		
	//mailbox
	if( $act =="mailbox" || $act =="mailbox.html" )
	{
		$extra = $vnT->url_array[$pos+2] ;
		if($extra) $extra_link = "|".$extra;
		$_GET[$vnT->conf['cmd']] = "mod:member|act:mailbox".$extra_link;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:member|do:mailbox".$extra_link;
	} 

  //money
  if ( $act == "money.html") {    
    $_GET[$vnT->conf['cmd']] = "mod:member|act:money" ;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:money" ;		
  }
	if ($act == "money") {     
		$doAct = str_replace(".html","",$vnT->url_array[$pos+2]) ;
		$_GET[$vnT->conf['cmd']] = "mod:member|act:money|do:".$doAct;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:member|act:money|do:".$doAct;
  }

	//api_changepass
  if ($act == "api_changepass" || $act == "api_changepass.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|do:api_changepass";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|do:api_changepass";
  }
	//openidlogon
  if ($act == "openidlogon" || $act == "openidlogon.html") {
    $_GET[$vnT->conf['cmd']] = "mod:member|act:openidlogon";
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:openidlogon";
  }
  if($act=="popup" || $act == "popup.html")
  {
    $popupName = str_replace(".html","",$vnT->url_array[$pos+2]);
    $_GET[$vnT->conf['cmd']] = "mod:member|act:popup|do:".$popupName ;
    $QUERY_STRING = $vnT->conf['cmd'] . "=mod:member|act:popup|do:".$popupName ;
  }
	//ajax
	if($vnT->url_array[$pos+1]=="ajax" || $vnT->url_array[$pos+1]=="ajax.html")	
	{		
		$ajaxName = str_replace(".html","",$vnT->url_array[$pos+2]);
		$_GET[$vnT->conf['cmd']] = "mod:$mod|act:ajax|do:".$ajaxName ;
		$QUERY_STRING = $vnT->conf['cmd']."=mod:$mod|act:ajax|do:".$ajaxName ;
	}
	
 
} //end news
?>