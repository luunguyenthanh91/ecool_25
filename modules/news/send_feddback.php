<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../_config.php");
require_once ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//load mailer
require_once ($conf['rootpath'] . "libraries/phpmailer/phpmailer.php");
$vnT->mailer = new PHPMailer();
require_once ($conf['rootpath'] . "includes/class_functions.php");
$func = new Func_Global();
$conf = $func->fetchDbConfig($conf);
$vnT->conf = $conf;
$vnT->lang_name = ($_GET['lang']) ? $_GET['lang'] : "vn";
$linkMod = "?" . $conf['cmd'] . "=mod:news";
$func->load_language("news");
$ok = 0;
$mess = "";
if (! empty($_GET["newsid"]) && is_numeric($_GET["newsid"])) {
  $news_id = $_GET["newsid"];
  $sql = "select * from news n, news_desc nd  WHERE n.newsid=nd.newsid AND display=1 AND lang='$vnT->lang_name'	AND n.newsid ={$news_id}";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $title_news = $row['title'];
    $url = $conf['rooturl'] . "news/detail/" . $news_id . "/" . $func->make_url($row['title']) . ".html";
  }
}
$url = $conf['rooturl'];
$data = $_POST;
$data['title_news'] = "<a href=\"{$url}\">" . $title_news . "</a>";
if (empty($data["title"]))
  $data['title'] = $title_news;
if (isset($_POST['btnSend'])) {
  $content_email = $func->load_MailTemp("send_feedback");
  $qu_find = array(
    '{name}' , 
    '{content}' , 
    '{url}');
  $qu_replace = array(
    $_POST['name'] , 
    $_POST['content'] , 
    $url);
  $message = str_replace($qu_find, $qu_replace, $content_email);
  $subject = $_POST['title'];
  $sent = $func->doSendMail($conf['email'], $subject, $message, $_POST["email"]);
  $mess = 'Thông tin liên lạc của bạn đã được gửi đi';
  //end send
  $mess = "Góp ý của bạn đã được gửi đi";
  $ok = 1;
}
mt_srand((double) microtime() * 1000000);
$num = mt_rand(100000, 999999);
$scode = $func->NDK_encode($num);
$img_code = "../../includes/sec_image.php?code=$scode";
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/popup.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function getRefToDivMod( divID, oDoc ) {
  if( !oDoc ) { oDoc = document; }
  if( document.layers ) {
    if( oDoc.layers[divID] ) { return oDoc.layers[divID]; } else {
      for( var x = 0, y; !y && x < oDoc.layers.length; x++ ) {
        y = getRefToDivMod(divID,oDoc.layers[x].document); }
      return y; } }
  if( document.getElementById ) { return oDoc.getElementById(divID); }
  if( document.all ) { return oDoc.all[divID]; }
  return document[divID];
}
function resizeWinTo( idOfDiv ) {
  var oH = getRefToDivMod( idOfDiv ); if( !oH ) { return false; }
  var oW = oH.clip ? oH.clip.width : oH.offsetWidth;
  var oH = oH.clip ? oH.clip.height : oH.offsetHeight; if( !oH ) { return false; }
  var x = window; x.resizeTo( oW + 200, oH + 200 );
  var myW = 0, myH = 0, d = x.document.documentElement, b = x.document.body;
  if( x.innerWidth ) { myW = x.innerWidth; myH = x.innerHeight; }
  else if( d && d.clientWidth ) { myW = d.clientWidth; myH = d.clientHeight; }
  else if( b && b.clientWidth ) { myW = b.clientWidth; myH = b.clientHeight; }
  if( window.opera && !document.childNodes ) { myW += 16; }
  	var myw = oW + ( ( oW + 200 ) - myW )+(5);
	var myh = oH + ( (oH + 200 ) - myH )+(5*2);
	if(myw > screen.availWidth){
		myw = screen.availWidth;
	}
	if(myh > screen.availHeight){
		myh = screen.availHeight;
	} 
  x.resizeTo( myw, myh );
  var scW = screen.availWidth ? screen.availWidth : screen.width;
  var scH = screen.availHeight ? screen.availHeight : screen.height;
  x.moveTo(Math.round((scW-myw)/2),Math.round((scH-myh)/2));
}
// -->
</script>
<script language=javascript>
	function checkform(f) {			
		var name = f.name.value;
		if (name == '') {
			alert('Pls enter name');
			f.name.focus();
			return false;
		}
			
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		email = f.email.value;
		if (email == '') {
			alert('Pls enter Email');
			f.email.focus();
			return false;
		}
		if (email != '' && email.match(re)==null) {
			alert(' Email Invalid');
			f.email.focus();
			return false;
		}
		
		
		var title = f.title.value;
		if (title == '') {
			alert('Pls enter your title');
			f.title.focus();
			return false;
		}
		
		if (f.h_code.value != f.security_code.value ) {
			alert('Mã bảo vệ không chính xác');
			f.security_code.focus();
			return false;
		}

		return true;
	}
</script>
<body onLoad="resizeWinTo('pcontainer');">
<div id="pcontainer" style="margin: 5px">

<?php
if ($ok == 0) {
  ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="30" class="font_f_title"><img src="images/mail_send.gif"
			align="absmiddle" />&nbsp;GÓP Ý BÀI VIẾT</td>
	</tr>

	<tr>
		<td bgcolor="#FF0000" height="2"></td>
	</tr>
</table>

<table width="100%" border="0" align="center" cellpadding="0"
	cellspacing="0">
	<tr>
		<td align="center" height="30" class="font_title_news">Bài viết : <strong><?=$data['title_news']?></strong></td>
	</tr>
	<tr>
		<td> <?=$mess?></td>
	</tr>
	<tr>
		<td align="left">
		<form action="" method="post" name="contact" id="contact"
			onSubmit="return checkform(this);">
		<table width="100%" border="0" align="center" cellpadding="2"
			cellspacing="2">
			<tr>
				<td width="30%"><strong>Tên của bạn </strong> <font color="red">(*)</font>
				:</td>
				<td width="70%"><input class="textfiled" name="name" type="text"
					id="name" size="40" maxlength="250" value="<?=$data['name']?>" /></td>
			</tr>
			<tr>
				<td><strong>Email của bạn</strong> <font color="red">(*)</font> :</td>
				<td><input class="textfiled" name="email" type="text" id="email"
					size="40" maxlength="250" value="<?=$data['email']?>" /></td>
			</tr>
			<tr>
				<td valign="top"><strong>Tiêu đề</strong> <font color="red">(*)</font>:
				</td>
				<td><input class="textfiled" name="title" type="text" size="40"
					maxlength="250" value="<?=$data['title']?>" /></td>
			</tr>
			<tr>
				<td valign="top"><strong>Nội dung góp ý</strong> <font color="red">(*)</font>
				:</td>
				<td><textarea class="textarea" name="content" cols="40" rows="7"
					id="content"><?=$data['content']?></textarea></td>
			</tr>
			<tr>
				<td><strong>Mã bảo vệ :</strong> <font color="red">(*)</font></td>
				<td><input id="security_code" name="security_code" size="15"
					maxlength="6" class="textfiled" />&nbsp;<img
					src="<?php
  echo $img_code;
  ?>" align="absmiddle" /></td>
			</tr>

			<tr align="center">
				<td colspan="2"><br />
				<input type="hidden" name="h_code" value="<?php
  echo $num;
  ?>"> <input
					type="submit" name="btnSend" value="  Gửi  " class="button" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset" name="Reset"
					value=" Hủy bỏ " class="button" /></td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
</table>
<?php
} else {
  ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0"
	height="100%">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="3" cellpadding="3">
			<tr>
				<td align="center"><?
  echo $func->html_mess($mess);
  ?></td>
			</tr>
			<tr>
				<td align="center" height="30"><a href="javascript:window.close()"><strong><font
					color="#990000">[Close Window]</font></strong></a>
				</div>
				</td>
			</tr>
		</table>

		</td>
	</tr>
</table>

<?php
}
?>

</div>
</body>

</html>