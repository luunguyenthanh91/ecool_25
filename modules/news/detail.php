<?php
/*================================================================================*\
|| 							Name code : news.php	 		 																		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Access denied');
}
$nts = new sMain();

class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "detail";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . "/" . $this->module . "/html/detail.tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$this->skin->assign('DIR_JS', $vnT->dir_js);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
		$vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
    $newsid = (int) $input['itemID'];
	  //check
		if($_GET['preview']==1 && $_SESSION['admin_session'])	{
			$where = " ";
		}else{
			$where = " AND display=1 ";
		}
    $res_ck= $vnT->DB->query("SELECT * FROM news n, news_desc nd
															WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND n.newsid=$newsid $where ");
    if ($row = $vnT->DB->fetch_row($res_ck)){
      $vnT->DB->query("UPDATE news SET viewnum=viewnum+1 where newsid = $newsid");
			//SEO
			if ($row['metadesc'])	$vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey'])	$vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title'])	$vnT->conf['indextitle'] = $row['friendly_title'];
			$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
			$link_seo .= $row['friendly_url'].".html";
			$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />';
			$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
			$vnT->setting["meta_social_network"] .= "\n".'<meta itemprop="headline" content="'.$vnT->conf['indextitle'].'"/>';
			$vnT->setting["meta_social_network"].="\n".'<meta property="og:url" itemprop="url" content="'.$link_seo.'"/>';
			$vnT->setting["meta_social_network"] .= "\n".'<meta property="og:type" content="article" />';
			$vnT->setting["meta_social_network"] .= "\n".'<meta property="og:title" name="title" itemprop="name" content="'.$vnT->conf['indextitle'].'"/>';
			$vnT->setting['meta_social_network'].= "\n".'<meta property="og:description" itemprop="description" name="description" content="'.$vnT->conf['meta_description'].'" />';
			if($vnT->muti_lang>0){
				$res_lang = $vnT->DB->query("SELECT friendly_url,lang FROM news_desc WHERE newsid=".$row['newsid']." AND display=1 ")	;
				while ($row_lang = $vnT->DB->fetch_row($res_lang)){
					$link_lang = create_link ("detail",$row['newsid'],$row_lang['friendly_url']);
					$vnT->link_lang[$row_lang['lang']]=str_replace($vnT->link_root,ROOT_URI.$row_lang['lang']."/",$link_lang);
				}
			}
			$input['catID'] = (int) $row['cat_id'];
			$row['link_share'] = $link_seo;
      $data['main'] = $this->Detail($row);
    } else {
      $mess = $vnT->lang['news']['not_found_news'].' ID <strong>'.$newsid.'</strong>';
      $url = LINK_MOD . ".html";
      $vnT->func->html_redirect($url, $mess);
    }
		$navation = get_navation($input['catID'],$vnT->conf['indextitle']);
    
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function Detail ($info) {
    global $input, $vnT, $conf;
		$data = $info;
    $n_other = (! empty($vnT->setting['other_detail'])) ? $vnT->setting['other_detail'] : 10;
    $text = "";
		$newsid = (int)$info['newsid'];
		$cat_id = (int)$info['cat_id'];
		if ($info['permesion_view'] == 1 && $vnT->user['mem_id'] == 0) {
			$mess = $vnT->lang['news']['only_member'];
			$url = LINK_MOD . ".html";
			$vnT->func->html_redirect($url, $mess);
		}
		if ($info['picture']) {
			$data['pic'] = get_pic_news_detail($info['picture'], $info['picturedes'], $vnT->setting['imgdetail_width']);
			$social_network_picture = ROOT_URL."vnt_upload/news/".$info['picture'];
			$vnT->setting['meta_social_network'] .= "\n".'<link rel="image_src" href="'.$social_network_picture.'" / >';
			$vnT->setting['meta_social_network'] .= "\n".'<meta property="og:image" itemprop="thumbnailUrl" content="'.$social_network_picture.'"  />';
			$vnT->setting['meta_social_network'] .= "\n".'<meta itemprop="image" content="'.$social_network_picture.'">';
		}
		$data['title'] = $vnT->func->HTML($info['title']);
		//$data["date_post"] = @date('d/m/Y, H:i',$info["date_post"]). ' GMT+7';
    $data['link'] = ROOT_URL.create_link("detail", $info['newsid'], $info['friendly_url']);
    $data['date_post'] = get_datePost($info['date_post']);
		$data['content_news'] = $info['content'];
    if($info['tags']){
      $data['list_tags']= '<div class="myTags"><p>Tag : </p><ul class="list-inline text-left">'.$vnT->lib->get_list_tags($this->module,$info['tags']).'</ul></div>';
    }
    $link_share = $info['link_share'];
		$list_social_network = '<ul>';
    if($vnT->setting['social_network_share']){
      $arr_share = @explode(",",$vnT->setting['social_network_share']);
      foreach ($arr_share as $val){
        $list_social_network .= '<li>'.$vnT->lib->get_icon_share($val,$link_share).'</li>';
      }
    }
		if($vnT->setting['social_network_like']){
			$arr_share = @explode(",",$vnT->setting['social_network_like']);
			foreach ($arr_share as $val){
				$list_social_network .= '<li>'.$vnT->lib->get_icon_like($val,$link_share).'</li>';
			}
		}
    $list_social_network .='</ul>';
		$data['list_social_network'] = $list_social_network;
		//facebook_comment
		if ($vnT->setting['facebook_comment']){
			$data['facebook_comment']	= '<div class="facebook-comment" style="padding-top:10px;"><div class="fb-comments" data-href="'.$link_share.'" data-width="100%" data-num-posts="5" data-colorscheme="light"></div><div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId='.$vnT->setting['social_network_setting']['facebook_appId'].'"; fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script></div>' ;
		}
    if ($vnT->setting['print']) {
      $link_print =  LINK_MOD . "/print/{$newsid}/".$info['friendly_url'].".html";
      $data['print'] = "<a href='javascript:void();'' onClick=\"javascript:openPopUp('".$link_print."','Print',750,600, 'Yes')\" class='fa-print'>".$vnT->lang['news']['print_new']. "</a>";
    }
    // if ($vnT->setting['sendmail']) {
    //   $link_send_mail= DIR_MOD."/send_news.php?id=".$newsid."&lang={$vnT->lang_name}&TB_iframe=true&width=550&height=500";
    //   $data['tellfriend'] = "<a href=\"{$link_send_mail}\" class='thickbox font-icon fa-envelope'>" . $vnT->lang['news']['send_news'] . "</a>";
    // }
    if ($vnT->setting['comment']) {
			$data['comment'] = $this->do_Comment($info);
    }
    $w_other = get_where_cat($cat_id) . " AND n.newsid<>$newsid ";
    $data['other_news'] = news_other($w_other, 0, $n_other);
    $data['box_sidebar'] = box_sidebar();
    $this->skin->reset("html_detail1");
    $this->skin->assign("data", $data);
    $this->skin->parse("html_detail1");
    return $this->skin->text("html_detail1");
  }
  function do_Comment($info){
    global $DB, $func, $input, $vnT;
    $vnT->html->addScript($vnT->dir_js . "/jquery_alerts/jquery.alerts.js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/jquery_alerts/jquery.alerts.css");
    $newsid = (int)$info['newsid'];
    //check comment
    $data['list_comment'] = ' <div id="ext_comment" class="grid-comment">';
    $data['list_comment'] .= "<script language=\"JavaScript\"> vnTcomment.show_comment('".$newsid."','".$vnT->lang_name."','0'); </script>";
    $data['list_comment'] .= '</div>';
    $data['mem_id'] = (int)$vnT->user['mem_id'];
    $data['com_name'] = '';
    $data['com_email'] = '';
    if ($vnT->user['mem_id'] > 0) {
      $data['com_name'] = $vnT->user['full_name'];
      $data['com_email'] = $vnT->user['email'];
    }
    $sec_code = rand(100000, 999999);
    $_SESSION['sec_code'] = $sec_code;
    $data['code_num'] = $sec_code;
    $scode = $vnT->func->NDK_encode($sec_code);
    $data['ver_img'] = ROOT_URI . "includes/sec_image.php?code=$scode&h=38";
    $data['newsid'] = $info['newsid'];
    $data['lang'] = $vnT->lang_name;
    $data['mem_id'] = $vnT->user['mem_id'];
    $data['link_login'] = ROOT_URI . "member/login.html/" . $vnT->func->NDK_encode($vnT->seo_url);
    $data['err'] = $err;
    $data['link_action'] = $link;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_comment");
    $textout = $this->skin->text("html_comment");
    return $textout;
  }
}
?>
