<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

$conf = $func->fetchDbConfig("config", $conf);
switch ($_GET['do']) {
  case "product":
    $aResults = get_product();
  break;
  default:
    $aResults = array();
  break;
}

//get_customer
function get_product ()
{
  global $DB, $func, $conf, $vnT;
  $textout = array();
  $lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
  $keyword = $_GET['input'];
  $sql = "SELECT *
						FROM products p, products_desc pd
						WHERE p.p_id=pd.p_id 
						AND pd.lang='$lang'
						AND p.display=1
						AND p_name like '%" . $keyword . "%'
						ORDER BY p_name ASC, p.p_id DESC ";
  $res = $DB->query($sql);
  if ($num = $DB->num_rows($res)) {
    while ($row = $DB->fetch_row($res)) {
      $textout[] = array(
        'id' => $row['p_id'] , 
        'value' => $row['p_name'] , 
        'info' => "ID : " . $row['p_id']);
    }
  }
  return $textout;
}
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0
if (isset($_REQUEST['json'])) {
  header("Content-Type: application/json");
  echo "{\"results\": [";
  $arr = array();
  for ($i = 0; $i < count($aResults); $i ++) {
    $arr[] = "{\"id\": \"" . $aResults[$i]['id'] . "\", \"value\": \"" . $aResults[$i]['value'] . "\", \"info\": \"" . $aResults[$i]['info'] . "\"}";
  }
  echo implode(", ", $arr);
  echo "]}";
} else {
  header("Content-Type: text/xml");
  echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?><results>";
  for ($i = 0; $i < count($aResults); $i ++) {
    echo "<rs id=\"" . $aResults[$i]['id'] . "\" info=\"" . $aResults[$i]['info'] . "\">" . $aResults[$i]['value'] . "</rs>";
  }
  echo "</results>";
}
?>
