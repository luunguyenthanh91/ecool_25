<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $act = "search";
  
  /**
   * function sMain ()
   * Khoi tao 
   **/
  function sMain ()
  {
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");   

		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
 
	 		$(function() {
				$('#date_begin').datepicker({
					showOn: 'button',
					buttonImage: '".MOD_DIR_IMAGE."/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				});
				$('#date_end').datepicker({
					showOn: 'button',
					buttonImage: '".MOD_DIR_IMAGE."/calendar.gif',
					buttonImageOnly: true,
					changeMonth: true,
					changeYear: true
				});

			});
		
		");
		
    
		$data['main'] = $this->do_Search();
	  $data['box_sidebar'] = box_sidebar(); 
		$data['navation']	=	 '<a href="'.$vnT->link_root.'" >'.$vnT->lang['global']['homepage'].'</a> <span>&nbsp;</span> ' . "<a href='" . LINK_MOD . ".html'>" . $vnT->lang['news']['news'] . "</a> <span>&nbsp;</span>".$vnT->lang['news']['f_search'] ;
 		 
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  
  }
	

	
  
  /**
   * function list_product 
   * 
   **/
  function do_Search ()
  {
    global $DB, $func, $input, $vnT;
		
		$p = ((int) $input['p']) ? (int) $input['p'] : 1;
		$view =2 ;
		$where="";
		$data = $input ;
		
		$ext_pag = "?do_search=1";
		$keyword = $input['keyword'];		
		$arr_where = get_str_where ($keyword,$input) ;
		$result_keyword = $arr_where['result_keyword'];		
		$where = $arr_where['where'] ;
		$ext_pag .= $arr_where['ext'] ;
	
 		
		$sql_num =   "SELECT n.newsid
									FROM news n , news_desc nd
									WHERE n.newsid=nd.newsid
									AND lang='$vnT->lang_name'
									AND display=1 
									$where ";
    //echo "<br>".$sql_num;
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		
		$n = (! empty($vnT->setting['n_cat'])) ? $vnT->setting['n_cat'] : 10;
			
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		
		if($num_pages>1) {
			
			$root_link = LINK_MOD."/search.html" ;	
			$nav = "<div class=\"pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
		
  	$sql = "SELECT * 
						FROM news n , news_desc nd
						WHERE n.newsid=nd.newsid
						AND  lang='$vnT->lang_name'
						AND display=1
						$where  
						ORDER BY display_order DESC, date_post DESC
						LIMIT $start,$n";
    //echo $sqlnews;
    $result = $vnT->DB->query($sql);
    if ($num = $vnT->DB->num_rows($result))
    {
      $i = 0;
      $list_news = '';
      while ($row = $vnT->DB->fetch_row($result))
      {
        $i ++;
        
        $row['result_keyword'] = $result_keyword;
				
				$list_news .= '<div class="boxNews">';
				$list_news .= row_search($row);
				if ($i < $num) {
					$list_news .= '<br class="clear">
					<div class="hr" >&nbsp;</div>';
				} else {
					$list_news .= '<br class="clear">';
				}
				$list_news .= '</div>';
			 
      }
			
			$data['list_news'] = $list_news ;
			$data['nav'] = $nav ;
    
		} else  {
      $data['list_news'] ='<p align="center" class="font_err">'.$vnT->lang['news']['no_have_result'].'</p>';
    }
		
			
		$str_keyword.="<span class=\"font_keyword\">".$result_keyword."</span>" ;
 		$data['note_keyword'] = str_replace("{keyword}",$str_keyword,$vnT->lang['news']['note_keyword']);  
		
		$note_result = str_replace("{totals}","<b class=font_err>".$totals."</b>",$vnT->lang['news']['note_result']); 
		$data['note_result'] = str_replace("{num_pages}","<b>".$num_pages."</b>",$note_result);
 		
		$data['list_cat'] = List_Cat_News ($input['cat_id']);
 		
		$this->skin->assign("data", $data);
    $this->skin->parse("html_search");
		$nd['content'] = $this->skin->text("html_search");
		$nd['f_title'] = $vnT->lang['news']['f_search'];	

    //$nd['more'] = "<span class='date_news'>".$vnT->lib->box_ngaythang()."</span>&nbsp;";
    $textout = $vnT->skin_box->parse_box("box_middle", $nd);
 		
		
    return $textout;
		
  }
  

  
// end class
}
?>