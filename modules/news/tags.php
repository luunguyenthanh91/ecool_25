<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $act = "tags";
  
  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		$vnT->setting['menu_active'] = $this->module;
		$tagID = $input['tagID'];		
		$res_ck = $vnT->DB->query("SELECT * FROM ".$this->module."_tags WHERE tag_id=".$tagID." AND display=1");
		if($row = $vnT->DB->fetch_row($res_ck)){
			$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL ;
			$link_seo .= "tag/".$row['tag_id'].$vnT->func->make_url($row['name']).".html";			
			//SEO
			$vnT->conf['meta_extra'] .= "\n".'<link rel="canonical" href="'.$link_seo.'" />'; 
			$vnT->conf['meta_extra'] .= "\n". '<link rel="alternate" media="handheld" href="'.$link_seo.'"/>';
			$vnT->setting["meta_social_network"] .= "\n".'<meta itemprop="headline" content="'.$vnT->conf['indextitle'].'"/>';
			$vnT->setting["meta_social_network"].="\n".'<meta property="og:url" itemprop="url" content="'.$link_seo.'"/>';
			$vnT->setting["meta_social_network"] .= "\n".'<meta property="og:type" content="article" />';			 
			$vnT->setting["meta_social_network"] .= "\n".'<meta property="og:title" name="title" itemprop="name" content="'.$vnT->conf['indextitle'].'" />';
			$vnT->setting['meta_social_network'].= "\n".'<meta property="og:description" itemprop="description" name="description" content="'.$vnT->conf['meta_description'].'"/>';
			$vnT->conf['indextitle'] = $row['name'];
			$data['main'] = $this->do_Tags($row);
		}else{
			$vnT->func->header_redirect(LINK_MOD.".html")	;
		}
    $data['box_sidebar'] = box_sidebar();
		$navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function do_Tags ($info){
    global $DB, $func, $input, $vnT;
		$p = ((int) $input['p']) ? (int) $input['p'] : 1;
		$view =1;
		$ext_pag = "";
		$tagID = $info['tag_id'];
		$where = " and FIND_IN_SET('$tagID',tags)<>0 ";
		$sql_num = "SELECT n.newsid FROM news n , news_desc nd
								WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name' AND display=1 $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
		$n = (! empty($vnT->setting['n_cat'])) ? $vnT->setting['n_cat'] : 10;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1) {
			$root_link = LINK_MOD."/tags/".$tagID;
			$nav = "<div class=\"pagination\">".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>";
		}
    $num_row = 3;
		$view = 2;
		$data['list_news'] = row_news($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
		$str_keyword.="<span class=\"font_keyword\">".$result_keyword."</span>" ;
 		$data['note_keyword'] = str_replace("{keyword}",$str_keyword,$vnT->lang['news']['note_keyword']);
		$note_result = str_replace("{totals}","<b class=font_err>".$totals."</b>",$vnT->lang['news']['note_result']); 
		$data['note_result'] = str_replace("{num_pages}","<b>".$num_pages."</b>",$note_result);
		$this->skin->assign("data", $data);
    $this->skin->parse("html_tags");
		$nd['content'] = $this->skin->text("html_tags");
		$nd['f_title'] = '<h1>' . $vnT->lang["news"]["f_tags"].": ".$vnT->func->HTML($info['name']) . '</h1>';
    $textout = $vnT->skin_box->parse_box("box_middle", $nd);
    return $textout;
  }
// end class
}
?>