<?php
/*================================================================================*\
|| 							Name code : function_news.php 		 		 												  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 17/12/2007 by Thai Son
 **/
 
 
if (! defined('IN_vnT')) {
  die('Access denied');
}


 /**
 * @function : box_search  
 * @param 		:  null
 * @return		:  text
 */

function box_search ()
{
	global $vnT, $input ,$DB, $func;
	$text_out = "";
	
	$data['keyword'] = ($input['keyword']) ? $input['keyword'] : $vnT->lang['news']['keyword_default'];
	$data['link_search']  = LINK_MOD."/search/";
	$text_out = load_html("box_search",$data);
	return $text_out ;
}


 
 /**
 * @function : box_best_view
 * @param 		:  null
 * @return		:  text
 */
function box_best_view ()
{
  global $vnT, $func, $DB, $conf,$input;
  $textout = "";
	
	$cat_id = (int)$input['catID'];
	if($cat_id){
		$where = get_where_cat($cat_id);
 	}else{
		$where = '';
	}
	
   $result = $DB->query("SELECT *
											FROM news n , news_desc nd
											WHERE n.newsid=nd.newsid
											AND display=1  
											AND lang='$vnT->lang_name'
											{$where}
											ORDER BY viewnum DESC, date_post DESC LIMIT 0,10 ");
  if ($num = $DB->num_rows($result)) {
    $i = 0;
		
    $text = "<div class='sidebar-news'><ul>" ;
		while ($row = $DB->fetch_row($result)) {
      $i ++;
      $link = create_link("detail", $row['newsid'], $row['friendly_url'] );
      
			$w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
			
			$title = $vnT->func->HTML($row['title']);
			$short = $vnT->func->cut_string($vnT->func->check_html($row['short'],'nohtml'),200,1);
			$pic = ($row['picture']) ? $pic = get_pic_news($row['picture'],$w,'class=img') : "" ;
			
			$tooltip = '<table width=100% border=0 cellspacing=2 cellpadding=2><tr><td ><b class=tooltipTitle>' . $vnT->func->HTML($row['title']) . '</b></td></tr><tr><td class="tooltipDes">' . $pic . '' . $short . '</td></tr></table>';
			$tooltip = $vnT->func->txt_tooltip($tooltip);
		
			$last = ($i==$num) ? "class='last'" : "";
			$text .="<li ".$last." ><div><a href='".$link."' onmouseout=\" hideddrivetip();\" onmouseover=\"ddrivetip('" . $tooltip . "',350);\"   >".$title."</a></div></li>";
			 
    }
		$text .= '</ul></div> ';
		
		$nd['f_title']=$vnT->lang['news']['best_view'];
		$nd['content'] = $text ;
		$textout .=  $vnT->skin_box->parse_box("box_right",$nd);
   }
  
  return $textout ;
}

/**
 * @function : box_focus_news  
 * @param 		:  null
 * @return		:  text
 */
function box_focus_news (){
  global $vnT, $func, $DB, $conf,$input;
  $textout = "";
	$where = ' AND focus_main=1 ORDER BY focus_order DESC, date_post DESC';
  $result=$DB->query("SELECT * FROM news n , news_desc nd
											WHERE n.newsid=nd.newsid AND lang='$vnT->lang_name'
											AND display=1 AND focus_main=1 											
											{$where} LIMIT 0,10 ");
  if ($num = $DB->num_rows($result)) {
    $i = 0;
		$w = ($vnT->setting['imgblock_width']) ? $vnT->setting['imgblock_width'] : 90;
    $text .= '<div id="bn_slider">';
		while ($row = $DB->fetch_row($result)) {
      $i ++;
      $link = create_link("detail", $row['newsid'], $row['friendly_url'] );
			$title = $vnT->func->cut_string($vnT->func->HTML($row['title']),60,1);
			$picture = ($row['picture']) ? "news/".$row['picture'] : "news/".$vnT->setting['pic_nophoto'];
  		$src = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
      $text .= '<div class="item">
                  <div class="i-image">
                    <a href="'.$link.'" title="'.$title.'"><img src="'.$src.'" alt="'.$title.'"></a>
                  </div>
                  <div class="i-title"><a href="'.$link.'" title="'.$title.'">'.$title.'</a></div>
                  <div class="clear"></div>
                </div>';
    }
    $text .= '</div>';
		$nd['f_title']=$vnT->lang['news']['focus_news'];
		$nd['content'] = $text;
		$textout = $vnT->skin_box->parse_box("box",$nd); 
 	}
  return $textout;
}
/*----------- box_category --------------------*/
function box_category ()
{
  global $func, $DB, $conf, $vnT, $input;
	 
  // lay ROOT cat
  $sql = "select  *
					from news_category n, news_category_desc nd
					where n.cat_id=nd.cat_id
					AND lang='$vnT->lang_name' 
					and  parentid=0 
					and  display=1
					order by  cat_order DESC , date_post DESC";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $cat_cur =  $input["catID"];
    $text = '<div class="box_category"><ul >';
		
		 
    $i = 0;
    while ($row = $DB->fetch_row($result)) {
      $i ++;
      $cat_id = $row['cat_id'];
      $active = ($row['cat_id']==$input['catID']) ?  "class='current'" : "" ;
      $class = ($i == $num) ? " class ='last' " : "";
      $link = create_link("category", $cat_id, $row['friendly_url']);
      $text .= "<li {$class} ><a href='{$link}' {$active}  ><span  >" . $vnT->func->HTML($row['cat_name']) . "</span></a>";      
      $text .= "</li>";
    }
    $text .= '</ul></div>';
		
    
		$nd['content'] = $text;
    $nd['f_title'] = $vnT->lang['news']['cat_news'] ;
    $textout = $vnT->skin_box->parse_box("box", $nd);
    return $textout ;
		
  } else {
    return '';
  }
}

//======== get_catid ==============
function get_catid ($id)
{
  global $DB, $vnT;
  $res = $DB->query("SELECT cat_id FROM news WHERE newsid ={$id}");
  if ($row = $DB->fetch_row($res)) {
    return (int) $row['cat_id'];
  } else {
    return 0;
  }
}

//========Check Sub===============
function Check_Sub ($cid)
{
  global $DB, $vnT;
  $query = $DB->query("SELECT * FROM news_category WHERE parentid ={$cid}");
  if ($scat = $DB->fetch_row($query))
    return 1;
  else
    return 0;
}

//=========Get Child ============
function Get_Child ($cid)
{
  global $DB, $func, $input, $conf, $vnT;
  $sql = "select * from news_category where parentid = '{$cid}'";
  $query = $DB->query($sql);
  while ($row = $DB->fetch_row($query)) {
    $text[] = $row['cat_id'];
    $res_sub = $DB->query("select cat_id,parentid from news_category where parentid=" . $row['cat_id']);
    while ($row_sub = $DB->fetch_row($res_sub)) {
      $text[] = $row_sub['cat_id'];
    }
  }
  return $text;
} 
 

  
?>