<!-- BEGIN: block_news -->
<div id="tab-news">
	<ul id="tab-news-nav" class="tab_nav">
  <li ><a href="#tab1" style="width:55px;"><span>Tin mới</span></a></li>
  <li ><a href="#tab2"  style="width:80px;"><span>Tin đọc nhiều</span></a></li>
  <li  ><a href="#tab3"  style="width:61px;" class="last"><span>Tiêu điểm</span></a></li>
  </ul>
  <div class="clear"></div>
  <div id="tab1" class="tab" >
   	{data.list_new}
  </div>
  <div id="tab2" class="tab" style="display:none;" >
   	{data.list_views}
  </div>
  <div id="tab3" class="tab" style="display:none;" >
   	{data.list_focus}
  </div>
   <div class="clear"></div>
</div>
 <script type="text/javascript">
      new Yetii({
      id: "tab-news"
      });
</script>
<!-- END: block_news -->