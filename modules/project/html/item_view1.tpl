<!-- BEGIN: item_view1 -->
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="project">
    <div class="img">
    	<a href="{data.link}" title="{data.p_name}"><img src="{data.src}" alt="{data.p_name}" /></a>
    </div>
    <div class="tend">
    	<h3><a href="{data.link}" title="{data.p_name}">{data.p_name}</a></h3>
    </div>
  </div>
</div>
<!-- END: item_view1 -->
