<?php
/*================================================================================*\
|| 							Name code : ajax_music.php	 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai Son
**/

	define('IN_vnT',1);
	
	require_once("../../../_config.php"); 
	include($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;	
	include($conf['rootpath']."includes/class_functions.php"); 
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('tour');
	
	$linkMod = "?".$conf['cmd']."=mod:tour";
	$vnT->imagesdir="modules/tour/images/";
	$vnT->dirMod = "modules/tour/";
	
	switch ($_GET['do']){
		case "list_tour" : $jsout = list_tour() ;break;
		case "info_tour" : $jsout = info_tour() ;break;
		default  : $jsout ="Error ".$_GET['do'] ;break;
	}

/*-------------- List_SubCat --------------------*/
function List_SubCat ($cat_id){
global $func,$DB,$vnT;
	$output="";
	$query = $DB->query("SELECT * FROM tour_category WHERE parentid={$cat_id}  ");
	while ($cat=$DB->fetch_row($query)) {
		$output.=$cat["cat_id"].",";
		$output.=List_SubCat($cat['cat_id']);
	}
	return $output;
}

//list_tour
function list_tour() {
	global $vnT,$DB,$func,$conf;
	$cat_id = (int) $_GET['cat_id'];
	
	//lay dieu kien
	if ($cat_id){
		$subcat= List_SubCat($cat_id);
		$subcat = substr($subcat,0,-1);
		if (empty($subcat))
			$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
		else{
			$tmp = explode(",",$subcat);
			$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
			for ($i=0;$i<count($tmp);$i++){
				$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
			}
			$where .=" and (".$str_.") ";
		} 
	}
	
	$jsout= "<select size=1 id=\"tour_id\" name=\"tour_id\" onChange=\"load_info_tour(this.value,'{$vnT->lang_name}')\" >";
	$jsout.="<option value=\"0\">-- ".$vnT->lang['tour']['select_tour']." --</option>";
	
	
	$sql=" 	SELECT t.t_id,t.cat_id, tdes.title
					FROM tours t, tour_desc tdes 
					WHERE t.t_id = tdes.t_id
					AND t.is_display=1=1
					AND tdes.lang='$vnT->lang_name'
					$where
					ORDER BY t.t_id DESC ";
													
	$result = $DB->query($sql);
	while ($row=$DB->fetch_row($result )) {
		$title = $func->HTML($row['title']);
		$jsout.="<option value=\"{$row['t_id']}\" >{$title}</option>";
	}
	$jsout.="</select>";
	
	return $jsout;
}

//info_tour
function info_tour() {
	global $vnT,$DB,$func,$conf;
	$id = (int) $_GET['id'];
	
	$sql = "SELECT *
					FROM tours t, tour_desc tdes 
					WHERE t.t_id = tdes.t_id
					AND is_display=1=1
					AND lang='$vnT->lang_name'
					AND t.t_id=$id";
	$result = $DB->query($sql);
	
	if($data=$DB->fetch_row($result)){
		$link= "?".$conf['cmd']."=mod:tour|act:detail|id:{$id}";
	
		if($data['picture']){
			$w = 205 ;
			$pic = get_pic_thumb ($data['picture'],$w);
		}else{
			$pic = "<img  src=\"".$vnT->imagesdir."nophoto.jpg\"  >";
		}	
		$data['pic']	= "<a href=\"{$link}\">{$pic}</a>";
		
		$data['name']	= "<a href=\"{$link}\">".$func->HTML($data['title'])."</a>";
		
		
		$data['price']	="<span class='price'>".get_price_tour($data['price'])."</span> / ".$vnT->lang['tour']['guest'];
		$data['date_time'] =$func->HTML($data['date_time']);
		
		//option
		if ($data['options'])
		{
			$arr_op = unserialize($data['options']);
			$res_op = $DB->query("SELECT op.*, opd.op_name, opd.description, opd.lang 
													FROM tour_option op , tour_option_desc opd
													WHERE  op.op_id = opd.op_id
													AND opd.lang ='$vnT->lang_name'
													ORDER BY  op.op_order");
			while ($r_op = $DB->fetch_row($res_op))
			{
				if($arr_op[$r_op['op_id']])
				{
					$data['list_opiton'] .='	<tr>
						<td ><span class="smallFont">'.$func->HTML($r_op['op_name']).' : </span>
						<b>'.$arr_op[$r_op['op_id']].'</b></td>
					</tr>';
				}
			}
		}
		
		$data['view_more'] = "<input name='btnDetail' type='button'  value='".$vnT->lang['tour']['view_more']."' onClick=\"location.href='".$link."'\" />";
		$jsout = html_info_tour($data);
	}	

	return $jsout;
}

/*-------------- get_pic_thum --------------------*/
function get_pic_thumb ($picture,$w=""){
	global $vnT;
	$out="";
	$src_name = substr($picture,strrpos($picture,"/")+1);
	$dir = substr($picture,0,strrpos($picture,"/"));
	if($w){
		$src = "view_thumb.php?image=modules/tour/uploads/".$dir."/thumbs/".$src_name."&w=$w";
	}else{
		$src = "modules/tour/uploads/".$dir."/thumbs/".$src_name;
	}
	$out = "<img  src=\"{$src}\" >";
	
	return $out ;
}
/*-------------- get_price_tour --------------------*/
function get_price_tour ($price){
	global $func,$DB,$conf,$vnT;
	if ($price){
			$price = $func->format_number($price)." EU";
	}else{
		$price = "Call";
	}
	return $price;
}

/*---------- html_info_tour-----------*/
function html_info_tour($data){
global $input,$conf,$vnT ;
return<<<EOF
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="210" align="center" valign="top" style="padding-top:10px;"><div class="border_img">{$data['pic']}</div>
    </td>
    <td valign="top" >
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr><td><h3>{$data['name']}</h3></td></tr>
				 <tr>
				 <tr><td><span class="smallFont">{$vnT->lang['tour']['date_time']} :</span> <strong>{$data['date_time']}</strong></td></tr>
				 <tr>
					 <td><span class="smallFont">{$vnT->lang['tour']['price']} :</span> <strong>{$data['price']}</strong></td>
				 </tr>
				{$data['list_opiton']}
				 <tr>
					 <td><span class="smallFont">{$vnT->lang['tour']['desc_tour']} :</span> <p align="justify">{$data['description']}</p></td>
				 </tr>
				
				</table>
		</td>
  </tr>
</table>
EOF;
}

flush();
echo $jsout;
exit();

?>