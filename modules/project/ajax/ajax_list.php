<?php
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
 	require_once("../../../_config.php"); 
	require_once("../../../includes/class_db.php"); 
	require_once("../../../includes/class_functions.php"); 	
	$DB = new DB;
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	
	$vnT->linkMod = "?".$conf['cmd']."=mod:project";
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('product');
 
 
	switch ($_GET['do'])
	{
		case "project" : $aResults = get_project() ; break;
		default  : $aResults = array() ; break;
	}
 

 	//get_project
	function get_project() {
		global $DB,$func,$conf,$vnT;
		$textout = array();
		$keyword = $_GET['input'];				
 		$sql = "SELECT *
						FROM project n, project_desc nd
						WHERE n.pid=nd.pid 
						AND nd.lang='{$vnT->lang_name}'
						AND n.display=1
						AND title like '%".$keyword."%'
						ORDER BY title ASC, n.pid DESC "	;
						
		$res = $DB->query($sql);
		if($num = $DB->num_rows($res))
		{
			while	($row = $DB->fetch_row($res))
			{
				$textout[] = array(
													'id' =>  $row['pid'],
													'value' =>  $row['title'],
													'info' =>  "ID : ".$row['pid'],
												 );
			}
		}
		 
		return $textout;
	} 
	
 
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header ("Pragma: no-cache"); // HTTP/1.0



if (isset($_REQUEST['json']))
{
	header("Content-Type: application/json");

	echo "{\"results\": [";
	$arr = array();
	for ($i=0;$i<count($aResults);$i++)
	{
		$arr[] = "{\"id\": \"".$aResults[$i]['id']."\", \"value\": \"".$aResults[$i]['value']."\", \"info\": \"".$aResults[$i]['info']."\"}";
	}
	echo implode(", ", $arr);
	echo "]}";
}
else
{
	header("Content-Type: text/xml");

	echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?><results>";
	for ($i=0;$i<count($aResults);$i++)
	{
		echo "<rs id=\"".$aResults[$i]['id']."\" info=\"".$aResults[$i]['info']."\">".$aResults[$i]['value']."</rs>";
	}
	echo "</results>";
}

?>
