<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "project";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
    $vnT->html->addScript($vnT->dir_js . "/masonry/jquery.masonry.min.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO		
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];	
		}
		$data['main'] = $this->list_project();
		$navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['box_category'] = box_category();
    // echo "<pre/>";print_r(box_category());die;
    $data['link_action']  = $vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['project'].'.html';
    $data['slogan'] = $vnT->setting['slogan'];
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function list_project (){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $root_link = LINK_MOD.".html";
    $view = 1;
		$num_row=3;
		$sID = (int) $input['sID'];
		$where= '';
		if($sID){
			$where.= "AND status=".$sID;
			$ext_pag .= "&sID=".$sID;
		}
    $sql_num = "SELECT p.p_id FROM project p, project_desc pd
								WHERE p.p_id=pd.p_id AND lang='$vnT->lang_name' AND display=1 $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
    if($input['display']){
      $n = ($input['display']);
      $ext_pag .= "&display=".$input['display'];
    }else{
      $n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 9;
    }
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){
			$nav = "<div class='pagination'>".$vnT->func->paginate_search($root_link,$totals,$n,$ext_pag,$p)."</div>";
		}
    switch ($input['sort']) {
      case 'new':
        $where .= " ORDER BY date_post DESC ";
        break;
      case 'old':
        $where .= " ORDER BY date_post ASC ";
        break;
      case 'low':
        $where .= " ORDER BY price ASC ";
        break;
      case 'hight':
        $where .= " ORDER BY price DESC ";
        break;
      default:
        $where .= " ORDER BY p_order DESC, date_post DESC ";
        break;
    }
    //keyword 
    if (isset($input['keyword'])) $keyword=$input['keyword'];   
    if (!empty($keyword))
    { 
      $arr_keyword = array();
      if (stristr($keyword, ' AND ') !== false)
      {
          $arr_keyword        = @explode('AND', $keyword);
          $operator   = " AND ";
      }
      elseif (stristr($keyword, ' OR ') !== false)
      {
          $arr_keyword        = @explode('OR', $keyword);
          $operator   = " OR ";
      }
      elseif (stristr($keyword, ' + ') !== false)
      {
          $arr_keyword        = @explode('+', $keyword);
          $operator   = " OR ";
      }
      else
      {
          $arr_keyword        = @explode(' ', $keyword);
          $operator   = " AND ";
      }

      $where .= 'AND ('; 
      foreach ($arr_keyword AS $key => $val)
      {
        $val_utf8 =  strtolower($vnT->func->utf8_to_ascii($val)) ;
        if ($key > 0 && $key < count($arr_keyword) && count($arr_keyword) > 1)
        {
            $where .= $operator;
        }
        $val        = trim($val);        
        $where  .= "(p_name LIKE '%$val%' )";
      }
      $where .= ')';  
      $ext_pag .= "&keyword=" . rawurlencode($keyword);
    }
		$data['navation'] = get_navation ($cat_id);
    $data['row_project'] = row_project($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
		$nd['content'] = $this->skin->text("html_list");
 		// $nd['f_title'] = '<h1>'.$vnT->lang['project']['project'].'</h1>';
    // $nd['more'] = $vnT->setting['slogan'];
		$textout = $vnT->skin_box->parse_box("box_middle",$nd);
		return $textout;
  }


  
}
?>