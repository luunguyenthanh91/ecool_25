<?php
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	require_once("../../../_config.php"); 
	require_once($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;
	
	require_once($conf['rootpath']."includes/class_functions.php"); 	
	$func = new Func_Global;
	$conf=$func->fetchDbConfig($conf);
	
	$linkMod = "?".$conf['cmd']."=mod:product";
	$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang']  : "vn" ;
	$func->load_language('product');
	$dirUpload = "vnt_upload/product";
	
	
	$id = (int)$_GET['id'];
	//load 
	$sql = "SELECT m.* , p.picture as pic_pro , pd.p_name 
						FROM product_maps m , products p , products_desc pd
						WHERE  m.p_id= p.p_id
						AND p.p_id = pd.p_id
						AND pd.lang='$vnT->lang_name'
						AND m.map_id=".$id;
	
	$result = $DB->query($sql);
	if($data = $DB->fetch_row($result))
	{
		if($data['pic_pro']) {
			$src_name = substr($data['pic_pro'],strrpos($data['pic_pro'],"/")+1);
			$dir = substr($data['pic_pro'],0,strrpos($data['pic_pro'],"/"));
			$src = $conf['rooturl'].$dirUpload."/".$dir."/thumbs/".$src_name;
			$picture = '<img hspace="4" height="80" width="100" border="0" align="left" alt="" style="border: 1px solid rgb(172, 172, 172);" src="'.$src.'"/>';
		}else{
			$picture ='';
		}
		
		
	}
	

	$data['description'] = str_replace("\r\n","<br>",$data['description']);
//		echo $data['description'] ;
	$address = trim($data['address']);
	$description = '<div style="width: 350px; padding-right: 5px; font-family: Arial; font-size: 12px;">';
	$description .= '<b>'.$func->txt_tooltip($func->HTML($data['p_name'])).'</b><br/>' ;
	$description .= $picture ;
	$description .= '<span style="margin-left: 5px;">'.$data['description'].'</span>' ;
	$description .='</div>' ;
	
	require_once ($conf['rootpath']."includes/GoogleMap.php"); 
	$gm = & new GoogleMap("ABQIAAAA-IlYcZLc7Zqt4BzxcbQVNRR9VDsu3istyPGaE24d8VZcLlpuHxQ1CGI_6DFdByCdshNyv__-yJly_g");
	$gm->SetMarkerIconStyle('GT_PILLOW');
	$gm->SetMapZoom(17);
	$gm->SetMapWidth(720);
	$gm->SetMapHeight(450);
	$gm->SetMapControl('LARGE_PAN_ZOOM');
	$gm->mMapType = TRUE;
	$gm->SetAddress($address);
	$gm->SetInfoWindowText($description);
 
?>
<html>
<head>
<title>GoogleMap</title>
<?php echo $gm->GmapsKey(); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<body>
<?php echo $gm->MapHolder(); ?>
<?php echo $gm->InitJs(); ?>

</body>
</html>