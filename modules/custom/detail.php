<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "custom";
  var $action = "detail";

  function sMain (){
    global $vnT, $input;
    include ("function_".$this->module.".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $vnT->dir_js);
    $this->skin->assign('DIR_STYLE', $vnT->dir_style);
    $this->linkMod = $vnT->cmd . "=mod:" . $this->module;
    $vnT->html->addStyleSheet(DIR_MOD . "/css/".$this->module . ".css");
		$vnT->setting['menu_active'] = $this->module;
    $pID = (int)$input['itemID'];
		if($_GET['preview']==1 && $_SESSION['admin_session'])	{
			$where = " ";
		}else{
	    $vnT->DB->query("UPDATE custom SET views=views+1 WHERE p_id=$pID");
			$where = " AND display=1 ";
		}
		$result= $vnT->DB->query("SELECT * FROM custom p, custom_desc pd
															WHERE p.p_id =pd.p_id AND pd.lang='$vnT->lang_name' {$where} AND pd.p_id=$pID ");
		if($row = $vnT->DB->fetch_row($result)){
			if ($row['metadesc']) $vnT->conf['meta_description'] = $row['metadesc'];
			if ($row['metakey']) $vnT->conf['meta_keyword'] = $row['metakey'];
			if ($row['friendly_title']){
				$vnT->conf['indextitle'] = $row['friendly_title'];
			}
			$link_seo = ($vnT->muti_lang) ? ROOT_URL.$vnT->lang_name."/" : ROOT_URL;
      $link_seo .= $row['friendly_url'].".html";
      $this->skin->assign("data", $row);
      $this->skin->parse("html_360");
      $vnT->output .= $this->skin->text("html_360");
 		}else {
			$linkref = LINK_MOD.".html";
      @header("Location: ".$linkref."");
      echo "<meta http-equiv='refresh' content='0; url=".$linkref."'/>";
		}
  }
}
?>