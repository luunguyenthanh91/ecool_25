<?php
/*================================================================================*\
|| 							Name code : function_custom.php 		 		 											  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 17/12/2007 by Thai Son
**/

if ( !defined('IN_vnT') )	{ die('Access denied');	}

define("DIR_MOD", ROOT_URI . "modules/custom");
define('MOD_DIR_UPLOAD', ROOT_URI . 'vnt_upload/custom');
define("MOD_DIR_IMAGE", ROOT_URI . "modules/custom/images");
define("LINK_MOD",  $vnT->link_root . $vnT->setting['seo_name'][$vnT->lang_name]['custom']);	

function create_link ($act,$id,$title,$extra=""){
	global $vnT,$func,$DB,$conf;
	switch ($act){
		case "category" : $text = $vnT->link_root.$title.".html";	break;
		case "detail" : $text = $vnT->link_root.$title.".html";	break;
		default : $text = LINK_MOD."/".$act."/".$id."/".$func->make_url($title).".html"; break;
	}
	$text .= ($extra) ? "/".$extra : "";
	return $text;
}
function loadSetting (){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from custom_setting WHERE lang='$vnT->lang_name' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
	$res_s= $DB->query("SELECT n.cat_id,cat_code, cat_name FROM custom_category n, custom_category_desc nd
                      WHERE n.cat_id = nd.cat_id AND display = 1 AND lang='$vnT->lang_name' ");
  while ($row_s = $DB->fetch_row($res_s)) {
    $vnT->setting['cat_code'][$row_s['cat_id']] = $vnT->func->HTML($row_s['cat_code']);
  }
	unset($setting);
}
function load_html ($file, $data){
  global $vnT, $input;
  $html = new XiTemplate( DIR_MODULE . "/custom/html/" . $file . ".tpl");
  $html->assign('DIR_MOD', DIR_MOD);
  $html->assign('LANG', $vnT->lang);
  $html->assign('INPUT', $input);
  $html->assign('CONF', $vnT->conf);
  $html->assign('DIR_IMAGE', $vnT->dir_images);
  $html->assign("data", $data);
  $html->parse($file);
  return $html->text($file);
}
function get_where_cat ($cat_id){
  global $input, $conf, $vnT;	
  $text = ""; 	
	$subcat = List_SubCat($cat_id);
	$subcat = substr($subcat, 0, - 1);
	if (empty($subcat)) $text .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
	else{
		$tmp = explode(",", $subcat);
		$str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
		for ($i = 0; $i < count($tmp); $i ++)		{
			$str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
		}
		$text .= " and (" . $str_ . ") ";
	}
  return $text;
}
function get_cat_name ($cat_id){
  global $func, $DB, $conf, $vnT;
  $out = $vnT->lang['custom']['category'];
  $sql = "SELECT cat_name FROM custom_category_desc WHERE cat_id={$cat_id} and lang='$vnT->lang_name'";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)){
    $out = $func->HTML($row['cat_name']);
  }
  return $out;
}
function get_friendly_url ($table,$where=""){
  global $func, $DB, $conf, $vnT;
  $out = "";
  $sql = "SELECT friendly_url FROM {$table}_desc WHERE lang='$vnT->lang_name' {$where} ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $out = $row['friendly_url'];
  }
  return $out;
}
function get_navation ($cat_id,$ext=''){
  global $DB,$conf,$func,$vnT,$input;
  $output = '<ul itemscope="" itemtype="http://schema.org/BreadcrumbList">';
  $output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem" class="home"><a href="'.$vnT->link_root.'" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><i class="fa fa-home"></i></span></a></li> ';
	$output.= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="'.LINK_MOD.'.html"><span itemprop="name">'.$vnT->lang['custom']['custom'].'</span></a></li> ';
	if($ext)
		$output .= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><span itemprop="name">'.$ext.'</span></li>';
	$output .= '</ul>';
	return $output;
}
function get_pic_thumb ($picture, $w=100, $ext="" , $is_email=0){
  global $vnT;
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
	$link_url = ($is_email) ? ROOT_URL : ROOT_URI;
	$linkhinh = "vnt_upload/custom/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
	if($vnT->setting['thum_size']){
		$src = $link_url.$dir."/thumbs/{$w_thumb}_".$pic_name;				
	}else{
		$src =  $link_url.$dir."/thumbs/".$pic_name;	
	}
	if($w<$w_thumb) $ext .= " width='$w' ";
  $out = "<img src=\"{$src}\" {$ext} >";
  return $out;
}
function get_src_pic_custom ($picture, $w = "" ,$h=0,$crop=0){
	global $vnT,$func;	
  $out = "";	
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
  if ($w){
		$linkhinh = "vnt_upload/custom/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1);
		$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
		$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
		if (!file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			} else {
				@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			}
			$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w ,$h ,$crop);
		}
		$src = ROOT_URI . $file_thumbs;
  } else {
    $src = MOD_DIR_UPLOAD."/".$picture;    
  }
  return $src;
}
function get_pic_custom ($picture, $w = "", $ext=""){
	global $vnT,$func;	
  $out = "";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;	
	$linkhinh = "vnt_upload/custom/".$picture;
	$linkhinh = str_replace("//","/",$linkhinh);
	$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
	$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1);
  if ($w){
		$file_thumbs = $dir."/thumbs/{$w}_".substr($linkhinh,strrpos($linkhinh,"/")+1);
		$linkhinhthumbs = $vnT->conf['rootpath'].$file_thumbs;
		if (!file_exists($linkhinhthumbs)) {
			if (@is_dir($vnT->conf['rootpath'].$dir."/thumbs")) {
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			} else {
				@mkdir($vnT->conf['rootpath'].$dir."/thumbs",0777);
				@chmod($vnT->conf['rootpath'].$dir."/thumbs",0777);
			}
			$vnT->func->thum($vnT->conf['rootpath'].$linkhinh, $linkhinhthumbs, $w,$w ,"1:1");
		} 
		$src = ROOT_URI . $file_thumbs;
  } else {
    $src = MOD_DIR_UPLOAD . "/" . $picture;    
  }
	$alt = substr($pic_name, 0, strrpos($pic_name, "."));
  $out = "<img src=\"{$src}\" {$ext}>";
  return $out;
}
function List_Status_Pro ($selname, $did, $ext = ""){
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"{$selname}\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>".$vnT->lang['custom']['select_status']."</option>";
  $sql = "SELECT n.*, nd.title FROM custom_status n, custom_status_desc nd 
					WHERE n.status_id=nd.status_id AND nd.lang='$vnT->lang_name' AND  display=1 
					ORDER BY s_order ASC, n.status_id DESC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)){
	 	$selected = ($row['status_id'] == $did) ? " selected " : "";
		$text .= "<option value=\"{$row['status_id']}\" {$selected} >".$vnT->func->HTML($row['title'])."</option>";		
  }
  $text .= "</select>";
  return $text;
}
function Get_Cat($did=-1,$ext){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1  name=\"cat_id\"  class=\"select\" $ext >";
	$text.="<option value=\"0\">-- Tất cả danh mục --</option>";
	$query= $DB->query("SELECT c.cat_id,cd.cat_name,cd.description 
											FROM custom_category c , custom_category_desc cd
											WHERE c.cat_id=cd.cat_id AND cd.lang='$vnT->lang_name' AND c.parentid=0 
											ORDER BY cat_order ASC ,c.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($cat['cat_id'],$n,$did);
	}
	$text.="</select>";
	return $text;
}
function Get_Sub($cid,$n,$did=-1){
	global $vnT,$func,$DB,$conf;
	$output="";
	$k=$n;
	$query= $DB->query("SELECT c.cat_id,cd.cat_name,cd.description 
											FROM custom_category c , custom_category_desc cd
											WHERE c.cat_id=cd.cat_id AND cd.lang='$vnT->lang_name' AND c.parentid={$cid}
											ORDER BY cat_order ASC ,c.cat_id DESC ");
	while ($cat=$DB->fetch_row($query)) {
	$cat_name = $vnT->func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did){
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		} else {	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "--";
			$output.=" {$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($cat['cat_id'],$n,$did);
	}
	return $output;
}
function List_Search($did){
	global $func,$DB,$conf,$vnT;
	$text= "<select size=1 name=\"search\" >";
	if ($did =="maso")
		$text.="<option value=\"maso\" selected> {$vnT->lang['custom']['code_custom']} </option>";
	else
		$text.="<option value=\"maso\"> {$vnT->lang['custom']['code_custom']} </option>";
	if ($did =="name")
		$text.="<option value=\"name\" selected> {$vnT->lang['custom']['p_name']} </option>";
	else
		$text.="<option value=\"name\"> {$vnT->lang['custom']['p_name']} </option>";
	$text.="</select>";
	return $text;
}
function get_option ($info){
	global $vnT,$func,$DB;
	if ($info['options']){
		$textout='<table width="100%" border="0" cellspacing="1" cellpadding="1" class="table">';
		$arr_op = unserialize($info['options']);
		$res_op = $DB->query("SELECT * FROM custom_option n, custom_option_desc nd  
													WHERE n.op_id=nd.op_id AND lang ='$vnT->lang_name' AND n.display=1 
													ORDER BY n.op_order, n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			if($arr_op[$r_op['op_id']]){
				$textout .='<tr>
											<td class="colInfo1" width="80" nowrap>'.$func->HTML($r_op['op_name']).' : </td>
											<td class="colInfo2">'.$arr_op[$r_op['op_id']].'</td>
										</tr>';
			}
		}
		$textout.="</table>";
	}
	return $textout;
}
function get_imgvote ($votes){
  global $func, $DB, $conf;
  $voteimg = "";
  $votenum = floor($votes);
  $votemod = $votes - $votenum;
  for ($n = 0; $n < $votenum; $n ++)
    $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
  if ($votemod > 0.3){
    if ($votemod > 0.7)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star2.gif\" />";
    else
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star1.gif\"  />";
    for ($n = 0; $n < (4 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  } else {
    for ($n = 0; $n < (5 - $votenum); $n ++)
      $voteimg .= "<img src=\"" . MOD_DIR_IMAGE . "/star0.gif\"  />";
  }
  return $voteimg;
}
function get_p_name ($id){
	global $func,$DB,$conf,$vnT;
	$p_name="";
	$result = $DB->query("SELECT p_name FROM custom_desc WHERE lang='$vnT->lang_name' AND p_id=$id ");
	if ($row= $DB->fetch_row($result)){
		$p_name = $func->HTML($row['p_name']);
	}
	return $p_name ;
}
function List_SubCat ($cat_id){
  global $func, $DB, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM custom_category WHERE parentid={$cat_id}  ");
  while ($cat = $DB->fetch_row($query)){
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}
function get_tooltip ($data){
  global $func, $DB, $vnT, $conf;
	// cache
 	$param_cache = array($data['p_id']);
	$cache = $vnT->Cache->read_cache("pro_tooltip",$param_cache);
	if ($cache != _NOC) return $cache;
  $output = "";	
  $output .= '<div class=fTooltip>' . $vnT->func->HTML($data['p_name']) . ' </div>';	
	$output.='<table width=100%  border=0 cellspacing=0 cellpadding=0>';
	$output.='<tr >
            <td  class=tInfo1 nowrap width=50  >'.$vnT->lang['custom']['maso'].' </td>
            <td width=10  class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b >'.$data['maso'].'</b></td>
        </tr>';
	$output.='<tr >
            <td valign="top" class=tInfo1 nowrap  >'.$vnT->lang['custom']['price'].' </td>
            <td width=10 valign="top" class=tInfo1 align=center> : </td>
            <td class=tInfo2 ><b class=tPrice>'.get_price_custom($data['price']).'</b></td>
        </tr>';
	//option
	if ($data['options']){
		 
		$arr_op = unserialize($data['options']);		 
		$res_op = $DB->query("select * from custom_option n, custom_option_desc nd  
													where  n.op_id=nd.op_id
													AND nd.lang='$vnT->lang_name'
													AND n.display=1  
													AND n.focus=1
													ORDER BY n.op_order ASC ,  n.op_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			if($arr_op[$r_op['op_id']]){
				$output .= '<tr>
											<td class=tInfo1  nowrap >'.$func->HTML($r_op['op_name']).' </td>
											<td width=10 valign="top" class=tInfo1 align=center> : </td>
											<td class=tInfo2> <span class="color">'.$arr_op[$r_op['op_id']].'</span></td>
										</tr>';
			}
		}
	}
				
	$output.="</table>";
	$output .= '<table width=100% border=0 cellspacing=2 cellpadding=2><tr><td >';  
	if($data['desc_status']) {
		$output .= '<h3 class=th3 >'.$vnT->lang['custom']['f_promotion'].'</h3>'; 
  	$output .= '<div align=justify >'.$vnT->func->HTML($data['desc_status']) . '</div>';
	}
	$output .='</tr></table>';
	$vnT->Cache->save_cache("pro_tooltip", $output, $param_cache);
  return $output;
}
function html_row ($row){
  global $input, $conf, $vnT, $func;
  $pID = (int) $row['p_id'];
	$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 370;
  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
	$data['p_name'] = $vnT->func->HTML($row['p_name']);
	$picture = ($row['picture']) ? 'custom/'.$row['picture'] : 'custom/'.$vnT->setting['pic_nophoto'];
	$data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
	//$data['pic'] = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['p_name']."' title='".$row['p_name']."' ",1,0,array("fix_width"=>1));
  $output = load_html("item_custom", $data);
  return $output;
}
function html_col ($data){
  global $input, $vnT, $func, $DB;
  $pID = (int) $data['p_id'];
  $link = create_link("detail",$pID,$data['friendly_url']);
  $data['checkbox'] = "<input name=\"ch_id[]\" id=\"ch_id\" type=\"checkbox\" value='" . $pID . "'  class=\"checkbox\" onClick=\"javascript:select_list('item{$pID}')\"  />";
	$w = ($vnT->setting['img_width_list']) ? $vnT->setting['img_width_list'] : 100;
	$pic = ($data['picture']) ?  get_pic_custom($data['picture'],$w," alt='".$data['p_name']."' title='".$data['p_name']."' ") : "<img  src=\"" . MOD_DIR_IMAGE . "/nophoto.gif\"  >";
  $data['pic'] = "<a href=\"{$link}\" title='".$data['p_name']."'>{$pic}</a>";
	$p_name = $vnT->func->HTML($data['p_name']);	
  $data['name'] = "<a href=\"{$link}\" title='".$data['p_name']."' >" . $p_name . "</a>";
  $data['list_option']  = get_option($data) ; 
  return load_html("item_view2", $data);
}
function row_custom ($where, $start, $n, $num_row=4, $view=1){
  global $vnT, $input, $DB, $func, $conf;
  $text = "";
  $sql = "SELECT * FROM custom p, custom_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND lang='$vnT->lang_name' $where LIMIT $start,$n";
  $result = $vnT->DB->query($sql);
  if ($num = $vnT->DB->num_rows($result)){
    if ($view == 2){
      $i = 1;
      while ($row = $DB->fetch_row($result)){
        $text .= html_col($row);
        $i ++;
      }
    } else {
			while ($row = $DB->fetch_row($result)){
				$text .= html_row($row);
			}
    }
  } else {
    $text = "<div class='noItem'>{$vnT->lang['custom']['no_have_custom']}</div>";
  }
  return $text;
}
function scroll_custom ($scroll_name, $where,$start=0,$n=10){
	global $DB, $func, $input, $vnT;  
	//$param_cache = array($scroll_name,$input['pID']) ;	
	//$cache = $vnT->Cache->read_cache("scroll_custom",$param_cache);
	//if ($cache != _NOC) return $cache;   
	$sql = "SELECT * FROM custom p, custom_desc pd
					WHERE p.p_id=pd.p_id AND display=1 AND pd.lang='$vnT->lang_name' $where LIMIT $start,$n";
	$result = $vnT->DB->query($sql);
	if ($num = $vnT->DB->num_rows($result)){
		$w = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 270;
		while ($row = $vnT->DB->fetch_row($result)){
		  $data['link'] = create_link("detail",$pID,$row['friendly_url']);
			$data['p_name'] = $vnT->func->HTML($row['p_name']);
			$picture = ($row['picture']) ? "custom/".$row['picture'] : "custom/".$vnT->setting['pic_nophoto'];
		  $data['src'] = $vnT->func->get_src_modules($picture, $w ,'',1,'1.5:1');
		  $text .= load_html("item_other", $data);
		}
	} else{
		$text .='<li class=noItem>'.$vnT->lang['custom']['no_have_custom'].'</li>';
	}
	$text_out = $text;
	//$vnT->Cache->save_cache("scroll_custom", $text_out,$param_cache);	
	return $text_out;
}
function box_sidebar (){
	global $vnT, $input;
	$textout = '';
	//$textout .= $vnT->lib->get_banner_sidebar('sidebar');;
 	return $textout;
}
function box_filter(){
	global $vnT,$BD,$input;
	$data = array();
	// $data['box_category'] = box_category();
	// $data['Display_Filter'] = Display_Filter($input['display']);
	// $data['Sort_Filter'] = Sort_Filter($input['sort']);
	// return load_html('box_filter',$data);
}
function Display_Filter($key){
	global $vnT;
	$n = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 8;
	$arr[1]['key'] = $n;
	$arr[1]['value'] = $n;
	$arr[2]['key'] = $n + $n;
	$arr[2]['value'] = $n + $n;
	$arr[3]['key'] = $n + $n*2;
	$arr[3]['value'] = $n + $n*2;
	$arr[4]['key'] = $n + $n*3;
	$arr[4]['value'] = $n + $n*3;
	$cur_link = $vnT->seo_url;
	$cur_title = $n;
	$text .= '<select name="display" id="display" onchange="location=this.value;">';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		$link = build_links($cur_link,"display",$arr[$i]['key']);
		if($key==$arr[$i]['key']) $cur_title = $arr[$i]['key'];
		$text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].'</option>';
	}
	$text .= '</select>';
	$data['f_title'] = $vnT->lang['custom']['display_filter'];
	$data['content'] = $text;
	return $vnT->skin_box->parse_box("box_custom_filter", $data);
}
function Sort_Filter($key){
	global $vnT;
	$arr[1]['key'] = 'new';
	$arr[1]['value'] = $vnT->lang['custom']['sort_new'];

	$arr[2]['key'] = 'old';
	$arr[2]['value'] = $vnT->lang['custom']['sort_old'];

	// $arr[3]['key'] = 'low';
	// $arr[3]['value'] = $vnT->lang['product']['price_low'];
	
	// $arr[4]['key'] = 'hight';
	// $arr[4]['value'] = $vnT->lang['product']['price_hight'];
	$cur_link = $vnT->seo_url;
	$text ='<select name="sort" id="sort" onchange="location=this.value;">';
	$link_default = build_links($cur_link,"sort",'default');
	$text.= '<option value="'.$link_default.'">'.$vnT->lang['custom']['sort_default'].'</option>';
	for($i=1;$i<=count($arr);$i++){
		$selected = ($key==$arr[$i]['key']) ? 'selected' : '';
		$link = build_links($cur_link,"sort",$arr[$i]['key']);
		$text .= '<option value="'.$link.'" '.$selected.'>'.$arr[$i]['value'].'</option>';
	}
	$text .= '</select>';
	$data['f_title'] = $vnT->lang['custom']['sort_filter'];
	$data['content'] = $text;
	return $vnT->skin_box->parse_box("box_custom_filter", $data);
}
function build_links ($cur_link ,$key,$value, $act="add")	{
	global $vnT ,$input;
	$new_link = "";
	$p = ((int) $input['p']) ? (int) $input['p'] : 1;
	$check = @explode("?",$cur_link);
	if($check[1]){
		$arr_new_param = @explode("&", $check[1]);	
		$is_new = 1;
		$new_link = $check[0]."?" ;
		foreach ($arr_new_param as $param){
			$tmp = @explode("=",$param);
			if ( $tmp[0] == $key ) {
				$new_link .=  $tmp[0] . "=" .$value ."&" ;
				$is_new=0;
			}else{
				$new_link .=  $param ."&" ;	
			}
		}
		if($is_new==1) {
			$new_link .= $key."=".$value; 
		}else{
			$new_link = substr($new_link,0,-1); 	
		}
	}else{
		$new_link = $cur_link ."/?".$key."=".$value; 	
	}
	return $new_link ;
}
function box_category (){
  global $vnT, $func, $DB, $conf, $input;
  $text = $textout = '';
  $cat_id = (int) $input['catID'];
  $parentid = get_parent_id($cat_id);
  $query= $DB->query("SELECT n.*,nd.cat_name,nd.friendly_url FROM custom_category n, custom_category_desc nd
                      WHERE n.cat_id=nd.cat_id AND lang='{$vnT->lang_name}' AND display=1 AND parentid={$parentid}
                      ORDER BY cat_order ASC, nd.cat_id DESC, date_post DESC");
  if ($num = $DB->num_rows($query)) {
    $text = '<ul>';
    $acm = (!$input['catID']) ? 'class="current"' : '';
    $text .= '<li '.$acm.'><a href="'.LINK_MOD.'.html">'.$vnT->lang['custom']['all_custom'].'</a></li>';
    while ($row = $DB->fetch_row($query)) {
      $link = create_link('category',$row['cat_id'], $row['friendly_url']);
      $class = ($cat_id == $row['cat_id']) ? 'class="current"' : '';
      $text .= '<li '.$class.'><a href="'.$link.'">'.$vnT->func->HTML($row['cat_name']).'</a></li>';
    }
    $text .= '</ul>';
    $data['content'] = $text;
    $textout = load_html('box_cat',$data);
  }
  return $textout;
}
function get_parent_id($cat_id){
  global $vnT,$DB;
  $parentid = 0;
  $result = $DB->query("SELECT n.cat_id, parentid FROM custom_category n, custom_category_desc nd
                        WHERE n.cat_id = nd.cat_id AND display = 1
                        AND lang='$vnT->lang_name' AND parentid = {$cat_id}");
  if($num = $DB->num_rows($result)){
    $parentid = $cat_id;
  } else{
    $result = $DB->query("SELECT parentid FROM custom_category WHERE cat_id = {$cat_id}");
    if($row = $DB->fetch_row($result))
      $parentid = $row['parentid'];
  }
  return $parentid;
}
function get_price_custom ($price,$unit ='đ',$default=""){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price).$unit;
	}else{
		$price = ($default) ? $default : $vnT->lang['custom']['price_default'];
	}
	return $price;
}
function get_list_custom($did){
	global $vnT,$DB,$input;
	$result = $DB->query("SELECT c.p_id, p_name FROM custom c, custom_desc cd
												WHERE c.p_id = cd.p_id AND display = 1 AND lang ='$vnT->lang_name'
												ORDER BY p_order DESC, c.p_id DESC");
	$text = '<select name="p_id" id="p_id">';
	$text.= '<option value="0">'.$vnT->lang['custom']['select_custom'].'</option>';
	if($num = $DB->num_rows($result)){
		while ($row = $DB->fetch_row($result)) {
			$selected = ($did == $row['p_id']) ? 'selected' : '';
			$text.= '<option value="'.$row['p_id'].'" '.$selected.'>'.$vnT->func->HTML($row['p_name']).'</option>';
		}
	}
	$text .= '</select>';
	return $text;
}
function get_attr_option(){
	global $vnT,$DB;
	$arr_out = array();
	$result = $DB->query("SELECT * FROM custom_attr c, custom_attr_desc cd
												WHERE c.attr_id = cd.attr_id AND display = 1 AND lang ='$vnT->lang_name'
												ORDER BY display_order ASC, c.attr_id DESC");
	if($num = $DB->num_rows($result)){
		while ($row = $DB->fetch_row($result)) {
      $arr[$row['attr_type']][$row['attr_id']] = $row;
    }
	}
	// Project
  $num = count($arr['project']);
  if ($num > 0) {
  	$project = '<select name="project" id="project">';
  	$project.= '<option value="0">'.$vnT->lang['custom']['select_project'].'</option>';
    foreach ($arr['project'] as $attr_id => $row) {
      $i++;
      $title = $vnT->func->HTML($row['title']);
      $project.= '<option value="'.$attr_id.'">'.$title.'</option>';
    }
    $project.= '</select>';
  }
  // Product
  $num = count($arr['product']);
  if ($num > 0) {
  	$product = '<select name="product" id="product">';
  	$product.= '<option value="0">'.$vnT->lang['custom']['select_product'].'</option>';
    foreach ($arr['product'] as $attr_id => $row) {
      $i++;
      $title = $vnT->func->HTML($row['title']);
      $product.= '<option value="'.$attr_id.'">'.$title.'</option>';
    }
    $product.= '</select>';
  }
  // Style
  $num = count($arr['style']);
  if ($num > 0) {
  	$style = '<select name="style" id="style">';
  	$style.= '<option value="0">'.$vnT->lang['custom']['select_style'].'</option>';
    foreach ($arr['style'] as $attr_id => $row) {
      $i++;
      $title = $vnT->func->HTML($row['title']);
      $style.= '<option value="'.$attr_id.'">'.$title.'</option>';
    }
    $style.= '</select>';
  }
  // Material
  $num = count($arr['material']);
  if ($num > 0) {
  	$material = '<select name="material" id="material">';
  	$material.= '<option value="0">'.$vnT->lang['custom']['select_material'].'</option>';
    foreach ($arr['material'] as $attr_id => $row) {
      $i++;
      $title = $vnT->func->HTML($row['title']);
      $material.= '<option value="'.$attr_id.'">'.$title.'</option>';
    }
    $material.= '</select>';
  }
  $arr_out['project'] = $project;
  $arr_out['product'] = $product;
  $arr_out['style'] = $style;
  $arr_out['material'] = $material;
  return $arr_out;
}
function load_attr_name(){
	global $vnT,$DB;
	$arr_out = array();
	$result = $DB->query("SELECT c.attr_id, title FROM custom_attr c, custom_attr_desc cd
												WHERE c.attr_id = cd.attr_id AND display = 1 AND lang ='$vnT->lang_name'
												ORDER BY display_order ASC, c.attr_id DESC");
	if($num = $DB->num_rows($result)){
		while ($row = $DB->fetch_row($result)) {
      $vnT->setting['attr_name'][$row['attr_id']] = $vnT->func->HTML($row['title']);
    }
	}
  unset($row);
}
?>