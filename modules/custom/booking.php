<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
	var $output = "";
	var $skin = "";
	var $linkUrl = "";
	var $module = "custom";
	var $action = "booking";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		$this->linkMod = ROOT_URL.$vnT->cmd . "=mod:".$this->module;
		$this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		$err="";
		switch ($input['do']) {
			case 'success':
				$data['main'] = $this->do_Success();
				$vnT->conf['indextitle'] = $vnT->lang['custom']['f_booking_success'];
				break;
			default:
				$data['main'] = $this->do_Booking($row);
				$vnT->conf['indextitle'] = $vnT->lang['custom']['f_booking'];
				break;
		}
	  $navation = get_navation($input['catID']);
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
		$this->skin->assign("data", $data);
  	$this->skin->parse("modules");
		$vnT->output .= $this->skin->text("modules");
	}
	function do_Booking(){
		global $vnT,$DB,$input,$func;
		$err = '';
		if (isset($_POST['btnConfirm'])){
			if ($vnT->user['sec_code'] == $input['security_code']){
				if(empty($err)){
					$cot['p_id'] = (int) $input['p_id'];
					$cot['name'] = $input['name'];
					$cot['phone'] = $input['phone'];
					$cot['email'] = $input['email'];
					$cot['address'] = $input['address'];
					$cot['status'] = 1;
					$cot['comment'] = $vnT->func->txt_HTML($_POST['comment']);
					$cot['date_post'] = time();
					$ok = $vnT->DB->do_insert("custom_booking",$cot);
					if($ok){
						$content_email = $vnT->func->load_MailTemp("booking");
						$qu_find = array(
							'{domain}' ,
							'{name}' ,
							'{email}' ,
							'{phone}' ,
							'{address}' ,
							'{custom}' ,
							'{content}',
							'{date}'
						);
						$qu_replace = array(
							$_SERVER['HTTP_HOST'] ,
							$input['name'],
							$input['email'],
							$input['phone'],
							$input['address'],
							get_p_name($input['p_id']),
							$vnT->func->HTML($_POST["comment"]),
							date("H:i, d/m/Y")
						);
						$message = str_replace($qu_find, $qu_replace, $content_email);
						$subject = str_replace("{host}",$_SERVER['HTTP_HOST'],$vnT->lang['custom']['subject_booking']);
						$sent = $vnT->func->doSendMail($vnT->conf['email'], $subject, $message, $input["email"], $file_attach);
						$link_ref = LINK_MOD.'/booking.html/?do=success';
						$vnT->func->header_redirect($link_ref);
					}else{
						$err = $func->html_err($DB->debug());
					}
				}else{
					$err = $vnT->func->html_err($err);
				}
			}else{
				$err = $vnT->func->html_err($vnT->lang['custom']['security_code_invalid']);
			}
		}else{
			echo 'Access Denied!';

		}
		$data['list_custom'] = get_list_custom($input['id']);
		$data['err'] = $err;
		$vnT->user['sec_code'] = $vnT->func->get_security_code();
	  $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
	  $data['ver_img'] = ROOT_URL."includes/sec_image.php?code=$scode&h=38";
	  $this->skin->assign("data", $data);
  	$this->skin->parse("html_booking");
		return $this->skin->text("html_booking");
	}
	function do_Success(){
		global $vnT,$DB,$input;
	  $this->skin->assign("data", $data);
  	$this->skin->parse("html_booking_success");
		return $this->skin->text("html_booking_success");
	}
}
?>