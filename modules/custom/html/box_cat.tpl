<!-- BEGIN: box_cat -->
<div class="menuCollect hidden-sm hidden-xs">
  <div class="wrapping">
    <div class="wrapper">
      {data.content}
    </div>
  </div>
</div>
<div class="wrapper hidden-lg hidden-md">
  <div class="select-j">
    <div class="title">{LANG.collection.select_category}</div>
    <div class="content">
      {data.content}
    </div>
  </div>
</div>
<!-- END: box_cat -->