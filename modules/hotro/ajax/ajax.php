<?php
	if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {  
		die('Hacking attempt!');
	}
	session_start();
	define('IN_vnT', 1);
	define('PATH_ROOT', dirname(__FILE__));
	define('DS', DIRECTORY_SEPARATOR); 
	
	require_once("../../../_config.php"); 
	require_once ($conf['rootpath'] . 'includes' . DS . 'defines.php');
	require_once($conf['rootpath']."includes/class_db.php"); 
	$vnT->DB= $DB = new DB;
	require_once ($conf['rootpath']."includes/class_functions.php"); 
	$vnT->func = $func = new Func_Global();
	$vnT->conf = $conf = $func->fetchDbConfig($conf);
	require "../../../includes/JSON.php";
 	
	 
	$vnT->lang_name = (isset($_POST['lang'])) ? $_POST['lang']  : "vn" ;
	$func->load_language('dealer');

  $result = $DB->query("select * from dealer_setting WHERE lang='$vnT->lang_name' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
		
	switch ($_GET['do']){ 
		case "option_state" : $jsout = get_option_state() ;break;
		case "list_dealer" : $jsout = get_list_dealer() ;break;
		default  : $jsout ="Error" ;break;
	}

 
//get_option_state
function get_option_state ()
{
  global $DB, $func, $conf, $vnT;
  $textout = "";
  $city = (int)$_POST['city'];
  $did = (int)$_POST['state'];
	
  $textout .= "<option value=\"\" selected>" . $vnT->lang['dealer']['select_state'] . "</option>";
	
	$sql = "SELECT * FROM iso_states where display=1 and city={$city}  order by s_order ASC , name ASC  ";
  //	echo $sql;
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) 
	{    
    while ($row = $DB->fetch_row($result)) 
		{
			$selected = ($row['id'] == $did) ? "selected" : "" ;       
			$textout .= "<option value=\"{$row['id']}\" {$selected} >" . $func->HTML($row['name']) . "</option>";
    }
	}	
   
  return $textout;
}
 

//get_list_dealer
function get_list_dealer ()
{
  global $DB, $func, $conf, $vnT;
  $textout = "";
	$key = $_POST['key'];
	$cat_id = (int)$_POST['cat_id'];
  $city = (int)$_POST['city'];
  $state = (int)$_POST['state'];
	
	$where = '';
	
	if($cat_id){
		$where  .= " AND cat_id='".$cat_id."'";
	}
	if($city){
		$where  .= " AND city='".$city."'";
	}
	if($state){
		$where  .= " AND state='".$state."'";
	}
	if($key){	 
		$where .= " and (title  like '%" . $key . "%'  OR address like '%" . $key . "%' )";	
	}
  $sql = "SELECT * FROM dealer n, dealer_desc nd		
					WHERE n.did=nd.did AND display=1 AND lang='$vnT->lang_name' $where 
					ORDER BY d_order ASC , date_post DESC ";
		$result = $vnT->DB->query($sql);
		if ($num = $vnT->DB->num_rows($result)){
      $i = 0; $list= "";	 $w = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
      while ($row = $DB->fetch_row($result)){
				$info = "<span class='icon_address'>".$row['full_address']."</span>";
				if($row['phone'])
					$info .= "<span class='icon_phone'>".$row['phone']."</span>";
				if($row['fax'])
					$info .= "<span class='icon_fax'>".$row['fax']."</span>";
				if($row['email'])
					$info .= "<span class='icon_email'>".$row['email']."</span>";
				if($row['website'])
					$info .= "<span class='icon_web'>".$row['website']."</span>";
				
				
				if ($row['picture'] || $vnT->setting['nophoto']) 
				{
					$picture = ($row['picture']) ? "dealer/".$row['picture'] : "dealer/".$vnT->setting['pic_nophoto'];
					$pic = $vnT->func->get_pic_modules($picture, $w, 0, " alt='".$row['title']."' title='".$row['title']."' ", 1, 0);
				} else {
					$pic = "";
				}
				
				//$pic = ($row['picture']) ? $vnT->func->get_pic_modules("dealer/".$row['picture'],70,0," alt='".$row['title']."' ",1,0) : "<img src='".ROOT_URI . "modules/dealer/images/nophoto.gif' />";
				$list .= "<div class=\"item\"><a href='javascript:void(0)' onclick=\"activeMap('".$row['did']."','','','')\">
                                        <div class=\"i-description\">
                                            <p class=\"name\">".$row['title']."</p>
                                            <p class=\"address\">".$row['full_address']."</p>
                                        </div>
                                        <div class=\"clear\"></div>
                                    </a></div>";
			}
			$mess_num =  str_replace("{num}",$num,$vnT->lang['dealer']['mess_result']);
		}else{
			$list= "<div class='noItem'>".$vnT->lang['dealer']['mess_no_dealer']."</div>";	
		}
   	$arr_json['html'] = $list; 
 		$arr_json['mess_num'] = $mess_num;	 		
		$json = new Services_JSON( );
	  $textout = $json->encode($arr_json);

 		return $textout;
}



echo $jsout;
$vnT->DB->close();
?>