String.prototype.getQueryHash = function (name, defaultVal) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\#&$]" + name + "=([^&#]*)"),
		results = regex.exec(this);
	return results == null ? (defaultVal == undefined ? "" : defaultVal) : decodeURIComponent(results[1].replace(/\+/g, " "));
};
function cityChange(city_value){
	var mydata =  "city="+city_value+'&lang='+lang;
	$.ajax({
		async: true,
		url: ROOT+"modules/dealer/ajax/ajax.php?do=option_state",
		type: 'POST',
		data: mydata ,
		success: function (html) {
			$("#state").html(html) ;
		}
	});
} 
$(document).ready(function(){	 
	$('#btn_list').click(function () {
    var $this = $(this);
    if ($this.hasClass('active')) {
      $this.removeClass();
      $('ul.list-filters').css({
        "display": "none"
      });
    } else {
      $this.addClass('active');
      $('ul.list-filters').css({
        "display": "block"
      });
    }
  });
});

