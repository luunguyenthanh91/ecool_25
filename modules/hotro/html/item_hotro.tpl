<!-- BEGIN: item_hotro -->

<div class="col-12 col-lg-6">
  <div class="support-portfolio">
    <div class="row">
      <div class="col-8">
        <h3 class="mt-0 fs-13 font-weight-bold"> {data.title}</h3>
        <p class="fs-10"> {data.short}</p><a class="fs-10 text-center text-blue" href="{data.link}">{LANG.hotro.detail_hotro}<img class="arrowdown" src="{DIR_IMAGE}/arrowdown.png"></a>
      </div>
      <div class="col-4"><img class="pt-2 float-right img-info-support" src="{data.src}"></div>
    </div>
  </div>
</div>

<!-- END: item_hotro -->