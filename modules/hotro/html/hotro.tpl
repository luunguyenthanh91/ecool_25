<!-- BEGIN: modules -->
<div class="page-support">
      <div class="header container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="#">{LANG.hotro.f_navation}</a></li>
          </ol>
        </nav>
      </div>
      <div class="container">
        <div class="banner mb-5" style="background-image:url('{DIR_IMAGE}/support/ic-29.png')">
          <div class="banner__content">
            <h1 class="fs-18 fs-lg-28 font-weight-bold">{LANG.hotro.giupgi}</h1>
            <div class="input-group form-sm pt-3">
              <form class="question-form">
                <input class="w-100 form-control my-0 py-1 lime-border" type="text" placeholder="{LANG.hotro.cauhoicuaban}" aria-label="Search">
                <button class="btn" type="submit"><img src="{DIR_IMAGE}/search.png"></button>
              </form>
            </div><span class="text-white-50 font-italic">{LANG.hotro.taisao}</span>
          </div>
        </div>
      </div>
      <div class="container hotro-box">
        <div class="row">
          {data.focus_hotro}
        </div>
      </div>
      <section class="bg-gray pt-5 pb-5">
        <div class="container hotro-cont">
          <div class="row">
            <div class="col-12 text-center">
              <h1 class="text-center fs-28 font-weight-bold"> {LANG.hotro.timtt}</h1>
            </div>
            {data.main}
           <!--  <div class="col-12 text-center pt-5"><a class="fs-10 text-blue" href="#">Hiển thị thông tin nhiều hơn<img class="arrowdown" src="{DIR_IMAGE}/arrowdown.png"></a></div> -->
          </div>
        </div>
      </section>
      {data.nd_hotro}
    </div>
<!-- END: modules -->

<!-- BEGIN: html_list -->
{data.list_hotro}
<!-- {data.nav} -->

<!-- END: html_list -->


<!-- BEGIN: html_detail -->
<div class="news-detail">
      <div class="container">
        <nav class="fs-10 font-weight-light" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="/">{LANG.hotro.f_navation}</a></li>
            
          </ol>
        </nav>
      </div>
      <div class="container container--790 pb-5">
        <div class="row">
          <div class="col-12 text-center fs-16 fs-lg-31 font-weight-bold">
            <h1>{data.title}</h1>
          </div>
          
          <div class="col-12 fs-10 news-content text-justify">
            {data.description}
            <br/><br/>
            
          </div>
        </div>
      </div>
      
    </div>




<!-- END: html_detail -->

<!-- BEGIN: item_other -->
<div class="item">
  <div class="i-img">
    <a href="{data.link}" title="{data.title}">
      <img src="{data.src}" alt="{data.title}" />
    </a>
  </div>
  <div class="i-desc">
    <div class="i-name">
      <h3><a href="{data.link}" title="{data.title}">{data.title}</a></h3>
    </div>
    {data.info}
  </div>
</div>
<!-- END: item_other -->
