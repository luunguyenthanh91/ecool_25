<?php 
session_start();
define('IN_vnT', 1);
define('PATH_ROOT', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
require_once("../../_config.php");  
require_once("../../includes/class_db.php"); 
require_once("../../includes/class_functions.php");
$vnT->DB = $DB = new DB;
$vnT->func = $func = new Func_Global;
$vnT->conf = $conf = $func->fetchDbConfig($conf);
$vnT->lang_name = (isset($_GET['lang'])) ? $_GET['lang'] : "vn";
$func->load_language('epaper');

$lang=$vnT->lang_name;
$id = (int) $_GET["eID"];

$res_s = $DB->query("SELECT * FROM epaper_setting WHERE lang='".$vnT->lang_name."' ");
$row_s = $DB->fetch_row($res_s);

$result_e = $DB->query("SELECT * FROM epaper n, epaper_desc nd
												WHERE n.e_id=nd.e_id AND n.e_id = $id AND lang = '{$vnT->lang_name}' ");
$row_e = $DB->fetch_row($result_e); 
 
$list_book = "";
$src_cover = $conf['rooturl'].'vnt_upload/epaper/book_image/'.$row_e['picture'];  

$list_slider = '<ul id="slider">';
$list_slider .= '<li class="page1">	<img src="'.$src_cover.'" alt="" />	</li>';
$list_page = '<li data-page="1" data-address="page-1"></li>';
$i=2;			
$sql = "SELECT * FROM epaper_picture WHERE e_id=$id ORDER BY pic_order ASC ,id ASC ";
$result = $vnT->DB->query($sql);
while($row = $vnT->DB->fetch_row($result)){ 
	$src = $conf['rooturl'].'vnt_upload/epaper/'.$row['picture'];  
	$pic_name = ($row['pic_name']) ? $row['pic_name'] : "&nbsp;";
	$page = $i-1;	
	$pic_desc = ($row['description']) ? '<div class="page_book">'.$row['description'].'</div>' : '';
	if($i%2==0){
		$meta="left";
		$list_book .= '<div style="background-image:url('.$src.');">				
				'.$pic_desc.'
				<div class="meta '.$meta.'">
					<span class="num">'.$page.'</span>					
					<span class="description">'.$pic_name.'</span>
				</div>
			</div>';
		$list_page .= '<li data-page="'.$i.'" data-address="page-'.$i.'-'.($i+1).'"></li>';
	}	else {
		$meta = "right";
		$list_book .= '<div style="background-image:url('.$src.');">
				'.$pic_desc.'
				<div class="meta '.$meta.'">
					<span class="description">'.$pic_name.'</span>
					<span class="num">'.$page.'</span>
				</div>
			</div>';
		$list_page .= '<li data-page="'.$i.'" data-address="page-'.($i-1).'-'.$i.'"></li>';	
	}
	$list_slider .= '<li class="page'.$i.'"><img src="'.$src.'" alt=""/></li>';
	$i++;
}
$list_slider .= '</ul>';
$data['title'] = $vnT->func->HTML($row_e['title']);
$data['description'] = $row_e['description'];
$data['src_cover'] = $src_cover;
$data['list_page'] = $list_page;
$data['list_book'] = $list_book;
$data['list_slider'] = $list_slider;
$data['background_flash'] = ($row_s['background_flash']) ? "style='background:url(".$row_s['background_flash'].")'" : "";
$data['link_logo'] = ($row_s['link_logo']) ? $row_s['link_logo']  : "http://".$_SERVER['HTTP_HOST'] ;
$data['logo_flash'] = ($row_s['logo_flash']) ?  $row_s['logo_flash'] : $vnT->dir_images."/logo.png" ;
if($row_e['link_down']){
	$data['file_download'] = '<li><a href="'.$row_e['link_down'].'" class="download" title="Download" target="_blank"></a></li>';
}
?>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <?php echo $conf['meta_extra']?>
	<title><?php echo $func->HTML($row_e['title']) ?></title>
	<link rel="stylesheet" href="<?php echo $conf['rooturl']; ?>modules/epaper/css/book_styles.css" type="text/css" />
	<link href="<?php echo $conf['rooturl'];?>skins/default/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $conf['rooturl'] ; ?>modules/epaper/css/epaper.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $conf['rooturl'] ; ?>modules/epaper/css/detail.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $conf['rooturl'] ;?>js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>modules/epaper/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>modules/epaper/js/turn.min.js"></script>
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>modules/epaper/js/jquery.address-1.5.min.js"></script>
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>modules/epaper/js/onload.js"></script>
	<script type="text/javascript" src="<?php echo $conf['rooturl'];?>modules/epaper/js/epaper.js"></script>
	<style type="text/css" media="screen">
    .wrapper {
      max-width: 1190px;
      margin: 0 auto;
      padding: 0 18px;
    }
    img {
      border: 0px;
      max-width: 100%;
    }
  </style>
  <?php echo $conf['extra_footer'];?>
</head>
<body style="background: #f2f2f2;">
	<div id="page">
    <section id="address">
     	<ul><?php echo $data['list_page']; ?></ul>
   	</section>
   	<div id="book">  
      <div id="cover" style="background-image:url(<?php echo $data['src_cover']; ?>);" >&nbsp;</div> 
      <?php echo $data['list_book']; ?>
		</div>
    <a class="nav_arrow prev"></a>
    <a class="nav_arrow next"></a>
  </div>
  <footer id="footer">
    <div class="wrapper">
      <div class="logon">
        <a href="<?php echo $data['link_logo']; ?>" id="logo" target="_blank" style="max-width: 170px;">
          <img src="<?php echo $data['logo_flash']; ?>" alt="" />
        </a>
      </div>
      <div class="all_nav">
        <nav id="center" class="menu">
          <ul>
          	<?php echo $data['file_download']; ?>
            <li>
              <a class="zoom_in" title="Zoom In"></a>
            </li>
            <li>
              <a class="zoom_out" title="Zoom Out"></a>
            </li>
            <li>
              <a class="zoom_original" title="Zoom Original (scale 1:1)">1:1</a>
            </li>
            <li>
              <a class="home" title="Show Home Page"></a>
            </li>
          </ul>
        </nav>
        <nav id="right" class="menu">
          <ul>
            <li>
              <span><?php echo $vnT->lang['epaper']['go_page']; ?></span>
            </li>
            <li class="goto">
              <input type="text" id="page-number" placeholder="01" />
              <button type="button">Tìm</button>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </footer>
  <div class="overlay" id="contact">
   	<form>
      <a class="close">X</a>
      <fieldset>
       	<h3>Liên hệ</h3>
       	<p>
          <input type="text" value="Họ tên" id="name" class="req" />
       	</p>
       	<p>
          <input type="text" value="Email" id="email" class="req" />
       	</p>
       	<p>
          <textarea id="message" class="req">Lời nhắn</textarea>
       	</p>
       	<p>
          <button type="submit">Gửi đi</button>
       	</p>
      </fieldset>
      <fieldset class="thanks">
       	<h3>Message Sent</h3>
       	<p>This window will close in 5 seconds.</p>
      </fieldset>
   	</form>
  </div>
</html>