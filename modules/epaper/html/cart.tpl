<!-- BEGIN: modules -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    
    <td id="vnt-main"  valign="top">
    
    <div id="BoxShopping">
{data.nav_shopping}
{data.mess}
{data.main}
</div>
    
    </td>
    <td id="vnt-sidebar" valign="top">{data.box_sidebar} </td>
  </tr>
</table> 
<!-- END: modules --> 

<!-- BEGIN: html_cart -->
<br />
<p>{LANG.epaper.note_cart}</p>
<form action="{data.link_action}" method="post" name="f_cart">
  <table width="100%" border="0"  cellpadding="2" cellspacing="1" class="border_cart">
        <tr  >
          <td class="row_title_cart"  width="5%"  align="center"><b>{LANG.epaper.stt}</b> </td>
          <td class="row_title_cart"  width="10%"  align="center"><b>{LANG.epaper.pic}</b> </td>
          <td class="row_title_cart"  width="40%"  align="center"><b>{LANG.epaper.p_name}</b> </td>          
					<td class="row_title_cart" width="15%"  align="center"><b>{LANG.epaper.price}</b></td>
          <td class="row_title_cart"  width="10%"  align="center"><b>{LANG.epaper.quantity}</b> </td>
					<td class="row_title_cart" width="15%"  align="center"><b>{LANG.epaper.total}</b></td>
          <td class="row_title_cart"  width="5%" >&nbsp;</td>
        </tr>
        {data.row_cart}
        <tr  >
          <td class="row_total_cart" colspan="5"  align="right"><b>{LANG.epaper.basket_total} </b> </td>
          <td class="row_total_cart" align="right" style="padding-right:3px"><b>{data.cart_total}</b></td>
          <td class="row_total_cart">&nbsp;</td>
        </tr>
      </table>
<br />
<p align="center">
	<button  id="btnContinue" name="btnContinue" type="button" class="btn"  onClick="location.href='{data.link_continue_shopping}'" value="{LANG.epaper.btn_continue_shopping}" ><span >{LANG.epaper.btn_continue_shopping}</span></button> &nbsp;
  <button  id="btnEmpty" name="btnEmpty" type="button" class="btn"  onClick="location.href='{data.link_remove}'" value="{LANG.epaper.btn_empty_cart}" ><span >{LANG.epaper.btn_empty_cart}</span></button> &nbsp; 
  <button  id="btnEdit" name="btnEdit" type="submit" class="btn"   value="{LANG.epaper.btn_recalculate}" ><span >{LANG.epaper.btn_recalculate}</span></button> &nbsp;  
  <button  id="btnCheckout" name="btnCheckout" type="submit" class="btn" value="{LANG.epaper.btn_recalculate}" ><span >{LANG.epaper.btn_check_out}</span></button> &nbsp;     
	<input type="hidden" name="modify" value="modify" />
  <input type="hidden" name="link_ref" value="{data.link_ref}" />
</p>
</form>

<p style="text-align:center" class="font_err">{LANG.epaper.note_re_calculate}</p>
<br />

<!-- END: html_cart -->



<!-- BEGIN: html_empty_cart -->
<p style="text-align:center" ><strong class='font_err'>{LANG.epaper.empty_cart}</strong></p>
<p>{LANG.epaper.note_empty_cart}</p>
<p style="text-align:center" class="err">{data.back_epaper}</p>
<!-- END: html_empty_cart -->



