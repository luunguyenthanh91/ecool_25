<!-- BEGIN: html_paper -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html style="height:100%; width:100%;" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{CONF.indextitle} {CONF.extra_title}</title>
<meta name="robots" content="index, follow"/>
<meta name="author" content="{CONF.indextitle}"/>
<meta name="description" CONTENT="{CONF.meta_description}" />
<meta name="keywords" CONTENT="{CONF.meta_keyword}" />
<link rel="SHORTCUT ICON" href="{CONF.rooturl}favicon.ico" type="image/x-icon" />
<link rel="icon" href="{CONF.rooturl}favicon.ico" type="image/gif" />
<link href="{DIR_MOD}/css/paper.css" rel="stylesheet" type="text/css" />
<link href="{DIR_MOD}/css/detail.css" rel="stylesheet" type="text/css" />
<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var cmd = "{CONF.cmd}";
</script>
<script type="text/javascript" src="{DIR_MOD}/flash/swfobject.js"></script>
<script type="text/javascript" src="{DIR_MOD}/flash/swfaddress.js"></script>
<script type="text/javascript" src="{DIR_MOD}/flash/swffit.js"></script>
</head>
<body bgcolor="#666666" style="margin:0">
			<div id="flashcontent">
			</div>		
</body>

<script type="text/javascript">
   	var flashvars = {};
	flashvars.books_url_date="{DIR_MOD}/flash/xml/books.php?eID={data.e_id}";
	flashvars.initServices=true;
	var params = { allowfullscreen:true,allowScriptAccess:"always"};
	var attributes = { id:'flashObject',bgcolor:'#262C2C' }; // give an id to the flash object
	
	swfobject.embedSWF("{DIR_MOD}/flash/book.swf", "flashcontent", "100%", "100%", "8", "expressInstall.swf", flashvars, params,attributes);
	//swffit.fit("flashObject",800,650);
</script>

</html>
<!-- END: html_paper -->  
 