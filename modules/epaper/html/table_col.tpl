<!-- BEGIN: table_col -->
<h3>{data.name}</h3>

<table width="100%" border="0" cellspacing="0" cellpadding="0" >
  <tr>
        <td valign="top" style="padding-right:10px;" ><div class="short" >{data.description}</div></td>
        <td width="250" valign="top" class="tdRight" >
              <table  border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td align="center"><div class="img">{data.pic}</div></td>
                </tr>
                 <tr>
                  <td align="center">{data.view_detail}</td>
                </tr>
          </table>
          <div class="desc">{data.downloads}</div>

        </td>
  </tr>
</table>

<!-- END: table_col -->
