<!-- BEGIN: html_paper -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{CONF.indextitle} {CONF.extra_title}</title>
<meta name="robots" content="index, follow"/>
<meta name="author" content="{CONF.indextitle}"/>
<meta name="description" CONTENT="{CONF.meta_description}" />
<meta name="keywords" CONTENT="{CONF.meta_keyword}" />
<link rel="SHORTCUT ICON" href="{CONF.rooturl}favicon.ico" type="image/x-icon" />
<link rel="icon" href="{CONF.rooturl}favicon.ico" type="image/gif" />
<link href="{DIR_MOD}/css/paper.css" rel="stylesheet" type="text/css" />

<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var cmd = "{CONF.cmd}";
</script>
<script type="text/javascript" src="{DIR_JS}/javascript.js"></script>
</head>
<body>
<table width="850" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr><td align="left" valign="top" >
		<img src="{DIR_MOD}/images/img_flash.jpg" width="850" height="620" />
  </td></tr>
  <tr><td align="center" height="1" >
		<img src="{DIR_MOD}/images/hr_footer.gif"  height="1" />
  </td></tr>
  <tr>
  	<td style="padding-top:10px">
    	<table border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td class="logo_footer"><a href="{CONF.rooturl}main.html"><img src="{DIR_IMAGE}/logo_footer.png" width="140" height="50" /></a></td>
          <td class="copyright">{LANG.global.copyright}<br>[MADEBY]</td>
        </tr>
      </table>
  	</td>
  </tr>
</table>
</body>
</html>
<!-- END: html_paper -->  
 
