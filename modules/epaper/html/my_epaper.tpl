<!-- BEGIN: modules -->
<script type="text/javascript">
	 var mess_error = new Array(); 
	 mess_error['please_chose_item']   	= "{LANG.epaper.please_chose_item}";
	 mess_error['error_only_epaper']   	= "{LANG.epaper.error_only_member}";
	 mess_error['are_you_sure_del']   	= "{LANG.epaper.are_you_sure_del}";
	</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>	
  <td   valign="top" id="block_middle">  
  {data.main}
</td>
<td width="250" valign="top" id="block_right">{data.box_sidebar}</td> 
</tr>
</table> 

<!-- END: modules -->
 

<!-- BEGIN: html_edit -->
<br />

<script language=javascript>
	function checkform(f) {			

		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		
		if (f.cat_id.value ==0) {
			alert("{LANG.epaper.err_empty_categoty}");
			f.cat_id.focus();
			return false;
		}
 	 
		if (f.p_name.value == '') {
			alert("{LANG.epaper.err_empty_p_name}");
			f.p_name.focus();
			return false;
		}
		
  		
		return true;
	}
  
</script>
<script src="{CONF.rooturl}plugins/editors/nicEdit/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function() {
//	new nicEditor({fullPanel : true}).panelInstance('description');
	
	new nicEditor({fullPanel : true,iconsPath : ROOT+'plugins/editors/nicEdit/nicEditorIcons.gif'}).panelInstance('description');

});
</script>

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  onSubmit="return checkform(this);" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="mytable">
		
    <tr > 
      <td class="row1" >{LANG.epaper.category} : </td>
      <td class="row0" >{data.list_cat} <span class="font_err">(*)</span></td>
    </tr>
    
    <tr >
     <td class="row1" >{LANG.epaper.p_name} : </td>
     <td class="row0"><input name="p_name" type="text" size="60" maxlength="250" value="{data.p_name}"  class="textfield" > <span class="font_err">(*)</span></td>
    </tr>
   

    <tr >
     <td class="row1" width="20%">{LANG.epaper.picture}: </td>
     <td class="row0">{data.pic}
      <input name="image" type="file" id="image" size="30" maxlength="250"> (Only *.jpg, *.gif)
    </td>
    </tr> 
      
    <tr >
     <td class="row1" >{LANG.epaper.description}: </td>
     <td class="row0"> <textarea name="description" id="description" style="width:100%" rows="20" class="textarea"   >{data.description}</textarea></td>
    </tr>   
   
   
      
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
      <input type="hidden" id="map_lat" name="map_lat" value="{data.map_lat}" />
			<input type="hidden" id="map_lng" name="map_lng" value="{data.map_lng}" />
			<input type="hidden" id="map_information" name="map_information" value="{data.map_information}" />	
      
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
 
<br />

<!-- END: html_edit --> 


<!-- BEGIN: html_edit_pic -->
<br />
<script type="text/javascript">
	/* <![CDATA[*/
	
	var count_pic = 3 ;
	var num = {data.num_pic}
	function create_pic_pro() 
	{
		if(count_pic < 5) 
		{	
			var newDivStr='';
			newDivStr="<input name='pic_detail[]' type='file' id='pic_detail' size='40' > (*.jpg,*.gif) Only !";
			
			var newdiv = document.createElement('div');
			newdiv.innerHTML = newDivStr;
	
		
			// Create Elements
			var pic_tr = document.createElement("tr");
			var pic_td1 = document.createElement("td");
			var pic_td2 = document.createElement("td");		
			var pic_pro_count = document.createTextNode("Picture " + (count_pic+1) +" :");
			count_pic++;		
			num++;
			// Elements - TD/TR
			pic_tr.setAttribute('id', "pic-pro-" + count_pic);
			pic_td1.setAttribute('class', "row1");
			pic_td2.setAttribute('class', "row0");
			
			// Appending
			pic_tr.appendChild(pic_td1);
			pic_tr.appendChild(pic_td2);
			pic_td1.appendChild(pic_pro_count);		
			pic_td2.appendChild(newdiv);
			document.getElementById("pic_epaper").appendChild(pic_tr);
			
			document.getElementById("num").value = parseInt(document.getElementById('num').value) +1;
		}else{
			alert("{LANG.epaper.err_max_pic_upload}");
		}

	}
	function remove_pic_pro() {
		if(count_pic == 1) {
			alert("{LANG.epaper.err_min_pic_upload}");
		} else {
			document.getElementById("pic_epaper").removeChild(document.getElementById("pic-pro-" + count_pic));
			count_pic--;
			document.getElementById("num").value = parseInt(document.getElementById('num').value) -1;				
		}
	}
	

	/* ]]> */
</script>
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic}</td>
    <td style="padding:5px;line-height:20px;">MS : <strong class="font_err">{data.maso}</strong><br />
				{LANG.epaper.p_name}: <strong>{data.p_name}</strong><br />
        ID : #<strong class="font_err">{data.e_id}</strong>
    		</td>
  </tr>
</table>
<br />
<p align="center" ><strong>{LANG.epaper.list_picture_epaper}</strong></p>
<table width="100%" border="0" cellspacing="2" cellpadding="5">
  <tr> {data.list_pic_old} </tr>
</table>

{data.err}

<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="mytable">
 
    <tbody id="pic_epaper">
    
    <!-- BEGIN: html_row -->
    <tr id="pic-pro-{row.stt}" >
     <td class="row1" width="20%">Picture {row.stt} : </td>
     <td class="row0"><input name="pic_detail[]" type="file" id="pic_detail" size="40" maxlength="250"> (*.jpg,*.gif) Only !
     
     </td>
 		</tr>
    
    
    <!-- END: html_row -->
    
    </tbody>
    
    <tr height="20">
      <td class="row1" >&nbsp; </td>
      <td  class="row2"><a href="javascript:create_pic_pro();">[Add more picture]</a> &nbsp; | &nbsp; <a href="javascript:remove_pic_pro();">[Remove picture]</a>
      <span id="message_pic">&nbsp;</span>
      </td>
    </tr>
 
    
	</table>
  <br />

  <p align="center">
		    <input type="hidden" name="num" id="num" size="5" value="{data.num}" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnSubmit" value="Submit" class="button"> &nbsp;&nbsp;
				<input type="reset" name="btnReset" value="Reset" class="button">
  </p>
</form>
<br>
<!-- END: html_edit_pic -->

<!-- BEGIN: html_option -->
<br />
<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center"  bgcolor="#CCCCCC">
  <tr bgcolor="#FFFFFF">
    <td width="150" align="center" style="padding:5px;">{data.pic}</td>
    <td style="padding:5px;line-height:20px;">MS : <strong class="font_err">{data.maso}</strong><br />
				{LANG.epaper.p_name}: <strong>{data.p_name}</strong><br />
        ID : #<strong class="font_err">{data.e_id}</strong>
    		</td>
  </tr>
</table>
<br />
<form id="form1" name="form1" method="post" action="{data.link_action}">
 
<table width="100%" border="0" cellspacing="0" cellpadding="2">
 <tr>
    <td height="25"  style="border-bottom:1px solid #B84120 ;"><h2 class="title">{LANG.epaper.option_text}</h2>
    </td>
  </tr>
</table>
<br />

<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center">
{data.list_option}
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="2">
 <tr>
    <td height="25"  style="border-bottom:1px solid #B84120 ;"><h2 class="title">{LANG.epaper.option_check_box}</h2></td>
  </tr>
</table>
<br />
{data.list_option_check}
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td  align="center">
	<input name="have_option" type="hidden" value="{data.have_option}" />
	<input name="btnSubmit" type="submit" value=" {LANG.epaper.btn_update} " class="button" /> &nbsp; 
	&nbsp;<input name="reset" type="reset"  value=" {LANG.epaper.btn_reset} " class="button"/></td>
  </tr>
</table>

</form>
<br /> 
<!-- END: html_option -->


<!-- BEGIN: html_manage -->
<form name="f_Search" method="post" action="{data.link_search}" onsubmit="return checkform(this)" >

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="bottom">
    <table   border="0" cellpadding="2" cellspacing="2">
  
  <tr>
  <td  width="25"  align="center" ><img src="{DIR_MOD}/images/arr_down.jpg" /></td>
    <td ><a href="javascript:del_selected('{data.action_del}');"><img src="{DIR_MOD}/images/delete.gif" align="absmiddle" />&nbsp;{LANG.epaper.del_epaper}</a></td>
    <td ><a href="{data.link_add}"><img src="{DIR_MOD}/images/add.gif" align="absmiddle" />&nbsp;{LANG.epaper.add_epaper} </a></td>    
    </tr>
</table>

    </td>
    <td width="50%" align="right" height="30">
    <table   border="0" cellspacing="2" cellpadding="2">
      <tr>
        <td><strong>{LANG.epaper.search} :</strong></td>
        <td><input name="key" id="key" type="text" class="textfiled" value="{INPUT.key}" style="width:150px;" /></td>
        <td><input name="btnSearch" type="submit" value=" Tìm " class="button" /></td>
      </tr>
    </table>

       </td>
  </tr>
</table>
</form>

{data.err}
{data.table_list}
{data.nav}
<br>
<!-- END: html_manage -->


