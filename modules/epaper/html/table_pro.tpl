<!-- BEGIN: table_pro -->
<!-- <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
  <div class="catalogue">
    <div class="img">
      <img src="{data.src}" alt="{data.title}">
      <div class="tools">
        <a class="download" href="{data.link_download}" target="_blank" rel="nofollow">
        	<i class="fa fa-download"></i>
        </a>
        <a class="view" href="{data.link}" target="_blank" rel="nofollow"><i class="fa fa-search-plus"></i></a>
      </div>
    </div>
    <div class="tend"><h3><a href="{data.link}" target="_blank" rel="nofollow">{data.title}</a></h3></div>
  </div>
</div> -->
<div class="col-lg-4 col-12 item-support">
    <div class="item">
          <img src="{data.src}" alt="" class="banner_download"/>
          <p class="text-center support-title">{data.title}</p>
          <div class="btn-box">
              <a href="{data.link_download}" target="_blank"> <img class="img-icon-left" src="{DIR_IMAGE}/seen.png" alt="{data.title}"></a>
              <a href="{data.link_download}" download><img class="img-icon-right" src="{DIR_IMAGE}/download.png" alt=""></a>
          </div>
    </div>
</div>
<!-- END: table_pro -->
