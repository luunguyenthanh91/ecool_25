<!-- BEGIN: modules -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>    
    <td id="vnt-main" >
    
   <script language="JavaScript" type="text/javascript">
var mess = "{LANG.epaper.mess_cancel}";
function check_cancel(theURL) {
	if (confirm(mess)) {
		window.location.href=theURL;
	}			
}
function submitDoc(formName) { 
  var obj;
  if (obj=findObj(formName)!=null) {
		findObj(formName).submit(); 
  }else {
		alert('The form you are attempting to submit called \'' + formName + '\' couldn\'t be found. Please make sure the submitDoc function has the correct id and name.');
	}
}
</script>
<div id="BoxShopping">
{data.nav_shopping}
<form action="{data.link_action}" method="post" name="f_confirm" onsubmit="return checkform(this);">
{data.main}
</form>

<table  border="0" align="center" cellpadding="0" cellspacing="0">
   <tr>
    <td>&nbsp;</td>
    <td height="25"  valign="middle" align="center"><strong>{LANG.epaper.order_progress}</strong><br /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="{DIR_MOD}/images/prog_left.gif" width="130" height="19" alt="" /></td>
    <td><img src="{DIR_MOD}/images/orange.gif" width="130" height="19" alt="" /></td>
    <td><img src="{DIR_MOD}/images/prog_right_grey.gif" width="130" height="19" alt="" /></td>
  </tr>
  <tr>
    <td height="25">0% </td>
     <td align="center" >50%</td>
     <td  align="right">100%</td>
  </tr>
</table>
</div>
     </td>
     <td id="vnt-sidebar" >{data.box_sidebar} </td>
  </tr>
</table> 
<!-- END: modules -->  
 


<!-- BEGIN: html_order_information -->
<script language="JavaScript" type="text/javascript">
function checkform(f) {			
		d_name = f.d_name.value;
		if (d_name == '') {
			alert("{LANG.epaper.err_fullname_empty}");
			f.d_name.focus();
			return false;
		}
        		
		var re =/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$/gi;
		email = f.d_email.value;
		if (email == '') {
			alert("{LANG.epaper.err_email_empty}");
			f.d_email.focus();
			return false;
		}
		if (email != '' && email.match(re)==null) {
			alert("{LANG.epaper.err_email_invalid}");
			f.d_email.focus();
			return false;
		}
		d_address = f.d_address.value;
		if (d_address == '') {
			alert("{LANG.epaper.err_address_empty}");
			f.d_address.focus();
			return false;
		}
		
 		
    d_phone = f.d_phone.value;
		if (d_phone == '') {
			alert("{LANG.epaper.err_phone_empty}");
			f.d_phone.focus();
			return false;
		}

		c_name = f.c_name.value;
		if (c_name == '') {
			alert("{LANG.epaper.err_fullname_empty}");
			f.c_name.focus();
			return false;
		}
		c_address = f.c_address.value;
		if (c_address == '') {
			alert("{LANG.epaper.err_address_empty}");
			f.c_address.focus();
			return false;
		}
		
		 
		
    c_phone = f.c_phone.value;
		if (c_phone == '') {
			alert("{LANG.epaper.err_phone_empty}");
			f.c_phone.focus();
			return false;
		}
		
		security_code = f.security_code.value;
		if (security_code == '') {
			alert("{LANG.epaper.err_security_code_empty}");
			f.security_code.focus();
			return false;
		}
		
		return true;
		
}

	function doGetFiled (f){
		if (f.chSame.checked==true){
			f.c_name.value = f.d_name.value ;
			f.c_address.value = f.d_address.value ;
  			f.c_phone.value = f.d_phone.value ;
		}else{
			f.c_name.value = '' ;
			f.c_address.value = '' ;
 			f.c_phone.value = '' ;
		}
	}
</script>

{data.list_cart}
<br />
{data.err}
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
	<tr>
    <td colspan="2" class="shopping_title"><strong>{LANG.epaper.f_checkout_payment}:</strong> </td>
  </tr>
  <tr>
    <td width="25%"><strong>{LANG.epaper.full_name}: </strong>&nbsp;<font class="font_err">(*)</font></td>
    <td ><input type="text"  class="textfiled" name="d_name" value="{data.d_name}" size="50" /></td>
  </tr>
    <tr>
    <td><strong>{LANG.epaper.email}: </strong> &nbsp;<font class="font_err">(*)</font></td>
    <td><input type="text" class="textfiled"  name="d_email" value="{data.d_email}" size="50" /></td>
  </tr>
  <tr>
    <td><strong>{LANG.epaper.address}: </strong>&nbsp;<font class="font_err">(*)</font></td>
    <td><input type="text" class="textfiled"  name="d_address" value="{data.d_address}" size="50" /></td>
  </tr>
 
  

  <tr>
    <td><strong>{LANG.epaper.phone}: </strong> &nbsp;<font class="font_err">(*)</font></td>
    <td><input type="text" class="textfiled" name="d_phone" value="{data.d_phone}" size="20" /></td>
  </tr>
</table>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
  <tr>
    <td colspan="2"><input name="chSame" type="checkbox" value="" onclick="javascript:doGetFiled(this.form);" /> &nbsp; {LANG.epaper.mess_check_shipping} </td>
  </tr>
  <tr>
    <td colspan="2" class="shopping_title"><strong>{LANG.epaper.f_checkout_shipping}:</strong> </td>
  </tr>
  <tr>
    <td width="25%"><strong>{LANG.epaper.full_name}: </strong>&nbsp;<font class="font_err">(*)</font></td>
    <td ><input type="text"  class="textfiled" name="c_name" value="" size="50" /></td>
  </tr>
  <tr>
    <td><strong>{LANG.epaper.address}: </strong>&nbsp;<font class="font_err">(*)</font></td>
    <td><input type="text" class="textfiled"  name="c_address" value="" size="50" /></td>
  </tr>
 
  
  <tr>
    <td><strong>{LANG.epaper.phone}: </strong> &nbsp;<font class="font_err">(*)</font></td>
    <td><input type="text" class="textfiled" name="c_phone" value="" size="20" /></td>
  </tr>

</table>
<br />
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center">
  <tr>
  	<td>&nbsp;</td>
    <td  class="font_err">{LANG.epaper.l_comment}</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
    <td  ><textarea name="comment" class="textarea" cols="40" rows="5">{data.comment}</textarea></td>
  </tr>
	<tr>
    <td width="25%" ><strong>{LANG.epaper.security_code} :</strong> &nbsp;<font class="font_err">(*)</font></td>
		<td align="left" ><input name="security_code" type="text" id="security_code" size="10" maxlength="6" value="{data.security_code}" class="textfiled" />&nbsp;<img src="{data.ver_img}" align="absmiddle" /></td>
  </tr>
</table><br />

<p style="text-align:center">
 <button  id="btnConfirm" name="btnConfirm" type="submit" class="btn" value="{LANG.epaper.btn_confirm}" ><span >{LANG.epaper.btn_confirm}</span></button>  
</p>

<br>
<!-- END: html_order_information --> 


<!-- BEGIN: html_transfer -->
<script language="JavaScript" type="text/javascript">
	window.onload = Redirector;
	function Redirector() {
		document.f_confirm.submit();			
	}
</script>
			
<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
      <tr> <td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
							<td align="center" >{data.hidden_value}</td>
						</tr>
						<tr>
							<td align="center" height="100">Please waitting ...... <br />
					<img src="{DIR_MOD}/images/progress.gif"  alt="" title="" onload="submitDoc('f_confirm')"  />
					</td>
				</tr>
				<tr>
		</table>
	</td>
	</tr>
</table>
<!-- END: html_transfer -->

