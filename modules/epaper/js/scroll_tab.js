
/***********************
* ABN javascript
* carousel js and dependencies
**************************/
/**
* global function ala prototype lib
*/

function $(name){
	return document.getElementById(name);
}
Function.prototype.bind = function(obj) {
	var method = this,
		temp = function() {
			return method.apply(obj, arguments);
		};
	return temp;
}

/**
* instanciate global BBY js object
*/
BBY = {};
/**
* set util namespace
*/
BBY.util = {};
/**
* generic utility functions
*/
BBY.util.prototype = {
	setDisplay:function(obj, state){
		obj.style.display = state;
	},
	
	setStyleAtts:function(domnode, styleatt, value){
		domnode.style[styleatt] = value + 'px';
	},
	
	getStyleAtts:function(domnode, styleatt){
		return domnode.style[styleatt];
	},
	
	changeOpacity:function(theobj, opac){
		var obj = theobj.style;
		obj.opacity = (opac / 100);
		obj.MozOpacity = (opac / 100);
		obj.KhtmlOpacity = (opac / 100);
		obj.filter = "alpha(opacity=" + opac + ")"
	},
	findPosition:function(theobject){
		var curleft = curtop = 0;
		if (theobject.offsetParent) {
			curleft = theobject.offsetLeft;
			curtop = theobject.offsetTop;
			while (theobject = theobject.offsetParent) {
				curleft += theobject.offsetLeft
				curtop += theobject.offsetTop
			}
		}
		return [curleft,curtop];
	},
	
	findHeightWidth:function(theobject){
		var theheight = theobject.offsetHeight;
		var thewidth = theobject.offsetWidth;
		return[theheight, thewidth];
	}
}

/**
* event handling object and methods
* usage: BBY.util.Event.addListener(document, "mouseover", this); 
*/
BBY.util.Event = {};
BBY.util.Event.prototype = {
	addListener:function(obj, eventName, listener){
		if (obj.attachEvent){
			obj.attachEvent("on"+eventName, listener);
		}else if(obj.addEventListener){
			obj.addEventListener(eventName, listener, false);
		}else{
			return false;
		}
		return true;
	},
	
	removeListener:function(obj, eventName, listener){
		if(obj.detachEvent){
			obj.detachEvent("on"+eventName, listener);
		}else if(obj.removeEventListener){
			obj.removeEventListener(eventName, listener, false);
		}else{
			return false;
		}
		return true;
	}
}

/**
* set dhtml namespace
*/
BBY.dhtml = {};
/**
* Carousel javascript for ABN
* usage: BBY.dhtml.Carousel({visible:3,increment:3,contentframe:"contentframeid",content:"contentid",backbutton:"backbuttonid",forwardbutton:"forwardbuttonid"}); 
*/
BBY.dhtml.Carousel = function(args){
	this.visibleElements = args.visible;
	this.incrementNum = args.increment;
	this.contentFrame = $(""+args.contentframe+"");
	this.prevButton = $(""+args.backbutton+"");
	this.nextButton = $(""+args.forwardbutton+"");
	this.carouselContent = args.content;
	this.isRunning = false;
	this.init();
}

BBY.dhtml.Carousel.prototype = {
	init:function(){
		//set inital index to 0
		this.currentIndex = 0;
		//set initial scroll increment width to 0
		this.scrollIncrement = 0;
		//set the current increment value to 0
		this.incrementValue = 0;
		// total number of cols
		this.cols = $(""+this.carouselContent+"").getElementsByTagName("li");
		this.checkDisable();
		
		BBY.util.Event.prototype.addListener(this.prevButton, "click", this.getPrev.bind(this));
		BBY.util.Event.prototype.addListener(this.nextButton, "click", this.getNext.bind(this));
	},
	
	getPrev:function(){
		//width of the cols
		this.width = this.cols[0].offsetWidth;
		
		if((this.currentIndex - this.incrementNum) <= 0){
			this.currentIndex = 0;
		}else{
			//decrement the index
			this.currentIndex = parseInt(this.currentIndex - (this.currentIndex - this.incrementNum));
		}
		
		this.scrollIncrement = parseInt(this.currentIndex * this.width);
		
		//set interval for scrollNext function
		if(this.isRunning == true)return;
		
		this.inter = setInterval(this.scrollPrev.bind(this), 10);
		this.isRunning = true;
	},
	
	getNext:function(){
		//width of the cols
		this.width = this.cols[0].offsetWidth;
		
		//increment the index by the increment number
		this.currentIndex = parseInt((this.currentIndex + this.incrementNum));
		
		if((this.currentIndex + this.visibleElements) >= this.cols.length){
			this.currentIndex = this.currentIndex - ((this.currentIndex + this.visibleElements) - this.cols.length);
		}		
		
		this.scrollIncrement = parseInt(this.currentIndex * this.width);
		
		//set interval for scrollNext function
		if(this.isRunning == true)return;
		
		this.inter = setInterval(this.scrollNext.bind(this), 10);
		this.isRunning = true;
	},
	
	scrollNext:function(){
		if(this.incrementValue > -(this.scrollIncrement)){
			this.incrementValue = this.incrementValue - 15;
			this.contentFrame.style.left = this.incrementValue + 'px';
		}else{
			clearInterval(this.inter);
			this.checkDisable();
			this.isRunning = false;
			
		}
	},
	
	scrollPrev:function(){
		if(this.incrementValue < -(this.scrollIncrement)){
			this.incrementValue = this.incrementValue + 15;
			this.contentFrame.style.left = this.incrementValue + 'px';
		}else{
			clearInterval(this.inter);
			this.checkDisable();
			this.isRunning = false;
			
		}
	},
	
	checkDisable:function(){
		var back = ((this.currentIndex == 0)) ? BBY.util.prototype.changeOpacity(this.prevButton, 40) : BBY.util.prototype.changeOpacity(this.prevButton, 100);
		var fwd = ((this.currentIndex + this.visibleElements) >= this.cols.length) ? BBY.util.prototype.changeOpacity(this.nextButton, 40) : BBY.util.prototype.changeOpacity(this.nextButton, 100);
	}
}

