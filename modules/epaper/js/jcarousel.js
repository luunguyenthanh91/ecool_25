function mycarousel_initCallback1(carousel) {
    jQuery('#next-other_epaper').bind('click', function() {
        carousel.next();
        return false;
    });

    jQuery('#prev-other_epaper').bind('click', function() {
        carousel.prev();
        return false;
    });
};

function mycarousel_initCallback2(carousel) {
    jQuery('#next-belong_epaper').bind('click', function() {
        carousel.next();
        return false;
    });

    jQuery('#prev-belong_epaper').bind('click', function() {
        carousel.prev();
        return false;
    });
};


jQuery(document).ready(function() {
    jQuery("#other_epaper ul").jcarousel({
				initCallback: mycarousel_initCallback1,
        scroll: 2,
				buttonNextHTML: null,
        buttonPrevHTML: null
    });
		
		jQuery("#belong_epaper ul").jcarousel({
				initCallback: mycarousel_initCallback2,
        scroll: 2,
				buttonNextHTML: null,
        buttonPrevHTML: null
    });
		
});