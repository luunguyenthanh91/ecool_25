<?php
/*================================================================================*\
|| 							Name code : main.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $link_mod = "epaper";

  function sMain (){
    global $vnT, $input;
    include ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate( DIR_MODULE."/". $this->module . "/html/". $this->module . ".tpl");
    $this->skin->assign('DIR_MOD', DIR_MOD);
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('INPUT', $input);
    $this->skin->assign('CONF', $vnT->conf);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $vnT->html->addStyleSheet(DIR_MOD . "/css/" . $this->module . ".css");
    $vnT->html->addScript(DIR_MOD . "/js/" . $this->module . ".js");
		//active menu
		$vnT->setting['menu_active'] = $this->module;
		//SEO		
		if ($vnT->setting['metakey'])	$vnT->conf['meta_keyword'] = $vnT->setting['metakey'];
		if ($vnT->setting['metadesc'])	$vnT->conf['meta_description'] = $vnT->setting['metadesc'];		
		if ($vnT->setting['friendly_title']){	 
			$vnT->conf['indextitle'] = $vnT->setting['friendly_title'];	
		}
    $data['main'] = $this->list_epaper();
		$data['f_title'] = $vnT->lang['epaper']['epaper'];
    $navation ='<ul><li class="home"><a href="'.$vnT->link_root.'"><span itemprop="title"><i class="fa fa-home"></i></span></a></li> <li><a href="'.$vnT->link_root.$vnT->setting['seo_name'][$vnT->lang_name]['epaper'].'.html"><span itemprop="title">'.$vnT->lang['epaper']['epaper'].'</span></a></li> </ul>';
    $data['fixed_sidebar'] = $vnT->lib->fixed_sidebar();
    $data['navation'] = $vnT->lib->box_navation($navation);
    $vnT->setting['banner'] = $vnT->lib->get_child_slide('child');
    $this->skin->assign("data", $data);
    $this->skin->parse("modules");
    $vnT->output .= $this->skin->text("modules");
  }
  function list_epaper (){
    global $DB, $func, $input, $vnT;
    $p = ((int) $input['p']) ? (int) $input['p'] : 1;
    $view = 1;
		$num_row = 5;
    $sql_num = "SELECT e_id FROM epaper WHERE display=1 AND lang = '". $vnT->lang_name ."' $where ";
    $res_num = $vnT->DB->query($sql_num);
    $totals = $vnT->DB->num_rows($res_num);
	  $n_list = ($vnT->setting['n_list']) ? $vnT->setting['n_list'] : 10;
		$n_grid = ($vnT->setting['n_grid']) ? $vnT->setting['n_grid'] : 20;
		$n = ($view == 2) ? $n_list : $n_grid;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
		if($num_pages>1){
			$root_link = LINK_MOD.".html"; 
			$nav = "<div class='pagination'>".$vnT->func->htaccess_paginate($root_link,$totals,$n,$ext_pag,$p)."</div>" ;
		}
    $where .= " ORDER BY e_order DESC , date_post DESC ";
    $data['row_epaper'] = row_epaper($where, $start, $n, $num_row, $view);
    $data['nav'] = $nav;
    $this->skin->assign("data", $data);
    $this->skin->parse("html_list");
   //  $nd['content'] .= $this->skin->text("html_list");		
 		// $nd['f_title'] = $vnT->lang['epaper']['epaper'];
   //  $nd['more'] = $vnT->setting['slogan'];
    // $textout .= $vnT->skin_box->parse_box("box_middle", $nd);
		return  $this->skin->text("html_list");

  }
// end class
}
?>