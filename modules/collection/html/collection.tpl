<!-- BEGIN: modules -->
<div id="vnt-content">
  <!--===MAIN TOP===-->
  <div class="vnt-main-top">
      <div class="vnt-slide">
          <div id="vnt-slide" class="slick-init">
              <div class="item"><div class="img">
                  <img src="{DIR_IMAGE}/about/slide.jpg" alt="">
              </div></div>
          </div>
          <div id="vnt-navation" class="breadcrumb hidden-sm hidden-xs" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <div class="wrapper">
                  <div class="navation">
                      {data.navation}
                      <div class="clear"></div>
                  </div>
              </div>
          </div>
      </div>
      
  </div>
<div class="wrapping">
  <div class="wrapCont">
    <div class="wrapper">
      {data.main}
    </div>
  </div>
  <div id="flagEnd"></div>
</div></div>
<!-- END: modules -->

<!-- BEGIN: html_list -->
<div class="row">
  {data.row_collection}
</div>
{data.nav}
<!-- END: html_list -->

<!-- BEGIN: html_search -->
<div class="formSearch"> 
  <form name='modSearch' id="modSearch" method='get' action="{data.link_action}">
    <table width="90%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td class="col1" width="20%" nowrap="nowrap"><strong>{LANG.collection.keyword}</strong></td>
        <td><input type="text" class="textfiled" name="keyword" id="keyword" value="{INPUT.keyword}" size="30"/></td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap" ><strong>{LANG.collection.category}</strong></td>
        <td>{data.list_cat}</td>
      </tr>
      <tr>
        <td class="col1" nowrap="nowrap">&nbsp;</td>
        <td>
          <button id="btnSearch" name="btnSearch" type="submit" class="btn" value="{LANG.collection.btn_search}">
            <span>{LANG.collection.btn_search}</span>
          </button>
        </td>
      </tr>
    </table>
  </form>
</div>
<p class="mess_result">{data.note_keyword} <br>{data.note_result}</p>
<div id="List_View">
  {data.list_search}
  <br class="clear">
</div> 
{data.nav}
<!-- END: html_search -->

<!-- BEGIN: detail -->
<div class="collectContent">
  <div class="grid">
    <div class="col">
      <div class="caption">
        <div class="tend"><h1>{data.p_name}</h1></div>
        <div class="des">{data.text_short}</div>
        <div class="link">
          <a href="{data.link_desc}" id="contentFlag"><span>{LANG.collection.view_more}</span></a>
          {data.btn_video}
        </div>
        {data.btn360}
      </div>
    </div>
    <div class="col">
      <div class="img"><img src="{data.src}" alt="{data.p_name}" /></div>
    </div>
  </div>
</div>
<div id="slideImg" class="slick-init">
  {data.list_pic}
</div>
<div class="templateDesign">
  {data.description2}
</div>
<div class="partDetail">
  <div class="grid">
    <div class="col"><div class="img"><img src="{data.src}" alt="{data.p_name}" /></div></div>
    <div class="col">
      <div class="caption">
        <div class="title"><h2>{data.p_name}</h2></div>
        <div class="attr">
          <ul>
            {data.row_option}
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- BEGIN: html_video -->
<div class="aboutVideo" id="videoFlag" {row.style_bg}>
  {row.desc}
  <div class="img"><img src="{DIR_IMAGE}/per.png" alt="Play video"></div>
  <a href="{row.link_embed}" class="link fancybox.iframe"></a>
</div>
<!-- END: html_video -->
<div class="myTools">
  <div class="share">
    {data.list_social_network}
    <div class="clear"></div>
  </div>
  <div class="print hidden-sm hidden-xs">
    <ul>
      <li><a href="javascript:print();" class="fa-print">{LANG.collection.prints}</a></li>
    </ul>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>
{data.comment}
{data.facebook_comment}
{data.other_collection}
<!-- END: detail -->

<!-- BEGIN: html_other_collection -->
<div class="collectOther">
  <div class="title">{data.f_title}</div>
  <div class="content">
    <div id="slideOther" class="slick-init">
      {data.list}
    </div>
  </div>
</div>
<!-- END: html_other_collection -->

<!-- BEGIN: html_360 -->
<div class="iframe360">
  <iframe src="{data.link360}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen onload="this.width=screen.width;this.height=screen.height;"></iframe>
</div>
<!-- END: html_360 -->

<!-- BEGIN: html_desc -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Trang chủ</title> 
  <!-- DÙNG CHUNG CHO TÒAN SITE -->
  <link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/font/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
  <link href="{DIR_MOD}/css/collection.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{DIR_JS}/mscrollbar/jquery.mCustomScrollbar.min.css" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/mscrollbar/jquery.mCustomScrollbar.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    if(typeof $(".the-info-content").offset() =='object'){
      $(".the-info-content").mCustomScrollbar();
    }
  });
  </script>
</head>
<body>
  <div class="designPopup" style="max-width: 740px;">
    <div class="content">
      <div class="the-info-wrap">
        <div class="the-info-content desc">
          <div class="t1">{data.p_name}</div>
          {data.description}
        </div>
      </div>
    </div>
  </div>
</body>
</html>
<!-- END: html_desc -->

<!-- BEGIN: html_slide -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Trang chủ</title> 
  <link href="{DIR_JS}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/slick/slick.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/menumobile/menumobile.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_JS}/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
  <link href="{DIR_STYLE}/font/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="{DIR_STYLE}/screen.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{DIR_JS}/jquery.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/jquery-migrate.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/bootstrap/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/slick/slick.min.js"></script>
  <script type="text/javascript" src="{DIR_JS}/menumobile/menumobile.js"></script>
  <script type="text/javascript" src="{DIR_JS}/fancybox/jquery.fancybox.js"></script>
  <script type="text/javascript" src="{DIR_JS}/core.js"></script>
  <link href="{DIR_MOD}/css/collection.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="{DIR_MOD}/js/collection.js"></script>
</head>
<body>
  <div id="slideGalleryWrap">
    <div id="slideGallery" class="slick-init">
      {data.list_for}
    </div>
    <div id="slideNavGallery">
      {data.list_nav}
    </div>
  </div>
</body>
</html>
<!-- END: html_slide -->

<!-- BEGIN: html_comment --> 
<script language="javascript">
  js_lang['err_name_empty'] = "{LANG.collection.err_name_empty}";
  js_lang['err_email_empty'] = "{LANG.collection.err_email_empty}";
  js_lang['err_email_invalid'] = "{LANG.collection.err_email_invalid}";
  js_lang['security_code_invalid'] = "{LANG.collection.security_code_invalid}";
  js_lang['err_security_code_empty'] = "{LANG.collection.err_security_code_empty}";
  js_lang['err_title_empty'] = "{LANG.collection.err_title_empty}";
  js_lang['err_content_comment_empty'] = "{LANG.collection.err_content_comment_empty}";
  js_lang['send_comment_success'] = "{LANG.collection.send_comment_success}";
  js_lang['send_reply_success'] = "{LANG.collection.send_reply_success}";
  js_lang['err_conntent_minchar'] = "{LANG.collection.err_conntent_minchar}";
  js_lang['mess_error_post'] = "{LANG.collection.mess_error_post}";
</script>
<script type="text/javascript" src="{DIR_MOD}/js/comment.js"></script>
<div class="comment">
  <div class="title">{LANG.collection.f_comment}</div>
  <div class="formComment">
    <form action="{data.link_action}" method="post" name="fComment" id="fComment" onsubmit="return vnTcomment.post_comment('{data.p_id}','{data.lang}');">
      <div class="w_content">
        <textarea id="com_content" name="com_content" class="form-control" placeholder="{LANG.collection.comment_default}"></textarea>
        <div class="content-info" style="display: none;">
          <div class="info-title">{LANG.collection.enter_info_comment}</div>
          <input type="text" name="com_email" id="com_email" class="form-control" placeholder="Email" value="{data.com_email}"/>
          <input type="text" name="com_name" id="com_name" class="form-control" placeholder="{LANG.collection.full_name}" value="{data.com_name}"/>
          <div class="input-group">
            <input type="text" name="security_code" id="security_code" class="form-control" placeholder="{LANG.collection.security_code}">
            <div class="input-group-img">
              <img class="security_ver" src="{data.ver_img}" alt="">
            </div>
          </div>
          <button id="btn-search" name="btn-search" class="btn" type="submit">{LANG.collection.btn_send_comment}</button>
          <button id="btn-close" name="btn-close" class="btn" type="button">{LANG.collection.btn_close}</button>
        </div>
      </div>
    </form>
  </div>
  {data.list_comment}
</div>
<!-- END: html_comment -->