<!-- BEGIN: item_collection -->
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div class="collect">
    <div class="img">
      <a href="{data.link}" title="{data.p_name}">
        <img src="{data.src}" alt="{data.p_name}"/>
      </a>
    </div>
    <div class="tend">
      <h3><a href="{data.link}" title="{data.p_name}">{data.p_name}</a></h3>
    </div>
  </div>
</div>
<!-- END: item_collection -->