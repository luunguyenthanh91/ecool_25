<?php
/*================================================================================*\
|| 							Name code : cart.php 		 		 																	  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                					# ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Access denied');
}
$nts = new sMain();
class sMain
{
	var $output = "";
	var $skin = "";
	var $linkUrl = "";
	var $module = "collection";
	var $action = "booking";

	function sMain(){
		global $vnT,$input,$func,$cart,$DB,$conf;
		include ("function_".$this->module.".php");
		loadSetting();
		$this->linkMod = ROOT_URL.$vnT->cmd . "=mod:".$this->module;
		$this->skin = new XiTemplate( DIR_MODULE ."/". $this->module . "/html/". $this->action . ".tpl");
  	$this->skin->assign('DIR_MOD', DIR_MOD);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('INPUT', $input);
		$this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
		$this->skin->assign('DIR_STYLE', $vnT->dir_style);
		$this->skin->assign('DIR_SKIN', $vnT->dir_skin);
		$this->skin->assign('DIR_JS', $vnT->dir_js);
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
		//active menu
		$vnT->setting['menu_active'] = $this->module; 
		$vnT->conf['indextitle'] = $vnT->lang['collection']['f_info_order'];
		$err="";
		switch ($input['do']) {
			case 'success':
				$data['main'] = $this->do_Success();
				break;
			default:
				$data['main'] = $this->do_Booking($row);
				break;
		}
	  // $data['ListHour'] = ListHour();
	  // $data['ListMinute'] = ListMinute();
	  // $data['List_Branch'] = List_Branch();
		$this->skin->assign("data", $data);		
  	$this->skin->parse("modules");
		echo $this->skin->text("modules");
		exit();
	}
	function do_Booking(){
		global $vnT,$DB,$input,$func;
		$err = '';
		if (isset($_POST['btnConfirm'])){
			if ($vnT->user['sec_code'] == $input['security_code']){
				if(empty($err)){
					$cot['p_id'] = (int) $input['p_id'];
					$cot['name'] = $input['name'];
					$cot['phone'] = $input['phone'];
					$cot['email'] = $input['email'];
					$cot['address'] = $input['address'];
					$cot['status'] = 1;
					$cot['comment'] = $vnT->func->txt_HTML($_POST['comment']);
					$cot['date_post'] = time();
					$ok = $vnT->DB->do_insert("collection_booking",$cot);
					if($ok){
						$content_email = $vnT->func->load_MailTemp("booking");
						$qu_find = array(
							'{domain}' , 
							'{name}' , 
							'{email}' , 
							'{phone}' , 
							'{address}' , 
							'{collection}' , 
							'{content}',
							'{date}'
						);
						$qu_replace = array(
							$_SERVER['HTTP_HOST'] , 
							$input['name'] , 
							$input['email'] ,
							$input['phone'] , 
							$input['address'],
							get_p_name($input['p_id']),
							$vnT->func->HTML($_POST["comment"]) , 
							date("H:i, d/m/Y")
						);
						$message = str_replace($qu_find, $qu_replace, $content_email);
						$subject = str_replace("{host}",$_SERVER['HTTP_HOST'],$vnT->lang['collection']['subject_booking']);
						$sent = $vnT->func->doSendMail($vnT->conf['email'], $subject, $message, $input["email"], $file_attach);
						$link_ref = LINK_MOD.'/booking.html/?do=success';
						$vnT->func->header_redirect($link_ref);
					}else{
						$err = $func->html_err($DB->debug());
					}
				}else{
					$err = $vnT->func->html_err($err);
				}
			}else{
				$err = $vnT->func->html_err($vnT->lang['collection']['security_code_invalid']);
			}
		}
		$data['list_collection'] = get_list_collection($input['id']);
		$data['err'] = $err;
		$vnT->user['sec_code'] = $vnT->func->get_security_code();
	  $scode = $vnT->func->NDK_encode($vnT->user['sec_code']);
	  $data['ver_img'] = ROOT_URL."includes/sec_image.php?code=$scode&h=38";
	  $this->skin->assign("data", $data);
  	$this->skin->parse("html_booking");
		return $this->skin->text("html_booking");
	}
	function do_Success(){
		global $vnT,$DB,$input;
	  $this->skin->assign("data", $data);
  	$this->skin->parse("html_booking_success");
		return $this->skin->text("html_booking_success");
	}
}
?>