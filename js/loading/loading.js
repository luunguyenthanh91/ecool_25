$(document).ready(function() {
    // SELECTOR LAZYLOADING
    function setlazy(el,eff,delay){
        el.addClass("lazyloading");
        el.attr("data-eff",eff);
        el.attr("data-delay",delay);
    }
    //////////////////////////
    /////// LOADING WEB///////
    //////////////////////////
    setlazy($(".box_mid .mid-title .titleL"),'fadeInUp','0.1');
    setlazy($(".box_mid .mid-title .titleR"),'fadeInUp','0.2');
    setlazy($(".myTools"),'fadeInUp','0.2');
    setlazy($(".comment"),'fadeInUp','0.2');
    setlazy($(".date_share"),'fadeInUp','0.2');
    setlazy($(".pagination"),'fadeInUp','0.2');
    setlazy($("#slideCollect"),'fadeInLeft','0.2');
    time=0.3;
    $(".collecttionHome .itemCollect").each(function(){
        setlazy($(this),'fadeInRight',time);
        time+=0.2;
    });
    setlazy($(".menuPduct, .menuPject"),'fadeInRight','0.2');
    setlazy($(".product"),'fadeInUp','0.2');
    time=0.3;
    $("#slideProject .item").each(function(){
        setlazy($(this),'fadeInUp',time);
        time+=0.2;
    });
    setlazy($(".newsHome .news"),'fadeIn','0.2');
    setlazy($(".aboutHome"),'fadeIn','0.2');
    setlazy($(".aboutBlank"),'fadeInUp','0.2');
    setlazy($(".vnt-about-num"),'fadeInUp','0.2');
    setlazy($(".opinionHome"),'fadeInUp','0.2');
    setlazy($(".linkAll"),'fadeInUp','0.2');
    //==========
    setlazy($("#vnt-slide"),'fadeIn','0.2');
    setlazy($(".breadcrumb"),'fadeInUp','0.2');
    setlazy($(".about .img"),'fadeIn','0.2');
    setlazy($(".about .caption"),'fadeInUp','0.2');
    setlazy($(".aboutVideo"),'fadeIn','0.2');
    setlazy($(".aboutVideo .t1"),'fadeInLeft','0.2');
    setlazy($(".aboutVideo .t2"),'fadeInRight','0.2');
    setlazy($(".aboutVideo .img"),'fadeInUp','0.2');
    //==========
    setlazy($(".boxFilter"),'fadeIn','0.2');
    setlazy($("#slideThumbnail"),'fadeInUp','0.2');
    setlazy($(".dfTitle"),'fadeInUp','0.2');
    setlazy($(".project"),'slideInUp','0.2');
    time=0.3;
    $(".projectAttr ul li").each(function(){
        setlazy($(this),'fadeInRight',time);
        time+=0.2;
    });
    setlazy($(".projectPrice"),'fadeInUp','0.2');
    setlazy($(".projectContact"),'fadeInUp','0.2');
    setlazy($(".projectTag"),'fadeInUp','0.3');
    setlazy($(".projectSocial"),'fadeInUp','0.4');
    setlazy($(".projectContent .caption"),'fadeInLeft','0.2');
    setlazy($(".projectContent .img"),'fadeInRight','0.2');
    setlazy($("#slideImg"),'fadeIn','0.2');
    setlazy($(".templateDesign"),'fadeIn','0.2');
    //==========
    setlazy($(".menuCollect"),'fadeInUp','0.4');
    setlazy($(".collect"),'fadeIn','0.2');
    setlazy($(".collectContent .caption"),'fadeInLeft','0.2');
    setlazy($(".collectContent .img"),'fadeInRight','0.2');
    setlazy($(".partDetail .caption"),'fadeInLeft','0.2');
    setlazy($(".partDetail .img"),'fadeInRight','0.2');
    setlazy($(".btnRecive"),'fadeInUp','0.2');
    setlazy($(".collectOther .title"),'fadeInUp','0.2');
    //==========
    // setlazy($(".productTools"),'fadeIn','0.2');
    // setlazy($("#thumbnail-for"),'fadeInUp','0.2');
    // setlazy($(".vnt-thumbnail-for .ribbon"),'fadeInUp','0.2');
    // setlazy($(".vnt-thumbnail-for .zoom"),'fadeInUp','0.2');
    // setlazy($("#thumbnail-nav"),'fadeInUp','0.2');
    // setlazy($(".productTitle"),'fadeInUp','0.2');
    // time=0.3;
    // $(".productAttr ul li").each(function(){
    //     setlazy($(this),'fadeInRight',time);
    //     time+=0.2;
    // });
    // setlazy($(".productDes"),'fadeInUp','0.2');
    // setlazy($(".productVideo"),'fadeInUp','0.2');
    // setlazy($(".productSupport"),'fadeInRight','0.2');
    // setlazy($(".productContent .tab-list"),'fadeInUp','0.2');
    // setlazy($(".productContent .the-content"),'fadeIn','0.2');
    // setlazy($(".productOther .title"),'fadeInUp','0.2');
    //==========
    setlazy($(".newsS .img"),'fadeInLeft','0.2');
    setlazy($(".newsS .caption"),'fadeInRight','0.2');
    setlazy($(".news .img"),'fadeIn','0.2');
    setlazy($(".news .date"),'fadeInUp','0.2');
    setlazy($(".news .tend"),'fadeInUp','0.2');
    setlazy($(".news .des"),'fadeInUp','0.2');
    setlazy($(".newsTitle"),'fadeInUp','0.2');
    setlazy($(".newsOther .title"),'fadeInUp','0.2');
    setlazy($(".newsOther ul li"),'fadeInLeft','0.2');
    setlazy($(".box_news_sidebar"),'fadeInRight','0.2');
    //==========
    setlazy($(".showroom .img"),'fadeInLeft','0.2');
    setlazy($(".showroom .caption"),'fadeInUp','0.2');
    setlazy($(".showroom .gallery"),'fadeInRight','0.2');
    //==========
    setlazy($(".catalogue"),'fadeInUp','0.2');
    //==========
    setlazy($(".customDes"),'fadeInUp','0.2');
    time=0.3;
    $(".customItem a").each(function(){
        setlazy($(this),'flipInX',time);
        time+=0.2;
    });
    setlazy($(".custommiItem .title"),'fadeInUp','0.2');
    time=0.3;
    $(".custommiItem .item").each(function(){
        setlazy($(this),'flipInY',time);
        time+=0.2;
    });
    setlazy($(".formCustom .titleBig"),'fadeInUp','0.2');
    setlazy($(".formCustom .box"),'fadeInUp','0.2');
    //==========
    setlazy($(".info-contact"),'fadeIn','0.2');
    setlazy($(".info-contact .img"),'fadeInUp','0.2');
    setlazy($(".info-contact .name"),'fadeInUp','0.4');
    time=0.3;
    $(".info-contact ul li").each(function(){
        setlazy($(this),'fadeInUp',time);
        time+=0.2;
    });
    setlazy($(".info-contact .view-map-contact"),'fadeInUp','0.4');
    setlazy($(".form-contact .textContact"),'fadeInUp','0.2');
    setlazy($(".form-contact .form-group"),'fadeInUp','0.2');
    /////////////////////////////
    /////// LOADING CLASS ///////
    /////////////////////////////
    setlazy($(".effectIn"),'fadeIn','0.15');
    setlazy($(".effectInUp"),'fadeInUp','0.15');
    setlazy($(".effectInLeft"),'fadeInLeft','0.15');
    setlazy($(".effectInRight"),'fadeInDown','0.15');
    setlazy($(".effectFlipX"),'flipInX','0.15');
    setlazy($(".effectFlipY"),'flipInY','0.15')
    // BIGIN LAZYLOADING
    var isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    if((isMobile == false) && ($(window).innerWidth() > 991)){
        $("body").my_effect_scroll({
            itemSelector : ".lazyloading",
            height_per: 30,
            height_px: 300,
            classItemVisible : function(){
                var duration = $(this).attr("data-delay");
                var effect = $(this).attr("data-eff");
                $(this).css({
                    "-webkit-animation-delay": duration+"s",
                    "-o-animation-delay": duration+"s",
                    "animation-delay": duration+"s",
                });
                $(this).removeClass("lazyloading");
                $(this).addClass("lazy-start");
                $(this).on("webkitAnimationEnd oanimationend msAnimationEnd animationend",function(){
                    $(this).removeClass("lazy-start");
                    $(this).removeClass(effect);
                    $(this).removeAttr("data-delay");
                    $(this).removeAttr("data-eff");
                });
                var effect = $(this).attr("data-eff");
                return effect;
            }
        });
    }else{
        $(".lazyloading").each(function(){
            var duration = $(this).attr("data-delay");
            var effect = $(this).attr("data-eff");
            $(this).css({
                "-webkit-animation-delay": duration+"s",
                "-o-animation-delay": duration+"s",
                "animation-delay": duration+"s",
            });
            $(this).removeClass("lazyloading");
            $(this).addClass("lazy-start");
            $(this).on("webkitAnimationEnd oanimationend msAnimationEnd animationend",function(){
                $(this).removeClass("lazy-start");
                $(this).removeClass(effect);
                $(this).removeAttr("data-delay");
                $(this).removeAttr("data-eff");
            });
            var effect = $(this).attr("data-eff");
            return effect;
        });
    }
});