$(document).ready(function(){
    // MANSORY
    $(window).load(function(){
        $('.gridPR').masonry({
            itemSelector: '.gridPR .col',
            columnWidth: '.gridPR .colC',
            percentPosition: true,
        });
    });
    $("#slideThumbnail").slick({
    	fade:true,
    	autoplay:true,
    	speed:1024,
        dots:true,
    });
    $("#slideOther").slick({
        slidesToShow: 3,
        autoplay:true,
        speed:1024,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
    $("#slideThumbnail").click(function(){
        var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {
            index: link, 
            event: event,
            container: '#blueimp-gallery',
            carousel: true
        },
        links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    });
});