<?php
$lang = array(
  
	'manage_dealer' => "video Manager",
  'add_dealer' => "Add video",
  'edit_dealer' => "update video",
  'no_have_dealer' => 'There is no video',
  'title' => "Title",
  'pic' => "Image",
  'short' => 'Short Description',
  'content' => "Content",

// Cat_dealer
  'add_cat_dealer' => "Add new category",
  'edit_cat_dealer' => "update category",
  'manage_cat_dealer' => "portfolio management",
	'no_have_category' => 'There is no list',
	'category' => 'Category',
	'show_home' => 'Show Home',
	'show_menu' => 'Show menu',
	'show_main' => 'Main Show',
	
	// Settings
	'manage_setting' => 'Manage configuration',
	'manage_setting' => 'Manage configuration',
	'edit_setting_success' => 'configuration update success',
	'setting_module' => 'general configuration for modules',
	'active_bqt' => 'Activate by administrators',
	'imgthumb_width' => 'thumbnail size',
	'imgdetail_width' => 'Size Detail',
	'n_list' => 'Number of view as list',
	'n_main' => 'Number in each category in the Home',
	'n_mem' => 'Members show at Home',
	'n_comment' => 'Number of comments in a page',
	'comment' => 'Product Review',
	'nophoto' => 'The image <strong> Nophoto </ strong> as the photo does not believe',
	'imgthumb_width' => 'Thumbnail size',
	'detail_width_max' => 'maximum width of the image details',
	'img_width' => 'Maximum image width upload',
	'folder_upload' => 'upload directory',
	'thum_size' => 'Create thumb size', 
		
	);
?>