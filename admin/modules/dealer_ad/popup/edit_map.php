<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

$lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
//maps
$id = (int) $_GET['id'];
$query = $DB->query("SELECT * FROM dealer n ,dealer_desc nd  WHERE n.did=nd.did AND n.did=$id AND lang='$lang' ");
if ($row_m = $DB->fetch_row($query)) {
  $data['map_information'] = str_replace("\r\n", "<br>", $row_m['map_desc']);
  $data['map_lat'] = ($row_m['map_lat']) ? $row_m['map_lat'] : "10.804866895605";
  $data['map_lng'] = ($row_m['map_lng']) ? $row_m['map_lng'] : "106.64199984239";
  $data['set_map'] = " placeMarker(defaultLatLng, 1);\n	old_marker = clickmarker;";
} else {
  $data['map_lat'] = "10.804866895605";
  $data['map_lng'] = "106.64199984239";
}
?>
<html>
<head>
<title>GoogleMap</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">



<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"
	onLoad=" initialize();">
<center>
<form action="" method="post" name="fMap">
<table width="100%" cellpadding="2" cellspacing="0" border="0"
	bgcolor="#FFFFFF">
	<tr>
		<td align="center"><script type="text/javascript"
			src="http://maps.google.com/maps/api/js?sensor=true"></script> <script
			type="text/javascript">
			var map;
			var infowindow;
			var clickmarker;
			var old_marker;
			var update 			= 0;
			var marker_placed = false;
			var contentString = '';
			var information	= '<?php
  echo $data['map_information'];
  ?>';
			var arrLatLng		= Array(0, 0);
			var arrCity			= Array();
			
			function initialize(){
				arrCity[2] = new google.maps.LatLng(21.027521, 105.852449);
				arrCity[3] = new google.maps.LatLng(10.759579, 106.668661);
				arrCity[32] = new google.maps.LatLng(20.860221, 106.680783);
				arrCity[65] = new google.maps.LatLng(16.050528, 108.213351);
				arrCity[15] = new google.maps.LatLng(10.032231, 105.783058);
								
				var defaultLatLng = new google.maps.LatLng(<?php
    echo $data['map_lat'];
    ?>,<?php
    echo $data['map_lng'];
    ?>);
				var myOptions		= {zoom: 16,
						center: defaultLatLng,
						scrollwheel : false,
						mapTypeId: google.maps.MapTypeId.ROADMAP};
						map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
						
						<?php
      echo $data['set_map'];
      ?>
 								
						map.setCenter(defaultLatLng);
						google.maps.event.addListener(map, 'click', function(event){
							if(marker_placed) Cancel();
							placeMarker(event.latLng, 0);
						});
				
			}
			
			function placeMarker(location, upd){
				information = information.replace("<br>", "\r\n") ;
				contentString = '<div style="margin:5px"><div align="left" style="font-size:11px; font-weight:bold; color:#30A602">Thông tin vị trí </div><div style="margin:5px 0px 5px 0px"><textarea class="form_control" id="gmap_information" name="gmap_information" style="width:300px; height:70px">' + information + '</textarea><div align="left" style="color:#787878; font-size:10px">Tối đa 200 ký tự.</div><div align="center" style="color:#FF0000; font-size:11px">Bạn phải ấn nút <b>"Cập nhật"</b> vị trí trên bản đồ mới được lưu</div></div><div><input type="button" class="form_button" value="Cập nhật" style="width:75px; font-size:11px" onclick="set_position_map()" />&nbsp;<input type="button" class="form_button" value="Xóa" style="width:75px; font-size:11px" onclick="clear_position_map()" /></div></div>';
				
				update		= upd;
				marker_placed = true;
				
				clickmarker = new google.maps.Marker({
					position: location,
					clickable: false,
					map: map
				});
				
				arrLatLng	= Array(location.lat(), location.lng());
				
				infowindow	= new google.maps.InfoWindow({content: contentString});
				infowindow.open(map, clickmarker);
				
				google.maps.event.addListener(infowindow, 'closeclick', function(){
					if(update == 0) clickmarker.setVisible(false);
					clickable = true;
					if(old_marker != undefined) clickmarker = old_marker;
				});
				
				google.maps.event.addListener(clickmarker, 'click', function(){
					infowindow.open(map, clickmarker);
					update = 1;
					document.getElementById("gmap_information").value  = information;
				});
				
				if(old_marker != undefined){
					document.getElementById("gmap_information").value  = information;
				}
				
			}
			
			function Cancel(){
				clickmarker.setVisible(false);
				infowindow.close();
				marker_placed = false;
				if(old_marker != undefined) old_marker.setVisible(true);
			}
			</script> <script language="javascript" type="text/javascript">		
			
			var win = window.dialogArguments || opener || parent || top;
				
			function set_position_map(){
				
				info = document.getElementById("gmap_information");
				
				ob1 = document.getElementById("map_lat");
				ob2 = document.getElementById("map_lng");
				ob3 = document.getElementById("map_information");
				
				ob1.value	= arrLatLng[0];
				ob2.value	= arrLatLng[1];
				ob3.value	= info.value;

				window.parent.document.getElementById("map_lat").value=arrLatLng[0];
				window.parent.document.getElementById("map_lng").value=arrLatLng[1];
				window.parent.document.getElementById("map_information").value=info.value;
				
				information	= info.value;
				infowindow.close();
				
				if(old_marker != undefined && update == 0) old_marker.setVisible(false);
				old_marker	= clickmarker;
				
			}
			
			function clear_position_map(){
				ob1 = document.getElementById("map_lat");
				ob2 = document.getElementById("map_lng");
				ob3 = document.getElementById("map_information");
				

				ob1.value	= 0;
				ob2.value	= 0;
				ob3.value	= '';
				
				Cancel();
				clickmarker.setVisible(false);
				if(old_marker != undefined) old_marker.setVisible(false);
				clickmarker	= undefined;
				old_marker	= undefined;
				information = "";
			}
			
			function changeCityMap(id){
				if(arrCity[id] != undefined){ 
					map.set_center(arrCity[id]);
					show_map();
				}
				else hide_map();
			}
			
			function change_map_size(value){
				switch (value){
					case "1": 
						document.getElementById("map_canvas").style.width = "600px"; 
						document.getElementById("map_canvas").style.height = "400px"; 
					break;
					case "2": 
						document.getElementById("map_canvas").style.width = "780px"; 
						document.getElementById("map_canvas").style.height = "500px"; 
					break;
					case "3": 
						document.getElementById("map_canvas").style.width = "1024px"; 
						document.getElementById("map_canvas").style.height = "600px"; 
					break;
				}
			}
			
			function show_hide_map(){
				ob1 = document.getElementById("show_hide_link");
				ob2 = document.getElementById("map_canvas");
				if(ob2.style.display == "none"){
					ob1.innerHTML = "Ẩn bản đồ";
					ob2.style.display = "block";
				}
				else{
					ob1.innerHTML = "Hiển thị bản đồ";
					ob2.style.display = "none";
				}
			}

			/* Ẩn bản đồ*/
			function hide_map(){
				ob1 = document.getElementById("show_hide_link");
				ob2 = document.getElementById("map_canvas");

				ob1.innerHTML = "Hiển thị bản đồ";
				ob2.style.display = "none";
			}

			/* Hiện bản đồ*/
			function show_map(){
				ob1 = document.getElementById("show_hide_link");
				ob2 = document.getElementById("map_canvas");

				ob1.innerHTML = "Ẩn bản đồ";
				ob2.style.display = "block";
			}
			</script>
		<div class="text_normal"
			style="margin-bottom: 5px; background: #F2F2F2; padding: 5px; width: 780px; max-width: 770px">		<font color="#0000FF">Bạn hãy tìm địa điểm rồi click vào bản đồ để		đánh dấu vị trí.</font>   </div>
		<div id="map_canvas" style="width: 780px; height: 500px"></div>

		<input type="hidden" id="map_lat" name="map_lat"
			value="<?php
  echo $data['map_lat'];
  ?>" /> <input type="hidden"
			id="map_lng" name="map_lng" value="<?php
  echo $data['map_lng'];
  ?>" />
		<input type="hidden" id="map_information" name="map_information"
			value="<?php
  echo $data['map_information'];
  ?>" /></td>


		</td>
	</tr>
</table>
</form>
</div>

</body>
</html>
