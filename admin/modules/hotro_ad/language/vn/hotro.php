<?php
$lang = array(
  'manage_dealer' => "Quản lý support" , 
  'add_dealer' => "Thêm support" , 
  'edit_dealer' => "Cập nhật support" , 
  'no_have_dealer' => 'Chưa có support nào' , 
  'title' => "Title" , 
  'pic' => "Hình" , 
  'short' => "Mô tả ngắn" , 
  'content' => "Nội dung",
	
	// cat_dealer
  'add_cat_dealer' => "Thêm danh mục mới" ,
  'edit_cat_dealer' => "Cập nhật danh mục" ,
  'manage_cat_dealer' => "Quản lý danh mục " ,
	'no_have_category'	=>	'Chưa có danh mục nào',
  'category' => 'Chuyên mục' ,
  'show_home' => 'Show Home' ,
  'show_menu' => 'Show Menu' ,
  'show_main' => 'Show Main' ,

	//setting
	'manage_setting' => 'Quản lý cấu hình ' ,
	'manage_setting' => 'Quản lý cấu hình ' ,
	'edit_setting_success' => 'Cập nhật cấu hình  thành công' ,
	'setting_module' => 'Cấu hình chung cho module ' ,
	'active_bqt' => 'Kích hoạt bởi BQT' ,
	'imgthumb_width' => 'Kích thước hình nhỏ' ,
	'imgdetail_width' => 'Kích thước hình chi tiết' ,
	'n_list' => 'Số tin xem theo list' ,
	'n_main' => 'Số tin trong từng category ở Home' ,
	'n_mem'	 => 'Số thành viên show ở Home' ,
	'n_comment' => 'Số comment trong 1 trang' ,
	'comment' => 'Đánh giá sản phẩm ' ,
	'nophoto' => 'Hiện hình <strong>nophoto</strong> khi tin không có ảnh' ,
	'imgthumb_width' => 'Kích thước hình nhỏ' ,
	'detail_width_max' => 'Chiều ngang tối đa của hình xem chi tiết' ,
	'img_width' => 'Chiều ngang tối đa hình upload',
	'folder_upload' => 'Thư mục upload',
	'thum_size' => 'Tạo thumb size',


	'add_utilities' => "Thêm tiện ích mới" ,
	'edit_utilities' => "Cập nhật tiện ích" ,
	'manage_utilities' => "Quản lý tiện ích " ,
	'no_have_utilities'	=>	'Chưa có tiện ích nào',
	
	);
?>