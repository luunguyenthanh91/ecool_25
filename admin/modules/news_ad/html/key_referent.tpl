<!-- BEGIN: edit -->
{data.err}
      <form action="{data.link_action}" method="post" name="myForm" enctype="multipart/form-data" class="validate">
        <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">          

		  <tr class="form-required">
			 <td align="right" width="20%" class="row1">Tên :</td>
			 <td  align="left" class="row0"><input name="title" type="text" size="70" maxlength="250" value="{data.title}"></td>
		  </tr>

          <tr >
          <td class="row1">&nbsp;</td>
            <td class="row0" >
            <input type="hidden" name="do_submit" value="1">
            <input type="submit" name="btnSubmit" value="Submit" class="button">
              <input type="reset" name="btnReset" value="Reset" class="button">
            </td></tr>
        </table>
    </form>
<br>
<!-- END: edit -->

<!-- BEGIN: view -->
<br />
{data.list_referent}
<br />
{data.list_news}
<!-- END: view -->

<!-- BEGIN: list_news -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
			<td align="left" width="15%"><strong>{LANG.category}:</strong> &nbsp;</td>
			<td align="left">{data.listcat}</td>
  </tr>
  
  <tr>
    <td ><strong>{LANG.search} :</strong></td>
    <td align="left">
     {data.list_search} <strong> {LANG.keyword} :</strong>  <input name="keyword" value="{data.keyword}" size="20" type="text" />
     &nbsp; <input type="submit" name="btnSearch" value=" Search " class="button"></td>
  </tr>

  </table>
</form>

<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: list_news -->

<!-- BEGIN: list_referent -->
<script language="javascript">
function check_all_focus()	{
	for ( i=0;i < document.f_referent.elements.length ; i++ ){
		if (document.f_referent.elements[i].type=="checkbox" && document.f_referent.elements[i].name!="all"){
			row_id = 'rowkey_'+document.f_referent.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.f_referent.all.checked==true ){
			document.f_referent.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
		}
		else{
			document.f_referent.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row";
			}
		}
		
	}
}

function do_check_focus (id){
	
	for ( i=0;i < document.f_referent.elements.length ; i++ ){
		if (document.f_referent.elements[i].type=="checkbox" && document.f_referent.elements[i].name!="all" && document.f_referent.elements[i].value == id){
			document.f_referent.elements[i].checked=true;
			getobj("rowkey_"+id).className = "row_select";
			
			break;
		}	
	}
	
}
function selected_item_focus(){
		var name_count = document.f_referent.length;

		for (i=0;i<name_count;i++){
			if (document.f_referent.elements[i].checked){
				return true;
			}
		}
		alert('Hãy chọn ít nhất 1 record');
		return false;
	}
	
function del_news_selected(action) {
		if (selected_item_focus()){
			question = confirm('Bạn có chắc muốn XÓA không ?')
			if (question != "0"){
				document.f_referent.action=action;
				document.f_referent.submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{data.f_title}</td>
  </tr>
</table>
{data.err}
<form action="{data.link_action}" method="post" name="f_referent" id="f_referent">
<table width="100%"  border="0" cellspacing="0" cellpadding="0" class="bg_tbl">

	<tr>
		<td >
    
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="adminlist">
    <thead>
		  <tr>
			  <th width="5%" align="center"><input type="checkbox" name="all" onclick="javascript:check_all_focus();"></th>
			  <th width="15%" align="center">{LANG.picture}</th>
			  <th width="45%" align="center">{LANG.title}</th>
			  <th width="30%" align="center">{LANG.category} </th>
		  </tr>
      </thead>
      <tbody>
      
      	<!-- BEGIN: row_referent -->
        <tr class="{row.class}" id="{row.row_id}"> 
          <td align="center">{row.check_box}&nbsp;</td>
          <td align="center">{row.picture}&nbsp;</td>
          <td align="left">{row.title}&nbsp;</td>
          <td align="center">{row.cat_name}&nbsp;</td>
        </tr>
        <!-- END: row_referent -->
        
				<!-- BEGIN: row_referent_no -->
         <tr class="row0" >
           <td  colspan="4" align="center" class="font_err" >{mess}</td>
         </tr>
         <!-- END: row_referent_no -->
      </tbody>
		</table>

		</td>
	</tr>
	<tr>
		<td  style="padding:5px;">
		<input type="button" name="btnDel" value="Xóa các tin đã chọn" class="button" onclick="javascript:del_news_selected('{data.link_del}');" ></td>
	</tr>
</table>
</form>
    
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1" class="bg_tab" align=center>
      <tr  height=25>
        <td align="left" >{data.nav}</td>
      </tr>
</table>
<!-- END: list_referent -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->