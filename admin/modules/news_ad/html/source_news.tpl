<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="news"  class="validate">
<table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
	<!-- BEGIN: select_lang -->
		<tr >
			<td class="row1" >{LANG.language} : </td>
			<td  align="left" class="row0"><input name="rLang" id="rLang" type="radio" value="0" /> {LANG.only_this_lang} <input name="rLang" id="rLang" type="radio" value="1" checked="checked" /> {LANG.all_lang}</td>
		</tr>
    <!-- END: select_lang -->
    
		<tr class="form-required">
			<td class="row1">{LANG.title}: <span class="font_err">(*)</span></td>
			<td class="row0"><input type="text" name="s_title" id="textfield" value="{data.s_title}" class="textfiled"  size="50"></td>
		</tr>
		<tr>
			<td class="row1">{LANG.description}: </td>
			<td class="row0">{data.html_content}</td>
		</tr>		        
  <tr align="center">
  	<td class="row1">&nbsp; </td>
     <td class="row0">
			<input type="hidden" name="do_submit" value="1"  />
			<input type="submit" name="btnSubmit" value="Submit" class="button">
      <input type="reset" name="Submit2" value="Reset" class="button">            
	</td></tr>
</table>
</form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
{data.err}
<table width="100%"  border="0" align="center" cellspacing="0" cellpadding="0" class="tableborder">
  <tr>
    <td ><strong>{LANG.totals}  : </strong><span class="font_err"><strong>{data.totals}</strong></span></td>
  </tr>
</table>
<br />
{data.table_list}

<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1">
  <tr>
    <td  height="30">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->