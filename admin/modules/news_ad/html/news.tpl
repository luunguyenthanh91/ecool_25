<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
  var wpseo_lang = 'en';
  var wpseo_meta_desc_length = '155';
  var wpseo_title = 'title';
  var wpseo_content = 'content';
  var wpseo_title_template = '%%title%%';
  var wpseo_metadesc_template = '';
  var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
  var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
  $(document).ready(function() {
  	$('#myForm').validate({
  		rules: {
  			cat_id: {
  				required: true
  			},
  			title: {
  				required: true,
  				minlength: 3
  			}
      },
      messages: {	    	
  			cat_id: {
  				required: "{LANG.err_select_required}" 
  			},
  			title: {
  				required: "{LANG.err_text_required}",
  				minlength: "{LANG.err_length} 3 {LANG.char}" 
  			} 
  		}
  	});
  	
  	{data.js_preview} 
  });
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate" >
  <div class="boxAdminForm">
    <div class="block-left">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
        <tr>
          <td width="15%" class="row1" nowrap="nowrap">{LANG.cat_news}&nbsp;: </td>
          <td align="left" class="row0">{data.listcat}</td>
        </tr>
        <tr>
          <td class="row1" nowrap="nowrap">{LANG.title_news}&nbsp;: </td>
          <td align="left" class="row0">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td style="padding:0px;"><input name="title" id="title" type="text" size="70" maxlength="250" value="{data.title}" style="width:100%"  onkeyup="vnTMXH.setTitle(this.value)"></td>
                <td width="90" nowrap="nowrap" style="padding:0px;" align="right">{data.list_ref}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="row1" >{LANG.picture}: </td>
          <td class="row0"><div id="ext_picture" class="picture" >{data.pic}</div>
            <input type="hidden" name="picture"	 id="picture" value="{data.picture}" />
            <div id="btnU_picture" class="div_upload" {data.style_upload} >
              <div class="button2">
                <div class="image">
                  <a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}">Chọn hình</a>
                </div>
              </div>
            </div>
            <br/><br/><span class="font_err">{LANG.image_size_3_2}</span>
          </td>
        </tr>
        <tr>
          <td class="row1" nowrap="nowrap">{LANG.short_news}: </td>
          <td class="row0">
            <textarea name="short" id="short" rows="4" class="textarea" style="width:95%">{data.short}</textarea>
          </td>
        </tr>
        <tr>
          <td class="row1" colspan="2" >
            <div class="panel with-nav-tabs panel-default ">
              <div class="panel-heading">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#Tab1">{LANG.content_news}</a></li>
                </ul>
              </div>
              <div class="panel-body">
                <div class="tab-content">
                  <div id="Tab1" class="tab-pane fade in active">
                    {data.html_content}
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td class="row1" >Tags : </td>
          <td class="row0" align="left">
            <div>
              <input type="text" style="width:95%"  name="tags"  id="tags" value="" class="textfiled"/>
            </div>
            <span class="font_err">Kết thúc mỗi từ bằng dấu "<strong>,</strong>"</span>
          </td>
        </tr>
      </table>
    </div><br/>
    <div class="block-right">
      <div class="desc">
        <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
          <tr class="row_title" >
            <td class="font_title">
              <img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/>Search Engine Optimization :
            </td>
          </tr>
        </table>
        <div class="desc_content" style="background: #ffffff">
          <div class="general">
            <h4 class="wpseo-heading" style="display: none;">General</h4>
            <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
              <tr>
                <td class="row1"><label for="yoast_wpseo_snippetpreview">Snippet Preview:</label></td>
                <td class="row0"><div id="wpseosnippet">
                  <a href="#" class="wpseo_title"></a><br/>
                  <a class="wpseo_url" href="#"></a>
                  <p class="desc"><span class="content" style="color: rgb(136, 136, 136);"></span></p>
                </div></td>
              </tr>
              <tr>
                <td class="row0"><label ><strong>Friendly<br>URL :</strong></label></td>
                <td class="row0">
                  <input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield" style="width:98%"><br>
                  <span class="font_err">({LANG.mess_friendly_url})</span>
                </td>
              </tr>
              <tr>
                <td class="row1"><label for="yoast_wpseo_title">Friendly Title:</label></td>
                <td class="row0"><input type="text" class="textfield" value="{data.friendly_title}" name="friendly_title" id="friendly_title"  style="width:98%"   /><br/><p>Title display in search engines is limited to 70 chars, <span id="friendly_title-length"><span class="good">70</span></span> chars left.<br/></td>
              </tr>
              <tr>
                <td class="row1"><label for="metadesc">Meta Description:</label></td>
                <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" class="textarea"  style="width:98%">{data.metadesc}</textarea><p>The <code>meta</code> description will be limited to 155 chars (because of date display), <span id="metadesc-length"><span class="good">155</span></span> chars left. </p><div id="metadesc_notice"></div></td>
              </tr>
              <tr>
                <td class="row1" ><label for="metakey">Meta Keyword:</label></td>
                <td class="row0">
                  <input type="text" class="textfield" value="{data.metakey}" name="metakey"  id="metakey"  style="width:98%"/><br/><p></p><div style="width: 300px;" class="alignright"><p id="related_keywords_heading" style="display: none;">Related keywords:</p><div id="wpseo_tag_suggestions"></div></div>
                </td>
              </tr>
            </table>
          </div>
          <div class="results" style="padding: 10px;">
            <div id="focuskwresults">
              <div class="article_heading">
                <div class="label">Article Heading</div><span class="wrong">NO</span>
              </div>
              <div class="page_url"><div class="label">Page URL</div><span class="wrong">NO</span></div>
              <div class="page_title"><div class="label">Page title</div><span class="wrong">NO</span></div>
              <div class="meta_desc"><div class="label">Meta description</div><span class="wrong">NO</span></div>
              <div class="content_result"><div class="label">Content</div><span class="wrong">NO</span></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div><br/>
      <div class="desc">
        <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
          <tr class="row_title" >
            <td class="font_title"><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Xem Demo tương tác FaceBook : </td>
          </tr>
        </table>
        <div class="desc_content" style="background: #ffffff; padding: 10px;">
          <div class="divFacebook"> {data.img_mxh}
            <div class="face-info">
              <div class="title_mxh" id="title_mxh" >{data.friendly_title}</div>
              <div class="link_mxh" id="link_mxh" >{data.link_mxh}</div>
              <div class="description_mxh" id="description_mxh" >{data.metadesc}</div>
            </div>
          </div>
        </div>
      </div><br/>
      <div class="desc">
        <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
          <tr class="row_title" >
            <td colspan="2" class="font_title"><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> {LANG.other_option}</td>
          </tr>
        </table>
        <div class="desc_content">
          <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">
            <tr>
              <td class="row1" width="150" nowrap="nowrap">{LANG.post_by}</td>
              <td align="left" class="row0">
                <input name="poster" type="text" id="poster" size="50" maxlength="250" value="{data.poster}" >
              </td>
            </tr>
            <tr>
              <td class="row1" nowrap="nowrap">{LANG.date_post}</td>
              <td align="left" class="row0">
                <input name="gio" type="text" size="5" maxlength="5" value="{data.gio}">&nbsp;,&nbsp;
                <input id="ngay" name="ngay" type="text" size="10" value="{data.ngay}" class="dates">
              </td>
            </tr>
            <tr>
              <td class="row1">{LANG.display_news}</td>
              <td align="left" class="row0">{data.list_display}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="row0" colspan="2"  align="center" height="50">
        <input type="hidden" name="do_submit"	 value="1" />
        <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
        <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp; 
      </td>
    </tr>
  </table>
</form><br>
<!-- END: edit --> 

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
  <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
      <td align="left" class="row1"><strong>{LANG.category}: </strong></td>
      <td align="left" class="row0">{data.list_cat}</td>
    </tr>
    <tr>
      <td class="row1" ><strong>{LANG.view_day_from}:</strong></td>
      <td align="left" class="row0"><input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"  class="dates"   />
        &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong>
        <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"  class="dates"  /></td>
    </tr>
    <tr>
    <tr>
      <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp; </td>
      <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
        <input name="keyword"  value="{data.keyword}"type="text" size="20">
        <input name="btnSearch" type="submit" value=" Search " class="button"></td>
    </tr>
    <tr>
      <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
      <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
    </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage --> 
