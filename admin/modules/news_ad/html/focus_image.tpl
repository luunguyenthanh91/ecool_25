<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="add_pic" id="add_pic" >
        <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center  class="admintable">         
          <tr>
            <td width="20%" align="right" class="row1">Title : </td>
            <td  align="left" class="row0"><input name="title" type="text" id="title" size="70" maxlength="250" value="{data.title}" readonly="readonly"></td>
          </tr>
         <tr >
     <td class="row1" align="right">{LANG.picture} : <p> <span class="font_err">{data.note_size}</span></p></td>
     <td class="row0">
     	{data.pic}<br>
      <div class="ext_upload">
      <input name="chk_upload" id="chk_upload" type="radio" value="0" checked> 
      Insert URL's image &nbsp; <input name="picture" type="text" size="50" maxlength="250" onchange="do_ChoseUpload('ext_upload',0);" > <br>
      <input name="chk_upload" id="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
      <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
      </div>
    </td>
    </tr> 
		  
        
  <tr >
    <td class="row1" align="right">&nbsp; </td>
    <td colspan="2" class="row0">
    <input type="hidden" name="newsid" value="{data.newsid}" >
    <input type="submit" name="btnEdit" value="Submit" class="button">
    <input type="reset" name="Submit2" value="Reset" class="button">            
 	 </td>
  </tr>
</table>
    </form>
	<br />
<!-- END: edit -->

<!-- BEGIN: list_news -->
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{LANG.list_news}</td>
  </tr>
</table>
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: list_news -->


<!-- BEGIN: manage -->
 <br />
{data.table_list_focus}
<br />
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    <tr>
    <td align="left" width="15%" nowrap="nowrap"><strong>{LANG.category}:</strong> &nbsp;</td>
    <td align="left">{data.listcat}&nbsp;</td>
  </tr>
<tr>
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
  </table>

</form>
 

{data.table_list}
<br />
<!-- END: manage -->

<!-- BEGIN: list_focus_news -->
<script language="javascript">
function check_all_focus()	{
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all"){
			row_id = 'rowfocus_'+document.f_focus.elements[i].value;
		}else{
			row_id="";
		}
		
		if ( document.f_focus.all.checked==true ){
			document.f_focus.elements[i].checked = true;
			if (row_id!="" ){
				getobj(row_id).className = "row_select";
			}
			$('#'+row_id).find(".spancheckbox").addClass("checked");
		}
		else{
			document.f_focus.elements[i].checked  = false;
			if (row_id!="" ){
				if (i%2==0)
					getobj(row_id).className = "row1";
				else
					getobj(row_id).className = "row2";
			}
			$('#'+row_id).find(".spancheckbox").removeClass("checked");
		}
		
	}
}

function do_check_focus (id){
	
	for ( i=0;i < document.f_focus.elements.length ; i++ ){
		if (document.f_focus.elements[i].type=="checkbox" && document.f_focus.elements[i].name!="all" && document.f_focus.elements[i].value == id){
			document.f_focus.elements[i].checked=true;
			getobj("rowfocus_"+id).className = "row_select";
			
			break;
		}	
	}
	
}
function selected_item_focus(){
		var name_count = document.f_focus.length;

		for (i=0;i<name_count;i++){
			if (document.f_focus.elements[i].checked){
				return true;
			}
		}
		alert('Hãy chọn ít nhất 1 record');
		return false;
	}
	
function del_focus_selected(action) {
		if (selected_item_focus()){
			question = confirm('Bạn có chắc muốn XÓA không ?')
			if (question != "0"){
				document.f_focus.action=action;
				document.f_focus.submit();
			}else{
			  alert ('Phew~');
		    }
		}
	
}

</script>
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td align="left" width="15%" nowrap="nowrap">{LANG.total_focus} &nbsp; : </td>
    <td align="left"  class="font_err"><strong>{data.totals}</strong></td>
  </tr>
 </table>
 
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:5PX;">
  <tr>
  <td height="30" style="border-bottom: 2px solid rgb(184, 65, 32);" class="font_title">{LANG.list_focus_news_images}</td>
  </tr>
</table>
{data.err}
<form action="{data.link_action}" method="post" name="f_focus" id="f_focus">
	<div class="box-manage">

		<table  class="table table-sm table-bordered table-hover " id="table_list" >
    <thead>
		  <tr>
			  <th width="5%" align="center"><input type="checkbox" name="all" onclick="javascript:check_all_focus();"></th>
				<th width="10%">{LANG.order}</th>
			  <th width="20%" align="center">{LANG.picture}</th>
			  <th width="55%" align="center">{LANG.title}</th>
			  <th width="15%" align="center">Action</th>
		  </tr>
      </thead>
      <tbody>
		   <!-- BEGIN: row_focus -->
      <tr class="{row.class}" id="{row.row_id}"> 
        <td align="center">{row.check_box}&nbsp;</td>
        <td align="center">{row.order}&nbsp;</td>
        <td align="center">{row.picture}&nbsp;</td>
        <td align="left">{row.title}&nbsp;</td>
        <td align="center">{row.action}&nbsp;</td>
      </tr>
      <!-- END: row_focus -->
      
      <!-- BEGIN: row_focus_no -->
         <tr class="row0" >
           <td  colspan="5" align="center" class="font_err" >{mess}</td>
         </tr>
         <!-- END: row_focus_no -->
         
      </tbody>
		</table>
		<div class="nav-action nav-bottom">
		<input type="submit" name="btnEdit" value="{LANG.update_order}" class="button"  >
		<input type="button" name="btnDel" value="{LANG.del_focus}" class="button" onclick="javascript:del_focus_selected('{data.link_del}');" ></td>
	 	</div>
	</div>
</form>    
<br>
<table width="100%"  border="0" cellspacing="1" cellpadding="1" class="bg_tab" align=center>
      <tr  height=25>
        <td align="left" >{data.nav}</td>
      </tr>
</table>
<!-- END: list_focus_news -->
