<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="news"  class="validate">
<table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">		
		<tr class="form-required">
			<td width="20%" class="row1">{LANG.cat_news}: <span class="font_err">(*)</span></td>
			<td class="row0">{data.list_cat} </td>
		</tr>
		<tr class="form-required">
			<td class="row1">{LANG.page}: <span class="font_err">(*)</span></td>
			<td class="row0">{data.list_site}</td>
		</tr>
		<tr class="form-required">
			<td class="row1">URL: <span class="font_err">(*)</span></td>
			<td class="row0"><input name="url" class="textfiled" type="text"  value="{data.url}" size="60" /></td>
		</tr>

		<tr>
			<td class="row1">Là RSS : </td>
			<td class="row0">{data.list_rss}</td>
		</tr>
		
		<tr>
			<td class="row1">{LANG.auto}  : </td>
			<td class="row0">{data.list_auto}</td>
		</tr>
		<tr>
			<td class="row1">{LANG.timer}  : </td>
			<td class="row0"><input name="timer" class="textfiled" type="text"  value="{data.timer}" size="10" /> s</td>
		</tr>
		

		        
  <tr align="center">
  	<td class="row1">&nbsp; </td>
     <td class="row0">
			<input type="hidden" name="do_submit" value="1"  />
			<input type="submit" name="btnSubmit" value="Submit" class="button">
      <input type="reset" name="Submit2" value="Reset" class="button">            
	</td></tr>
</table>
</form>
<br>
<!-- END: edit -->
<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left" class="row1">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->