<!-- BEGIN: manage -->
{data.err}
<form action="{data.link_action}" method="post" name="f_setting" id="f_setting" >
<table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
	</tr>
	<tr>
    <td class="row1" valign="top" >Module slogan :</td>
    <td class="row0">
      <input name="slogan" id="slogan" type="text" size="70" maxlength="250" value="{data.slogan}">
    </td>
  </tr>
 	<tr>
    <td class="row1" valign="top" >Friendly Title :</td>
    <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
  </tr>
  <tr>
    <td class="row1">Meta Keyword :</td>
    <td class="row0"><input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield"></td>
  </tr>
  <tr>
    <td class="row1">Meta Description : </td>
    <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
  </tr>
  <tr>
    <td class="row1">Rebuild Link SEO: </td>
    <td class="row0"><input type="button" name="Rebuild" value=" &nbsp; Rebuild Link &nbsp;" class="button" onclick="location.href='{data.link_rebuild}'" /></td>
  </tr>
</table><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.setting_news}</strong></td>
	</tr>
	<tr>
		<td width="35%" class="row1">{LANG.comment_news} :</td>
		<td class="row0">{data.comment}</td>
	</tr>
	<tr>
		<td class="row1">{LANG.confirm_comment}:</td>
		<td class="row0">{data.comment_display}</td>
	</tr>
	<tr>
		<td class="row1">{LANG.print_news}:</td>
		<td class="row0">{data.print}</td>
	</tr>
	<tr>
	  <td class="row1">{LANG.get_image}: </td>
	  <td class="row0">{data.get_image}</td>
  </tr>
</table><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="admintable">
 	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.config_news}</strong></td>
	</tr>
	<tr>
		<td width="35%" class="row1">{LANG.imgthumb_align}:</td>
		<td class="row0">{data.imgthumb_align}</td>
	</tr>
	<tr>
		<td width="35%" class="row1">{LANG.show_nophoto}:</td>
		<td class="row0">{data.nophoto}</td>
	</tr>
	<tr >
   	<td class="row1">Hình Nophoto : </td>
   	<td class="row0">
   		<div id="ext_pic_nophoto" class="picture" >{data.image_nophoto}</div>
      <input type="hidden" name="pic_nophoto"	 id="pic_nophoto" value="{data.pic_nophoto}" />
      <div id="btnU_pic_nophoto" class="div_upload" {data.style_upload} ><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
  	</td>
  </tr>
	<tr>
		<td class="row1">{LANG.imgthumb_width} :</td>
		<td class="row0"><input name="imgthumb_width" size="20" type="text" class="textfiled" value="{data.imgthumb_width}" onKeyPress="return is_num(event,'imgthumb_width')"></td>
	</tr>
	<tr>
		<td class="row1">{LANG.imgdetail_width} :</td>
		<td class="row0"><input name="imgdetail_width" size="20" type="text" class="textfiled" value="{data.imgdetail_width}"  onkeypress="return is_num(event,'imgdetail_width')"></td>
	</tr>
	<tr>
		<td class="row1">{LANG.img_width} :</td>
		<td class="row0"><input name="img_width" size="20" type="text" class="textfiled" value="{data.img_width}"  onkeypress="return is_num(event,'img_width')"></td>
	</tr>
  <tr>
		<td class="row1">{LANG.imgfocus_width}:</td>
		<td class="row0"><input name="imgfocus_width" size="20" type="text" class="textfiled" value="{data.imgfocus_width}"  onkeypress="return is_num(event,'imgfocus_width')"></td>
	</tr>
  <tr>
		<td class="row1">{LANG.imgbox_width}:</td>
		<td class="row0"><input name="imgblock_width" size="20" type="text" class="textfiled" value="{data.imgblock_width}"  onkeypress="return is_num(event,'imgblock_width')"></td>
	</tr>
	<tr>
		<td class="row1">{LANG.folder_upload} :</td>
		<td class="row0">{data.folder_upload}</td>
	</tr>
</table><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.config_category}</strong></td>
	</tr>
	<tr>
		<td width="35%" class="row1">{LANG.n_focus_cat}:</td>
		<td class="row0"><input name="n_focus_cat" size="20" type="text" class="textfiled" value="{data.n_focus_cat}" onKeyPress="return is_num(event,'n_focus_cat')"></td>
	</tr>
	<tr>
		<td class="row1">{LANG.n_cat} :</td>
		<td class="row0"><input name="n_cat" size="20" type="text" class="textfiled" value="{data.n_cat}" onKeyPress="return is_num(event,'n_cat')"></td>
	</tr>
</table><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"  class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.config_detail}</strong></td>
	</tr>
	<tr>
		<td class="row1" width="35%">{LANG.n_referent} :</td>
		<td class="row0"><input name="n_referent" size="20" type="text" class="textfiled" value="{data.n_referent}" onKeyPress="return is_num(event,'n_referent')"></td>
	</tr>
	<tr>
		<td class="row1">{LANG.n_other} :</td>
		<td class="row0"><input name="other_detail" size="20" type="text" class="textfiled" value="{data.other_detail}" onKeyPress="return is_num(event,'other_detail')"></td>
	</tr>
</table><br>
<p align="center">
	<input type="hidden" name="do_submit" value="1">
	<input name="btnEdit" type="submit" value="Submit" class="button">
</p>
</form><br />
<!-- END: manage -->