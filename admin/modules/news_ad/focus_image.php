<?php
/*================================================================================*\
|| 						           Name code : focus_news.php 		 		            	  			||
||  				     Copyright @2008 by Thai Son - CMS vnTRUST                  			||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "focus_image";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;

    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		
		$vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module . ".js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/auto_suggest/autosuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/auto_suggest/bsn.AutoSuggest.js");
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_focus_image'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_focus_image'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_focus_image'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
		$arr_focusid = $vnT->input["focus_id"] ;
    if(is_array($arr_focusid)) 
	  {
    	
		 	foreach ($arr_focusid as $newsid) 
			{
				$res_ck = $DB->query("SELECT id FROM news_focus_image WHERE newsid=$newsid AND lang='$lang' ");
				if(!$DB->num_rows($res_ck))
				{
					$cot['newsid'] = $newsid;
					$cot['picture'] = ''; 
					$ok = $DB->do_insert("news_focus_image",$cot);					
				}
			}
    }
		 
		//xoa cache
		$func->clear_cache();
		//insert adminlog
		$func->insertlog("Set focus", $_GET['act'], $ids);
		$mess = $vnT->lang['set_focus_success'];
		$url = $this->linkUrl ;
    $func->html_redirect($url, $mess);
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
	  $w = ($vnT->setting['imgfocus_width']) ? $vnT->setting['imgfocus_width'] : 500;
    if ($vnT->input['btnEdit']) 
		{
			//upload
			if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
				$up['path'] = MOD_DIR_UPLOAD;
				$up['dir'] = "news_focus";
				$up['file'] = $_FILES['image'];
				$up['type'] = "hinh";
				$up['w'] = $w;
				$result = $vnT->File->Upload($up);
				if (empty($result['err'])) {
					$picture = $result['link'];
					$file_type = $result['type'];
				} else {
					$err = $func->html_err($result['err']);
				}
			} else {
				if ($vnT->input['picture']) {
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir'] = "news_focus";
					$up['url'] = $vnT->input['picture'];
					$up['type'] = "hinh";
					$up['w'] = $w;
					$result = $vnT->File->UploadURL($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}
			} //end upload
			
      if (empty($err)) 
			{
       
        $cot['newsid'] = $vnT->input['newsid'];
				if ($vnT->input['chk_upload'] == 1 || ! empty($picture)) {
          // Del Image
          $query = $DB->query("SELECT picture FROM news_focus_image WHERE id='{$id}'");
          if ($img = $DB->fetch_row($query)) {
            $file_pic = MOD_DIR_UPLOAD . "news_focus/" . $img['picture'];
            //echo $file_pic;
            if (! empty($img['picture']) && file_exists($file_pic))
              @unlink($file_pic);
          }
          // end del
          $cot['picture'] = $picture;
        }
        
        $ok = $DB->do_update("news_focus_image", $cot, "id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl  ;
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT img.* , n.title	FROM news_focus_image img , news_desc n WHERE img.newsid=n.newsid AND n.lang='$lang' AND img.id='{$id}'");
    if ($data = $DB->fetch_row($query)) 
		{
      if ($data['picture']) {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "news_focus/" . $data['picture'] . "\"  />";
      } else {
        $data['pic'] = "";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
		
		$data['note_size'] = 'Kích thước 410x220 pixels';
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_focus"])) {
      $ids = implode(',', $vnT->input["del_focus"]);
    }
    // Del Image
    $query = $DB->query("SELECT picture FROM news_focus_image WHERE id IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query)) {
      $file_pic = MOD_DIR_UPLOAD . "news_focus/" . $img['picture'];
      if ((file_exists($file_pic)) && (! empty($img['picture'])))
        unlink($file_pic);
    }
    // End del image
    $query = 'DELETE FROM news_focus_image WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $data['table_list_focus'] = $this->list_focus_news($lang);
    $data['table_list'] = $this->list_news($lang);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW	
    $id = $row['newsid'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"focus_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = "?mod=news&act=news&sub=edit&id={$id}&lang=$lang";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = $this->get_picture($row['picture'], 50);
    }
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['cat_name'] = $this->get_cat_name($row['cat_id'], $lang);
    $output['picture'] = $picture;
    return $output;
  }

  /**
   * function list_news 
   *  
   **/
  function list_news ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $p = ((int) $vnT->input['p']) ? $vnT->input['p'] : 1;
		$cat_id = ((int) $vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0;
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "newsid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		
    $where = "";		
    $ext = "";
    if (! empty($cat_id)) {
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
		
		if(!empty($keyword)){
			switch($search){
				case "newsid" : $where .=" and  n.newsid = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
		
    $query = $DB->query("SELECT n.newsid 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang' ");
    $totals = $DB->num_rows($query);
    $n = 20;
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)     $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl."&sub=add";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|40%|left" , 
      'cat_name' => $vnT->lang['category'] . "|30%|center");
    $sql = "SELECT * 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang'
						$where 
						ORDER BY date_post DESC, n.newsid DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['newsid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] = "&nbsp;&nbsp;<input type=\"button\" name=\"btnEdit\" value=\"" . $vnT->lang['set_focus_news'] . "\" class=\"button\" onclick=\"javascript:do_submit('do_edit')\">";
    $data['table_list'] = $func->ShowTable($table);
    $data['listcat'] = $this->Get_Cat($cat_id, $lang);
    $data['list_search'] = $this->List_Search($search);
    $data['keyword'] = $keyword;
    $data['link_fsearch'] = "";
    $data['nav'] = $nav;
    $data['f_title'] = 'DANH SÁCH CÁC TIN TỨC';
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_news");
    return $this->skin->text("list_news");
  }

  /**
   * function render_row_focus 
   *  
   **/
  function render_row_focus ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "rowfocus_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_focus[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = $this->linkUrl."&sub=edit&id={$id}";
		$link_del = $this->linkUrl."&sub=del&id={$id}";

    $output['order'] = "<input name=\"txtOrder[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['display_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check_focus($id)' />";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = "<img src=\"" . MOD_DIR_UPLOAD . "news_focus/" . $row['picture'] . "\" width=100  />";
    }
    $output['title'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['title']) . "</a>";
    $output['picture'] = $picture;
		
		$output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
		
    return $output;
  }

  /**
   * function list_focus_news 
   *  
   **/
  function list_focus_news ($lang)
  {
    global $func, $DB, $conf, $vnT;
    //update
    if (isset($vnT->input["btnEdit"])) {
      //xoa cache
      $func->clear_cache();
      if (isset($vnT->input["del_focus"]))     $h_id = $vnT->input["del_focus"];
      $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
      $str_mess = "";
      if (isset($vnT->input["txtOrder"]))     $arr_order = $vnT->input["txtOrder"];
      for ($i = 0; $i < count($h_id); $i ++) {
          $dup['display_order'] = $arr_order[$h_id[$i]];
        $ok = $DB->do_update("news_focus_image", $dup, "id={$h_id[$i]}");
        if ($ok) {
          $str_mess .= $h_id[$i] . ", ";
        }
      }
      $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
      $err = $func->html_mess($mess);
      //insert adminlog
      $func->insertlog("Update Order", $_GET['act'], $str_mess);
    }
		
		$p1 = ((int) $vnT->input['p1']) ? $vnT->input['p1'] : 1;

    $query = $DB->query("SELECT img.*  
							FROM news_focus_image img , news_desc n 
							WHERE img.newsid=n.newsid
							AND n.lang='$lang' ");
    $totals = $DB->num_rows($query);
    $n = 5;
    $num_pages = ceil($totals / $n);
    if ($p1 > $num_pages)  $p = $num_pages;
		 if ($p1 < 1)   $p1 = 1;
    $start = ($p1 - 1) * $n;
    $nav = "<div align=left>&nbsp;<span class=\"pagelink\">{$num_pages} Page(s) :</span>&nbsp;";
    for ($i = 1; $i < $num_pages + 1; $i ++) {
      if ($i == $p1)
        $nav .= "<span class=\"pagecur\">{$i}</span>&nbsp";
      else
        $nav .= "<a href='" . $this->linkUrl . "&p1={$i}'><span class=\"pagelink\">{$i}</span></a>&nbsp ";
    }
    $nav .= "</div>";
    $html_row = "";
    $sql .= " SELECT img.* , n.title
							FROM news_focus_image img , news_desc n 
							WHERE img.newsid=n.newsid
							AND n.lang='$lang'
							ORDER BY display_order ASC , id DESC
							LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      while ($row = $DB->fetch_row($result)) {
        $row['ext'] = "";
        $row_info = $this->render_row_focus($row, $lang);
        /*assign the array to a template variable*/
        $this->skin->assign('row', $row_info);
        $this->skin->parse("list_focus_news.row_focus");
      }
    } else {
      $this->skin->assign('mess', $vnT->lang['no_have_focus']);
      $this->skin->parse("list_focus_news.row_focus_no");
    }
    $data['html_row'] = $html_row;
    $data['totals'] = $totals;
    $data['nav'] = $nav;
    $data['err'] = $err;
    $data['link_del'] = $this->linkUrl . "&sub=del";
    $data['link_action'] = $this->linkUrl ;

    //$text_out .= $this->skin->html_list_focus_news($data);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("list_focus_news");
    $text_out = $this->skin->text("list_focus_news");
    return $text_out;
  }
	
  // end class
}
$vntModule = new vntModule();
?>
