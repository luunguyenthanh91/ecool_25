<?php
/*================================================================================*\
|| 							Name code : faqs.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}

//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "bot_news";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        {
          $nd['f_title'] = 'Tạo bot news mới';
          $nd['content'] = $this->do_Add($lang);
        }
        ;
      break;
      case 'edit':
        {
          $nd['f_title'] = 'Cập nhật bot news';
          $nd['content'] = $this->do_Edit($lang);
        }
        ;
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        {
          $nd['f_title'] = 'Danh sách bot news';
          $nd['content'] = $this->do_Manage($lang);
        }
        ;
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['display'] = 1;
    $data['gio'] = date("H:i");
    $data['ngay'] = date("d/m/Y");
    $data['answer_by'] = $vnT->admininfo['username'];
    $data['file_attach'] = '<span class=font_err>Chức năng này vô hiệu lực trong admin</span>';
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $url = trim($vnT->input['url']);
      // Check for Error
      $query = $DB->query("SELECT bid FROM news_bot  WHERE url='{$url}' ");
      if ($check = $DB->fetch_row($query))
        $err = $func->html_err("URL existed");
        // insert CSDL
      if (empty($err)) {
        $cot['site'] = $vnT->input['site'];
        $cot['cat_id'] = $vnT->input['cat_id'];
        $cot['url'] = $url;
        $cot['rss'] = $vnT->input['rss'];
        $cot['auto'] = $vnT->input['auto'];
        $cot['timer'] = $vnT->input['timer'];
        $cot['lang'] = $lang;
        $kq = $DB->do_insert("news_bot", $cot);
        if ($kq) {
          //xoa cache
          $func->clear_cache();
          $fid = $DB->insertid();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $fid);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['list_cat'] = $this->Get_Cat($vnT->input['cat_id'], $lang);
    $data['list_site'] = $this->List_Site_Bot($vnT->input['site'], "");
    $data['list_rss'] =  vnT_HTML::list_yesno("rss", $data['rss']); 
    $data['list_auto'] = vnT_HTML::list_yesno("auto", $data['auto']);  
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $url = trim($vnT->input['url']);
      // Check for Error
      $query = $DB->query("SELECT bid FROM news_bot  WHERE url='{$url}' and bid<>$id ");
      if ($check = $DB->fetch_row($query))
        $err = $func->html_err("URL existed");
      if (empty($err)) {
        $cot['site'] = $vnT->input['site'];
        $cot['cat_id'] = $vnT->input['cat_id'];
        $cot['url'] = $url;
        $cot['rss'] = $vnT->input['rss'];
        $cot['auto'] = $vnT->input['auto'];
        $cot['timer'] = $vnT->input['timer'];
        $kq = $DB->do_update("news_bot", $cot, "bid=$id");
        if ($kq) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM news_bot WHERE bid=$id");
    if ($data = $DB->fetch_row($query)) {
      $data['list_cat'] = $this->Get_Cat($data['cat_id'], $lang);
      $data['list_site'] = $this->List_Site_Bot($data['site'], "");
      $data['list_rss'] =  vnT_HTML::list_yesno("rss", $data['rss']); 
   	  $data['list_auto'] = vnT_HTML::list_yesno("auto", $data['auto']);  
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $del = 1;
      $qr = " OR bid='{$id}' ";
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
      $key = $vnT->input["del_id"];
    }
    $query = "DELETE FROM news_bot WHERE bid=-1" . $qr;
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['bid'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['f_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $output['site'] = $func->HTML($row['site']);
    $output['cat_id'] = get_cat_name($row['cat_id'], $lang);
    $output['auto'] = $row['auto'];
    $output['rss'] = $row['rss'];
    $output['timer'] = $row['timer'];
    $output['url'] = "<a href=\"{$link_edit}\">" . $func->HTML($row['url']) . "</a>";
    if ($row['display'] == 1) {
      $display = "<img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 />";
    }
    $output['run'] = "<a onclick=\"run_newsbot('{$row['site']}',{$id});\" href=\"javascript:;\"><img  alt=\"Run BOT\" src=\"" . $vnT->dir_images . "/but_run.gif\" width=16/></a>";
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['s_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("news_bot", $dup, "bid={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("news_bot", $dup, "bid={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("news_bot", $dup, "bid={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT bid FROM news_bot where lang='$lang'  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"all\" class=\"checkbox\" onclick=\"javascript:checkall();\" value='all' />|5%|center" , 
      'site' => "Trang|10%|center" , 
      'cat_id' => "Danh mục|20%|center" , 
      'url' => "URL |30%|left" , 
      'rss' => "RSS |5%|center" , 
      'auto' => "Auto|5%|center" , 
      'timer' => "Thời gian|10%|center" , 
      'run' => "Run Bot|7%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM news_bot where lang='$lang' ORDER BY  bid DESC  LIMIT $start,$n";
    // print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_faqs'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
$vntModule = new vntModule();
?>
