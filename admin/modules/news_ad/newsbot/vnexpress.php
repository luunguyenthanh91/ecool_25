<?
//===============================================================
// vnexpress.net News Robot - Coded by NDK (ndkmusic@gmail.com)
// Last Modified: 2/3/2008
//===============================================================
@ini_set("max_execution_time", 0);
define('IN_vnT', 1);
define('DIR_UPLOAD', '../../../../vnt_upload/news');
//define('DB_QUERIES', true);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;

$conf = $func->fetchDbConfig($conf);
/*-------------- loadSetting --------------------*/

$setting = array();
$result = $DB->query("select * from news_setting");
$setting = $DB->fetch_row($result);
foreach ($setting as $k => $v) {
	$setting[$k] = stripslashes($v);
}
 



///============= thum_news
function thum_news ($imgfile = "", $path, $max)
{
  //	$fext = mime_content_type ($imgfile); 
  $info = getimagesize($imgfile);
  $mime = $info[2];
  $fext = ($mime == 1 ? 'image/gif' : ($mime == 2 ? 'image/jpeg' : ($mime == 3 ? 'image/png' : NULL)));
  switch ($fext) {
    case 'image/jpeg':
      if (! function_exists('imagecreatefromjpeg')) {
        die('No create from JPEG support');
      } else {
        $img['src'] = @imagecreatefromjpeg($imgfile);
      }
    break;
    case 'image/png':
      if (! function_exists('imagecreatefrompng')) {
        die("No create from PNG support");
      } else {
        $img['src'] = @imagecreatefrompng($imgfile);
      }
    break;
    case 'image/gif':
      if (! function_exists('imagecreatefromgif')) {
        die("No create from GIF support");
      } else {
        $img['src'] = @imagecreatefromgif($imgfile);
      }
    break;
    default:
      if (! function_exists('imagecreatefromjpeg')) {
        die('No create from JPEG support');
      } else {
        $img['src'] = @imagecreatefromjpeg($imgfile);
      }
    break;
  }
  $img['old_w'] = imagesx($img['src']);
  $img['old_h'] = imagesy($img['src']);
  $new_h = $img['old_h'];
  $new_w = $img['old_w'];
  if ($img['old_w'] > $max) {
    $new_w = $max;
    $new_h = ($max / $img['old_w']) * $img['old_h'];
  }
  if ($new_h > $max) {
    $new_h = $max;
    $new_w = ($new_h / $img['old_h']) * $img['old_w'];
  }
  $img['des'] = imagecreatetruecolor($new_w, $new_h);
  $balck = imagecolorallocate($img['des'], 000, 000, 000);
  imagefill($img['des'], 1, 1, $balck);
  imagecopyresampled($img['des'], $img['src'], 0, 0, 0, 0, $new_w, $new_h, $img['old_w'], $img['old_h']);
  $lastx = strrpos($imgfile, "/");
  $path = $path . substr($imgfile, $lastx + 1);
  touch($path);
  switch ($fext) {
    case 'image/pjpeg':
    case 'image/jpeg':
    case 'image/jpg':
      imagejpeg($img['des'], $path, 100);
    break;
    case 'image/png':
      imagepng($img['des'], $path, 100);
    break;
    case 'image/gif':
      imagegif($img['des'], $path, 100);
    break;
    default:
      imagejpeg($img['des'], $path, 100);
    break;
  }
  imagedestroy($img['des']);
}

///================== replace_img =============
function replace_img ($str, $rootpath, $rooturl, $w_thumb=100)
{
  global $conf;
	
  $data['result'] = $tmp = $str;
  $data['url'] = "";
  $up = 1;
  $path_thumb = $rootpath . "thumbs/";
  $rootURI = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $rooturl);
  while ($start = strpos($tmp, "src=")) {
    $end = strpos($tmp, '"', $start + strlen("src=") + 1);
    $http = substr($tmp, $start + strlen("src=") + 1, ($end - ($start + strlen("src=") + 1)));
    //echo "http = ".$http."<br>" ;
    if (! strstr($http, $conf['rooturl'])) {
      // upload
      $fext = strtolower(substr($http, strrpos($http, ".") + 1));
      if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp")) {
        $lastx = strrpos($http, "/");
        $fname = $rootpath . substr($http, $lastx + 1);
        $fname = str_replace("%20", "_", $fname);
        $fname = str_replace(" ", "_", $fname);
        $fname = str_replace("&amp;", "_", $fname);
        $fname = str_replace("&", "_", $fname);
        $fname = str_replace(";", "_", $fname);
        if (file_exists($fname)) {
          $fname = $rootpath . time() . "_" . substr($http, $lastx + 1);
        }
        $file = @fopen($fname, "w");
        if ($f = @fopen($http, "r")) {
          while (! @feof($f)) {
            @fwrite($file, fread($f, 1024));
          }
          @fclose($f);
          @fclose($file);
          $url = $rootURI . substr($fname, strrpos($fname, "/") + 1);
          $data['result'] = str_replace($http, $url, $data['result']);
          if ($up == 1) {
            $data['url'] = substr($fname, strrpos($fname, "/") + 1);
            thum_news($rooturl . substr($fname, strrpos($fname, "/") + 1), $path_thumb, $w_thumb);
            $up = 0;
          }
        } else
          $data['err'] = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
      } else
        $data['err'] = "Image Type Not Support";
    } //end if strstr
    $tmp = substr($tmp, $end + 1);
  } // end while
  return $data;
}
//======================== Load Config ====================
$time = time();
$idonly = (int) $_GET['idonly'];
if ($idonly > 0) {
  $qr = $DB->query("SELECT * FROM news_bot WHERE site='vnexpress' AND bid='{$idonly}'");
} else {
  $qr = $DB->query("SELECT * FROM news_bot WHERE (lastget+timer)<={$time} AND site='vnexpress' AND auto ORDER BY lastget ASC");
}
$newsbot = $DB->get_array($qr);
//print_r($newsbot);
//======================== Begin BOT ======================
if (count($newsbot) > 0) {
  // Load do` nghe` ra :D
  include_once ("classes/socket.php");
  include_once ("classes/xml2ary.php");
  $sock = new sock_connect();
  for ($i = 0; $i < count($newsbot); $i ++) {
    $nbot = $newsbot[$i];
    // Get XML
    //echo $nbot['url']."<br>";
    $news_arr = ($nbot['rss']) ? get_rss($nbot['url']) : get_html($nbot['url']);
    $news_arr = array_reverse($news_arr);
    if (count($news_arr > 0)) {
      for ($k = 0; $k < count($news_arr); $k ++) {
        $news = $news_arr[$k];
        // Kiem tra da import chua?
        $qr = $DB->query("SELECT newsid FROM news WHERE sourceurl='" . $news['link'] . "'");
        if ($res = $DB->fetch_row($qr)) {
          // Skip
        } else {
          //echo $news['link']."<br>";
          // Lay noi dung
          $html = $sock->get_url($news['link']);
          $str_start = "<P class=Title>";
          $str_end = "<div class=\"folder-news\">";
          $html = substr($html, strpos($html, $str_start));
          $html = substr($html, 0, strpos($html, $str_end));
          //co comment
          if (strstr($html, 'box-item')) {
            $str_end_c = "<div class=\"box-item\"";
            $html = substr($html, 0, strpos($html, $str_end_c));
          }
          $image = $news['image'];
          if (! empty($html)) {
            $html = "<div>" . $html;
            preg_match_all("/ src=['|\"](.*?)['|\"]/", $html, $matches, PREG_SET_ORDER);
            for ($im = 0; $im < count($matches); $im ++) {
              $http = $matches[$im][1];
              $newlink = (substr($http, 0, 5) != "http:") ? ((substr($http, 0, 1) != "/") ? $url : "http://vnexpress.net") . $http : $http;
              if (($im == 0) && (empty($image)))
                $image = $newlink;
              if ($newlink != $http)
                $html = str_replace($http, $newlink, $html);
            }
            preg_match_all("/ href=['|\"](.*?)['|\"]/", $html, $matches, PREG_SET_ORDER);
            for ($im = 0; $im < count($matches); $im ++) {
              $http = $matches[$im][1];
              if (substr($http, 0, 5) != "http:")
                $html = str_replace($http, "http://vnexpress.net" . $http, $html);
            }
            preg_match_all("/\<P class=Title\>(.+)\<\/P\>/i", $html, $matches);
            $title = trim(str_replace("&nbsp;", "", $matches[1][0]));
            //echo "<br> title= ".$title."<br>";
            $html = preg_replace("/\<P class=Title\>(.+)\<\/P\>/i", "", $html);
            preg_match_all("/\<P class=Lead\>(.+)\<\/P\>/i", $html, $matches);
            $matches[1][0] .= "<BR>";
            $des = strip_tags(substr($matches[1][0], 0, strpos($matches[1][0], "<BR")));
            //echo "<br> des= ".$des."<br>";
          }
          $html = preg_replace("/\<P class=Title\>(.+)\<\/P\>/i", "", $html);
          // OK
          // lay hinh
          $dir = date("m_Y");
          $dir_thumbs = $dir . "/thumbs";
          $path_dir = DIR_UPLOAD . "/" . $dir;
          $path_thumb_dir = DIR_UPLOAD . "/" . $dir_thumbs;
          if (! file_exists($path_dir)) {
            @mkdir($path_dir, 0777);
            @exec("chmod 777 {$path_dir}");
            if (! file_exists($path_thumb_dir)) {
              @mkdir($path_thumb_dir, 0777);
              @exec("chmod 777 {$path_thumb_dir}");
            }
          }
          $rootpath = DIR_UPLOAD . "/" . $dir . "/";
          $rooturl = $conf['rooturl'] . "vnt_upload/news/" . $dir . "/";

          $tmp_content = replace_img($html, $rootpath, $rooturl, $setting['imgthumb_width']);
          $content = $tmp_content['result'];
          if ($tmp_content['url']) {
            $picture = $dir . "/" . $tmp_content['url'];
          } else {
            $picture = "";
          }
          //$content = $html ;
          //$pictur = $image ;
          $dbup = array(
            'cat_id' => $nbot['cat_id'] , 
            'picture' => $picture , 
            'poster' => "VNExpress BOT" , 
            'source' => 1 , 
            'sourceurl' => $news['link'] , 
            'date_post' => (! empty($news['pubDate'])) ? strtotime($news['pubDate']) : time() , 
            'type_show' => 2 );
          if ((! empty($title)) && (! empty($content))) {
            $doit = $DB->do_insert("news", $dbup);
            if ($doit) {         
							$newsid = $DB->insertid();
							//update content
							$cot_d['newsid'] = $newsid;
							$cot_d['title'] = strip_tags($title);
							$cot_d['short'] = $des;
							$cot_d['content'] = $content;
							
							$cot_d['friendly_url'] = $func->make_url($title);
							$cot_d['friendly_title'] = $func->utf8_to_ascii($title);
														
							$query_lang = $DB->query("select name from language ");
							while ( $row = $DB->fetch_row($query_lang) ) {
								$cot_d['lang'] = $row['name'];
								$DB->do_insert("news_desc",$cot_d);
							}
							
              echo "Category {$nbot['cat_id']}: {$title}<br>";
            }
          }
          // Nghi ngoi xiu O.o
          //break;
          sleep(2);
        } // end kiem tra
      } // end for
    } // end if news_arr
    $doit = $DB->query("UPDATE news_bot SET lastget='" . time() . "' WHERE bid='{$nbot['bid']}'");
    sleep(1);
  }
}

// ===================== Functions ========================
function get_rss ($link)
{
  global $sock;
  $news_arr = array();
  $xml = $sock->get_xml(rawurldecode($link));
  if (! empty($xml)) {
    // Xu ly XML
    $res = xml2ary($xml);
    $items = $res['rss']['_c']['channel']['_c']['item'];
    // Xu ly lai cho gon
    for ($k = 0; $k < count($items); $k ++) {
      $tmp = array(
        'title' => $items[$k]['_c']['title']['_v'] , 
        'description' => $items[$k]['_c']['description']['_v'] , 
        'link' => $items[$k]['_c']['link']['_v'] , 
        'pubDate' => $items[$k]['_c']['pubDate']['_v']);
      if ((! empty($tmp['title'])) && (! empty($tmp['link'])))
        $news_arr[] = $tmp;
    }
  }
  return $news_arr;
}

//---------
function get_html ($link)
{
  global $sock;
  $news_arr = array();
  $html = $sock->get_url($link);
  if (! empty($html)) {
    $html = preg_replace("([\\r\\n|\\n|\\r]*)", "", $html);
    //echo $html;
    preg_match_all("/ href=\"([^\"]*)\"([^\"]*) class=\"link-title\"/i", $html, $matches);
    $items = $matches[1];
    //echo "<pre>" ;
    //print_r($items);
    //echo "</pre>" ;
    preg_match_all("/ src=\"([^\"]*)\"/i", $html, $matches);
    $images = $matches[1];
    for ($k = 0; $k < count($items); $k ++) {
      $image = "";
      for ($j = 0; $j < count($images); $j ++) {
        if (substr($images[$j], 0, strlen($items[$k])) == $items[$k])
          $image = "http://vnexpress.net" . $images[$j];
      }
      $tmp = array(
        'title' => " " , 
        'description' => " " , 
        'link' => "http://vnexpress.net" . $items[$k] , 
        'pubDate' => "" , 
        'image' => $image);
      if (! empty($tmp['link']))
        $news_arr[] = $tmp;
    }
    //echo "<pre>" ;
  //print_r($news_arr);
  //echo "</pre>" ;
  }
  return $news_arr;
}
//======================== End BOT ========================
$DB->close();
?>