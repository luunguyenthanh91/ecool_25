<?

class sock_connect
{
  var $host = "";
  var $path = "";
  var $port = 80;
  var $proxy = "";
  var $html = "";
  var $proxy_regex = '(\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b)';

  //=======================================================
  function sock_connect ()
  {
    @error_reporting(0);
    @ini_set("max_execution_time", 0);
    @ini_set("default_socket_timeout", 10);
  }

  //=======================================================
  function get_url ($url)
  {
    preg_match('@^(?:http://)?(?:www.)?([^/]+)@i', $url, $matches);
    $this->host = $matches[1];
    $this->path = substr($url, strpos($url, $this->host) + strlen($this->host));
    $packet = "GET " . $this->path . " HTTP/1.1\r\n";
    $packet .= "User-Agent: Googlebot/2.1 (+http://www.google.com/bot.html)\r\n";
    $packet .= "Host: " . $this->host . "\r\n";
    $packet .= "Connection: close\r\n\r\n";
    $this->sendpacket($packet);
    $html = $this->html;
    // end
    return $html;
  }

  //=======================================================
  function get_xml ($url)
  {
    preg_match('@^(?:http://)?(?:www.)?([^/]+)@i', $url, $matches);
    $this->host = $matches[1];
    $this->path = substr($url, strpos($url, $this->host) + strlen($this->host));
    $packet = "GET " . $this->path . " HTTP/1.1\r\n";
    $packet .= "User-Agent: Googlebot/2.1 (+http://www.google.com/bot.html)\r\n";
    $packet .= "Host: " . $this->host . "\r\n";
    $packet .= "Connection: close\r\n\r\n";
    $this->sendpacket($packet);
    $html = $this->parse_XML($this->html);
    // end
    return $html;
  }

  //=======================================================
  function parse_XML ($str)
  {
    $xml = $str;
    $start = "<?xml";
    $end = "</rss>";
    $xml = substr($xml, strpos($xml, $start));
    $xml = substr($xml, 0, strpos($xml, $end) + strlen($end));
    return $xml;
  }

  //=======================================================
  function sendpacket ($packet)
  {
    if ($this->proxy == '') {
      $ock = fsockopen(gethostbyname($this->host), $this->port);
      if (! $ock) {
        echo 'No response from ' . $this->host . ':' . $this->port;
        die();
      }
    } else {
      $c = preg_match($this->proxy_regex, $this->proxy);
      if (! $c) {
        echo 'Not a valid proxy...';
        die();
      }
      $parts = explode(':', $this->proxy);
      echo "Connecting to " . $parts[0] . ":" . $parts[1] . " proxy...\r\n";
      $ock = fsockopen($parts[0], $parts[1]);
      if (! $ock) {
        echo 'No response from proxy...';
        die();
      }
    }
    fputs($ock, $packet);
    if ($this->proxy == '') {
      $this->html = '';
      while (! feof($ock)) {
        $this->html .= fgets($ock);
      }
    } else {
      $this->html = '';
      while ((! feof($ock)) or (! eregi(chr(0x0d) . chr(0x0a) . chr(0x0d) . chr(0x0a), $this->html))) {
        $this->html .= fread($ock, 1);
      }
    }
    fclose($ock);
  }

  //=======================================================
  function sendpacket1 ($packet)
  {
    if ($this->proxy == '') {
      $ock = fsockopen(gethostbyname($this->host), $this->port);
      if (! $ock) {
        echo 'No response from ' . $this->host . ':' . $this->port;
        die();
      }
    } else {
      $c = preg_match($this->proxy_regex, $this->proxy);
      if (! $c) {
        echo 'Not a valid proxy...';
        die();
      }
      $parts = explode(':', $this->proxy);
      echo "Connecting to " . $parts[0] . ":" . $parts[1] . " proxy...\r\n";
      $ock = fsockopen($parts[0], $parts[1]);
      if (! $ock) {
        echo 'No response from proxy...';
        die();
      }
    }
    fputs($ock, $packet);
    if ($this->proxy == '') {
      $html = '';
      while (! feof($ock)) {
        $html .= fgets($ock);
      }
    } else {
      $html = '';
      while ((! feof($ock)) or (! eregi(chr(0x0d) . chr(0x0a) . chr(0x0d) . chr(0x0a), $this->html))) {
        $html .= fread($ock, 1);
      }
    }
    fclose($ock);
    return $html;
  }

  function quick_dump ($string)
  {
    $result = '';
    $exa = '';
    $cont = 0;
    for ($i = 0; $i <= strlen($string) - 1; $i ++) {
      if ((ord($string[$i]) <= 32) | (ord($string[$i]) > 126)) {
        $result .= "  .";
      } else {
        $result .= "  " . $string[$i];
      }
      if (strlen(dechex(ord($string[$i]))) == 2) {
        $exa .= " " . dechex(ord($string[$i]));
      } else {
        $exa .= " 0" . dechex(ord($string[$i]));
      }
      $cont ++;
      if ($cont == 15) {
        $cont = 0;
        $result .= "\r\n";
        $exa .= "\r\n";
      }
    }
    return $exa . "\r\n" . $result;
  }

  function make_seed ()
  {
    list ($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
  }
  // end class
}
?>