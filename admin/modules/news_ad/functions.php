<?php
/*================================================================================*\
|| 							Name code : funtions_music.php 		 			      	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
/*=======================CHANGE LOG================================================
DATE 14/12/2007 :
	- function getToolbar 	them 2 var mod va act de xac dinh ( la`m bieng edit wa ), 
													khong su dung lang (hktrung)
													
	- define MOD_DIR_UPLOAD		thu muc upload cua module ( hktrung )
==================================================================================*/
if (!defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/news/');
define('ROOT_UPLOAD', 'vnt_upload/news/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/news/');


class Model
{
  function __construct()
  {
    $lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
    $this->loadSetting($lang);
  }

  /*-------------- loadSetting --------------------*/
  function loadSetting($lang = "vn")
  {
    global $vnT, $func, $DB, $conf;
    $setting = array();
    $result = $DB->query("select * from news_setting WHERE lang='$lang' ");
    $setting = $DB->fetch_row($result);
    foreach ($setting as $k => $v) {
      $vnT->setting[$k] = stripslashes($v);
    }

    $vnT->setting['arr_category'] = array();
    $res_cat = $DB->query("SELECT n.* , nd.cat_name , nd.friendly_url
            							FROM news_category n, news_category_desc nd 
            							WHERE n.cat_id=nd.cat_id  
            							AND nd.lang='$lang'  
            							AND display != -1
            							ORDER BY cat_order ASC , n.cat_id ASC ");
    while ($row_cat = $DB->fetch_row($res_cat)) {
      $vnT->setting['arr_category'][$row_cat['cat_id']] = $row_cat;
    }
    $vnT->setting['arr_type'] = array("article" => 'Bài viết' , "video" => 'Video', "gallery" => 'Hình ảnh' );
    $vnT->setting['arr_video_type'] = array( 'url' => 'URL Video', 'embed' => 'Embed Video' );
    unset($setting);
  }
  function Get_Cat($did = -1, $lang, $ext = "")  {
    global $func, $DB, $conf;
    $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
    $text .= "<option value=\"\">-- Root --</option>";
    $query= $DB->query("SELECT n.*, nd.cat_name FROM news_category n,news_category_desc nd 
                        WHERE n.cat_id=nd.cat_id AND nd.lang='$lang' 
                        AND n.parentid=0 AND display != -1
                        ORDER BY cat_order ASC ,n.cat_id DESC");
    while ($cat = $DB->fetch_row($query)) {
      $cat_name = $func->HTML($cat['cat_name']);

      $selected = ($cat['cat_id'] == $did) ? " selected" : "";
      $text .= "<option value=\"{$cat['cat_id']}\" " . $selected . ">{$cat_name}</option>";
      $n = 1;
      $text .= $this->Get_Sub($did, $cat['cat_id'], $n, $lang);
    }
    $text .= "</select>";
    return $text;
  }

  /*** Ham Get_Sub   */
  function Get_Sub($did, $cid, $n, $lang)
  {
    global $func, $DB, $conf;
    $output = "";
    $k = $n;
    $query = $DB->query("SELECT n.*, nd.cat_name FROM news_category n,news_category_desc nd 
            WHERE n.cat_id=nd.cat_id
            AND nd.lang='$lang' 
            AND n.parentid={$cid}
            AND display != -1
            ORDER BY cat_order DESC ,n.cat_id DESC");
    while ($cat = $DB->fetch_row($query)) {
      $cat_name = $func->HTML($cat['cat_name']);
      $selected = ($cat['cat_id'] == $did) ? " selected" : "";
      $output .= "<option value=\"{$cat['cat_id']}\" " . $selected . ">";
      for ($i = 0; $i < $k; $i++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";

      $n = $k + 1;
      $output .= $this->Get_Sub($did, $cat['cat_id'], $n, $lang);
    }
    return $output;
  }


  /***** Ham List_SubCat *****/
  function List_SubCat($cat_id)
  {
    global $func, $DB, $conf;
    $output = "";
    $query = $DB->query("SELECT * FROM news_category WHERE parentid={$cat_id}");
    while ($cat = $DB->fetch_row($query)) {
      $output .= $cat["cat_id"] . ",";
      $output .= $this->List_SubCat($cat['cat_id']);
    }
    return $output;
  }

  //---------------- get_cat_name
  function get_cat_name($cat_id, $lang)
  {
    global $vnT, $func, $DB, $conf;
    $result = $DB->query("select cat_name from news_category_desc  where cat_id=$cat_id and lang='$lang'");
    if ($row = $DB->fetch_row($result)) {
      $cat_name = $func->HTML($row['cat_name']);
    }
    return $cat_name;
  }



  //-----------------  get_catCode
  function get_catCode($parentid, $cat_id)
  {
    global $vnT, $func, $DB, $conf;
    $text = "";
    $sql = "select cat_id,cat_code from news_category where cat_id =$parentid ";
    $result = $DB->query($sql);
    if ($row = $DB->fetch_row($result)) {
      $text = $row['cat_code'] . "_" . $cat_id;
    } else
      $text = $cat_id;
    return $text;
  }

  /*-------------- get_cat_root --------------------*/
  function get_cat_root($cat_id)
  {
    global $DB, $conf, $func, $vnT, $input;
    $text = $cat_id;
    $res = $DB->query("SELECT cat_code,cat_id FROM news_category where cat_id=" . (int)$cat_id);
    if ($cat = $DB->fetch_row($res)) {
      $cat_code = $cat['cat_code'];
      $tmp = explode("_", $cat_code);
      $text = $tmp[0];
    }
    return $text;
  }


  /***** Ham get_cat_list *****/
  function get_cat_list($cat_id)
  {
    global $vnT, $func, $DB, $conf;
    $arr_cat = array();
    $res = $DB->query("SELECT cat_id,cat_code FROM news_category where cat_id in (" . $cat_id . ") ");
    while ($row = $DB->fetch_row($res)) {
      $tmp = @explode("_", $row['cat_code']);
      $arr_cat = array_merge($arr_cat, $tmp);
    }
    $arr_cat = array_unique($arr_cat);
    $out = @implode(",", $arr_cat);

    return $out;
  }

  /***** Ham rebuild_cat_list *****/
  function rebuild_cat_list($cat_id)
  {
    global $vnT, $func, $DB, $conf;
    $arr_cat = array();
    $res = $DB->query("SELECT newsid,cat_id FROM news WHERE  FIND_IN_SET('$cat_id',cat_list)<>0 ");
    while ($row = $DB->fetch_row($res)) {
      $DB->query("UPDATE news SET cat_list='" . $this->get_cat_list($row['cat_id']) . "' WHERE newsid=" . $row['newsid']);
    }
    return false;
  }

  /***** Ham rebuild_cat_code *****/
  function rebuild_cat_code($parentid, $cat_id)
  {
    global $vnT, $func, $DB, $conf;

    $cat_code = $this->get_catCode($parentid, $cat_id);
    $DB->query("UPDATE news_category SET cat_code='" . $cat_code . "' WHERE cat_id=" . $cat_id);

    $res_chid = $DB->query("SELECT cat_id FROM news_category WHERE  parentid=" . $cat_id);
    while ($row_chid = $DB->fetch_row($res_chid)) {
      $this->rebuild_cat_code($cat_id, $row_chid['cat_id']);
    }

    return false;
  }


  //-----------------  get_picture
  function get_picture($picture, $w = "")
  {
    global $vnT, $func, $DB, $conf;
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    if ($picture) {
      $linkhinh = "../vnt_upload/news/" . $picture;
      $linkhinh = str_replace("//", "/", $linkhinh);
      $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
      $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
      $src = $dir . "/thumbs/" . $pic_name;
    }
    if ($w < $w_thumb) $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
    return $out;
  }


  /*** Ham List_Keyref ****/
  function List_Keyref($did = -1, $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $text = "<select size=1 id=\"keyref_id\" name=\"keyref_id\" {$ext} >";
    $text .= "<option value=\"0\">-- " . $vnT->lang['select_keyref'] . " --</option>";
    $query = $DB->query("SELECT * FROM news_key_referent WHERE display=1  ORDER BY keyref_order");
    while ($cat = $DB->fetch_row($query)) {
      $title = $func->HTML($cat['title']);
      if ($cat['keyref_id'] == $did)
        $text .= "<option value=\"{$cat['keyref_id']}\" selected>{$title}</option>";
      else
        $text .= "<option value=\"{$cat['keyref_id']}\" >{$title}</option>";
    }
    $text .= "</select>";
    return $text;
  }

  /*------ get_num_referent ---*/
  function get_num_referent($id)
  {
    global $func, $DB, $conf;
    $result = $DB->query("SELECT newsid FROM news WHERE keyref_id=$id  ");
    $num = $DB->num_rows($result);
    return intval($num);
  }


  /*** Ham List_Source ****/
  function List_Source($did = -1, $lang = "vn", $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $text = "<select size=1 id=\"source\" name=\"source\" {$ext} >";
    $text .= "<option value=\"\">-- " . $vnT->lang['select_source'] . " --</option>";
    $sql = "SELECT * FROM news_source WHERE display=1 AND lang='$lang'  ORDER BY s_order";
    //echo $sql;
    $query = $DB->query($sql);
    while ($cat = $DB->fetch_row($query)) {
      $title = $cat['s_title'];
      if ($cat['sid'] == $did)
        $text .= "<option value=\"{$cat['sid']}\" selected>{$title}</option>";
      else
        $text .= "<option value=\"{$cat['sid']}\" >{$title}</option>";
    }
    $text .= "</select>";
    return $text;
  }

  /*** Ham List_Site_Bot ****/
  function List_Site_Bot($did = -1, $ext = "")
  {
    global $func, $DB, $conf;
    $text = "<select size=1 name=\"site\" id=\"site\" {$ext}  >";
    if ($did == "vnexpress")
      $text .= "<option value=\"vnexpress\" selected> vnexpress.net  </option>";
    else
      $text .= "<option value=\"vnexpress\"> vnexpress.net </option>";
    $text .= "</select>";
    return $text;
  }

  /*-------------- get_pic_input --------------------*/
  function get_pic_input ($item_id , $dir ,$w=1000 , $w_thumb=150)
  {
    global $func, $DB, $conf, $vnT;
    $arr_out = array();
    $stt = 1 ;
    if($item_id){
      $arr_pic_old = $_POST['picture_old'] ;
      $arr_old_id = array();
      if(is_array($arr_pic_old)){
        foreach ($arr_pic_old as $pic_old) {

          $tmp = @explode("|",$pic_old);
          $pic_id = trim($tmp[0]);
          $pic_src = trim($tmp[1]);
          $pic_name = $_POST['pic_name_old'][$pic_id];
          $pic_order =  $_POST['pic_order_old'][$pic_id];

          $dup = array();
          $dup['pic_name'] =$pic_name;
          $dup['pic_order'] = $pic_order;
          $vnT->DB->do_update("news_picture",$dup,"item_id=".$item_id." AND id=".$pic_id);
          $stt++;
          $arr_old_id[] = $pic_id;
          $arr_out[$pic_order]['picture'] = $pic_src;
          $arr_out[$pic_order]['pic_name'] = $pic_name;
          $arr_out[$pic_order]['pic_order'] = $pic_order;
        }

        $vnT->DB->query("DELETE FROM news_picture WHERE item_id=".$item_id." AND id not in(".@implode(",",$arr_old_id).")") ;

      }
    }

    $arr_pic = $_POST['pictures'];

    if(is_array($arr_pic))
    {
      foreach($arr_pic as $k => $picture)
      {
        $pic_name =  $_POST['pic_name'][$k];
        $pic_order =  $_POST['pic_order'][$k];
        if($item_id){
          $cot = array();
          $cot['item_id'] = $item_id ;
          $cot['picture'] = $picture ;
          $cot['pic_name'] = $pic_name;
          $cot['pic_order'] = $pic_order;
          $vnT->DB->do_insert("news_picture",$cot);
        }
        $arr_out[$pic_order]['picture'] = $picture;
        $arr_out[$pic_order]['pic_name'] =  $pic_name;
        $arr_out[$pic_order]['pic_order'] = $pic_order;

      }

    }

    return $arr_out;
  }
  ///================== replace_img =============
  function replace_img($str, $dir)
  {
    global $conf, $func, $vnT;
    $data['result'] = $tmp = $str;
    $data['url'] = "";
    $ok_getimg = 1;
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    if ($dir) {
      $path_dir = MOD_DIR_UPLOAD . $dir . "/";
      $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD . $dir . "/";
    } else {
      $path_dir = MOD_DIR_UPLOAD;
      $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD;
    }
    $path_thumb = $path_dir . "thumbs";
    if (!is_dir($path_thumb)) {
      @mkdir($path_thumb, 0777);
      @exec("chmod 777 {$path_thumb}");
    }
    while ($start = strpos($tmp, "src=")) {
      $end = strpos($tmp, '"', $start + 7);
      $http = substr($tmp, $start + 6, ($end - ($start + 7)));
      if (!strstr($http, $conf['rooturl']) && (strstr($http, "http://") || strstr($http, "https://"))) {
        // upload
        $fext = strtolower(substr($http, strrpos($http, ".") + 1));
        if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp")) {
          $lastx = strrpos($http, "/");
          $fname = $path_dir . substr($http, $lastx + 1);
          $fname = str_replace("%20", "_", $fname);
          $fname = str_replace(" ", "_", $fname);
          $fname = str_replace("&amp;", "_", $fname);
          $fname = str_replace("&", "_", $fname);
          $fname = str_replace(";", "_", $fname);
          if (file_exists($fname)) {
            $fname = $path_dir . time() . "_" . substr($http, $lastx + 1);
          }
          $file_name = substr($fname, strrpos($fname, "/") + 1);
          $file = @fopen($fname, "w");
          if ($f = @fopen($http, "r")) {
            while (!@feof($f)) {
              @fwrite($file, fread($f, 1024));
            }
            @fclose($f);
            @fclose($file);
            $url = $rooturl . $file_name;
            /*if ($ok_getimg ==1){
              $data['url'] = $dir . "/" . $file_name;
              $file_thumb = $path_thumb . "/" . $file_name;
              $func->thum( 'http://' . $_SERVER['HTTP_HOST'].$url, $file_thumb, $w_thumb);
              $ok_getimg = 0;
            }*/
            $data['result'] = str_replace($http, $url, $data['result']);
          } else
            $data['err'] = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
        } else
          $data['err'] = "Image Type Not Support";
      } //end if strstr
      $tmp = substr($tmp, $end + 1);
    } // end while
    return $data;
  }

  //======================= List_Search =======================
  function List_Search($did, $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $arr_where = array('newsid' => 'News ID', 'title' => $vnT->lang['title_news'], 'short' => $vnT->lang['short_news'], 'date_post' => $vnT->lang['date_post'] . " (d/m/Y)");

    $text = "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
    foreach ($arr_where as $key => $value) {
      $selected = ($did == $key) ? "selected" : "";
      $text .= "<option value=\"{$key}\" {$selected} > {$value} </option>";
    }
    $text .= "</select>";

    return $text;
  }

  //============List_Display
  function List_Display($did = 0, $ext = "")
  {
    global $func, $vnT, $conf;

    $text = "<select size=1 name=\"display\" id=\"display\" {$ext}'>";
    $text .= "<option value=\"-1\" > -- Tất cả -- </option>";
    if ($did == "0")
      $text .= "<option value=\"0\" selected> " . $vnT->lang['display_no'] . " </option>";
    else
      $text .= "<option value=\"0\" > " . $vnT->lang['display_no'] . " </option>";

    if ($did == "1")
      $text .= "<option value=\"1\" selected> " . $vnT->lang['display_yes'] . " </option>";
    else
      $text .= "<option value=\"1\"> " . $vnT->lang['display_yes'] . " </option>";

    $text .= "</select>";
    return $text;
  }

  //------do_DeleteSub
  function do_DeleteSub($ids, $lang = "vn")
  {
    global $func, $DB, $conf, $vnT;
    $res = $DB->query("SELECT cat_id,parentid FROM news_category WHERE parentid in (" . $ids . ")");
    while ($row = $DB->fetch_row($res)) {
      do_DeleteSub($row['cat_id'], $lang);
      $DB->query("UPDATE news_category_desc SET display=-1 WHERE  cat_id = " . $row['cat_id'] . " AND lang='" . $lang . "' ");

      //insert RecycleBin
      $rb_log['module'] = "news";
      $rb_log['action'] = "category";
      $rb_log['tbl_data'] = "news_category_desc";
      $rb_log['name_id'] = "cat_id";
      $rb_log['item_id'] = $row['cat_id'];
      $rb_log['lang'] = $lang;
      $func->insertRecycleBin($rb_log);

    }

  }

}

$model = new Model();
?>