<?php
/*================================================================================*\
|| 							Name code : funtions_about.php 		 			      	         			  # ||
||  				Copyright © 2008 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (!defined('IN_vnT')) {
    die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/about');
/*-------------- loadSetting --------------------*/
function loadSetting($lang = "vn")
{
    global $vnT, $func, $DB, $conf;
    $setting = array();
    $result = $DB->query("select * from about_setting WHERE lang='" . $lang . "' ");
    $setting = $DB->fetch_row($result);
    foreach ($setting as $k => $v) {
        $vnT->setting[$k] = stripslashes($v);
    }
    unset($setting);
}

//======================= List_Search =======================
function List_Search($did)
{
    global $func, $DB, $conf, $vnT;
    $arr_search = array(
        'hid' => "ID", 'title' => $vnT->lang['title'], 'date_post' => $vnT->lang['date_post']
    );
    $text = vnT_HTML::selectbox("search", $arr_search, $did);
    return $text;
}

/*** Ham Get_Cat ****/
function Get_Cat($did = -1, $ext = "", $lang = "vn")
{
    global $func, $DB, $conf;
    $text = "<select size=1 id=\"parentid\" name=\"parentid\" {$ext} >";
    $text .= "<option value=\"\">-- Root --</option>";
    $query = $DB->query("SELECT * FROM about n,about_desc nd
					WHERE n.aid=nd.aid AND nd.lang='$lang'
					AND is_focus=1
					ORDER BY display_order ASC, date_post ASC");
    while ($cat = $DB->fetch_row($query)) {
        $title = $func->HTML($cat['title']);
        if ($cat['aid'] == $did) $text .= "<option value=\"{$cat['aid']}\" selected>{$title}</option>";
        else
            $text .= "<option value=\"{$cat['aid']}\" >{$title}</option>";
        $n = 1;
        $text .= Get_Sub($did, $cat['aid'], $n, $lang);
    }
    $text .= "</select>";
    return $text;
}

/*** Ham Get_Sub   */
function Get_Sub($did, $cid, $n, $lang)
{
    global $func, $DB, $conf;
    //	print "SELECT * FROM about WHERE parentid={$cid} and lang='$lang' order by a_order<br>";
    $output = "";
    $k = $n;
    $query = $DB->query("SELECT * FROM about n,about_desc nd
					WHERE n.aid=nd.aid AND nd.lang='$lang'
					AND parentid=$cid
					$where 
					ORDER BY display_order ASC, date_post ASC");
    while ($cat = $DB->fetch_row($query)) {
        $title = $func->HTML($cat['title']);
        if ($cat['aid'] == $did) {
            $output .= "<option value=\"{$cat['aid']}\" selected>";
            for ($i = 0; $i < $k; $i++)
                $output .= "|-- ";
            $output .= "{$title}</option>";
        } else {
            $output .= "<option value=\"{$cat['aid']}\" >";
            for ($i = 0; $i < $k; $i++)
                $output .= "|-- ";
            $output .= "{$title}</option>";
        }
        $n = $k + 1;
        $output .= Get_Sub($did, $cat['aid'], $n, $lang);
    }
    return $output;
}

/***** Ham List_SubCat *****/
function List_SubCat($cat_id)
{
    global $func, $DB, $conf;
    $output = "";
    $query = $DB->query("SELECT * FROM about WHERE parentid={$cat_id}");
    while ($cat = $DB->fetch_row($query)) {
        $output .= $cat["aid"] . ",";
        $output .= List_SubCat($cat['aid']);
    }
    return $output;
}

function is_wp_error($thing)
{
    if (is_object($thing) && is_a($thing, 'WP_Error'))
        return true;
    return false;
}

function absint($maybeint)
{
    return abs(intval($maybeint));
}

function getAboutTitle ($id, $lang) {
    global $func, $DB, $conf, $vnT;

    $sql = "SELECT * FROM about_desc WHERE aid=$id AND lang='$lang'";
    $res = $DB->query($sql);
    $row = $DB->fetch_row($res);
    $name = $row["title"];

    return $name;
}

?>