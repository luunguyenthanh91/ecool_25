/* Keypress, Click Handle */
var KEYPR = {
	isCtrl : false,
	isShift : false,
	shiftOffset : 0,
	allowKey : [ 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123 ],
	isSelectable: false,
	isFileSelectable: false,
	init : function(){
		$('body').keyup(function(e){
			if( ! $(e.target).is('.dynamic') && $.inArray( e.keyCode, KEYPR.allowKey ) == -1 ){
				e.preventDefault();
			}else{
				return;
			}

			// Ctrl key unpress
			if( e.keyCode == 17 ){
				KEYPR.isCtrl = false;
			}else if( e.keyCode == 16 ){
				KEYPR.isShift = false;
			}
		});

		$('body').keydown(function(e){
			if( ! $(e.target).is('.dynamic') && $.inArray( e.keyCode, KEYPR.allowKey ) == -1 ){
				e.preventDefault();
			}else{
				return;
			}
			// Ctrl key press
			if( e.keyCode == 17 /* Ctrl */ ){
				KEYPR.isCtrl = true;
			}else if( e.keyCode == 27 /* ESC */ ){
				// Unselect all file
				$(".imgsel").removeClass("imgsel");
        vnTGallery.setSelFile();

				KEYPR.shiftOffset = 0;
			}else if( e.keyCode == 65 /* A */ && e.ctrlKey === true ){
				// Select all file
				$(".imgcontent").addClass("imgsel");
				vnTGallery.setSelFile();

			}else if( e.keyCode == 16 /* Shift */ ){
				KEYPR.isShift = true;
			}

		});

		// Unselect file when click on wrap area
		$('#imglist').click(function(e){
			if( KEYPR.isSelectable == false ){
				if( $(e.target).is('#imglist') ){
					$(".imgsel").removeClass("imgsel");
				}
			}

			KEYPR.isSelectable = false;
		});
	}
};

function randomNum(a) {
  for (var b = "", d = 0; d < a; d++) {
    b += "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".charAt(Math.floor(Math.random() * 62));
  }
  return b
}

// Xu ly keo tha chuot chon file
function fileSelecting(e, ui){
  //alert('fileSelecting');
  if( e.ctrlKey ){
    alert('fileSelecting ctrlKey');
    if( $(ui.selecting).is('.imgsel') ){
      $(ui.selecting).addClass('imgtempunsel');
    }else{
      $(ui.selecting).addClass('imgtempsel');
    }
  }else if( e.shiftKey ){
    alert('fileSelecting shiftKey');
    $(ui.selecting).addClass('imgtempsel');
  }else{
    $(ui.selecting).removeClass('imgtempunsel').addClass('imgtempsel');
    $('#imglist .imgcontent:not(.imgtempsel)').addClass('imgtempunsel');
  }
}

// Xu ly khi thoi chon file
function fileUnselect(e, ui){
  $(ui.unselecting).removeClass('imgtempunsel imgtempsel');
}

// Xu ly khi ket thuc chon file
function fileSelectStop(e, ui){
  $('#imglist .ui-selected').removeClass('ui-selected');
  $('.imgtempsel').addClass('imgsel').removeClass('imgtempsel');
  $('.imgtempunsel').removeClass('imgsel imgtempunsel');
  //alert('fileSelectStop');

}


vnTGallery = {
//	--------------------------------- main action -------------------------------- //



  insertValue:function(){
    vnTGallery.setSelFile();
    var obj =  $("input[name=obj]").val();
  	var folder  = $("#foldervalue").attr("title");
    var list_file =  $("input[name=selFile]").val();

    if(list_file)
    {
			arr_file = list_file.split("|");
			var list_img = '';
      for (a in arr_file ) {
      	if(list_img!='') { list_img+='|'; }
        list_img +=  folder+'/'+arr_file[a] ;
      }

      self.parent.vnTRUST.callbackGallery(obj,list_img) ;
    }else{
      alert('Vui lòng chọn 1 hình')	;
    }


  },

	setSelFile:function(){
    $("input[name=selFile]").val('');

    if( $('.imgsel').length ){
      fileName = new Array();
      $.each( $('.imgsel'), function(){
        fileName.push( $(this).attr("title") );
      });
      fileName = fileName.join('|');

      $("input[name=selFile]").val(fileName);
    }
		
	},	
	
	folderClick:function (a) {

		var b = $(a).attr("title");


		if (b != $("span#foldervalue").attr("title")) {
				
			$("span#foldervalue").attr("title", b);
			$("span#view_dir").attr("title", $(a).is(".view_dir") ? "1" : "0");

			$("span.folder").css("color", "");
			$(a).css("color", "red");
			 
			if ($(a).is(".view_dir")) {
	

				$("div#imglist").html('<p style="padding:20px; text-align:center"><img src="' + DIR_IMAGE + '/load_bar.gif"/> please wait...</p>').load("index.php?mod=media&act=remote&do=gallery&path=" + b  + "&type=image"  + "&random=" + randomNum(10))
			} else {
				$("div#imglist").text("")
			}

		}
	},

  menuMouseup : function (a) {
		$(a).attr("title");
		$("span").attr("name", "");
		$(a).attr("name", "current");
	},

  fileMouseup : function ( file, e ){


  	if( KEYPR.isFileSelectable == false ){
    // Set shift offset
    if( e.which != 3 && ! KEYPR.isShift ){
      // Reset shift offset
      KEYPR.shiftOffset = 0;

      $.each( $('.imgcontent'), function(k, v){
        if( v == file ){
          KEYPR.shiftOffset = k;
          return false;
        }
      });
    }

    // e.which: 1: Left Mouse, 2: Center Mouse, 3: Right Mouse
    if( KEYPR.isCtrl ){
      if( $(file).is('.imgsel') && e.which != 3 ){
        $(file).removeClass('imgsel');
      }else{
        $(file).addClass('imgsel');
      }
    }else if( KEYPR.isShift && e.which != 3 ){
      var clickOffset = -1;
      $('.imgcontent').removeClass('imgsel');

      $.each( $('.imgcontent'), function(k, v){
        if( v == file ){
          clickOffset = k;
        }

        if( ( clickOffset == -1 && k >= KEYPR.shiftOffset ) || ( clickOffset != -1 && k <= KEYPR.shiftOffset ) || v == file ){
          if( ! $(v).is('.imgsel') ){
            $(v).addClass('imgsel');
          }
        }
      });
    }else{
      if( e.which != 3 || ( e.which == 3 && ! $(file).is('.imgsel') ) ){
        $('.imgsel').removeClass('imgsel');
        $(file).addClass('imgsel');
      }
    }

    vnTGallery.setSelFile();



  }

		KEYPR.isFileSelectable = false;
	},


	init : function () {
    KEYPR.init();
  }
}; 

