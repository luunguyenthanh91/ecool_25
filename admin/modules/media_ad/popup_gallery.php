<?php
/*================================================================================*\
|| 							Name code : media.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "media";
	var $action = "popup_gallery";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_media.php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module."_ad" . DS . "html" . DS . $this->action.".tpl");
    $this->skin->assign('PATH_ROOT', $conf['rootpath']);
    $this->skin->assign('LANG', $vnT->lang);		
	  $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('ROOT_URI', ROOT_URI);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
	  $this->skin->assign('DIR_STYLE', $vnT->dir_style );		 
    $this->skin->assign('DIR_JS', $conf['rooturl'] . "js");
    $this->skin->assign("admininfo", $vnT->admininfo);
    $this->skin->assign('DIR_IMAGE_MEDIA', DIR_IMAGE_MEDIA);

    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action  ;
		switch ($_GET['stype'])
		{
			default : $data['main'] =   $this->do_PopupGallery(); break ;
		}    
		
    $this->skin->assign('data', $data);		
    $this->skin->parse("html_popup");
    $output =  $this->skin->text("html_popup");
		
    flush();
    echo $output;
    exit();
  }

  //============ do_PopupGallery
  function do_PopupGallery ()
  {
    global $vnT, $func, $DB, $conf;
    $err ='';
    $obj_return = $_GET['obj'];
    $module = $_GET['module'] ;
    $folder = ($_GET['folder']== $_GET['module']."/") ? $_GET['module'] : $_GET['folder'] ;
    $type = ($_GET['type']) ? $_GET['type'] : "file";

    $data['path']	 = ($module) ? $module : "File";
    $data['folder']	 = $folder;
    $data['path_img'] = $folder ;


    $data['module'] = $module;
    $data['type'] = $type ;
    $data['max_upload'] = ini_get('upload_max_filesize');
    $data['err'] = $err;
    $data['obj_return'] = $obj_return;
    $data['show_pic'] = (int)$_GET['show_pic'];



    $data['link_action'] = $this->linkUrl ;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_gallery");
    return $this->skin->text("html_gallery");
  }


   // end class
}
?>