<?php
/*================================================================================*\
|| 							Name code : mail_list.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "mail_list";
  var $action = "mail_list";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $this->skin->assign('DIR_JS', $conf['rooturl'] . "admin/js");
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang["add_send_mail"];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'send_mail':
        $nd['f_title'] = $vnT->lang["f_send_mail"];
        $nd['content'] = $this->do_Send_Mail($lang);
      break;
      case 'send_page':
        $nd['f_title'] = $vnT->lang["f_send_page"];
        $nd['content'] = $this->do_Send_Page($lang);
      break;
      case 'send_all':
        $nd['f_title'] = $vnT->lang["f_send_all"];
        $nd['content'] = $this->do_Send_All($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_mail_list'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $text_email = trim($_POST['text_email']);
      $arr_dong = explode("\n", $text_email);
      foreach ($arr_dong as $k => $v) {
        $arr_email = explode(",", trim($v));
        foreach ($arr_email as $key => $value) {
          $email = trim($value);
          if ($email) {
            //check existed
            $res_ck = $DB->query("select email from listmail where email='$email'");
            if (! $DB->num_rows($res_ck)) {
              $cot['cat_id'] = $_POST['cat_id'];
              $cot['email'] = $email;
              $cot['datesubmit'] = time();
              $ok = $DB->do_insert("listmail", $cot);
            }
          }
        }
      }
      //insert adminlog
      $func->insertlog("Add", $_GET['act'], "");
      $mess = $vnT->lang['add_success'];
      $url = $this->linkUrl . "&sub=add";
      $func->html_redirect($url, $mess);
    }
    $data['list_cat'] = Get_Cat($data['cat_id']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Send_Mail 
   * 
   **/
  function do_Send_Mail ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $result = $DB->query("select * from listmail where id=$id ");
    if ($row = $DB->fetch_row($result)) {
      $email = $row["email"];
    }
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $message = stripslashes ($_POST['content']);
		  $message = str_replace(ROOT_URI."vnt_upload/File",$conf['rooturl']."vnt_upload/File",$message);
			
      $link_del = $conf['rooturl'] . "del_mailist.php?email=" . $email;
      $message .= "<br><hr>";
      $message .= "Nếu bạn không muốn nhận thông tin mới từ chúng tôi vui lòng click vào link bên dưới <br>Link hủy newsletter : <a href='" . $link_del . "'>" . $link_del . "</a>";
      $sent = $func->doSendMail($email, $vnT->input['subject'], $message, $conf['email']);
      //insert adminlog
      $func->insertlog("Send mail", $_GET['act'], $id);
      //insert adminlog
      $func->insertlog("Edit", $_GET['act'], $id);
      $err = "Đã gửi email tới $email ";
      $url = $this->linkUrl . "&sub=send_mail&id=$id";
      $func->html_redirect($url, $err);
    }
    $data['send_for'] = "Send mail to : " . $email;
    $data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Default");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=send_mail&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("send_mail");
    return $this->skin->text("send_mail");
  }

  /**
   * function do_Send_Page 
   * 
   **/
  function do_Send_Page ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $text_id = $vnT->input['mem_id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
     	
			//die($message);
      $count = 0;
      $result = $DB->query("select * from listmail where id in (" . $text_id . ") ");
      $arr_email = array();
      while ($row = $DB->fetch_row($result)) {
        $email = $row['email'];
        $link_del = $conf['rooturl'] . "del_mailist.php?email=" . $email;
				$message = stripslashes ($_POST['content']);
			  $message = str_replace(ROOT_URI."vnt_upload/File",$conf['rooturl']."vnt_upload/File",$message);
        $message .= "<br><hr>";
        $message .= "Nếu bạn không muốn nhận thông tin mới từ chúng tôi vui lòng click vào link bên dưới <br>Link hủy newsletter : <a href='" . $link_del . "'>" . $link_del . "</a>";
        $sent = $func->doSendMail($email, $vnT->input['subject'], $message, $conf['email']);
        $count = $count + 1;
      }
      //insert adminlog
      $func->insertlog("Send mail", $_GET['act'], $text_id);
      //insert adminlog
      $func->insertlog("Edit", $_GET['act'], $id);
      $err = "<p>Có <b>$count</b> khách hàng nhận được email này !!! </p>";
      $url = $this->linkUrl . "&sub=send_mail&id=$id";
      $func->html_redirect($url, $err);
    }
    $result = $DB->query("select id from listmail where id in (" . $text_id . ") ");
    $total = $DB->num_rows($result);
    $data['send_for'] = str_replace("{totals}", $total, $vnT->lang['send_for']);
    $data["html_content"] = $vnT->editor->doDisplay('content', $_POST['content'], '100%', '500', "Default");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=send_page&mem_id=" . $text_id;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("send_mail");
    return $this->skin->text("send_mail");
  }

  /**
   * function do_Send_All 
   * 
   **/
  function do_Send_All ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $where = "";
    $cat_id = (int) $vnT->input['cat_id'];
    if ($cat_id) {
      $where = " WHERE cat_id=$cat_id";
    }
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      
      $count = 0;
      $result = $DB->query("select * from listmail $where ");
      $arr_email = array();
      while ($row = $DB->fetch_row($result)) {
        $email = $row['email'];
        $link_del = $conf['rooturl'] . "del_mailist.php?email=" . $email;
				$message = stripslashes ($_POST['content']);
			  $message = str_replace(ROOT_URI."vnt_upload/File",$conf['rooturl']."vnt_upload/File",$message);
        $message .= "<br><hr>";
        $message .= "Nếu bạn không muốn nhận thông tin mới từ chúng tôi vui lòng click vào link bên dưới <br>Link hủy newsletter : <a href='" . $link_del . "'>" . $link_del . "</a>";
        $sent = $func->doSendMail($email, $_POST['subject'], $message, $conf['email']);
        $count = $count + 1;
      }
      //insert adminlog
      $func->insertlog("Send mail", $_GET['act'], $text_id);
      //insert adminlog
      $func->insertlog("Edit", $_GET['act'], $id);
      $err = "<p>Có <b>$count</b> khách hàng nhận được email này !!! </p>";
      $url = $this->linkUrl . "&sub=send_mail&id=$id";
      $func->html_redirect($url, $err);
    }
		
    $result = $DB->query("select id from listmail $where ");
    $total = $DB->num_rows($result);
    if ($cat_id) {
      $data['send_for'] = "Gửi email  cho <font color=red>$total</font> khách hàng thuộc nhóm <font color=red>" . get_cat_name($cat_id) . "</font>";
    } else {
      $data['send_for'] = str_replace("{totals}", $total, $vnT->lang['send_for']);
    }
    $data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Default");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=send_all&cat_id=" . $cat_id;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("send_mail");
    return $this->skin->text("send_mail");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM listmail WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_send = $this->linkUrl . "&sub=send_mail&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['email'] = "<a href=\"{$link_send}\" ><strong>" . $func->HTML($row['email']) . "</strong></a>";
    $output['cat_name'] = get_cat_name($row['cat_id']);
		$output['name'] = ($row['name']) ? $row['name'] : "Khach hang ".$id;
    $output['datesubmit'] = date("H:i, d/m/Y", $row['datesubmit']);
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_send . '"><img src="' . $vnT->dir_images . '/send_mail.gif"  alt="Send "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addScript("modules/mail_list_ad/js/mail_list.js");
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
    $cat_id = (int) $vnT->input['cat_id'];
    if ($cat_id) {
      $where = " AND cat_id=$cat_id";
			$ext_page.="cat_id=$cat_id|";
			$ext.="&cat_id={$cat_id}";
    }
		if(!empty($keyword)){
			$where .=" AND email like '%$keyword%' ";	
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT id FROM listmail WHERE id<>0 $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . $ex;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" ,       
			'email' => "Email |35%|left" , 
			'name' => "Name |20%|left" , 
      'cat_name' => $vnT->lang['group'] . "|20%|left" , 
      'datesubmit' => $vnT->lang['date_submit'] . "|15%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM listmail WHERE id<>0 $where  ORDER BY  id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_mail_list'] . "</div>";
    }
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['send_email_chose'] . ' " class="button" onclick="do_send_maillist()">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['list_cat'] = Get_Cat($cat_id, "onChange='submit();'");
    if ($cat_id) {
      $cat_name = get_cat_name($cat_id);
      $data['download'] = '<a href="modules/mail_list_ad/_download.php?cat_id=' . $cat_id . '"><strong>Download Email của nhóm ' . $cat_name . '</strong></a>';
      $data['send_all'] = '<a href="' . $this->linkUrl . '&sub=send_all&cat_id=' . $cat_id . '"><img src="' . $vnT->dir_images . '/email.gif" align="absmiddle"><b> Gửi email cho nhóm ' . $cat_name . '</b></a>';
    } else {
      $data['download'] = '<a href="modules/mail_list_ad/_download.php"><strong>Download Email List</strong></a>';
      $data['send_all'] = '<a href="' . $this->linkUrl . '&sub=send_all"><img src="' . $vnT->dir_images . '/email.gif" align="absmiddle"><b>' . $vnT->lang['send_email_all'] . '</b></a>';
    }
    $data['totals'] = $totals;
    $data['err'] = $err;
		$data['keyword'] = $keyword;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>