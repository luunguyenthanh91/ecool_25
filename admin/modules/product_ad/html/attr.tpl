<!-- BEGIN: edit -->
<script language=javascript>
  $(document).ready(function() {
  	$('#myForm').validate({
  		rules: { 
  			title: {
  				required: true 
  			}
      },
      messages: {
  			title: {
  				required: "{LANG.err_text_required}" 
  			} 
  		}
  	});
  });
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<tr class="form-required">
      <td class="row1" width="20%">{LANG.title}: </td>
      <td class="row0">
        <input name="title" id="title" type="text" size="50" maxlength="250" value="{data.title}">
      </td>
    </tr>
    <!-- <tr >
      <td class="row1" width="20%">{LANG.picture}: </td>
      <td class="row0">
       	<div id="ext_picture" class="picture" >{data.pic}</div>
        <input type="hidden" name="picture" id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload}>
          <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
      </td>
    </tr>
    <tr>
      <td class="row1">{LANG.description}: </td>
      <td class="row0"><textarea name="description" id="description" rows="5" cols="50" style="width:90%" class="textarea">{data.description}</textarea></td>
    </tr> -->
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
    <td width="30%" align="left"><strong>{LANG.totals} :</strong> &nbsp; <b class="font_err">{data.totals}</b></td>
    <td  align="right"><strong>{LANG.search} :</strong> <input name="keyword" value="{data.keyword}" size="20" type="text" /> &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->