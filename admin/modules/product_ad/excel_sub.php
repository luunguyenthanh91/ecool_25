<?php
//@ini_set("display_errors", "0");
session_start();
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);

require_once("../../../_config.php");
include($conf['rootpath'] . "includes/class_db.php");
$DB = new DB;
//Functions
include($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

if (empty($_SESSION['admin_session'])) {
  die("Ban khong co quyen truy cap trang nay");
}
require_once($conf['rootpath'].'libraries/excel/PHPExcel.php');

$lang = ($_GET['lang']) ? $_GET['lang'] : "vn";

$res_s= $DB->query("SELECT * FROM product_status n , product_status_desc nd
                    WHERE n.status_id=nd.status_id AND lang='$lang' ");
while ($row_s = $DB->fetch_row($res_s)) {
  $vnT->setting['product_status'][$row_s['status_id']] = $row_s;
}

$res_cat = $DB->query("SELECT n.cat_id, nd.cat_name 
                      FROM product_category n , product_category_desc nd 
                      WHERE n.cat_id=nd.cat_id AND lang='$lang' ");
while ($row_cat = $DB->fetch_row($res_cat)) {
  $vnT->setting['arr_category'][$row_cat['cat_id']] = $row_cat['cat_name'];
}
$file_name = 'productSub_' . time();
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
  ->setLastModifiedBy("Maarten Balliauw")
  ->setTitle("Office 2007 XLSX Test Document")
  ->setSubject("Office 2007 XLSX Test Document")
  ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
  ->setKeywords("office 2007 openxml php")
  ->setCategory("Test result file");

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('DANH SACH SAN PHAM');


$cat_id = ((int)$_GET['cat_id']) ? $_GET['cat_id'] : 0;
$brand_id = ((int)$_GET['brand_id']) ? $_GET['brand_id'] : 0;
$status_id = ((int)$_GET['status_id']) ? $_GET['status_id'] : 0;

$search = ($_GET['search']) ? $_GET['search'] : "p_id";
$keyword = ($_GET['keyword']) ? $_GET['keyword'] : "";
$direction = ($_GET['direction']) ? $_GET['direction'] : "DESC";
$adminid = ((int)$_GET['adminid']) ? $_GET['adminid'] : 0;
$date_begin = ($_GET['date_begin']) ? $_GET['date_begin'] : "";
$date_end = ($_GET['date_end']) ? $_GET['date_end'] : "";

$where = " ";

if (!empty($cat_id)) {
  $where .= "  and FIND_IN_SET('$cat_id',cat_list)<>0  ";
}

if ($status_id) {
  $where .= " and FIND_IN_SET('$status_id',status)<>0 ";
}
if ($date_begin || $date_end) {
  $tmp1 = @explode("/", $date_begin);
  $time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

  $tmp2 = @explode("/", $date_end);
  $time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

  $where .= " AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
}

if (!empty($keyword)) {
  switch ($search) {
    case "p_id" :
      $where .= " and  p.p_id = $keyword ";
      break;
    case "date_post" :
      $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
      break;
    default :
      $where .= " and $search like '%$keyword%' ";
      break;
  }
}
$sortField = ($_GET['sortField']) ? $_GET['sortField'] : "date_post";
$sortOrder = ($_GET['sortOrder']) ? $_GET['sortOrder'] : "desc";
$OrderBy = " ORDER BY $sortField $sortOrder , date_post DESC ";

/*$objPHPExcel->getActiveSheet()->mergeCells('A1:G1')  ;
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'DANH SÁCH SẢN PHẨM')	;
$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
*/

$sql = "SELECT p.p_id, p.cat_id, p.price, p.maso ,p.picture, p.status ,p.date_post , pd.p_name
				FROM products p, products_desc pd 
				WHERE p.p_id=pd.p_id AND display<> -1 AND pd.lang='$lang' AND is_parent=1 $where  $OrderBy ";
$reuslt = $DB->query($sql);
if ($num = $DB->num_rows($reuslt)) {
  // Add some data
  $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Mã SP Cha')
    ->setCellValue('B1', 'Mã SP')
    ->setCellValue('C1', 'Tên SP con')
    ->setCellValue('D1', 'Giá')
    ->setCellValue('E1', 'Danh mục');
  $dong = 2;
  $stt = 0;
  while ($row = $DB->fetch_row($reuslt)) {
    $p_id = $row['p_id'];
    $masocha = $row['maso'];
    $rs = $DB->query("SELECT * FROM products_sub p, products_sub_desc pd
                      WHERE p.subid =pd.subid AND display <> -1 AND parentid=$p_id 
                      ORDER BY sub_order ASC, p.subid DESC ");
    if($num_sub = $DB->num_rows($rs)){
      while ($row_sub = $DB->fetch_row($rs)) {
        $stt++;
        $maso = $row_sub['maso'];
        $pic = "\n\n\n\n";
        $p_name = $func->HTML($row_sub['title']);
        $price = $row_sub['price'];
        $cat_name = $vnT->setting['arr_category'][(int)$row_sub['cat_id']];
        //$text_status = $vnT->setting['product_status'][$row['status']]['title'];
        //$date_post = @date("d/m/Y", $row['date_post']);
        $objPHPExcel->setActiveSheetIndex(0)
          ->setCellValue('A' . $dong, $masocha)
          ->setCellValue('B' . $dong, $maso)
          ->setCellValue('C' . $dong, $p_name)
          ->setCellValue('D' . $dong, $price)
          ->setCellValue('E' . $dong, $cat_name);
        $objPHPExcel->getActiveSheet()->getStyle('A' . $dong . ':E' . $dong)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $dong++;
      }
    }
  }
}


$objPHPExcel->getActiveSheet()->getStyle('E2:E' . $dong)->getNumberFormat()->setFormatCode('0');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);



$tableStyle = new PHPExcel_Style();
$tableStyle->applyFromArray(
  array('fill' 	=> array(
    'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
    'color'		=> array('argb' => 'ffffffff')
  ),
    'borders' => array(
      'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
      'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
    )
  ));


$objPHPExcel->getActiveSheet()->setSharedStyle($tableStyle, "A1:E".($dong-1));

$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setARGB('FF808080');
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);

$DB->close();

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');

exit;


?>