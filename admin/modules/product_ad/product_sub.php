<?php
/*================================================================================*\
|| 							Name code : product.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')){
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "product";
	var $action = "product_sub";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    loadSetting();
		$this->skin = new XiTemplate(DIR_MODULE.DS.$this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&p_id=".$vnT->input['p_id']."&lang=" . $lang;
		$this->p_id = $vnT->input['p_id'];
		$vnT->html->addScript("modules/" . $this->module."_ad/js/".$this->module.".js");
		$vnT->html->addStyleSheet( "modules/product_ad/css/selectbox.css");
		$vnT->html->addScript("modules/product_ad/js/selecbox.js");
    switch ($vnT->input['sub']){
      case 'add':
        $nd['f_title'] = $vnT->lang['add_product'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_product'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_product'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $vnT->input['p_id'];
    //$nd['menu'] = $func->getToolbar($this->module,$this->action, $lang, "&p_id=".$vnT->input['p_id']);
    $nd['menu'] = $this->New_Toolbar($this->module, $this->action, $lang,"&p_id=".$vnT->input['p_id']);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action."&p_id=".$vnT->input['p_id'], $lang);		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");  
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;		
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750 ;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);		
    $p_id = $vnT->input['p_id'];
    $err = "";
    if ($vnT->input['do_submit'] == 1){
			$data = $_POST;
      $cat_id = (int) $vnT->input['cat_id'];
 			$maso = trim($vnT->input['maso']);
			$price = str_replace(array(",","."),"",trim($_POST['price']));
      $price_old = str_replace(array(",","."),"",trim($_POST['price_old']));
      $picture = $vnT->input['picture'];
			if ($maso){
				$res_ck = $DB->query("SELECT subid,maso FROM products_sub WHERE maso='$maso' ");
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
      if (empty($err)){
        $cot['cat_id'] = $cat_id;
        $cot['cat_list'] = $this->get_cat_list($cat_id);
				$cot['maso'] = $maso;
				$cot['parentid'] = $p_id;
				$cot['price'] =  $price;
        $cot['price_old'] = $price_old;
        $cot['discount'] = $vnT->input['discount'];
				$cot['picture'] = $picture;
        $cot['onhand'] = (int) $vnT->input['onhand'];
				$cot['date_post'] = time();
				$cot['date_update'] = time();
				$cot['adminid'] = $vnT->admininfo['adminid'];
        $ok = $DB->do_insert("products_sub", $cot);
        if ($ok){
          $subid = $DB->insertid();
          $cot_d['subid'] = $subid;
          $cot_d['title'] = trim($vnT->input['title']);
          $cot_d['color'] = trim($vnT->input['color']);
          $cot_d['param'] = trim($vnT->input['param']);
          $cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $query_lang = $DB->query("SELECT name FROM language ");
          while ( $row = $DB->fetch_row($query_lang) ) {
            $cot_d['lang'] = $row['name'];
            $DB->do_insert("products_sub_desc",$cot_d);
          }
          if(empty($maso)){
            $dup['maso'] = $subid;
          }
          $DB->do_update("products_sub", $dup, "subid=$subid");
          $func->clear_cache();
          $func->insertlog("Add", $_GET['act'], $p_id);					
          $mess = $vnT->lang['add_success'];
					$url = $this->linkUrl . "&sub=add&p_id=".$vnT->input['p_id'];						
          $func->html_redirect($url, $mess);
        }else{
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    }
    $data['discount'] = ($vnT->input['discount']) ? $vnT->input['discount'] : 0;
    $data['list_cat'] = $this->Get_Cat($vnT->input['cat_id'],$lang);
		$info_product = $this->GetInfo_Product($p_id, $lang);
		$data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '250', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl ."&sub=add";
    $this->skin->assign('data', $data);    
    $this->skin->parse("edit");
    return $this->skin->text("edit");  
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;		
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750 ;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    $id = (int) $vnT->input['id'];
		$p_id = $vnT->input['p_id'];
    if ($vnT->input['do_submit']){
      $data = $_POST;
      $cat_id = (int) $vnT->input['cat_id'];
      $maso = trim($vnT->input['maso']);
			$price = str_replace(array(",","."),"",trim($_POST['price']));
      $price_old = str_replace(array(",","."),"",trim($_POST['price_old']));
			$picture = $vnT->input['picture'];
			if ($maso){
				$res_ck = $DB->query("SELECT subid,maso FROM products_sub 
                              WHERE maso='$maso' AND lang='$lang' AND subid<>$id ");
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
      if (empty($err)){
        $cot['maso'] = $maso;			
				$cot['cat_id'] = $cat_id;
        $cot['cat_list'] = $this->get_cat_list($cat_id);
				$cot['price'] =  $price;
        $cot['price_old'] =  $price_old;
        $cot['discount'] = $vnT->input['discount'];
        $cot['onhand'] = (int) $vnT->input['onhand'];
				$cot['picture'] = $picture;
				$cot['date_update'] = time();
        $ok = $DB->do_update("products_sub", $cot, "subid=$id");
        if ($ok){
          //$cot_d['subid'] = $subid;
          $cot_d['title'] = trim($vnT->input['title']);
          $cot_d['color'] = trim($vnT->input['color']);
          $cot_d['param'] = trim($vnT->input['param']);
          $cot_d['description'] = $DB->mySQLSafe($_POST['description']);
          $DB->do_update("products_sub_desc",$cot_d," subid={$id} AND lang='{$lang}'");
          $func->clear_cache();
          $func->insertlog("Edit", $_GET['act'], $id);          
          $err = $vnT->lang["edit_success"];
					$url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }    
    }    
    $query= $DB->query("SELECT * FROM products_sub p, products_sub_desc pd
            						WHERE p.subid = pd.subid AND lang='$lang' AND p.subid = $id ");
    if ($data = $DB->fetch_row($query)){
  	  if ($data['picture']) {
        $data['pic'] = $this->get_picture($data['picture'],$w_thumb)." <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
  		  $data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }
    }else{
      $mess = $vnT->lang['not_found']." ID : ".$id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['list_cat'] = $this->Get_Cat($data['cat_id'],$lang);
 		$data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '250', "Default",$this->module,$dir); 
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $this->skin->assign('data', $data);    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}
		$ok = $DB->query('DELETE FROM products_sub WHERE subid IN('.$ids.')');
    $ok = $DB->query('DELETE FROM products_sub_desc WHERE subid IN('.$ids.')');
    if ($ok){
      $mess = $vnT->lang["del_success"];
      $func->clear_cache();
    } else{
      $mess = $vnT->lang["del_failt"];
    }
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang)  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['subid'];
    $p_id = $vnT->input['p_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=".$id."')";
    $output['price'] = "<input name=\"txtPrice[{$id}]\" type=\"text\" style=\"text-align:center\" size=\"10\"  value=\"{$row['price']}\" onchange='javascript:do_check($id)' />";
    $output['onhand'] = "<input name=\"txtOnhand[{$id}]\" type='text' style='text-align:center' size=\"5\" value=\"{$row['onhand']}\" onchange='javascript:do_check($id)' />";   
		if ($row['picture']){
			$output['picture']= $this->get_picture($row['picture'],50);
		}else $output['picture'] ="No image";
  	$output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\"  style=\"text-align:center\" value=\"{$row['sub_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";	
  	$output['maso'] = "<b>#".$row['maso']."</b>";
    $output['color'] = "<b>".$row['color']."</b>";
    $output['param'] = "<b>".$row['param']."</b>";
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images."/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id' title='".$vnT->lang['click_do_display']."'><img src=\"".$vnT->dir_images."/nodispay.gif\" width=15/></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="'.$link_edit.'"><img src="'.$vnT->dir_images.'/edit.gif" alt="Edit "></a>&nbsp;';
    $output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="'.$link_del.'"><img src="'.$vnT->dir_images.'/delete.gif" alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    if ($vnT->input["do_action"]){
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]){
      	case "do_edit":
          $arr_order = (isset($vnT->input["txt_Order"])) ? $vnT->input["txt_Order"] : array();
          $arr_price = (isset($vnT->input["txtPrice"])) ? $vnT->input["txtPrice"] : array();
          $arr_onhand = (isset($vnT->input["txtOnhand"])) ? $vnT->input["txtOnhand"] : array();
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
						$dup['sub_order'] = $arr_order[$h_id[$i]];
            $dup['onhand'] = $arr_onhand[$h_id[$i]];
						$dup['price'] = $arr_price[$h_id[$i]]; 
						$dup['date_update'] = time();
            $ok = $DB->do_update("products_sub", $dup, "subid=".$h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i].", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2)."</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success']." ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("products_sub_desc", $dup, "subid=".$h_id[$i]);
            if ($ok){
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2)."</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_display":
          $mess .= "- ".$vnT->lang['display_success']." ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("products_sub_desc", $dup, "subid=".$h_id[$i]);
            if ($ok){
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
		if((int)$vnT->input["do_display"]){
			$ok = $DB->query("UPDATE products_sub_desc SET display=1 WHERE subid=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- ".$vnT->lang['display_success']." ID: <strong>".$vnT->input["do_display"]."</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]){
			$ok = $DB->query("UPDATE products_sub_desc SET display=0 WHERE subid=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- ".$vnT->lang['display_success']." ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
      $func->clear_cache();
		}
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		$p_id = $vnT->input['p_id'];
    $info_product = $this->GetInfo_Product($p_id, $lang);
    if($info_product['is_parent'] == 1){
   		$ext_page=$ext_page."p=$p";
      $query= $DB->query("SELECT p.subid FROM products_sub p, products_sub_desc pd
                          WHERE p.subid = pd.subid AND parentid=$p_id AND lang ='$lang' $where");
      $totals = intval($DB->num_rows($query));    
      $num_pages = ceil($totals / $n);
      if ($p > $num_pages) $p = $num_pages;
      if ($p < 1) $p = 1;
      $start = ($p - 1) * $n;
      $nav = $func->paginate($totals, $n, $ext, $p);
      $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
  		$ext_link = $ext."&p=$p";
      $table['title'] = array(
  			'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\"/>|5%|center" , 
  			'order' => $vnT->lang['order']." ".$data['SortLink']['p_order']."|5%|center",
  			'picture' => $vnT->lang['picture']." |10%|center",	
  			'maso' =>	"Mã SP|15%|left",
  			'price' =>	"Giá|15%|center",
        'onhand' =>  "Tồn kho|10%|center",
  			'color' =>	"Màu sắc|15%|center",
  			'param' =>	"Thông số|20%|center",
  			'action' => "Action|15%|center"
  		);
      $sql = "SELECT * FROM products_sub p, products_sub_desc pd
        			WHERE p.subid = pd.subid AND parentid=$p_id AND lang ='$lang' $where 
              ORDER BY sub_order ASC,date_update DESC, date_post DESC LIMIT $start,$n";
      $reuslt = $DB->query($sql);
      if ($DB->num_rows($reuslt)){
        $row = $DB->get_array($result);
        for ($i = 0; $i < count($row); $i ++){
  				$row[$i]['ext_link'] = $ext_link ;
  				$row[$i]['ext_page'] = $ext_page;
          $row_info = $this->render_row($row[$i],$lang);
          $row_field[$i] = $row_info;
          $row_field[$i]['stt'] = ($i + 1);
          $row_field[$i]['row_id'] = "row_" . $row[$i]['p_id'];
          $row_field[$i]['ext'] = "";
        }
        $table['row'] = $row_field;
      } else {
        $table['row'] = array();
        $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_product'] ."</div>";
      }
  		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
  		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
  		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
  		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
      $table_list = $func->ShowTable($table);
  		if ($info_product['picture']){
  			$data['pic'] = $this->get_picture($info_product['picture'],120);
      } else 
  			$data['pic'] ="No image";
  		$data['maso'] = $info_product['maso'];
  		$data['p_name'] = $info_product['p_name'];
  		$data['p_id'] = $p_id;
      $data['table_list'] = $table_list;
      $data['totals'] = $totals;
      $data['err'] = $err;
      $data['nav'] = $nav;
      $this->skin->assign('data', $data);
      $this->skin->parse("manage");
      return $this->skin->text("manage");
    }else{
      $mess = 'Sản phẩm này không phải sản phẩm đại diện';
      $url = "?mod=product&act=product&lang=$lang";
      $func->html_redirect($url, $mess);
    }
  }
}
$vntModule = new vntModule();
?>
