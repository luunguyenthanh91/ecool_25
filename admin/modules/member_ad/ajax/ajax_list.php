<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);
 
 
	switch ($_GET['do'])
	{
		case "member" : $aResults = get_member() ; break;
		case "company" : $aResults = get_company() ; break;
		
		default  : $aResults = get_all() ; break;
	}
 	
	//get_all
	function get_all() {
		global $DB,$func,$conf,$vnT;
		$textout = array();
		$lang = ($_GET['lang']) ? $_GET['lang'] : "vn" ;
		$keyword = $_GET['input'];		
		
 		$sql = "SELECT mem_id,username,full_name , email
						FROM members 
						WHERE mem_id>0 AND (username like '%".$keyword."%' OR full_name like '%".$keyword."%')
						ORDER BY username ASC, full_name ASC, mem_id DESC "	;
		//echo 	"sql = ".$sql;			
		$res = $DB->query($sql);
		if($num = $DB->num_rows($res))
		{
			while	($row = $DB->fetch_row($res))
			{
				$textout[] = array(
													'id' =>  $row['mem_id'],
													'value' =>  $row['full_name'],
													'info' =>  " [".$row['email']."]",
												 );
			}
		}
		 
		return $textout;
	}
	

 	//get_member
	function get_member() {
		global $DB,$func,$conf,$vnT;
		$textout = array();
		$lang = ($_GET['lang']) ? $_GET['lang'] : "vn" ;
		$keyword = $_GET['input'];		
		
 		$sql = "SELECT mem_id,username,full_name , email
						FROM members 
						WHERE full_name like '%".$keyword."%'
						ORDER BY full_name ASC, mem_id DESC "	;
						
		$res = $DB->query($sql);
		if($num = $DB->num_rows($res))
		{
			while	($row = $DB->fetch_row($res))
			{
				$textout[] = array(
													'id' =>  $row['mem_id'],
													'value' =>  $row['full_name'],
													'info' =>  " [".$row['email']."]",
												 );
			}
		}
		 
		return $textout;
	} 
	
	//get_company
	function get_company() {
		global $DB,$func,$conf,$vnT;
		$textout = array();
		$lang = ($_GET['lang']) ? $_GET['lang'] : "vn" ;
		$keyword = $_GET['input'];		
		
 		$sql = "SELECT mem_id,username,full_name
						FROM members
						WHERE mem_type=2
						AND username like '%".$keyword."%'
						ORDER BY username ASC, mem_id DESC "	;
						
		$res = $DB->query($sql);
		if($num = $DB->num_rows($res))
		{
			while	($row = $DB->fetch_row($res))
			{
				$textout[] = array(
													'id' =>  $row['mem_id'],
													'value' =>  $row['username'],
													'info' =>  " [".$row['full_name']."]",
												 );
			}
		}
		 
		return $textout;
	} 
	
 
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header ("Pragma: no-cache"); // HTTP/1.0



if (isset($_REQUEST['json']))
{
	header("Content-Type: application/json");

	echo "{\"results\": [";
	$arr = array();
	for ($i=0;$i<count($aResults);$i++)
	{
		$arr[] = "{\"id\": \"".$aResults[$i]['id']."\", \"value\": \"".$aResults[$i]['value']."\", \"info\": \"".$aResults[$i]['info']."\"}";
	}
	echo implode(", ", $arr);
	echo "]}";
}
else
{
	header("Content-Type: text/xml");

	echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?><results>";
	for ($i=0;$i<count($aResults);$i++)
	{
		echo "<rs id=\"".$aResults[$i]['id']."\" info=\"".$aResults[$i]['info']."\">".$aResults[$i]['value']."</rs>";
	}
	echo "</results>";
}
$DB->close();
?>
