<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "money";

  /**
   * function vntModule ()
   * Khoi tao
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = "Nập tiền vào tài khoản thành viên";
        $nd['content'] = $this->do_Add($lang);
        break;

      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = 'Quản lý nạp tiền ';
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }


  /**
   * function do_Add
   * Them  moi
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnT->html->addStyleSheet($vnT->dir_js . "/auto_suggest/autosuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/auto_suggest/bsn.AutoSuggest.js");
    $id = (int)$vnT->input['id'];

    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;

      if(empty($err))
      {
        $mem_id = (int)$_POST['mem_id'];
        $price = $_POST['price'];
        $order_code = 'DH-'.date('His-dmY');

        $cot['type'] = "bqt";
        $cot['mem_id'] = $mem_id ;
        $cot['order_code'] = $order_code;
        $cot['value'] = $price;
        $cot['status'] = 1 ;
        $cot['code'] =  md5(uniqid(microtime()));
        $cot['date_post'] = time();

        $ok =  $vnT->DB->do_insert("member_money", $cot);
        if ($ok) {

          $DB->query("UPDATE members SET mem_point=mem_point+".$price." WHERE mem_id=".$mem_id);
          $cot_h['mem_id'] = $mem_id;
          $cot_h['action'] = 'add';
          $cot_h['value'] = $price;
          $cot_h['reason'] = "Nạp tiền từ BQT ";
          $cot_h['id_reason'] = $mem_id;
          $cot_h['code_reason'] = $mem_id;
          $cot_h['notes'] = "Nạp ".$vnT->func->format_number($price)." đ vào Tài Khoản từ BQT";
          $cot_h['datesubmit'] = time();
          $DB->do_insert("money_history", $cot_h);

          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $mem_id);


          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }

    if($id)
    {
      $res_mem = $vnT->DB->query("SELECT * FROM members WHERE mem_id=".$id)	;
      if($row_mem = $vnT->DB->fetch_row($res_mem))
      {
        $data['username'] = $row_mem['full_name'];
        $data['mem_id'] = $id;
      }
    }

    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("add");
    return $this->skin->text("add");
  }


  /**
   * function do_Task
   *
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }

    $res_ck = $DB->query("SELECT * FROM member_money WHERE id IN (" . $ids . ")");
    while($row_ck = $DB->fetch_row($res_ck))
    {
      if($row_ck['status']==0){
        $del=1;
        $DB->query("DELETE FROM member_money WHERE id=".$row_ck['id'])	;
      }
    }

    if ($del) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    //$link_edit = $this->linkUrl . '&sub=edit&id=' . $func->NDK_encode($id) . '&ext=' . $row['ext_page'];
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_send_email = $this->linkUrl . "&sub=send_email&id=" . $id;

    //$output['member'] = $func->HTML($row['d_name']) ;
    $output['date_post'] = date("H:i, d/m/Y", $row['date_post']);
    $output['code'] = '<b >'.$row['order_code'].'</b>';


    $link_mem = ($row['mem_group']==2) ? "?mod=member&act=company&sub=edit&id=".$row['mem_id']  : "?mod=member&act=member&sub=edit&id=".$row['mem_id'];

    $customer = "Mem ID: <b class='font_err'><a href='".$link_mem."'>" . $func->HTML($row['mem_id'])."</a></b> " ;
    $customer .='&nbsp;<a   title="Lịch sử nạp Tiền" class="thickbox" id="option_search" href="?mod=member&act=money_statistics&sub=money_add&mem_id=' . $row['mem_id'] . '&TB_iframe=true&width=700&height=500" ><img src="' . $vnT->dir_images . '/but_view.gif"  alt="Xem chi tiết " width=22 align="absmiddle"> </a> ';
    $customer .= "<br>Tên : <strong>" . $func->HTML($row['full_name'])."</strong>" ;
    $customer .= "<br>Email : <strong> " . $func->HTML($row['email'])."</strong>" ;
    $customer .= "<br>ĐT : <strong> " . $func->HTML($row['phone'])."</strong>" ;


    $output['customer'] = $customer;
    $output['value'] = "<b class='font_err'>".$func->format_number($row['value'],".")." VNĐ</b>";

    switch ($row['status']) {
      case "0":
        $output['status'] = "<b class=red>Chờ thanh toán</b>";
        break;
      case "1":
        $output['status'] =  "<b class=blue>Đã thanh toán</b>";
        break;
      case "2":
        $output['status'] = "Hủy bỏ";
        break;
    }

    switch ($row['type'])
    {
      case "bqt"	: $output['type'] = "Nạp từ BQT" ; break ;
      default :  $output['type'] = "Nạp qua Ngân Lượng" ; break ;
    }

    if($row['cardpin'])
    {
      $output['info_card'] = "Mã thẻ : ".$row['cardpin']."<br>Serial: ".$row['serial']."<br>Nhà cung cấp: ".$row['provider'] ;
    }else{
      $output['info_card'] = '---';
    }

    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    //$output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/but_view.gif"  alt="View " width=22></a>&nbsp;';
    if($row['status']==1){
      $output['action'] .= '---';
    }else{
      $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    }
    return $output;
  }

  /**
   * function do_Manage()
   * Quan ly
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({ 
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({ 
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		");

    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])   $h_id = $vnT->input["del_id"];
      if ($vnT->input["hmem"])   $hmem = $vnT->input["hmem"];

      switch ($vnT->input["do_action"]) {
        case "do_display":
          $mess .= "- Xét đã thanh toán cho  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 1;
            $ok = $DB->do_update("member_money", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $res_c = $DB->query("SELECT * FROM member_money WHERE id=".$h_id[$i]." ") ;
              if($row_c = $DB->fetch_row($res_c))
              {
                $DB->query("UPDATE members SET mem_point=mem_point+".$row_c['value']." WHERE mem_id=".$row_c['mem_id']." ");

                //inser gold_history
                $cot_h['mem_id'] = $row_c['mem_id'];
                $cot_h['action'] = 'add';
                $cot_h['value'] = $row_c['value'];
                $cot_h['reason'] = "Nạp tiền vào tài khoản";
                $cot_h['id_reason'] = $h_id[$i];
                $cot_h['code_reason'] = $row_c['order_code'];
                $cot_h['notes'] =  "Nạp ".$func->format_number($row_c['value'])." đ vào Tài Khoản từ thẻ điện thoại";
                $cot_h['datesubmit'] = time();
                $DB->do_insert("money_history", $cot_h);
              }

              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- Xóa đã thanh toán cho  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 0;
            $ok = $DB->do_update("member_money", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $res_c = $DB->query("SELECT * FROM member_money WHERE id=".$h_id[$i]) ;
              if($row_c = $DB->fetch_row($res_c))
              {
                $DB->query("UPDATE members SET mem_point=mem_point-".$row_c['value']." WHERE mem_id=".$row_c['mem_id']." ");
                //del gold_history
                $DB->query("DELETE FROM money_history WHERE mem_id=".$row_c['mem_id']." AND id_reason=".$h_id[$i]."  ");
              }

              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;

    $status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "-1";
    $search = ($vnT->input['search']) ? $vnT->input['search'] : "id";
    $keyword = (trim($vnT->input['keyword'])) ? trim($vnT->input['keyword']) : "";
    $date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
    $date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";

    $where ="";
    if($status!="-1") {
      $where.=" AND status=$status ";
      $ext.="&status=".$status;
      $ext_page.= "status=".$status."|";
    }
    if($date_begin || $date_end )
    {
      $tmp1 = @explode("/", $date_begin);
      $time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);

      $tmp2 = @explode("/", $date_end);
      $time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);

      $where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
      $ext.="&date_begin=".$date_begin."&date_end=".$date_end;
      $ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
    }

    if (! empty($keyword)) {
      if ($search == "date_post") {
        $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";
      } else {
        $where .= " and $search like '%$keyword%' ";
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }

    $where .=" AND id>0 ";

    $query = $DB->query("SELECT  c.id FROM member_money c, members m  WHERE c.mem_id=m.mem_id  $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)   $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . $ext;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" ,
      'date_post' => "Ngày nạp|10%|center" ,
      'code' => "Mã nạp|10%|center" ,
      'customer' => "Thông tin người nạp|30%|left" ,
      'type' => "Hình thức nạp|15%|center" ,
      'value' => "Giá trị nạp|12%|center" ,
      'status' => "Trạng thái|13%|center" ,
      'action' => "Action|5%|center");
    $sql = "SELECT c.* , m.username, m.email,m.full_name,m.phone,m.address FROM member_money c, members m  WHERE c.mem_id=m.mem_id $where ORDER BY  c.date_post DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có nạp tiền nào</div>";
    }

    $table['button'] = '<input type="button" name="btnUpdate" value=" Xác nhận đã thanh toán và cộng tiền vào TK " class="button" onclick="do_submit(\'do_display\')" >';
    $table['button'] .= '<input type="button" name="btnUpdate" value=" Sét chưa thanh toán và trừ tiền TK" class="button" onclick="do_submit(\'do_hidden\')" >';


    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';

    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
    $data['list_search'] =   vnT_HTML::selectbox("search", array(
      'order_code' => 'Mã đơn hàng' ,'username'  => 'Tài khoản người nạp' ,'full_name' => 'Tên người nạp'  , 'email' => 'Email người nạp','phone' => 'Điện thoại người nạp'
    ), $search);

    $data['list_status'] =   vnT_HTML::selectbox("status", array('-1' => '--- Tất cả --' , '0' => 'Chờ thanh toán' , '1' => 'Đã thanh toán'), $status);

    $data['date_begin'] = $date_begin;
    $data['date_end'] = $date_end;

    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>