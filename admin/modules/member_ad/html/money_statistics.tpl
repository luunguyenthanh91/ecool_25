<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
   
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
  
  <tr>
    <td width="15%"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" ><b class="font_err">{data.totals}</b></td>
  </tr>

</table>
</form>
{data.err}
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
 
   <tr>
     <td>
   	
<table cellspacing="1" class="adminlist" id="table_list">
<thead>
<tr height="25"> 
		<th width="5%" align="center" >Mem ID</th>
    <th  align="left" >Thành viên</th>
    <th width="15%" align="center" >Tiền nạp</th>
		<th width="15%" align="center" >Tiền được thưởng</th> 
		<th width="15%" align="center" >Tiền sử dụng</th>
    <th width="15%" align="center" >Tổng số Tiền còn lại</th> 
		</tr>
</thead>

   
<!-- BEGIN: html_row -->
<tr class="{row.class}" id="{row.row_id}"> 
	 <td align="center" >{row.mem_id}</td>
	<td align="left" ><strong>{row.member}</strong></td>
  <td align="center" >{row.text_money_add}</td>
  <td align="center"  >{row.text_money_apply}</td>
  <td align="center" >{row.text_money_use}</td>  
   <td align="center" >{row.text_total_price}</td>
</tr>
<tr id="trQ{row.order_id}" style="display:none">
  <td></td>
  <td colspan="9" id="tdQ{row.order_id}" class="QuickView">&nbsp;</td>
</tr>
<!-- END: html_row -->

<!-- BEGIN: html_row_no -->
<tr class="row0" > 
	<td  colspan="8" align="center" class="font_err" >No have Order</td>
</tr>
<!-- END: html_row_no -->

<tr class="row0" > 
	<td align="right" colspan="2" ><strong>Tổng cộng : </strong></td>
  <td align="center" ><b class="font_err" style="font-size:16px;">{data.total_money_add}</b></td>
  <td align="center"  ><b class="font_err" style="font-size:16px;">{data.total_money_apply}</b></td>
  <td align="center" ><b class="font_err" style="font-size:16px;">{data.total_money_use}</b></td>  
  <td align="center" ><b class="font_err" style="font-size:16px;">{data.total_all}</b></td> 
</tr>
</table>
</td>
   </tr>
    
</table>
<input type="hidden" name="do_action" id="do_action" value="" >
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->


<!-- BEGIN: html_popup -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<!--[if gte IE 6]>
<link rel='stylesheet' href="{DIR_STYLE}/ie.css" type='text/css' media='all' />
<![endif]-->
<link href="{DIR_STYLE}/style_tooltips.css" rel="stylesheet" type="text/css" />
<link rel='stylesheet' href='{DIR_JS}/thickbox/thickbox.css' type='text/css' media='all' />
{EXT_STYLE}
<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
	var lang_js = new Array(); 
		lang_js["please_chose_item"]   	= "{LANG.please_chose_item}'";
		lang_js["are_you_sure_del"]   	= "{LANG.are_you_sure_del}";
</script>
<script language="javascript1.2" src="{DIR_JS}/jquery.js"></script> 
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin.js"></script>

<script language="javascript1.2" src="{DIR_JS}/admin/common.js"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/ajax-response.js"></script>
<script type="text/javascript" src="{DIR_JS}/tooltips.js"></script>
<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js'></script> 
<script id='ext_javascript'></script>
{EXT_HEAD}

</head>
<body style="margin:10px;" > 
<div style="padding:5px; color:#F00; font-size:14px; " > {data.f_title} </div>
{data.err}
<br />
{data.table_list}
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr> 
    <td align="right"><strong>Tổng cộng :</strong></td>
    <td width="130" align="center" nowrap> <b class="font_err" style="font-size:16px;">{data.total_price}</b></td>
  </tr>
</table>

<br />
 
</body>
</html> 
<!-- END: html_popup -->
