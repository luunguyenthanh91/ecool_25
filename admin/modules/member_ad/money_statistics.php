<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "money_statistics";
	var $money_action = "";


  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
		$this->money_action =  array("register" => 'Đăng ký' , "add" => 'Nạp', "receive" => 'Nhận', "use" => 'Dùng' , "send" => 'Chuyển' , "order" => 'Mua hàng', "active_estore" => 'Kích hoạt G.Hàng', "tellfriend" => 'Giới thiệu', "tellfriend" => 'Giới thiệu', "complete_order" => 'Hoàn tất đơn hàng') ;
		
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    switch ($vnT->input['sub']) {
      case 'money_add':
        $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
        $this->skin->assign("DIR_STYLE", $vnT->dir_style);
        $this->skin->assign("DIR_JS", $vnT->dir_js);
        flush();
        echo $this->do_money_add($lang);
        exit();
        break;
				case 'money_apply':
        $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
        $this->skin->assign("DIR_STYLE", $vnT->dir_style);
        $this->skin->assign("DIR_JS", $vnT->dir_js);
        flush();
        echo $this->do_money_apply($lang);
        exit();
        break;
				case 'money_use':
        $this->skin->assign("DIR_IMAGE", $vnT->dir_images);
        $this->skin->assign("DIR_STYLE", $vnT->dir_style);
        $this->skin->assign("DIR_JS", $vnT->dir_js);
        flush();
        echo $this->do_money_use($lang);
        exit();
        break;
				
      default:
        $nd['f_title'] = 'Quản lý lịch sử Tiền ';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
   

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['mem_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    //$link_edit = $this->linkUrl . '&sub=edit&id=' . $func->NDK_encode($id) . '&ext=' . $row['ext_page'];
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
 
    //$output['member'] = $func->HTML($row['d_name']) ;
    $output['date_post'] = date("H:i, d/m/Y", $row['date_post']); 
		
		$link_mem = ($row['mem_group']==2) ? "?mod=member&act=company&sub=edit&id=".$row['mem_id']  : "?mod=member&act=member&sub=edit&id=".$row['mem_id'];
		 
		$output['mem_id'] =  "<b class='font_err'><a href='".$link_mem."'>" . $func->HTML($row['mem_id'])."</a></b>" ;
		
		$member = "<strong>" . $func->HTML($row['full_name'])."</strong>" ;
		$member .= "<br>Email : <strong> " . $func->HTML($row['email'])."</strong>" ;	
		$member .= "<br>ĐT : <strong> " . $func->HTML($row['phone'])."</strong>" ;
		
			
		$output['member'] = $member;
		
		
		$money_add = 0;
		$money_apply = 0;
		$money_use = 0;
		
		$res_c = $DB->query("SELECT * FROM money_history WHERE mem_id=".$id );
		if($num_c= $DB->num_rows($res_c))
		{
			
			while($row_c = $DB->fetch_row($res_c))	
			{
				if($row_c['action']=="add")	{ $money_add+=$row_c['value'] ;}
				if($row_c['action']=="receive")	{ $money_add+=$row_c['value'] ;}
				
				if($row_c['action']=="register")	{ $money_apply+=$row_c['value'] ;}
				if($row_c['action']=="tellfriend")	{ $money_apply+=$row_c['value'] ;}
				if($row_c['action']=="loantin")	{ $money_apply+=$row_c['value'] ;}				
				if($row_c['action']=="complete_order")	{ $money_apply+=$row_c['value'] ;}		
						
				if($row_c['action']=="send")	{ $money_use+=$row_c['value'] ;}				
				if($row_c['action']=="order")	{ $money_use+=$row_c['value'] ;}
				if($row_c['action']=="active_estore")	{ $money_use+=$row_c['value'] ;}
				if($row_c['action']=="use")	{ $money_use+=$row_c['value'] ;}
				
			}
		} 
		
		$output['money_add'] = $money_add ;
		$output['text_money_add']= "<b>".get_format_money($money_add) . '</b> &nbsp;<a   title="Lịch sử nạp Tiền" class="thickbox" id="option_search" href="?mod=member&act=money_statistics&sub=money_add&mem_id=' . $id . '&TB_iframe=true&width=700&height=500" ><img src="' . $vnT->dir_images . '/but_view.gif"  alt="Xem chi tiết " width=22 align="absmiddle"> </a> '; 
		
		$output['money_apply'] = $money_apply ;
		$output['text_money_apply']= "<b>".get_format_money($money_apply) . '</b> &nbsp;<a  title="Lịch sử Tiền thưởng" class="thickbox" id="option_search" href="?mod=member&act=money_statistics&sub=money_apply&mem_id=' . $id . '&TB_iframe=true&width=700&height=500"  ><img src="' . $vnT->dir_images . '/but_view.gif"  alt="Xem chi tiết " width=22 align="absmiddle"> </a> '; 
		
		$output['money_use'] = $money_use ;
		$output['text_money_use']= "<b>".get_format_money($money_use) . '</b> &nbsp;<a   title="Lịch sử sử dụng Tiền" class="thickbox" id="option_search" href="?mod=member&act=money_statistics&sub=money_use&mem_id=' . $id . '&TB_iframe=true&width=700&height=500" ><img src="' . $vnT->dir_images . '/but_view.gif"  alt="Xem chi tiết " width=22 align="absmiddle"> </a> '; 
		
		$output['total_price'] = $row['mem_point'] ;
		
		$output['text_total_price'] =  "<b class='font_err'>".get_format_money($row['mem_point'])."</b>";
    
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/but_view.gif"  alt="View " width=22></a>&nbsp;';
    //$output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		 
		 
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = 100;
     
		$status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "-1";
		$search = ($vnT->input['search']) ? $vnT->input['search'] : "id";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
 		
		$where ="";
		 
		
    if (! empty($keyword)) {
      if ($search == "mem_id") {
        $where .= " and mem_id = '{$keyword}' ";
      } else {
        $where .= " and $search like '%$keyword%' ";
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&search={$search}&keyword={$keyword}";
    }
		
    $query = $DB->query("SELECT  m.mem_id FROM members m WHERE m.mem_id >0 $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)   $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p); 
		
		
		$total_money_add = 0;
		$total_money_apply = 0;
		$total_money_use = 0;
		$total_all = 0;
    $sql = "SELECT * FROM   members m  WHERE   m.mem_id >0 $where ORDER BY  m.date_join DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql); 
    if ($DB->num_rows($reuslt))
    {
      $i = 0;
      while ($row = $DB->fetch_row($reuslt))
      {
        $i ++;
        $row['ext'] = "";
				$row['stt'] = $i;
        $row['ext_page'] = $ext_page;
				$row['ext_link'] = $ext."&p=".$p ;
				
				 
				
				$total_salary += $row['salary'] ;
        $row_info = $this->render_row($row, $lang);
				
				$total_money_add += $row_info['money_add'];
				$total_money_apply += $row_info['money_apply'];
				$total_money_use += $row_info['money_use'];
				$total_all  += $row_info['total_price'];
		
        $row_info['class'] = ($i % 2) ? "row1" : "row0";
        $this->skin->assign('row', $row_info);
        $this->skin->parse("manage.html_row"); 
      }
    } else
    {
      $mess = $vnT->lang['no_have_order'];
      $this->skin->assign('mess', $mess);
      $this->skin->parse("manage.html_row_no");
    } 
		
		$table['button'] = ''; 		  
		$data['total_money_add'] = get_format_money($total_money_add) ;
		$data['total_money_apply'] = get_format_money($total_money_apply);
		$data['total_money_use'] =   get_format_money($total_money_use);
		$data['total_all'] = get_format_money($total_all);
     
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
		$data['list_search'] =   vnT_HTML::selectbox("search", array(
      'mem_id' => 'Mem ID' ,'username' => 'Username' , 'full_name' => 'Họ tên'  , 'email' => 'Email thành viên', 'phone' => 'Điện thoại '  
    ), $search);
		 
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end; 
		
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
	
	/**
   * function do_money_add() 
   * 
   **/
  function do_money_add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $mem_id = (int) $vnT->input['mem_id'];    
    //check
    $res = $DB->query("SELECT mem_id , full_name , email , mem_point FROM members WHERE mem_id=" . $mem_id);
    if ($r = $DB->fetch_row($res))   {
      $data['f_title'] = "Lịch sử nạp Tiền của : ".$r['full_name']." (".$r['email'].") ";
    }  else  {
      die("Thành viên này không tồn tại");
    }
		  
		$total_price=0;		
    $table['link_action'] = $this->linkUrl . "&sub=sub_comment&b_id=" . $b_id;
    $table['title'] = array(      
      'stt' => "STT|5%|center" , 
      'action' => "&nbsp;|10%|left" , 
      'reason' => "Từ |30%|left",
			'datesubmit' => "Ngày |15%|center",
			'value' => "Tiền nạp |15%|center",
    );
    
		$where =  "AND (action='add' OR action='receive' ) ";	
    $sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC ";  
		
		$reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        
				$total_price += $row[$i]['value'];
				
				$row_info['stt'] = ($i+1);
				$row_info['action'] = $this->money_action[$row[$i]['action']];
				$row_info['value'] = get_format_money($row[$i]['value']);
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']);
				$row_info['reason'] = $row[$i]['reason'];
				
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có  </div>";
    }
		
		$table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
	 
		$data['total_price'] = get_format_money($total_price); 

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("html_popup");
    return $this->skin->text("html_popup");
  }
	
	/**
   * function do_money_apply() 
   * 
   **/
  function do_money_apply ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $mem_id = (int) $vnT->input['mem_id'];    
    //check
    $res = $DB->query("SELECT mem_id , full_name , email , mem_point FROM members WHERE mem_id=" . $mem_id);
    if ($r = $DB->fetch_row($res))   {
      $data['f_title'] = "Lịch sử Tiền thưởng của : ".$r['full_name']." (".$r['email'].") ";
    }  else  {
      die("Thành viên này không tồn tại");
    }
		  
		$total_price=0;		
    $table['link_action'] = $this->linkUrl . "&sub=sub_comment&b_id=" . $b_id;
    $table['title'] = array(      
      'stt' => "STT|5%|center" , 
      'action' => "&nbsp;|10%|left" , 
      'reason' => "Lý do thưởng |30%|left",
			'datesubmit' => "Ngày |15%|center",
			'value' => "Tiền thưởng |15%|center",
    );
    
		$where =  " AND (action='loantin' OR action='register' OR action='tellfriend' OR action='complete_order' ) ";	
    $sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC ";  
		
		$reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        
				$total_price += $row[$i]['value'];
				
				$row_info['stt'] = ($i+1);
				$row_info['action'] = $this->money_action[$row[$i]['action']];
				$row_info['value'] = get_format_money($row[$i]['value']);
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']);
				$row_info['reason'] = $row[$i]['reason'];
				
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có  </div>";
    }
		
		$table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
	 
		$data['total_price'] = get_format_money($total_price); 

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("html_popup");
    return $this->skin->text("html_popup");
  }
	
	/**
   * function do_money_add() 
   * 
   **/
  function do_money_use ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $mem_id = (int) $vnT->input['mem_id'];    
    //check
    $res = $DB->query("SELECT mem_id , full_name , email , mem_point FROM members WHERE mem_id=" . $mem_id);
    if ($r = $DB->fetch_row($res))   {
      $data['f_title'] = "Lịch sử sử dụng Tiền của : ".$r['full_name']." (".$r['email'].") ";
    }  else  {
      die("Thành viên này không tồn tại");
    }
		  
		$total_price=0;		
    $table['link_action'] = $this->linkUrl . "&sub=sub_comment&b_id=" . $b_id;
    $table['title'] = array(      
      'stt' => "STT|5%|center" , 
      'action' => "&nbsp;|10%|left" , 
      'reason' => "Lý do sử dụng |30%|left",
			'datesubmit' => "Ngày |15%|center",
			'value' => "Tiền sử dụng |15%|center",
    );
    
		$where =  " AND (action='use' OR action='send' OR action='order' OR action='active_estore' ) ";	
    $sql = "SELECT * 
						FROM money_history 
						WHERE mem_id=$mem_id 
						{$where}
						ORDER BY datesubmit DESC ";  
		
		$reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        
				$total_price += $row[$i]['value'];
				
				$row_info['stt'] = ($i+1);
				$row_info['action'] = $this->money_action[$row[$i]['action']];
				$row_info['value'] = get_format_money($row[$i]['value']);
				$row_info['datesubmit'] = @date("H:i , d/m/Y",$row[$i]['datesubmit']);
				$row_info['reason'] = $row[$i]['reason'];
				
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có  </div>";
    }
		
		$table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
	 
		$data['total_price'] = get_format_money($total_price); 

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("html_popup");
    return $this->skin->text("html_popup");
  }	
	
  // end class
}
?>