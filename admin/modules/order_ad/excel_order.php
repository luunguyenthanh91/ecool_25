<?php
	session_start();
	define('IN_vnT',1);
	define('DS', DIRECTORY_SEPARATOR);
	
	require_once("../../../_config.php"); 
	include($conf['rootpath']."includes/class_db.php"); 
	$DB = new DB;	
	//Functions
	include ($conf['rootpath'] . 'includes/class_functions.php');
	include($conf['rootpath'] . 'includes/admin.class.php');
	$func  = new Func_Admin;
	$conf = $func->fetchDbConfig($conf);
	
 	if(empty($_SESSION['admin_session']))	{
		die("Ban khong co quyen truy cap trang nay") ;
	}
	
  require_once($conf['rootpath'].'libraries/excel/Worksheet.php');
  require_once($conf['rootpath'].'libraries/excel/Workbook.php');
	
	
	function get_city_name ($code)
	{
		global $vnT, $func, $DB, $conf;
		$text = $code;
		$sql = "select name from iso_cities where code='$code' ";
		$result = $DB->query($sql);
		if ($row = $DB->fetch_row($result)) {
			$text = $func->HTML($row['name']);
		}
		return $text;
	}
	//----------- get_status_order
	function get_status_order ($id, $lang = "vn")
	{
		global $func, $DB, $conf;
		$text = "UnKnow";
		$result = $DB->query("SELECT * FROM order_status where status_id=$id");
		if ($row = $DB->fetch_row($result)) {
			$text = $func->fetch_content($row['title'], $lang); 
		}
		return $text;
	}
 
  function HeaderingExcel($filename) {
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$filename" );
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Pragma: public");
	}

  // HTTP headers
	$filename = 'order_'.time().'.xls';
  HeaderingExcel($filename);

  // Creating a workbook
	$workbook = new Workbook("-");
	
	// Format for the headings
	$format_header =& $workbook->add_format();
	$format_header->set_size(22);
	$format_header->set_color('red');
	$format_header->set_align('center');
	$format_header->set_align('vcenter');
	$format_header->set_merge(); 
	
// Creating the second worksheet
	$worksheet =& $workbook->add_worksheet("THONG KE HOA DON");
 		
		$lang = ($_GET['lang']) ? $_GET['lang'] : "vn";
	  $keyword = ($_GET['keyword']) ? $_GET['keyword'] : "";
    $status = (isset($_GET['status']))? $_GET['status'] : "-1";
    $type_mem = (isset($_GET['type_mem'])) ? $_GET['type_mem'] : "-1";
    $search = ($_GET['search']) ? $_GET['search'] : "order_code";
		$date_begin = ($_GET['date_begin']) ?  $_GET['date_begin'] : "";
		$date_end = ($_GET['date_end']) ?  $_GET['date_end'] : "";
		$mem_id = ($_GET['mem_id'])  ? $_GET['mem_id'] : 0;
		
    $where = " ";
    if ($type_mem != "-1") {
      if ($type_mem == 0) {
        $where .= " and mem_id=0 ";
      } else {
        $where .= " and mem_id<>0 ";
      } 
    }
		if ($status != "-1") {
      $where .= " and status=" . $status; 
    }
		
		if ($mem_id) {
      $where .= " and mem_id=$mem_id "; 
    }		
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_order BETWEEN {$time_begin} AND {$time_end} ) "; 
		}
		
		if ($keyword) {
			switch ($search)
			{
				case  "order_code" :  $where .= " AND order_code = '$keyword' "; break ;
				case  "mem_id" :  $where .= " AND mem_id =$keyword "; break ;
				case  "date_order" :  $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_order),'%d/%m/%Y') = '{$keyword}' "; break ;
				default :	$where .= " AND $search like '%$keyword%' "; break ;
			}        
    } 
	
	$sql = "SELECT * FROM order_sum  where order_id <>0 $where ORDER BY  date_order DESC , order_id DESC  ";
		
	// 
	$reuslt = $DB->query($sql);
	if ($num = $DB->num_rows($reuslt))
	{
		 
		// Format for the headings
		$formatot =& $workbook->add_format();
		$formatot->set_size(12);
		$formatot->set_align('center'); 
		$formatot->set_bold();	
		
		$worksheet->set_column(0,0,5);
		$worksheet->set_column(0,1,20);
		$worksheet->set_column(0,2,30);
		$worksheet->set_column(0,3,20);
		$worksheet->set_column(0,4,20);
		$worksheet->set_column(0,5,50);
		$worksheet->set_column(0,6,20);
		$worksheet->set_column(0,7,20);
		$worksheet->set_column(0,8,20);
		
		
		$worksheet->write_string(0,0,"STT",$formatot);
		$worksheet->write_string(0,1,"Ma HD",$formatot);
		$worksheet->write_string(0,2,"Khach hang ",$formatot);		
		$worksheet->write_string(0,3,"Email",$formatot);
		$worksheet->write_string(0,4,"Dien thoai",$formatot);		
		$worksheet->write_string(0,5,"Dia chi",$formatot); 
		$worksheet->write_string(0,6,"Tong tien",$formatot); 
		$worksheet->write_string(0,7,"Ngay dat hang",$formatot);				
		$worksheet->write_string(0,8,"Trang thai",$formatot);
		
		
		
		
		$format_center =& $workbook->add_format();
		$format_center->set_align('center');
		
		$format_left =& $workbook->add_format();
		$format_left->set_text_wrap();
		$format_left->set_align('left');
				
		
		$dong =1;	
		$stt=0;
		while($row = $DB->fetch_row($reuslt))
		{
			$stt++;
			$order_code = $row['order_code'] ;
			
			// nguoi mua
			$name =   	$func->utf8_to_ascii($row['d_name']) ;
			
			$address =  $row['d_address'];
			if ($row['d_city']) {
				$address .= ", " . get_city_name($row['d_city']);
			} 
			$address	=	$func->utf8_to_ascii($address);		
			
			$phone =  $row['d_phone'];
			$email =  $row['d_email'];
  			
			//  
			$mem_point = (int)$row['mem_point_earned'];
			$total_price = (int)$row['total_price'];
			$date_order = @date("H:i, d/m/Y",$row['date_order'])  ;
			$status_order = $func->utf8_to_ascii(get_status_order($row['status'],$lang));
			
			$worksheet->write($dong,0,$stt,$format_center);
			$worksheet->write_string($dong,1,$order_code,$format_center);
			$worksheet->write_string($dong,2,$name,$format_left);
			$worksheet->write_string($dong,3,$email,$format_left);			
			$worksheet->write_string($dong,4,$phone,$format_center);			
			$worksheet->write_string($dong,5,$address,$format_left); 
			$worksheet->write_string($dong,6,$total_price,$format_center); 		
			$worksheet->write_string($dong,7,$date_order,$format_center);
			$worksheet->write_string($dong,8,$status_order,$format_center);
					
			$dong++;
		}
	}	

	$workbook->close();
 
	$DB->close();

?>