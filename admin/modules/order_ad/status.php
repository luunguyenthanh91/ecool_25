<?php
/*================================================================================*\
|| 							Name code : status.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (!defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "order";
  var $action = "status";

  function vntModule(){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE.DS.$this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_status'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_status'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_status'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnt->input['is_order'] = 1;
    $vnt->input['edit_price'] = 0;
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $res_lang = $DB->query("select * from language ");
      while ($row_lang = $DB->fetch_row($res_lang)) {
        $name = $row_lang['name'];
        $arr_title[$name] = $vnT->input['title'];
      }
      if (empty($err)) {
        $cot['title'] = serialize($arr_title);
        $cot['color'] = $vnT->input['color'];
        $cot['is_default'] = $vnT->input['is_default'];
        $cot['is_confirm'] = $vnT->input['is_confirm'];
        $cot['is_complete'] = $vnT->input['is_complete'];
        $cot['is_shipping'] = $vnT->input['is_shipping'];
        $cot['is_payment'] = $vnT->input['is_payment'];
        $cot['is_cancel'] = $vnT->input['is_cancel'];
        $cot['is_customer'] = $vnT->input['is_customer'];
        $ok = $DB->do_insert("order_status", $cot);
        if ($ok) {
          $status_id = $DB->insertid();
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $status_id);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['is_default'] = vnT_HTML::list_yesno("is_default", $vnT->input['is_default']);
    $data['is_confirm'] = vnT_HTML::list_yesno("is_confirm", $vnT->input['is_confirm']);
    $data['is_complete'] = vnT_HTML::list_yesno("is_complete", $vnT->input['is_complete']);
    $data['is_shipping'] = vnT_HTML::list_yesno("is_shipping", $vnT->input['is_shipping']);
    $data['is_payment'] = vnT_HTML::list_yesno("is_payment", $vnT->input['is_payment']);
    $data['is_cancel'] = vnT_HTML::list_yesno("is_cancel", $vnT->input['is_cancel']);
    $data['is_customer'] = vnT_HTML::list_yesno("is_customer", $vnT->input['is_customer']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int)$vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      if (empty($err)) {
        $cot['title'] = $func->update_content("order_status","title", "status_id=$id ", $lang, $vnT->input['title']);
        $cot['color'] = $vnT->input['color'];
        $cot['is_default'] = $vnT->input['is_default'];
        $cot['is_confirm'] = $vnT->input['is_confirm'];
        $cot['is_complete'] = $vnT->input['is_complete'];
        $cot['is_shipping'] = $vnT->input['is_shipping'];
        $cot['is_payment'] = $vnT->input['is_payment'];
        $cot['is_cancel'] = $vnT->input['is_cancel'];
        $cot['is_customer'] = $vnT->input['is_customer'];
        // more here ...
        $ok = $DB->do_update("order_status", $cot, "status_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM order_status WHERE status_id=$id");
    if ($data = $DB->fetch_row($query)) {
      $data['title'] = $func->fetch_content($data['title'], $lang);
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['is_default'] = vnT_HTML::list_yesno("is_default", $data['is_default']);
    $data['is_confirm'] = vnT_HTML::list_yesno("is_confirm", $data['is_confirm']);
    $data['is_complete'] = vnT_HTML::list_yesno("is_complete", $data['is_complete']);
    $data['is_shipping'] = vnT_HTML::list_yesno("is_shipping", $data['is_shipping']);
    $data['is_payment'] = vnT_HTML::list_yesno("is_payment", $data['is_payment']);
    $data['is_cancel'] = vnT_HTML::list_yesno("is_cancel", $data['is_cancel']);
    $data['is_customer'] = vnT_HTML::list_yesno("is_customer", $data['is_customer']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int)$vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM order_status WHERE status_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
      //xoa cache
      $func->clear_cache();
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    $id = $row['status_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['s_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)'/>";
    $output['title'] = "<strong style='color:#".$row['color']."'>".$func->fetch_content($row['title'], $lang)."</strong>";
    $output['is_default'] = ($row['is_default']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_confirm'] = ($row['is_confirm']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_complete'] = ($row['is_complete']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_shipping'] = ($row['is_shipping']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_payment'] = ($row['is_payment']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_cancel'] = ($row['is_cancel']) ? "<b class=font_err>Yes</b>" : "No";
    $output['is_customer'] = ($row['is_customer']) ? "<b class=font_err>Yes</b>" : "No";
    $link_display = $this->linkUrl . $row['ext_link'];
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display . "&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."'><img src=\"" .$vnT->dir_images."/dispay.gif\" width=15/></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id' title='".$vnT->lang['click_do_display']."'><img src=\"".$vnT->dir_images."/nodispay.gif\" width=15 /></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="'.$link_edit.'"><img src="'.$vnT->dir_images.'/edit.gif" alt="Edit"></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="'.$link_del.'"><img src="'.$vnT->dir_images . '/delete.gif" alt="Delete "></a>';
    return $output;
  }
  function do_Manage($lang){
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['s_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("order_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("order_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("order_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, -2) . "</strong><br>";
          $err = $func->html_mess($mess);
          break;
      }
    }
    if ((int)$vnT->input["do_display"]) {
      $ok = $DB->query("Update order_status SET display=1 WHERE status_id=" . $vnT->input["do_display"]);
      if ($ok) {
        $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>" . $vnT->input["do_display"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      //xoa cache
      $func->clear_cache();
    }
    if ((int)$vnT->input["do_hidden"]) {
      $ok = $DB->query("Update order_status SET display=0 WHERE status_id=" . $vnT->input["do_hidden"]);
      if ($ok) {
        $mess .= "- ".$vnT->lang['display_success']." ID: <strong>" . $vnT->input["do_hidden"] . "</strong><br>";
        $err = $func->html_mess($mess);
      }
      $func->clear_cache();
    }
    $p = ((int)$vnT->input['p']) ? $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT status_id FROM order_status  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
    $ext_link = $ext . "&p=$p";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center",
      'order' => $vnT->lang['order'] . "|7%|center",
      'title' => $vnT->lang['status'] . "||left",
      'is_default' => "Is Default |8%|center",
      'is_confirm' => "Is Confirm |8%|center",
      'is_payment' => "Is Payment |8%|center",
      'is_shipping' => "Is Shipping |8%|center",
      'is_complete' => "Is Complete |8%|center",
      'is_cancel' => "Is Cancel |8%|center",
      'is_customer' => "Is Cutomer|8%|center",
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM order_status	ORDER BY  s_order  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i++) {
        $row[$i]['ext_link'] = $ext_link;
        $row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['status_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_status'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}
$vntModule = new vntModule();
?>