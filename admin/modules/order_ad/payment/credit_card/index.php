
<p><a href="http://www.paypal.com/"><img
	src="<?=$path_module?>/logo.gif" alt="" border="0" title="" /></a></p>

<?php
if (isset($msg)) {
  echo stripslashes($msg);
}
?>

<p class="copyText">This is the standard PayPal gateway for basic IPN or
standard form integration. If you wish to use PayPal Pro please disable
this gateway and configure &quot;PayPal Express Checkout&quot; and
&quot;PayPal Direct Payment&quot;.</p>



<form action="<?=$link_action?>" method="post" name="f_config">

<table border="0" cellspacing="0" cellpadding="3" width="100%">

	<tr>

		<td colspan="2" class="row_tittle">Configuration Settings</td>
	</tr>

	<tr>

		<td align="left" class="tdText"><strong>Description:</strong></td>

		<td class="tdText"><input type="text" name="module[desc]"
			value="<?php
  echo $module['desc'];
  ?>" class="textbox" size="30" /></td>
	</tr>

	<tr>

		<td align="left" class="tdText"><strong>Email Address:</strong></td>

		<td class="tdText"><input type="text" name="module[email]"
			value="<?php
  echo $module['email'];
  ?>" class="textbox" size="30" /></td>
	</tr>

	<tr>

		<td align="left" class="tdText"><strong>Method:</strong><br />

		If IPN is selected you must login to PayPal and enable IPN.<br />

		<strong>IPN URL:</strong><br /><?php
  echo $conf['rooturl'] . "modules/gateway/PayPal/ipn.php";
  ?></td>

		<td class="tdText"><select name="module[method]">

			<option value="ipn"
				<?php
    if ($module['method'] == "ipn")
      echo "selected='selected'";
    ?>>IPN
			(Recommended)</option>

			<option value="std"
				<?php
    if ($module['method'] == "std")
      echo "selected='selected'";
    ?>>Standard</option>
		</select></td>
	</tr>

	<tr>

		<td align="left" class="tdText"><strong>Test mode?</strong><br />

		(Requires a PayPal Developer Account)</td>

		<td class="tdText"><select name="module[testMode]">

			<option value="1"
				<?php
    if ($module['testMode'] == 1)
      echo "selected='selected'";
    ?>>Yes</option>

			<option value="0"
				<?php
    if ($module['testMode'] == 0)
      echo "selected='selected'";
    ?>>No</option>

		</select></td>
	</tr>

	<tr>

		<td colspan="2" align="right" class="tdText">
		<div align="left">IMPORTANT: Many have not been able to get IPN
		working with Sandbox. If so try a live transaction of 0.01 and it
		should work!</div>
		</td>
	</tr>

	<tr>

		<td colspan="2" align="center"><input type="submit" name="btnConfig"
			value="Edit Config" /></td>
	</tr>
</table>

</form>
