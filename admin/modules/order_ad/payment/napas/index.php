


<?php 

if(isset($msg)){ 

	echo stripslashes($msg); 

} 

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="150"><a href="http://napas.com.vn"><img src="modules/order_ad/payment/napas/logo.png" alt="" border="0" title="" /></a></td>
    <td valign="top" style="padding-left:10px;"><p style="line-height:18px;" >Công ty Cổ phần Thanh toán Quốc gia Việt nam (NAPAS) được thành lập vào năm 2004, đổi tên từ Công ty Cổ phần Chuyển mạch Tài chính Quốc gia Việt Nam (Banknetvn) kể từ ngày 04/02/2016, trên cơ sở sáp nhập với Công ty CP Dịch vụ thẻ Smartlink để xây dựng Trung tâm chuyển mạch thẻ thống nhất theo chỉ đạo của Thủ tướng Chính phủ và Ngân hàng Nhà nước Việt Nam.</p></td>
  </tr>
</table>
 

<form action="<?php echo $link_action; ?>" method="post" name="f_config">
<h3 class="font_title">Configuration Settings</h3><br />

<table width="100%" cellspacing="1" cellpadding="1" border="0" class="admintable">
   
 
  <tr >
     <td  colspan="2" class="row_title" > <strong>Cấu hình thẻ nội địa</strong></td>
 		</tr> 
    
    <tr >
     <td class="row1" > VPC Version : </td>
     <td class="row0"><input name="module[vpc_Version]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_Version']; ?>" class="textfield">  
     </td>
 		</tr>
   
   <tr >
     <td class="row1" > Command Type : </td>
     <td class="row0"><input name="module[vpc_Command]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_Command']; ?>" class="textfield">  
     </td>
 		</tr>
    
		<tr >
     <td class="row1" > MerchantID : </td>
     <td class="row0"><input name="module[vpc_Merchant]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_Merchant']; ?>" class="textfield">  
     </td>
 		</tr>     
     <tr   >
     <td class="row1" width="20%" > Merchant AccessCode : </td>
     <td class="row0"><input name="module[vpc_AccessCode]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_AccessCode']; ?>" class="textfield">  
     </td>
 		</tr>  
    <tr   >
     <td class="row1" > Secure Hash : </td>
     <td class="row0"><input name="module[vpc_SecureHash]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_SecureHash']; ?>" class="textfield">  
     </td>
 		</tr> 
    
     <tr >
     <td class="row1" > vpc_Currency : </td>
     <td class="row0"><input name="module[vpc_Currency]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_Currency']; ?>" class="textfield">  
     </td>
 		</tr> 
     <tr   >
     <td class="row1" > vpc_Locale : </td>
     <td class="row0"><input name="module[vpc_Locale]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_Locale']; ?>" class="textfield">  
     </td>
 		</tr> 
    
   	<tr   >
     <td class="row1" > PaymentUrl : </td>
     <td class="row0"><input name="module[PaymentUrl]" type="text" size="70" maxlength="250" value="<?php echo $module['PaymentUrl']; ?>" class="textfield">  
     </td>
 		</tr>


  <tr >
    <td  colspan="2" class="row_title" > <strong>Cấu hình thẻ Quốc tế</strong></td>
  </tr>

  <tr >
    <td class="row1" > VPC Version : </td>
    <td class="row0"><input name="module[vpc_VersionQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_VersionQT']; ?>" class="textfield">
    </td>
  </tr>

  <tr >
    <td class="row1" > Command Type : </td>
    <td class="row0"><input name="module[vpc_CommandQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_CommandQT']; ?>" class="textfield">
    </td>
  </tr>
  <tr >
    <td class="row1" > MerchantID : </td>
    <td class="row0"><input name="module[vpc_MerchantQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_MerchantQT']; ?>" class="textfield">
    </td>
  </tr>

  <tr   >
    <td class="row1" width="20%" > Merchant AccessCode : </td>
    <td class="row0"><input name="module[vpc_AccessCodeQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_AccessCodeQT']; ?>" class="textfield">
    </td>
  </tr>

  <tr   >
    <td class="row1" > Secure Hash : </td>
    <td class="row0"><input name="module[vpc_SecureHashQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_SecureHashQT']; ?>" class="textfield">
    </td>
  </tr>

  <tr >
    <td class="row1" > vpc_Currency : </td>
    <td class="row0"><input name="module[vpc_CurrencyQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_CurrencyQT']; ?>" class="textfield">
    </td>
  </tr>
  <tr   >
    <td class="row1" > vpc_Locale : </td>
    <td class="row0"><input name="module[vpc_LocaleQT]" type="text" size="70" maxlength="250" value="<?php echo $module['vpc_LocaleQT']; ?>" class="textfield">
    </td>
  </tr>

  <tr   >
    <td class="row1" > PaymentUrl : </td>
    <td class="row0"><input name="module[PaymentUrlQT]" type="text" size="70" maxlength="250" value="<?php echo $module['PaymentUrlQT']; ?>" class="textfield">
    </td>
  </tr>




  <tr>
      <td  class="row1">&nbsp;</td>
      <td  class="row0" ><input type="submit" name="btnConfig" value="Edit Config" class="button" /></td>
    </tr>
</table>

</form>
