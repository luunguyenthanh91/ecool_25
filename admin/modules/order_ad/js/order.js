(function (jQuery) {
	jQuery.fn.clickoutside = function (callback) {
		var outside = 1,
			self = $(this);
		self.cb = callback;
		this.click(function () {
			outside = 0
		});
		$(document).click(function (event) {
			if (event.button == 0) {
				outside && self.cb();
				outside = 1
			}
		});
		return $(this)
	}
})(jQuery);



vnTOrder = {
//	--------------------------------- main action -------------------------------- // 
	update_order_status:function (orderid, statusid) {
  	
	var mydata =  'oid='+orderid+"&sid="+statusid; 
		$.ajax({
			async: true,
			dataType: 'json',
			url: "modules/order_ad/ajax/ajax.php?do=updateOrderStatus",
			type: 'POST',
			data: mydata ,
			success: function (data) {			
				$('#ext_status'+orderid).html(data.html); 
				display_message ('ajax_mess',data.mess,0);
				if(data.ok==0) {
					jAlert(data.mess,'Báo lỗi')	;
				}
				$('#div_status'+orderid).hide();
			},
			error: function()
			{
				$('#ext_status'+orderid).attr('src', DIR_IMAGE+'/ajax-blank.gif');
				alert('Update Error');
			}
		});
		
	} ,

	update_order_admin:function (orderid, adminid) {

		var mydata =  'oid='+orderid+"&adminid="+adminid;
		$.ajax({
			async: true,
			dataType: 'json',
			url: "modules/order_ad/ajax/ajax.php?do=updateOrderAdmin",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				$('#ext_admin'+orderid).html(data.html);
				display_message ('ajax_mess',data.mess,0);
				if(data.ok==0) {
					jAlert(data.mess,'Báo lỗi')	;
				}
				$('#div_admin'+orderid).hide();
			},
			error: function()
			{
				$('#ext_admin'+orderid).attr('src', DIR_IMAGE+'/ajax-blank.gif');
				alert('Update Error');
			}
		});

	} ,

	update_quick_order_tatus:function (orderid, statusid) {

		var mydata =  'oid='+orderid+"&sid="+statusid;
		$.ajax({
			async: true,
			dataType: 'json',
			url: "modules/order_ad/ajax/ajax.php?do=updateDKNStatus",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				$('#ext_status'+orderid).html(data.html);
				display_message ('ajax_mess',data.mess,0);
				if(data.ok==0) {
					jAlert(data.mess,'Báo lỗi')	;
				}
				$('#div_status'+orderid).hide();
			},
			error: function()
			{
				$('#ext_admin'+orderid).attr('src', DIR_IMAGE+'/ajax-blank.gif');
				alert('Update Error');
			}
		});

	} ,


	
	do_Print_Multi:function () {
		if (selected_item()){
			question = confirm('Bạn có chắc muốn In các đơn hàng này ?')
			if (question != "0"){
				var ids = '-1';
				$(".adminlist tbody :input:checkbox").each( function() {
					var c = $(this).attr('checked');
					if (c){
						ids += ","+$(this).val();
					}
				});

				openPopUp('?mod=order&act=order&lang=vn&sub=print_multi&ids='+ids,'Print',800,600, 'Yes')
			}else{
				alert ('Phew~');
			}
		}

	},


	load_NumOrderPhone:function () {

		var mydata =  'phone='+$("#list_phone").val() ;
		$.ajax({
			async: true,
			dataType: 'json',
			url: "modules/order_ad/ajax/ajax.php?do=order_phone",
			type: 'POST',
			data: mydata ,
			success: function (data) {

				$.each(data, function(idx, val) {
					$(".order_phone_"+idx).html(val);
				});
			}
		});

	},

	addNote:function(id)
	{
		//tb_show('Cập nhật ghi chú','?mod=order&act=order&sub=note_qty&id='+id+'&height=100');
		var note_order = $("#note_order").val();
		var mydata =  'id='+id+'&note_order='+encodeURIComponent(note_order);
		$.ajax({
			async: true,
			dataType: 'json',
			url: "modules/order_ad/ajax/ajax.php?do=addNote",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				if(data.ok==1) {
					tb_remove();
					alert(data.mess)	;
				}
			}
		});
	},


	init:function () {
		$(".load_city").change(function() {
			var ext_display = $(this).attr("data-city");

			var mydata =  "do=option_city&country="+ $(this).val()+"&lang="+lang;
			$.ajax({
				type: "GET",
				url: ROOT+'load_ajax.php',
				data: mydata,
				success: function(html){
					$("#"+ext_display).html(html);
				}
			});
		});

		$(".load_state").change(function() {
			var ext_display = $(this).attr("data-state");

			var mydata =  "do=option_state&city="+ $(this).val()+"&lang="+lang;
			$.ajax({
				type: "GET",
				url: ROOT+'load_ajax.php',
				data: mydata,
				success: function(html){
					$("#"+ext_display).html(html);
				}
			});
		});


		$(".load_ward").change(function() {
			var ext_display = $(this).attr("data-ward") ;

			var mydata =  "do=option_ward&state="+ $(this).val()+"&lang="+lang;
			$.ajax({
				type: "GET",
				url: ROOT+'load_ajax.php',
				data: mydata,
				success: function(html){
					$("#"+ext_display).html(html);
				}
			});
		});



		$(".boxDropDown a").click(function () {
			$(this).next().slideToggle(0, function () {
				if ($(this).is(":hidden")) {
					$(this).prev().removeClass("active")
				} else {
					$(this).prev().addClass("active")
				}
			})
		});

		$(".boxDropDown a").clickoutside(function () {
			$(this).next().hide();
		});

	}

};


$(document).ready(function () {
	vnTOrder.init();
});




