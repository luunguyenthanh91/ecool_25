<!-- BEGIN: add --> 
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: { 
				pid: {
					required: true 
				}
	    },
	    messages: {	    	
				 
				pid: {
						required: "{LANG.err_select_required}" 
				} 
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  id="myForm" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

 <tr class="form-required">

    <tr>
     <td class="row1"  width="20%" >Số lượng Code cần tạo: </td>
     <td class="row0"><b class="font_err"><input name="num_code" type="text" size="20" maxlength="4" value="{data.num_code}" onKeyPress="return is_num(event,'n_list')"  ></td>
    </tr>
    <tr>
     <td class="row1">{LANG.promotion_price}: </td>
     <td  class="row0"><input name="price" type="text" size="20" maxlength="250" value="{data.price}"> {data.price_type}</td>
    </tr>
	  <tr>
		  <td class="row1">{LANG.expires}: </td>
		  <td  class="row0">

			  <div class="type_expire">
				  <div class="expire-item clearfix">
					  <label class="div-label"  ><input type="radio" name="chk_time" id="chk" value="1" {data.checked.1} > {LANG.by_number} :</label>
					  <div class="div-input">{LANG.quantity}: <input type="text" name="quantity" id="quantity" value="{data.quantity}">
					  </div>
				  </div>
				  <div class="expire-item clearfix">
					  <label class="div-label"><input type="radio" name="chk_time" id="no_chk" value="0" {data.checked.0} > {LANG.by_date} : </label>
					  <div class="div-input">{LANG.date_begin}  : <input type="text" name="day_start" id="day_start"  value="{data.day_start}" size="10" maxlength="10" class="dates">  &nbsp; | &nbsp; {LANG.date_expire} : <input type="text" name="day_end" id="day_end" value="{data.day_end}"  size="10" maxlength="10" class="dates" /></div>
				  </div>
			  </div>



		  </td>
	  </tr>
     <tr>
     <td class="row1">{LANG.display}: </td>
     <td  class="row0">{data.list_display}</td>
    </tr>
    
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
            	<input type="hidden" name="chk_ok"	id="chk_ok" value="{data.expired}" />
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
if(getobj('chk_ok').value==1)
{
	getobj('amount').style.display='block';
	getobj('day_time').style.display='none';
	getobj('chk').checked=true;
}
else
{
	getobj('no_chk').checked=true;
	getobj('day_time').style.display='block';
	getobj('amount').style.display='none';
}
</script>
<br>
<!-- END: add -->



<!-- BEGIN: edit --> 
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: { 
				code: {
					required: true 
				}
	    },
	    messages: {	    	
				 
				code: {
						required: "{LANG.err_text_required}" 
				} 
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  id="myForm" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    
    <tr>
     <td class="row1"  width="20%" >Promotion Code: </td>
     <td class="row0"><input name="code" type="text" size="30" maxlength="250" value="{data.code}"></td>
    </tr>
    <tr>
     <td class="row1">{LANG.promotion_price}: </td>
     <td  class="row0"><input name="price" type="text" size="20" maxlength="250" value="{data.price}"> {data.price_type}</td>
    </tr>
    <tr>
     <td class="row1">{LANG.expires}: </td>
     <td  class="row0">

		 <div class="type_expire">
			 <div class="expire-item clearfix">
				 <label class="div-label"  ><input type="radio" name="chk_time" id="chk" value="1" {data.checked.1} > {LANG.by_number} :</label>
				 <div class="div-input">{LANG.quantity}: <input type="text" name="quantity" id="quantity" value="{data.quantity}">
				 </div>
			 </div>
			 <div class="expire-item clearfix">
				 <label class="div-label"><input type="radio" name="chk_time" id="no_chk" value="0" {data.checked.0} > {LANG.by_date} : </label>
				 <div class="div-input">{LANG.date_begin}  : <input type="text" name="day_start" id="day_start"  value="{data.day_start}" size="10" maxlength="10" class="dates">  &nbsp; | &nbsp; {LANG.date_expire} : <input type="text" name="day_end" id="day_end" value="{data.day_end}"  size="10" maxlength="10" class="dates" /></div>
			 </div>
		 </div>


	 </td>
    </tr>    
     <tr>
     <td class="row1">{LANG.display}: </td>
     <td  class="row0">{data.list_display}</td>
    </tr>
    
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
            	<input type="hidden" name="chk_ok"	id="chk_ok" value="{data.expired}" />
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">
if(getobj('chk_ok').value==1)
{
	getobj('amount').style.display='block';
	getobj('day_time').style.display='none';
	getobj('chk').checked=true;
}
else
{
	getobj('no_chk').checked=true;
	getobj('day_time').style.display='block';
	getobj('amount').style.display='none';
}
</script>
<br>
<!-- END: edit -->



<!-- BEGIN: html_import -->

{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
	<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">


		<tr   >
			<td   class="row1" width="30%" >File Excel (*.xls) : </td>
			<td  align="left" class="row0">

				<div class="div-upload" id="uploader_xls_file">
					<div  class="input-file"  >
						<div class="div-btn"><a id="pickfiles_xls_file" href="javascript:void(0);" class="btnBrowse" >Chọn File</a ></div>
						<div class="div-input"><span class="del-file" title="Xóa file"></span><input name="xls_file" id="xls_file" value=""  type="text"  class="text-file" ></div>
					</div>
					<div class="upload-result" ></div>
					<script type="text/javascript" src="{DIR_JS}/plupload_236/plupload.full.min.js" charset="UTF-8"></script>
					<script language=javascript>
						var options_uppic = {
							runtimes: 'gears,html5,flash,silverlight,browserplus',
							browse_button: 'pickfiles_xls_file',
							max_file_count: 1,
							multi_selection: false,
							url:  'ajax-upload.php',
							filters : [
								{title : "Document files", extensions : "xls,xlsx"}
							],
							flash_swf_url : ROOT+'js/plupload_219/Moxie.swf',
							silverlight_xap_url : ROOT+'js/plupload_219/Moxie.xap',

							multipart_params: {folder_upload : "{data.folder_upload}", dir_upload : "{data.dir_upload}"}
						};
						vnTRUST.vnTUpload('xls_file',options_uppic ) ;
					</script>
				</div>



			</td>
		</tr>


		<tr align="center">
			<td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnSubmit" value="Bắt đầu import" class="button">
			</td>
		</tr>

	</table>
</form>
<p align="center"><b class="font_err">File excel phải có định dạng</b> <br><img src="modules/order_ad/images/excel_coupon.jpg" /></p>
<p align="center"><a href="modules/order_ad/images/excel_coupon.xls" target="_blank"><strong>Download File Excel mẫu</strong></a></p>
<br>
<!-- END: html_import -->


<!-- BEGIN: manage -->




<form action="{data.link_fsearch}" method="post" name="fSearch">

	<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">


		<tr>
			<td class="row1" ><strong>{LANG.search} :</strong>   </td>  <td class="row0"><input name="keyword" value="{data.keyword}" size="20" type="text" />  &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
		</tr>

		<td width="15%" align="left"><strong>{LANG.totals}:</strong>  &nbsp;</td>
		<td   align="left"><b class="font_err">{data.totals}</b> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="{data.upload_excel}"><img src="{DIR_IMAGE}/icon_excel.gif"  alt="excel" align="absmiddle"/> <b>Thêm từ file Excel</b> </a></td>
		</tr>
	</table>
</form>



{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->