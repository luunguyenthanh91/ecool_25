<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr class="form-required">
     <td class="row1" width="20%">{LANG.name}: </td>
     <td class="row0"><input name="name" type="text" size="50" maxlength="250" value="{data.name}"></td>
    </tr>
    
		<tr class="form-required">
     <td class="row1" width="20%">{LANG.title}: </td>
     <td class="row0"><input name="title" type="text" size="50" maxlength="250" value="{data.title}"></td>
    </tr>
    <!-- <tr >
        <td class="row1" width="20%">{LANG.price}: </td>
        <td class="row0">
            <input name="s_type" id="s_type" type="radio" value="0" {data.r_type1} align="absmiddle" /> 
            <input name="price" type="text" size="20" maxlength="250" value="{data.price}"> VNĐ <br>
            <input name="s_type" type="radio" value="1" {data.r_type2} align="absmiddle" />&nbsp;{LANG.postal_price} (shipping)
        </td>
    </tr> -->
    <tr >
     <td class="row1" width="20%">{LANG.description}: </td>
     <td class="row0">{data.html_content}</td>
    </tr>
    <tr >
     <td class="row1" width="20%">{LANG.display}: </td>
     <td class="row0">{data.list_display}</td>
    </tr>
			
	<tr align="center">
        <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
    <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
        <tr>
            <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
            <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
        </tr>
    </table>
</form>
{data.err}<br />
{data.table_list}<br />
<!-- END: manage -->