<!-- BEGIN: edit -->
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" class="validate">
    <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
        <tr class="form-required">
            <td class="row1" width="30%">{LANG.title}:</td>
            <td class="row0"><input name="title" type="text" size="50" maxlength="250" value="{data.title}"></td>
        </tr>
        <tr>
            <td class="row1" width="20%">Status Color :</td>
            <td class="row0">
                <input name="color" id="color" type="text" size="20" maxlength="250" value="{data.color}">
                (Ex: White : FFFFFF , Black : 000000, Red : FF0000 )
            </td>
        </tr>
        <tr>
            <td class="row1">{LANG.is_default}:</td>
            <td class="row0">{data.is_default}</td>
        </tr>
        <tr>
            <td class="row1">Là trạng thái xác nhận đơn hàng:</td>
            <td class="row0">{data.is_confirm}</td>
        </tr>
        <tr>
            <td class="row1">{LANG.is_payment} :</td>
            <td class="row0">{data.is_payment}</td>
        </tr>
        <tr>
            <td class="row1">Lả trạng thái đã vận chuyển :</td>
            <td class="row0">{data.is_shipping}</td>
        </tr>

        <tr>
            <td class="row1">{LANG.is_complete}:</td>
            <td class="row0">{data.is_complete}</td>
        </tr>
        <tr>
            <td class="row1">{LANG.is_cancel} :</td>
            <td class="row0">{data.is_cancel}</td>
        </tr>
        <tr>
            <td class="row1">{LANG.is_customer} :</td>
            <td class="row0">{data.is_customer}</td>
        </tr>

        <tr align="center">
            <td class="row1">&nbsp; </td>
            <td class="row0">
                <input type="hidden" name="do_submit" value="1"/>
                <input type="submit" name="btnAdd" value="Submit" class="button">
                <input type="reset" name="btnReset" value="Reset" class="button">
            </td>
        </tr>
    </table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
    <table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
        <tr>
            <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
            <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
        </tr>
    </table>
</form>
{data.err}
<br/>
{data.table_list}
<br/>
<table width="100%" border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
    <tr>
        <td height="25">{data.nav}</td>
    </tr>
</table>
<br/>
<!-- END: manage -->