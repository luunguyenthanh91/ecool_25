<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "order";
  var $action = "quick_order";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
		$vnT->html->addScript("modules/" . $this->module . "_ad/js/" . $this->module . ".js"); 
		
		loadSetting($lang);
		
    switch ($vnT->input['sub']) {
      
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý đăng ký mua hàng nhanh';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

   
  /**
   * function do_Task 
   *   
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT; 
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
		

		
    $query = 'DELETE FROM order_quicks WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    //$link_edit = $this->linkUrl . '&sub=edit&id=' . $func->NDK_encode($id) . '&ext=' . $row['ext_page'];
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
       
 		$output['id'] = "<a href=\"{$link_edit}\"><strong class=font_err >#" . $row['id'] . "</strong></a>";
    ;
		 
		$item = '<a href="?mod=product&act=product&sub=edit&id='.$row['item_id'].'" target="_blank"><strong>'.$func->HTML($row['item_title']).'</strong></a>';
		
		$output['item'] = $item;
		
    
     $output['customer'] = "Họ tên : <strong>" . $func->HTML($row['full_name']) . "</strong> ";
    if($row['email'])
      $output['customer']  .= "<div style='padding:2px;'>Email : ".$row['email']."</div>";
    $output['customer']  .= "<div style='padding:2px;'>ĐT : ".$row['phone']."</div>";   
    if($row['address'])
      $output['customer']  .= "<div style='padding:2px;'>ĐC : ".$row['address']."</div>"; 
     


    $info = '<div  >'.$func->HTML($row['comment']).'</div>';
    
    $output['info'] = $info;

		
    $output['date_order'] = date("H:i, d/m/Y", $row['date_order']);
    
		//$output['status'] = $vnT->setting['status'][$row['status']]; 
		
		$output['text_status'] = "<div class='boxDropDown' class='active' ><a href=\"javascript:void(0);\"><span id='ext_status".$id."'>".$vnT->setting['status'][$row['status']]."</span> <img src=\"modules/order_ad/images/arr_down.gif\"  alt=\"Down\" ></a>" ;
		$output['text_status'] .='<div style="display:none;" class="list-dropdown" id="div_status'.$id.'"><div class="list-dropdown-item"><ul>'; 
		foreach ($vnT->setting['status'] as $key => $value)
		{		
			$ext_onClick = "onClick=\"vnTOrder.update_quick_order_tatus('".$id."', '".$key."')\"";
			$output['text_status'] .= "<li><a title='Cập nhật trang thái này cho HĐ' href=\"javascript:void(0);\" {$ext_onClick} >".$value."</a></li>";
		}
		$output['text_status'] .= '</ul><div class="clear"></div></div></div>';
		$output['text_status'] .= '</div>';

    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />'; 
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;   
        //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) { 
        case "do_hidden":
          $mess .= "- Sét đã hủy bỏ ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 2;
            $ok = $DB->do_update("order_quicks", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- Sét đã liên lạc  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['status'] = 1;
            $ok = $DB->do_update("order_quicks", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
    $status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "0";
    if($status)
    {
      $where .=" AND status=".$status;
      $ext_page .= "status=$status|";
      $ext = "&status=$status";
    }
    if($keyword){
      $where .=" AND name like '%".$keyword."%' ";
      $ext_page .= "keyword=$keyword|";
      $ext = "&keyword=$keyword";
    } 

    $query = $DB->query("SELECT * FROM order_quicks WHERE id<>0  $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'date_order' => "Ngày đặt|10%|center" ,
      'item' => "Sản phẩm|20%|left" ,
			'customer' => "Khách hàng |25%|left" ,       
			'info' => "Ghi chú||left" ,         
      'text_status' => "Trạng thái|13%|center" , 
      'action' => "Action|5%|center");
    $sql = "SELECT * FROM order_quicks  WHERE id<>0 $where ORDER BY  id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_order'] . "</div>";
    }
 		
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
		
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['list_status'] = list_status_order($status );
    $data['totals'] = $totals; 
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>