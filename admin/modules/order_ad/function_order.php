<?php
/*================================================================================*\
|| 							Name code : funtion_product.php	 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/product/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/product/');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn"){
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from product_setting WHERE lang='$lang'");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
	$res_s = $DB->query("SELECT * FROM order_status WHERE display=1 ORDER BY s_order ASC");
	while($row_s = $DB->fetch_row($res_s)){
		if($row_s['is_default']==1)	 {
			$vnT->setting['status_default']	= $row_s['status_id'];
		}
    if($row_s['is_confirm']==1)  {
      $vnT->setting['status_confirm'] = $row_s['status_id'];
    }
    if($row_s['is_shipping']==1)	 {
      $vnT->setting['status_shipping']	= $row_s['status_id'];
    }
    if($row_s['is_print']==1)	 {
      $vnT->setting['status_print']	= $row_s['status_id'];
    }
		if($row_s['is_complete']==1)	 {
			$vnT->setting['status_complete']	= $row_s['status_id'];
		}
    if($row_s['is_cancel']==1)	 {
      $vnT->setting['status_cancel']	= $row_s['status_id'];
    }
    if($row_s['is_customer']==1)	 {
			$vnT->setting['status_customer']	= $row_s['status_id'];
		}
    $vnT->setting['status'][$row_s['status_id']] =  "<b style='color:#".$row_s['color']."'>" . $func->fetch_content($row_s['title'], $lang) . "</b>";
	}
  $res_dp = $DB->query("SELECT dp_id,title,is_default FROM delivery_partner  ORDER BY display_order ASC") ;
  while($row_dp = $vnT->DB->fetch_row($res_dp)) {
    $vnT->setting['arr_delivery_partner'][$row_dp['dp_id']] = $row_dp['title'];
    if($row_dp['is_default']){
      $vnT->setting['transporter_default'] = $row_dp['dp_id'];
    }
  }
  $res_ad = $DB->query("SELECT adminid,username FROM admin  ORDER BY adminid ASC") ;
  while($row_ad = $vnT->DB->fetch_row($res_ad)) {
    $vnT->setting['arr_admin'][$row_ad['adminid']] = $row_ad['username'];
  }
  $res_city = $DB->query("SELECT id,name FROM iso_cities ");
  while ($row_city = $DB->fetch_row($res_city))  {
    $vnT->setting['arr_city'][$row_city['id']] = $row_city['name'];
  }
  $res_s = $DB->query("SELECT id,name FROM iso_states ");
  while ($row_s = $DB->fetch_row($res_s))  {
    $vnT->setting['arr_state'][$row_s['id']] = $row_s['name'];
  }
  unset($setting);
}

//====== get_cat_name
function get_cat_name ($cat_id, $lang = "vn")
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_name from product_category where cat_id in ($cat_id) order by cat_id DESC";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['cat_name']);
  }
  return $text;
}

function get_city_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = $code;
  if($vnT->setting['arr_city'][$code]){
    $text = $vnT->setting['arr_city'][$code] ;
  }else{
    $sql = "select name from iso_cities where id=".$code ;
    $result = $DB->query($sql);
    if ($row = $DB->fetch_row($result)) {
      $text = $func->HTML($row['name']);
    }
  }

  return $text;
}

function get_country_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select name from iso_countries where iso='$code' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  //	$text = $vnT->arr_order['country'][$code];
  return $text;
}

//----- get_info_address
function get_info_address ($data, $type = "")
{
  global $DB, $input, $func, $vnT, $conf;
  if ($type == "shipping") {
    $text = $vnT->lang['full_name'] . " : <b>" . $data['c_name'] . "</b>";
    $text .= "<br>" . $vnT->lang['address'] . " : " . $data['c_address'];
    if ($data['c_city'])
      $text .= "<br>" . $vnT->lang['city'] . " : " . get_city_name($data['c_city']);
    $text .= "<br> " . $vnT->lang['country'] . " : " . get_country_name($data['c_country']);
    if ($data['c_phone'])
      $text .= "<br>" . $vnT->lang['phone'] . " : " . $data['c_phone'];
    if ($data['c_mobile'])
      $text .= "<br>" . $vnT->lang['mobile'] . " : " . $data['c_mobile'];
  } else {
    $type_mem = ($data['mem_id'] == 0) ? "Khách vãng lai" : "Thành viên";
    $text = $vnT->lang['full_name'] . " : <b>" . $data['d_name'] . "</b> <span class=font_err>(" . $type_mem . ")</span>";
    if ($data['d_company'])
      $text .= "<br>" . $vnT->lang['company'] . " : " . $data['d_company'];
    $text .= "<br>" . $vnT->lang['address'] . " : " . $data['d_address'];
    if ($data['d_state'])
      $text .= ", " . get_state_name($data['d_state']);
    if ($data['d_city'])
      $text .= ", " . get_city_name($data['d_city']);

    if ($data['d_zipcode'])
      $text .= ", " . $data['d_zipcode'];
    $text .= "<br>" . $vnT->lang['phone'] . " : " . $data['d_phone'];
    if ($data['d_mobile'])
      $text .= "<br>" . $vnT->lang['mobile'] . " : " . $data['d_mobile'];
    if ($data['d_fax'])
      $text .= "<br>Fax : " . $data['d_fax'];
    if ($data['d_email'])
      $text .= "<br>E-mail : " . $data['d_email'];
  }
  return $text;
}

//=================Functions===============
function List_View ($did)
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"view\">";
  if ($did == "1")
    $text .= "<option value=\"10\" selected> 10 </option>";
  else
    $text .= "<option value=\"10\" > 10 </option>";
  if ($did == "20")
    $text .= "<option value=\"20\" selected> 20 </option>";
  else
    $text .= "<option value=\"20\"> 20 </option>";
  if ($did == "50")
    $text .= "<option value=\"50\" selected> 50 </option>";
  else
    $text .= "<option value=\"50\"> 50 </option>";
  $text .= "</select>";
  return $text;
}

//---------
function List_Search ($did, $ext = "")
{
  global $func, $DB, $conf, $vnT;
	$arr_where = array('order_code'=> 'Mã đơn hàng','tracking_no'=> 'Mã Vận Chuyển','mem_id'=> 'ID thành viên','d_name'=>  "Họ tên khách hàng",'d_email'=> 'Email', 'd_phone' => 'Điện thoại', 'd_address' => 'Địa chỉ', 'date_order' => "Ngày đặt hàng (d/m/Y)" );
	
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}


//------------- 
function list_status_order ($did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select size=1 name=\"status\" id=\"status\" {$ext} >";
  $text .= "<option value=\"0\">" . $vnT->lang['select_status'] . "</option>";
  $sql = "SELECT * FROM order_status where display=1 order by s_order ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['status_id'] == $did) {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->fetch_content($row['title'], $lang) . "</option>";
    } else {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->fetch_content($row['title'], $lang) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//----------- get_status_order
function get_status_order ($id, $lang = "vn")
{
  global $func, $DB, $conf;
  $text = "UnKnow";
  $result = $DB->query("SELECT * FROM order_status where status_id=$id");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
    if ($row['is_default']) {
      $text = "<b class=red>" . $text . "</b>";
    }
    if ($row['is_complete']) {
      $text = "<b class=blue>" . $text . "</b>";
    }
  }
  return $text;
}

//----------- get_status_complete
function get_status_complete ()
{
  global $func, $DB, $conf;
  $text = "5";
  $result = $DB->query("SELECT * FROM order_status where is_complete=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}

//----------- get_status_customer
function get_status_customer ()
{
  global $func, $DB, $conf;
  $text = "7";
  $result = $DB->query("SELECT * FROM order_status where is_customer=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}

//====== get_p_name
function get_p_name ($p_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select p_name from products where p_id =$p_id  ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['p_name']);
  }
  return $text;
}

//-----------------  get_pic_product
function get_pic_product ($picture, $w = 150)
{
  global $vnT, $func, $DB, $conf;
  $out = "";
  $ext = "";
  $w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100;
  if ($picture) {
    $linkhinh = "../vnt_upload/product/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    if ($vnT->setting['thum_size']) {
      $file_thumbs = $dir . "/thumbs/{$w_thumb}_" . $pic_name;
      /*if (!file_exists($file_thumbs)) 
			{
				if (@is_dir($dir."/thumbs")) {
					@chmod($dir."/thumbs",0777);
				} else {
					@mkdir($dir."/thumbs",0777);
					@chmod($dir."/thumbs",0777);
				}		
				// thum hinh
				$func->thum($linkhinh, $file_thumbs, $w_thumb);
			}	*/
      $src = $file_thumbs;
    } else {
      $src = $dir . "/thumbs/" . $pic_name;
    }
  }
  if ($w < $w_thumb)
    $ext = " width='$w' ";
  $out = "<img  src=\"{$src}\" {$ext} >";
  return $out;
}

/*-------------- get_price_pro --------------------*/
/*function get_price_pro ($price,$default="Call"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = sprintf( "%.2f",$price );
		$nguyen = (int) $price;
		$dot =strpos($price,".");
		if ($dot)
			$du =  substr ($price,strpos($price,"."),3);
		else $du = "";
		
		$price = "$".$func->format_number($nguyen).$du ;
	}else{
		$price = $default;
	}
	return $price;
}*/
function get_price_pro ($price, $default = "0")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    $price = $func->format_number($price) . " đ";
  } else {
    $price = $default;
  }
  return $price;
}

/*-------------- get_price_lang --------------------*/
function get_price_lang ($price, $lang = "en", $default = "Call")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    if ($lang == "en") {
      $price = sprintf("%.2f", $price);
      $nguyen = (int) $price;
      $dot = strpos($price, ".");
      if ($dot)
        $du = substr($price, strpos($price, "."), 3);
      else
        $du = "";
      $price = "$" . $func->format_number($nguyen) . $du;
    } else {
      if ($conf['rate'])
        $rate = intval($conf['rate']);
      else
        $rate = 1;
      $price = $price * $conf['rate'];
      $price = $func->format_number($price) . " VNĐ";
    }
  } else {
    $price = $default;
  }
  return $price;
}

//====== List_Direction
function List_Direction ($did)
{
  global $func, $DB, $conf, $vnT;
  $text = "<select size=1 name=\"direction\">";
  if ($did == "ASC")
    $text .= "<option value=\"ASC\" selected>{$vnT->lang['asc']}</option>";
  else
    $text .= "<option value=\"ASC\" >{$vnT->lang['asc']}</option>";
  if ($did == "DESC")
    $text .= "<option value=\"DESC\" selected>{$vnT->lang['desc']}</option>";
  else
    $text .= "<option value=\"DESC\">{$vnT->lang['desc']}</option>";
  $text .= "</select>";
  return $text;
}

//====== Get_Cat
function Get_Cat ($did = -1, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
  $text .= "<option value=\"0\">-- Root --</option>";
  $query = $DB->query("SELECT * FROM product_category WHERE parentid=0 order by cat_order");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub($did, $cat['cat_id'], $n);
  }
  $text .= "</select>";
  return $text;
}

//====== Get_Sub
function Get_Sub ($did, $cid, $n)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT * FROM product_category WHERE parentid={$cid}  order by cat_order");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub($did, $cat['cat_id'], $n);
  }
  return $output;
}

//====== List_SubCat
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM product_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

//====== List_Status_Pro
function List_Status_Pro ($selname, $did, $ext)
{
  global $func, $DB, $conf;
  $text = "<select name=\"{$selname}\"  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Chọn trạng thái --</option>";
  $sql = "SELECT * FROM product_status where display=1 order by s_order ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['status_id'] == $did) {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->HTML($row['title']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->HTML($row['title']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_Country
function List_Country ($did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"country\" id=\"country\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn quốc gia --</option>";
  $sql = "SELECT * FROM iso_countries where display=1 order by name ASC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['iso'] == $did) {
      $text .= "<option value=\"{$row['iso']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['iso']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_City
function List_City ($country, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"city\" id=\"city\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn tỉnh thành --</option>";
  $sql = "SELECT * FROM iso_cities where display=1 and country='$country' order by c_order ASC , name ASC   ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $selected =  ($row['id'] == $did)  ? "selected" : "";
    $text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_State
function List_State ($city, $did = "" , $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"state\" id=\"state\" class='select form-control'  {$ext}   >";
  $text .= "<option value=\"\" selected>Chọn Quận/ Huyện</option>";
  $sql = "SELECT * FROM iso_states where display=1 and city='$city'  order by s_order ASC , name ASC  ";
  $result = $vnT->DB->query($sql);
  while ($row = $vnT->DB->fetch_row($result)) {
    $selected =  ($row['id'] == $did)  ? "selected" : "";
    $text .= "<option value=\"{$row['id']}\" {$selected} >" . $vnT->func->HTML($row['name']) . "</option>";
  }
  $text .= "</select>";
  return $text;
}

/*-------------- get_state_name --------------------*/
function get_state_name ($id)
{
  global $func, $DB, $conf, $vnT;
  $text = $id;
  if($vnT->setting['arr_state'][$id]){
    $text = $vnT->setting['arr_state'][$id] ;
  }else{
    $result = $vnT->DB->query("SELECT name FROM iso_states WHERE id=".$id);
    if ($row = $vnT->DB->fetch_row($result)) {
      $text = $vnT->func->HTML($row['name']);
    }
  }

  return $text;
}


//-----------------  List_Shipping_Method
function List_Shipping_Method ($did = "", $lang, $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"method\" id=\"method\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Chọn phương thức --</option>";
  $sql = "SELECT * FROM shipping_method where display=1 and s_type=1 order by s_order ASC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $title = $func->fetch_content($row['title'], $lang);
    if ($row['name'] == $did) {
      $text .= "<option value=\"{$row['name']}\" selected>" . $title . "</option>";
    } else {
      $text .= "<option value=\"{$row['name']}\">" . $title . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

function List_Display ($did)
{
  global $func, $DB, $conf;
  $output = "<select name=status>";
  if ($did == "0")
    $output .= " <option value=\"0\" selected> Disabled </option>";
  else
    $output .= " <option value=\"0\" > Disabled </option>";
  if ($did == "1")
    $output .= " <option value=\"1\" selected> Enabled </option>";
  else
    $output .= " <option value=\"1\" > Enabled </option>";
  $output .= "</select>";
  return $output;
}

function get_method_name ($table, $name, $lang = "en")
{
  global $DB, $input, $func, $vnT, $conf;
  $text = "";
  $reuslt = $DB->query("select title from {$table} where name='{$name}' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
  }
  return $text;
}

//----------- get_type_price
function get_type_price ($id)
{
  global $func, $DB, $conf;
  $text = "&nbsp;";
  $result = $DB->query("SELECT name FROM product_price WHERE id=$id");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['name'];
  }
  return $text;
}

/******** PROMOTION ************/
function get_promotion_name ($did, $lang)
{
  global $vnT, $conf, $DB, $func;
  $sql = "SELECT * FROM promotions where  pid={$did}";
  $result = $DB->query($sql);
  $title = "";
  if ($row = $DB->fetch_row($result)) {
    $title = $func->fetch_content($row['title'], $lang);
  }
  return $title;
}

function Promotion ($did = "", $lang = "vn", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"pid\" id=\"pid\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Promotion --</option>";
  $sql = "SELECT * FROM promotions where display=1 order by porder ASC,pid DESC";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $title = $func->fetch_content($row['title'], $lang);
    if ($row['pid'] == $did) {
      $text .= "<option value=\"{$row['pid']}\" selected>" . $title . "</option>";
    } else {
      $text .= "<option value=\"{$row['pid']}\">" . $title . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

function List_Status ($did, $ext = "")
{
  global $func, $DB, $conf;
  $output = "<select name=status class='select' {$ext} >";
	$output .= " <option value=\"-1\"  > -- Tất cả   </option>";
  if ($did == "0")
    $output .= " <option value=\"0\" selected> Đăng ký mới   </option>";
  else
    $output .= " <option value=\"0\" >  Đăng ký mới  </option>";
  if ($did == "1")
    $output .= " <option value=\"1\" selected> Đã liên lạc lại </option>";
  else
    $output .= " <option value=\"1\" > Đã liên lạc lại </option>";
  $output .= "</select>";
  return $output;
}

//------list_admin
function list_admin ($did,$ext=""){
  global $func,$DB,$conf,$vnT;

  $text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
  $text .= "<option value=\"\" selected>-- Chọn Nhân Viên --</option>";
  $sql="SELECT adminid,username	FROM admin order by  username ASC ";

  $result = $DB->query ($sql);
  $i=0;
  while ($row = $DB->fetch_row($result))
  {
    $i++;
    $username = $func->HTML($row['username']);
    $selected = ($did==$row['adminid']) ? " selected " : "";
    $text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
  }

  $text.="</select>";
  return $text;
}


//------get_admin
function get_admin($adminid) {
  global $vnT,$func,$DB,$conf;
  $text = "Admin";
  $sql ="select username from admin where adminid =$adminid  ";
  $result =$DB->query ($sql);
  if ($row = $DB->fetch_row($result)){
    $text = $func->HTML($row['username']);
  }
  return $text;
}



// List_Size
function List_Size($list,$name,$ext=""){
  global $func,$DB,$conf,$vnT;

  $result = $DB->query("select * from product_size where size_id in (".$list.") and display=1 order by  display_order ASC, size_id DESC ");

  if($num = $DB->num_rows($res_size))
  {
    $out .= "<select size=1  name='list_size'  class='select' id='size_".$name."'   {$ext}>";
    $out.="<option value=\"\" selected>Chọn size ---</option>";
    while($row	=	$DB->fetch_row($result))
    {

      if ($did == $row['size_id'] ){
        $out.="<option value=\"{$row['size_id']}\" selected>".$row['title']."</option>";
      }else{
        $out.="<option value=\"{$row['size_id']}\">".$row['title']."</option>";
      }
    }
    $out .='</select>';
  }else{
    $out='';
  }

  return $out;
}


//======================= List_Search =======================
function List_Search_pro ($did,$ext="")
{
  global $func, $DB, $conf, $vnT;
  $arr_where = array('p_id'=>  $vnT->lang['id'] ,'maso'=>  $vnT->lang['maso'],'p_name'=> $vnT->lang['product_name'],'price'=> $vnT->lang['price'], 'date_post' => $vnT->lang['date_post']." (d/m/Y)",'description'=> $vnT->lang['description'] );

  $text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
  foreach ($arr_where as $key => $value)
  {
    $selected = ($did==$key) ? "selected"	: "";
    $text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
  }
  $text.="</select>";

  return $text;
}


/*-------------- get_size_name --------------------*/
function get_size_name ($id)
{
  global $func, $DB, $conf, $vnT;
  $text = $id;
  $result = $vnT->DB->query("SELECT title FROM product_size WHERE size_id=".$id);
  if ($row = $vnT->DB->fetch_row($result)) {
    $text = $vnT->func->HTML($row['title']);
  }
  return $text;
}


//----- get_info_address
function  get_info_address_print ($data){
  global	$DB,$input,$func,$vnT,$conf;

    //name
    $text = "<div class='name'> Nhận : <b>".$data['d_name']."</b></div>";

    //address
    $text .= "<div class='address'>Địa chỉ : " .$data['d_address']. "</div>";

    if($data['d_state'])
      $text .= "Quận huyện : ".get_state_name($data['d_state']);
    if($data['d_city'])
      $text .= "<br> Tỉnh thành : ".get_city_name($data['d_city']);

    if($data['d_phone'])
      $text .= "<br>Điện thoại : ".$data['d_phone'];
    if($data['d_email'])
      $text .= "<br>Email : ".$data['d_email'];



  return $text ;
}


//----- get_text_address
function  get_text_address($data){
  global	$DB,$input,$func,$vnT,$conf;



  //address
  $text = $data['d_address'] ;
  if($data['d_state'])
    $text .= ", ".get_state_name($data['d_state']);
  if($data['d_city'])
    $text .= ", ".get_city_name($data['d_city']);

  return $text ;
}



function List_Delivery_Partner ($did = "",  $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"dp_id\" id=\"dp_id\" class='select'  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Chọn đối tác giao hàng --</option>";
  $sql = "SELECT * FROM delivery_partner where display=1 order by display_order ASC, date_post DESC";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $title = $func->HTML($row['title']);
    $selected =  ($row['dp_id'] == $did) ? " selected" : "" ;
    $text .= "<option value=\"{$row['dp_id']}\" ".$selected." >" . $title . "</option>";
  }
  $text .= "</select>";
  return $text;
}


function get_delivery_partner ($id = "" )
{
  global $vnT, $conf, $DB, $func;
  $text = '';
  $sql = "SELECT * FROM delivery_partner where dp_id=".$id;
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['title']);
  }
  return $text;
}



function get_transporter ($city, $state )
{
  global $vnT, $conf, $DB, $func;
  $text = $vnT->setting['transporter_default'];
  if($city){
    $res = $DB->query("SELECT dp_id FROM iso_cities WHERE id=".$city);
    if($r = $DB->fetch_row($res))
    {
      if($r['dp_id']>0){
        $text = $r['dp_id'];
      }
    }
  }

  if($state){
    $res = $DB->query("SELECT dp_id FROM iso_states WHERE id=".$state);
    if($r = $DB->fetch_row($res))
    {
      if($r['dp_id']>0){
        $text = $r['dp_id'];
      }
    }
  }

  return $text;
}

/*------ List_State_Muti ---------*/
function List_State_Muti($city="2",$did="", $ext=""){
  global $func,$DB,$conf;
  if ($did)
    $arr_selected = explode(",",$did);
  else{
    $arr_selected = array();
  }

  $sql="SELECT * FROM iso_states where display=1 and city=".$city."  order by s_order  ASC, name ASC  ";
  $result = $DB->query ($sql);

  $text='<div id="StateSelect" class="ISSelect">';
  $text.='<ul>';
  $i=0;$ngang=0;
  while ($row = $DB->fetch_row($result))
  {
    $i++;
    if (in_array($row['id'],$arr_selected)){
      $checked ="checked";
      $class= "class='select'";
    }else{
      $checked ="";
      $class ="";
    }

    $name = $func->HTML($row['name']);
    $text.='<li '.$class.' ><input name="ckState[]" id="ckState" type="checkbox" value="'.$row['id'].'" '.$checked.'  />&nbsp;'.$name.'</li>';

  }

  $text.='</ul></div>';

  return $text;
}

?>