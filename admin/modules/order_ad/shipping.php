<?php
/*================================================================================*\
|| 							Name code : department.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "order";
  var $action = "shipping";

  function vntModule (){
    global $Template, $vnT, $func, $DB, $conf;
    $this->skin = new XiTemplate(DIR_MODULE.DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;

    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_shipping'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_shipping'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_shipping'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  function do_Add ($lang){
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['r_type1'] = " checked='checked' ";
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $name = $vnT->input['name'];
      $res_lang = $DB->query("select * from language ");
      while ($row_lang = $DB->fetch_row($res_lang)) {
        $arr_title[$row_lang['name']] = $func->txt_HTML($vnT->input['title']);
        $arr_description[$row_lang['name']] = $DB->mySQLSafe($_POST['description']);
      }
      // Check for existed
      $res_chk = $DB->query("SELECT * FROM shipping_method  WHERE name='{$name}'  ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
        // insert CSDL
      if (empty($err)) {
        $cot['name'] = $name;
        $cot['title'] = serialize($arr_title);
        $cot['description'] = serialize($arr_description);
        $cot['price'] = $vnT->input['price'];
        $cot['s_type'] = $vnT->input['s_type'];
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_insert("shipping_method", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['list_display'] = vnT_HTML::list_yesno("display", $vnT->input['display']);
    $data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '300', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Edit ($lang){
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $name = $vnT->input['name'];
      // Check for existed
      $res_chk = $DB->query("SELECT * FROM shipping_method  WHERE name='{$name}' and id<>$id  ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
      if (empty($err)) {
        $cot['name'] = $name;
        $cot['title'] = $func->update_content("shipping_method", "title", "id=$id ", $lang, $vnT->input['title']);
        $cot['description'] = $func->update_content("shipping_method", "description", "id=$id ", $lang, $DB->mySQLSafe($_POST['description']));
        $cot['price'] = $vnT->input['price'];
        $cot['s_type'] = $vnT->input['s_type'];
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_update("shipping_method", $cot, "id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM shipping_method WHERE id=$id");
    if ($data = $DB->fetch_row($query)) {
      $data['title'] = $func->fetch_content($data['title'], $lang);
      $data['description'] = $func->fetch_content($data['description'], $lang);
      if ($data['s_type'] == 1) {
        $data['r_type1'] = " ";
        $data['r_type2'] = " checked='checked' ";
      } else {
        $data['r_type1'] = " checked='checked' ";
        $data['r_type2'] = " ";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '300', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  function do_Del ($lang){
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM shipping_method WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }
  function render_row ($row_info, $lang){
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['s_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $output['name'] = "<a href=\"{$link_edit}\"><strong>" . $row['name'] . "</strong></a>";
    /*if ($row['s_type'] == 1) {
      $output['price'] = "<a href='?mod=order&act=shipping_price'>".$vnT->lang['postal_price']."</a>";
    } else {
      $output['price'] = " <input name=\"txt_Price[{$id}]\" type=\"text\" size=\"10\"  style=\"text-align:center\" value=\"{$row['price']}\"  onchange='javascript:do_check($id)'  /> ";
    }*/

    $title = $func->fetch_content($row['title'], $lang);
    $output['title'] = $row['ext'] . "&nbsp;<strong>" . $title . "</strong>";

    $output['price'] = " <input name=\"txt_Price[{$id}]\" type=\"text\" size=\"10\"  style=\"text-align:center\" value=\"{$row['price']}\"  onchange='javascript:do_check($id)'  /> ";
    if ($row['s_type'] == 1) {
      $output['price'] = " <a href='?mod=country&act=city' target='_blank'>[Cấu hình giá]</a>";
    }
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  function do_Manage ($lang){
    global $vnT, $func, $DB, $conf;
    if (isset($_POST['btnUpdate'])){
      $state_nofree = @implode(",",$_POST['ckState']);
      $up['ship_free'] = $vnT->input['ship_free'] ;
      $up['state_nofree'] = $state_nofree ;
      $ok = $DB->do_update("product_setting",$up,"lang='".$lang."'");
      if ($ok){
        $mess = $vnT->lang["edit_success"];
        $url = $this->linkUrl ;
        $func->html_redirect($url, $mess);
      }
    }
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          if (isset($vnT->input["txt_Price"]))
            $arr_price = $vnT->input["txt_Price"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['s_order'] = $arr_order[$h_id[$i]];
            $dup['price'] = $arr_price[$h_id[$i]];
            $ok = $DB->do_update("shipping_method", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("shipping_method", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("shipping_method", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		if((int)$vnT->input["do_display"]) {
			$ok = $DB->query("Update shipping_method SET display=1 WHERE id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {
			$ok = $DB->query("Update shipping_method SET display=0 WHERE id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ?  $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT id FROM shipping_method  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)      $p = $num_pages;
    if ($p < 1)      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'name' => $vnT->lang['name'] . " |15%|center" , 
      'title' => $vnT->lang['title'] . " ||left" ,
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM shipping_method  ORDER BY  s_order ASC, id DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_shipping_method'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;

    $data['ship_free'] = $vnT->setting['ship_free'];
    $data['list_state_nofree'] = $this->List_State_Muti(2,$vnT->setting['state_nofree']);

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
}

$vntModule = new vntModule();
?>
