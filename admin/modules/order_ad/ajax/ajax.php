<?php
@ini_set("display_errors", "0");
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");

if ( !defined('COOKIEPATH') )
	define('COOKIEPATH', preg_replace('|https?://[^/]+|i', '', $conf['rooturl'] ) );
if ( !defined('ADMIN_COOKIE_PATH') )
	define( 'ADMIN_COOKIE_PATH', COOKIEPATH . 'admin' );
if ( !defined('COOKIEHASH') )
	define('COOKIEHASH', md5($conf['rooturl']));
if ( !defined('AUTH_COOKIE') )
	define('AUTH_COOKIE', 'vnt_admin_'.COOKIEHASH);
if ( !defined('LOGGED_IN_COOKIE') )
	define('LOGGED_IN_COOKIE', 'vnt_login_'.COOKIEHASH);


include ($conf['rootpath'] . "includes/class_db.php");
$vnT->DB = $DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$vnT->func = $func  = new Func_Admin;
$vnT->conf = $conf = $func->fetchDbConfig($conf);
//load mailer
require_once($conf['rootpath']."libraries/phpmailer/phpmailer.php");
$vnT->mailer = new PHPMailer();

$go_login = 1;
$arr_auth = explode("|",$_COOKIE[LOGGED_IN_COOKIE]);
$res_ck = $DB->query("SELECT * FROM admin WHERE username='".$arr_auth[0]."' ");
if($row_ck = $DB->fetch_row($res_ck))
{
	$auth_hash = md5($row_ck['adminid'] . '|' . $row_ck['password']);
	if($arr_auth[1] ==$auth_hash )
	{
		$vnT->admininfo = $row_ck;
		$go_login=0;
	}
}
if($go_login==1) {die('Hacking attempt!');}


require_once($conf['rootpath'] ."/includes/JSON.php");
$vnT->lang_name = ($_POST['lang']) ? $_POST['lang'] : "vn";

$res_s = $DB->query("SELECT * FROM order_status WHERE display=1 ORDER BY s_order ASC");
while($row_s = $DB->fetch_row($res_s))
{
	if($row_s['is_default']==1)	 {
		$vnT->setting['status_default']	= $row_s['status_id'];
	}
	if($row_s['is_payment']==1)	 {
		$vnT->setting['status_payment']	= $row_s['status_id'];
	}

	if($row_s['is_complete']==1)	 {
		$vnT->setting['status_complete']	= $row_s['status_id'];
	}

	$vnT->setting['status'][$row_s['status_id']] =  "<b style=color:#".$row_s['color']." >" . $func->fetch_content($row_s['title'], $vnT->lang_name) . "</b>";

}

$res_ad = $DB->query("SELECT adminid,username FROM admin  ORDER BY adminid ASC") ;
while($row_ad = $DB->fetch_row($res_ad)) {
	$vnT->setting['arr_admin'][$row_ad['adminid']] = $row_ad['username'];
}

switch ($_GET['do']) {
	case "updateOrderAdmin" :    $jsout = do_updateOrderAdmin();  break;
	case "updateOrderStatus":    $jsout = do_updateOrderStatus();  break;
	case "updateCreditStatus":    $jsout = do_updateCreditStatus();  break;
	case "updateDKNStatus":    $jsout = do_updateDKNStatus();  break;
	case "order_phone":    $jsout = do_OrderPhone();  break;

	case "addNote":    $jsout = do_addNote();  break;

  default:    $jsout = "Error";  break;
}
$vnT->DB->close();



//do_updateOrderAdmin
function do_updateOrderAdmin()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$oid = (int)$_POST['oid'];
	$adminid =(int)$_POST['adminid'];
	$arr_json['ok'] = 0;
	$res_ck = $DB->query("SELECT order_id FROM order_sum WHERE order_id=".$oid);
	if($row_ck = $DB->fetch_row($res_ck))
	{

		$cot['poster'] = $adminid;

		if(empty ($err))
		{
			$ok  = $DB->do_update("order_sum",$cot," order_id=".$oid);
			if($ok) {
				$arr_json['ok'] = 1 ;
				$arr_json['html'] =  $vnT->setting['arr_admin'][$adminid];
				$arr_json['mess'] = "NV phụ trách của HĐ order <b class='font_err'>#".$oid."</b> đã được cập nhật thành : ".$vnT->setting['arr_admin'][$adminid] ;
			}

		}else{
			$arr_json['mess'] = $err ;
		}
	}else{
		$arr_json['mess'] = "Order : ".$oid." Not Found ... " ;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}


//do_updateOrderStatus
function do_updateOrderStatus()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$oid = (int)$_POST['oid'];
	$sid =(int)$_POST['sid'];
 	$arr_json['ok'] = 0;
	$res_ck = $DB->query("SELECT * FROM order_sum WHERE order_id=".$oid);
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$up_status = ($row_ck['status']+1) ;
		$cot['status'] = $sid;
		if($vnT->setting['status_payment']  == $sid) {
			$cot['poster'] = $vnT->admininfo['adminid'];
		}

		if(empty ($err))
		{
			$ok  = $DB->do_update("order_sum",$cot," order_id=".$oid);
			if($ok) {


				//insert history
				$cot_h['order_id'] = $oid;
				$cot_h['status'] = $sid;
				$cot_h['status_old'] = $row_ck['status'];
				$cot_h['note'] = "Cập nhật trạng thái HĐ từ ".$vnT->setting['status'][$row_ck['status']]." sang ".$vnT->setting['status'][$sid];

				$cot_h['adminid'] = $vnT->admininfo['adminid'] ;
				$cot_h['admin_name'] = $vnT->admininfo['username'] ;
				$cot_h['date_submit'] = time();
				$DB->do_insert("order_history",$cot_h);


				$arr_json['ok'] = 1 ;
				$arr_json['html'] =  $vnT->setting['status'][$sid];
				$arr_json['mess'] = "Trạng thái của HĐ order <b class='font_err'>#".$oid."</b> đã được cập nhật thành : ".$vnT->setting['status'][$sid] ;
			}

		}else{
			$arr_json['mess'] = $err ;
		}
	}else{
		$arr_json['mess'] = "Order : ".$oid." Not Found ... " ;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}



//do_updateCreditStatus
function do_updateCreditStatus()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$oid = (int)$_POST['oid'];
	$sid =(int)$_POST['sid'];
 	$arr_json['ok'] = 0;
	$res_ck = $DB->query("SELECT * FROM tragop WHERE tragop_id=".$oid);
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$up_status = ($row_ck['status']+1) ;
		$cot['status'] = $sid;

		if(empty ($err))
		{
			$ok  = $DB->do_update("tragop",$cot,"tragop_id=".$oid);
			if($ok) {
				$arr_json['ok'] = 1 ;
				$arr_json['html'] =  $vnT->setting['status'][$sid];
				$arr_json['mess'] = "Trạng thái của HĐ order <b class='font_err'>#".$oid."</b> đã được cập nhật thành : ".$vnT->setting['status'][$sid] ;
			}

		}else{
			$arr_json['mess'] = $err ;
		}
	}else{
		$arr_json['mess'] = "Order : ".$oid." Not Found ... " ;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}


//do_updateDKNStatus
function do_updateDKNStatus()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$oid = (int)$_POST['oid'];
	$sid =(int)$_POST['sid'];

 	$arr_json['ok'] = 0;
	$res_ck = $DB->query("SELECT * FROM order_quicks WHERE id=".$oid);
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$up_status = ($row_ck['status']+1) ;
		$cot['status'] = $sid;
		if(empty ($err))
		{
			$ok  = $DB->do_update("order_quicks",$cot," id=".$oid);
			if($ok) {
				$arr_json['ok'] = 1 ;
				$arr_json['html'] =  $vnT->setting['status'][$sid];
				$arr_json['mess'] = "Trạng thái của HĐ order <b class='font_err'>#".$oid."</b> đã được cập nhật thành : ".$vnT->setting['status'][$sid] ;
			}
		}else{
			$arr_json['mess'] = $err ;
		}
	}else{
		$arr_json['mess'] = "Order : ".$oid." Not Found ... " ;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}



//do_OrderPhone
function do_OrderPhone()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$phone = $_POST['phone'];
	$arr_json['ok'] = 0;

	$arr_phone = @explode(",",$phone);
	$where = " AND (" ;
	$i=0;
	foreach ($arr_phone as $val)
	{
		if($i>0){ $where .= " OR " ; }
		$where .= " d_phone='".$val."' ";
		$i++;
	}
	$where .= " )";

	$arr_out = array();
	$res_ck = $DB->query("SELECT order_id,d_phone FROM order_sum WHERE order_id>0  ".$where ." ORDER BY d_phone ASC");
	while($row_ck = $DB->fetch_row($res_ck)){
		$arr_out[trim($row_ck['d_phone'])] += 1 ;
	}
	$arr_json = $arr_out ;
	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}



//do_addNote
function do_addNote()
{
	global $DB,$func,$conf,$vnT;
	$arr_json = array();
	$id = (int)$_POST['id'];
	$note_order = $_POST['note_order'];

	$arr_json['ok'] = 0;
	$res_ck = $DB->query("SELECT order_id, note_order FROM order_sum WHERE order_id=".$id);
	if($row_ck = $DB->fetch_row($res_ck))
	{
		$cot['note_order'] = $func->txt_HTML($note_order);
		$ok = $DB->do_update("order_sum",$cot,"order_id=".$id);
		if($ok) {
			$arr_json['ok'] = 1 ;
			$arr_json['mess'] = "Cập nhật ghi chú thành công" ;
		}

	}else{
		$arr_json['mess'] = "Order Item : ".$id." Not Found ... " ;
	}

	$json = new Services_JSON( );
	$textout = $json->encode($arr_json);

	return $textout;
}


flush();
echo $jsout;
exit();



?>
