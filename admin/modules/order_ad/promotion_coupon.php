<?php
/*================================================================================*\
|| 							Name code : status.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
//load Model
include(dirname(__FILE__) . "/functions.php");

class vntModule extends Model
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "order";
  var $action = "promotion_coupon";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign("DIR_JS", $vnT->dir_js);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;

    $vnT->html->addStyleSheet( "modules/".$this->module."_ad/css/" . $this->module . ".css");
    $vnT->html->addScript("modules/".$this->module."_ad/js/" . $this->module . ".js");

    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");
    $vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('.dates').datepicker({
						showOn: 'both',
						buttonImage: '" . $vnT->dir_images . "/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					}); 

			});
		
		");

    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = "Thêm coupon mới";
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = "Cập nhật coupon ";
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'import':
        $nd['f_title'] = "Import mã coupon";
        $nd['content'] = $this->do_Import($lang);
        break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = "Quản lý coupon khuyến mãi";
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
	
	function check_exist_code ($code)
	{
		global $DB,$conf,$func ;
		//check
		$res_ck = $DB->query("SELECT id FROM promotion_coupon WHERE code='".$code."' ");
		if($res_ck = $DB->fetch_row($res_ck))
		{
			return 1;		
		}else{
			return 0;
		}			
	}

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;

    $err = "";
		$data['display'] = 1;
		$data['num_code'] = 10 ;
		
		$data['day_start'] = date("d/m/Y");
    $data['day_end'] = date("d/m/Y", (time() + 30 * 24 * 3600));
    $data['price_type'] = "VND";

    if ($vnT->input['do_submit'] == 1) 
		{
      $data = $_POST;

			$num_code = (int)$_POST['num_code'] ;

      $chk_time = $vnT->input['chk_time'];
      if ($chk_time) {
        $quantity = $vnT->input['quantity'];
        $day_start = "";
        $day_end = "";
        $vnT->input['display'] = 1;
      } else {
        $quantity = "";

        $dayone = @explode("/", $vnT->input['day_start']);
        $daystart = @mktime(0, 0, 0, $dayone[1], $dayone[0], $dayone[2]);
        $daytwo = @explode("/", $vnT->input['day_end']);
        $day_end = @mktime(23, 59, 59, $daytwo[1], $daytwo[0], $daytwo[2]);
      }
			
			$ok_insert = 0 ; 
			for($n=0 ; $n<$num_code ; $n++)
			{
				 
				$code = strtoupper($func->m_random_str(9)); 
				$i=0; 
				while($this->check_exist_code($code))
				{
					$i++; 
					$code = strtoupper($func->m_random_str(9)); 
					if($i>20) {
						die('Server qua tai');
						break ;					
					}
				}			 
				
				// insert CSDL			 
				$cot['pid'] = $vnT->input['pid'];
				$cot['code'] = strtoupper($code);
				$cot['price'] = trim($vnT->input['price']);
				$cot['price_type'] = trim($vnT->input['price_type']);
				$cot['expired'] = $chk_time;
				$cot['quantity'] = $quantity;
				$cot['day_start'] = $daystart;
				$cot['day_end'] = $day_end;
				$cot['display'] = $vnT->input['display'];
				$cot['datesubmit'] = time();
				$ok = $DB->do_insert("promotion_coupon", $cot);
				if($ok) {
					$ok_insert = 1 ;	
				}
			 
				
			}//end for
			
			
			if($ok_insert){
					//xoa cache
				$func->clear_cache();
				//insert adminlog
				$func->insertlog("Add", $_GET['act'] );
				$mess = $vnT->lang['add_success'];
				$url = $this->linkUrl . "&sub=add";
				$func->html_redirect($url, $mess);	
			}else{
				$err = $func->html_err($vnT->lang['add_failt'] );
			}
     
    }
    $data['price_type'] = vnT_HTML::selectbox("price_type", array(
      '%' => '%' , 
      'VND' => 'VNĐ'), $data['price_type']);
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $data['list_promotion'] = Promotion($vnT->input['pid'], $lang);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("add");
    return $this->skin->text("add");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;

    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $code = trim($vnT->input['code']);
      $res_ck = $vnT->DB->query("SELECT id FROM promotion_coupon WHERE code='".$code."' AND id<>".$id);
      if($vnT->DB->num_rows($res_ck)){
        $err =  $func->html_err("Mã <b>".$code ."</b> đã tồn tại");
      }


      $chk_time = $vnT->input['chk_time'];
      if ($chk_time) {
        $quantity = $vnT->input['quantity'];
        $day_start = "";
        $day_end = "";
        $vnT->input['display'] = 1;
      } else {
        $quantity = "";

        $dayone = @explode("/", $vnT->input['day_start']);
        $day_start = @mktime(0, 0, 0, $dayone[1], $dayone[0], $dayone[2]);
        $daytwo = @explode("/", $vnT->input['day_end']);
        $day_end = @mktime(23, 59, 59, $daytwo[1], $daytwo[0], $daytwo[2]);
      }

      if (empty($err)) {
        $cot['pid'] = $vnT->input['pid'];
        $cot['code'] = $code;
        $cot['price'] = trim($vnT->input['price']);
        $cot['price_type'] = trim($vnT->input['price_type']);
        $cot['expired'] = $chk_time;
        $cot['quantity'] = $quantity;

        $cot['day_start'] = $day_start;
        $cot['day_end'] = $day_end;
				$cot['display'] = $vnT->input['display'] ;
        // more here ...
        $ok = $DB->do_update("promotion_coupon", $cot, "id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM promotion_coupon WHERE id=$id");
    if ($data = $DB->fetch_row($query)) {
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['price_type'] = vnT_HTML::selectbox("price_type", array(
      '%' => '%' , 
      'VND' => 'VNĐ'), $data['price_type']);

    $data['day_start'] = @date('d/m/Y', $data['day_start']);
    $data['day_end'] = @date('d/m/Y', $data['day_end']);

    $data['checked'][$data['expired']] = " checked";

		$data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
		$data['list_promotion'] = Promotion($data['pid'], $lang);
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Import()
   **/
  function do_Import ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err="";




    if(isset($vnT->input['do_submit']))
    {
      $err="";
      $data = $_POST;


      /*echo"<pre>";
      print_r($_FILES);
      echo"</pre>";*/

      $xls_file = $_POST['xls_file'] ;
      if(empty($xls_file)){
        $err = $func->html_err("Vui lòng chọn file upload");
      }


      if(empty($err))
      {

        include ('../libraries/excel/PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $filename="../vnt_upload/File/".$xls_file;
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  **/
        $objPHPExcel = $objReader->load($filename);

        $total_sheets=$objPHPExcel->getSheetCount();

        $allSheetName=$objPHPExcel->getSheetNames();
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        $colname=array(
          0 => 'code',
          1 => 'price',
          2 => 'day_start',
          3 => 'day_end'
        );



        $cot_tmp = array();

        for ($row = 2; $row <= $highestRow;++$row)
        {
          for ($col = 0; $col <$highestColumnIndex;++$col)
          {
            $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
            $cot_tmp[$row-1][$colname[$col]]=$value;
          }

        }

        /*echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo"<pre>";
        print_r($cot_tmp);
        echo"</pre>";
        die();
*/

        foreach ($cot_tmp as $value )
        {
          $cot = array();
          $code = trim( str_replace(" ","",$value['code']));
          $price = trim( str_replace(" ","",$value['price']));

          $tmp = explode("/", $value['day_start']);
          $day_start = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
          $tmp1 = explode("/", $value['day_end']);
          $day_end = mktime(23, 59, 59, $tmp1[1], $tmp1[0], $tmp1[2]);

          if($code)
          {
            //check
            $res_ck = $DB->query("SELECT id FROM promotion_coupon WHERE code='".$code."' " )	;
            if(!$row_ck = $DB->fetch_row($res_ck))
            {
              $cot['code'] = $code ;
              $cot['price'] = $price ;
              $cot['price_type'] = "VND";
              $cot['day_start'] = ($day_start) ?  $day_start :  time() ;
              $cot['day_end'] = $day_end ;
              $cot['datesubmit'] = time() ;
              $cot['expired'] = 0 ;
              $cot['status'] = 0;
              $cot['display'] = 1;
              $ok = $DB->do_insert("promotion_coupon",$cot);
              //echo $DB->debug();
            }
          }

          /*echo"<pre>";
          print_r($cot);
          echo"</pre>";*/

        }

        //del file
        if (file_exists($filename)) {
          @unlink($filename);
        }

        //xoa cache
        $func->clear_cache();
        //insert adminlog
        $func->insertlog("Import Excel", $_GET['act'], $xls_file );

        $mess = $vnT->lang['add_success'];
        $url = $this->linkUrl ;
        $func->html_redirect($url, $mess);
        $err = $vnT->lang['add_success'];
      }
    }


    $folder_upload = $vnT->conf['rootpath'].'vnt_upload/File' ;
    $data['folder_upload'] = $folder_upload;
    $data['dir_upload'] = "";

    $data['err'] = $err;

    $data['link_action'] = $this->linkUrl."&sub=import" ;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("html_import");
    return $this->skin->text("html_import");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM promotion_coupon WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['displayorder']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
     
	  $output['code'] = "<b class=font_err>" . $row['code'] . "</b>";
    $output['price'] = $row['price'] . " " . $row['price_type'];



    if ($row['expired'] == 1) {
      $infomation = "Số lượng:  <strong>".$row['quantity']."</strong>"	;
      if($row['quantity']==0){
        $infomation = "Số lượng:  <strong>Không giới hạn</strong>"	;
      }
      $infomation .= "<div>SL đã dùng :  <strong>".$row['quantity_now']."</strong></div>"	;
    } else {
      $infomation = "Ngày bắt đầu :  <strong>".@date("d/m/Y",$row['day_start'])."</strong>"	;
      $infomation .= "<div>".$vnT->lang['date_expire'] ." :  <strong>".@date("d/m/Y",$row['day_end'])."</strong></div>"	;
    }



 		$output['infomation'] = $infomation ;

    if ($row['expired'] == 1) { //so luong

      if($row['quantity_now']>= $row['quantity'] && $row['quantity']>0) {
        $text_status = "<b class='red'>Đã hết SL</b>" ;
      }else{
        $text_status =  ($row['status']) ? "<b class='red'>Đã dùng (".$row['quantity_now'].")</b>" : '<b>Chưa dùng</b>';
      }

    }else{ // neu thoi gian
      if($row['day_end'] < time() )	{
        $text_status = "<b class='red'>Đã hết hạn</b>" ;
      }else{
        $text_status = ($row['status']) ? "<b class='red'>Đã dùng (".$row['quantity_now'].")</b>" : "<b>Chưa dùng</b>"	;
      }
    }


		
		
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
			
			$text_status =  "<b>Đã khóa</b>"	 ; 
    }
		
		$output['status'] = $text_status ;
    
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;


    if(isset($_POST['btnAdd'])){

      $arr_promotion_limit = array();

       foreach ($_POST['price_text'] as $key => $price_text){
         $arr_promotion_limit[$price_text] = $_POST['num_coupon'][$key];
       }
      $promotion_limit = serialize($arr_promotion_limit) ;
      $res_s = $vnT->DB->query("UPDATE product_setting SET promotion_limit='".$promotion_limit."' , promotion_max=". (int)$_POST['num_max']." WHERE lang='$lang'");
      $err = $func->html_mess("Cập nhật thành công ");

    }

    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['displayorder'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("promotion_coupon", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("promotion_coupon", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("promotion_coupon", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update promotion_coupon SET display=1 WHERE id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update promotion_coupon SET display=0 WHERE id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $pid = ((int) $vnT->input['pid']) ? $pid = $vnT->input['pid'] : 0;
		$keyword =  ( $vnT->input['keyword']) ? $vnT->input['keyword'] : "";
    $where = '';
		if ($pid) {
      $where .= " AND pid=$pid ";
      $ext .= "&pid=$pid";
    }
		if($keyword) {
			$where .= " AND code like '%".$keyword."%' ";
      $ext .= "&keyword=$keyword";	
		}
		
    $query = $DB->query("SELECT id FROM promotion_coupon WHERE id>0 $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)      $p = $num_pages;
    if ($p < 1)   $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'code' => "Code |15%|center" , 
      'price' => $vnT->lang['promotion_price'] ." |15%|center" ,
			'infomation' => "Thông tin ||left" ,
			'status' =>  "Tình trạng|13%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM promotion_coupon WHERE id>0 $where	ORDER BY  datesubmit DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
				$row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err > Chưa có coupon nào</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;

    // lay setting
    $res_s = $vnT->DB->query("SELECT promotion_limit , promotion_max FROM product_setting WHERE lang='$lang'");
    $row_s = $vnT->DB->fetch_row($res_s);
    if($row_s['promotion_limit'])
    {
      $arr_promotion_limit = unserialize($row_s['promotion_limit']);

      foreach ($arr_promotion_limit as $price_coupon => $num_coupon){

        $row_limit['price_coupon'] = $price_coupon;
        $row_limit['num_coupon'] = $num_coupon;
        $this->skin->assign('row', $row_limit);
        $this->skin->parse("manage.row_limit");

      }

      $data['num_max'] = (int)$row_s['promotion_max'];
    }


    $data['list_promotion'] = Promotion($pid, $lang, "onChange='submit();'");
		$data['keyword'] = $keyword;
    $data['err'] = $err;
    $data['nav'] = $nav;

    $data['upload_excel'] =  $this->linkUrl ."&sub=import&lang=".$lang.$ext;

    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
$vntModule = new vntModule();
?>