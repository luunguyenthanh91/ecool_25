<?php
/*================================================================================*\
|| 							Name code : funtions_activity.php 		 			     	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/service/');
define('ROOT_UPLOAD', 'vnt_upload/service/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/service/');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang)
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from service_setting WHERE lang='$lang' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}

/*** Ham Get_Cat ****/
function Get_Cat ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT  * FROM service_category 
					WHERE  lang='$lang' 
					AND parentid=0 
					order by cat_order ASC , cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub ($did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT * FROM service_category  
					WHERE lang='$lang' 
					AND parentid={$cid}
					order by cat_order ASC , cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  return $output;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT * FROM service_category  
					WHERE lang='$lang' 
					AND  parentid=0 
					order by cat_order ASC , cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n,$lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
	
 
  $query = $DB->query("SELECT * FROM service_category 
					WHERE  lang='$lang' 
					AND  parentid={$cid}
					order by cat_order ASC , cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  return $output;
}

/***** Ham List_SubCat *****/
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf;
  $output = "";
  $query = $DB->query("SELECT * FROM service_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

/***** Ham get_cat_list *****/
function get_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT cat_id,cat_code FROM service_category where cat_id in (".$cat_id.") ");
	while ($row=$DB->fetch_row($res)) 
	{
		$tmp = @explode("_",$row['cat_code']);
		$arr_cat = array_merge($arr_cat,$tmp) ;
	}
	$arr_cat = array_unique($arr_cat);
	$out = @implode(",",$arr_cat);
	
	return $out;	
}

/***** Ham rebuild_cat_list *****/
function rebuild_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT service_id,cat_id FROM service WHERE  FIND_IN_SET('$cat_id',cat_list)<>0 ");
	while($row = $DB->fetch_row($res))
	{
		$DB->query("UPDATE service SET cat_list='".get_cat_list($row['cat_id'])."' WHERE service_id=".$row['service_id'])	;
	}	
	return false;	
}

/***** Ham rebuild_cat_code *****/
function rebuild_cat_code ($parentid,$cat_id){
	global $vnT,$func,$DB,$conf;
	
	$cat_code = get_catCode($parentid,$cat_id) ;
	$DB->query("UPDATE service_category SET cat_code='".$cat_code."' WHERE cat_id=".$cat_id)	;
 	
	$res_chid = $DB->query("SELECT cat_id FROM service_category WHERE  parentid=".$cat_id);
	while($row_chid = $DB->fetch_row($res_chid))
	{		
		rebuild_cat_code($cat_id,$row_chid['cat_id']);
	} 
	
	return false;	
}



//---------------- get_service_name
function get_service_name ($id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("select title from service_desc  where service_id=$id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $title = $func->HTML($row['title']);
  }
  return $title;
}

/*** list_service ****/
function list_service ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"service_id\" name=\"service_id\" {$ext} >";
  $text .= "<option value=\"\">-- Chọn dịch vụ --</option>";
  $query = $DB->query("SELECT n.* ,nd.title
					FROM service n, service_desc nd
					WHERE  n.service_id=nd.service_id
					AND lang='$lang' 
					AND  display=1
					AND parentid=0
					order by s_order, date_post DESC");
  while ($row = $DB->fetch_row($query)) {
    if ($row['service_id'] == $did)
      $text .= "<option value=\"{$row['service_id']}\" selected>" . $func->HTML($row['title']) . "</option>";
    else
      $text .= "<option value=\"{$row['service_id']}\" >" . $func->HTML($row['title']) . "</option>";
  }
  $text .= "</select>";
  return $text;
}

function List_Status ($did)
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"status\">";
  if ($did == "0")
    $text .= "<option value=\"0\" selected> Liên hệ mới </option>";
  else
    $text .= "<option value=\"0\" > Liên hệ mới </option>";
  if ($did == "1")
    $text .= "<option value=\"1\" selected> Đã xác nhận </option>";
  else
    $text .= "<option value=\"1\"> Đã xác nhận </option>";
  if ($did == "2")
    $text .= "<option value=\"2\" selected> Hủy bỏ </option>";
  else
    $text .= "<option value=\"2\"> Hủy bỏ </option>";
  $text .= "</select>";
  return $text;
}

//======================= List_Search =======================
function List_Search ($did,$ext="")
{
  global $func, $DB, $conf, $vnT;
	$arr_where = array('service_id'=> ' ID','title'=>  $vnT->lang['title'],'short'=> $vnT->lang['short_service'], 'date_post' => $vnT->lang['date_post'] );
	
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}
//------list_admin
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;

	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- Chọn Admin --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result))
	{
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	
	$text.="</select>";
	return $text;
}

//------get_admin
function get_admin($adminid) {
global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}



///================== replace_img =============
function replace_img ($str, $dir)
{
  global $conf, $func, $vnT;
  $data['result'] = $tmp = $str;
  $data['url'] = "";
  $ok_getimg = 1;
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  if ($dir) {
    $path_dir = MOD_DIR_UPLOAD . $dir . "/";
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD . $dir . "/";
  } else {
    $path_dir = MOD_DIR_UPLOAD;
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD;
  }
  $path_thumb = $path_dir . "thumbs";
  if (! is_dir($path_thumb)) {
    @mkdir($path_thumb, 0777);
    @exec("chmod 777 {$path_thumb}");
  }
  while ($start = strpos($tmp, "src=")) {
    $end = strpos($tmp, '"', $start + 7);
    $http = substr($tmp, $start + 6, ($end - ($start + 7)));
    if ( !strstr($http, $conf['rooturl']) && (strstr($http, "http://") || strstr($http, "https://")) ) {
      // upload
      $fext = strtolower(substr($http, strrpos($http, ".") + 1));
      if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp")) {
        $lastx = strrpos($http, "/");
        $fname = $path_dir . substr($http, $lastx + 1);
        $fname = str_replace("%20", "_", $fname);
        $fname = str_replace(" ", "_", $fname);
        $fname = str_replace("&amp;", "_", $fname);
        $fname = str_replace("&", "_", $fname);
        $fname = str_replace(";", "_", $fname);
        if (file_exists($fname)) {
          $fname = $path_dir . time() . "_" . substr($http, $lastx + 1);
        }
        $file_name = substr($fname, strrpos($fname, "/") + 1);
        $file = @fopen($fname, "w");
        if ($f = @fopen($http, "r")) {
          while (! @feof($f)) {
            @fwrite($file, fread($f, 1024));
          }
          @fclose($f);
          @fclose($file);
          $url = $rooturl . $file_name; 
          /*if ($ok_getimg ==1){
            $data['url'] = $dir . "/" . $file_name;
            $file_thumb = $path_thumb . "/" . $file_name;         
            $func->thum( 'http://' . $_SERVER['HTTP_HOST'].$url, $file_thumb, $w_thumb);
            $ok_getimg = 0;
          }*/
          $data['result'] = str_replace($http, $url, $data['result']);
        } else
          $data['err'] = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
      } else
        $data['err'] = "Image Type Not Support";
    } //end if strstr
    $tmp = substr($tmp, $end + 1);
  } // end while
  return $data;
}

 
//-----------------  get_pic_thumb
function get_pic_thumb ($picture, $w = "")
{
  global $conf, $vnT;
  $out = "";
  if (! empty($picture)) {
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $linkhinh = "../vnt_upload/service/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    $src = $dir . "/thumbs/" . $pic_name;     
    if ($w < $w_thumb)
      $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
  } else {
    $out = "No Picture";
  }
  return $out;
}

//------do_DeleteSub
function do_DeleteSub($ids,$lang="vn")
{
	global $func, $DB, $conf, $vnT;
	$res = $DB->query("SELECT cat_id,parentid FROM service_category WHERE parentid in (".$ids.")");
	while($row = $DB->fetch_row($res))
	{
		do_DeleteSub($row['cat_id'],$lang);
		
		$res_d = $DB->query("SELECT id FROM service_category_desc WHERE cat_id=".$row['cat_id']." AND lang<>'".$lang."' ");
		if(!$DB->num_rows($res_d)){
			$DB->query("DELETE FROM service_category WHERE  cat_id=".$row['cat_id'] ); 
		}	
		$DB->query("DELETE FROM service_category_desc WHERE  cat_id=".$row['cat_id']." AND lang='".$lang."' ");					
	}
	
}

?>