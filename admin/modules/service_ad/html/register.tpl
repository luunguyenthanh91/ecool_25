<!-- BEGIN: edit -->
<style >
table.table_form {
	background-color:#E7E7E7;
	border-spacing:1px;
	color:#666666;
	width:100%;
}
.table_form td  {
	border:1px solid #FFFFFF;
	height:25px;
	padding:3px;
}

.table_form .col1  {
	background:#FAFAFA;
	font-weight:bold;
	background:#F9F9F9;
	border-top:1px solid #FFFFFF;
}
.table_form .col1 span {
	font-weight:normal;
}

.table_form .col2  {
	background:#FFFFFF;
	border-top:1px solid #FFFFFF;
}
</style>
<br />
<p align="center"><strong class="font_err">{data.type_name} </strong></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td><table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td ><fieldset>
          <legend class="font_err">&nbsp;<strong>THÔNG TIN KHÁCH HÀNG</strong>&nbsp;</legend>
           <table width="100%" border="0" cellspacing="3" cellpadding="3">
      <tr>
				<td class="row0">Tổ chức và cá nhân :</td>
				<td class="row1">{data.company}</td>
			</tr>
      
			<tr>
				<td width="20%" class="row0">Họ tên : </td>
				<td class="row1">{data.full_name}</td>
			</tr>
      <tr>
				<td class="row0">Giới tính :  </td>
				<td class="row1">{data.gender}</td>
			</tr>
      
			<tr>
				<td class="row0">Ngày sinh : </td>
				<td class="row1">{data.birthday}</td>
			</tr>			
			<tr>
				<td class="row0">Địa chỉ :  </td>
				<td class="row1">{data.address}</td>
			</tr> 
      <tr>
				<td class="row0">Thành phố :  </td>
				<td class="row1">{data.city}</td>
			</tr>
			<tr>
				<td class="row0">Điện thoại : </td>
				<td class="row1">{data.phone}</td>
			</tr> 
      <tr>
				<td class="row0">Email :</td>
				<td class="row1">{data.email}</td>
			</tr>  
      
    </table>
          </fieldset></td>
        </tr>
      </table>
      
      
    <br>
    
    <table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td ><fieldset>
          <legend class="font_err">&nbsp;<strong>THÔNG TIN ĐĂNG KÝ</strong>&nbsp;</legend>
           <table width="100%" border="0" cellspacing="3" cellpadding="3">
       <tr>
				<td class="row0"  width="20%">Mẫu xe  : </td>
				<td class="row1">{data.product}</td>
			</tr>  
     
      
      {data.option_more}
      
      <tr>
				<td class="row0">Có thể liên lạc theo :  </td>
				<td class="row1">{data.contact_by}</td>
			</tr>
      
       <tr >
				<td class="row0">Ghi chú :  </td>
				<td class="row1">{data.note}</td>
			</tr>
      <tr >
				<td class="row0">Ngày đăng ký :  </td>
				<td class="row1">{data.date_post}</td>
			</tr>
           
      
    </table>
          </fieldset></td>
        </tr>
      </table>
       
      
    </td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="border-bottom:2px solid #FF0000" height="30"><strong class="font_err">Modify </strong></td>
  </tr>
  <tr>
    <td><form action="{data.link_action}" method="post">
       <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
        <tr>
          <td width="30%"  colspan="2" align="center"><strong><font color="#FF0000">{data.msg}</font></strong> </td>
        </tr>
        <tr>
          <td width="30%" align="right" class="row1"><strong>Ngày cập nhật  :</strong> </td>
          <td width="70%" align="left" class="row0"><input type="text" name="date_update" id="date_update" value="{data.date_update}" maxlength="10">
            &nbsp; <font color="#FF0000"> (dd/mm/Y)</font></td>
        </tr>
        <tr >
          <td align="right" class="row1"><strong>Trạng thái : </strong></td>
          <td align="left" class="row0">{data.list_status}</td>
        </tr>

        <tr>
        	<td align="right" class="row1">&nbsp;</td>
          <td  class="row0"><input type="submit" name="btnSave" value=" Save " class="button">
          </td>
        </tr>
      </table>
    </form></td>
  </tr>
</table>
<br>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
    <td ><strong>Trạng thái:</strong> &nbsp;</td>
    <td ><b class="font_err">{data.list_status}</b></td>
  </tr>
 <tr>
    <td class="row1" ><strong>Đăng ký từ ngày :</strong>  </td>
    <td align="left" class="row0"> 
    <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"    /> &nbsp;&nbsp; <strong>đến :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   /> </td>
  </tr> 
  <tr>
  
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
  
  <tr>
    <td width="15%"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" ><b class="font_err">{data.totals}</b></td>
  </tr>

</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->