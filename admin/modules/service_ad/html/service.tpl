<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
var wpseo_lang = 'en';
var wpseo_meta_desc_length = '155';
var wpseo_title = 'title';
var wpseo_content = 'content';
var wpseo_title_template = '%%title%%';
var wpseo_metadesc_template = '';
var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
$(document).ready(function() {
	$('#myForm').validate({
		rules: {					
			
				title: {
					required: true,
					minlength: 3
				} 
	    },
	    messages: {	    	
			
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
	
	{data.js_preview} 
	
});

</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" style="min-width:600px;" >
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		
 
    
    <tr class="form-required">
     <td class="row1" >{LANG.title_service}: </td>
     <td class="row0">
     	 <input name="title" id="title" type="text" size="60" maxlength="250" value="{data.title}" style="width:100%"  onkeyup="vnTMXH.setTitle(this.value)">
      </td>
    </tr>

    <tr >
      <td class="row1" >{LANG.picture}: </td>
      <td class="row0"><div id="ext_picture" class="picture" >{data.pic}</div>
        <input type="hidden" name="picture"	 id="picture" value="{data.picture}" />
        <div id="btnU_picture" class="div_upload" {data.style_upload} >
          <div class="button2">
            <div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div>
          </div>
        </div></td>
    </tr>
    <tr>
      <td class="row1" nowrap="nowrap">Ghi chú hình : </td>
      <td class="row0"><input id="picturedes" name="picturedes" type="text" size="70" maxlength="250" value="{data.picturedes}" style="width:100%"></td>
    </tr>
    <tr>
			<td class="row1">Mô tả ngắn&nbsp;: </td>
			<td align="left" class="row0">{data.html_short}</td>
		</tr>	
 
     <tr >
     <td class="row1" colspan="2" ><p><strong>{LANG.description}:</strong></p> {data.html_content}</td>
    </tr>  	 		   
    
    
     
		 
	</table>
  
   </td>
    <td width="40%" valign="top" style="padding-left:10px;min-width:400px;">
    	
      
      <div class="general">
		<h4 class="wpseo-heading" style="display: none;">General</h4>
		<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
			<tr>
      	<td class="row1"><label for="yoast_wpseo_snippetpreview">Snippet Preview:</label></td>
      	<td class="row0"><div id="wpseosnippet">
			<a href="#" class="wpseo_title"></a><br/>
		<a class="wpseo_url"  href="#"></a> 
			<p  class="desc"><span class="content" style="color: rgb(136, 136, 136);"></span></p>
		</div></td>
    
    </tr>
    
    
    <tr>
    	<td  class="row0" ><label ><strong>Friendly<br>URL :</strong></label></td>
      
    <td class="row0"> 
      <input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield" style="width:98%"  >
      <br>
      <span class="font_err">({LANG.mess_friendly_url})</span> </td>
  </tr>
              
    
   <tr> 
   <td class="row1"><label for="yoast_wpseo_title">Friendly Title:</label></td>
   <td class="row0"><input type="text" class="textfield" value="{data.friendly_title}" name="friendly_title" id="friendly_title"  style="width:98%"   /><br/><p>Title display in search engines is limited to 70 chars, <span id="friendly_title-length"><span class="good">70</span></span> chars left.<br/></td>
   </tr>
   
   <tr>
   <td class="row1"><label for="metadesc">Meta Description:</label></td>
   <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" class="textarea"  style="width:98%">{data.metadesc}</textarea><p>The <code>meta</code> description will be limited to 155 chars (because of date display), <span id="metadesc-length"><span class="good">155</span></span> chars left. </p><div id="metadesc_notice"></div></td>
   </tr>
   <tr>
   <td class="row1" ><label for="metakey">Meta Keyword:</label></td>
   <td class="row0"><input type="text" class="textfield" value="{data.metakey}" name="metakey"  id="metakey"  style="width:98%"/><br/><p></p><div style="width: 300px;" class="alignright"><p id="related_keywords_heading" style="display: none;">Related keywords:</p><div id="wpseo_tag_suggestions"></div></div>
		</td>
    </tr>
    </table>
</div>
 

 <br>
<div class="results">
<div id="focuskwresults">
	<div class="article_heading">
  	<div class="label">Article Heading</div><span class="wrong">NO</span></div>
    <div class="page_url"><div class="label">Page URL</div><span class="wrong">NO</span></div>
    <div class="page_title"><div class="label">Page title</div><span class="wrong">NO</span></div>
    <div class="meta_desc"><div class="label">Meta description</div><span class="wrong">NO</span></div>
    <div class="content_result"><div class="label">Content</div><span class="wrong">NO</span></div></div>
	 
    
    <div class="clear"></div>
	</div>		
 
  


          </div>
        </div>
        
        <br/>
        
        
        <div class="desc">
          <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
            <tr class="row_title" >
              <td  class="font_title" ><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Xem Demo tương tác FaceBook : </td>
            </tr>
            <tr>
              <td class="row0 desc_content"><div class="divFacebook"> {data.img_mxh}
                  <div class="face-info">
                    <div class="title_mxh" id="title_mxh" >{data.friendly_title}</div>
                    <div class="link_mxh" id="link_mxh" >{data.link_mxh}</div>
                    <div class="description_mxh" id="description_mxh" >{data.metadesc}</div>
                  </div>
                </div></td>
            </tr>
          </table>
        </div>
        <br/>
        <div class="desc"> 
       <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">		
        <tr class="row_title" >
         <td  colspan="2" class="font_title" ><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> {LANG.other_option}</td>
        </tr>
        </table>
        <div class="desc_content">
       <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable">		
   
         
        <tr>
          <td class="row1" nowrap="nowrap">{LANG.date_post}</td>
          <td align="left" class="row0"><input name="gio" type="text" size="5" maxlength="5" value="{data.gio}">&nbsp;,&nbsp;<input  id="ngay" name="ngay" type="text" size="10" maxlength="10" value="{data.ngay}"></td>
        </tr>  
        <tr>
            <td class="row1">{LANG.display_service}</td>
            <td align="left" class="row0">{data.list_display}</td>
        </tr>  
     	</table>
      </div>
      </div>
      
    
    </td>
  </tr>
  <tr >
    <td class="row0" colspan="2" style="padding-top:10px; padding-left:200px;">
     <input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button"> &nbsp;
				<input type="reset" name="Submit2" value="Reset" class="button"> &nbsp;
        <input type="submit" name="btn_preview" value="{LANG.btn_preview}" class="button">&nbsp;
    </td>
  </tr>
</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
    
  <tr>
    <td class="row1" ><strong>{LANG.view_day_from}:</strong>  </td>
    <td align="left" class="row0"> 
    <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"    /> &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10"   /> </td>
  </tr>
  <tr>
  
  <tr>
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
   <tr>
    <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->



<!-- BEGIN: form_contact -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
<link rel="icon" href="vntrust.ico" type="image/gif" >
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<!--[if gte IE 6]>
<link rel='stylesheet' href="{DIR_STYLE}/ie.css" type='text/css' media='all' />
<![endif]-->

<link rel='stylesheet' href='{DIR_JS}/thickbox/thickbox.css' type='text/css' media='all' />
{EXT_STYLE}
<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
</script>
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin.js"></script>
<script language="javascript1.2" src="{DIR_JS}/jquery.js"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/common.js"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/ajax-response.js"></script>
<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js'></script>
<script id='ext_javascript'></script>
<script type="text/javascript" src="{CONF.rooturl}/plugins/editors/fckeditor/fckeditor.js"></script>

{EXT_HEAD}

</head>
<body >
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm"  onSubmit="return checkform(this);" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		
  
    <tr >
     <td class="row1" >Tên form: </td>
     <td class="row0">{data.name}</td>
    </tr>
  
    
    <tr >
     <td class="row1" colspan="2" ><p>{data.html_content}</td>
    </tr>   
			
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
</body>
</html>      
<!-- END: body -->

<!-- END: form_contact -->