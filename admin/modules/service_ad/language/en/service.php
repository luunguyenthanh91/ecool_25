<?php
$lang = array(
	 //Cat_service
  'add_cat_service' => "Add new category",
  'edit_cat_service' => "Update category",
  'manage_cat_service' => "manage category",
  // Service
  'add_service' => "Add service",
  'edit_service' => "Update service",
  'manage_service' => "Service Management",
  'no_have_service' => 'no service',
  'err_empty_cat' => "Please select a category for services",
  'cat_service' => "Category",
  'title_service' => "Title",
  'short_service' => 'Short description',
  'content_service' => "Service content",
  'images_service' => "Image ",
  'picturedes_service' => "Pic Description",
  'source_service' => 'Source (from) ',
  'display_service' => "Display",
  'date_post' => "Date post",
  'post_by' => "Author",
  'other_option' => "options SHOW",
  'total_service' => "General Information",
  'focus_main' => "Focus to home ",
  'list_service' => "List  service",
	'service_parent'	=>	'Service parent',	
  
  
  // Setting
  'manage_setting_service' => 'Configuration management services',
  'edit_setting_service_success' => 'Update successfully configure services',
  'setting_module_service' => 'General Configuration for Services Module',
  'active_bqt' => 'Activate by BQT',
  'imgthumb_width' => 'thumbnail size',
  'imgdetail_width' => 'Size of details',
  'n_list' => 'The list service view',
  'n_grid' => 'Number of grid services view',
  'n_main' => 'Number of services in each category in the Home',
  'n_comment' => 'Number of comments in a page',
  'comment' => 'Product Reviews',
  'nophoto' => 'Show the <strong> nophoto </ strong> when no image file' 
	
  );
?>