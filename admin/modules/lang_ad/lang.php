<?php
/*================================================================================*\
|| 							Name code : lang.php 		 		          										  	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                   				  ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "lang";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_lang.php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . "lang_ad" . DS . "html" . DS . "lang.tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->linkUrl = "?mod=lang&act=lang";
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_lang'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang["edit_lang"];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'edit_lang':
        $nd['f_title'] = $vnT->lang['edit_pharse_lang'];
        $nd['content'] = $this->do_Edit_Lang($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_lang'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = getToolbar("lang");
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them lang moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $data['charset'] = "UTF-8";
    $data['date_format'] = "d/m/Y";
    $data['time_format'] = "H:i A";
    $data['num_format'] = "";
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $name = $vnT->input['name'];
      $title = $vnT->input['title'];
      $charset = $vnT->input['charset'];
      $date_format = $vnT->input["date_format"];
      $time_format = $vnT->input["time_format"];
      $unit = $vnT->input['unit'];
      $num_format = $vnT->input['num_format'];
      $is_default = $vnT->input['is_default'];
      // Check for Error
      $query = $DB->query("SELECT * FROM language WHERE name='{$name}' ");
      if ($check = $DB->fetch_row($query))
        $err = str_replace("<title>", "Code Name", $vnT->lang['title_exist']);
        // End check
      if (! empty($_FILES['image']) && ($_FILES['image']['name'] != "")) {
        $up['path'] = DIR_UPLOAD;
        $up['dir'] = "";
        $up['file'] = $_FILES['image'];
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $result['link'];
        } else {
          $err = $func->html_err($result['err']);
        }
      }
      if (empty($err)) {
        // set default
        if ($is_default == 1) {
          $DB->query("update  language set is_default=0 ");
        }
        $cot['name'] = $name;
        $cot['title'] = $title;
        $cot['picture'] = $picture;
        $cot['charset'] = $charset;
        $cot['date_format'] = $date_format;
        $cot['time_format'] = $time_format;
        $cot['unit'] = $unit;
        $cot['num_format'] = $num_format;
        $cot['is_default'] = $is_default;
        $ok = $DB->do_insert("language", $cot);
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $err = $vnT->lang["add_success"];
          $url = $this->linkUrl;
          $func->html_redirect($url, $err);
        } else {
          $err = $func->html_err($vnT->lang["add_failt"] . $DB->debug());
        }
      }
    }
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $data['err'] = $err;
    $data['list_default'] = vnT_HTML::list_yesno("is_default", $data['is_default']);
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat gioi thieu 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $name = $vnT->input['name'];
      $title = $vnT->input['title'];
      $charset = $vnT->input['charset'];
      $date_format = $vnT->input["date_format"];
      $time_format = $vnT->input["time_format"];
      $unit = $vnT->input['unit'];
      $num_format = $vnT->input['num_format'];
      $is_default = $vnT->input['is_default'];
      // Check for Error
      $query = $DB->query("SELECT * FROM language WHERE name='{$name}' and  lang_id<>$id ");
      if ($check = $DB->fetch_row($query))
        $err = str_replace("<title>", "Code Name", $vnT->lang['title_exist']);
        // End check
      if ($vnT->input['chk_upload'] == 1) {
        if (! empty($_FILES['image']) && ($_FILES['image']['name'] != "")) {
          $up['path'] = DIR_UPLOAD;
          $up['dir'] = "";
          $up['file'] = $_FILES['image'];
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      }
      if (empty($err)) {
        // set default
        if ($is_default == 1) {
          $DB->query("update  language set is_default=0 ");
        }
        $cot['name'] = $name;
        $cot['title'] = $title;
        $cot['charset'] = $charset;
        $cot['date_format'] = $date_format;
        $cot['time_format'] = $time_format;
        $cot['unit'] = $unit;
        $cot['num_format'] = $num_format;
        $cot['is_default'] = $is_default;
        if ($vnT->input['chk_upload'] == 1) {
          $img_q = $DB->query("SELECT picture FROM language WHERE lang_id=$id ");
          if ($img = $DB->fetch_row($img_q)) {
            $file_pic = DIR_UPLOAD . "/" . $img['picture'];
            if ((file_exists($file_pic)) && (! empty($img['picture'])))
              unlink($file_pic);
          }
          $cot['picture'] = $picture;
        }
        $ok = $DB->do_update("language", $cot, "lang_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl;
          $func->html_redirect($url, $err);
        } else {
          $err = $func->html_err($vnT->lang["add_failt"] . $DB->debug());
        }
      }
    }
    $sql = "select * from language where lang_id=$id";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
      $data['list_default'] = vnT_HTML::list_yesno("is_default", $data['is_default']);
      if (! empty($data['picture'])) {
        $data['pic'] = "<img src=\" " . DIR_UPLOAD . "/" . $data['picture'] . "\" ><br>";
        $data['pic'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"1\"> Upload Image &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        $data['pic'] .= "<input name=\"chk_upload\" type=\"radio\" value=\"0\" checked> Keep Image <br>";
      } else
        $data['pic'] = "<input name=\"chk_upload\" type=\"hidden\" value=\"1\">";
    }
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    $data['err'] = $err;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit_Lang 
   * 
   **/
  function do_Edit_Lang ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $name = ($vnT->input['name']) ? $vnT->input['name'] : "vn";
    $type = ($vnT->input['type']) ? $vnT->input['type'] : "global";
    "global";
    $phrase = ($vnT->input['phrase']) ? $vnT->input['phrase'] : "global";
    "global";
    if ($type == "global") {
      $FILE_NAME = PATH_ROOT . DS . "language" . DS . $name . DS . $phrase . ".php";
    } else {
      $FILE_NAME = PATH_ROOT . DS . "language" . DS . $name . DS . $type . DS . $phrase . ".php";
    }
    //set default
    if ($vnT->input['btn_SetDefault']) {
      $DB->query("update  language set is_default=0 ");
      $ok = $DB->query("update  language set is_default=1 WHERE name='{$name}' ");
      if ($ok) {
        //xoa cache
         $func->clear_cache();
					
				//insert adminlog
        $func->insertlog("Set Default", $_GET['act'], $name);
        $err = $vnT->lang["set_default_success"];
        $url = $this->linkUrl . "&sub=edit_lang&name=$name&type=$type&phrase=$phrase";
        $func->html_redirect($url, $err);
      } else {
        $err = $func->html_err($vnT->lang["set_default_failt"]);
      }
    }
    // btnEdit
    if ($vnT->input['btnUpdate']) {
      $file_string = "<?php \n";
      $file_string .= $vnT->input['header'];
      $file_string .= "\nif ( !defined('IN_vnT') )	{ die('Access denied');	} \n";
      $file_string .= "$" . "lang = array ( \n";
      $cot = $_POST['cot'];
      ksort($cot);
      foreach ($cot as $key => $value) {
        
        $value = str_replace("\r\n", "<br>", $value);
        if (get_magic_quotes_gpc()) {
					$value = preg_replace("/'/", "\\'", $value);
          $value = stripslashes($value);
        }
        $file_string .= "\t$key";
        $file_string .= "\t => \t";
        $file_string .= "'$value' ,\n";
        //	echo "key = $key <br>";
      //	echo "value = $value <br>";
      }
      $file_string .= "); \n?>\n";
      if ($FH = @fopen($FILE_NAME, 'w')) {
        fwrite($FH, $file_string, strlen($file_string));
        fclose($FH);
        $mess = "Update  Successfull !!!";
        //insert adminlog
        $func->insertlog("Edit Phrase", $_GET['act'], $phrase);
      } else {
        $mess = "Cannot write into file <strong>$phrase.php</strong> , please check cmod this file !!!";
      }
      $url = $this->linkUrl . "&sub=edit_lang&name=$name&type=$type&phrase=$phrase";
      flush();
      echo $func->html_redirect($url, $mess);
      exit();
    }
    //echo $FILE_NAME;
    if (file_exists($FILE_NAME)) {
      if (! is_writable($FILE_NAME)) {
        @chmod($FILE_NAME, 0777);
      }
      if ($FH = @fopen($FILE_NAME, 'rb')) {
        $content = @fread($FH, filesize($FILE_NAME));
        @fclose($FH);
      }
      //
      $s_header = strpos($content, "/*");
      $e_header = strpos($content, "*/", $s_header) + strlen("*/");
      $len_header = $e_header - $s_header;
      $header = trim(substr($content, $s_header, $len_header));
      //
      $s_text = strpos($content, "array (") + strlen("array (");
      $e_text = strpos($content, ");", $s_text);
      $len_text = $e_text - $s_text;
      $text = trim(substr($content, $s_text, $len_text));
      //xoa cac comment

			//$text = preg_replace('#(//)(.*?)(\n)#si', '', $text);
      $text = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $text);
      $arr_text = explode("\n", $text);
      asort($arr_text);
      foreach ($arr_text as $value) {
        $arr = explode("=>", $value);
        //nhay cuoi
				$arr[0] = trim($arr[0]);
        $arr[1] = trim($arr[1]);
        $pos_end = strrpos($arr[1], "'");
        $text_value = substr($arr[1], 1, $pos_end - 1);
        $str_find = array(
          "\'" , 
          "<br>");
        $str_replace = array(
          "'" , 
          "\n");
        $var_text = str_replace($str_find, $str_replace, trim($text_value));
				
				
        if (! empty($arr[0]) ) {
          $row['varname'] = trim($arr[0]);
          $row['text'] = trim($var_text);
          $this->skin->assign('row', $row);
          $this->skin->parse("edit_phrase.row_lang");
        }
      }
    }
    $data['header'] = $header;
    $data['list_lang'] = List_Lang($name);
    $data['list_type'] = List_Type($type);
    $data['list_pharse'] = List_Phrase($type, $phrase);
    $data['phrase'] = $phrase;
    $data['link_action'] = $this->linkUrl . "&sub=edit_lang&name=$name";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit_phrase");
    return $this->skin->text("edit_phrase");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    // Del Image
    $query = $DB->query("SELECT picture FROM language WHERE lang_id IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query)) {
      $file_pic = DIR_UPLOAD . "/" . $img['picture'];
      if ((file_exists($file_pic)) && (! empty($img['picture'])))
        unlink($file_pic);
    }
    // End del image
    $query = "DELETE FROM language WHERE lang_id IN (" . $ids . ") ";
    if ($ok = $DB->query($query)) {
      //xoa cache
      $func->clear_cache();
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['lang_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id;
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "')";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $picture = '<img src="' . DIR_UPLOAD . '/' . $row['picture'] . '" />';
    }
    $output['picture'] = $picture;
    $name = $row['name'];
    $output['name'] = "<a href=\"{$link_edit}\"><strong>" . $row['name'] . "</strong></a>";
    $output['title'] = $row['title'];
    $output['edit_block'] = "<a href=\"" . $this->linkUrl . "&sub=edit_lang&name={$name}\">Edit / Translate {$row['title']}</a>";
    if ($row['is_default']) {
      $default = "<img src=\"{$vnT->dir_images}/dispay.gif\" />";
    } else {
      $default = "<a href=\"" . $this->linkUrl . "&default_id={$id}\"><img src=\"{$vnT->dir_images}/nodispay.gif\" /></a>";
    }
    $output['default'] = $default;
    $output['action'] = '
			<input name=h_id[]" type="hidden" value="' . $id . '" />
			<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;	
			<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ((int) $vnT->input['default_id']) {
      $default_id = (int) $vnT->input['default_id'];
      $DB->query("update  language set is_default=0 ");
      $ok = $DB->query("update  language set is_default=1 WHERE lang_id=$default_id");
      if ($ok) {
        $err = $func->html_mess($vnT->lang['edit_success']);
      } else {
        $err = $func->html_err($vnT->lang['edit_failt']);
      }
      //insert adminlog
      $func->insertlog("Set Default", $_GET['act'], $default_id);
    }
    $table['link_action'] = $this->linkUrl;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'name' => "Code name|10%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|15%|center" , 
      'edit_block' => $vnT->lang['edit_pharse_lang'] . " |35%|center" , 
      'default' => $vnT->lang['default'] . "|15%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM language ORDER BY lang_id DESC ";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['lang_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_lang'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $data['table_list'] = $func->ShowTable($table);
    $data['totals'] = (int) $totals;
    $data['err'] = $err;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
