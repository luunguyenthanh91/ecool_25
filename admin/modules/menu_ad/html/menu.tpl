<!-- BEGIN: edit -->
<script language="javascript" >
$(document).ready(function() {
	$('#myForm').validate({
		rules: {
				pos: {
					required: true 
				},			
				title: {
					required: true,
					minlength: 3
				} 
				
	    },
	    messages: {	 
				pos: {
						required: "{LANG.err_select_required}"
				},			   	
				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
				
		}
	});
});

	function LoadAjax(pos,did,lang) {
		$.ajax({
				url: "modules/menu_ad/ajax/ajax.php",
				cache: false,
				type: "POST",
				data: ({ 'pos': pos, 'did': did, 'lang': lang }),
				dataType: "html",		 
				success: function(html)
				{ 		
					$("#ext_parent").html(html) ;  
				} 
		});
		 
	}
			
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	
     <tr>
            <td class="row1" width="25%" >{LANG.position} : </td>
            <td class="row0" >{data.list_pos}</td>
          </tr>
          <tr class="form-required" >
            <td  class="row1">{LANG.menu_name} : </td>
            <td  class="row0"><input name="title" id="title" type="text" size="60" maxlength="250" value="{data.title}"  >
			</td>
          </tr>
  		<tr   >
            <td  class="row1">Name Action : </td>
            <td  class="row0"><input name="name" id="name" type="text" size="30" maxlength="250" value="{data.name}"  > <span class="font_err">Dùng để xác định trạng thái Current của menu</span>
			</td>
          </tr>	  
		  <tr  >
            <td class="row1">{LANG.menu_link} : </td>
            <td  class="row0"><input name="menu_link" id="menu_link" type="text" size="60" maxlength="250" value="{data.menu_link}" class="textfield"></td>
          </tr>

		  
		   <tr>
            <td class="row1">{LANG.target} : </td>
            <td  class="row0">{data.list_target}</td>
          </tr>
          
          
		<tr>
            <td  class="row1"> {LANG.sub_menu_of} : </td>
            <td class="row0"><span id="ext_parent">{data.list_cat}</span></td>
          </tr>


		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnSubmit" value="Submit" class="button">
				<input type="reset" name="btnReset" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
  	<td align="left">{LANG.position}: &nbsp;</td>
    <td >{data.list_pos}</td>
  </tr>
  </table>
</form>
{data.err}
<br />
<form id="manage" name="manage" method="post" action="{data.link_action}">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
   <tr>
  	<td >
    <table  border="0" cellspacing="2" cellpadding="2">
    	<tr>
           <td width="40" align="center"><img src="{DIR_IMAGE}/arr_top.gif" width="17" height="17"></td>
           <td>{data.button}</td>
         </tr>
     </table>
     </td>
   </tr>
   <tr>
     <td>
   	
<table cellspacing="1" class="adminlist" id="table_list">
<thead>
<tr height="25">
		<th width="5%" align="center" ><input type="checkbox" value="all" class="checkbox" name="checkall" id="checkall"/></td>
		<th width="10%" align="center" >{LANG.order}</th>
		<th width="10%" align="center" >Name</th>
		<th width="30%" align="left" >{LANG.title}</th>
		<th width="35%" align="left" >Link</th>
		<th width="10%"  align="center" >Action</th>
		</tr>
</thead>
<tbody>
   
<!-- BEGIN: html_row -->
<tr class="{row.class}" id="{row.row_id}"> 
	<td align="center" >{row.check_box}</td>
	<td align="left" >{row.order}</td>
	<td align="center" >{row.name}</td>
	<td align="left" >{row.title}</td>
	<td align="left" >{row.menu_link}</td>
  <td align="center" >{row.action}</td>
</tr>
<!-- END: html_row -->

<!-- BEGIN: html_row_no -->
<tr class="row0" > 
	<td  colspan="7" align="center" class="font_err" >{mess}</td>
</tr>
<!-- END: html_row_no -->
</tbody>
</table>
</td>
   </tr>
    <tr>
      <td >
         <table  border="0" cellspacing="2" cellpadding="2">
           <tr>
           <td width="40" align="center"><img src="{DIR_IMAGE}/arr_bottom.gif" ></td>
           <td>{data.button}</td>
         </tr>
    	 </table>
     </td>
   </tr>
</table>
<input type="hidden" name="do_action" id="do_action" value="" >
</form>
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->