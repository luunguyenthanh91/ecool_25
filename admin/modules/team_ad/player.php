<?php
/*================================================================================*\
|| 							Name code : player.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "team";
  var $action = "player";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    loadSetting();
		
		$this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign("DIR_JS", $vnT->dir_js);		
		$this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;		
		$vnT->html->addStyleSheet( "modules/" . $this->module."_ad/css/".$this->module.".css");
		$vnT->html->addScript("modules/" . $this->module."_ad/js/".$this->module.".js");
		
		$vnT->html->addStyleSheet( $vnT->dir_js."/multi-select/multi-select.css");
		$vnT->html->addScript($vnT->dir_js."/multi-select/multi-select.js");
		
    switch ($vnT->input['sub'])
    {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_player'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_player'];
        $nd['content'] = $this->do_Edit($lang);
        break;
			case 'duplicate':
        $this->do_Duplicate($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_player'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $err = "";
    $vnt->input['price']=0;
		$vnt->input['display'] =1;
 		$rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750 ;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
    if ($vnT->input['do_submit'] == 1)
    {
			$data = $_POST; 
			
			$p_id = @implode(",",$vnT->input['list_cat']); 
			
			
			$p_name = $vnT->input['p_name'];  
			
 			$picture = $vnT->input['picture'];
 			$picture1 = $vnT->input['picture1'];
			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]); 
			if($vnT->input['op_value']){
				$options = serialize($vnT->input['op_value']);
			}
			
			//check maso
			if ($maso){
				$res_ck = $DB->query("select player_id,maso from teams_player where maso='$maso' AND lang='$lang'  ") ;
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
 			
      // insert CSDL
      if (empty($err))
      {
				$cot['p_id'] = $p_id;
				$cot['p_list'] = get_p_list($p_id);
					
				$cot['picture'] = $picture;			
				$cot['picture1'] = $picture1;
				$cot['date_post'] = time();
				$cot['date_update'] = time();			
				$cot['adminid'] =  $vnT->admininfo['adminid']; 
			
        $ok = $DB->do_insert("teams_player", $cot);
        if ($ok)
        {
					$player_id = $DB->insertid();
					 
				 	//player content 
					$cot_d['player_id'] = $player_id;					
					$cot_d['p_name'] = $p_name;
					$cot_d['vitri'] =  $vnT->input['vitri'];
					$cot_d['soao'] =  $vnT->input['soao'];
					$cot_d['namsinh'] =  $vnT->input['namsinh'];
					$cot_d['description'] = $func->txt_HTML($_POST['description']);
					$cot_d['key_search'] =  strtolower($func->utf8_to_ascii($p_name)) ;
					$cot_d['display'] = $vnT->input['display'];
					
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($p_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($p_name);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $p_name ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['description'],'nohtml'),200,1) ;
					
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("teams_player_desc",$cot_d);
					}
					
					
					//update maso
					if(empty($maso)){
						$dup['maso'] = create_maso ($p_id,$player_id);
 					} 		
					$dup['p_order'] = $player_id;          
          $DB->do_update("teams_player", $dup, "player_id=$player_id");
					
					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $player_id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$player_id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE teams_player_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE player_id=".$player_id) ; 
					}
					
					//xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $player_id);
					
          $mess = $vnT->lang['add_success'];
          if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$player_id&preview=1";
						$DB->query("Update teams_player SET display=0 WHERE player_id=$player_id ");
					}else{
						$url = $this->linkUrl . "&sub=add";						
					}
          $func->html_redirect($url, $mess);
        }
        else
        {
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    
    }
		
		
		//option
		$res_op = $DB->query("select * from player_option n, player_option_desc nd  
													where  n.oplayer_id=nd.oplayer_id
													AND nd.lang='$lang'
													AND n.display=1 
													order by n.op_order, n.oplayer_id DESC");
		while ($r_op = $DB->fetch_row($res_op)){
			$r_op['op_name'] = $func->HTML($r_op['op_name']);			
			$this->skin->assign('row', $r_op);
			$this->skin->parse("edit.row_option");
		}
		 
		$data['list_cat'] = List_Cat_Team($p_id,$lang);
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
		
		$data['list_status'] = List_Status_Pro('status',$vnt->input['status'],$lang) ;
		$data['list_display'] = vnT_HTML::list_yesno ("display",$vnt->input['display']) ;		
		$data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '450', "Default",$this->module,$dir);
 		
		$data['link_seo'] = $conf['rooturl']."xxx.html";
		if($data['friendly_url'])
			$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
		
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
		$data['link_upload1'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture1&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  
  }
  
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $id = (int) $vnT->input['id'];
    $rate = ($vnT->setting['rate']) ? $vnT->setting['rate'] : 1;
		$w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 750 ;
		$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
		
    if ($vnT->input['do_submit'])
    {
      $data = $_POST;
 			$p_id = @implode(",",$vnT->input['list_cat']);
			
			$p_name = $vnT->input['p_name'];
			
			$ngay = explode("/",$vnT->input['date_issue']);
			
			$picture = $vnT->input['picture'];
			$picture1 = $vnT->input['picture1'];
 			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]); 
			if($vnT->input['op_value']){
				$options = $vnT->format->txt_serialize($vnT->input['op_value']);
			}
			
			//check maso
			if ($maso){
				$res_ck = $DB->query("select player_id,maso from teams_player where maso='$maso' AND lang='$lang' and player_id<>$id ") ;
				if ($DB->num_rows($res_ck)) $err = $func->html_err($vnT->lang['maso_existed']);
			}
			
			 
      
      if (empty($err))
      {
				$cot['p_id'] =  $p_id;
				$cot['p_list'] = get_p_list($p_id);
				
				$cot['picture'] = $picture;
				$cot['picture1'] = $picture1;
				$cot['status'] = $vnT->input['status'];				
				$cot['date_update'] = time();			
		 
				// more here ...
        $ok = $DB->do_update("teams_player", $cot, "player_id=$id");
        if ($ok)
        {
					//update content
					$cot_d['p_name'] = $p_name;
					
 					$cot_d['vitri'] =  $vnT->input['vitri'];
 					$cot_d['soao'] =  $vnT->input['soao'];
					$cot_d['namsinh'] =  $vnT->input['namsinh'];
					$cot_d['key_search'] =  strtolower($func->utf8_to_ascii($p_name)) ;
					$cot_d['display'] = $vnT->input['display'];
					$cot_d['description'] = $func->txt_HTML($_POST['description']);				
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($p_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $func->utf8_to_ascii($p_name);
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? trim($vnT->input['metakey']) :  $p_name ;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? trim($vnT->input['metadesc']) :  $func->cut_string($func->check_html($_POST['description'],'nohtml'),200,1) ;
					
					$DB->do_update("teams_player_desc",$cot_d," player_id={$id} and lang='{$lang}'");
					
					//build seo_url
					$seo['sub'] = 'edit';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE teams_player_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE lang='".$lang."' AND player_id=".$id) ;
					}
					
					//xoa cache
          $func->clear_cache();
					
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);          
          $err = $vnT->lang["edit_success"];
					
					if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$id&preview=1";
					}else{
						$url = $this->linkUrl . "&sub=edit&id=$id";
					}
          
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    
    }
    
    $query = $DB->query("SELECT *
													FROM teams_player p, teams_player_desc pd
													WHERE p.player_id=pd.player_id 
													AND pd.lang='$lang'
													AND p.player_id=$id ");
    if ($data = $DB->fetch_row($query))
    {
			if($vnT->input['preview']==1)	{
				  
				$link_preview = $conf['rooturl'];
				if($vnT->muti_lang) $link_preview .= $lang."/";				
				$link_preview.= $data['friendly_url'].".html/?preview=1";
				
				$mess_preview = str_replace(array("{title}","{link}"),array($data['p_name'],$link_preview),$vnT->lang['mess_preview']);
				$data['js_preview'] = "tb_show('".$mess_preview."', '".$link_preview."&TB_iframe=true&width=1000&height=700',null)";
			}
			
			if ($data['picture']) {
						//$dir = substr($data['picture'], 0, strrpos($data['picture'], "/"));				
		        $data['pic'] = get_pic_player($data['picture'],$w_thumb) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
						$data['style_upload'] = "style='display:none' ";
		      } else {
		        $data['pic'] = "";
		      } 
				
		      if ($data['picture1']) {
						//$dir = substr($data['picture'], 0, strrpos($data['picture'], "/"));				
		        $data['pic1'] = get_pic_player($data['picture1'],$w_thumb) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
						$data['style_upload1'] = "style='display:none' ";
		      } else {
		        $data['pic1'] = "";
		      } 

			//option
			if($data['options']){
				$options = unserialize($data['options']);
			}
			
			
			$res_op = $DB->query("select * from player_option n, player_option_desc nd  
														where  n.oplayer_id=nd.oplayer_id
														AND nd.lang='$lang'
														AND n.display=1 
														order by n.op_order, nd.oplayer_id DESC ");
			while ($r_op = $DB->fetch_row($res_op)){
				$r_op['op_name'] = $func->HTML($r_op['op_name']);			
				$r_op['op_value'] = $options[$r_op['oplayer_id']];
				$this->skin->assign('row', $r_op);
				$this->skin->parse("edit.row_option");
			}
			
			 
			$data['link_seo'] = $conf['rooturl']."xxx.html";
			if($data['friendly_url'])
				$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
			
			$data['img_mxh'] = '<div class="face-img"><img src="'.$conf['rooturl'].'image.php?image='.$conf['rooturl'].ROOT_UPLOAD.'/'.$data['picture'].'&width=120&height=120&cropratio=1:1" alt="" /></div>';
			
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    
		$data['list_cat'] = List_Cat_Team($data['p_id'],$lang); 
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
		
 		$data['list_status'] = List_Status_Pro('status',$data['status'],$lang) ;
		$data['list_display'] = vnT_HTML::list_yesno ("display",$data['display']) ;
		
		$data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '450', "Default",$this->module,$dir); 
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
		$data['link_upload1'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture1&type=image&TB_iframe=true&width=900&height=474';
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
	
	/**
   * function do_Duplicate 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Duplicate ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
 		 
		$res = $DB->query("SELECT * FROM teams_player WHERE player_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
				//Duplicate teams_player
        $cot = $row;
				$cot["player_id"] = ""; 
				$cot["views"] = 0;
				$cot["votes"] = 0;
				$cot["numvote"] = 0;
				$cot["date_post"] = time();
				$cot["date_update"] = time();
				$DB->do_insert("teams_player", $cot);
				//End Duplicate teams_player
				
				$player_id = $DB->insertid();
				if($player_id)
				{
					//update maso 
					$dup['maso'] = create_maso ($cot['p_id'],$player_id); 							
					$dup['p_order'] = $player_id; 
          $DB->do_update("teams_player", $dup, "player_id=$player_id");
					
					//Duplicate teams_player_desc
					$res_d = $DB->query("SELECT * FROM teams_player_desc WHERE player_id=".$row['player_id']." ");
					while ($row_d = $DB->fetch_row($res_d))  {
						$dup_d = $row_d;
						$dup_d["id"] = "";
						$dup_d["player_id"] = $player_id;
						$DB->do_insert("teams_player_desc", $dup_d);
					}
					//End Duplicate teams_player_desc
					
					//Duplicate player_picture
					$res_pic = $DB->query("SELECT * FROM player_picture WHERE player_id=".$row['player_id']." ");
					while ($row_pic = $DB->fetch_row($res_pic))  {
						$dup_pic = $row_pic;
						$dup_pic["id"] = "";
						$dup_pic["player_id"] = $player_id;
						$DB->do_insert("player_picture", $dup_pic);
					}
					//End Duplicate player_picture
					
					
					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $player_id;
					$seo['friendly_url'] = $dup_d['friendly_url']."-".time() ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$player_id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE teams_player_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE player_id=".$player_id) ; 
					}
					
				}
      }
      $mess = $vnT->lang["duplicate_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["duplicate_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
 		 
		$res = $DB->query("SELECT player_id FROM teams_player WHERE player_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {

			$DB->query("UPDATE teams_player_desc SET display=-1 WHERE  player_id IN (" . $ids . ") AND lang='".$lang."' ");

			//insert RecycleBin
			$rb_log['module'] = $this->module;
			$rb_log['action'] = "detail";
			$rb_log['tbl_data'] = "teams_player_desc";
			$rb_log['name_id'] = "player_id";
			$rb_log['item_id'] = $ids;
			$rb_log['lang'] = $lang;
			$func->insertRecycleBin($rb_log);

      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['player_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
		$link_duplicate = $this->linkUrl . '&sub=duplicate&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_pic = "?mod=player&act=pic_player&id={$id}&lang=$lang&sub=edit";
		
			
		if ($row['picture']){
			$output['picture']= get_pic_player($row['picture'],50);
		}else $output['picture'] ="No image";
 		
		 $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\"  style=\"text-align:center\" value=\"{$row['p_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		 
		$output['price'] = "<input name=\"txtPrice[{$id}]\" type=\"text\" style=\"text-align:center\" size=\"10\"  value=\"{$row['price']}\" onchange='javascript:do_check($id)' />";
    	 
    $output['p_name'] = "<strong><a href='" . $link_edit . "' >" . $func->HTML($row['p_name']) . "</a></strong>";
		$output['p_name'] .= '<div style="padding:2px;"> <span class=font_err >(MS: <b >'.$row['maso'].'</b>)</div>'; 
		 
 		$info = "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
 		$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['views']."</strong></div>";
		$output['info'] = $info ;
		
		$output['pic_other'] = '<a href="' . $link_pic . '"><img src="' . $vnT->dir_images . '/ico_hinhanh.gif"  alt="Picture "></a>';
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_duplicate . '"><img src="' . $vnT->dir_images . '/icon_duplicate.png"  alt="Duplicate "></a>&nbsp;';
		$output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
			$mess ='';
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
						if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
            if (isset($vnT->input["txtPrice"])) $arr_price = $vnT->input["txtPrice"] ; else $arr_price=""; 
            $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++)
            {
							$dup['p_order'] = $arr_order[$h_id[$i]]; 
							$dup['price'] = $arr_price[$h_id[$i]]; 
							$dup['date_update'] = time();
              $ok = $DB->do_update("teams_player", $dup, "player_id=" . $h_id[$i]);
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("teams_player_desc", $dup, "lang='$lang' AND player_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("teams_player_desc", $dup, "lang='$lang' AND player_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update teams_player_desc SET display=1 WHERE lang='$lang' AND player_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update teams_player_desc SET display=0 WHERE lang='$lang' AND player_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		$cat_id = ((int)$vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0;
		$p_id = ((int) $vnT->input['p_id']) ?   $vnT->input['p_id'] : 0;
		$status_id = ((int) $vnT->input['status_id']) ?   $vnT->input['status_id'] : 0; 
		
		// $search = ($vnT->input['search']) ?  $vnT->input['search'] : "player_id";
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ?   $vnT->input['direction'] : "DESC";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";
		
		$where ="  ";
		$ext_page='';
		$ext='';
		if (!empty($cat_id)) {
      $where .= " AND p_id=$cat_id ";
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
 		 
		if($status_id){
			$where .=" and status=$status_id ";
			$ext_page.="status_id=$status_id|";
			$ext.="&status_id={$status_id}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "player_id" : $where .=" and  p.player_id = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and p_name like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		 
		$sortField = ($vnT->input['sortField']) ? $vnT->input['sortField'] : "p_order";
		$sortOrder = ($vnT->input['sortOrder']) ? $vnT->input['sortOrder'] : "DESC";
		 
		$OrderBy = " ORDER BY $sortField $sortOrder , date_post DESC ";		 
		
 		$ext_page=$ext_page."p=$p";
		 
    $query = $DB->query("SELECT p.player_id
													FROM teams_player p, teams_player_desc pd
													WHERE p.player_id=pd.player_id 
													AND pd.lang='$lang' 
													AND display != -1 
													$where");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
		$sortLinks = array("player_id","p_order","p_name","price","date_post" );
		$data['SortLink'] = $func->BuildSortingLinks($sortLinks,$this->linkUrl.$ext."&p=".$p, $sortField, $sortOrder); 
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'order' => $vnT->lang['order']." ".$data['SortLink']['p_order']."|10%|center",				
				'picture' => $vnT->lang['picture']." |8%|center",				
				'p_name' => $vnT->lang['player_name']." ".$data['SortLink']['p_name']."|27%|left",
				//'price' => " Giá ".$data['SortLink']['price']."|12%|center",
				//'pic_other' => $vnT->lang['other_pic']."|8%|center",
				'info' =>$vnT->lang['infomartion']. " ".$data['SortLink']['date_post']."|20%|left",
				'action' => "Action|15%|center"
				);
    
    $sql = "SELECT *
						FROM teams_player p, teams_player_desc pd
						WHERE p.player_id=pd.player_id 
						AND pd.lang='$lang'
						AND display != -1 
						$where 
						$OrderBy LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['player_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_player'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDuplicate" value=" '.$vnT->lang['duplicate'].' " class="button" onclick="action_selected(\''.$this->linkUrl.'&sub=duplicate&ext='.$ext_page.'\', \''.$vnT->lang["duplicate"].'?\')">';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['listcat']=Get_Cat_Player($cat_id,$lang);
		$data['list_status'] = List_Status_Pro("status_id",$status_id,$lang);
		$data['list_search']=List_Search($search); 
		$data['keyword'] = $keyword;
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['list_search'] = List_Search($search);
		 
	
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>