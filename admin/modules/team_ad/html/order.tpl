<!-- BEGIN: edit -->
 
<p align="center">Order no : <font color="#FF0000" size="+1"><strong>{data.order_code}</strong></font></p>
<table width="98%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td ><fieldset>
          <legend class="font_err">&nbsp;<strong>{LANG.f_info_order}</strong>&nbsp;</legend>
          <table width="100%" border="0" cellspacing="2" cellpadding="2">

            <tr>
              <td valign="top"><strong>{LANG.payment_address} :</strong> <br />
                {data.info_customer}</td>
              <td valign="top"><strong>{LANG.shipping_address} :</strong> <br />
{data.info_receiver}</td>
            </tr>
    
          </table>
        </fieldset></td>
      </tr>
    </table>
      <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td><fieldset>
            <legend class="font_err">&nbsp;<strong>{LANG.f_info_cart}</strong>&nbsp;</legend>
            <br />
            <table width="100%" border="0"  cellpadding="2" cellspacing="1" class="border_cart">
            <tr >
              <td width="5%" class="row_title_cart"  align="center"><b>{LANG.stt}</b> </td>
              <td width="10%" class="row_title_cart"  align="center"><b>{LANG.picture}</b> </td>
              <td width="40%" class="row_title_cart"  align="center"><b>{LANG.p_name}</b> </td>
              <td width="20%" class="row_title_cart" align="center"><b>{LANG.price}</b></td>
              <td width="10%" class="row_title_cart" align="center"><b>{LANG.quantity}</b> </td>
              <td width="15%" class="row_title_cart" align="center"><b>{LANG.total}</b> </td>
            </tr>
              
              <!-- BEGIN: row_cart -->
              <tr  height=25>
                <td class="row_item_cart" align="center" >{row.stt}</td>
                <td class="row_item_cart" align="center">{row.pic}</td>
                <td class="row_item_cart" align="left">{row.p_name}</td>
                <td class="row_item_cart" align="right" >{row.price}</td>
                <td class="row_item_cart" align="center" >{row.quantity}</td>
                <td class="row_item_cart" align="right" >{row.total}</td>
              </tr>
     					<!-- END: row_cart -->
          
              <tr  height="20">
               <td class="row_total_cart"  colspan="5"  align="right"><b>{LANG.total_price}: </b> </td>
               <td  class="row_total_cart"   align="right" style="padding-right:5px" ><b>{data.total_price}</b></td>
							</tr>

            </table>
          </fieldset></td>
        </tr>
      </table>
      <br>
      <table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
        <tr>
          <td><strong>{LANG.comment_order} </strong> : <p>{data.comment}</p></td>
        </tr>
				<tr>
          <td><strong>{LANG.date_order}:</strong> {data.date_order}</td>
        </tr>
        <tr>
          <td><strong>{LANG.date_ship}</strong> : {data.text_ship_date}</td>
        </tr>
				
      </table>
    <br>

{data.err}
<form action="{data.link_action}" method="post" name="myForm">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
 <tr class="row_title">
    <td  height="30" class="font_title" colspan="2">{LANG.update_order}</td>
  </tr>
	<tr >
    <td width="30%"  class="row1"><strong>{LANG.date_ship}:</strong> </td>
    <td width="70%" class="row0"><input type="text" name="ship_date" id="ship_date" value="{data.ship_date}"  maxlength="10"  onclick="pickDate(this,document.myForm.ship_date);" readonly="readonly"  /> <span class="font_err">(dd/mm/YYYY)</span></td>
  </tr>
  <tr >
    <td class="row1"><strong>{LANG.update_status}: </strong></td>
    <td class="row0">{data.list_status}</td>
  </tr>
  <tr>
    <td class="row1"><strong>{LANG.note}  : </strong></td>
    <td class="row0"><textarea name="note" rows="5" cols="40">{data.note}</textarea></td>
  </tr>
  <tr>
  	<td class="row1">&nbsp;</td>
    <td class="row0">
    <input type="submit" name="btnSave" value=" Save "  class="button"> &nbsp;&nbsp; 
    <!--<input type="button" name="btnPrint" value=" Print Order " onclick="javascript:openPopUp('{data.link_print}','Print',700,600, 'Yes')"   class="button"> -->
    </td>
  </tr>
</table>
</form>		

<br>
<br>
<!-- END: edit -->

<!-- BEGIN: send_email -->
{data.err}
<form action="{$data['link_action']}" method="post" >
   <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">

    <tr>
      <td width="20%"   class="row1" >Kh&#225;ch h&#224;ng: </td>
      <td width="80%" class="row0" >
        <input type="text" name="full_name" value="{data.full_name}" size="50" readonly="ReadOnly"></td>
    </tr>
    <tr>
      <td  class="row1" >Email : </td>
      <td class="row0" ><input type="text" name="email" value="{data.email}" size="50" readonly="ReadOnly"></td>
    </tr>
    <tr>
      <td  class="row1" >Ti&#234;u &#273;&#7873; : </td>
      <td class="row0" ><input type="text" name="subject" size="50" value="{data.subject}"></td>
    </tr>
    <tr>
      <td  class="row1" >N&#7897;i dung g&#7917;i : </td>
      <td class="row0">{data.html_content}</td>
    </tr>
    <tr>
      <td class="row1">&nbsp;</td>
      <td class="row0" >
	  <input type="submit" name="btnSend" value="Submit" class="button">&nbsp;&nbsp;
	  <input type="reset" name="Reset" value="Reset" class="button"></td>
    </tr>
	</table>
	</form>
<br />
<!-- END: send_email -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" ><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
    <td > <strong>Order ID :</strong> </td>
    <td ><input name="keyword"  value="{data.keyword}"type="text"> <input name="btnSearch" type="submit" value=" Search ! " class="button"></td>
  </tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->