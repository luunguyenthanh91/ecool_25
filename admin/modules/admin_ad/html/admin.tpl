<!-- BEGIN: edit -->
<script language="javascript" >
$(document).ready(function() {

	jQuery.validator.addMethod("re_password", function( value, element ) {
		if($("#password").val()!='' || $("#re_password").val()!='' ){
			if($("#password").val()!=$("#re_password").val()) {
				return false	;
			}else{
				return true ;		
			}
		}else{
			return true ;	
		}		
	}, "Xác nhận mật khảu không đúng");
	
	$('#myForm').validate({
		rules: {			
				username: {
					required: true,
					minlength: 3
				},
				re_password : "re_password",
				email: {
					required: true,
					email: true
				}
	    },
	    messages: {	    	
				username: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				},
				email: {
						required: "{LANG.err_text_required}",
						email: "{LANG.err_email_invalid}" 
				} 
		}
	});
});
</script>
{data.err}
<form action="{data.link_action}" method="post" name="myForm" id="myForm" class="validate">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
<tr class="form-required">
	<td width="20%" align="right" class="row1"><strong>Username :</strong> </td>
	<td  class="row0"><input name="username" type="text" id="username" size="40" maxlength="50" value="{data.username}" class="textfield" ></td>
</tr>
<tr>
	<td align="right" class="row1"><strong>Password :</strong> </td>
	<td class="row0"><input name="password" type="password" id="password" size="40" maxlength="50" value="" class="textfield"></td>
</tr>
<tr>
	<td align="right" class="row1"><strong>Nhập lại Password :</strong> </td>
	<td class="row0"><input name="re_password" type="password" id="re_password" size="40" maxlength="50" value="" class="textfield"></td>  
</tr>
<tr class="form-required">
	<td align="right" class="row1" ><strong>Email :</strong> </td>
	<td class="row0" ><input name="email" type="text" id="email" size="40" maxlength="50" value="{data.email}" class="textfield"></td>
</tr>
{data.list_group}

<tr>
	<td align="right" class="row1" >&nbsp;</td>
	<td height="50" class="row0" >
	<input name="do_submit" id="do_submit" type="hidden" value="1">
	<input type="submit" name="Submit" value="Submit" class="button">&nbsp;&nbsp;
    <input type="reset" name="reset" value="Reset" class="button">
    </td>
</tr>
</table>
</form>

<br />
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%" align="left">{LANG.totals}: &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>

  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->