<?php
/*================================================================================*\
|| 							Name code : cat_video.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "video";
  var $action = "category";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_cat_video'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_cat_video'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'move':
        $nd['f_title'] = "Di chuyển tin  ";
        $nd['content'] = $this->do_Move($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_cat_video'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Cat($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them gioi thieu moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $cat_name = $vnT->input['cat_name'];
      $parentid = $vnT->input["cat_id"];
      //upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "category";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 500;
        $result = $vnT->File->Upload($up);
        if (empty($result['err'])) {
          $picture = $result['link'];
          $file_type = $result['type'];
        } else {
          $err = $func->html_err($result['err']);
        }
      } else {
        if ($vnT->input['picture']) {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "category";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 500;
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
            $file_type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
      // insert CSDL
      if (empty($err)) {
        $cot['parentid'] = $parentid;
        $cot['picture'] = $picture;
				$ok = $DB->do_insert("video_category", $cot);
				if ($ok)
        {
          $cat_id = $DB->insertid();
          
          //update cat content
          $cot_d['cat_id'] = $cat_id;
					$cot_d['cat_name'] = $cat_name;
					$cot_d['description'] = $vnT->input['description'];				
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($cat_name);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $cat_name ." | ". $func->utf8_to_ascii($cat_name);
					$cot_d['metakey'] = $vnT->input['metakey'];
					$cot_d['metadesc'] = $vnT->input['metadesc'];	
					
					$query_lang = $DB->query("select name from language ");
          while ($row = $DB->fetch_row($query_lang))
          {
            $cot_d['lang'] = $row['name'];
            $DB->do_insert("video_category_desc", $cot_d);
          }
          
          //update catcode				
          $dup['cat_code'] = get_catCode($parentid, $cat_id);
          $DB->do_update("video_category", $dup, "cat_id=$cat_id");
          
          //xoa cache				
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $cat_id);
          
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        }
				else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
		
    $data['listcat'] = Get_Cat($vnT->input['cat_id'], $lang, ""); 
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat admin
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $cat_name = $vnT->input['cat_name'];
      $parentid = $vnT->input["cat_id"];
      // Check for Error
      if ($parentid == $id)
        $err = $func->html_err($vnT->lang['err_parentid_invalid']);
      if (empty($err)) {
        //upload
        if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "category";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = 500;
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
            $file_type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        } else {
          if ($vnT->input['picture']) {
            $up['path'] = MOD_DIR_UPLOAD;
            $up['dir'] = "category";
            $up['url'] = $vnT->input['picture'];
            $up['type'] = "hinh";
            $up['w'] = 500;
            $result = $vnT->File->UploadURL($up);
            if (empty($result['err'])) {
              $picture = $result['link'];
              $file_type = $result['type'];
            } else {
              $err = $func->html_err($result['err']);
            }
          }
        } //end upload		
      }
      if (empty($err)) {
        $cot['parentid'] = $parentid;

        if ($vnT->input['chk_upload'] == 1 || ! empty($picture)) {
          // Del Image
          $query = $DB->query("SELECT picture FROM video_category WHERE cat_id='{$id}'");
          if ($img = $DB->fetch_row($query)) {
            $file_pic = MOD_DIR_UPLOAD . "category/" . $img['picture'];
            //echo $file_pic;
            if (! empty($img['picture']) && file_exists($file_pic))
              @unlink($file_pic);
          }
          // end del
          $cot['picture'] = $picture;
        }
				$cot['date_update'] = time();
				$ok = $DB->do_update("video_category", $cot, "cat_id=$id");
				
        if ($ok) 
				{
          
           //update catcode
					$dup['cat_name'] = $cat_name;
					$dup['description'] = $vnT->input['description'];					
					$dup['friendly_url'] = (trim($vnT->input['friendly_url'])) ? trim($vnT->input['friendly_url']) :  $func->make_url($cat_name);
					$dup['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) : $cat_name ." | ". $func->utf8_to_ascii($cat_name);
					$dup['metakey'] = $vnT->input['metakey'];
					$dup['metadesc'] = $vnT->input['metadesc'];	
          //$dup['cat_code'] = get_catCode($parentid, $id);
          $DB->do_update("video_category_desc", $dup, "cat_id=$id");
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } 
				else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM video_category  WHERE lang='$lang' AND cat_id=$id");
		$query = $DB->query("SELECT * FROM video_category n , video_category_desc nd 
												WHERE n.cat_id=nd.cat_id
												AND nd.lang='$lang' 
												AND n.cat_id=$id");
    if ($data = $DB->fetch_row($query)) {
      if ($data['picture']) {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "category/" . $data['picture'] . "\" width=100 />";
      } else {
        $data['pic'] = "";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['listcat'] = Get_Cat($data['parentid'], $lang, "");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Move    
   **/
  function do_Move ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnT->html->addScript("modules/video_ad/js/video.js");
    if ($vnT->input['btnAll']) {
      $cat1 = $vnT->input['cat1'];
      $cat_chose = $vnT->input['cat_chose'];
      /*	
			print "cat1 = ".$cat1."<br>";
			print "cat_chose = ".$cat_chose."<br>";
		*/
      if ((int) $cat1 && (int) $cat_chose) {
        //check
        $res_ck = $DB->query("select cat_id,video_id from videos where video_id<>0 and cat_id=$cat1  ");
        while ($row_ck = $DB->fetch_row($res_ck)) {
          $cat_new = $cat_chose;
          //			echo "cat_new = $cat_new <br>";
          $DB->query("UPDATE videos SET cat_id='{$cat_new}' where video_id=" . $row_ck['video_id']);
        }
        //xoa cache
        $func->clear_cache();
        //insert adminlog
        $func->insertlog("Move video", $_GET['act'], $cat1);
        $err = $vnT->lang["move_item_success"];
        $url = $this->linkUrl . "&sub=move";
        $func->html_redirect($url, $err);
      } else {
        $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      $cat_chose = $vnT->input['do_action'];
      $mess .= "- " . $vnT->lang['move_success'] . " ID: <strong>";
      $str_mess = "";
      for ($i = 0; $i < count($h_id); $i ++) {
        $dup['cat_id'] = $cat_chose;
        $ok = $DB->do_update("videos", $dup, "video_id={$h_id[$i]}");
        if ($ok) {
          $str_mess .= $h_id[$i] . ", ";
        }
      }
      $mess .= substr($str_mess, 0, - 2) . "</strong> qua danh muc ID <b>{$cat_chose}</b>";
      //insert adminlog
      $func->insertlog("Move Download", $vnT->input['act'], $cat1);
      $err = $func->html_mess($mess);
      $url = $this->linkUrl . "&sub=move";
      flush();
      echo $func->html_redirect($url, $err);
      exit();
    }
    if ($vnT->input['cat1'])   $cat1 = (int) $vnT->input['cat1'];
    $data['list_cat'] = Get_Category("cat1", $cat1, $lang, "onChange=\"LoadAjax('GetList','ext_list','cat_id='+this.value+'&lang=$lang',1); LoadAjax('GetNum','ext_cat1','cat_id='+this.value+'&lang=$lang',0) \"");
    $data['list_cat_chose'] = Get_Category("cat_chose", $cat_chose, $lang, "onChange=\"LoadAjax('GetNum','ext_cat2','cat_id='+this.value+'&lang=$lang',0);\"");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=move";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("move");
    return $this->skin->text("move");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
	
		
    // Del Image
    $query = $DB->query("SELECT picture FROM video_category WHERE cat_id IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query)) {
      $file_pic = MOD_DIR_UPLOAD . "category/" . $img['picture'];
      if ((file_exists($file_pic)) && (! empty($img['picture'])))
        unlink($file_pic);
    }
    // End del image
    $	$res = $DB->query("SELECT * FROM video_category WHERE cat_id IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
        $res_d = $DB->query("SELECT id FROM video_category_desc WHERE cat_id=".$row['cat_id']." AND lang<>'".$lang."' ");
				if(!$DB->num_rows($res_d))
				{
					$DB->query("DELETE FROM video_category WHERE  cat_id=".$row['cat_id'] ); 
				}	
				$DB->query("DELETE FROM video_category_desc WHERE  cat_id=".$row['cat_id']." AND lang='".$lang."' ");	
      }
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    } 
		
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['cat_id'];
    $row_id = "row_" . $id;
    $output['row_id'] = $row_id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    if ($row['picture']) {
      $output['picture'] = "<img src=\"" . MOD_DIR_UPLOAD . "category/" . $row['picture'] . "\" width=50 />";
    } else
      $output['picture'] = "No image";
    $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['cat_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $text_edit = "video_category_desc|cat_name|id=" . $row['id'];
    $cat_name = "<span id='edit-text-" . $id . "' onClick=\"quick_edit('edit-text-" . $id . "','$text_edit');\">" . $func->HTML($row['cat_name']) . "</span>";
		
		
		 
    if ($row['ext']) {
      $output['cat_name'] = $row['ext'] . "&nbsp;" . $cat_name;
      $output['show_home'] = "---";
      $output['is_focus'] = "---"; 			
    } else {
      $output['cat_name'] = "<strong>" . $cat_name . "</strong>";
      $output['show_home'] = vnT_HTML::list_yesno("show_home[{$id}]", $row['show_home'], "onchange='javascript:do_check($id)'");
      $output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    }
   	$output['link_cat'] = "video/" . $row['friendly_url'] ."-".$row['cat_id'].".html";
		 
    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '" ><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '" ><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))    $arr_order = $vnT->input["txt_Order"];          
					if (isset($vnT->input["is_focus"]))  $is_focus = $vnT->input["is_focus"];   
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['cat_order'] = $arr_order[$h_id[$i]];
            $dup['is_focus'] = $is_focus[$h_id[$i]];
            $ok = $DB->do_update("video_category", $dup, "cat_id={$h_id[$i]}");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        
					case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['display'] = 0;
            $ok = $DB->do_update("video_category_desc", $dup, "lang='$lang' AND cat_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
				case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++)
          {
            $dup['display'] = 1;
            $ok = $DB->do_update("video_category_desc", $dup, "lang='$lang' AND cat_id=" . $h_id[$i]);
            if ($ok)
            {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update video_category_desc SET display=1 WHERE  lang='$lang' AND cat_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update video_category_desc SET display=0 WHERE lang='$lang' AND cat_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
		 
    $where = "";
		$query = $DB->query("SELECT n.cat_id FROM video_category n , video_category_desc nd 
												WHERE n.cat_id=nd.cat_id
												AND nd.lang='$lang'
												AND n.parentid=0 ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)      $p = $num_pages;
    if ($p < 1)      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
		
		$sql = "SELECT * FROM video_category n , video_category_desc nd 
						WHERE n.cat_id=nd.cat_id
						AND nd.lang='$lang'
						AND n.parentid=0
					  ORDER BY  cat_order ASC, n.cat_id ASC  
						LIMIT $start,$n";
    //echo $sql;
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $i = 0;
      while ($row = $DB->fetch_row($reuslt)) {
        $i ++;
				$row['ext_link'] = "&p=".$p ;
        $row['ext'] = "";
        $row_info = $this->render_row($row, $lang);
        $row_info['class'] = ($i % 2) ? "row1" : "row0";
        $this->skin->assign('row', $row_info);
        $this->skin->parse("manage.html_row");
        $n = 1;
        $this->Row_Sub($row['cat_id'], $n, &$i, $lang);
      }
    } else {
      if ($pos) {
        $mess = $vnT->lang['no_have_cat_video'];
      } else {
        $mess = $vnT->lang['select_position'];
      }
      $this->skin->assign('mess', $mess);
      $this->skin->parse("manage.html_row_no");
    }
    $data['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $data['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }

  //===========Row_Sub=========
  function Row_Sub ($cid, $n, &$i, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $textout = "";
    $space = "&nbsp;&nbsp;&nbsp;&nbsp;";
    $n1 = $n;
		
		$sql = "SELECT * FROM video_category n , video_category_desc nd 
						WHERE n.cat_id=nd.cat_id
						AND nd.lang='$lang'
						AND n.parentid='{$cid}'
						ORDER BY n.cat_order ASC, n.cat_id ASC ";
    //	print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    while ($row = $DB->fetch_row($result)) {
      $i ++;
			$row['ext_link'] = "&p=".$_GET['p'] ;
      $row['ext'] = "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
      $width = "";
      for ($k = 1; $k < $n1; $k ++) {
        $width .= $space;
        $row['ext'] = $width . "&nbsp;<img src=\"{$vnT->dir_images}/line3.gif\" align=\"absmiddle\"/>";
      }
      $row_info = $this->render_row($row, $lang);
      $row_info['class'] = ($i % 2) ? "row1" : "row0";
      $this->skin->assign('row', $row_info);
      $this->skin->parse("manage.html_row");
      $n = $n1 + 1;
      $this->Row_Sub($row['cat_id'], $n, &$i, $lang);
    }
  }
  // end class
}
?>