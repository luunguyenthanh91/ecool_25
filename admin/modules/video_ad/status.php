<?php
/*================================================================================*\
|| 							Name code : status.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "video";
  var $action = "status";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_status'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_status'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_status'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $name = $vnT->input['name'];
      // Check for Error
      $res_chk = $DB->query("SELECT * FROM video_status  WHERE name='{$name}' ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
        //upload
      if (empty($err)) {
        if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "status";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = 100;
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
            $file_type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        } else {
          if ($vnT->input['picture']) {
            $up['path'] = MOD_DIR_UPLOAD;
            $up['dir'] = "status";
            $up['url'] = $vnT->input['picture'];
            $up['type'] = "hinh";
            $up['w'] = 100;
            $result = $vnT->File->UploadURL($up);
            if (empty($result['err'])) {
              $picture = $result['link'];
              $file_type = $result['type'];
            } else {
              $err = $func->html_err($result['err']);
            }
          }
        } //end upload
      }
      // insert CSDL
      if (empty($err)) {
        $cot['name'] = $name;
        $cot['picture'] = $picture;
        $cot['title'] = $vnT->input['title'];
        $ok = $DB->do_insert("video_status", $cot);
        if ($ok) {
          $status_id = $DB->insertid();
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $status_id);
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $name = $vnT->input['name'];
      // Check for Error
      $res_chk = $DB->query("SELECT * FROM video_status  WHERE name='{$name}' and status_id<>$id ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
        //upload
      if (empty($err)) {
        if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "") {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "status";
          $up['file'] = $_FILES['image'];
          $up['type'] = "hinh";
          $up['w'] = 100;
          $result = $vnT->File->Upload($up);
          if (empty($result['err'])) {
            $picture = $result['link'];
            $file_type = $result['type'];
          } else {
            $err = $func->html_err($result['err']);
          }
        } else {
          if ($vnT->input['picture']) {
            $up['path'] = MOD_DIR_UPLOAD;
            $up['dir'] = "status";
            $up['url'] = $vnT->input['picture'];
            $up['type'] = "hinh";
            $up['w'] = 100;
            $result = $vnT->File->UploadURL($up);
            if (empty($result['err'])) {
              $picture = $result['link'];
              $file_type = $result['type'];
            } else {
              $err = $func->html_err($result['err']);
            }
          }
        } //end upload
      }
      if (empty($err)) {
        $cot['name'] = $name;
        $cot['title'] = $vnT->input['title'];
        if ($vnT->input['chk_upload'] == 1 || ! empty($picture)) {
          $img_q = $DB->query("SELECT picture FROM video_status WHERE status_id=$id ");
          if ($img = $DB->fetch_row($img_q)) {
            $file_pic = MOD_DIR_UPLOAD . "status/" . $img['picture'];
            if ((file_exists($file_pic)) && (! empty($img['picture'])))
              unlink($file_pic);
          }
          $cot['picture'] = $picture;
        }
        // more here ...
        $ok = $DB->do_update("video_status", $cot, "status_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM video_status  
													WHERE   status_id=$id");
    if ($data = $DB->fetch_row($query)) {
      if (! empty($data['picture'])) {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "status/{$data['picture']}\" ><br>";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    // Del Image
    $query = $DB->query("SELECT picture FROM video_status WHERE status_id IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query)) {
      $file_pic = MOD_DIR_UPLOAD . "status/" . $img['picture'];
      if ((file_exists($file_pic)) && (! empty($img['picture'])))
        unlink($file_pic);
    }
    // End del image
    $query = 'DELETE FROM video_status WHERE status_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['status_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['s_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    if ($row['picture']) {
      $output['picture'] = "<img src=\"" . MOD_DIR_UPLOAD . "status/" . $row['picture'] . "\"  />";
    } else
      $output['picture'] = "No image";
    $output['name'] = "<a href=\"{$link_edit}\">" . $row['name'] . "</a>";
    $text_edit = "video_status|title|id=" . $id;
    $output['title'] = "<strong><span id='edit-text-" . $id . "' onClick=\"quick_edit('edit-text-" . $id . "','$text_edit');\">" . $func->HTML($row['title']) . "</span></strong>";
    $output['is_focus'] = vnT_HTML::list_yesno("is_focus[{$id}]", $row['is_focus'], "onchange='javascript:do_check($id)'");
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          if (isset($vnT->input["is_focus"]))
            $is_focus = $vnT->input["is_focus"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['s_order'] = $arr_order[$h_id[$i]];
            $dup['is_focus'] = $is_focus[$h_id[$i]];
            $ok = $DB->do_update("video_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("video_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("video_status", $dup, "status_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT status_id FROM video_status  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'picture' => "Icon|10%|center" , 
      'name' => $vnT->lang['name'] . "|15%|center" , 
      'title' => $vnT->lang['title'] . "|40%|left" , 
      'is_focus' => "Focus|10%|center" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM video_status  ORDER BY s_order ,status_id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['status_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_status'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>