<?php
/*================================================================================*\
|| 							Name code : funtions_video.php 		 			          	     		  # ||
||  				Copyright © 2008 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 03/02/2008 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/video/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/video/');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn")
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from video_setting WHERE lang='$lang' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}

//-----------------  get_cat_name
function get_cat_name ($cat_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_name from video_category_desc where cat_id =$cat_id and lang='{$lang}' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['cat_name']);
  }
  return $text;
}

//-----------------  get_pic_video
function get_pic_video ($picture, $w = 100)
{
  global $conf, $vnT;
  $out = "";
  if (! empty($picture)) {
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $src = "../vnt_upload/video/" . $picture;
    if ($w < $w_thumb)
      $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
  } else {
    $out = "No Picture";
  }
  return $out;
}

/***** Ham get_parentid *****/
function get_parentid ($cat_id)
{
  global $func, $DB, $conf;
  $cid = list_parentid($cat_id);
  $cid = substr($cid, strrpos($cid, ",") + 1);
  return $cid;
}

function list_parentid ($cat_id)
{
  global $func, $DB, $conf;
  $res = $DB->query("SELECT parentid,cat_id FROM video_category where cat_id in (" . $cat_id . ") and parentid>0  ");
  while ($cat = $DB->fetch_row($res)) {
    $output .= "," . $cat["parentid"];
    $output .= list_parentid($cat['parentid']);
  }
  return $output;
}

//-----------------  get_catCode
function get_catCode ($parentid, $cat_id)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_id,cat_code from video_category where cat_id =$parentid ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['cat_code'] . "_" . $cat_id;
  } else
    $text = $cat_id;
  return $text;
}

/***** Ham List_SubCat *****/
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf, $vnT;
  $output = "";
  $query = $DB->query("SELECT * FROM video_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

//======================= LIST Search =======================
function List_Search ($did)
{
  global $func, $DB, $conf, $vnT;
  $text = "<select size=1 name=\"search\" class='select' >";
  if ($did == "c_id")
    $text .= "<option value=\"c_id\" selected> " . $vnT->lang['id'] . "  </option>";
  else
    $text .= "<option value=\"c_id\"> " . $vnT->lang['id'] . " </option>";
  if ($did == "title")
    $text .= "<option value=\"title\" selected> " . $vnT->lang['video_name'] . " </option>";
  else
    $text .= "<option value=\"title\"> " . $vnT->lang['video_name'] . " </option>";
  if ($did == "date_post")
    $text .= "<option value=\"date_post\" selected> " . $vnT->lang['date_post'] . " (d/m/Y) </option>";
  else
    $text .= "<option value=\"date_post\"> " . $vnT->lang['date_post'] . " (d/m/Y) </option>";
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Cat ****/
function Get_Cat ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
  $text .= "<option value=\"0\">-- Root --</option>";
	$query = $DB->query("SELECT n.*,nd.cat_name FROM video_category n, video_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected style='font-weight:bold;'>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" style='font-weight:bold;' >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub ($did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM video_category n, video_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid
				ORDER BY n.cat_order ASC, n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  return $output;
}

// List_Status 
function List_Status ($selname, $did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select name=\"{$selname}\"  {$ext}   >";
  $text .= "<option value=\"0\" selected>-- Chọn trạng thái --</option>";
  $sql = "SELECT * FROM video_status  
				where display=1 
				order by s_order , status_id DESC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['status_id'] == $did) {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->HTML($row['title']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->HTML($row['title']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//------list_admin
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;

	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- Chọn Admin --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result))
	{
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	
	$text.="</select>";
	return $text;
}

//------get_admin
function get_admin($adminid) {
global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}
function get_code_img ($url_youtube)
{
  global $conf, $func, $vnT;

	$code_img = str_replace("http://www.youtube.com/watch?", "", $url_youtube);
	$code_img = str_replace("https://www.youtube.com/watch?", "", $code_img);
	$code_img = str_replace("//www.youtube.com/watch?", "", $code_img);
	$tmp = explode("&",$code_img);
	foreach($tmp as $vk)
	{
		$tmp_1 = explode("=",$vk);
		$code_img = ($tmp_1[0] == "v") ? $tmp_1[1] : $code_img;
	}

	return $code_img;
}

//================== get_img_youtube =============
function get_img_youtube ($str, $dir, $type="embed")
{
  global $conf, $func, $vnT;
	$text = "";

	if($type == "url_youtube")
	{
		$code_img = str_replace("http://www.youtube.com/watch?v=", "", $str);
		$code_img = str_replace("https://www.youtube.com/watch?v=", "", $code_img);
		$code_img = str_replace("//www.youtube.com/watch?v=", "", $code_img);
		$tmp = explode("&",$code_img);
		$code_img = ($tmp[0]) ? $tmp[0] : $code_img;
		
		$text = save_img_youtube ($code_img, $dir);
		
		return $text;
	}
	
  $tmp = $str;
	$str = str_replace(array('\"',"\'","\&quot;"),'"',$str);

	preg_match_all("/ src=['|\"](.*?)['|\"]/", $str, $arr_src);
	
	foreach($arr_src[1] as $key => $value)
	{
		$code_img = $value;
		if($type == "embed")
		{
			$code_img = str_replace("https://www.youtube-nocookie.com/embed/", "", $code_img);
			$code_img = str_replace("http://www.youtube-nocookie.com/embed/", "", $code_img);
			$code_img = str_replace("//www.youtube-nocookie.com/embed/", "", $code_img);
			$code_img = str_replace("https://www.youtube.com/embed/", "", $code_img);
			$code_img = str_replace("http://www.youtube.com/embed/", "", $code_img);
			$code_img = str_replace("//www.youtube.com/embed/", "", $code_img);
			
			$tmp = explode("?",$code_img);
			$code_img = ($tmp[0]) ? $tmp[0] : $code_img;
			//$code_img = str_replace("?rel=0", "", $code_img);
		}
		
		if($code_img)
		{
			$text = save_img_youtube ($code_img, $dir);
			if ($text) {
				return $text;
				break;
			} 
		}
	}
	return $text;
}

//================== save_img_youtube =============
function save_img_youtube ($code_img="", $dir="")
{
  global $conf, $func, $vnT;
	$text = "";
	
	if ($dir) {
    $path_dir = MOD_DIR_UPLOAD . $dir . "/";
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD . $dir . "/";
  } else {
    $path_dir = MOD_DIR_UPLOAD;
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD;
  }
  $path_thumb = $path_dir . "thumbs";
  if (! is_dir($path_thumb)) {
    @mkdir($path_thumb, 0777);
    @exec("chmod 777 {$path_thumb}");
  }
	
	if($code_img)
	{
		$img_http = "http://img.youtube.com/vi/".$code_img."/0.jpg";
		$file_name = time().".jpg";
		$fname = $path_dir . $file_name;
		if (file_exists($fname)) {
			$fname = $path_dir . time() . "_" . $file_name;
		}

		$file = @fopen($fname, "w");
		if ($f = @fopen($img_http, "r")) {
			while (! @feof($f)) {
				@fwrite($file, fread($f, 1024));
			}
			@fclose($f);
			@fclose($file);
			$url = $rooturl . $file_name; 
			
			if($dir)
			{
				$text = $dir."/".$file_name;
			}
			else
			{
				$text = $file_name;
			}
		} 
	}
	return $text;
}

//======================= LIST Search =======================
function op_view_video ($arr_cur = array())
{
  global $func, $DB, $conf, $vnT;
  $arr_default = array(
		"autoplay" => 0,
		"start" => "00:00:00",
		"theme" => "dark",
		"disablekb" => 0,
		"showinfo" => 1,
		"modestbranding" => 0,
		"controls" => 1,
		"autohide" => 0,
		"color" => "red",
		"allowfullscreen" => 1,
		"loop" => 0,
		"rel" => 1,
		"hd" => 0
	);
	
	$arr_data = array(
		"autoplay" => "Autoplay",
		"start" => "Start Position",
		"theme" => "Theme",
		"disablekb" => "Disable Keyboard Controls",
		"showinfo" => "Show Info(title etc)",
		"modestbranding" => "Modest Branding",
		"controls" => "Controls",
		"autohide" => "Autohide Controls",
		"color" => "Progress Bar Color",
		"allowfullscreen" => "Allow Fullscreen",
		"loop" => "Loop",
		"rel" => "Related Videos",
		"hd" => "High Quality"
	);
	
	$text = "";
	foreach($arr_data as $k => $v)
	{
		$arr_cur[$k] = ($arr_cur[$k]) ? $arr_cur[$k] : $arr_default[$k];
		$checked = array();
		$checked[$arr_cur[$k]] = " checked='checked'";
		
		$v1 = 1;
		$v0 = 0;
		$t1 = "Yes";
		$t0 = "No";
		
		if($k == "start")
		{
			$arr_cur["start"] = ($arr_cur["start"]) ? $arr_cur["start"] : "00:00:00";
			$content = '<input id="op_view_video_'.$k.'" name="op_view_video['.$k.']" type="text" size="10" maxlength="10" value="'.$arr_cur["start"].'" '.$checked[1].'/>(Hour:Min:Sec)';
		}
		else
		{
			if($k == "theme")
			{
				$v1 = "dark";
				$v0 = "light";
				$t1 = "Dark";
				$t0 = "Light";

			}
			elseif($k == "color")
			{
				$v1 = "red";
				$v0 = "white";
				$t1 = "Red";
				$t0 = "White";
			}
			$content = '<input id="op_view_video_'.$k.'" name="op_view_video['.$k.']" type="radio" value="'.$v1.'" '.$checked[$v1].'/> '.$t1.' &nbsp; &nbsp; &nbsp;
			<input id="op_view_video_'.$k.'" name="op_view_video['.$k.']" type="radio" value="'.$v0.'" '.$checked[$v0].'/> '.$t0.'';
		}
		
		$text .= '<tr>
			 <td class="row1" width="30%" >'.$v.': </td>
			 <td class="row0">'.$content.'</td>
			</tr>';
	}
	
	
	
	
  return $text;
}

//================== make_iframe_youtube =============
function make_iframe_youtube ($code_img, $op_view_video=array(), $media_w=700, $media_h=525)
{
  global $conf, $func, $vnT;
	
	$ext = '';
	$attr = '';
	foreach($op_view_video as $k => $v)
	{
		$is_ext = 1;
		
		if($k == "allowfullscreen")
		{
			$is_ext = 0;
		}
		
		if($is_ext == 1)
		{
			if($k == "start")
			{
				$tmp = explode(":",$v);
				$v = $tmp[0]*60*60+$tmp[1]*60+$tmp[2];
			}
			$ext .= ($ext) ?  '&' : '?';
			$ext .= $k.'='.$v;
		}
		else
		{
			$attr .= ' '.$k;
		}
	}
	
	$text = '&lt;iframe width=\&quot;'.$media_w.'\&quot; height=\&quot;'.$media_h.'\&quot; src=\&quot;http://www.youtube.com/embed/'.$code_img.$ext.'\&quot; frameborder=\&quot;0\&quot; '.$attr.'&gt;&lt;/iframe&gt;';	
  
	return $text;
}

//- playFLV
function playFLV ($url, $w = 295, $h = 287,$pic="")
{
  global $vnT, $conf;
  return <<<EOF
<p id="player_media"><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this player.</p></div>
<script type="text/javascript">
	var flashvars = {
		file: "{$url}",
		image: "{$pic}" 
	};
	var params = {
		allowfullscreen: "true"
	};
	var attributes = false ;
	swfobject.embedSWF("{$vnT->conf['rooturl']}modules/video/flvplayer.swf", "player_media", "{$w}", "{$h}", "9.0.0", "expressInstall.swf",flashvars, params, attributes);
</script>
EOF;
}
?>