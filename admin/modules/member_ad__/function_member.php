<?php
/*================================================================================*\
|| 							Name code : funtions_member.php 		 			      	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/member/');
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/member/');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn")
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from member_setting WHERE lang='$lang' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }

  $result = $vnT->DB->query("SELECT * FROM  mem_group");
  while($row = $vnT->DB->fetch_row($result))
  {
    $src = $conf['rooturl'].'vnt_upload/member/'.$row['picture'];
    $alt = $row['g_name'];
    $vnT->setting['g_name'][$row['g_id']] = "<b style='color:#".$row['g_color']."'>".$row['g_name']."</b>";
    $vnT->setting['img_group'][$row['g_id']] = ($row['picture']) ? '<img src="'.$src.'" alt="'.$alt.'" align=absmiddle />' : '';
    $vnT->setting['g_color'][$row['g_id']] = $row['g_color'];
  }

  $vnT->setting['arr_gender'] = array("1"=>"Nam","2"=>'Nữ' );
	$vnT->setting['discount_type'] = array("USD"=>"USD","%"=>"%");
	
  unset($setting);
}


//-----------------  get_cat_name
function get_group_name ($g_id)
{
  global $vnT, $func, $DB, $conf, $lang_acp;
  $text = "";
  $sql = "select * from mem_group where g_id =$g_id ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['g_name'];
  }
  return $text;
}

//-----------------------------List_Cat
function List_Mem_Group ($did = "", $submit)
{
  global $vnT, $conf, $DB, $func;
  if ($submit)
    $text = "<select size=1 id=\"mem_group\" name=\"mem_group\" onChange=\"submit();\" >";
  else
    $text = "<select size=1 id=\"mem_group\" name=\"mem_group\" >";
  $text .= "<option value=\"0\">-- Please choose --</option>";
  $query = $DB->query("SELECT * FROM mem_group WHERE display=1 order by g_order");
  while ($group = $DB->fetch_row($query)) {
    $group_name = $group['g_name'];
    if ($group['g_id'] == $did)
      $text .= "<option value=\"{$group['g_id']}\" selected>{$group_name}</option>";
    else
      $text .= "<option value=\"{$group['g_id']}\" >{$group_name}</option>";
  }
  $text .= "</select>";
  return $text;
}

//----List_Gender
function List_Gender ($did, $ext="")
{
	global $vnT,$func, $DB, $conf;
	$text = "";
	foreach ($vnT->setting['arr_gender'] as $key => $value)
	{
		$checked = ($did==$key) ? " checked " : "";
		$text .= '<input name="gender" id="gender" type="radio" value="'.$key.'" '.$checked.' align="absmiddle" /> '.$value.' &nbsp; '	 ;
	}
	return  $text ;
}

//------- List_Search ---------------------
function List_Search ($did,$ext="")
{
  global $vnT,$func, $DB, $conf;
	$arr_search = array("mem_id"=>"Member ID" ,"username"=>"Username" ,"email"=>"Email"  ,"full_name"=> $vnT->lang['full_name'] ,"date_join"=> $vnT->lang['date_join']  );
  $text = "<select size=1 name=\"search\" id='search' class='select' >";
	foreach ($arr_search as $key => $value)
	{
		$selected = ($key==$did) ? "selected"	 : "";
		$text .= "<option value=\"{$key}\" {$selected} > ".$value." </option>";
	} 
	
  $text .= "</select>";
  return $text;
}


//------- ListDay ---------------------
function ListDay ($did)
{
  global $input, $conf, $vnT;
  $text = '<select name="day"  style="width:50px; text-align:center;">';
  for ($i = 1; $i <= 31; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}

//------- ListMonth ---------------------
function ListMonth ($did)
{
  global $input, $conf, $vnT;
  $text = '<select name="month"  style="width:50px; text-align:center;">';
  for ($i = 1; $i <= 12; $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}

//------- ListYear ---------------------
function ListYear ($did)
{
  global $input, $conf, $vnT;
  $text = '<select name="year"  style="width:70px; text-align:center;">';
  for ($i = 1900; $i <= date("Y"); $i ++) {
    if ($i < 10)
      $value = "0" . $i;
    else
      $value = $i;
    if ($value == $did) {
      $text .= "<option value=\"{$value}\" selected >" . $value . "</option>";
    } else {
      $text .= "<option value=\"{$value}\" >" . $value . "</option>";
    }
  }
  $text .= '</select>';
  return $text;
}

//-----------------  Shipping_Country
function List_Country ($did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"country\" id=\"country\" class='select'  {$ext}   >";
  $sql = "SELECT * FROM iso_countries where display=1 order by name ASC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['iso'] == $did) {
      $text .= "<option value=\"{$row['iso']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['iso']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

function get_country_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select name from iso_countries where iso='$code' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

//-----------------  List_City
function List_City ($country, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"city\" id=\"city\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn thành phố --</option>";
  $sql = "SELECT * FROM iso_cities where display=1 and country='$country'  order by c_order ASC , name ASC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['id'] == $did) {
      $text .= "<option value=\"{$row['code']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['code']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

function get_city_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = $code;
  $sql = "select name from iso_cities where id=$code ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

//-----------------  List_State
function List_State ($city, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $sql = "SELECT * FROM iso_states where display=1 and city='$city'  order by s_order ASC , name ASC ";
  $result = $DB->query($sql);
  if ($num = $DB->num_rows($result)) {
    $text = "<select name=\"state\" id=\"state\" class='select'  {$ext}   >";
    $text .= "<option value=\"\" selected>-- Chọn quận huyện --</option>";
    while ($row = $DB->fetch_row($result)) {
      if ($row['id'] == $did) {
        $text .= "<option value=\"{$row['id']}\" selected>" . $func->HTML($row['name']) . "</option>";
      } else {
        $text .= "<option value=\"{$row['id']}\">" . $func->HTML($row['name']) . "</option>";
      }
    }
    $text .= "</select>";
  } else {
    $text = '<input type="text" size="50" class="textfiled" value="' . $did . '" name="state" id="state" />';
  }
  return $text;
}

/*-------------- get_state_name --------------------*/
function get_state_name ($code)
{
  global $func, $DB, $conf, $vnT;
  $text = $code;
  $result = $DB->query("SELECT name FROM iso_states WHERE id='$code' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

//-----------------------------list_group_default
function list_group_default ($did = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select size=1 id=\"group_default\" name=\"group_default\" >";
  $query = $DB->query("SELECT * FROM mem_group WHERE display=1 order by g_order");
  while ($group = $DB->fetch_row($query)) {
    $group_name = $group['g_name'];
    if ($group['g_id'] == $did)
      $text .= "<option value=\"{$group['g_id']}\" selected>{$group_name}</option>";
    else
      $text .= "<option value=\"{$group['g_id']}\" >{$group_name}</option>";
  }
  $text .= "</select>";
  return $text;
}

function get_total_price ($mem_id, $where = "")
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select sum(total_price) as totals from order_sum where mem_id=$mem_id $where ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['totals'];
  }
  return get_price_pro($text);
}

function get_total_point ($mem_id, $where)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select sum(mem_point_earned) as totals from order_sum where mem_id=$mem_id $where ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['totals'];
  }
  return $text;
}

//---------
function List_Search_Order ($did, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"search\" id=\"search\" {$ext} >";
  if ($did == "order_code")
    $text .= "<option value=\"order_code\" selected> ORDER ID </option>";
  else
    $text .= "<option value=\"order_code\" >  ORDER ID </option>";
  if ($did == "d_name")
    $text .= "<option value=\"d_name\" selected> Tên khách hàng </option>";
  else
    $text .= "<option value=\"d_name\"> Tên khách hàng </option>";
  if ($did == "d_email")
    $text .= "<option value=\"d_email\" selected> Email khách hàng </option>";
  else
    $text .= "<option value=\"d_email\"> Email khách hàng </option>";
  if ($did == "date_order")
    $text .= "<option value=\"date_order\" selected> Ngày đặt hàng (d/m/Y) </option>";
  else
    $text .= "<option value=\"date_order\"> Ngày đặt hàng (d/m/Y) </option>";
  $text .= "</select>";
  return $text;
}

//------------- 
function List_Status ($did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"status\" id=\"status\" {$ext} >";
  $text .= "<option value=\"0\">--- Chọn trạng thái ---</option>";
  $sql = "SELECT * FROM order_status where display=1 order by s_order ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['status_id'] == $did) {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->fetch_content($row['title'], $lang) . "</option>";
    } else {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->fetch_content($row['title'], $lang) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//----------- get_status_order
function get_status_order ($id, $lang = "vn")
{
  global $func, $DB, $conf;
  $text = "UnKnow";
  $result = $DB->query("SELECT * FROM order_status where status_id=$id");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
    if ($row['is_default']) {
      $text = "<b class=red>" . $text . "</b>";
    }
    if ($row['is_complete']) {
      $text = "<b class=blue>" . $text . "</b>";
    }
  }
  return $text;
}

//----------- get_status_complete
function get_status_complete ()
{
  global $func, $DB, $conf;
  $text = "5";
  $result = $DB->query("SELECT * FROM order_status where is_complete=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}

//----------- get_status_customer
function get_status_customer ()
{
  global $func, $DB, $conf;
  $text = "7";
  $result = $DB->query("SELECT * FROM order_status where is_customer=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}
 
/*-------------- get_price_pro --------------------*/
function get_price_pro ($price,$default="0"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = sprintf( "%.2f",$price );
		$nguyen = (int) $price;
		$dot =strpos($price,".");
		if ($dot)
			$du =  substr ($price,strpos($price,"."),3);
		else $du = "";
		
		$price = "$".$func->format_number($nguyen).$du ;
	}else{
		$price = $default;
	}
	return $price;
}
 

/*-------------- get_price_lang --------------------*/
function get_price_lang ($price, $lang = "vn", $default = "Call")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    if ($lang == "en") {
      $nguyen = (int) $price;
      $dot = strpos($price, ".");
      if ($dot)
        $du = substr($price, strpos($price, "."), 3);
      else
        $du = "";
      $price = "$" . $func->format_number($nguyen) . $du;
    } else {
      if ($conf['rate'])
        $rate = intval($conf['rate']);
      else
        $rate = 1;
      $price = $price * $conf['rate'];
      $price = $func->format_number($price) . " VNĐ";
    }
  } else {
    $price = $default;
  }
  return $price;
}

function get_method_name ($table, $name, $lang = "vn")
{
  global $DB, $input, $func, $vnT, $conf;
  $text = "";
  $reuslt = $DB->query("select title from {$table} where name='{$name}' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
  }
  return $text;
}

/* MORE /*/
//============================= list_member
function list_member ($ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"list_cat[]\" id=\"list_cat\"  multiple {$ext} >";
  $sql = "SELECT m.* ,  (SELECT sum(total_price) FROM order_sum o WHERE m.mem_id=o.mem_id group by o.mem_id  )  as total_order
				FROM members	m 
				WHERE m.m_status=1
				order by  total_order DESC";
  $result = $DB->query($sql);
  $i = 0;
  while ($row = $DB->fetch_row($result)) {
    $i ++;
    $username = $func->HTML($row['cat_name']);
    $text .= "<option value=\"{$row['mem_id']}\">  " . $row['full_name'] . " [" . $row['mem_code'] . "] (" . get_price_pro((int) $row['total_order']) . ") </option>";
  }
  $text .= "</select>";
  return $text;
}

//----------------- list_member_chose
function list_member_chose ($ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"cat_chose[]\" id=\"cat_chose\"  multiple  {$ext} >";
  $text .= "</select>";
  return $text;
}

function get_format_money ($price)
{
  global $func, $DB, $conf, $vnT;
  $price = $func->format_number($price) . " VNĐ";
  return $price;
}
?>