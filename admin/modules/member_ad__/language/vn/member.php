<?php
$lang = array(
  //Quản lý thanh vien
  'manage_member' => "Quản lý thành viên" , 
  'add_member' => "Thêm thành viên mới" , 
  'edit_member' => "Cập nhật thành viên" , 
  'no_have_member' => 'Chưa có thành viên nào' , 
  'manage_mem_group' => "Quản lý nhóm" , 
  'add_mem_group' => "Thêm nhóm mới" , 
  'edit_mem_group' => "Cập nhật nhóm" , 
  'no_have_mem_group' => 'Chưa có nhóm nào' , 
  'username_existed' => 'Tài khoản tồn tại' , 
  'email_existed' => 'Email tồn tại' , 
  'group_name' => 'Tên nhóm' , 
  'group_default' => 'Nhóm mặc định khi đăng ký ' , 
  'group_member' => 'Nhóm thành viên' , 
  'hide_email' => 'Ẩn Email' , 
  'newsletter' => 'Nhân thông tin mới BQT' , 
	'zipcode' => 'Zipcode' , 
  'state' => 'Quận huyện' , 
  'info_member' => 'THÔNG TIN TÀI KHOẢN' , 
  'status_member' => 'THÔNG TIN HOẠT ĐỘNG' , 
  'note_password' => 'Điền mật khẩu mới để thay đổi mật khẩu (Nếu để trống sẽ giữ lại mật khẩu cũ) ' , 
  'date_join' => 'Ngày đăng ký' , 
  'date_last_login' => 'Lần đăng nhập gần nhất' , 
  'num_login' => 'Số lần đăng nhập' , 
  'announce_for_member' => 'Thống báo tới thành viên' , 
  'banned_account' => 'Khóa tài khoản' , 
  'active_account' => 'Kích hoạt tài khoản' , 
  'del_account' => 'Xóa tài khoản' , 
  'not_active' => 'Chưa kích hoạt' , 
  'active' => 'Đã kích hoạt' , 
  'banned' => 'Bị khóa' , 
  'no_login' => 'Chưa login lần nào' , 
  'mess_active_success' => 'Kích hoạt tài khoản thành công' , 
  'mess_ban_success' => 'Khóa tài khoản thành công' , 
  'mess_unban_success' => 'Tài khoản đã được kích hoạt lại ' , 
  'all_status' => 'Tất cả trạng thái' , 
  'manage_member_log' => 'Member statistics' , 
  'detail_member_log' => 'Detail log' , 
  'no_have_member_log' => 'No have member logs' , 
  'option' => 'OPTION' , 
  //seting
  'manage_setting' => 'Cấu hình thành viên' , 
  'w_avatar' => 'Chiều ngang hình hình Avatar' , 
  'active_email' => 'Kích hoạt qua Email' , 
  'insert_maillist' => 'Thêm email vào maillist khi đăng ký' , 
  'term_of_register' => 'Điều khoản đăng ký' , 
  'active_method' => 'Kích hoạt tài khoản',
	'active_method_0'	=>	'Không cần kích hoạt',
	'active_method_1'	=>	'Kích hoạt qua email',
	'active_method_2'	=>	'Kích hoạt bởi BQT',
	'mess_join'	=>	'Lợi ích của thành viên',
	
	);
?>