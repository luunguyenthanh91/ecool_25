<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();
//Functions
include ($conf['rootpath'] . 'includes/class_functions.php');
include($conf['rootpath'] . 'includes/admin.class.php');
$func  = new Func_Admin;
$conf = $func->fetchDbConfig($conf);

// Ham Get_Sub
/*-------------- get_price_pro --------------------*/
function get_price_pro ($price, $default = "0 đ")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    $price = $func->format_number($price) . " đ";
  } else {
    $price = $default;
  }
  return $price;
}

function get_total_price ($mem_id, $where = "")
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select sum(total_price) as totals from order_sum where mem_id=$mem_id $where ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['totals'];
  }
  return get_price_pro($text);
}
$list_member = $_POST['list_member'];
$lang = ($_POST['lang']) ? $_POST['lang'] : "vn";
$arr_mem = explode(",", $list_member);
$count = count($arr_mem) - 1;
$id = rand(1, $count);
$mem_id = $arr_mem[$id];
$res = $DB->query("SELECT mem_id,mem_code,username,full_name from members where mem_id=" . $mem_id);
if ($row = $DB->fetch_row($res)) {
  $jsout = $row['full_name'] . ' [<b class=font_err>' . $row['mem_code'] . '</b>] (' . get_total_price($mem_id) . ')  <input name="mem_id" id="mem_id" type="hidden" value="' . $mem_id . '" />';
} else {
  $jsout = "Error ! Vui lòng chọn lại khách hàng ";
}
flush();
echo $jsout;
exit();
?>