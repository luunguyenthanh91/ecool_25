<?php
/*================================================================================*\
|| 							Name code : funtion_product.php	 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}

//---------
function List_Search_Order ($did, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"search\" id=\"search\" {$ext} >";
  if ($did == "order_code")
    $text .= "<option value=\"order_code\" selected> ORDER ID </option>";
  else
    $text .= "<option value=\"order_code\" >  ORDER ID </option>";
  if ($did == "d_name")
    $text .= "<option value=\"d_name\" selected> Tên khách hàng </option>";
  else
    $text .= "<option value=\"d_name\"> Tên khách hàng </option>";
  if ($did == "d_email")
    $text .= "<option value=\"d_email\" selected> Email khách hàng </option>";
  else
    $text .= "<option value=\"d_email\"> Email khách hàng </option>";
  if ($did == "date_order")
    $text .= "<option value=\"date_order\" selected> Ngày đặt hàng (d/m/Y) </option>";
  else
    $text .= "<option value=\"date_order\"> Ngày đặt hàng (d/m/Y) </option>";
  $text .= "</select>";
  return $text;
}

//------------- 
function List_Status ($did, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"status\" id=\"status\" {$ext} >";
  $text .= "<option value=\"0\">--- Chọn trạng thái ---</option>";
  $sql = "SELECT * FROM order_status where display=1 order by s_order ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['status_id'] == $did) {
      $text .= "<option value=\"{$row['status_id']}\" selected>" . $func->fetch_content($row['title'], $lang) . "</option>";
    } else {
      $text .= "<option value=\"{$row['status_id']}\">" . $func->fetch_content($row['title'], $lang) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//----------- get_status_order
function get_status_order ($id, $lang = "vn")
{
  global $func, $DB, $conf;
  $text = "UnKnow";
  $result = $DB->query("SELECT * FROM order_status where status_id=$id");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
    if ($row['is_default']) {
      $text = "<b class=red>" . $text . "</b>";
    }
    if ($row['is_complete']) {
      $text = "<b class=blue>" . $text . "</b>";
    }
  }
  return $text;
}

//----------- get_status_complete
function get_status_complete ()
{
  global $func, $DB, $conf;
  $text = "5";
  $result = $DB->query("SELECT * FROM order_status where is_complete=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}

//----------- get_status_customer
function get_status_customer ()
{
  global $func, $DB, $conf;
  $text = "7";
  $result = $DB->query("SELECT * FROM order_status where is_customer=1");
  if ($row = $DB->fetch_row($result)) {
    $text = $row['status_id'];
  }
  return $text;
}

/*-------------- get_price_pro --------------------*/
function get_price_pro ($price, $default = "0 đ")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    $price = $func->format_number($price) . " đ";
  } else {
    $price = $default;
  }
  return $price;
}

/*-------------- get_price_lang --------------------*/
function get_price_lang ($price, $lang = "vn", $default = "Call")
{
  global $func, $DB, $conf, $vnT;
  if ($price) {
    if ($lang == "en") {
      $nguyen = (int) $price;
      $dot = strpos($price, ".");
      if ($dot)
        $du = substr($price, strpos($price, "."), 3);
      else
        $du = "";
      $price = "$" . $func->format_number($nguyen) . $du;
    } else {
      if ($conf['rate'])
        $rate = intval($conf['rate']);
      else
        $rate = 1;
      $price = $price * $conf['rate'];
      $price = $func->format_number($price) . " VNĐ";
    }
  } else {
    $price = $default;
  }
  return $price;
}

function get_method_name ($table, $name, $lang = "vn")
{
  global $DB, $input, $func, $vnT, $conf;
  $text = "";
  $reuslt = $DB->query("select title from {$table} where name='{$name}' ");
  if ($row = $DB->fetch_row($result)) {
    $text = $func->fetch_content($row['title'], $lang);
  }
  return $text;
}
?>