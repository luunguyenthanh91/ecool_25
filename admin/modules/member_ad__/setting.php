<?php
/*================================================================================*\
|| 							Name code : member.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "member";
  var $action = "setting";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {      
      case 'edit':
	   		 $nd['content']=$this->do_Edit($lang);
      break;
			default:
        $nd['f_title'] = $vnT->lang['manage_setting'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
		$nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
	
	/**
   * function do_Edit 
   * Cap nhat gioi thieu 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;

    if ($vnT->input['do_submit']) {
      $data = $_POST;
			$res_check = $DB->query("select * from member_setting where id=1 ");
      if($row = $DB->fetch_row($res_check))
			{
				foreach ($row as $key => $value) 
				{ 
					if ($key != 'id' && isset($vnT->input[$key])) {
						if ($key == 'term_of_register' ||$key == 'mess_join' || $key=='thongtin_vipham' ||$key == 'mess_register_success') {
							$dup[$key] = $DB->mySQLSafe($data[$key]);
						} else {
							$dup[$key] = $vnT->input[$key];
						}
					}
				}
				$res_lang = $DB->query("SELECT id FROM member_setting WHERE lang='$lang'");
				if($DB->num_rows($res_lang))
				{
					$ok = $DB->do_update("member_setting", $dup, "lang='$lang'");	
				}else{
					$dup['lang'] = $lang ;
					$ok = $DB->do_insert("member_setting", $dup );	
				}				
			}
			
			//xoa cache
      $func->clear_cache();
			//insert adminlog
      $func->insertlog("Setting", $_GET['act'], $id);
			$err = $vnT->lang["edit_success"];
			$url = $this->linkUrl;
			$func->html_redirect($url, $err);
		}
  }
	
  /**
   * function do_Manage() 
   * Quan ly record
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $result = $DB->query("select * from member_setting  where lang='$lang' ");
    $data = $DB->fetch_row($result);
    $data['active_method'] = vnT_HTML::selectbox("active_method", array(
      '0' => $vnT->lang['active_method_0'] , 
      '1' => $vnT->lang['active_method_1'] , 
      '2' => $vnT->lang['active_method_2']),$data['active_method'] );
    $data['group_default'] = list_group_default($data['group_default']);
    $data['insert_maillist'] = vnT_HTML::radio_yesno("insert_maillist", $data['insert_maillist']);
		$data['mess_join'] = $vnT->editor->doDisplay('mess_join', $data['mess_join'], '100%', '250', "Normal");
		$data['term_of_register'] = $vnT->editor->doDisplay('term_of_register', $data['term_of_register'], '100%', '250', "Normal");

    $data['mess_register_success'] = $vnT->editor->doDisplay('mess_register_success', $data['mess_register_success'], '100%', '250', "Normal");
    $data['link_action'] = $this->linkUrl."&sub=edit";				
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
		
  }

  
  // end class
}
?>