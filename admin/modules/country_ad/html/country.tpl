<!-- BEGIN: edit -->
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			
				name: {
					required: true,
					minlength: 3
				},
				iso: {
					required: true 
				}
	    },
	    messages: {
	    	
				name: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} ,
				iso: {
						required: "{LANG.err_text_required}" 
				} 
		}
	});
});
</script>

{data.err}
<br>


      <form action="{data.link_action}" method="post" name="myForm" id="myForm"   class="validate">
        <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
  
          <tr class="form-required" >
            <td  class="row1" width="30%"> Code name : </td>
            <td class="row0"><input name="iso" id="iso" type="text"  size="10" maxlength="250" value="{data.iso}" /> 
            </td>
		  </tr>
      
      <tr>
            <td class="row1"> Code name 3 : </td>
            <td class="row0"><input name="iso3" type="text"  size="10" maxlength="250" value="{data.iso3}" /> 
            </td>
		  </tr>
          
          
          <tr class="form-required" >
            <td class="row1" >Tên quốc gia : </td>
            <td class="row0"><input name="name"   type="text" id="name" size="50" maxlength="250" value="{data.name}" /></td>
          </tr>
          
		 


          <tr align="center">
						<td class="row1" >&nbsp; </td>
            <td class="row0">
            <input type="hidden" name="do_submit" value="1" />
            <input type="submit" name="btnSubmit" value="Submit" class="button" / >
            <input type="reset" name="Submit2" value="Reset" class="button" />           
             </td></tr>
        </table>
    </form>
<br>
 
<!-- END: edit -->

<!-- BEGIN: manage -->
{data.err}
<form action="" method="post">


<br />
<table width="100%"  border="0" align="center" cellspacing="2" cellpadding="2" class="tableborder">
  <tr>
    <td ><strong>Tổng số  : </strong> <span class="font_err"><strong>{data.totals}</strong></span></td>
  </tr>
  <tr>

	  <td ><strong>Tìm theo :</strong>  {data.list_search} <strong> Từ khóa :</strong> <input name="keyword"  value="{data.keyword}"type="text"> <input name="btnSearch" type="submit" value=" Search ! "></td>
  </tr>
  <tr>
    <td ><strong>Trạng thái  :</strong> {data.list_display}</td>
  </tr>
</table>
</form>
<br />
{data.table_list}
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1">
  <tr>
    <td  height="30">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->