<?php
/*================================================================================*\
|| 							Name code : funtion_product.php	 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}

//====== List_Display
  function List_Display ($did = -1, $ext = "")
  {
    global $func, $DB, $conf, $vnT;
    $text = "<select size=1 name=\"display\" id=\"display\" class='select' {$ext} >";
    $text .= "<option value=\"-1\"> Tất cả </option>";
    if ($did == "1")
      $text .= "<option value=\"1\" selected> Hiển thị </option>";
    else
      $text .= "<option value=\"1\" > Hiển thị </option>";
    if ($did == "0")
      $text .= "<option value=\"0\" selected> Ần </option>";
    else
      $text .= "<option value=\"0\">Ần</option>";
    $text .= "</select>";
    return $text;
  }

function get_city_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = $code;
  $sql = "select name from iso_cities where code='$code' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

function get_country_name ($code)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select name from iso_countries where iso='$code' ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $func->HTML($row['name']);
  }
  return $text;
}

//-----------------  List_Country
function List_Country ($did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"country\" id=\"country\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn quốc gia --</option>";
  $sql = "SELECT * FROM iso_countries where display=1 order by name ASC ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['iso'] == $did) {
      $text .= "<option value=\"{$row['iso']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['iso']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_City
function List_City ($did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"city\" id=\"city\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn tỉnh thành --</option>";
  $sql = "SELECT * FROM iso_cities where display=1  order by c_order ASC , name ASC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['id'] == $did) {
      $text .= "<option value=\"{$row['id']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['id']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}

//-----------------  List_State
function List_State ($city, $did = "", $ext = "")
{
  global $vnT, $conf, $DB, $func;
  $text = "<select name=\"state\" id=\"state\" class='select'  {$ext}   >";
  $text .= "<option value=\"\" selected>-- Chọn Quận huyện--</option>";
  $sql = "SELECT * FROM iso_states where display=1 and city='$city'  order by s_order ASC , name ASC  ";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    if ($row['id'] == $did) {
      $text .= "<option value=\"{$row['id']}\" selected>" . $func->HTML($row['name']) . "</option>";
    } else {
      $text .= "<option value=\"{$row['id']}\">" . $func->HTML($row['name']) . "</option>";
    }
  }
  $text .= "</select>";
  return $text;
}
?>