<!-- BEGIN: edit -->
<link href="{DIR_JS}/metabox/seo-metabox.css" rel="stylesheet" type="text/css" />
<style>
  span.text_chose {
    position: absolute;
    margin-left: 3px;
  }
</style>
<script type="text/javascript" src="{DIR_JS}/metabox/seo-metabox.js"></script>
<script language="javascript" >
  var wpseo_lang = 'en';
  var wpseo_meta_desc_length = '155';
  var wpseo_title = 'p_name';
  var wpseo_content = 'metadesc';
  var wpseo_title_template = '%%title%%';
  var wpseo_metadesc_template = '';
  var wpseo_permalink_template = '{CONF.rooturl}%postname%.html';
  var wpseo_keyword_suggest_nonce = 'a7c4d81c79'; 
  $(document).ready(function() {
  	$('#myForm').validate({
  	  rules: {
  			p_name: {
  				required: true,
  				minlength: 3
  			}
      },
      messages: {
        p_name: {
  				required: "{LANG.err_text_required}",
  				minlength: "{LANG.err_length} 3 {LANG.char}" 
  			}
  	  }
  	});
  });
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
<div class="boxAdminForm">
  <div class="block-left">
    <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
      <tr class="form-required">
        <td class="row1">Tiêu đề: </td>
        <td class="row0">
          <input name="p_name" id="p_name" type="text" size="60" maxlength="250" value="{data.p_name}" onkeyup="vnTMXH.setTitle(this.value)" class="form-control">
        </td>
      </tr>
      <tr>
        <td class="row1">Link 360 : </td>
        <td class="row0" align="left">
          <input name="link360" id="link360" type="text" size="60" value="{data.link360}" class="form-control">
        </td>
      </tr>
      <tr>
        <td class="row1" width="20%">{LANG.picture}: </td>
        <td class="row0">
          <div id="ext_picture" class="picture" >{data.pic}</div>
          <input type="hidden" name="picture" id="picture" value="{data.picture}" />
          <div id="btnU_picture" class="div_upload" {data.style_upload}><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
        </td>
      </tr>
    </table>
  </div>
  <div class="block-right">
    <div class="desc">
      <table width="100%"  border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
        <tr class="row_title" >
          <td class="font_title"><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Search Engine Optimization :  </td>
        </tr>
      </table>
      <div class="desc_content" style="background: #ffffff">
        <div class="general">
          <h4 class="wpseo-heading" style="display: none;">General</h4>
          <table width="100%" border="0" cellspacing="1" cellpadding="1" class="admintable">
            <tr>
              <td class="row1" width="20%"><label for="yoast_wpseo_snippetpreview">Snippet Preview:</label></td>
              <td class="row0">
                <div id="wpseosnippet">
                  <a href="#" class="wpseo_title"></a><br/>
                  <a class="wpseo_url"  href="#"></a>
                  <p class="desc"><span class="content" style="color: rgb(136, 136, 136);"></span></p>
                </div>
              </td>
            </tr>
            <tr>
              <td class="row0" ><label ><strong>Friendly<br>URL :</strong></label></td>
              <td class="row0">
                <input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield" style="width:98%"><br>
                <span class="font_err">({LANG.mess_friendly_url})</span>
              </td>
            </tr>
            <tr>
              <td class="row1"><label for="yoast_wpseo_title">Friendly Title:</label></td>
              <td class="row0"><input type="text" class="textfield" value="{data.friendly_title}" name="friendly_title" id="friendly_title"  style="width:98%"   /><br/><p>Title display in search engines is limited to 70 chars, <span id="friendly_title-length"><span class="good">70</span></span> chars left.<br/></td>
            </tr>
            <tr>
              <td class="row1"><label for="metadesc">Meta Description:</label></td>
              <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" class="textarea"  style="width:98%">{data.metadesc}</textarea><p>The <code>meta</code> description will be limited to 155 chars (because of date display), <span id="metadesc-length"><span class="good">155</span></span> chars left. </p><div id="metadesc_notice"></div></td>
            </tr>
            <tr>
              <td class="row1" ><label for="metakey">Meta Keyword:</label></td>
              <td class="row0"><input type="text" class="textfield" value="{data.metakey}" name="metakey"  id="metakey"  style="width:98%"/><br/><p></p><div style="width: 300px;" class="alignright"><p id="related_keywords_heading" style="display: none;">Related keywords:</p><div id="wpseo_tag_suggestions"></div></div>
              </td>
            </tr>
          </table>
        </div>
        <div class="results" style="padding: 10px;">
          <div id="focuskwresults">
            <div class="article_heading">
              <div class="label">Article Heading</div><span class="wrong">NO</span></div>
            <div class="page_url"><div class="label">Page URL</div><span class="wrong">NO</span></div>
            <div class="page_title"><div class="label">Page title</div><span class="wrong">NO</span></div>
            <div class="meta_desc">
              <div class="label">Meta description</div><span class="wrong">NO</span>
            </div>
            <div class="content_result">
              <div class="label">Content</div><span class="wrong">NO</span></div>
            </div>
          <div class="clear"></div>
        </div>
      </div>
    </div><br/>
    <div class="desc">
      <table width="100%" border="0" cellspacing="2" cellpadding="2" align=center class="admintable desc_title">
        <tr class="row_title" >
          <td class="font_title" ><img src="{DIR_IMAGE}/toggle_minus.png" alt="bt_add" title="Collapse" align="absmiddle"/> Xem Demo tương tác FaceBook : </td>
        </tr>
      </table>
      <div class="desc_content" style="background: #ffffff; padding: 10px;">
        <div class="divFacebook"> {data.img_mxh}
          <div class="face-info">
            <div class="title_mxh" id="title_mxh" >{data.friendly_title}</div>
            <div class="link_mxh" id="link_mxh" >{data.link_mxh}</div>
            <div class="description_mxh" id="description_mxh" >{data.metadesc}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="row0" align="center" height="50" >
      <input type="hidden" name="do_submit"	 value="1" />
      <input type="submit" name="btnAdd" value="{LANG.btn_submit}" class="button">&nbsp;
      <input type="reset" name="btnReset" value="{LANG.btn_reset}" class="button">&nbsp;
    </td>
  </tr>
</table>
</form>
<!-- END: edit -->

<!-- BEGIN: manage -->
<div class="box-search">
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  
  <tr>
    <td width="15%" align="left"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left"><b class="font_err">{data.totals}</b></td>
  </tr>
</table>
</form>
</div>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->