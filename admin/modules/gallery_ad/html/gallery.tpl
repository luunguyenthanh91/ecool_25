<!-- BEGIN: edit -->
<script language=javascript>
$(document).ready(function() {
	$('#myForm').validate({
		rules: {			

				title: {
					required: true,
					minlength: 3
				}
	    },
	    messages: {

				title: {
						required: "{LANG.err_text_required}",
						minlength: "{LANG.err_length} 3 {LANG.char}" 
				} 
		}
	});
});
</script>
 
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm"  class="validate"  >

  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<tr class="form-required">
       <td class="row1">{LANG.cat_name}</td>
       <td class="row0">{data.list_cat}</td>
    </tr>
    
    <tr class="form-required">
     <td class="row1" width="20%">{LANG.gallery_name}: </td>
     <td class="row0"><input name="title" id="title" type="text" size="70" maxlength="250" value="{data.title}"></td>
    </tr>
    
   <tr >
     <td class="row1" width="20%">{LANG.picture}: </td>
     <td class="row0">
     	<div id="ext_picture" class="picture" >{data.pic}</div>
      <input type="hidden" name="picture"	 id="picture" value="{data.picture}" />
      <div id="btnU_picture" class="div_upload" {data.style_upload} ><div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="{data.link_upload}" >Chọn hình</a></div></div></div>
    </td>
    </tr> 
    
		<tr>
     <td class="row1" width="20%">{LANG.cat_description}: </td>
     <td class="row0">{data.html_description}</td>
    </tr>
    <tr class="row_title" >
     <td  colspan="2" class="font_title" >Search Engine Optimization : </td>
    </tr> 
 
     <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0"><input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span></td>
    </tr>
     <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
    <tr>
      <td class="row1"  >{LANG.display} :&nbsp; </td>
      <td align="left" class="row0">{data.list_display}</td>
    </tr>
    
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button">
				<input type="reset" name="Submit2" value="Reset" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->


<!-- BEGIN: photo -->

<script language=javascript>
	$(document).ready(function() {
		$('#myForm').validate({
			rules: {
				gid: {
					required: true
				}
			},
			messages: {
				gid : {
					required: "{LANG.err_select_required}"
				}
			}
		});
	});




	function attachCallbacks(a) {
		a.bind("Error", function (a, e) {
			$('#bugcallbacks').append("<div>Error: " + e.code +   ", Message: " + e.message +   (e.file ? ", File: " + e.file.name : "") + "</div>"   );
			a.refresh(); // Reposition Flash/Silverlight
			$('#bugcallbacks').show();
		});
		a.bind("FileUploaded", function (b, e, c) {
			1 == a.total.queued && ("undefined" == typeof debug ? $("#photoalbum_form").submit() : ($("#bugcallbacks").show(), 0 < b.e.code && $("#bugcallbacks").append("<div>File: " + b.result + " <br/ > Error code: " + e.message + " <hr></div>")))
		})
	}

	// Convert divs to queue widgets when the DOM is ready
	$(function() {

		$("#uploader").pluploadQueue({
			// General settings
			runtimes : 'gears,flash,silverlight,browserplus,html5',
			url : 'modules/gallery_ad/ajax/upload.php',
			max_file_size : '{data.max_upload}',
			chunk_size : '1mb',
			rename : true,
			multipart: true,
			multipart_params: {
				'gid': '{data.gid}',
				'folder_upload': '{data.folder_upload}',
				'folder': '{data.folder}',
				'w' : '{data.w_pic}',
				'crop' : '0',
				'thumb' : '1',
				'w_thumb': '{data.w_thumb}'
			},

			// Resize images on clientside if we can
			resize : {width : 1000, height : 1000, quality : 90},
			// Specify what files to browse for
			filters : [
				{title : "Image files", extensions : "jpg,gif,png"}
			],

			flash_swf_url : ROOT+'js/plupload/plupload.flash.swf',
			silverlight_xap_url : ROOT+'js/plupload/plupload.silverlight.xap' ,
			preinit: attachCallbacks
		});


	});
</script>


<form action="{data.link_action}" method="post" name="photoalbum_form" id="photoalbum_form"   enctype="multipart/form-data" >

	<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<tr >
			<td class="row1" width="20%" align="right">Album : </td>
			<td class="row0"><b class="font_err">{data.title}</b> <input type="hidden" name="gid" id="gif" value="{data.gid}" /></td>
		</tr>
</table>
<br>
	{data.err}

	<div style=" padding-bottom: 10px; margin-top: 20px ; border-bottom: 1px dotted #cccccc" ><b class="font_title" style="font-size: 20px; text-transform: uppercase; line-height: 30px;" > Dang sách các hình cho  Album này: </b></div>

	<form action="{data.link_action}" method="post"  name="myForm"  >
		<table width="100%" border="0" cellspacing="2" cellpadding="5">
			<tr> {data.list_pic_old} </tr>
		</table>
		<p align="center">
			<input type="submit" name="btnUpdate" value="Cập nhật thứ tự" class="button">
		</p>
	</form>



	<div style=" padding-bottom: 10px; margin-top: 20px ; border-bottom: 1px dotted #cccccc" ><b class="font_title" style="font-size: 20px; text-transform: uppercase; line-height: 30px;" > Upload hình cho  Album này: </b></div>


				<div style="padding:20px">

					<input type="hidden" name="hlistfile" id="hlistfile" value="" />
					<div id="uploader">
						<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
					</div>


					<br />

					<div class="square note">
						<ul>
							<li>Mỗi lần upload tối đa <b>50 ảnh</b>. Giữ phím <b>Ctrl</b> để chọn thêm ảnh, phím <b>Shift</b> để chọn cùng lúc nhiều ảnh.</li>
							<li>Dung lượng mỗi ảnh tối đa <b>{data.max_upload}</b></li>
							<li>Định dạng ảnh cho phép: <b>.jpg</b>, <b>.jpeg</b>, <b>.gif</b> hoặc <b>.png</b></li>
						</ul>
						<div class="clear"></div>
					</div>

					<div id="bugcallbacks" style="display: none "></div>
				</div>


</form>


<br>
<!-- END: photo -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
	<tr>
    <td align="left" class="row1"><strong>{LANG.cat_name}: </strong></td>
    <td align="left" class="row0">{data.list_cat}</td>
  </tr>
   
  <tr>
    <td class="row1" ><strong>{LANG.view_day_from}:</strong>  </td>
    <td align="left" class="row0"> 
    <input type="text" name="date_begin" id="date_begin" value="{data.date_begin}" size="15" maxlength="10"   class="dates"  /> &nbsp;&nbsp; <strong>{LANG.view_day_to} :</strong> <input type="text" name="date_end" id="date_end" value="{data.date_end}" size="15" maxlength="10" class="dates"  /> </td>
  </tr>
  <tr>
  
  <tr>
  <td align="left"><strong>{LANG.search}  :</strong> &nbsp;&nbsp;&nbsp;  </td>
  <td align="left">{data.list_search} &nbsp;&nbsp;<strong>{LANG.keyword} :</strong> &nbsp;
    <input name="keyword"  value="{data.keyword}"type="text" size="20">
    <input name="btnSearch" type="submit" value=" Search " class="button"></td>
  </tr>
   <tr>
    <td width="15%" align="left" class="row1"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" align="left" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->