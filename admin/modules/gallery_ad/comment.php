<?php
/*================================================================================*\
|| 							Name code : estore.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "gallery";
	var $action = "comment";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {
      
			case 'edit':
        $nd['f_title'] = "Cập nhật ý kiến / phản hồi";
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = "Quản lý bình luận / Đánh giá";
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    
    if ($vnT->input['do_submit'])
    {
      $data = $_POST;			 
			
 			$cot['name'] = $vnT->input['name'];			 
			$cot['email'] = $vnT->input['email'];		 	
			$cot['title'] = $func->txt_HTML($_POST['title']); 
			$cot['content'] = $func->txt_HTML($_POST['content']); 
			$cot['hidden_email'] = $vnT->input['hidden_email'];	
				 
			// more here ...
			$ok = $DB->do_update("gallery_comment", $cot, "cid=$id");
			if ($ok)
			{
				 
				//xoa cache
				$func->clear_cache();
				//insert adminlog
				$func->insertlog("Edit", $_GET['act'], $id);
				
				$err = $vnT->lang["edit_success"];
				$url = $this->linkUrl . "&sub=edit&id=$id";
				$func->html_redirect($url, $err);
			} else
				$err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
     
    }
    
    $query = $DB->query("SELECT  c.*,n.title as title_gallery 
												 FROM gallery_comment c LEFT JOIN gallery_desc n
												 ON c.id=n.gid  
												 WHERE c.cid=$id");
    if ($data = $DB->fetch_row($query))
    {
 			$data['title_item']	 = "<a href='?mod=gallery&act=gallery&lang={$lang}&sub=edit&id=".$data['id']."'>".$data['title_gallery']."</a>";
			$data['list_hidden_email'] =  vnT_HTML::list_yesno ("hidden_email",$data['hidden_email']) ;
    				
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
     
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		}


    $res = $DB->query("SELECT * FROM gallery_comment WHERE cid IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {

      while ($row = $DB->fetch_row($res))  {
        if($row['display']==1){
          $DB->query("UPDATE gallerys SET num_comment=num_comment-1 WHERE gid=".$row['id']);
        }
        $DB->query("DELETE FROM gallery_comment WHERE  cid=".$row['cid'] );
      }
      $mess = $vnT->lang["del_success"];
      //xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    }

		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['cid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl.'&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_item = "?mod=gallery&act=gallery&sub=edit&id={$row['id']}&lang=$lang";
		
		$output['date_post'] .= @date("H:i, d/m/Y",$row['date_post']) ;
		$output['date_post'] .= "<div style='padding:2px' >Bởi : <b class='font_err'>".$row['name']."</b></div>";
		$output['date_post'] .= "<div style='padding:2px' >Email: ".$row['email']."</div>";
 		
		$output['title_item'] .= "<b><a href='".$link_item."'>".$func->HTML($row['title_gallery'])."</a></b>";

    $output['content']  = "<b>".$func->HTML($row['title'])."</b><br>".$func->HTML($row['content']) ;
 		
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&gid=".$row['id']."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&gid=".$row['id']."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<input name=h_gid['.$id.']" type="hidden" value="' . $row['id'] . '" />';
    $output['action'] .="<a href=\"{$link_edit}\" ><img src=\"". $vnT->dir_images ."/edit.gif\"  /></a>&nbsp;";
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    //update
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      $arr_gid = $_POST['h_gid'];
      switch ($vnT->input["do_action"])
      {
        
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("gallery_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){
                do_Rebuild_Comment($arr_gid[$h_id[$i]]);
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("gallery_comment", $dup, "cid=" . $h_id[$i]);
              if ($ok){
                do_Rebuild_Comment($arr_gid[$h_id[$i]]);
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
    if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update gallery_comment SET display=1 WHERE cid=".$vnT->input["do_display"]." ");
			if($ok){
        do_Rebuild_Comment($vnT->input["gid"]);
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update gallery_comment SET display=0 WHERE cid=".$vnT->input["do_hidden"]."  ");
			if($ok){
        do_Rebuild_Comment($vnT->input["gid"]);
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;

		$display = (isset($vnT->input['display'])) ? $display = $vnT->input['display'] : "-1";
		
		if($display!="-1"){
			$where .=" and c.display=$display ";	
			$ext_page.="display=$display|";
			$ext.="&display={$display}";
		}
		
		
		
		
    $query = $DB->query("SELECT  c.cid
					 FROM gallery_comment c LEFT JOIN gallery_desc n
					 ON c.id=n.gid  
					 WHERE n.lang='$lang'
					 $where ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
				 'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				 'date_post' => $vnT->lang['date_post']."|18%|center",
				 'title_item' => "gallery|20%|left",
				 'content' =>  " Nội dung | |left",
				 'action' => "Action|7%|center",
				);
    
    $sql = "SELECT  c.*,n.title as title_gallery 
					 FROM gallery_comment c LEFT JOIN gallery_desc n
					 ON c.id=n.gid  
					 WHERE n.lang='$lang'
					 $where 
					 ORDER BY c.date_post DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['cid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_comment'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		
		$data['list_display'] = vnT_HTML::selectbox("display", array(  '-1' => '--- Tất cả ---', '0' => 'Chưa xét duyệt' , '1' => 'Đã xét duyệt' ), $display,"","onChange='submit();'");


		
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>