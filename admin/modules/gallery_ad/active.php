<?php
/*================================================================================*\
|| 							Name code : job.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "gallery";
  var $action = "active";
  var $setting = array();

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->setting = loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad/" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module . "_ad" . ".js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/auto_suggest/autosuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/auto_suggest/bsn.AutoSuggest.js");
    switch ($vnT->input['sub']) {
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = "Kích hoat web dep";
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = $DB->query("SELECT picture FROM gallery_images WHERE gid IN ( " . $ids . " ) ");
    while ($img = $DB->fetch_row($query)) {
      if ($img['picture']) {
        $path_file = MOD_DIR_UPLOAD . $img['picture'];
        if (file_exists($path_file) && $img['picture'])
          @unlink($path_file);
      }
    }
    $query = 'DELETE FROM gallery_images WHERE gid IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['gid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['g_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $text_edit = "gallery_images_desc|title|id=" . $row['id'];
    $output['title'] .= "<p><strong><span id='edit-text-" . $id . "' onClick=\"quick_edit('edit-text-" . $id . "','$text_edit');\">" . $func->HTML($row['title']) . "</span></strong></p>";
   
 				
		$output['info'] ='<div style="padding:2px 0px">Danh mục: <b >'.get_cat_name($row['cat_id'],$lang).'</b></div>';
		$output['info'] .= '<div style="padding:2px 0px">Views: <b >'.(int)($row['views']).'</b></div>';

    
		$output['picture'] = get_pic_gallery($row['picture'], 60);
    $output['num_view'] = (int) $row["num_view"];
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))   $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['g_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("gallery_images", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("gallery_images", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("gallery_images", $dup, "gid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $where = " AND display=0";
    $ext_page = "";
    $ext = "";

    $cat_id = ((int)$vnT->input['cat_id']) ? $vnT->input['cat_id'] : 0 ;
    $search = ($vnT->input['search']) ?  $vnT->input['search'] : "gid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
    
	  if (! empty($cat_id)) {
      $a_cat_id = List_SubCat($cat_id);
      $a_cat_id = substr($a_cat_id, 0, - 1);
      if (empty($a_cat_id))
        $where .= " and FIND_IN_SET('$cat_id',cat_id)<>0 ";
      else {
        $tmp = explode(",", $a_cat_id);
        $str_ = " FIND_IN_SET('$cat_id',cat_id)<>0 ";
        for ($i = 0; $i < count($tmp); $i ++) {
          $str_ .= " or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
        }
        $where .= " and (" . $str_ . ") ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext .= "&cat_id=$cat_id";
    }
    if (! empty($search)) {
      $ext_page .= "search=$search|";
      $ext .= "&search={$search}";
    }
    if (! empty($keyword)) {
      switch ($search) {
        case "gid": $where .= " and gid = $keyword ";   break;
        case "date_post": $where .= " and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' ";  break;
        default: $where .= " and $search like '%$keyword%' ";  break;
      }
      $ext_page .= "keyword=$keyword|";
      $ext .= "&keyword={$keyword}";
    }

    $ext_page = $ext_page . "|p=$p";

    //echo $search ;
    $query = $DB->query("SELECT gid
													FROM gallery_images
													WHERE lang='$lang' 
													{$where} ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)    $p = $num_pages;
    if ($p < 1)    $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . $ext;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . " |10%|center" , 
      'title' => $vnT->lang['title_file'] . " |30%|left" , 
      'info' => "Thông tin |30%|left" , 
      'num_view' => $vnT->lang['view_num'] . " |10%|center" , 
      'action' => "Action|12%|center");
    $sql = "SELECT *
						FROM gallery_images  
						WHERE  lang='$lang'
						{$where}
						ORDER BY date_post DESC  
						LIMIT $start,$n";
    //    print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['gid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_gallery'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
    $data['listCat'] = Get_Cat($cat_id, $lang);
    $data['list_search'] = List_Search($search);
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
	// end class
}
?>
