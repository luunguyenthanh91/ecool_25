<?php
session_start();
define('IN_vnT', 1); 
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
include ($conf['rootpath'] . "includes/class_functions.php"); 	 

$DB = new DB;
$func = new Func_Global;

	function file_safe_name($text)
	{
		$text = str_replace(array(' ', '-'), array('_','_'), $text) ;
		$text = preg_replace('/[^A-Za-z0-9_]/', '', $text) ;
		return $text ;							
	}   
	$admin_user = $_POST["auth_user"] ;
	$admin_pass = $_POST["auth_pass"] ;
   	 
	$cat_id = $_POST["cat_id"] ;
	$folder = $_POST["folder"] ;
	$folder_upload	= $_POST["folder_upload"] ;
	$w = $_POST["w"] ;
	$crop = $_POST['crop'] ;
	$thumb = $_POST['thumb']; 
	$w_thumb = ($_POST['w_thumb']) ? $_POST['w_thumb'] : 100 ;
	
	//$myFile = "logs.txt";
	//$fh = fopen($myFile, 'a') or die("can't open file");
	

	//check 
	$sql = "select * from admin WHERE username='$admin_user' AND password='$admin_pass' "; 
	$result = $DB->query($sql);
	if (!$DB->num_rows($result)) {
		echo 'Bạn không có quyền upload File.' ;
		//fwrite($fh, 'Bạn không có quyền upload File.');
		//fwrite($fh, "\r\n");	
		//fclose($fh);
		exit(0);
	}else{
		if (!isset($_FILES["resume_file"]) || !is_uploaded_file($_FILES["resume_file"]["tmp_name"]) || $_FILES["resume_file"]["error"] != 0) {
			echo "Có lỗi xảy ra . Không upload được";
			//fwrite($fh, 'Có lỗi xảy ra . Không upload được');
			//fwrite($fh, "\r\n");	
			//fclose($fh);
			exit(0);
		} else {
			
			$image = $_FILES["resume_file"];
			$image['name'] = str_replace("%20", "_",$image['name']);
			$image['name'] = str_replace(" ", "_", $image['name']);
			$image['name'] = str_replace("&amp;", "_", $image['name']);
			$image['name'] = str_replace("&", "_", $image['name']);
			$image['name'] = str_replace(";", "_", $image['name']);
			
			$type = strtolower(substr($image['name'],strrpos($image['name'],".")+1));
			$file_size = $image['size'];
			$src_name = substr( $image['name'],0,strrpos($image['name'],".") );
			$src_name = file_safe_name($src_name) ;			
			$file_name =  $src_name.".".$type;  
			
			$link_file = $folder_upload."/".$file_name;
			
			if (file_exists($link_file)){
					$file_name = $src_name."_".time().".".$type;
					$link_file = $folder_upload."/".$file_name; 	
			} 
			 
			
			if($w){
				if($crop){
					$func->thum ($_FILES["resume_file"]["tmp_name"], $link_file, $w, $w,"1:1");
				}else{
					$func->thum ($_FILES["resume_file"]["tmp_name"], $link_file, $w);
				} 		
				
				if($thumb==1)	{ 		
					$file_thumb =  $folder_upload."/thumbs/" . $file_name   ; 
					$func->thum ($link_file, $file_thumb, $w_thumb);					 		 
				}
				$ok = 1;
			}else{
				$ok = copy($_FILES["resume_file"]["tmp_name"],$link_file);
			}

			if($ok)
			{			
							
				$picture = ($folder) ? $folder."/".$file_name : $file_name; 
				$title = str_replace("_"," ",$src_name);
				
				$cot['cat_id'] = $cat_id;
        $cot['picture'] = $picture;
        $cot['file_size'] = $file_size;
        $cot['file_type'] = $type; 
				
				
				$cot['adminid'] =  1;				
 				
				$cot['date_post'] = time();
				$cot['date_update'] = time();
				$kq = $DB->do_insert("gallery_images", $cot);
				if($kq){
					$gid = $DB->insertid();
				 	
					$cot_d['gid'] = $gid;
					$cot_d['title'] = $title;
 				
					//SEO
					$cot_d['friendly_url'] =   $func->make_url($title);
					$cot_d['friendly_title'] =  $title ." - ". $func->utf8_to_ascii($title);
					
					$query_lang = $DB->query("select name from language ");
					while ( $row_lang = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row_lang['name'];
						$DB->do_insert("gallery_images_desc",$cot_d);
					}
					  
					echo $gid;
					//fwrite($fh, 'OK id  = '.$gid);
					//fwrite($fh, "\r\n");	
					//fclose($fh);
				}else{
					echo "Có lỗi xảy ra . Không insert database được";
					//fwrite($fh, 'Có lỗi xảy ra . Không insert database được');
					//fwrite($fh, "\r\n");	
					//fclose($fh);
					exit(0);		
				}				
			}else{
				echo "Có lỗi xảy ra . Không insert copy file được";
				//fwrite($fh, 'Có lỗi xảy ra . Không insert copy file được');
				//fwrite($fh, "\r\n");	
				//fclose($fh);
				exit(0);	
			}
		}
	}
	
	
	
$DB->close();
?>