 
<!-- BEGIN: edit -->
<br> 
<script language=javascript>
	function show_type(cat) 
	{		
 		switch (cat)
		{
			case '1' : 
				$("#ext_type0").hide();
				$("#ext_type1").show();
				$("#ext_type3").hide();
			break ;
			case '2' : 
				$("#ext_type0").hide();
				$("#ext_type1").hide();
				$("#ext_type3").hide();
			break ;
			case '3' : 
				$("#ext_type0").hide();
				$("#ext_type1").hide();
				$("#ext_type3").show();
			break ;
			default: 
				$("#ext_type0").show();
				$("#ext_type1").hide();
				$("#ext_type3").hide();			
		}	
		
	}
	
	function show_size(pos) 
	{			
		$.ajax({
				url: "modules/advertise_ad/ajax/ajax.php",
				cache: false,
				type: "POST",
				data: ({ 'do': 'get_size',  'pos': pos }),
				dataType: "json",		 
				success: function(json)
				{ 		
					$("#width").val(json['width']) ; 
					$("#height").val(json['height']) ; 
				} 
		});
	
	}
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myform" id="myform" >
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
		<!-- BEGIN: select_lang -->
		<tr >
			<td class="row1" >{LANG.language} : </td>
			<td  align="left" class="row0"><input name="rLang" id="rLang" type="radio" value="0" /> {LANG.only_this_lang} <input name="rLang" id="rLang" type="radio" value="1" checked="checked" /> {LANG.all_lang}</td>
		</tr>
    <!-- END: select_lang -->
		  <tr>
            <td width="20%"  class="row1">{LANG.position} : </td>
            <td  align="left" class="row0"><strong>{data.vitri}</strong></td>
          </tr>
          <tr>
            <td class="row1">{LANG.title}  : </td>
            <td class="row0" align="left"><input name="title" type="text" id="title" size="50" maxlength="250" value="{data.title}" /></td>
          </tr>
		  
        <tr id="ext_type0" >
        <td colspan="2" class="row0" align="center">
        {data.html_img}
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr >
                <td width="20%" class="row1">{LANG.picture} :&nbsp;</td>
                <td align="left" class="row0">
                  <div class="ext_upload">
                  <input name="chk_upload" type="radio" value="0" checked> 
                  Insert URL's image &nbsp; <input name="l_url" type="text" size="50" maxlength="250" value="{data.l_url}" onchange="do_ChoseUpload('ext_upload',0);" > <input name="rGet" id="rGet" type="checkbox" value="1" checked="checked" /> Lấy file <br>
                  <input name="chk_upload" type="radio" value="1"> Upload Picture &nbsp;&nbsp;&nbsp;
                  <input name="image" type="file" id="image" size="30" maxlength="250" onchange="do_ChoseUpload('ext_upload',1);">
                  </div>
                </td>
              </tr>		  
          </table>
    
        </td>
       </tr>
       
       <tr >
         <td class="row1" width="20%">Màu nền: </td>
         <td class="row0"><input type="text" name="bg_color" id="bg_color" size="7" value="{data.bg_color}" /></td>
       </tr>
        
       <tr >
         <td class="row1" width="20%">Kiểu hiển thị:<br /> (background-attachment) </td>
         <td class="row0">{data.bg_attachment}</td>
       </tr>
       
       <tr >
         <td class="row1" width="20%">Canh theo chiều ngang:<br /> (background-position ngang) </td>
         <td class="row0">{data.bg_position_n}</td>
       </tr>
       <tr >
         <td class="row1" width="20%">Canh theo chiều dọc:<br /> (background-position dọc) </td>
         <td class="row0">{data.bg_position_v}</td>
       </tr>
       
       <tr >
         <td class="row1" width="20%">Tùy chỉnh lặp lại:<br /> (background-repeat) </td>
         <td class="row0">{data.bg_repeat}</td>
       </tr>
        
		   <tr >
       
		     <td class="row1">{LANG.date_post} : </td>
		     <td align="left" class="row0" ><input name="date_add" id="date_add" type="text"  size="20" maxlength="250" value="{data.date_add}" />&nbsp;<span class="font_err" >(dd/mm/YYYY)</span></td>
	      </tr>
		   <tr >
            <td class="row1" >{LANG.date_expire}: </td>
			<td align="left" class="row0" ><input name="date_expire" id="date_expire"  type="text"  size="20" maxlength="250" value="{data.date_expire}" />&nbsp;<span class="font_err" >(dd/mm/YYYY)</span></td>
          </tr>
		 

      <tr >
        <td class="row1" >{LANG.module_show}:</td>
        <td align="left" class="row0">{data.list_module_show}</td>
		  </tr>
      <tr >
        <td class="row1" >{LANG.cat_show}:</td>
        <td align="left" class="row0">{data.list_cat_show}</td>
		  </tr>
      
     <tr >
      <td class="row1">{LANG.display}: </td>
      <td align="left" class="row0">{data.list_display}</td>
		  </tr>
          <tr >
          <td class="row1" >&nbsp;</td>
            <td class="row0">
            <input type="hidden" name="do_submit" value="1">
            <input type="submit" name="Submit" value="Submit" class="button">
            <input type="reset" name="Submit2" value="Reset" class="button">           
			 </td></tr>
        </table>
    </form>
<br><br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
  <tr>
    <td width="15%" class="row1">{LANG.totals}: &nbsp;</td>
    <td width="85%" class="row0"><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
    <td  class="row1"><strong>Modules :</strong> </td>
    <td class="row0">{data.list_module}</td>
  </tr>
   <tr >
	 	<td  class="row1"><strong>{LANG.position}:</strong> </td>
    <td class="row0">{data.list_pos}</td>
  </tr> 
  </table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->