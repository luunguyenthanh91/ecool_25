<?php
/*================================================================================*\
|| 							Name code : advertise.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "advertise";
  var $action = "advertise";

  /**
   * function vntModule ()
   * Khoi tao
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $pos = ($vnT->input['pos']) ? $vnT->input['pos'] : $vnT->setting['pos'];

		$nd['menu'] = getToolbar($vnT->input['sub'], $pos, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);

    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_advertise'];
        $nd['content'] = $this->do_Add($pos, $lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_advertise'];
        $nd['content'] = $this->do_Edit($pos, $lang);
      break;
      case 'manage':
      	$nd['f_title'] = $vnT->lang['manage_advertise'];
        $nd['content'] = $this->do_Manage($pos, $lang);
      break;
      case 'del':    $this->do_Del($pos, $lang);  break;
      default:
        $nd['f_title'] = $vnT->lang['manage_advertise'];
        $nd['content'] = $this->do_Manage($pos, $lang);
      break;
    }

    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }


  /**
   * function do_Add
   * Them gioi thieu moi
   **/
  function do_Add ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;

		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_add').datepicker({
					changeMonth: true,
					changeYear: true
				});

				$('#date_expire').datepicker({
					changeMonth: true,
					changeYear: true
				});
			});

		");
    $err = "";
    $data['l_link'] = "http://";
    $err = "";
    $data['date_add'] = date("d/m/Y");
    $data['date_expire'] = date("d/m/Y", (time() + 30 * 24 * 3600));
    $data['target'] = "_blank";
    $data['display'] = 1;
    $data['width'] = $vnT->setting[$pos]['width'];
    $data['height'] = $vnT->setting[$pos]['height'];
		$data['type_ad']=0;

    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      $arr_module = $_POST['module_show'];
      $module_show = implode(",", $arr_module);
      $type_ad = $vnT->input['type_ad'];

			switch ($type_ad)
			{
				case 1 :
					$picture = $DB->mySQLSafe($_POST['content']);
				break	;
				case 2 :
					 $picture = "";
				break	;
				case 3 :
					 $picture = $func->txt_HTML($_POST['script']);
				break	;
				default :
					$picture = $vnT->input['picture'];
					$file_type =  strtolower(substr($picture, strrpos($picture, ".") + 1));
					if (empty($picture))
						$err = $func->html_err("No Web link Image selected");

          $mobile_image = $vnT->input['mobile_image'];
					$file_type1 =  strtolower(substr($mobile_image, strrpos($mobile_image, ".") + 1));
					if (empty($mobile_image))
						$err = $func->html_err("No Web link Image selected");

				break;
			}




      // insert CSDL
      if (empty($err)) {
        $cot = array(
          "title" => $vnT->input['title'] ,
          "img" => $picture ,
          "mobile_image" => $mobile_image ,
          "type" => $file_type ,
          "link" => trim($_POST['l_link']) ,
          "type_ad" => $type_ad ,
          "height" => $vnT->input['height'] ,
          "width" => $vnT->input['width'] ,
          "pos" => $pos ,
          "target" => $vnT->input['target'] ,
          "date_add" => $date_add ,
          "date_expire" => $date_expire ,
          "module_show" => $module_show ,
          "date_add" => $date_add ,
          "display" => $vnT->input['display'] ,
          "description" => $DB->mySQLSafe($_POST['description']),
          "lang" => $lang);
        $ok = $DB->do_insert("advertise", $cot);
        if ($ok) {

					//update content
					if($vnT->input['rLang'])
					{
						$query_lang = $DB->query("select name from language WHERE name<>'" . $lang . "' ");
						while ($row_lang = $DB->fetch_row($query_lang)) {
							$cot['lang'] = $row_lang['name'];
							$DB->do_insert("advertise", $cot);
						}
					}

          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add&pos={$pos}";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['vitri'] = List_Pos($pos, "onChange='show_size(this.value);'");
    $data['list_type_ad'] = list_type_ad($data['type_ad'],"onChange='show_type(this.value);'");

    $data['list_module_show'] = List_Module_Show($module_show);
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['list_target'] = List_Target($data['target']);
    $data['readonly'] = 'readonly="readonly"';

		$data['html_content'] = $vnT->editor->doDisplay('content', $data['content'], '100%', '250', "Normal");
    $data['html_description'] = $vnT->editor->doDisplay('description', $data['description'], '100%', '250', "Normal");
		switch ($data['type_ad'])
		{
			case 1 :
				$data['style0']=" style='display:none' ";
				$data['style1']="";
				$data['style3']=" style='display:none' ";
			break	;
			case 2 :
				$data['style0']=" style='display:none' ";
				$data['style1']=" style='display:none' ";
				$data['style3']=" style='display:none' ";
			break	;
			case 3 :
				$data['style0']=" style='display:none' ";
				$data['style1']=" style='display:none' ";
				$data['style3']="";
			break	;
			default :
				$data['style0']="";
				$data['style1']=" style=\"display:none;\" ";
				$data['style3']=" style=\"display:none;\" ";
			break;
		}

		if($vnT->muti_lang) {
       $this->skin->parse("edit.select_lang");
		}

    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add&pos=$pos";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit
   * Cap nhat admin
   **/
  function do_Edit ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");

    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_add').datepicker({
					changeMonth: true,
					changeYear: true
				});

				$('#date_expire').datepicker({
					changeMonth: true,
					changeYear: true
				});
			});

		");
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $l_url = $vnT->input['l_url'];
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      $arr_module = $_POST['module_show'];
      $module_show = implode(",", $arr_module);
      $type_ad = $vnT->input['type_ad'];

			switch ($type_ad)
			{
				case 1 :
					$picture = stripslashes ($_POST['content']);
		 			$picture = str_replace(ROOT_URI."vnt_upload/File",$conf['rooturl']."vnt_upload/File",$picture);
					$picture = $DB->mySQLSafe($picture);
				break	;
				case 2 :
					 $picture = "";
				break	;
				case 3 :
					 $picture = $func->txt_HTML($_POST['script']);
				break	;
				default :
					$picture = $vnT->input['picture'];
					$file_type =  strtolower(substr($picture, strrpos($picture, ".") + 1));

          $mobile_image = $vnT->input['mobile_image'];
					$file_type1 =  strtolower(substr($mobile_image, strrpos($mobile_image, ".") + 1));
				break;
			}
			// end type

      if (empty($err)) {
        $cot = array(
          "title" => $vnT->input['title'] ,
          "link" => trim($_POST['l_link']) ,
          "type_ad" => $type_ad ,
          "height" => $vnT->input['height'] ,
          "width" => $vnT->input['width'] ,
          "pos" => $pos ,
          "target" => $vnT->input['target'] ,
          "date_add" => $date_add ,
          "date_expire" => $date_expire ,
          "module_show" => $module_show ,
          "date_add" => $date_add ,
          "display" => $vnT->input['display'] ,
          "description" => $DB->mySQLSafe($_POST['description']),
          "lang" => $lang
					);
        if (! empty($picture) ) {
          $cot['img'] = $picture ;
          $cot['type'] = $file_type;
        }
        if (! empty($mobile_image) ) {
          $cot['mobile_image'] = $mobile_image ;
        }
        $ok = $DB->do_update("advertise", $cot, "l_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=manage&pos=$pos";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM advertise WHERE l_id={$id} ");
    if ($data = $DB->fetch_row($query)) {
      $data['l_link'] = $data['link'];

      $data['date_add'] = ($data['date_add']) ? @date("d/m/Y", $data['date_add']) : "";
      $data['date_expire'] = ($data['date_expire']) ? @date("d/m/Y", $data['date_expire']) : "";

      $data['vitri'] = List_Pos($pos, "onChange='show_size(this.value);'");
      $data['list_module_show'] = List_Module_Show($data['module_show']);
      $data['list_target'] = List_Target($data['target']);
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);

			$data['list_type_ad'] = list_type_ad($data['type_ad'],"onChange='show_type(this.value);'");

			switch ($data['type_ad'])
			{
				case 1 :
					$data['style0']=" style='display:none' ";
					$data['style1']="";
					$data['style3']=" style='display:none' ";
					$content_text = $data['img'];
				break	;
				case 2 :
					$data['style0']=" style='display:none' ";
					$data['style1']=" style='display:none' ";
					$data['style3']=" style='display:none' ";
				break	;
				case 3 :
					$data['style0']=" style='display:none' ";
					$data['style1']=" style='display:none' ";
					$data['style3']="";
					$data['script'] = $data['img'];
				break	;
				default :
					$data['style0']="";
					$data['style1']=" style=\"display:none;\" ";
					$data['style3']=" style=\"display:none;\" ";
					if($data['img'])
					{
						$src =  (strstr($data['img'],"http://")) ? $data['img'] :  MOD_DIR_UPLOAD . "/" .  $data['img'] ;
						if ($data['type'] == "swf")
						{
							$data['html_img'] = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"  width="' . $data['width'] . '" height="' . $data['height'] . '" >
						<param name="movie" value="' . $src . '" />
						<param name="quality" value="high" />
						<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"    width="' . $data['width'] . '" height="' . $data['height'] . '"></embed>
					</object>';
						} else {
							$data['html_img'] = "<img src=\"{$src}\" />";
						}

						$data['picture'] = $data['img'];
					}

          if($data['mobile_image'])
					{
						$src =  (strstr($data['mobile_image'],"http://")) ? $data['mobile_image'] :  MOD_DIR_UPLOAD . "/" .  $data['mobile_image'] ;

						$data['html_img_mobile'] = "<img src=\"{$src}\" />";

						$data['mobile_image'] = $data['mobile_image'];
					}


				break;
			}

			$data['html_content'] = $vnT->editor->doDisplay('content', $content_text, '100%', '250', "Normal");
      $data['html_description'] = $vnT->editor->doDisplay('description', $data['description'], '100%', '250', "Normal");

    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&pos=$pos&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del
   * Xoa 1 ... n  gioi thieu
   **/
  function do_Del ($pos, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM advertise WHERE l_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
			 //xoa cache
      $func->clear_cache();
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&sub=manage&pos=$pos&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['l_id'];
    $row_id = "row_" . $id;
    $pos = $row['pos'];
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&pos=' . $pos . '&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&pos=" . $pos . "&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['l_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";

		switch ($row['type_ad'])
		{
			case 1 :
				$output['picture'] = $row['img']  ;
			break	;
			case 2 :
				$output['picture'] = $row['title'] . "<br>Link : ". $row['link'];
			break	;
			case 3 :
				$output['picture'] = $func->HTML($row['img']);
			break	;
			default :
				if($row['img'])
				{
					$src =  (strstr($row['img'],"http://")) ? $row['img'] :  MOD_DIR_UPLOAD . "/" .  $row['img'] ;

					if ($row['type'] == "swf") {
						$picture = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"  width="' . $row['width'] . '"  height="' . $row['height'] . '" >
							<param name="movie" value="' . $src . '" />
							<param name="quality" value="high" />
							<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"   width="' . $row['width'] . '" height="' . $row['height'] . '"></embed>
						</object>';
					} else {
						$picture = "<img src=\"" . $src . "\">";
					}
				} else  $picture = "No image";

				$output['picture'] = $row['title'] . "<br>" . $picture . "<br>" . $row['link'];
			break;
		}





    $output['num_click'] = $row['num_click'] . "&nbsp;l&#7847;n";
    $link_display = $this->linkUrl.$row['ext_link'];
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }

    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage()
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])    $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))   $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['l_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("advertise", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("advertise", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("advertise", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }

		if((int)$vnT->input["do_display"]) {
			$ok = $DB->query("Update advertise SET display=1 WHERE l_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {
			$ok = $DB->query("Update advertise SET display=0 WHERE l_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";
				$err = $func->html_mess($mess);
			}
			//xoa cache
      $func->clear_cache();
		}

    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $where = " AND  pos ='{$pos}'";
    $ext = "&pos=$pos";
    $module = $vnT->input['module'];
    if ($module) {
      $where .= " and  (FIND_IN_SET('$module',module_show) or (module_show='') ) ";
      $ext = "&module=$module";
    }
    $query = $DB->query("SELECT l_id FROM advertise WHERE lang='$lang' $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage&pos=$pos{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" ,
      'order' => $vnT->lang['order'] . "|10%|center" ,
      'picture' => $vnT->lang['logo'] . " |60%|center" ,
      'num_click' => "Click |7%|center" ,
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM advertise WHERE lang='$lang' $where ORDER BY l_order LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['l_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_advertise'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&pos=$pos&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['list_pos'] = List_Pos($pos, "onChange='submit()'");
    $data['list_module'] = List_Module($module, "onChange='submit();'");
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>
