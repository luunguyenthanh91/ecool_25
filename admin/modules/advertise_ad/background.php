<?php
/*================================================================================*\
|| 							Name code : background.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "advertise";
  var $action = "background";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $this->skin->assign('DIR_IMAGE', $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    //$pos = ($vnT->input['pos']) ? $vnT->input['pos'] : $vnT->setting['pos'];
		$pos = ($vnT->input['pos']) ? $vnT->input['pos'] : "background";
		
		$vnT->html->addScript( $vnT->dir_js."/jquery.modcoder.excolor/js/modcoder_excolor/jquery.modcoder.excolor.js");
		
		//$nd['menu'] = getToolbar($vnT->input['sub'], $pos, $lang);
		$nd['menu'] = $func->getToolbar($this->module, $this->action, $lang, "&pos=".$pos);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_background'];
        $nd['content'] = $this->do_Add($pos, $lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_background'];
        $nd['content'] = $this->do_Edit($pos, $lang);
      break;
      case 'manage':
      	$nd['f_title'] = $vnT->lang['manage_background'];
        $nd['content'] = $this->do_Manage($pos, $lang);
      break;
      case 'del':    $this->do_Del($pos, $lang);  break;
      default:
        $nd['f_title'] = $vnT->lang['manage_background'];
        $nd['content'] = $this->do_Manage($pos, $lang);
      break;
    }
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }
  

  /**
   * function do_Add 
   * Them gioi thieu moi 
   **/
  function do_Add ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
		
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(document).ready( function() {
				$('#date_add').datepicker({
					changeMonth: true,
					changeYear: true
				});
				
				$('#date_expire').datepicker({
					changeMonth: true,
					changeYear: true
				});
				
				$('#bg_color').modcoder_excolor();
			});
		
		");
    $err = "";
    $data['l_link'] = "http://";
    $err = "";
    $data['date_add'] = date("d/m/Y");
    $data['date_expire'] = date("d/m/Y", (time() + 30 * 24 * 3600));
    $data['target'] = "_blank";
    $data['display'] = 1;
    $data['width'] = $vnT->setting[$pos]['width'];
    $data['height'] = $vnT->setting[$pos]['height'];
		$data['type_ad']=0;
		
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      $arr_module = $_POST['module_show'];
      $module_show = implode(",", $arr_module);
			
			$arr_cat = $_POST['cat_show'];
      $cat_show = implode(",", $arr_cat);
			
			$bg_color = $_POST["bg_color"];
			
      if ($_POST['chk_upload'] == 1 && ! empty($_FILES['image'])) {
				$up['path'] = MOD_DIR_UPLOAD;
				$up['dir'] = "";
				$up['file'] = $_FILES['image'];
				$up['type'] = "hinh";
				$up['w'] = 2000;
				$result = $vnT->File->Upload($up);
				if (empty($result['err'])) {
					$picture = $result['link'];
					$file_type = $result['type'];
				} else {
					$err = $func->html_err($result['err']);
				}
			} else {
				$l_url = trim($vnT->input['l_url']);
				if (! empty($l_url) && ($vnT->input['rGet']==1) ) 
				{
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir'] = $dir;
					$up['url'] = $l_url;
					$up['w'] = 2000;
					$result = $vnT->File->UploadURL($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}else{
					$picture = $l_url;
					$file_type =  strtolower(substr($picture, strrpos($picture, ".") + 1));
				}
			}
			if (empty($picture) && empty($bg_color))
				$err = $func->html_err("No Web link Image and Color selected");	 
			
			 
      // insert CSDL
      if (empty($err)) {
        $cot = array(
          "title" => $vnT->input['title'] , 
          "img" => $picture , 
					"bg_color" => $bg_color , 
					"bg_attachment" => $vnT->input['bg_attachment'] , 
					"bg_position_n" => $vnT->input['bg_position_n'] , 
					"bg_position_v" => $vnT->input['bg_position_v'] , 
					"bg_repeat" => $vnT->input['bg_repeat'] , 
          "type" => $file_type , 
          "pos" => $pos , 
          "date_add" => $date_add , 
          "date_expire" => $date_expire , 
          "module_show" => $module_show , 
					"cat_show" => $cat_show , 
          "date_add" => $date_add , 
          "display" => $vnT->input['display'] , 
          "lang" => $lang);
        $ok = $DB->do_insert("background", $cot);
        if ($ok) {
					
					//update content
					if($vnT->input['rLang'])
					{
						$query_lang = $DB->query("select name from language WHERE name<>'" . $lang . "' ");
						while ($row_lang = $DB->fetch_row($query_lang)) {
							$cot['lang'] = $row_lang['name'];
							$DB->do_insert("background", $cot);            
						}
					}
					
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add&pos={$pos}";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    //$data['vitri'] = List_Pos($pos, "onChange='show_size(this.value);'");
		$data['vitri'] = vnT_HTML::selectbox ("pos", array("background"=>"Background", "background_body"=>"Background body"), "background", "", "");
		
    $data['list_module_show'] = List_Module_Show($module_show);
		$data['list_cat_show'] = List_Cat_Show($cat_show,$lang);
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
		
		if($vnT->muti_lang) {
       $this->skin->parse("edit.select_lang");
		}
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add&pos=$pos";
		
		$data['bg_attachment'] = vnT_HTML::selectbox ("bg_attachment", array("fixed"=>"Đứng im", "scroll"=>"Cuộn theo trang"), "scroll", "", "");
		$data['bg_position_n'] = vnT_HTML::selectbox ("bg_position_n", array("center"=>"Ở giữa", "left"=>"Bên trái", "right"=>"Bên phải"), "center", "", "");
		$data['bg_position_v'] = vnT_HTML::selectbox ("bg_position_v", array("center"=>"Ở giữa", "top"=>"Trên cùng", "bottom"=>"Dưới cùng"), "center", "", "");
		$data['bg_repeat'] = vnT_HTML::selectbox ("bg_repeat", array("no-repeat"=>"Không lặp lại", "repeat"=>"Lại lại cả 2 chiều", "repeat-x"=>"Lặp lại theo chiều ngang", "repeat-y"=>"Lặp lại theo chiều dọc"), "no-repeat", "", "");
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat admin
   **/
  function do_Edit ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");
		
    $vnT->html->addScriptDeclaration("
	 		$(document).ready( function() {
				$('#date_add').datepicker({
					changeMonth: true,
					changeYear: true
				});
				
				$('#date_expire').datepicker({
					changeMonth: true,
					changeYear: true
				});
				$('#bg_color').modcoder_excolor();
			});
		");
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $l_url = $vnT->input['l_url'];
      if ($vnT->input['date_add']) {
        $tmp = explode("/", $data['date_add']);
        $date_add = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      if ($vnT->input['date_expire']) {
        $tmp = explode("/", $data['date_expire']);
        $date_expire = mktime(0, 0, 0, $tmp[1], $tmp[0], $tmp[2]);
      }
      $arr_module = $_POST['module_show'];
      $module_show = implode(",", $arr_module);
			
			$arr_cat = $_POST['cat_show'];
      $cat_show = implode(",", $arr_cat);
			
			$bg_color = $_POST["bg_color"];
			
      if ($_POST['chk_upload'] == 1 && ! empty($_FILES['image'])) 
			{
				$up['path'] = MOD_DIR_UPLOAD;
				$up['dir'] = "";
				$up['file'] = $_FILES['image'];
				$up['type'] = "hinh";
				$up['w'] = 2000;
				$result = $vnT->File->Upload($up);
				if (empty($result['err'])) {
					$picture = $result['link'];
					$file_type = $result['type'];
				} else {
					$err = $func->html_err($result['err']);
				}
			} else {
				$l_url = trim($vnT->input['l_url']);
				if (! empty($l_url) && ($vnT->input['rGet']==1) ) 
				{
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir'] = $dir;
					$up['url'] = $l_url;
					$up['w'] = 2004;
					$result = $vnT->File->UploadURL($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}else{
					$picture = $l_url;
					$file_type =  strtolower(substr($picture, strrpos($picture, ".") + 1));
				}
			}		
			
			// end type
			
      if (empty($err)) {
        $cot = array(
          "title" => $vnT->input['title'] , 
					"bg_color" => $bg_color , 
					"bg_attachment" => $vnT->input['bg_attachment'] , 
					"bg_position_n" => $vnT->input['bg_position_n'] , 
					"bg_position_v" => $vnT->input['bg_position_v'] , 
					"bg_repeat" => $vnT->input['bg_repeat'] , 
          "pos" => $pos , 
          "date_add" => $date_add , 
          "date_expire" => $date_expire , 
          "module_show" => $module_show , 
					"cat_show" => $cat_show , 
          "date_add" => $date_add , 
          "display" => $vnT->input['display'] , 
          "lang" => $lang
					);
        if (! empty($picture) ) {
          $cot['img'] = $picture ;
          $cot['type'] = $file_type;
        }
        $ok = $DB->do_update("background", $cot, "l_id=$id");
        if ($ok) {
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=manage&pos=$pos";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM background WHERE l_id={$id} ");
    if ($data = $DB->fetch_row($query)) {
      $data['l_link'] = $data['link'];
      
      $data['date_add'] = ($data['date_add']) ? @date("d/m/Y", $data['date_add']) : "";
      $data['date_expire'] = ($data['date_expire']) ? @date("d/m/Y", $data['date_expire']) : "";
      
      //$data['vitri'] = List_Pos($pos, "onChange='show_size(this.value);'");
			$data['vitri'] = vnT_HTML::selectbox ("pos", array("background"=>"Background", "background_body"=>"Background body"), $pos, "", "");
      $data['list_module_show'] = List_Module_Show($data['module_show']);
			$data['list_cat_show'] = List_Cat_Show($data['cat_show'],$lang);
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
			
			
			if($data['img'])
			{
				$src =  (strstr($data['img'],"http://")) ? $data['img'] :  MOD_DIR_UPLOAD . "/" .  $data['img'] ;	
				if ($data['type'] == "swf") 
				{
					$data['html_img'] = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"  width="' . $data['width'] . '" height="' . $data['height'] . '" >
				<param name="movie" value="' . $src . '" />
				<param name="quality" value="high" />
				<embed src="' . $src . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"    width="' . $data['width'] . '" height="' . $data['height'] . '"></embed>
			</object>';
				} else {
					$data['html_img'] = "<img src=\"{$src}\" />";
				}
			}
			
			$data['bg_attachment'] = vnT_HTML::selectbox ("bg_attachment", array("fixed"=>"Đứng im", "scroll"=>"Cuộn theo trang"), $data['bg_attachment'], "", "");
			$data['bg_position_n'] = vnT_HTML::selectbox ("bg_position_n", array("center"=>"Ở giữa", "left"=>"Bên trái", "right"=>"Bên phải"), $data['bg_position_n'], "", "");
			$data['bg_position_v'] = vnT_HTML::selectbox ("bg_position_v", array("center"=>"Ở giữa", "top"=>"Trên cùng", "bottom"=>"Dưới cùng"), $data['bg_position_v'], "", "");
			$data['bg_repeat'] = vnT_HTML::selectbox ("bg_repeat", array("no-repeat"=>"Không lặp lại", "repeat"=>"Lại lại cả 2 chiều", "repeat-x"=>"Lặp lại theo chiều ngang", "repeat-y"=>"Lặp lại theo chiều dọc"), $data['bg_repeat'], "", "");
     
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&pos=$pos&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($pos, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM background WHERE l_id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
			 //xoa cache
      $func->clear_cache();
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&sub=manage&pos=$pos&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['l_id'];
    $row_id = "row_" . $id;
    $pos = $row['pos'];
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&pos=' . $pos . '&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&pos=" . $pos . "&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['l_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    
		
		if($row['img'])
		{
			$src =  (strstr($row['img'],"http://")) ? $row['img'] :  MOD_DIR_UPLOAD . "/" .  $row['img'] ;	
		
			$picture = "<img src=\"" . $src . "\" style=\"max-width:400px;\">";
		} else  $picture = "No image";
		
		$output['picture'] = $row['title'] . "<br>" . $picture . "<br>";
		
		$output['info'] = '<div>Màu nền: <span style="background:'.$row['bg_color'].'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>';
		$output['info'] .= '<div><strong>background-attachment:</strong> <span >'.$row['bg_attachment'].'</span></div>';
		$output['info'] .= '<div><strong>background-position ngang:</strong> <span >'.$row['bg_position_n'].'</span></div>';
		$output['info'] .= '<div><strong>background-position dọc:</strong> <span >'.$row['bg_position_v'].'</span></div>';
		$output['info'] .= '<div><strong>background-repeat:</strong> <span >'.$row['bg_repeat'].'</span></div>';

    $link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($pos, $lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])    $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))   $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['l_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("background", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("background", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("background", $dup, "l_id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update background SET display=1 WHERE l_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update background SET display=0 WHERE l_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $where = " AND  pos ='{$pos}'";
    $ext = "&pos=$pos";
    $module = $vnT->input['module'];
    if ($module) {
      $where .= " and  (FIND_IN_SET('$module',module_show) or (module_show='') ) ";
      $ext = "&module=$module";
    }
    $query = $DB->query("SELECT l_id FROM background WHERE lang='$lang' $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage&pos=$pos{$ext}&p=$p"; 
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|10%|center" , 
      'picture' => $vnT->lang['logo'] . " |45%|center" , 
      'info' => "Info |30%|left" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM background WHERE lang='$lang' $where ORDER BY l_order LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['l_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_background'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&pos=$pos&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    //$data['list_pos'] = List_Pos($pos, "onChange='submit()'");
		$data['list_pos'] = vnT_HTML::selectbox ("pos", array("background"=>"Background", "background_body"=>"Background body"), $pos, "", "onChange='submit()'");
    $data['list_module'] = List_Module($module, "onChange='submit();'");
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>