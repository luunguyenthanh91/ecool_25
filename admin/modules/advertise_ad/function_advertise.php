<?php
/*================================================================================*\
|| 							Name code : funtions_advertise.php 		 			          	   		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/weblink');

/*-------------- loadSetting --------------------*/
function loadSetting ()
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from ad_pos WHERE display=1 ORDER BY display_order ASC , id DESC ");
  $i == 0;
  while ($row = $DB->fetch_row($result)) {
    $i ++;
    if ($i == 1) {
      $vnT->setting['pos'] = $row['name'];
    }
    $vnT->setting[$row['name']]['width'] = $row['width'];
    $vnT->setting[$row['name']]['height'] = $row['height'];
    $vnT->setting[$row['name']]['align'] = $row['align'];
    $vnT->setting[$row['name']]['type_show'] = $row['type_show'];
  }
  unset($setting);
}

function getToolbar ($sub = "", $pos = "", $lang = "vn")
{
  global $func, $DB, $conf, $vnT;
  if ($sub == 'manage') {
    $link_manage = "?mod=advertise&act=advertise&lang=" . $lang;
  } else {
    $link_manage = "?mod=advertise&act=advertise&sub=manage&pos=" . $pos . "&lang=" . $lang;
  }
  $menu = array(
    "add" => array(
      'icon' => "i_add" , 
      'title' => "Add" , 
      'link' => "?mod=advertise&act=advertise&sub=add&pos=" . $pos . "&lang=" . $lang) , 
    "edit" => array(
      'icon' => "i_edit" , 
      'title' => "Edit" , 
      'link' => "javascript:alert('" . $vnT->lang['action_no_active'] . "')") , 
    "manage" => array(
      'icon' => "i_manage" , 
      'title' => "Manage" , 
      'link' => $link_manage) , 
    "help" => array(
      'icon' => "i_help" , 
      'title' => "Help" , 
      'link' => "'help/index.php?mod=advertise&act=advertise','AdminCPHelp',1000, 600, 'yes','center'" , 
      'newwin' => 1));
  return $func->getMenu($menu);
}

function List_Check ($name, $did)
{
  global $func, $DB, $conf;
  if ($did == 1)
    $check_on = "checked";
  else
    $check_off = "checked";
  $text = "<table width=\"100\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">
            <tr>
                <td style=\"padding-left:3px\"><input name=\"{$name}\" type=\"radio\" value=\"1\" {$check_on} />&nbsp;On </td>
                <td style=\"padding-left:3px\"><input name=\"{$name}\" type=\"radio\" value=\"0\" {$check_off} />&nbsp;Off</td>
             </tr>
          </table>";
  return $text;
}

//---------- list_type_ad
function list_type_ad ($did, $ext = "")
{
  global $func, $DB, $conf;
  $arr_type = array(0 => 'Dạng hình logo, banner' , 
      1 => 'Dạng chữ (text)' , 
      2 => 'Dạng select box (Drop menu)',
			3 => 'Dạng script '
			);
	
	$text = "<select size=1 name=\"type_ad\" id=\"type_ad\"  {$ext} class='select' >";  
	foreach ($arr_type as $key => $value)
	{
		$selected	 = ($key==$did) ? " selected ": "" ;
		$text .= "<option value=\"{$key}\" {$selected} > {$value} </option>";
	} 
	
  $text .= "</select>";
  return $text;
}


function listAlign ($did)
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"align\">";
  if ($did == "left")
    $text .= "<option value=\"left\" selected> Left </option>";
  else
    $text .= "<option value=\"left\" > Left </option>";
  if ($did == "center")
    $text .= "<option value=\"center\" selected> Center </option>";
  else
    $text .= "<option value=\"center\"> Center </option>";
  if ($did == "right")
    $text .= "<option value=\"right\" selected> Right </option>";
  else
    $text .= "<option value=\"right\"> Right </option>";
  $text .= "</select>";
  return $text;
}


//-----------------List_Type_Show
function List_Type_Show ($did)
{
  global $vnT;
  $arr_type_show = array(
    0 => 'Theo chiều dọc' , 
    1 => 'Theo chiều ngang' , 
    2 => 'Marquee up' , 
    3 => 'Marquee down' , 
    4 => 'Marquee left' , 
    5 => 'Marquee right');
  $text = "<select size=1 name=\"type_show\">";
  $p = 0;
  foreach ($arr_type_show as $key => $value) {
    if ($did == $key)
      $text .= '<option value="' . $key . '" selected>' . $value . '</option>';
    else
      $text .= '<option value="' . $key . '">' . $value . '</option>';
  }
  $text .= "</select>";
  return $text;
}

//---------- List_Pos
function List_Pos ($did = -1, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"pos\" class='select' style=\"width:50%\" {$ext} >";
  $res = $DB->query("select * from ad_pos ORDER BY  display_order ASC , id DESC");
  while ($row = $DB->fetch_row($res)) {
    if ($did == $row['name'])
      $text .= "<option value=\"{$row['name']}\" selected> " . $row['title'] . " </option>";
    else
      $text .= "<option  value=\"{$row['name']}\" > " . $row['title'] . " </option>";
  }
  $text .= "</select>";
  return $text;
}

//--------------------
function List_Module_Show ($did = "")
{
  global $func, $DB, $conf, $vnT;
  $all = "";
  if ($did)
    $arr_selected = explode(",", $did);
  else {
    $arr_selected = array();
    $all = "selected";
  }
  $text = "<select name=\"module_show[]\" id=\"module_show\" size=\"10\" multiple style='width:50%'>";
  $text .= "<option value='' {$all} >-- " . $vnT->lang['all'] . " --</option>";
  if (in_array("main", $arr_selected)) {
    $text .= "<option value='main' selected  > " . $vnT->lang['home'] . " </option>";
  } else {
    $text .= "<option value='main'  > " . $vnT->lang['home'] . " </option>";
  }
  if (in_array("about", $arr_selected)) {
    $text .= "<option value='about' selected  > " . $vnT->lang['about'] . " </option>";
  } else {
    $text .= "<option value='about'  > " . $vnT->lang['about'] . " </option>";
  }
  if (in_array("contact", $arr_selected)) {
    $text .= "<option value='contact' selected  > " . $vnT->lang['contact'] . " </option>";
  } else {
    $text .= "<option value='contact'  > " . $vnT->lang['contact'] . " </option>";
  }
  if (in_array("page", $arr_selected)) {
    $text .= "<option value='page' selected  > " . $vnT->lang['mod_page'] . " </option>";
  } else {
    $text .= "<option value='page'  > " . $vnT->lang['mod_page'] . " </option>";
  }
  $sql = "select * from modules order by id DESC";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $text .= "<option value=\"" . $row['mod_name'] . "\"";
    if (in_array($row['mod_name'], $arr_selected)) {
      $text .= " selected";
    }
    $text .= ">" . $row['name'] . "</option>\n";
  }
  $text .= "</select>";
  return $text;
}

//==========
function List_Module ($did = "", $ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select name=\"module\" class='select' {$ext} >";
  $text .= "<option value='' selected  > -- Tất cả module -- </option>";
  if ($did == "main") {
    $text .= "<option value='main' selected  >  " . $vnT->lang['home'] . " </option>";
  } else {
    $text .= "<option value='main'  >  " . $vnT->lang['home'] . "</option>";
  }
  if ($did == "about") {
    $text .= "<option value='about' selected  >  " . $vnT->lang['about'] . " </option>";
  } else {
    $text .= "<option value='about'  >  " . $vnT->lang['about'] . " </option>";
  }
  if ($did == "contact") {
    $text .= "<option value='contact' selected  >  " . $vnT->lang['contact'] . " </option>";
  } else {
    $text .= "<option value='contact'  >  " . $vnT->lang['contact'] . " </option>";
  }
  if ($did == "page") {
    $text .= "<option value='page' selected  >  " . $vnT->lang['mod_page'] . " </option>";
  } else {
    $text .= "<option value='page'  >  " . $vnT->lang['mod_page'] . " </option>";
  }
  $sql = "select * from modules order by id DESC";
  $result = $DB->query($sql);
  while ($row = $DB->fetch_row($result)) {
    $text .= "<option value=\"" . $row['mod_name'] . "\"";
    if ($did == $row['mod_name']) {
      $text .= " selected";
    }
    $text .= ">" . $row['name'] . "</option>\n";
  }
  $text .= "</select>";
  return $text;
}

//========== List_Pos_Custom
function List_Pos_Custom ($did, $ext = '')
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"pos\" id=\"pos\" $ext >";
  $text .= "<option value=''  > -- Chọn vị trí -- </option>";
  if ($did == "left")
    $text .= "<option value=\"left\" selected> Logo Bên trái </option>";
  else
    $text .= "<option value=\"left\" > Logo Bên trái </option>";
  if ($did == "right")
    $text .= "<option value=\"right\" selected>Logo Bên phải </option>";
  else
    $text .= "<option value=\"right\"> Logo Bên phải </option>";
  $text .= "</select>";
  return $text;
}

function List_Target ($did = '_selt',$ext="")
{
  global $func, $DB, $conf;
	$arr_item = array("_self"=>"Tại trang (_self)","_blank"=>"Cửa sổ mới (_blank)","_parent"=>"Cửa sổ cha (_parent)","_top"=>"Cửa sổ trên cùng (_top)") ;
	
  $text = "<select size=1 name=\"target\" id='target' class='select'  {$ext} >";
	foreach ($arr_item as $key => $value)
	{
		$selected = ($key==$did) ? "selected" : "";
		$text .= "<option value=\"{$key}\" {$selected} > {$value} </option>";
	}
  
  $text .= "</select>";
  return $text;
}

//---------- list_background_pos
function list_background_pos ($did,$lang="vn", $ext = "")
{
  global $func, $DB, $conf;
	$arr_pos = array();
  $arr_detault = array("gallery"=>"Thư viện ảnh", "video"=>"Video", "pages"=>"Trang tĩnh");

	$arr_menu = array();
	$res_menu = $DB->query("SELECT * FROM menu WHERE parentid=0 AND display=1 AND	 pos='horizontal'  AND lang='$lang'	 ORDER BY menu_order ASC, menu_id ASC ");
	while($row_menu = $DB->fetch_row($res_menu))
	{
		$title = $func->HTML($row_menu['title']);
		$arr_menu[$row_menu['name']] = $title;
	}	
	
	$arr_pos = array_merge($arr_menu,$arr_detault);
   
	$text = "<select size=1 name=\"pos\" id=\"pos\"  {$ext} class='select' >";
	$text .= "<option value=''  > -- Chọn vị trí -- </option>";
	foreach ($arr_pos as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	 : "";
		$text .= "<option value=\"{$key}\" {$selected} > {$value} </option>";
	}
   
  $text .= "</select>";
  return $text;
}

?>