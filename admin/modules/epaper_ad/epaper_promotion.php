<?php
/*================================================================================*\
|| 							Name code : pic_epaper.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
  var $action = "epaper_promotion";
  
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    
    $vnT->html->addStyleSheet( "modules/".$this->module."_ad/css/".$this->module.".css");
		$vnT->html->addScript( "modules/".$this->module."_ad/js/".$this->module.".js");
    
    switch ($vnT->input['sub'])
    {
      
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_epaper_promotion'];  
        $nd['content'] = $this->do_Edit($lang);
        break;
			
      case 'del':
        $this->do_Del($lang);
        break;
			
      default:
        $nd['f_title'] = $vnT->lang['manage_epaper_promotion']; 
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  
  	
	/**
   * function do_Edit
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    $id = (int) $vnT->input['id'];
		
		
	  $ext = $vnT->input['ext'];
	  $err="";
		
		if ( isset($vnT->input['do_submit'])) 
		{
			$data = $_POST;
			$dup['price'] = $vnT->input['price'];
			$dup['price_old'] = $vnT->input['price_old'];
			$dup['date_post'] = time();
			$ok = $DB->do_update("epaper",$dup,"e_id=$id");
		
			if ($ok){ 
        //update content
        $cot_d['desc_status'] = $func->txt_HTML($data['desc_status']);  
        $DB->do_update("epaper_desc", $cot_d, " e_id={$id} and lang='{$lang}'");
				
				//xoa cache
				$func->clear_cache();
				
				//insert adminlog
				$func->insertlog("Edit", $_GET['act'], $id);
					
					
				$err = $vnT->lang["edit_success"] ;
				$ext_page = str_replace("|","&",$ext);
				$url = $this->linkUrl."&{$ext_page}";
				$func->html_redirect($url,$err);
			}else{
				$err = $func->html_err($vnT->lang["edit_failt"]) ; 
			}
		}
		
		$result = $DB->query("SELECT *
													FROM epaper p, epaper_desc pd
													WHERE p.e_id=pd.e_id 
													AND pd.lang='$lang'
													AND p.e_id=$id") ;
		if($data = $DB->fetch_row($result))
		{
			if (!empty($data['picture'])) 
			{
				$pic = get_pic_epaper ($data['picture'],100);
				$data['pic'] = "{$pic}<br>";
			}
			$data['maso'] = $data['maso'];
			$data['p_name'] = $func->HTML($data['p_name']);
			$data['desc_status'] = $func->txt_unHTML($data['desc_status']);
		}else{
			$mess = $vnT->lang['not_found']." ID : ".$id ; 
			$url = $this->linkUrl;
			flush();
					echo $func->html_redirect($url,$mess);
			exit();
		}
		
    
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

	
	/**
   * function do_Del
   * Xoa 1 ... n  
   **/
	 
	 function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    
    if ($id != 0)
    {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"]))
    {
      $ids = implode(',', $vnT->input["del_id"]);
    } 

    $query = 'UPDATE epaper SET status=0 ,  price_old=0 WHERE e_id IN (' . $ids . ')';
    if ($ok = $DB->query($query))
    { 
			$query = 'UPDATE epaper_desc SET desc_status=""  WHERE  e_id IN (' . $ids . ')';
      $mess = $vnT->lang["del_success"];			
			//xoa cache
			$func->clear_cache(); 
    } else
      $mess = $vnT->lang["del_failt"];
    
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl;
    $func->html_redirect($url, $mess);
  }

  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['e_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
   	$link_del = "javascript:del_item('".$this->linkUrl."&sub=del&id={$id}&ext={$row['ext_page']}')";
		$link_pro = "?mod=epaper&act=epaper&sub=edit&id={$id}&lang=$lang";
		
			
		if ($row['picture']){
			$output['picture']= get_pic_epaper($row['picture'],50);
		}else $output['picture'] ="No image";		
		
		$output['p_name'] = "Mã số : <b class=font_err>".$row['maso']."</b><br><a href=\"{$link_pro}\">".$func->HTML($row['p_name'])."</a>";
	
		if ($row['price_old'] ) 
			$output['price'] = "<b class='font_err'>".get_price_pro($row['price'])."</b><br><s>".get_price_pro($row['price_old'])."</s>";
		else
			$output['price'] = "<b class='font_err'>".get_price_pro($row['price'])."</b>";

		$output['desc_status'] = $func->cut_string($row['desc_status'],100,1)."&nbsp;";
	
	
		if ($row['display']==1){
			$display ="<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />" ;
		}else{
			$display ="<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />" ;
		}
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $array_promotion=array();
		$res = $DB->query("select status_id from epaper_status WHERE display=1 AND edit_price=1 ");
		while($r= $DB->fetch_row($res))
		{
			$array_promotion[] = $r['status_id'];
		}
		$id_promotion = @implode(",",$array_promotion);		
    
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$cat_id = ((int) $vnT->input['cat_id']) ?   $vnT->input['cat_id'] : 0;
		$status_id = ((int) $vnT->input['status_id']) ?  $vnT->input['status_id'] : 0;
		$search = ($vnT->input['search']) ?  $vnT->input['search'] : "e_id";
		$keyword = ($vnT->input['keyword']) ?  $vnT->input['keyword'] : "";
		$direction = ($vnT->input['direction']) ?   $vnT->input['direction'] : "DESC";
		
		$where ="";
		
		if($status_id){
			$where .=" AND status=$status_id ";
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}else{
			$where .=" AND status in (".$id_promotion.") ";
		}
	
		if(!empty($cat_id)){
			$a_cat_id = List_SubCat($cat_id);
			$a_cat_id  = substr($a_cat_id,0,-1);
			if (empty($a_cat_id))
				$where .=" and FIND_IN_SET('$cat_id',cat_id)<>0 ";
			else{
				$tmp = explode(",",$a_cat_id);
				$str_= " FIND_IN_SET('$cat_id',cat_id)<>0 ";	
				for ($i=0;$i<count($tmp);$i++){
					$str_ .=" or FIND_IN_SET('$tmp[$i]',cat_id)<>0 ";
				}
				$where .=" and (".$str_.") ";
			} 
			$ext_page .="cat_id=$cat_id|";	
			$ext.="&cat_id=$cat_id";
		}
		
		if(!empty($search)){
			$ext_page.="search=$search|";
			$ext.="&search={$search}";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "e_id" : $where .=" and  p.e_id = $keyword ";  break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&keyword={$keyword}";
		}
		 
		$order_by = ($search=="e_id") ? " order by p.e_id $direction " : " order by $search $direction ";
		$ext_page=$ext_page."direction=$direction|p=$p";
		$ext.="&direction=$direction";
	
    $query = $DB->query("SELECT p.e_id
					FROM epaper p, epaper_desc pd
					WHERE p.e_id=pd.e_id 
					AND pd.lang='$lang'
					$where ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'picture' => $vnT->lang['picture']."|10%|center",
				'p_name' => $vnT->lang['epaper_name']."|30%|left",
				'price' => $vnT->lang['price']." |15%|center",
				'desc_status' => $vnT->lang['desc_status']."|30%|left",					
				'action' => "Action|10%|center"
				);
    
    $sql = "SELECT *
						FROM epaper p, epaper_desc pd
						WHERE p.e_id=pd.e_id 
						AND pd.lang='$lang'
						$where 
						$order_by LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['e_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_epaper'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
		$data['listcat']=Get_Cat($cat_id,$lang);
		$data['list_search']=List_Search($search);
		$data['list_direction']=List_Direction($direction);
		$data['list_status'] = List_Status_Promotion($status_id,$lang,"");
		$data['keyword'] = $keyword;
	
    $data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  
// end class
}

?>