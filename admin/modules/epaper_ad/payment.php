<?php
/*================================================================================*\
|| 							Name code : payment.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
  var $action = "payment";
  var $dirUpload = "modules/epaper_ad/payment/";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_payment'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_payment'];
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'config':
        $nd['f_title'] = $vnT->lang['config_payment'];
        $nd['content'] = $this->do_Config($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_payment'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them gioi thieu moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
		$vnT->input['display']=1;
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $name = $vnT->input['name'];
      
      // Check for existed
     /* $res_chk = $DB->query("SELECT * FROM payment_method  WHERE name='{$name}'  ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
        // insert CSDL*/
				
			//upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "")
      {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "payment";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 100;
        
        $result = $vnT->File->Upload($up);
        if (empty($result['err']))
        {
          $picture = $result['link'];
          $file_type = $result['type'];
        }
        else
        {
          $err = $func->html_err($result['err']);
        }
      }
      else
      {
        if ($vnT->input['picture'])
        {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "payment";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 100;
          
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err']))
          {
            $picture = $result['link'];
            $file_type = $result['type'];
          }
          else
          {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
			
			
      if (empty($err)) {
        $cot['name'] = $name;
        $cot['picture'] = $picture;
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_insert("payment_method", $cot);
        if ($ok) {
					$eid = $DB->insertid();
					$cot_d['eid'] = $eid;
					$cot_d['title'] = $vnT->input['title'];
					$cot_d['short'] = $func->txt_HTML($_POST['short']);
        	$cot_d['description'] = $_POST['description'];
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("payment_method_desc",$cot_d);
					}
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $DB->insertid());
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
    $data['list_display'] = vnT_HTML::list_yesno("display", $vnT->input['display']);
    $data["html_content"] = $vnT->editor->doDisplay('description', $vnT->input['description'], '100%', '400', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat admin
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    if ($vnT->input['do_submit']) {
      $data = $_POST;
      $name = $vnT->input['name'];
      // Check for existed
      /*$res_chk = $DB->query("SELECT * FROM payment_method  WHERE name='{$name}' and eid<>$id  ");
      if ($check = $DB->fetch_row($res_chk))
        $err = $func->html_err("Name existed");
			//check upload*/
				//upload
      if ($vnT->input['chk_upload'] && ! empty($_FILES['image']) && $_FILES['image']['name'] != "")
      {
        $up['path'] = MOD_DIR_UPLOAD;
        $up['dir'] = "payment";
        $up['file'] = $_FILES['image'];
        $up['type'] = "hinh";
        $up['w'] = 100;
        
        $result = $vnT->File->Upload($up);
        if (empty($result['err']))
        {
          $picture = $result['link'];
          $file_type = $result['type'];
        }
        else
        {
          $err = $func->html_err($result['err']);
        }
      }
      else
      {
        if ($vnT->input['picture'])
        {
          $up['path'] = MOD_DIR_UPLOAD;
          $up['dir'] = "payment";
          $up['url'] = $vnT->input['picture'];
          $up['type'] = "hinh";
          $up['w'] = 100;
          
          $result = $vnT->File->UploadURL($up);
          if (empty($result['err']))
          {
            $picture = $result['link'];
            $file_type = $result['type'];
          }
          else
          {
            $err = $func->html_err($result['err']);
          }
        }
      } //end upload
			
      	
      if (empty($err)) {
        $cot['name'] = $name;
        if ($vnT->input['chk_upload'] == 1 || ! empty($picture))
        { 
          $cot['picture'] = $picture;
        }
        $cot['display'] = $vnT->input['display'];
        $ok = $DB->do_update("payment_method", $cot, "eid=$id");
        if ($ok) {
					$cot_d['short'] = $func->txt_HTML($_POST['short']);
					$cot_d['title'] =  $vnT->input['title'];
        	$cot_d['description'] =  $_POST['description'];
					$DB->do_update("payment_method_desc",$cot_d," eid={$id} and lang='{$lang}'");
          //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
      }
    }
    $query = $DB->query("SELECT * FROM payment_method p, payment_method_desc pd WHERE p.eid=pd.eid AND p.eid=$id AND lang='$lang' ");
    if ($data = $DB->fetch_row($query)) {
      $data['title'] = $data['title'];
      $data['description'] = $data['description'];
			if (! empty($data['picture']))
      {
        $data['pic'] = "<img src=\"" . MOD_DIR_UPLOAD . "payment/{$data['picture']}\" ><br>";
      }
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '400', "Normal");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Config 
   * Cap nhat admin
   **/
  function do_Config ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    $query = $DB->query("SELECT * FROM payment_method WHERE id=$id ");
    if ($row = $DB->fetch_row($query)) {
      $name = $row['name'];
      $title = $func->HTML($row['title']);
    }
    //old
    $module_old = unserialize($row['config']);
    if (isset($vnT->input['btnConfig'])) {
      $module_new = $_POST['module'];
      if (is_array($module_old)) {
        $newConfig = $module_old;
        foreach ($module_new as $key => $value) {
          if ($module_new[$key] != $module_old[$key]) {
            $newConfig[$key] = $value;
          }
        }
      } else {
        $newConfig = $vnT->input['module'];
      }
      $cot['config'] = serialize($newConfig);
      $ok = $DB->do_update("payment_method", $cot, "id=$id");
      if ($ok) {
        $err = $vnT->lang["edit_success"];
        $url = $this->linkUrl . "&sub=config&id={$id}";
        flush();
        echo $func->html_redirect($url, $err);
        exit();
      } else {
        $err = $DB->debug();
      }
    }
    $module = unserialize($row['config']);
    $path_module = $this->dirUpload . $name . "/";
    if ($row['logo'])
      $logo = $path_module . $row['logo'];
    else {
      $logo = $this->dirUpload . "logo.gif";
    }
    $link_action = $this->linkUrl . "&sub=config&id={$id}";
    //	print $path_module."index.php";
    if (file_exists($path_module . "index.php")) {
      ob_start();
      include ($path_module . "index.php");
      $content = ob_get_contents();
      ob_end_clean();
    } else {
      $content = "
			
			<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
					<td height=\"100\" align=center class=font_err>KHÔNG CÓ CẤU HÌNH CHO PHƯƠNG THỨC NÀY</td>
					</tr>
					 <tr>
					<td height=\"50\" align=center class=font_err><a href=\"" . $this->linkUrl . "\">[ Back ]</a></td>
					</tr>
				</table>";
    }
    $textout .= $err;
    $textout .= $content;
    return $textout;
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM payment_method WHERE eid IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['eid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_config = $this->linkUrl . "&sub=config&id={$id}";
    $output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['p_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
    $output['title'] = "<a href=\"{$link_edit}\"><strong>" . $row['title'] . "</strong></a>";
    $text_edit = "payment_method|title|id=" . $row['id'] . "|" . $lang;
		 $output['short'] = $row['short'] ;
    $output['config'] = "<a href='{$link_config}'><img src=\"" . $vnT->dir_images . "/but_run.gif\" /></a>";
    if ($row['picture'])   {
      $output['picture'] = "<img src=\"" . MOD_DIR_UPLOAD . "payment/" . $row['picture'] . "\" width=50 />";
    } else {
      $output['picture'] = "No image";
		}
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
          if (isset($vnT->input["txt_Order"]))
            $arr_order = $vnT->input["txt_Order"];
          $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
          $str_mess = "";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['p_order'] = $arr_order[$h_id[$i]];
            $ok = $DB->do_update("payment_method", $dup, "eid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("payment_method", $dup, "eid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("payment_method", $dup, "eid=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update payment_method SET display=1 WHERE eid=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update payment_method SET display=0 WHERE eid=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ? $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $query = $DB->query("SELECT * FROM payment_method p, payment_method_desc pd Where p.eid=pd.eid AND lang='$lang'  ORDER BY  p_order ASC, p.eid DESC  ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)     $p = $num_pages;
    if ($p < 1)     $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'order' => $vnT->lang['order'] . "|5%|center" ,
      'title' => $vnT->lang['title'] . " |20%|left" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM payment_method p, payment_method_desc pd Where p.eid=pd.eid AND lang='$lang'  ORDER BY  p_order ASC, p.eid DESC LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_staff'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>
