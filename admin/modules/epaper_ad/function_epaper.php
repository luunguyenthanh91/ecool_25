<?php
/*================================================================================*\
|| 							Name code : funtion_epaper.php	 		 			          	     		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
* @version : 1.0
* @date upgrade : 11/12/2007 by Thai Son
**/

if (!defined('IN_vnT')){
     die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD','../vnt_upload/epaper/');
define('MOD_ROOT_URL',$conf['rooturl'].'modules/epaper/');

 
/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn"){
	global $vnT,$func,$DB,$conf;
	$setting = array();
	$result = $DB->query("select * from epaper_setting WHERE lang='$lang' ");
	$setting = $DB->fetch_row($result);
	foreach ($setting as $k => $v)	{
		$vnT->setting[$k] = stripslashes($v);
	}
	unset($setting);
}

//------create_maso
function create_maso($cat_id,$e_id) {
global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select name from epaper_category where cat_id='{$cat_id}'  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text =  ($row['name']) ? $row['name']."-".$e_id : $e_id ;
	}else{
		$text = $e_id  ;
	}
 	
	return $text;
}

//------get_dir_upload
function get_dir_upload($dir) {
	global $vnT,$func,$DB,$conf;
	
	$path_dir = MOD_DIR_UPLOAD.$dir; 
	$path_thumb_dir = MOD_DIR_UPLOAD.$dir."/thumbs";
	if (!file_exists($path_dir)){
	 @mkdir($path_dir,0777);
	 if (!file_exists($path_thumb_dir)){
			@mkdir($path_thumb_dir,0777);
			@exec("chmod 777 {$path_thumb_dir}");
		}
 }
 return  $dir;
} 

//-----------------  get_cat_name
function get_cat_name($cat_id,$lang)
{
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select cat_name from epaper_category_desc where cat_id =$cat_id and lang='{$lang}' ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['cat_name']);
	}
	return $text;
}

/***** Ham get_parentid *****/
function get_parentid ($cat_id){
global $func,$DB,$conf;
	$cid = list_parentid ($cat_id);
	$cid = substr ($cid,strrpos($cid,",")+1);
	return $cid;	
}

function list_parentid ($cat_id){
global $func,$DB,$conf;
	$res = $DB->query("SELECT parentid,cat_id FROM epaper_category where cat_id in (".$cat_id.") and parentid>0  ");
	while ($cat=$DB->fetch_row($res)) {
		$output .=",".$cat["parentid"];
		$output .=list_parentid($cat['parentid']);
	}
	
	return $output;	
}

//-----------------  get_catCode
function get_catCode($parentid,$cat_id) {
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select cat_id,cat_code from epaper_category where cat_id =$parentid ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $row['cat_code']."_".$cat_id;
	}else $text = $cat_id;
	
	return $text;
}


//-----------------  get_cat_name
function get_p_name($e_id,$lang)
{
	global $vnT,$func,$DB,$conf;
	$text="";
	$sql ="select p_name from epaper where e_id =$e_id  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['p_name']);
	}
	
	return $text;
}

//-----------------  get_pic_epaper
function get_pic_epaper ($picture,$w=150){
	global $vnT,$func,$DB,$conf;
	$out=""; $ext="";
	$w_thumb = ($vnT->setting['img_width_grid']) ? $vnT->setting['img_width_grid'] : 100 ;
		
	if($picture) 
	{
		$linkhinh = "../vnt_upload/epaper/book_image/".$picture;
 		$linkhinh = str_replace("//","/",$linkhinh);
		$dir = substr($linkhinh,0,strrpos($linkhinh,"/"));
		$pic_name = substr($linkhinh,strrpos($linkhinh,"/")+1) ;
		
		if($vnT->setting['thum_size'])
		{
			$file_thumbs = $dir."/thumbs/{$w_thumb}_".$pic_name;
			if (!file_exists($file_thumbs)) 
			{
				if (@is_dir($dir."/thumbs")) {
					@chmod($dir."/thumbs",0777);
				} else {
					@mkdir($dir."/thumbs",0777);
					@chmod($dir."/thumbs",0777);
				}		
				// thum hinh
				$func->thum($linkhinh, $file_thumbs, $w_thumb);
			} 
	
			$src = $file_thumbs;	
		}else{
			$src = $dir."/thumbs/".$pic_name;	
		}
	}
  
	if($w<$w_thumb) $ext = " width='$w' ";
	
	$out = "<img  src=\"{$src}\" {$ext} >";
	
	return $out ;
}
  
 //-----------------  get_pic_epaper
function get_pic_epaper_pages ($id ,$picture,$w=150){
	global $vnT,$func,$DB,$conf;
	$out=""; $ext="";
	$arr_pic = explode("/",$picture);
	$src = "../vnt_upload/epaper/pages".$id."/thumbs/".$arr_pic[1];
  
	$ext = " width='$w' ";
	
	$out = "<img  src=\"{$src}\" {$ext} >";
	
	return $out ;
}
 
 
//====== get_price_pro
function get_price_pro ($price,$default="Call"){
	global $func,$DB,$conf,$vnT;
	if ($price){
		$price = $func->format_number($price)." VNĐ" ;		
	}else{
		$price = $default;
	}
	return $price;
}

//=============================List_Cat
function List_Cat($did="",$lang="vn"){
	global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	
	$text= "<select name=\"list_cat[]\" id=\"list_cat\"  multiple >";
	$sql="SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name']);
		if (!in_array($row['cat_id'],$arr_selected)){
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
		$n=1;
		$text.=List_Sub($row['cat_id'],$n,$did,$lang);
	}
	
	$text.="</select>";
	return $text;
}
//---------------Get_Sub
function List_Sub($cid,$n,$did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang'
				AND parentid={$cid} 
				order by cat_order");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if (!in_array($cat['cat_id'],$arr_selected)){
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=List_Sub($cat['cat_id'],$n,$did,$lang);
	}
	return $output;
}

//----------------- List_Cat_Chose
function List_Cat_Chose($did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$text= "<select name=\"cat_chose[]\" id=\"cat_chose\"  multiple >";
	$sql="SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name']);
		if (in_array($row['cat_id'],$arr_selected)){
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
		$n=1;
		$text.=Get_Sub_Chose($row['cat_id'],$n,$did,$lang);
	}
	
	$text.="</select>";
	return $text;
}
//---------------Get_Sub_Chose
function Get_Sub_Chose($cid,$n,$did="",$lang){
global $func,$DB,$conf,$vnT;
	if ($did)
		$arr_selected = explode(",",$did);
	else{
		$arr_selected = array(
		  '0',
		);
	}
	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang'
				AND parentid={$cid} 
				order by cat_order");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if (in_array($cat['cat_id'],$arr_selected)){
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub_Chose($cat['cat_id'],$n,$did,$lang);
	}
	return $output;
}

/***** Ham List_SubCat *****/
function List_SubCat ($cat_id){
global $func,$DB,$conf,$vnT;
	$output="";
	$query = $DB->query("SELECT * FROM epaper_category WHERE parentid={$cat_id}");
	while ($cat=$DB->fetch_row($query)) {
		$output.=$cat["cat_id"].",";
		$output.=List_SubCat($cat['cat_id']);
	}
	return $output;
}
 
 
/*** Ham Get_Cat ****/
function Get_Cat($did=-1,$lang,$ext=""){
global $func,$DB,$conf;
	
	$text= "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
	$text.="<option value=\"0\">-- Root --</option>";
	$query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ");

	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		if ($cat['cat_id']==$did)
			$text.="<option value=\"{$cat['cat_id']}\" selected style='font-weight:bold;'>{$cat_name}</option>";
		else
			$text.="<option value=\"{$cat['cat_id']}\" style='font-weight:bold;' >{$cat_name}</option>";
		$n=1;
		$text.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	$text.="</select>";
	return $text;
}
/*** Ham Get_Sub   */
function Get_Sub($did,$cid,$n,$lang){
global $func,$DB,$conf;

	$output="";
	$k=$n;
	$query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid
				ORDER BY n.cat_order ASC, n.cat_id DESC");
	while ($cat=$DB->fetch_row($query)) {
		$cat_name = $func->HTML($cat['cat_name']);
		
		if ($cat['cat_id']==$did)	{
			$output.="<option value=\"{$cat['cat_id']}\" selected>";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}else{	
			$output.="<option value=\"{$cat['cat_id']}\" >";
			for ($i=0;$i<$k;$i++) $output.= "|-- ";
			$output.="{$cat_name}</option>";
		}
		$n=$k+1;
		$output.=Get_Sub($did,$cat['cat_id'],$n,$lang);
	}
	return $output;
}




//=============================List_Cat
function List_Cat_Root($did="",$ext){
global $func,$DB,$conf;
	
	$text= "<select name=\"cat_id\" id=\"cat_id\" {$ext}   >";
	$text.="<option value=\"\" selected> Chọn danh mục  </option>";
	$sql="SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				ORDER BY n.cat_order ASC, n.cat_id DESC ";
	$result = $DB->query ($sql);
	while ($row = $DB->fetch_row($result)){
		$cat_name = $func->HTML($row['cat_name'],$lang);
		if ($row['cat_id']==$did){
			$text .= "<option value=\"{$row['cat_id']}\" selected>".$cat_name."</option>";
		} else{
			$text .= "<option value=\"{$row['cat_id']}\">".$cat_name."</option>";
		}
	}
	
	$text.="</select>";
	return $text;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  
  $text .= "<option value=\"0\">-- Root --</option>";
  $query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=0 
				order by cat_order");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}
/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*,nd.cat_name FROM epaper_category n, epaper_category_desc nd
				where n.cat_id=nd.cat_id
				AND nd.lang='$lang' 
				AND n.parentid=$cid 
				order by cat_order");
  while ($cat = $DB->fetch_row($query))
  {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
    {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    else
    {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n, $lang);
  }
  return $output;
} 

//get_datetime
function  get_datetime ($gio,$ngay){
	$out="";
	$gio = explode(":",$gio);
	$ngay = explode("/",$ngay);
	$out  = mktime($gio[0],$gio[1], 0, $ngay[1] , $ngay[0], $ngay[2]);
	return $out;
}


 
?>