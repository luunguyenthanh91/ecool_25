<?php
/*================================================================================*\
|| 							Name code : status.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT'))
{
  die('Hacking attempt!');
}

$vntModule = new vntModule();
class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "epaper";
	var $action = "status";
	
  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_".$this->module.".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=".$this->module."&act=".$this->action."&lang=" . $lang;
		
    switch ($vnT->input['sub'])
    {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_status'];
        $nd['content'] = $this->do_Add($lang);
        break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_status'];
        $nd['content'] = $this->do_Edit($lang);
        break;
      case 'del':
        $this->do_Del($lang);
        break;
      default:
        $nd['f_title'] = $vnT->lang['manage_status'];
        $nd['content'] = $this->do_Manage($lang);
        break;
    }
    $nd['menu'] = $func->getToolbar($this->module,$this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=".$this->module."&act=".$this->action, $lang);
		
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  
  }
  
  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $err = "";
    $vnt->input['is_order']=1;
		$vnt->input['edit_price'] =0;
		
    if ($vnT->input['do_submit'] == 1)
    {
			$data = $_POST;
			
			$name = $vnT->input['name'];
			
			// Check for Error
			$res_chk = $DB->query("SELECT * FROM epaper_status  WHERE name='{$name}' ");
			if ($check=$DB->fetch_row($res_chk)) $err=$func->html_err("Name existed");
			
			//upload
			if(empty($err))
			{				
				if( $vnT->input['chk_upload'] && !empty($_FILES['image']) && $_FILES['image']['name']!="" ) {
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir']= "status";
					$up['file']= $_FILES['image'];
					$up['type']= "hinh";
					$up['w']= 100;
			
					$result = $vnT->File->Upload($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}else {
					if( $vnT->input['picture'] ) {
						$up['path'] = MOD_DIR_UPLOAD;
						$up['dir'] = "status";
						$up['url'] = $vnT->input['picture'];					
						$up['type'] = "hinh";
						$up['w']= 100;					
						
						$result = $vnT->File->UploadURL($up);
						if (empty($result['err'])) {
							$picture = $result['link'];
							$file_type = $result['type'];
						} else {
							$err = $func->html_err($result['err']);
						}					
					}
				}//end upload
			}
			
      // insert CSDL
      if (empty($err))
      {
				$cot['name'] = $name;
				$cot['picture'] = $picture; 
				$cot['is_order'] = $vnT->input['is_order'];
        $cot['edit_price'] = $vnT->input['edit_price'];
        $ok = $DB->do_insert("epaper_status", $cot);
        if ($ok)
        {
					$status_id = $DB->insertid();				
					//update content
					$cot_d['status_id'] = $status_id;
					$cot_d['title'] = $vnT->input['title'] ;
					
					$query_lang = $DB->query("select name from language ");
					while ( $row = $DB->fetch_row($query_lang) ) {
						$cot_d['lang'] = $row['name'];
						$DB->do_insert("epaper_status_desc",$cot_d);
					}
					
					//xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $status_id);
					
          $mess = $vnT->lang['add_success'];
          $url = $this->linkUrl . "&sub=add";
          $func->html_redirect($url, $mess);
        }
        else
        {
          $err = $func->html_err($vnT->lang['add_failt'].$DB->debug());
        }
      }
    
    } 
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=add";
    $data['list_isorder'] = vnT_HTML::list_yesno("is_order", $vnT->input['is_order']);
    $data['list_price'] = vnT_HTML::list_yesno("edit_price", $vnT->input['edit_price']);
		
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  
  }
  
  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
    $id = (int) $vnT->input['id'];
    
    if ($vnT->input['do_submit'])
    {
      $data = $_POST;
			
      $name = $vnT->input['name'];
			
			// Check for Error
			$res_chk = $DB->query("SELECT * FROM epaper_status  WHERE name='{$name}' and status_id<>$id ");
			if ($check=$DB->fetch_row($res_chk)) $err=$func->html_err("Name existed");
			
			//upload
			if(empty($err))
			{				
				if( $vnT->input['chk_upload'] && !empty($_FILES['image']) && $_FILES['image']['name']!="" ) {
					$up['path'] = MOD_DIR_UPLOAD;
					$up['dir']= "status";
					$up['file']= $_FILES['image'];
					$up['type']= "hinh";
					$up['w']= 100;
			
					$result = $vnT->File->Upload($up);
					if (empty($result['err'])) {
						$picture = $result['link'];
						$file_type = $result['type'];
					} else {
						$err = $func->html_err($result['err']);
					}
				}else {
					if( $vnT->input['picture'] ) {
						$up['path'] = MOD_DIR_UPLOAD;
						$up['dir'] = "status";
						$up['url'] = $vnT->input['picture'];					
						$up['type'] = "hinh";
						$up['w']= 100;					
						
						$result = $vnT->File->UploadURL($up);
						if (empty($result['err'])) {
							$picture = $result['link'];
							$file_type = $result['type'];
						} else {
							$err = $func->html_err($result['err']);
						}					
					}
				}//end upload
			}
      
      if (empty($err))
      {
				$cot['name'] = $name; 
				
				if ($vnT->input['chk_upload']==1 || !empty($picture) )	
				{					
					$img_q = $DB->query("SELECT picture FROM epaper_status WHERE status_id=$id ");
						if ($img=$DB->fetch_row($img_q)) {
							$file_pic = MOD_DIR_UPLOAD."status/".$img['picture'];
							if ( (file_exists($file_pic)) && (!empty($img['picture'])) )
								unlink($file_pic);
						}
						$cot['picture']	= $picture;
				}
				$cot['is_order'] = $vnT->input['is_order'];
        $cot['edit_price'] = $vnT->input['edit_price'];
				
				// more here ...
        $ok = $DB->do_update("epaper_status", $cot, "status_id=$id");
        if ($ok)
        {
					//update content
					$cot_d['title'] = $vnT->input['title'] ;
					$DB->do_update ("epaper_status_desc",$cot_d,"status_id=$id and lang='{$lang}'");
					
					//xoa cache
          $func->clear_cache();
					
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
          
          $err = $vnT->lang["edit_success"];
          $url = $this->linkUrl . "&sub=edit&id=$id";
          $func->html_redirect($url, $err);
        } else
          $err = $func->html_err($vnT->lang["edit_failt"].$DB->debug());
      }
    
    }
    
    $query = $DB->query("SELECT * FROM epaper_status n, epaper_status_desc nd 
												WHERE n.status_id=nd.status_id
												AND nd.lang='$lang'
												AND n.status_id=$id");
    if ($data = $DB->fetch_row($query))
    {
			if (!empty($data['picture'])) 	{
				$data['pic'] = "<img src=\"".MOD_DIR_UPLOAD."status/{$data['picture']}\" ><br>";
			}
			$data['list_isorder'] = vnT_HTML::list_yesno("is_order", $data['is_order']);
    $data['list_price'] = vnT_HTML::list_yesno("edit_price", $data['edit_price']);
    }else{
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
     
		
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
  
  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 		
		
		// Del Image
		$query = $DB->query("SELECT picture FROM epaper_status WHERE status_id IN (".$ids.") ");
		while ($img=$DB->fetch_row($query)) {
			$file_pic = MOD_DIR_UPLOAD."status/".$img['picture'];
			if ( (file_exists($file_pic)) && (!empty($img['picture'])) )
				unlink($file_pic);
		}
		// End del image
		
		$query = 'DELETE FROM epaper_status WHERE status_id IN ('.$ids.')' ;
		if ($ok = $DB->query($query))
		{
			$DB->query("DELETE FROM epaper_status_desc WHERE status_id IN (".$ids.")");
			$mess = $vnT->lang["del_success"] ;
		} else
			$mess = $vnT->lang["del_failt"] ;
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }
  
  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['status_id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . '&sub=edit&id=' . $id . '&ext=' . $row['ext_page'];
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    
		$output['order'] = "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"2\" maxlength=\"2\" style=\"text-align:center\" value=\"{$row['s_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
		if ($row['picture']){
			$output['picture']= "<img src=\"".MOD_DIR_UPLOAD."status/".$row['picture']."\"  />";
		}else $output['picture'] ="No image";
		
 		$output['name'] = "<a href=\"{$link_edit}\">".$row['name']."</a>";		 
    $output['title'] = "<strong>" . $func->HTML($row['title']) . "</strong>";
 		
		$output['focus'] = vnT_HTML::list_yesno("focus[{$id}]",$row['focus'],"onchange='javascript:do_check($id)'"); 
		$output['is_order'] = vnT_HTML::list_yesno("is_order[{$id}]", $row['is_order'], "onchange='javascript:do_check($id)'");
    $output['edit_price'] = vnT_HTML::list_yesno("edit_price[{$id}]", $row['edit_price'], "onchange='javascript:do_check($id)'");
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';    
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
		$output['action'] .= $display.'&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }
  
  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    
    //update
    if ($vnT->input["do_action"])
    {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"]) $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"])
      {
        case "do_edit":
            if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
						if (isset($vnT->input["focus"])) $focus = $vnT->input["focus"];
					  if (isset($vnT->input["is_order"]))   $is_order = $vnT->input["is_order"];
         	  if (isset($vnT->input["edit_price"]))    $edit_price = $vnT->input["edit_price"];
            $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['s_order'] = $arr_order[$h_id[$i]];
							$dup['focus'] = $focus[$h_id[$i]];
							$dup['is_order'] = $is_order[$h_id[$i]];
            	$dup['edit_price'] = $edit_price[$h_id[$i]];
              $ok = $DB->do_update("epaper_status", $dup, "status_id=" . $h_id[$i]);
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
        case "do_hidden":
            $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 0;
              $ok = $DB->do_update("epaper_status", $dup, "status_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);

          break;
        case "do_display":
            $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
            for ($i = 0; $i < count($h_id); $i ++)
            {
              $dup['display'] = 1;
              $ok = $DB->do_update("epaper_status", $dup, "status_id=" . $h_id[$i]);
              if ($ok){
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          break;
      }
    }
    
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update epaper_status SET display=1 WHERE status_id=".$vnT->input["do_display"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update epaper_status SET display=0 WHERE status_id=".$vnT->input["do_hidden"]);
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
		$p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
		$n = ($conf['record']) ? $conf['record'] : 30;
		
		$keyword	= ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";		
		$where ="";
		if($keyword){
			$where .=" AND title like '%$keyword%' ";
			$ext.="&keyword=$keyword";
		}
		
    $query = $DB->query("SELECT n.status_id FROM epaper_status n, epaper_status_desc nd 
													WHERE n.status_id=nd.status_id
													AND nd.lang='$lang'   
													{$where} ");
    $totals = intval($DB->num_rows($query));    
		
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages) $p = $num_pages;
    if ($p < 1) $p = 1;
    $start = ($p - 1) * $n;
    
    $nav = $func->paginate($totals, $n, $ext, $p);
    
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
		$ext_link = $ext."&p=$p" ;
    $table['title'] = array(
				'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
				'order' => $vnT->lang['order'] . "|10%|center" , 
				'picture' => "Icon|10%|center",
				'name' =>  $vnT->lang['name']."|15%|center",
				'title' => $vnT->lang['title']."|30%|left",				
				'is_order' => $vnT->lang['is_order'] . "|15%|center" , 
				'edit_price' => $vnT->lang['is_edit_price'] . "|15%|center" ,  
				'focus' => "Focus|15%|center",
				'action' => "Action|15%|center"
				);
    
    $sql = "SELECT * FROM epaper_status n, epaper_status_desc nd 
						WHERE n.status_id=nd.status_id
						AND nd.lang='$lang'  
						{$where} 
						ORDER BY  n.s_order  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $reuslt = $DB->query($sql);
    if ($DB->num_rows($reuslt))
    {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++)
      {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i],$lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['status_id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    }
    else
    {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_status'] ."</div>";
    }
    
		$table['button'] = '<input type="button" name="btnHidden" value=" '.$vnT->lang['hidden'].' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDisplay" value=" '.$vnT->lang['display'].' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnEdit" value=" '.$vnT->lang['update'].' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDel" value=" '.$vnT->lang['delete'].' " class="button" onclick="del_selected(\''.$this->linkUrl.'&sub=del&ext='.$ext_page.'\')">';
    
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
		$data['err'] = $err;
    $data['nav'] = $nav;
    
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
	
  // end class
}

?>