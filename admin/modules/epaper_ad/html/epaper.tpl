<!-- BEGIN: edit -->
<script language=javascript>
  $(document).ready(function() {
  	/*jQuery.validator.addMethod("image", function( value, element ) {
  		if ($("#image").val()==''){
  			return false ;
  		}else{
  			return true ;
  		}
  	}, "Vui long up hình");*/
  	$('#myForm').validate({
  		rules: {			
  			title: {
  				required: true,
  				minlength: 3
  			}
      },
      messages: {
  			title: {
  				required: "{LANG.err_text_required}",
  				minlength: "{LANG.err_length} 3 {LANG.char}" 
  			} 
  		}
  	});
  	{data.js_preview} 
  });
</script>
{data.err}
<form action="{data.link_action}" method="post" enctype="multipart/form-data" name="myForm" id="myForm" class="validate">
  <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
    <tr class="form-required">
      <td class="row1" >{LANG.title}: </td>
      <td class="row0">
        <input name="title" id="title" type="text" size="60" maxlength="250" value="{data.title}">
      </td>
    </tr>
    <tr >
      <td class="row1" width="20%">{LANG.picture}: </td>
      <td class="row0">
       	{data.pic}
        <div class="ext_upload">
          <input name="chk_upload" id="chk_upload" type="radio" value="1">Upload Picture &nbsp;&nbsp;&nbsp;
          <input name="image" type="file" id="image" size="30" maxlength="250" onclick="do_ChoseUpload('ext_upload',1);" >
        </div>
        <span class="font_err" style="padding:4px;">Kích thước: [1000 x 1427] hoặc [590 x 842]</span>
      </td>
    </tr>
    <tr>
      <td class="row1"><strong>File download :</strong></td>
      <td align="left" class="row0" >
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding:0px;">
              <input name="link_down" id="link_down" type="text" class="textfield" size="60" value="{data.link_down}"/>
            </td>
            <td style="padding:0px;">
              <div class="button2">
                <div class="image">
                  <a href="?mod=media&act=popup_media&folder=File/catalogues&type=textbox&obj=link_down&TB_iframe=true&width=900&height=474" title="Add an File" class="thickbox" id="add_image" >Browse File </a>
                </div>
              </div><br/>
            </td>
          </tr>
        </table>
        <span class="font_err">(Lưu ý :Có thể copy link downoad từ <strong>HOST khác</strong> hoặc <strong>Browse file từ Thư viện</strong>)</span>
      </td>
    </tr>
    <tr>
      <td class="row1">{LANG.display}</td>
      <td class="row0">{data.list_display}</td>
    </tr>
		<tr class="row_title" >
      <td colspan="2" class="font_title" >Search Engine Optimization : </td>
    </tr>
    <tr>
      <td class="row1" valign="top" style="padding-top:10px;">Friendly URL :</td>
      <td class="row0"><input name="friendly_url" id="friendly_url" type="text" size="50" maxlength="250" value="{data.friendly_url}" class="textfield"><br><span class="font_err">({LANG.mess_friendly_url})</span></td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="60" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="60" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
		<tr align="center">
    <td class="row1" >&nbsp; </td>
			<td class="row0" >
				<input type="hidden" name="do_submit"	 value="1" />
				<input type="submit" name="btnAdd" value="Submit" class="button"> &nbsp;    
        <input type="submit" name="btn_preview" value="{LANG.btn_preview}" class="button">
			</td>
		</tr>
	</table>
</form>


<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
 
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->