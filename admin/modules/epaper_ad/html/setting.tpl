<!-- BEGIN: manage -->
<form action="{data.link_action}" method="post" name="f_config" id="f_config" >
{data.err}
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Search Engine Optimization (SEO):</strong></td>
	</tr>
    <tr>
      <td class="row1" valign="top" >Module Slogan :</td>
      <td class="row0">
        <input name="slogan" id="slogan" type="text" size="70" maxlength="250" value="{data.slogan}">
      </td>
    </tr>
    <tr>
      <td class="row1" valign="top" >Friendly Title :</td>
      <td class="row0"><input name="friendly_title" id="friendly_title" type="text" size="70" maxlength="250" value="{data.friendly_title}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Keyword :</td>
      <td class="row0"><input name="metakey" id="metakey" type="text" size="70" maxlength="250" value="{data.metakey}" class="textfield"></td>
    </tr>
    <tr>
      <td class="row1">Meta Description : </td>
      <td class="row0"><textarea name="metadesc" id="metadesc" rows="3" cols="50" style="width:90%" class="textarea">{data.metadesc}</textarea></td>
    </tr>
</table> 
<br>
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">Cấu hình chung file Đọc eppaer</strong></td>
	</tr>
	<tr>
    <td  class="row1">Link logo :</td>
    <td class="row0"><input name="link_logo" id="link_logo" type="text" size="70" maxlength="250" value="{data.link_logo}" class="textfield"></td>
  </tr>
  
   <tr >
    <td  class="row1">Logo trên trang đọc báo : </td>
    <td  class="row0">
    {data.pic_logo_flash}
     <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td style="padding:0px;" ><input name="logo_flash" id="logo_flash" type="text"  size="50" maxlength="250" value="{data.logo_flash}"></td>
        <td style="padding:0px;" > <div class="button2"><div class="image"><a title="Add an Image" class="thickbox" id="add_image" href="?mod=media&act=popup_media&type=textbox&folder=File&obj=logo_flash&TB_iframe=true&width=640&height=474" >Image</a></div></div></td>
      </tr>
    </table>
    
    </td>
  </tr>
  
  
</table>

 <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr class="row_title" >
		<td colspan="2" ><strong class="font_title">{LANG.config_epaper}</strong></td>
	</tr>
	 
  <tr>
    <td width="35%" class="row1" >{LANG.n_list} :</td>
    <td class="row0"><input name="n_list" size="20" type="text" class="textfiled" value="{data.n_list}" onKeyPress="return is_num(event,'n_list')" ></td>
  </tr>
  <tr>
    <td class="row1">{LANG.n_grid} :</td>
    <td class="row0"><input name="n_grid" size="20" type="text" class="textfiled" value="{data.n_grid}" onKeyPress="return is_num(event,'n_grid')"></td>
  </tr>

  <tr>
    <td class="row1">{LANG.show_nophoto}:</td>
    <td class="row0">{data.nophoto}</td>
  </tr>

  <tr>
    <td class="row1">{LANG.img_width_list}:</td>
    <td class="row0"><input name="img_width_list" size="20" type="text" class="textfiled" value="{data.img_width_list}" onKeyPress="return is_num(event,'img_width_list')"></td>
  </tr>
  <tr>
    <td class="row1">{LANG.img_width_grid}:</td>
    <td class="row0"><input name="img_width_grid" size="20" type="text" class="textfiled" value="{data.img_width_grid}" onKeyPress="return is_num(event,'img_width_grid')"></td>
  </tr>
  
  <tr>
    <td  class="row1">{LANG.img_width} :</td>
    <td class="row0"><input name="img_width" size="20" type="text" class="textfiled" value="{data.img_width}"  onkeypress="return is_num(event,'img_width')"></td>
  </tr>
  <tr>
    <td  class="row1">{LANG.img_height} :</td>
    <td class="row0"><input name="img_height" size="20" type="text" class="textfiled" value="{data.img_height}"  onkeypress="return is_num(event,'img_height')"></td>
  </tr>
 
</table>

 
<p align="center">
			<input type="hidden" name="num" value="{data.num}">
      <input type="hidden" name="do_submit" value="1">
      <input type="submit" name="btnEdit" value="Update >>" class="button">
</p>
</form>
<br />
<!-- END: manage -->