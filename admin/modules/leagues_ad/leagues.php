<?php
/*================================================================================*\
|| 							Name code : leagues.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "leagues";
  var $action = "leagues";
  var $setting = array();

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");    
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('CONF', $vnT->conf);
		$this->skin->assign('LANG', $vnT->lang);
		$this->skin->assign("DIR_JS", $vnT->dir_js);
		$this->skin->assign("DIR_IMAGE", $vnT->dir_images);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module . ".js");
    $vnT->html->addStyleSheet( "modules/" . $this->module."_ad/js/select2/select2.min.css");
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/select2/select2.min.js");
		
    loadSetting($lang);
		$nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
		
    switch ($vnT->input['sub']) {
      case 'add':
        $nd['f_title'] = $vnT->lang['add_leagues'];
        $nd['content'] = $this->do_Add($lang);
      break;
      case 'edit':
        $nd['f_title'] = $vnT->lang['edit_leagues'];
        $nd['content'] = $this->do_Edit($lang);
      break;
			case 'duplicate':
        $this->do_Duplicate($lang);
        break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = $vnT->lang['manage_leagues'];
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Add 
   * Them  moi 
   **/
  function do_Add ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#ngay').datepicker({
					changeMonth: true,
					changeYear: true
				});

			});
		
		");
    $w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 500;
    $w_thum = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $err = "";
    $data['display'] = 1;
    $data['gio'] = date("H:i");
    $data['ngay'] = date("d/m/Y");
    $data['poster'] = $vnT->admininfo['username'];
		$data['ref'] = 1;
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
		
    if ($vnT->input['do_submit'] == 1) {
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
			$cat_list = get_cat_list($cat_id);
      $team1 = $vnT->input['team1'];
      $team2 = $vnT->input['team2'];
      $gio = explode(":", $vnT->input['gio']);
      $ngay = explode("/", $vnT->input['ngay']);
      $date_post = mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
      $picture =  $vnT->input['picture'];
			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]); 
			
      if (empty($err)) {
								
        // lay hinh
        if ($vnT->setting['get_image']) {
          $tmp_content = replace_img($data['content'], $dir);
          $content = $tmp_content['result'];
          if (empty($picture)) {
            if ($tmp_content['url'])
              $picture = $tmp_content['url'];
          }
        } else {
          $content = $_POST['content'];
        }
      } //end if err			
      // insert CSDL
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id , 
					'cat_list' => $cat_list ,  
          'picture' => $picture ,
					'pic_social_network'  => $vnT->input['pic_social_network'] ,       
					'ref' => $vnT->input['ref'] , 
          'source' => $vnT->input['source'] , 
          'show_home' => $vnT->input['show_home'] , 
          'keyref_id' => $vnT->input['keyref_id'] , 
          'poster' => $vnT->input['poster'] , 
          'location' => $vnT->input['location'] , 
          'tyso_team1' => $func->txt_unHTML($vnT->input['tyso_team1']) , 
          'tyso_team2' => $func->txt_unHTML($vnT->input['tyso_team2']) , 
          'date_post' => $date_post , 
					'date_update' => time() , 
          'permesion_view' => $vnT->input['permesion_view'] , 
          'type_show' => $vnT->input['type_show'] 
					);
        
				$cot['adminid'] =  $vnT->admininfo['adminid'];
				$cot['tags'] = $info_tag['list_id'];
				$cot['tags_name'] = $info_tag['list_name'];
				
        $kq = $DB->do_insert("leagues", $cot);
        if ($kq) {
          
          $leaguesid = $DB->insertid();
					
          $DB->query("UPDATE leagues SET display_order=".$leaguesid." WHERE leaguesid=".$leaguesid);
					
					//desc
					$cot_d['leaguesid'] = $leaguesid;
					$cot_d['team1'] = $team1;
          $cot_d['team2'] = $team2;
					$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
					$cot_d['content'] = $DB->mySQLSafe($content);
					$cot_d['note'] = $vnT->input['note'];
					$cot_d['picturedes'] = $vnT->input['picturedes']; 
					
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? $func->make_url($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ;
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->txt_HTML($func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1));				 
					
					$cot_d['display'] = $vnT->input['display'];
          $cot_d['hot'] = $vnT->input['hot'];
					
				  $query_lang = $DB->query("select name from language ");
          while ($row_lang = $DB->fetch_row($query_lang))
          {
            $cot_d['lang'] = $row_lang['name'];
            $DB->do_insert("leagues_desc", $cot_d);
          }
					
					
					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $leaguesid;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$leaguesid;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE leagues_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE leaguesid=".$leaguesid) ; 
					}
					
					
           //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Add", $_GET['act'], $leaguesid);
          $mess = $vnT->lang['add_success'];
					
					if(isset($_POST['btn_preview']))	{
						$url = $this->linkUrl . "&sub=edit&id=$leaguesid&preview=1";
						$DB->query("Update leagues_desc SET display=0 WHERE leaguesid=$leaguesid ");
					}else{
						$url = $this->linkUrl . "&sub=add";						
					} 

          $func->html_redirect($url, $mess);
        } else {
          $err = $func->html_err($vnT->lang['add_failt'] . $DB->debug());
        }
      }
    }
   
	  $data['listcat'] = Get_Cat($vnT->input['cat_id'], $lang);
    $data['list_team1'] = Get_List_Team(-1,"A",$lang);
    $data['list_team2'] = Get_List_Team(-1,"B",$lang);
    
    $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
    $data['list_show_home'] = vnT_HTML::list_yesno("show_home", $data['show_home']);
    $data['list_hot'] = vnT_HTML::list_yesno("hot", $data['hot']);
    
		$data['list_keyref'] = List_Keyref($vnT->input['keyref_id']);
    $data['list_source'] = list_source($vnT->input['source'],$lang);
		$data['list_ref'] = vnT_HTML::selectbox("ref", array(        '0' => 'nofollow' ,         '1' => 'follow'), $data['ref']); 
    $data['list_permesion_view'] = vnT_HTML::selectbox("permesion_view", array(
        '0' => $vnT->lang['all'] , 
        '1' => $vnT->lang['only_member']), $data['permesion_view']);
    $data['list_type_show'] = vnT_HTML::selectbox("type_show", array(
        '0' => $vnT->lang['type_show_0']   , 
        '1' => $vnT->lang['type_show_1']   , 
        '2' => $vnT->lang['type_show_2'], 
        '3' => $vnT->lang['type_show_3']), $data['type_show']);
    
		$data["html_short"] = $vnT->editor->doDisplay('short', $vnT->input['short'], '100%', '200', "Normal",$this->module,$dir); 
		$data["html_content"] = $vnT->editor->doDisplay('content', $vnT->input['content'], '100%', '500', "Default",$this->module,$dir);
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
		
		$data['link_upload'] = '?mod=media&act=popup_media&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&type=image&TB_iframe=true&width=900&height=474';
			 
		$data['link_seo'] = $conf['rooturl']."xxx.html";
		if($data['friendly_url'])
			$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
			
    $data['err'] = $err;
    $data['display'] = 'style="display:none;"';
    $data['link_action'] = $this->linkUrl . "&sub=add";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Edit 
   * Cap nhat 
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet($vnT->dir_js."/autoSuggestv14/autoSuggest.css");
		$vnT->html->addScript($vnT->dir_js."/autoSuggestv14/jquery.autoSuggest.js");
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#ngay').datepicker({
					changeMonth: true,
					changeYear: true
				});

			});
		
		");
    $w = ($vnT->setting['img_width']) ? $vnT->setting['img_width'] : 500;
    $w_thum = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $id = (int) $vnT->input['id'];
		$dir = $vnT->func->get_dir_upload_module($this->module,$vnT->setting['folder_upload']);
		
    if ($vnT->input['do_submit']) 
		{
      $data = $_POST;
      $cat_id = $vnT->input['cat_id'];
			$cat_list = get_cat_list($cat_id) ;			
      $team1 = $vnT->input['team1'];
      $team2 = $vnT->input['team2'];
      $gio = @explode(":", $vnT->input['gio']);
      $ngay = @explode("/", $vnT->input['ngay']);
      $date_post = @mktime($gio[0], $gio[1], 0, $ngay[1], $ngay[0], $ngay[2]);
      $picture =  $vnT->input['picture'];
			$info_tag =  $vnT->func->get_input_tags_js ($this->module,$_POST["as_values_htag"]);  
			// echo "<pre/>";print_r($vnT->input);die;

      // banthang_team1
      if(isset($vnT->input['cauthu_ghiban_team1']))  {
        $cauthu_ghiban_team1 = $func->txt_unHTML($vnT->input['cauthu_ghiban_team1']);
        $time_ghiban_team1 = $vnT->input['time_ghiban_team1'];
        $array_t1 = array();
        // echo "<pre/>";print_r($cauthu_ghiban_team1);die;
        foreach ($cauthu_ghiban_team1 as $key => $value) {
          $array_t1[][$value] =  $time_ghiban_team1[$key];
        }
         // echo "<pre/>";print_r($array_t1);die;
        
        foreach ($array_t1 as $key1 => $value1) {
          foreach ($value1 as $key2 => $value2) {
            $banthang_team1 .= $key2.":".$value2."|";
          }
        }
      }else{
          $banthang_team1 = "";
      }

      // banthang_team2
      if(isset($vnT->input['cauthu_ghiban_team2'])) {
        $cauthu_ghiban_team2 = $func->txt_unHTML($vnT->input['cauthu_ghiban_team2']);
        $time_ghiban_team2 = $vnT->input['time_ghiban_team2'];
        $array_t2 = array();
        foreach ($cauthu_ghiban_team2 as $key => $value) {
          $array_t2[][$value] =  $time_ghiban_team2[$key];
        }

        foreach ($array_t2 as $key1 => $value1) {
          foreach ($value1 as $key2 => $value2) {
            $banthang_team2 .= $key2.":".$value2."|";
          }
        }
      }else{
        $banthang_team2 = "";
      }
      

      //upload
      if (empty($err)) {
         
        // lay hinh
        if ($vnT->setting['get_image']) 
				{
          $tmp_content = replace_img($data['content'], $dir);
          $content = $tmp_content['result'];
          if (empty($picture)) {
            if ($tmp_content['url'])
              $picture = $tmp_content['url'];
          }
        } else {
          $content = $_POST['content'];
        }
      } //end if err
      if (empty($err)) {
        $cot = array(
          'cat_id' => $cat_id , 
					'cat_list' => $cat_list ,
					'picture' => $picture ,
					'pic_social_network'  => $vnT->input['pic_social_network'] , 
					'ref' => $vnT->input['ref'] , 
          'source' => $vnT->input['source'] , 
          'show_home' => $vnT->input['show_home'] , 
          'keyref_id' => $vnT->input['keyref_id'] , 
          'poster' => $vnT->input['poster'] , 
          'date_post' => $date_post , 
          'location' => $vnT->input['location'] , 
          'tyso_team1' => $func->txt_unHTML($vnT->input['tyso_team1']) , 
          'tyso_team2' => $func->txt_unHTML($vnT->input['tyso_team2']) , 
          'banthang_team1' => $banthang_team1 , 
          'banthang_team2' => $banthang_team2 , 
					'date_update' => time() , 
          'permesion_view' => $vnT->input['permesion_view'] , 
          'type_show' => $vnT->input['type_show'] 
					);	
				$cot['tags'] = $info_tag['list_id'];
				$cot['tags_name'] = $info_tag['list_name'];				 
        $kq = $DB->do_update("leagues", $cot, "leaguesid=$id");
        if ($kq) {
          $cot_d['team1'] = $team1;
          $cot_d['team2'] = $team2;
					$cot_d['short'] = $DB->mySQLSafe($_POST['short']);
					$cot_d['content'] = $DB->mySQLSafe($content);
					$cot_d['note'] = $vnT->input['note'];
					$cot_d['picturedes'] = $vnT->input['picturedes']; 
					
					//SEO
					$cot_d['friendly_url'] = (trim($vnT->input['friendly_url'])) ? $func->make_url($vnT->input['friendly_url']) :  $func->make_url($_POST['title']);
					$cot_d['friendly_title'] = (trim($vnT->input['friendly_title'])) ? trim($vnT->input['friendly_title']) :  $title ;
					$cot_d['metakey'] = (trim($vnT->input['metakey'])) ? $vnT->input['metakey'] : $title;
					$cot_d['metadesc'] = (trim($vnT->input['metadesc'])) ? $vnT->input['metadesc'] : $func->txt_HTML($func->cut_string($func->check_html($_POST['short'],'nohtml'),200,1));         
					
					$cot_d['display'] = $vnT->input['display'];
          $cot_d['hot'] = $vnT->input['hot'];
					$DB->do_update("leagues_desc", $cot_d, "leaguesid=$id and lang='{$lang}'");
					
					//build seo_url
					$seo['sub'] = 'edit';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $id;
					$seo['friendly_url'] = $cot_d['friendly_url'] ;
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$id;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE leagues_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE lang='".$lang."' AND leaguesid=".$id) ;
					}
					
            //xoa cache
          $func->clear_cache();
          //insert adminlog
          $func->insertlog("Edit", $_GET['act'], $id);
 					
					$url = $this->linkUrl ;	
					$err = $vnT->lang["edit_success"];
          
          $func->html_redirect($url, $err);					
					
					
        } else {
          $err = $func->html_err($vnT->lang["edit_failt"] . $DB->debug());
        }
      } // if err
    } // if summit
    $query = $DB->query("SELECT * FROM leagues n, leagues_desc nd  WHERE n.leaguesid=nd.leaguesid AND lang='$lang' AND n.leaguesid=$id");
    if ($data = $DB->fetch_row($query)) 
		{
			
			if($vnT->input['preview']==1)	{
				  
				$link_preview = $conf['rooturl'];
				if($vnT->muti_lang) $link_preview .= $lang."/";				
				$link_preview.= $data['friendly_url'].".html/?preview=1";
				
				$mess_preview = str_replace(array("{title}","{link}"),array($data['p_name'],$link_preview),$vnT->lang['mess_preview']);
				$data['js_preview'] = "tb_show('".$mess_preview."', '".$link_preview."&TB_iframe=true&width=1000&height=700',null)";
			}
			
			//dir
      if ($data['picture']) {
				$dir = substr($data['picture'], 0, strrpos($data['picture'], "/"));				
        $data['pic'] = get_pic_thumb($data['picture'], $w_thum) . "  <a href=\"javascript:del_picture('picture')\" class=\"del\">Xóa</a>";
				$data['style_upload'] = "style='display:none' ";
      } else {
        $data['pic'] = "";
      }


      if ($data['banthang_team1']) {
        //echo "<pre/>";print_r($data['banthang_team1']);die;
        $string_banthang1 = $data['banthang_team1'];
        $banthang1 = explode("|", $string_banthang1);
        $array_banthang1 = array_filter($banthang1);
        foreach ($array_banthang1 as $key1 => $value1) {
          $sobanthang1 = explode(":", $value1);
          if($sobanthang1[0] == "" && $sobanthang1[1] == ""){
            $chitietbanthang1 .= '';
          }else{
            
            $list_cauthu = Get_List_Player($data['team1'],A,$lang,$sobanthang1[0]);
            $chitietbanthang1 .= '<li><label>Cầu thủ ghi bàn : </label><select class="player1" name="cauthu_ghiban_team1[]">'.$list_cauthu.'</select><label>&nbsp;&nbsp;&nbsp;Phút : </label><input name="time_ghiban_team1[]" id="time_ghiban_team1" type="text" maxlength="250" value="'.$sobanthang1[1].'" class="textfield" style="width:5%"> <a href="#" class="delete" style="color:red">Xóa</a></li>';
          }
        }
        $data['banthang_team1'] = $chitietbanthang1;
      } else{
        $data['banthang_team1'] = "";
      }


      if ($data['banthang_team2']) {
        
        $string_banthang2 = $data['banthang_team2'];
        $banthang2 = explode("|", $string_banthang2);
        $array_banthang2 = array_filter($banthang2);
        foreach ($array_banthang2 as $key2 => $value2) {
          $sobanthang2 = explode(":", $value2);
          if($sobanthang2[0] == "" && $sobanthang2[1] == ""){
            $chitietbanthang2 .= '';
          }else{
            $list_cauthu = Get_List_Player($data['team2'],B,$lang,$sobanthang2[0]);
            // echo "<pre/>";print_r($list_cauthu);die;
            $chitietbanthang2 .= '<li><label>Cầu thủ ghi bàn : </label><select class="player2" name="cauthu_ghiban_team2[]">'.$list_cauthu.'</select><label>&nbsp;&nbsp;&nbsp;Phút : </label><input name="time_ghiban_team2[]" id="time_ghiban_team2" type="text" maxlength="250" value="'.$sobanthang2[1].'" class="textfield" style="width:5%"> <a href="#" class="delete" style="color:red">Xóa</a></li>';
          }
        }
        // echo "<pre/>";print_r($chitietbanthang2);die;
        $data['banthang_team2'] = $chitietbanthang2;
      } else{
        $data['banthang_team2'] = "";
      }




      $data['name_team1'] = get_team_name($data['team1'],$lang);
      $data['name_team2'] = get_team_name($data['team2'],$lang);
      //echo "<pre/>";print_r($data);die;
      $data['team1'] = $func->txt_unHTML($data['team1']);      
      $data['team2'] = $func->txt_unHTML($data['team2']);
      
      $data['picturedes'] = $func->txt_unHTML($data['picturedes']);
      $data['source'] = $func->txt_unHTML($data['source']);
      $data['note'] = $func->txt_unHTML($data['note']);
      $data['poster'] = $func->txt_unHTML($data['poster']);
      $data['gio'] = date("H:i", $data['date_post']);
      $data['ngay'] = date("d/m/Y", $data['date_post']);
      $data['tyso_team1'] = $data['tyso_team1'] ;
      $data['tyso_team2'] = $data['tyso_team2'] ; 
      $data['list_display'] = vnT_HTML::list_yesno("display", $data['display']);
      $data['list_show_home'] = vnT_HTML::list_yesno("show_home", $data['show_home']);
      $data['list_hot'] = vnT_HTML::list_yesno("hot", $data['hot']);
      $data['listcat'] = Get_Cat($data['cat_id'], $lang);
      $data['list_team1'] = Get_List_Team($data['team1'],1,$lang);
      $data['list_team2'] = Get_List_Team($data['team2'],2,$lang);
      $data['list_player1'] = Get_List_Player($data['team1'],A,$lang);
      $data['list_player2'] = Get_List_Player($data['team2'],B,$lang);
      $data['list_keyref'] = List_Keyref($data['keyref_id']);
			$data['list_ref'] = vnT_HTML::selectbox("ref", array(        '0' => 'nofollow' ,         '1' => 'follow'), $data['ref']); 
      $data['list_source'] = list_source($data['source'],$lang);
      $data['list_permesion_view'] = vnT_HTML::selectbox("permesion_view", array(
        '0' => $vnT->lang['all'] , 
        '1' => $vnT->lang['only_member']), $data['permesion_view']);
      $data['list_type_show'] = vnT_HTML::selectbox("type_show", array(
        '0' => $vnT->lang['type_show_0']   , 
        '1' => $vnT->lang['type_show_1']   , 
        '2' => $vnT->lang['type_show_2'], 
        '3' => $vnT->lang['type_show_3']), $data['type_show']);
			
			$data['checked_pic_social_network'] = ($data['pic_social_network']==1) ? " checked ='checked' " : "";
			
			$data['link_seo'] = $conf['rooturl']."xxx.html";
			if($data['friendly_url'])
				$data['link_mxh'] = $conf['rooturl'].$data['friendly_url'].".html";
			
			$data['img_mxh'] = '<div class="face-img"><img src="'.$conf['rooturl'].'image.php?image='.$conf['rooturl'].ROOT_UPLOAD.'/'.$data['picture'].'&width=120&height=120&cropratio=1:1" alt="" /></div>';
			
    } else {
      $mess = $vnT->lang['not_found'] . " ID : " . $id;
      $url = $this->linkUrl;
      $func->html_redirect($url, $mess);
    }
 		
		$vnT->func->get_tags_js($this->module,"tags","htag",$data['tags']);
			
    $data["html_short"] = $vnT->editor->doDisplay('short', $data['short'], '100%', '200', "Normal",$this->module,$dir); 
		$data["html_content"] = $vnT->editor->doDisplay('content', $data['content'], '100%', '500', "Default",$this->module,$dir);
		$data['link_upload'] = '?mod=media&act=popup_media&type=modules&module='.$this->module.'&folder='.$this->module.'/'.$dir.'&obj=picture&TB_iframe=true&width=900&height=474';
		
    $data['err'] = $err;
		
					
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }
	
	/**
   * function do_Duplicate 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Duplicate ($lang)
  {
    global $func, $DB, $conf, $vnT;

		$id = (int) $vnT->input['id'];
		$ext = $vnT->input["ext"];
		$del = 0;
		$qr = "";
			
		if ($id != 0) {
			$ids = $id;
		}
		if (isset($vnT->input["del_id"])){
			$ids = implode(',', $vnT->input["del_id"]);
		} 	
		
		$arr_duplicated = array();
 		 
		$res = $DB->query("SELECT * FROM leagues WHERE leaguesid IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {			
      while ($row = $DB->fetch_row($res))  {
				//Duplicate products
        $cot = $row;
				$cot["leaguesid"] = ""; 
				$cot["viewnum"] = 0;
				$cot["date_post"] = time();
				$cot["date_update"] = time();
				$DB->do_insert("leagues", $cot);
				//End Duplicate products
				
				$leaguesid = $DB->insertid();
				if($leaguesid)
				{
					$DB->query("UPDATE leagues SET display_order=".$leaguesid." WHERE leaguesid=".$leaguesid);
					
					//Duplicate products_desc
					$res_d = $DB->query("SELECT * FROM leagues_desc WHERE leaguesid=".$row['leaguesid']." ");
					while ($row_d = $DB->fetch_row($res_d))  {
						$dup_d = $row_d;
						$dup_d["id"] = "";
						$dup_d["leaguesid"] = $leaguesid;
						
						$arr_duplicated[$row_d["lang"]][] = $dup_d['friendly_url'];
						//$dup_d["friendly_url"] .= "-".time();
						while(in_array($dup_d['friendly_url'], $arr_duplicated[$row_d["lang"]]))
						{
							$dup_d['friendly_url'] .= "-".time() ;
						}
						$arr_duplicated[$row_d["lang"]][] = $dup_d['friendly_url'];
					
						$DB->do_insert("leagues_desc", $dup_d);
					}
					//End Duplicate products_desc
					
					//build seo_url
					$seo['sub'] = 'add';
					$seo['modules'] = $this->module;
					$seo['action'] = "detail";
					$seo['name_id'] = "itemID";
					$seo['item_id'] = $leaguesid;
					$seo['friendly_url'] = $dup_d['friendly_url'];
					
					$seo['lang'] = $lang;					
					$seo['query_string'] = "mod:".$seo['modules']."|act:".$seo['action']."|".$seo['name_id'].":".$leaguesid;
					$res_seo = $func->build_seo_url($seo);
					if($res_seo['existed']==1){
						$DB->query("UPDATE leagues_desc SET friendly_url='".$res_seo['friendly_url']."' WHERE leaguesid=".$leaguesid) ; 
					}
					
					
				}
      }
      $mess = $vnT->lang["duplicate_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["duplicate_failt"];
    } 
		 
		$ext_page = str_replace("|", "&", $ext);
		$url = $this->linkUrl . "&{$ext_page}";
		$func->html_redirect($url, $mess);
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    
		$res = $DB->query("SELECT leaguesid FROM leagues WHERE leaguesid IN (" . $ids . ") ");
    if ($DB->num_rows($res))
    {
      $DB->query("UPDATE leagues_desc SET display=-1 WHERE  leaguesid IN (" . $ids . ") AND lang='".$lang."' ");

      //insert RecycleBin
      $rb_log['module'] = $this->module;
      $rb_log['action'] = "detail";
      $rb_log['tbl_data'] = "leagues_desc";
      $rb_log['name_id'] = "leaguesid";
      $rb_log['item_id'] = $ids;
      $rb_log['lang'] = $lang;
      $func->insertRecycleBin($rb_log);

      $mess = $vnT->lang["del_success"];
			//xoa cache
      $func->clear_cache();
    } else  {
      $mess = $vnT->lang["del_failt"];
    }  
     
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['leaguesid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
		$link_duplicate = $this->linkUrl . '&sub=duplicate&id=' . $id;
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $url_view = "zoom.php?image=" . MOD_DIR_UPLOAD . "{$row['picture']}&title=Zoom";
      $picture = "<a href=\"#Zoom\" onClick=\"openPopUp('{$url_view}', 'Zoom', 700, 600, 'yes')\">" . get_pic_thumb($row['picture'], 40) . "</a>";
    }
		
		 $output['order'] = $row['ext'] . "<input name=\"txt_Order[{$id}]\" type=\"text\" size=\"3\"  style=\"text-align:center\" value=\"{$row['display_order']}\"  onkeypress=\"return is_num(event,'txtOrder')\" onchange='javascript:do_check($id)' />";
		
    
    $team1 = get_team_name($row['team1'],$lang);
    $team2 = get_team_name($row['team2'],$lang);

    if($row['tyso_team1'] == ""){
      $row['tyso_team1'] = "--";
    }
    if($row['tyso_team2'] == ""){
      $row['tyso_team2'] = "--";
    }

    $output['picture'] = $team1['pic']." -vs- ".$team2['pic'] ;
 		$output['title'] = "<strong><a href='". $link_edit ."' >" . $team1['name'] ." &nbsp;<b style='color:#F77225'>[ ".$row['tyso_team1']." : ".$row['tyso_team2']." ] </b>&nbsp;". $team2['name'] . "</a></strong>";
		//$output['title'] .= ' <span class=font_err ><small>(ID: '.$row['leaguesid'].')</small></span>'; 
		//$output['title'] .= ' <div>Friendly url: '.$row['friendly_url'].'</div>'; 
		
 		//$info = "<div style='padding:2px;'>".$vnT->lang['date_post']." : <b>".@date("d/m/Y",$row['date_post'])."</b></div>";
    $info = "<div style='padding:2px;'>Ngày thi đấu : <b>".@date("H:i", $row['date_post'])." | ".@date("d/m/Y",$row['date_post'])."</b></div>";
    $info .= "<div style='padding:2px;'>Nơi thi đấu : <b>".$row['location']."</b></div>";
    $info .= "<div style='padding:2px;'>ID trận đấu : <b>".$row['leaguesid']."</b></div>";
 		//$info .=  "<div style='padding:2px;'>".$vnT->lang['views']." : <strong>".(int)$row['viewnum']."</strong></div>";
		$output['info'] = $info ;
		
    $data['gio'] = date("H:i", $data['date_post']);
      $data['ngay'] = date("d/m/Y", $data['date_post']);

    $output['cat_name'] = get_cat_name($row['cat_id'], $lang).' <small style="color:#00F;">(ID:'.$row['cat_id'].') </small>';
		
		
		$link_display = $this->linkUrl.$row['ext_link']; 		
    if ($row['display'] == 1) {
      $display = "<a href='".$link_display."&do_hidden=$id' title='".$vnT->lang['click_do_hidden']."' ><img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  /></a>";
    } else {
      $display = "<a href='".$link_display."&do_display=$id'  title='".$vnT->lang['click_do_display']."' ><img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 /></a>";
    }
		
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
		$output['action'] .= '<a href="' . $link_duplicate . '"><img src="' . $vnT->dir_images . '/icon_duplicate.png"  alt="Duplicate "></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_edit . '" ><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
		$vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_begin').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});
					$('#date_end').datepicker({
						showOn: 'button',
						buttonImage: '".$vnT->dir_images."/calendar.gif',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true
					});

			});
		
		"); 
		
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])   $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
					if (isset($vnT->input["txt_Order"])) $arr_order = $vnT->input["txt_Order"];
					$mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
					$str_mess = "";
					for ($i = 0; $i < count($h_id); $i ++)
					{
						$dup['display_order'] = $arr_order[$h_id[$i]]; 						 
						$dup['date_update'] = time();
						$ok = $DB->do_update("leagues", $dup, "leaguesid=" . $h_id[$i]);
						if ($ok) {
							$str_mess .= $h_id[$i] . ", ";
						}
					}
					$mess .= substr($str_mess, 0, - 2) . "</strong><br>";
					$err = $func->html_mess($mess);
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("leagues_desc", $dup, "leaguesid={$h_id[$i]} AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("leagues_desc", $dup, "leaguesid={$h_id[$i]}  AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }       
    }
		
		if((int)$vnT->input["do_display"]) {				
			$ok = $DB->query("Update leagues_desc SET display=1 WHERE leaguesid=".$vnT->input["do_display"]."  AND lang='$lang' ");
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_display"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}        
			//xoa cache
      $func->clear_cache();
		}
		if((int)$vnT->input["do_hidden"]) {				
			$ok = $DB->query("Update leagues_desc SET display=0 WHERE leaguesid=".$vnT->input["do_hidden"]."  AND lang='$lang' ");
			if($ok){
				$mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>".$vnT->input["do_hidden"] . "</strong><br>";	
				$err = $func->html_mess($mess);
			}    
			//xoa cache
      $func->clear_cache();
		}
		
    $p = ((int) $vnT->input['p']) ?  $vnT->input['p'] : 1;
      $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int) $vnT->input['cat_id']) ?  $vnT->input['cat_id'] : 0;
    $search = ($vnT->input['search']) ? $vnT->input['search'] : "leaguesid";
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		$adminid = ((int) $vnT->input['adminid']) ?   $vnT->input['adminid'] : 0;
		$date_begin = ($vnT->input['date_begin']) ?  $vnT->input['date_begin'] : "";
		$date_end = ($vnT->input['date_end']) ?  $vnT->input['date_end'] : "";

    $where = " ";
    $ext = "";
    $ext_page="";
    if (! empty($cat_id)) {
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
   	
		if(!empty($adminid)){
			$where .=" and adminid=$adminid ";
			$ext_page.="adminid=$adminid|";
			$ext.="&adminid={$adminid}";
		}
		
		if($date_begin || $date_end )
		{
			$tmp1 = @explode("/", $date_begin);
			$time_begin = @mktime(0, 0, 0, $tmp1[1], $tmp1[0], $tmp1[2]);
			
			$tmp2 = @explode("/", $date_end);
			$time_end = @mktime(23, 59, 59, $tmp2[1], $tmp2[0], $tmp2[2]);
			
			$where.=" AND (date_post BETWEEN {$time_begin} AND {$time_end} ) ";
			$ext.="&date_begin=".$date_begin."&date_end=".$date_end;
			$ext_page.= "date_begin=".$date_begin."|date_end=".$date_end."|";
		}
		
		if(!empty($keyword)){
			switch($search){
				case "leaguesid" : $where .=" and  n.leaguesid = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
 		
    $query = $DB->query("SELECT n.leaguesid 
												FROM leagues n, leagues_desc nd
												WHERE n.leaguesid=nd.leaguesid
												AND lang='$lang' 
												AND display != -1
												$where");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)      $p = $num_pages;
    if ($p < 1)      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
		$table['link_action'] = $this->linkUrl . "&p=$p";
		$ext_link = $ext."&p=$p" ;
		
		$sortField = ($vnT->input['sortField']) ? $vnT->input['sortField'] : "display_order";
		$sortOrder = ($vnT->input['sortOrder']) ? $vnT->input['sortOrder'] : "desc";
		$OrderBy = " ORDER BY $sortField $sortOrder , date_post DESC ";		 
		$sortLinks = array("display_order","title","cat_id","date_post","display" );
		$data['SortLink'] = $func->BuildSortingLinks($sortLinks,$this->linkUrl.$ext."&p=".$p, $sortField, $sortOrder); 
		
		$ext_link .= "&sortField=$sortField&sortOrder=$sortOrder" ;
		
		$table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|3%|center" , 
      'order' => $vnT->lang['order'] . " {$data['SortLink']['display_order']}|7%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => "Trận đấu" . " {$data['SortLink']['title']}||left" ,       
      'cat_name' => "Giải đấu" . " {$data['SortLink']['cat_id']}|20%|center" , 
			'info' =>$vnT->lang['infomartion']. " {$data['SortLink']['date_post']}|20%|left",
      'action' => "Action {$data['SortLink']['display']}|10%|center");
    $sql = "SELECT * 
						FROM leagues n, leagues_desc nd
						WHERE n.leaguesid=nd.leaguesid
						AND lang='$lang' 
						AND display != -1
						$where 
						$OrderBy LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) 
		{
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
				$row[$i]['ext_link'] = $ext_link ;
				$row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['leaguesid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_leagues'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnHidden" value=" ' . $vnT->lang['hidden'] . ' " class="button" onclick="do_submit(\'do_hidden\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDisplay" value=" ' . $vnT->lang['display'] . ' " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnEdit" value=" ' . $vnT->lang['update'] . ' " class="button" onclick="do_submit(\'do_edit\')">&nbsp;';
		$table['button'] .= '<input type="button" name="btnDuplicate" value=" '.$vnT->lang['duplicate'].' " class="button" onclick="action_selected(\''.$this->linkUrl.'&sub=duplicate&ext='.$ext_page.'\', \''.$vnT->lang["duplicate"].'?\')">';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
		
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['link_fsearch'] = $this->linkUrl;
    $data['list_cat'] = Get_Cat($cat_id, $lang, " ");
		
		$data['date_begin'] = $date_begin;
		$data['date_end'] = $date_end;
    $data['list_search'] = List_Search($search); 
		
    $data['keyword'] = $keyword;
    $data['cat_id'] = $cat_id;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
 
  // end class
}
?>
