<?php
/*================================================================================*\
|| 							Name code : funtions_music.php 		 			      	         		  # ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
/*=======================CHANGE LOG================================================
DATE 14/12/2007 :
	- function getToolbar 	them 2 var mod va act de xac dinh ( la`m bieng edit wa ), 
													khong su dung lang (hktrung)
													
	- define MOD_DIR_UPLOAD		thu muc upload cua module ( hktrung )
==================================================================================*/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
define('MOD_DIR_UPLOAD', '../vnt_upload/leagues/');
define('ROOT_UPLOAD', 'vnt_upload/leagues/'); 
define('MOD_ROOT_URL', $conf['rooturl'] . 'modules/leagues/');

/*-------------- loadSetting --------------------*/
function loadSetting ($lang="vn")
{
  global $vnT, $func, $DB, $conf;
  $setting = array();
  $result = $DB->query("select * from leagues_setting WHERE lang='$lang' ");
  $setting = $DB->fetch_row($result);
  foreach ($setting as $k => $v) {
    $vnT->setting[$k] = stripslashes($v);
  }
  unset($setting);
}
 

//============List_Display
function List_Display($did=0,$ext=""){
global $func,$vnT,$conf;

	$text= "<select size=1 name=\"display\" id=\"display\" {$ext}'>";
	$text.="<option value=\"-1\" > -- Tất cả -- </option>";
	if ($did =="0")
		$text.="<option value=\"0\" selected> ".$vnT->lang['display_no']." </option>";
	else
		$text.="<option value=\"0\" > ".$vnT->lang['display_no']." </option>";
		
	if ($did =="1")	
		$text.="<option value=\"1\" selected> ".$vnT->lang['display_yes']." </option>";
	else
		$text.="<option value=\"1\"> ".$vnT->lang['display_yes']." </option>";
		
	$text.="</select>";
	return $text;
}

//============ List_Permesion_Wiew
function List_Permesion_Wiew ($did = 0, $ext = "")
{
  global $func, $vnT, $conf;
  $text = "<select size=\"1\" name=\"permesion_view\" class=\"select\" {$ext} >";
  foreach ($vnT->arr_leagues['permesion_view'] as $key => $value) {
    if (isset($did) && ($did == $key))
      $text .= '<option value="' . $key . '" selected>' . $value . '</option>';
    else
      $text .= '<option value="' . $key . '">' . $value . '</option>';
  }
  $text .= "</select>";
  return $text;
}

//============ List_Type_Show
function List_Type_Show ($did = 0)
{
  global $func, $vnT, $conf;
  $text = "<select size=\"1\" name=\"type_show\" class=\"select\" {$ext} >";
  foreach ($vnT->arr_leagues['type_show_leagues'] as $key => $value) {
    if (isset($did) && ($did == $key))
      $text .= '<option value="' . $key . '" selected>' . $value . '</option>';
    else
      $text .= '<option value="' . $key . '">' . $value . '</option>';
  }
  $text .= "</select>";
  return $text;
}

// List_Status_Pro 
function List_Ratings_Pro ($selname,$did,$lang="vn",$ext=""){
  global $func,$DB,$conf;
  $text= "<select name=\"{$selname}\"  {$ext}   >";
  $text.="<option value=\"0\" selected>-- Chọn trạng thái --</option>";
  $sql="SELECT n.*, nd.title FROM product_status n, product_status_desc nd 
        where n.status_id=nd.status_id
        AND nd.lang='$lang'
        AND n.display=1 
        order by n.s_order ";
  $result = $DB->query ($sql);
  while ($row = $DB->fetch_row($result)){
    if ($row['status_id']==$did){
      $text .= "<option value=\"{$row['status_id']}\" selected>".$func->HTML($row['title'])."</option>";
    } else{
      $text .= "<option value=\"{$row['status_id']}\">".$func->HTML($row['title'])."</option>";
    }
  }
  $text.="</select>";
  return $text;
}


/*** Ham Get_Cat ****/
function Get_Cat ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} >";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$lang' 
					AND display<>-1
					AND n.parentid=0 
					order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Cat ****/
function Get_Cat2 ($did = -1, $lang, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} required>";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
          WHERE n.cat_id=nd.cat_id
          AND nd.lang='$lang' 
          AND display<>-1
          AND n.parentid=0 
          order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    //$text .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham lấy danh sách đội bóng ****/
function Get_List_Team ($did = -1,$stt,$lang="vn")
{
  global $func, $DB, $conf;
  $text .= "<option value=\"\">-- Chọn đội bóng $stt --</option>";
  $query = $DB->query("SELECT n.*, nd.p_name FROM teams n,teams_desc nd 
          WHERE n.p_id=nd.p_id
          AND nd.lang='$lang' 
          AND display<>-1
          order by p_order DESC ,n.p_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['p_name']);
    if ($cat['p_id'] == $did)
      $text .= "<option value=\"{$cat['p_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['p_id']}\" >{$cat_name}</option>";
    
  }
  
  return $text;
}


/*** Ham lấy danh sách cầu thủ theo id đội bóng ****/
function Get_List_Player ($id_doibong = -1,$stt,$lang="vn",$name_player="")
{

  global $func, $DB, $conf;
  $text .= "<option value=\"\">-- Chọn cầu thủ đội $stt --</option>";
  $query = $DB->query("SELECT n.*, nd.p_name FROM teams_player n,teams_player_desc nd 
          WHERE n.player_id=nd.player_id
          AND nd.lang='$lang' 
          AND display<>-1
          AND p_id in (".$id_doibong.")
          order by p_order DESC , n.player_id DESC");
  while ($cat = $DB->fetch_row($query)) {
      $cat_name = $func->HTML($cat['p_name']);
      if ($cat['p_name'] == $name_player)
        $text .= "<option value=\"{$cat_name}\" selected>{$cat_name}</option>";
      else
        $text .= "<option value=\"{$cat_name}\" >{$cat_name}</option>";
      
    }
  
  return $text;
}

/*** Ham lấy tên đội bóng dựa vào ID đội bóng ****/
function get_team_name ($p_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("SELECT n.*, nd.p_name FROM teams n,teams_desc nd 
          WHERE n.p_id=nd.p_id
          AND nd.lang='$lang'
          AND n.p_id=$p_id 
          ");
 
  $array_value = array();
  if ($row = $DB->fetch_row($result)) {
    $array_value['name'] = $func->HTML($row['p_name']);
    if(! empty($row['picture'])){
      $array_value['pic'] = get_pic_teams($row['picture'],40,$func->HTML($row['p_name']));      
    }
    
  }

  return $array_value;
}

/*** Ham Get_Sub   */
function Get_Sub ($did, $cid, $n, $lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$lang' 
					AND display<>-1 
					AND n.parentid={$cid} 
					order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub($did, $cat['cat_id'], $n, $lang);
  }
  return $output;
}

/*** Ham Get_Category ****/
function Get_Category ($selname, $did = -1, $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"{$selname}\" name=\"{$selname}\" {$ext} class='select'>";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$lang' 
					AND display<>-1 
					AND n.parentid=0 
					order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";
    $n = 1;
    $text .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  $text .= "</select>";
  return $text;
}

/*** Ham Get_Sub   */
function Get_Sub_Category ($selname, $did, $cid, $n,$lang)
{
  global $func, $DB, $conf;
  $output = "";
  $k = $n;
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$lang' 
					AND display<>-1 
					AND n.parentid={$cid}
					order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did) {
      $output .= "<option value=\"{$cat['cat_id']}\" selected>";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    } else {
      $output .= "<option value=\"{$cat['cat_id']}\" >";
      for ($i = 0; $i < $k; $i ++)
        $output .= "|-- ";
      $output .= "{$cat_name}</option>";
    }
    $n = $k + 1;
    $output .= Get_Sub_Category($selname, $did, $cat['cat_id'], $n,$lang);
  }
  return $output;
}


/***** Ham List_SubCat *****/
function List_SubCat ($cat_id)
{
  global $func, $DB, $conf;
  $output = "";
  $query = $DB->query("SELECT * FROM leagues_category WHERE parentid={$cat_id}");
  while ($cat = $DB->fetch_row($query)) {
    $output .= $cat["cat_id"] . ",";
    $output .= List_SubCat($cat['cat_id']);
  }
  return $output;
}

//---------------- get_cat_name
function get_cat_name ($cat_id, $lang)
{
  global $vnT, $func, $DB, $conf;
  $result = $DB->query("select cat_name from leagues_category_desc  where cat_id=$cat_id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $cat_name = $func->HTML($row['cat_name']);
  }
  return $cat_name;
}
//---------------- get_friendly_url
function get_friendly_url ($cat_id, $lang)
{
  global $vnT, $func, $DB, $conf;
	$out="";
  $result = $DB->query("select friendly_url from leagues_category_desc  where cat_id=$cat_id and lang='$lang'");
  if ($row = $DB->fetch_row($result)) {
    $out = $func->HTML($row['friendly_url']);
  }
  return $out;
}



//-----------------  get_catCode
function get_catCode ($parentid, $cat_id)
{
  global $vnT, $func, $DB, $conf;
  $text = "";
  $sql = "select cat_id,cat_code from leagues_category where cat_id =$parentid ";
  $result = $DB->query($sql);
  if ($row = $DB->fetch_row($result)) {
    $text = $row['cat_code'] . "_" . $cat_id;
  } else
    $text = $cat_id;
  return $text;
}

/*-------------- get_cat_root --------------------*/
function get_cat_root ($cat_id)
{
  global $DB, $conf, $func, $vnT, $input;
  $text = $cat_id;
  $res = $DB->query("SELECT cat_code,cat_id FROM leagues_category where cat_id=" . (int) $cat_id);
  if ($cat = $DB->fetch_row($res))
  {
    $cat_code = $cat['cat_code'];
    $tmp = explode("_", $cat_code);
    $text = $tmp[0];
  }
  return $text;
}



/***** Ham get_cat_list *****/
function get_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT cat_id,cat_code FROM leagues_category where cat_id in (".$cat_id.") ");
	while ($row=$DB->fetch_row($res)) 
	{
		$tmp = @explode("_",$row['cat_code']);
		$arr_cat = array_merge($arr_cat,$tmp) ;
	}
	$arr_cat = array_unique($arr_cat);
	$out = @implode(",",$arr_cat);
	
	return $out;	
}

/***** Ham rebuild_cat_list *****/
function rebuild_cat_list ($cat_id){
	global $vnT,$func,$DB,$conf;
	$arr_cat = array();
	$res = $DB->query("SELECT leaguesid,cat_id FROM leagues WHERE  FIND_IN_SET('$cat_id',cat_list)<>0 ");
	while($row = $DB->fetch_row($res))
	{
		$DB->query("UPDATE leagues SET cat_list='".get_cat_list($row['cat_id'])."' WHERE leaguesid=".$row['leaguesid'])	;
	}	
	return false;	
}

/***** Ham rebuild_cat_code *****/
function rebuild_cat_code ($parentid,$cat_id){
	global $vnT,$func,$DB,$conf;
	
	$cat_code = get_catCode($parentid,$cat_id) ;
	$DB->query("UPDATE leagues_category SET cat_code='".$cat_code."' WHERE cat_id=".$cat_id)	;
 	
	$res_chid = $DB->query("SELECT cat_id FROM leagues_category WHERE  parentid=".$cat_id);
	while($row_chid = $DB->fetch_row($res_chid))
	{		
		rebuild_cat_code($cat_id,$row_chid['cat_id']);
	} 
	
	return false;	
}


/*** Ham List_Cat_Root ****/
function List_Cat_Root ($did , $lang = "vn", $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 id=\"cat_id\" name=\"cat_id\" {$ext} class='select'>";
  $text .= "<option value=\"\">-- Root --</option>";
  $query = $DB->query("SELECT n.*, nd.cat_name FROM leagues_category n,leagues_category_desc nd 
					WHERE n.cat_id=nd.cat_id
					AND nd.lang='$lang' 
					AND n.parentid=0 
					order by cat_order DESC ,n.cat_id DESC");
  while ($cat = $DB->fetch_row($query)) {
    $cat_name = $func->HTML($cat['cat_name']);
    if ($cat['cat_id'] == $did)
      $text .= "<option value=\"{$cat['cat_id']}\" selected>{$cat_name}</option>";
    else
      $text .= "<option value=\"{$cat['cat_id']}\" >{$cat_name}</option>";     
  }
  $text .= "</select>";
  return $text;
}

//-----------------  get_pic_thumb
function get_pic_thumb ($picture, $w = "")
{
  global $conf, $vnT;
  $out = "";
  if (! empty($picture)) {
    $out = "";
    $ext = "";
    $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
    $linkhinh = "../vnt_upload/leagues/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    $src = $dir . "/thumbs/" . $pic_name;
    if ($w < $w_thumb)
      $ext = " width='$w' ";
    $out = "<img  src=\"{$src}\" {$ext} >";
  } else {
    $out = "No Picture";
  }
  return $out;
}

//-----------------  get_pic_leagues
function get_pic_leagues ($picture, $w = 150)
{
  global $vnT, $func, $DB, $conf;
  $out = "";
  $ext = "";
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  if ($picture) {
    $linkhinh = "../vnt_upload/leagues/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    $src = $dir . "/thumbs/" . $pic_name; 
  }
  if ($w < $w_thumb) $ext = " width='$w' ";
  $out = "<img  src=\"{$src}\" {$ext} >";
  return $out;
}

//-----------------  get_pic_leagues
function get_pic_teams ($picture, $w = 150,$title="")
{
  global $vnT, $func, $DB, $conf;
  $out = "";
  $ext = "";
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  if ($picture) {
    $linkhinh = "../vnt_upload/team/" . $picture;
    $linkhinh = str_replace("//", "/", $linkhinh);
    $dir = substr($linkhinh, 0, strrpos($linkhinh, "/"));
    $pic_name = substr($linkhinh, strrpos($linkhinh, "/") + 1);
    $src = $dir . "/thumbs/" . $pic_name; 
  }
  if ($w < $w_thumb) $ext = " width='$w' ";
  $out = "<img title=\"{$title}\"  src=\"{$src}\" {$ext} >";
  return $out;
}

/*** Ham List_Keyref ****/
function List_Keyref ($did = -1, $ext = "")
{
  global $func, $DB, $conf,$vnT;
  $text = "<select size=1 id=\"keyref_id\" name=\"keyref_id\" {$ext} >";
  $text .= "<option value=\"0\">-- ".$vnT->lang['select_keyref']." --</option>";
  $query = $DB->query("SELECT * FROM leagues_key_referent WHERE display=1  order by keyref_order");
  while ($cat = $DB->fetch_row($query)) {
    $title = $func->HTML($cat['title']);
    if ($cat['keyref_id'] == $did)
      $text .= "<option value=\"{$cat['keyref_id']}\" selected>{$title}</option>";
    else
      $text .= "<option value=\"{$cat['keyref_id']}\" >{$title}</option>";
  }
  $text .= "</select>";
  return $text;
}

 /*------ get_num_referent ---*/
function get_num_referent ($id)
{
  global $func, $DB, $conf;
  $result = $DB->query("SELECT leaguesid FROM leagues WHERE keyref_id=$id  ");
  $num = $DB->num_rows($result);
  return intval($num);
}
 

/*** Ham List_Source ****/
function List_Source ($did = -1,$lang="vn", $ext = "")
{
  global $func, $DB, $conf, $vnT;
  $text = "<select size=1 id=\"source\" name=\"source\" {$ext} >";
  $text .= "<option value=\"\">-- ".$vnT->lang['select_source']." --</option>";
  $sql = "SELECT * FROM leagues_source WHERE display=1 AND lang='$lang'  order by s_order";
  //echo $sql;
  $query = $DB->query($sql);
  while ($cat = $DB->fetch_row($query)) {
    $title = $cat['s_title'];
    if ($cat['sid'] == $did)
      $text .= "<option value=\"{$cat['sid']}\" selected>{$title}</option>";
    else
      $text .= "<option value=\"{$cat['sid']}\" >{$title}</option>";
  }
  $text .= "</select>";
  return $text;
}

/*** Ham List_Site_Bot ****/
function List_Site_Bot ($did = -1, $ext = "")
{
  global $func, $DB, $conf;
  $text = "<select size=1 name=\"site\" id=\"site\" {$ext}  >";
  if ($did == "vnexpress")
    $text .= "<option value=\"vnexpress\" selected> vnexpress.net  </option>";
  else
    $text .= "<option value=\"vnexpress\"> vnexpress.net </option>";
  $text .= "</select>";
  return $text;
}

///================== replace_img =============
function replace_img ($str, $dir)
{
  global $conf, $func, $vnT;
  $data['result'] = $tmp = $str;
  $data['url'] = "";
  $ok_getimg = 1;
  $w_thumb = ($vnT->setting['imgthumb_width']) ? $vnT->setting['imgthumb_width'] : 100;
  if ($dir) {
    $path_dir = MOD_DIR_UPLOAD . $dir . "/";
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD . $dir . "/";
  } else {
    $path_dir = MOD_DIR_UPLOAD;
    $rooturl = str_replace('http://' . $_SERVER['HTTP_HOST'], "", $conf['rooturl']) . ROOT_UPLOAD;
  }
  $path_thumb = $path_dir . "thumbs";
  if (! is_dir($path_thumb)) {
    @mkdir($path_thumb, 0777);
    @exec("chmod 777 {$path_thumb}");
  }
  while ($start = strpos($tmp, "src=")) {
    $end = strpos($tmp, '"', $start + 7);
    $http = substr($tmp, $start + 6, ($end - ($start + 7)));
    if ( !strstr($http, $conf['rooturl']) && (strstr($http, "http://") || strstr($http, "https://")) ) {
      // upload
      $fext = strtolower(substr($http, strrpos($http, ".") + 1));
      if (($fext == "jpg") || ($fext == "gif") || ($fext == "png") || ($fext == "bmp")) {
        $lastx = strrpos($http, "/");
        $fname = $path_dir . substr($http, $lastx + 1);
        $fname = str_replace("%20", "_", $fname);
        $fname = str_replace(" ", "_", $fname);
        $fname = str_replace("&amp;", "_", $fname);
        $fname = str_replace("&", "_", $fname);
        $fname = str_replace(";", "_", $fname);
        if (file_exists($fname)) {
          $fname = $path_dir . time() . "_" . substr($http, $lastx + 1);
        }
        $file_name = substr($fname, strrpos($fname, "/") + 1);
        $file = @fopen($fname, "w");
        if ($f = @fopen($http, "r")) {
          while (! @feof($f)) {
            @fwrite($file, fread($f, 1024));
          }
          @fclose($f);
          @fclose($file);
          $url = $rooturl . $file_name; 
          /*if ($ok_getimg ==1){
            $data['url'] = $dir . "/" . $file_name;
            $file_thumb = $path_thumb . "/" . $file_name;         
            $func->thum( 'http://' . $_SERVER['HTTP_HOST'].$url, $file_thumb, $w_thumb);
            $ok_getimg = 0;
          }*/
          $data['result'] = str_replace($http, $url, $data['result']);
        } else
          $data['err'] = "Cannot Read from this Image ! Plz save to your Computer and Upload It";
      } else
        $data['err'] = "Image Type Not Support";
    } //end if strstr
    $tmp = substr($tmp, $end + 1);
  } // end while
  return $data;
}

//======================= List_Search =======================
function List_Search ($did,$ext="")
{
  global $func, $DB, $conf, $vnT;
	$arr_where = array('leaguesid'=> 'leagues ID','title'=>  $vnT->lang['title_leagues'],'short'=> $vnT->lang['short_leagues'], 'date_post' => $vnT->lang['date_post']." (d/m/Y)" );
	
	$text= "<select size=1 name=\"search\" id='search' class='select' {$ext} >";
	foreach ($arr_where as $key => $value)
	{
		$selected = ($did==$key) ? "selected"	: "";
		$text.="<option value=\"{$key}\" {$selected} > {$value} </option>";
	}	
	$text.="</select>";	
	 
  return $text;
}
//------list_admin
function list_admin ($did,$ext=""){
	global $func,$DB,$conf,$vnT;

	$text= "<select name=\"adminid\" id=\"adminid\"   class='select' {$ext} >";
	$text .= "<option value=\"\" selected>-- Chọn Admin --</option>";
	$sql="SELECT adminid,username	FROM admin order by  username ASC ";
	
	$result = $DB->query ($sql);
	$i=0;
	while ($row = $DB->fetch_row($result))
	{
		$i++;
		$username = $func->HTML($row['username']);
		$selected = ($did==$row['adminid']) ? " selected " : "";
		$text .= "<option value=\"{$row['adminid']}\" ".$selected." >  ".$username."  </option>";
	}
	
	$text.="</select>";
	return $text;
}

//------get_admin
function get_admin($adminid) {
global $vnT,$func,$DB,$conf;
	$text = "Admin";
	$sql ="select username from admin where adminid =$adminid  ";
	$result =$DB->query ($sql);
	if ($row = $DB->fetch_row($result)){
		$text = $func->HTML($row['username']);
	}
	return $text;
}

//------do_DeleteSub
function do_DeleteSub($ids,$lang="vn")
{
	global $func, $DB, $conf, $vnT;
	$res = $DB->query("SELECT cat_id,parentid FROM leagues_category WHERE parentid in (".$ids.")");
	while($row = $DB->fetch_row($res))
	{
		do_DeleteSub($row['cat_id'],$lang);
    $DB->query("UPDATE leagues_category_desc SET display=-1 WHERE  cat_id = ".$row['cat_id']." AND lang='".$lang."' ");

    //insert RecycleBin
    $rb_log['module'] = "leagues";
    $rb_log['action'] = "category";
    $rb_log['tbl_data'] = "leagues_category_desc";
    $rb_log['name_id'] = "cat_id";
    $rb_log['item_id'] = $row['cat_id'];
    $rb_log['lang'] = $lang;
    $func->insertRecycleBin($rb_log);

	}
	
}

?>