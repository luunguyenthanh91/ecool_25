<?php
/*================================================================================*\
|| 							Name code : news.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "news";
  var $action = "active_news";
  var $setting = array();

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->setting = loadSetting();
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad/" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addScript("modules/" . $this->module . "_ad" . "/js/" . $this->module . "_ad" . ".js");
    $vnT->html->addStyleSheet($vnT->dir_js . "/auto_suggest/autosuggest.css");
    $vnT->html->addScript($vnT->dir_js . "/auto_suggest/bsn.AutoSuggest.js");
    switch ($vnT->input['sub']) {
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = "Kích hoat tin tức";
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Del 
   * Xoa 1 ... n  gioi thieu 
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    // Del Image
    $query = $DB->query("SELECT picture FROM news WHERE newsid IN (" . $ids . ") ");
    while ($img = $DB->fetch_row($query)) {
      $src_name = substr($img['picture'], strrpos($img['picture'], "/") + 1);
      $dir = substr($img['picture'], 0, strrpos($img['picture'], "/"));
      $path_file = MOD_DIR_UPLOAD . $img['picture'];
      $path_filethumbs = MOD_DIR_UPLOAD . $dir . "/thumbs/" . $src_name;
      if (file_exists($path_file) && $img['picture'])
        @unlink($path_file);
      if (file_exists($path_filethumbs))
        @unlink($path_filethumbs);
    }
    // End del image
    $query = 'DELETE FROM news WHERE newsid IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $DB->query('DELETE FROM news_desc WHERE newsid IN (' . $ids . ')');
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['newsid'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    if (empty($row['picture']))
      $picture = "<i>No Image</i>";
    else {
      $url_view = "zoom.php?image=" . MOD_DIR_UPLOAD . "{$row['picture']}&title=Zoom";
      $picture = "<a href=\"#Zoom\" onClick=\"openPopUp('{$url_view}', 'Zoom', 700, 600, 'yes')\">" . get_pic_thumb($row['picture'], 50) . "</a>";
    }
    $output['picture'] = $picture;
    $text_edit = "news|title|newsid=" . $id;
    $output['title'] = "<strong><span id='edit-text-" . $id . "' onClick=\"quick_edit('edit-text-" . $id . "','$text_edit');\">" . $func->HTML($row['title']) . "</span></strong>";
    $output['title'] .= "<br>Post by : <b class=font_err>" . $row['poster'] . "</b>";
    $output['date_post'] = date("d/m/Y", $row['date_post']);
    $output['p_name'] = get_p_name($row['p_id'], $lang);
    if ($row['display'] == 1) {
      $display = "<img src=\"" . $vnT->dir_images . "/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"" . $vnT->dir_images . "/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/edit.gif"  alt="Edit "></a>&nbsp;';
    $output['action'] .= $display . '&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly cac gioi thieu
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])   $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit": 
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("news_desc", $dup, "newsid={$h_id[$i]} AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("news_desc", $dup, "newsid={$h_id[$i]}  AND lang='$lang' ");
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      } 
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $cat_id = ((int) $vnT->input['cat_id']) ? $cat_id = $vnT->input['cat_id'] : 0;
    $search = ($vnT->input['search']) ? $search = $vnT->input['search'] : "newsid";
    $keyword = ($vnT->input['keyword']) ? $keyword = $vnT->input['keyword'] : "";
    $where = " AND display=0 ";
    $ext = "";
    if (! empty($cat_id)) {
      $subcat = List_SubCat($cat_id);
      if (! empty($subcat)) {
        $subcat = substr($subcat, 0, - 1);
        $a_cat_id = $cat_id . "," . $subcat;
        $a_cat_id = str_replace(",", "','", $a_cat_id);
        $where .= " and cat_id in ('" . $a_cat_id . "') ";
      } else {
        $where .= " and cat_id = $cat_id ";
      }
      $ext_page .= "cat_id=$cat_id|";
      $ext = "&cat_id=$cat_id";
    }
    
    if(!empty($keyword)){
			switch($search){
				case "newsid" : $where .=" and  n.newsid = $keyword ";   break;
				case "date_post" : $where .=" and DATE_FORMAT(FROM_UNIXTIME(date_post),'%d/%m/%Y') = '{$keyword}' "; break;
				default :$where .=" and $search like '%$keyword%' ";break;		
			}
			
			$ext_page.="keyword=$keyword|keyword=$keyword|";
			$ext.="&search={$search}&keyword={$keyword}";
		}
		
    $query = $DB->query("SELECT n.newsid 
												FROM news n, news_desc nd
												WHERE n.newsid=nd.newsid
												AND lang='$lang' 
												$where");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "{$ext}&p=$p";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'picture' => $vnT->lang['picture'] . "|10%|center" , 
      'title' => $vnT->lang['title'] . "|50%|left" , 
      'date_post' => $vnT->lang['date_post'] . " |10%|center" , 
      'p_name' => "Văn phòng|15%|center" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * 
						FROM news n, news_desc nd
						WHERE n.newsid=nd.newsid
						AND lang='$lang' 
						$where 
						ORDER BY date_post DESC, n.newsid DESC LIMIT $start,$n";
    // print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['newsid'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_news'] . "</div>";
    }
    $table['button'] .= '<input type="button" name="btnDisplay" value=" Kích họat tin đã chọn " class="button" onclick="do_submit(\'do_display\')">&nbsp;';
    $table['button'] .= '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['link_fsearch'] = $this->linkUrl;
    $data['list_cat'] = Get_Cat($cat_id, $lang, " ");
    $data['p_id'] = $p_id;
    $data['p_name'] = get_p_name($p_id, $lang);
    $data['list_search'] = List_Search($search);
    $data['keyword'] = $keyword;
    $data['cat_id'] = $cat_id;
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>
