
<!-- BEGIN: edit -->
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
  <tr>
    <td valign=top width=50% class="row0" >
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="40%" class="row1"><strong>{LANG.full_name} </strong>:</td>
      <td class="row0">{data.name}</td>
    </tr>  
    <tr>
      <td  class="row1"><strong>{LANG.address} </strong>:</td>
      <td class="row0">{data.address}</td>
    </tr> 
    <tr>
      <td  class="row1"><strong>Email</strong>:</td>
      <td class="row0">{data.email}</td>
    </tr> 
  </table>
   </td>
    <td valign=top  class="row0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="40%" class="row1"><strong>{LANG.company} </strong>:</td>
      <td class="row0">{data.company}</td>
    </tr>  
    <tr>
      <td  class="row1"><strong>{LANG.phone} </strong>:</td>
      <td class="row0">{data.phone}</td>
    </tr> 
    <tr>
      <td  class="row1"><strong>Fax</strong>:</td>
      <td class="row0">{data.fax}</td>
    </tr> 
  </table></td>
  </tr>
	
</table>
<br />
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
	<tr>
		<td class="row1" width="20%"><strong>{LANG.send_department} </strong>: </td>
		<td class="row0">{data.department}</td>
	</tr>

	<tr>
  	<td class="row1" width="20%"><strong>{LANG.content_contact} </strong>: </td>
		<td class="row0"><p align="justify"  >{data.content}</p></td>

	</tr>
</table>
<br />
 

<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="myform">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="20%" align="left">{LANG.totals}: &nbsp;</td>
    <td  align="left"><b class="font_err">{data.totals}</b></td>
  </tr>

  <tr>
 </table>
 <table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_unread.png" align="absmiddle" />&nbsp; {LANG.email_unread} &nbsp;&nbsp;&nbsp;</td>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_read.png" align="absmiddle" />&nbsp; {LANG.email_read}&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>

		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_reply.png" align="absmiddle" />&nbsp; {LANG.email_reply}&nbsp;&nbsp;&nbsp;</td>
		<td><img src="{DIR_IMAGE_MOD}/vnt_mail_forward.png" align="absmiddle" />&nbsp;&nbsp;{LANG.email_forward}&nbsp;&nbsp;</td>
	</tr>
</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->