<!-- BEGIN: edit -->
<style >
table.table_form {
	background-color:#E7E7E7;
	border-spacing:1px;
	color:#666666;
	width:100%;
}
.table_form td  {
	border:1px solid #FFFFFF;
	height:25px;
	padding:3px;
}

.table_form .col1  {
	background:#FAFAFA;
	font-weight:bold;
	background:#F9F9F9;
	border-top:1px solid #FFFFFF;
}
.table_form .col1 span {
	font-weight:normal;
}

.table_form .col2  {
	background:#FFFFFF;
	border-top:1px solid #FFFFFF;
}
</style>
<br />
<p align="center"><strong class="font_err">{data.subject} </strong></p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  
  <tr>
    <td><table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td ><fieldset>
          <legend class="font_err">&nbsp;<strong>THÔNG TIN KHÁCH HÀNG</strong>&nbsp;</legend>
           <table width="100%" border="0" cellspacing="3" cellpadding="3">
      <tr>
        <td>Họ và tên: <strong>{data.name}</strong> </td>
      </tr>

      <tr>
        <td>Công ty : <strong>{data.cmnd}</strong></td>
      </tr>
      <tr>
        <td> Điện thoại liên lạc : <strong>{data.phone}</strong></td>
      </tr>
      <tr>
        <td>Email : <strong>{data.email}</strong></td>
      </tr>
      <tr>
        <td>Địa chỉ : <strong>{data.address}</strong></td>
      </tr>
       
       
      
    </table>
          </fieldset></td>
        </tr>
      </table>
      
      
    <br>
    
    <table width="95%" border="0" cellspacing="2" cellpadding="2" align="center">
      <tr>
        <td ><fieldset>
          <legend class="font_err">&nbsp;<strong>THÔNG TIN ĐĂNG KÝ</strong>&nbsp;</legend>
           <table width="100%" border="0" cellspacing="3" cellpadding="3">
        <tr>
          <td>Đăng ký về : <strong>{data.type_name}</strong> </td>
        </tr>
        
				{data.info_order}
      	<tr>
        <td>Ngày đăng ký : <strong>{data.date_post}</strong>
        </td>
      </tr>
         
        
      
    </table>
          </fieldset></td>
        </tr>
      </table>
      
    </td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td style="border-bottom:2px solid #FF0000" height="30"><strong class="font_err">Modify </strong></td>
  </tr>
  <tr>
    <td><form action="{data.link_action}" method="post">
       <table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">
        <tr>
          <td width="30%"  colspan="2" align="center"><strong><font color="#FF0000">{data.msg}</font></strong> </td>
        </tr>
        <tr>
          <td width="30%" align="right" class="row1"><strong>Ngày cập nhật  :</strong> </td>
          <td width="70%" align="left" class="row0"><input type="text" name="date_update" id="date_update" value="{data.date_update}" maxlength="10">
            &nbsp; <font color="#FF0000"> (dd/mm/Y)</font></td>
        </tr>
        <tr >
          <td align="right" class="row1"><strong>Trạng thái : </strong></td>
          <td align="left" class="row0">{data.list_status}</td>
        </tr>

        <tr>
        	<td align="right" class="row1">&nbsp;</td>
          <td  class="row0"><input type="submit" name="btnSave" value=" Save " class="button">
          </td>
        </tr>
      </table>
    </form></td>
  </tr>
</table>
<br>
<br>
<!-- END: edit -->

<!-- BEGIN: manage -->
<form action="{data.link_fsearch}" method="post" name="fSearch">
<table width="100%" border="0" cellspacing="2" cellpadding="2" align="center" class="tableborder">
  <tr>
    <td width="15%"><strong>{LANG.totals}:</strong> &nbsp;</td>
    <td width="85%" ><b class="font_err">{data.totals}</b></td>
  </tr>
  <tr>
    <td ><strong>Trạng thái:</strong> &nbsp;</td>
    <td ><b class="font_err">{data.list_status}</b></td>
  </tr>
  <tr>
    <td  ><strong>Khách hàng:</strong> &nbsp;</td>
    <td  >   <input name="keyword" value="{data.keyword}" size="20" type="text" />  &nbsp;<input type="submit" name="btnGo" value=" Search " class="button"></td>
  </tr>

</table>
</form>
{data.err}
<br />
{data.table_list}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="25">{data.nav}</td>
  </tr>
</table>
<br />
<!-- END: manage -->