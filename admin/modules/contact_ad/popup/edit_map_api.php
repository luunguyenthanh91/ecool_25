<?php
define('IN_vnT', 1);
define('DS', DIRECTORY_SEPARATOR);
require_once ("../../../../_config.php");
include ($conf['rootpath'] . "includes/class_db.php");
$DB = new DB();

//conf
$result = $DB->query("SELECT array FROM config ");
while ($row = $DB->fetch_row($result))
{
	if ($row['array'] != "")
	{
		$base64Encoded = unserialize($row['array']);
		foreach ($base64Encoded as $key => $value)
		{
			$conf[base64_decode($key)] = stripslashes(base64_decode($value));
		}
	}
}
 
 
 
//maps
$id = (int) $_GET['id'];
$query = $DB->query("SELECT * FROM contact_config WHERE id=$id");
if ($row_m = $DB->fetch_row($query)) {
  $data['map_information'] = str_replace("\r\n", "<br>", $row_m['map_desc']);
  $data['map_lat'] = ($row_m['map_lat']) ? $row_m['map_lat'] : "10.804866895605";
  $data['map_lng'] = ($row_m['map_lng']) ? $row_m['map_lng'] : "106.64199984239";
  $data['set_map'] = " placeMarker(defaultLatLng, 1);\n	old_marker = clickmarker;";
} else {
  $data['map_lat'] = "10.804866895605";
  $data['map_lng'] = "106.64199984239";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Google Maps</title>
 <style type="text/css">    
 body {
  font-family: Arial, sans serif;
  font-size: 12px;
}
#placemark_b {
  width:31px;
  height:31px;
  background-image: url(http://google.com/mapfiles/ms/t/Bmu.png);
	display:block;
}
#placemark_b.selected {
  background-image: url(http://google.com/mapfiles/ms/t/Bmd.png);
}
</style>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo $conf['GoogleMapsAPIKey'] ?>" type="text/javascript"></script>
<script type="text/javascript">

    var map = null;
    var geocoder = null;
		var defaultLatLng = new GLatLng(<?php echo $data['map_lat']; ?>,<?php echo $data['map_lng']; ?>);
		var infowindow;
		var clickmarker;
		var old_marker;
		var update 			= 0;
		var marker_placed = false;
		var contentString = '';
		var information	= '<?php  echo $data['map_information'];  ?>';
		 

		function initialize() {
			if (GBrowserIsCompatible()) {
				map = new GMap2(document.getElementById("map_canvas"));
				map.setCenter(defaultLatLng, 16); 
				map.setUIToDefault();
				geocoder = new GClientGeocoder();
				  
				var marker = new GMarker(defaultLatLng, {draggable: true});
				placeMarker(marker, 0);
				GEvent.addListener(marker, "dragend", function() {
          placeMarker(marker, 0);
        });
				GEvent.addListener(marker, "click", function() {
						placeMarker(marker, 0);
				});
        map.addOverlay(marker);
			}
		} 
		
		
    function showAddress(address) {
      map.clearOverlays();
			if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
              map.setCenter(point, 16);
              var marker = new GMarker(point, {draggable: true});
              map.addOverlay(marker);
              GEvent.addListener(marker, "dragend", function() {
								placeMarker(marker, 0);
              });
              GEvent.addListener(marker, "click", function() {

								placeMarker(marker, 0);
							
              });
	      			GEvent.trigger(marker, "click");
            }
          }
        );
      }
    } 
 		
 
		function setMarker() {
			document.getElementById("placemark_b").className="selected" ;
			var listener = GEvent.addListener(map, "click", function(overlay, latlng) {
				if (latlng) {
					document.getElementById("placemark_b").className="unselected" ;
					map.clearOverlays();
					GEvent.removeListener(listener);
				 
					var marker = new GMarker(latlng, {draggable: true});
					map.addOverlay(marker);
					
					GEvent.addListener(marker, "dragend", function() {
						placeMarker(marker, 0);
					});
					GEvent.addListener(marker, "click", function() {
						placeMarker(marker, 0);					
					}); 
					GEvent.trigger(marker, "click");
				}
			});
		}
		
	 

		
		function placeMarker(marker, upd){
				information = information.replace("<br>", "\r\n") ;
				contentString = '<div style="margin:5px"><div align="left" style="font-size:11px; font-weight:bold; color:#30A602">Thông tin vị trí </div><div style="margin:5px 0px 5px 0px"><textarea class="form_control" id="gmap_information" name="gmap_information" style="width:300px; height:70px">' + information + '</textarea><div align="left" style="color:#787878; font-size:10px">Tối đa 200 ký tự.</div><div align="center" style="color:#FF0000; font-size:11px">Bạn phải ấn nút <b>"Cập nhật"</b> vị trí trên bản đồ mới được lưu</div></div><div><input type="button" class="form_button" value="Cập nhật" style="width:75px; font-size:11px" onclick="set_position_map()" />&nbsp;<input type="button" class="form_button" value="Xóa" style="width:75px; font-size:11px" onclick="clear_position_map()" /></div></div>';				
				
				update		= upd;
				marker_placed = true;
				clickmarker = marker; 
				
				arrLatLng	= Array(marker.getLatLng().lat(), marker.getLatLng().lng());				
				infowindow = marker.openInfoWindowHtml(contentString);
			}
			
 			
			var win = window.dialogArguments || opener || parent || top;
				
			function set_position_map(){
				
				info = document.getElementById("gmap_information");
				
				ob1 = document.getElementById("map_lat");
				ob2 = document.getElementById("map_lng");
				ob3 = document.getElementById("map_information");
				
				ob1.value	= arrLatLng[0];
				ob2.value	= arrLatLng[1];
				ob3.value	= info.value;

				window.parent.document.getElementById("map_lat").value=arrLatLng[0];
				window.parent.document.getElementById("map_lng").value=arrLatLng[1];
				window.parent.document.getElementById("map_information").value=info.value;
				
				information	= info.value;
				map.closeInfoWindow();
				
				if(old_marker != undefined && update == 0) GEvent.addListener(old_marker, "visiblitychanged", false); 
				old_marker	= clickmarker;
				 
				
			}
			
			
			function clear_position_map(){
				ob1 = document.getElementById("map_lat");
				ob2 = document.getElementById("map_lng");
				ob3 = document.getElementById("map_information"); 
				
				information = '<?php  echo $data['map_information']; ?>';
				ob1.value	= <?php  echo $data['map_lat']; ?>;
				ob2.value	= <?php  echo $data['map_lng']; ?>;
				ob3.value	= '<?php  echo $data['map_information']; ?>';
				
				window.parent.document.getElementById("map_lat").value=<?php  echo $data['map_lat']; ?>;
				window.parent.document.getElementById("map_lng").value=<?php  echo $data['map_lng']; ?>;
				window.parent.document.getElementById("map_information").value= information;
				
				map.clearOverlays();
				var marker = new GMarker(defaultLatLng, {draggable: true});
				map.addOverlay(marker);
				
				placeMarker(marker, 0);
				GEvent.addListener(marker, "dragend", function() {
          placeMarker(marker, 0);
        });
				GEvent.addListener(marker, "click", function() {
						placeMarker(marker, 0);
				});        
				
				
			}
			
    </script>
</head>

<body onload="initialize()" onunload="GUnload()">
<form action="#" onsubmit="showAddress(this.address.value); return false">
 

<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td>Nhập địa chỉ : <input type="text" style="width:350px" name="address" value="" />
        <input type="submit" value="Go!" /></td>
    <td><table   border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Hoặc chọn</td>
    <td style="padding:0px 3px;"><a id="placemark_b" href="javascript:void(0);" onclick="setMarker()" style="text-decoration:none;" >&nbsp;</a></td>
    <td>rồi Click vào bản đồ</td>
  </tr>
</table>
  </td>
  </tr>
</table>

       
    <div id="map_canvas" style="width: 780px; height: 450px"></div>      
    
    <input type="hidden" id="map_lat" name="map_lat" value=""> 
    <input type="hidden" id="map_lng" name="map_lng" value="" />
    <input type="hidden" id="map_information" name="map_information" value="" />
  </form>

  </body>
</html>
