<?php
/*================================================================================*\
|| 							Name code : contact.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "contact";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_contact.php");
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=contact&act=config&lang=" . $lang;
    $nd['f_title'] = $vnT->lang['config_contact'];
    $nd['content'] = $this->do_Manage($lang);
    $nd['row_lang'] = $func->html_lang("?mod=contact&act=config&lang", $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  //=================Functions===============
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    if ($vnT->input['btnSubmit']) {
      $cot = array(
        'description' => $DB->mySQLSafe($_POST['description']) , 
        'map_desc' => $func->txt_HTML($_POST['map_information']) , 
        'map_lat' => $vnT->input['map_lat'] , 
        'map_lng' => $vnT->input['map_lng']);
      //check 
      $res_ck = $DB->query("select id from contact_config where lang='$lang' ");
      if (! $DB->num_rows($res_ck)) {
        $cot['lang'] = $lang;
        $ok = $DB->do_insert("contact_config", $cot);
      } else {
        $ok = $DB->do_update("contact_config", $cot, "lang='$lang'");
      }
      if ($ok) {
        //xoa cache
        $func->clear_cache();
        //insert adminlog
        $func->insertlog("Edit", $_GET['act'], "");
        $err = $vnT->lang["edit_success"];
        $url = $this->linkUrl;
        $func->html_redirect($url, $err);
      } else {
        $err = $func->html_err($DB->debug());
      }
    }
    $result = $DB->query("select * from contact_config where lang='$lang' ");
    if ($data = $DB->fetch_row($result)) {
      $data['src'] = $data['map'];
    }
    $data['maps'] = '<iframe height="500" frameborder="0" style="width:780px;" scrolling="no" border="false" noresize="" src="modules/contact_ad/popup/edit_map.php?id=' . $data['id'] . '" marginheight="0" marginwidth="0"/></iframe>';
    $data["html_content"] = $vnT->editor->doDisplay('description', $data['description'], '100%', '350', "Default");
    $data['err'] = $err;
    $data['link_action'] = $this->linkUrl;
    return $this->html_manage($data);
  }

  //=================Skin===================
  function html_manage ($data)
  {
    global $func, $vnT, $conf;
    return <<<EOF
<br />
{$data['err']}
<form action="{$data['link_action']}" method="post" enctype="multipart/form-data" name="myform"  >
<table width="100%"  border="0"  cellspacing="1" cellpadding="1" class="admintable">


			<tr>
			<td colspan=2  align="center" class="row1"><p><strong>Cấu trúc hiển thị :</strong></p>
			{$data['html_content']}
			</td>
		</tr>
		  <tr class="row_title" >
     <td  colspan="2" class="font_title" >Bản đồ (Googe Map): </td>
    </tr> 
    <tr  >
     <td  class="row1" colspan="2"  align="center">{$data['maps']}
     
     
     <input type="hidden" id="map_lat" name="map_lat" value="{$data['map_lat']}" />
			<input type="hidden" id="map_lng" name="map_lng" value="{$data['map_lng']}" />
			<input type="hidden" id="map_information" name="map_information" value="{$data['map_desc']}" />	
      
     </td>
    </tr>
		
		<tr >
			<td colspan="2" align="center">
				<input type="submit" name="btnSubmit" value="Cập nhật" class="button">
			</td>
		</tr>
	</table>
</form>
<br>
EOF;
  }
  // end class
}
?>