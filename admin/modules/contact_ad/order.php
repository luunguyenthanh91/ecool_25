<?php
/*================================================================================*\
|| 							Name code : order.php 		 		            	  ||
||  				Copyright @2008 by Thai Son - CMS vnTRUST                     ||
\*================================================================================*/
/**
 * @version : 2.0
 * @date upgrade : 09/01/2009 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$vntModule = new vntModule();

class vntModule
{
  var $output = "";
  var $skin = "";
  var $linkUrl = "";
  var $module = "contact";
  var $action = "order";

  /**
   * function vntModule ()
   * Khoi tao 
   **/
  function vntModule ()
  {
    global $Template, $vnT, $func, $DB, $conf;
    require_once ("function_" . $this->module . ".php");
    $this->skin = new XiTemplate(DIR_MODULE . DS . $this->module . "_ad" . DS . "html" . DS . $this->action . ".tpl");
    $this->skin->assign('LANG', $vnT->lang);
    $lang = ($vnT->input['lang']) ? $lang = $vnT->input['lang'] : $func->get_lang_default();
    $this->linkUrl = "?mod=" . $this->module . "&act=" . $this->action . "&lang=" . $lang;
    $vnT->html->addStyleSheet("modules/" . $this->module . "_ad/css/" . $this->module . ".css");
    switch ($vnT->input['sub']) {
      case 'edit':
        $nd['f_title'] = 'Chi tiết đơn đặt hàng';
        $nd['content'] = $this->do_Edit($lang);
      break;
      case 'del':
        $this->do_Del($lang);
      break;
      default:
        $nd['f_title'] = 'Quản lý đơn đặt hàng ';
        $nd['content'] = $this->do_Manage($lang);
      break;
    }
    $nd['menu'] = $func->getToolbar_Small($this->module, $this->action, $lang);
    $nd['row_lang'] = $func->html_lang("?mod=" . $this->module . "&act=" . $this->action, $lang);
    $Template->assign("data", $nd);
    $Template->parse("box_main");
    $vnT->output .= $Template->text("box_main");
  }

  /**
   * function do_Edit 
   *   
   **/
  function do_Edit ($lang)
  {
    global $vnT, $func, $DB, $conf, $a_order_status;
     global $vnT, $func, $DB, $conf;
    $vnT->html->addStyleSheet( $vnT->dir_js . "/jquery_ui/themes/base/ui.all.css");
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.core.js");		
		$vnT->html->addScript($vnT->dir_js . "/jquery_ui/ui.datepicker.js");  
    $vnT->html->addScriptDeclaration("
	 		$(function() {
				$('#date_update').datepicker({
					changeMonth: true,
					changeYear: true
				});

			});
		
		");
		
    $id = $vnT->input['id'];
    $msg = "";
    if (isset($_POST["btnSave"])) {
      if ($_POST["date_update"] != "") {
        $tmp = explode("/", $_POST["date_update"]);
        $date_update = mktime(date("H"), date("i"), 0, $tmp[1], $tmp[0], $tmp[2]);
      } else {
        $date_update = time();
      }
      $status = $_POST["status"];
      $dataup["date_update"] = $date_update;
      $dataup["status"] = $status;
      $ok = $DB->do_update("contact_order", $dataup, "id='{$id}'");
      if ($ok) {
        $err = "Modify Successful";
        $url = $this->linkUrl . "&sub=edit&id=$id";
        $func->html_redirect($url, $err);
      } else
        $err = $vnT->lang["edit_failt"];
    }
    $sql = "select * from contact_order where id ='{$id}' ";
    $result = $DB->query($sql);
    if ($data = $DB->fetch_row($result)) {
		
			$arr_type = array("1"=>'Sản phẩm', 2=>'Dịch vụ') ;
			
      $data['date_post'] = date("H:i, d/m/Y", $data['date_post']);
			$data['type_name'] = $arr_type[$data['type_order']];
			$data['content'] = $func->HTML($data['content']);
			
			switch ($data['type_order'])
			{
				 
				case 2 :  
					$$info_order = '<tr>
													<td>Loại dịch vụ  : <strong>'.get_cat_name('service',$data['cat_service'],$lang).'</strong> </td>
												</tr>';
						$info_order .= '<tr>
													<td>Dịch vụ  : <strong>'.get_service_name($data['service_id'],$lang).'</strong> </td>
												</tr>';
					 																						
						$info_order .= '<tr>
													<td>Địa chỉ lắp đặt : <strong>'.$func->HTML($data['address']).'</strong> </td>
												</tr>';
						if($data['content'])						
							$info_order .= '<tr>
													<td>Ghi chú : <strong>'.$func->HTML($data['content']).'</strong> </td>
												</tr>';	
				break ;
				case 1 :  
						$info_order = '<tr>
													<td>Loại Sản phẩm  : <strong>'.get_cat_name('product',$data['cat_pro'],$lang).'</strong> </td>
												</tr>';
						$info_order .= '<tr>
													<td> Sản phẩm  : <strong>'.get_p_name($data['p_id'],$lang).'</strong> </td>
												</tr>';
						$info_order .= '<tr>
													<td>Số lượng : <strong>'.$data['quantity'].'</strong> </td>
												</tr>';																							
						$info_order .= '<tr>
													<td>Địa chỉ giao hàng : <strong>'.$func->HTML($data['ship_address']).'</strong> </td>
												</tr>';
						$info_order .= '<tr>
													<td>Thời gian giao hàng : <strong>'.$func->HTML($data['ship_time']).'</strong> </td>
												</tr>';												
				break ;
			
			}
			$data['info_order'] = $info_order;
			
			
    }
    
		$data['date_update'] = ($data['date_update']) ? date("d/m/Y", $data['date_update']) : date("d/m/Y");
    $data['id'] = $id;
    $data['list_status'] = List_Status($data['status']);
    $data['link_action'] = $this->linkUrl . "&sub=edit&id=$id";
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("edit");
    return $this->skin->text("edit");
  }

  /**
   * function do_Task 
   *   
   **/
  function do_Del ($lang)
  {
    global $func, $DB, $conf, $vnT;
    global $func, $DB, $conf, $vnT;
    $id = (int) $vnT->input['id'];
    $ext = $vnT->input["ext"];
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $ids = $id;
    }
    if (isset($vnT->input["del_id"])) {
      $ids = implode(',', $vnT->input["del_id"]);
    }
    $query = 'DELETE FROM contact_order WHERE id IN (' . $ids . ')';
    if ($ok = $DB->query($query)) {
      $mess = $vnT->lang["del_success"];
    } else
      $mess = $vnT->lang["del_failt"];
    $ext_page = str_replace("|", "&", $ext);
    $url = $this->linkUrl . "&{$ext_page}";
    $func->html_redirect($url, $mess);
  }

  /**
   * function render_row 
   * list cac record
   **/
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = vnT_HTML::checkbox("del_id[]", $id, 0, " ");
    //$link_edit = $this->linkUrl . '&sub=edit&id=' . $func->NDK_encode($id) . '&ext=' . $row['ext_page'];
    $link_edit = $this->linkUrl . "&sub=edit&id={$id}";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id=" . $id . "&ext=" . $row['ext_page'] . "')";
    $link_send_email = $this->linkUrl . "&sub=send_email&id=" . $id;
    $output['id'] = "<a href=\"{$link_edit}\"><strong class=font_err >#" . $row['id'] . "</strong></a>";
    ;
    //$output['member'] = $func->HTML($row['d_name']) ;
    $output['date_post'] = date("H:i, d/m/Y", $row['date_post']);
 
    $output['customer'] = "Họ tên : <a href=\"{$link_edit}\"><strong>" . $func->HTML($row['name']) . "</strong></a> ";
    
		$output['customer']  .= "<div style='padding:2px;'>ĐT : ".$row['phone']."</div>";
		if($row['email'])
		$output['customer']  .= "<div style='padding:2px;'>Email : ".$row['email']."</div>";
		
		$arr_type = array("1"=>'Sản phẩm', 2=>'Dịch vụ') ;
		$info.= "<div style='padding:2px;'>Đăng ký về : <strong>".$arr_type[$row['type_order']]."</strong></div>";  
		
		
		switch ($row['type_order'])
		{
			 
			case 2 :  
				$$info = '<div style="padding:2px;">Loại dịch vụ  : <strong>'.get_cat_name('service',$row['cat_service'],$lang).'</strong> </div>';
				$info .= '<div style="padding:2px;">Dịch vụ  : <strong>'.get_service_name($row['service_id'],$lang).'</strong> </div>';
			 
			break ;
			case 1 :  
					$info = '<div style="padding:2px;">Loại Sản phẩm  : <strong>'.get_cat_name('product',$row['cat_pro'],$lang).'</strong> </div>';
					$info .= '<div style="padding:2px;">Sản phẩm  : <strong>'.get_p_name($row['p_id'],$lang).'</strong> </div>';
													
			break ;
		
		}
 
		
		$output['info'] = $info;
		
    $output['date_order'] = date("H:i, d/m/Y", $row['date_order']);
    switch ($row['status']) {
      case "0":
        $output['status'] = "<b class=red>Đăng ký mới</b>";
      break;
      case "1":
        $output['status'] =  "<b class=blue>Đã xác nhận</b>";
      break;
      case "2":
        $output['status'] = "Hủy bỏ";
      break;
    }
    if ($row['display'] == 1) {
      $display = "<img src=\"{$vnT->dir_images}/dispay.gif\" width=15  />";
    } else {
      $display = "<img src=\"{$vnT->dir_images}/nodispay.gif\"  width=15 />";
    }
    $output['action'] = '<input name=h_id[]" type="hidden" value="' . $id . '" />';
    $output['action'] .= '<a href="' . $link_edit . '"><img src="' . $vnT->dir_images . '/but_view.gif"  alt="View " width=22></a>&nbsp;';
    $output['action'] .= '<a href="' . $link_del . '"><img src="' . $vnT->dir_images . '/delete.gif"  alt="Delete "></a>';
    return $output;
  }

  /**
   * function do_Manage() 
   * Quan ly 
   **/
  function do_Manage ($lang)
  {
    global $vnT, $func, $DB, $conf;
    //update
    if ($vnT->input["do_action"]) {
      //xoa cache
      $func->clear_cache();
      if ($vnT->input["del_id"])
        $h_id = $vnT->input["del_id"];
      switch ($vnT->input["do_action"]) {
        case "do_edit":
        break;
        case "do_hidden":
          $mess .= "- " . $vnT->lang['hidden_success'] . " ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 0;
            $ok = $DB->do_update("contact_order", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
        case "do_display":
          $mess .= "- " . $vnT->lang['display_success'] . "  ID: <strong>";
          for ($i = 0; $i < count($h_id); $i ++) {
            $dup['display'] = 1;
            $ok = $DB->do_update("contact_order", $dup, "id=" . $h_id[$i]);
            if ($ok) {
              $str_mess .= $h_id[$i] . ", ";
            }
          }
          $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
          $err = $func->html_mess($mess);
        break;
      }
    }
    $p = ((int) $vnT->input['p']) ? $p = $vnT->input['p'] : 1;
    $n = ($conf['record']) ? $conf['record'] : 30;
    $keyword = ($vnT->input['keyword']) ? $vnT->input['keyword'] : "";
		$status = (isset($vnT->input['status'])) ? $vnT->input['status'] : "-1";
		if($status!=-1)
		{
			$where .=" AND status=".$status;
			$ext_page .= "status=$status|";
      $ext = "&status=$status";
		}
		if($keyword){
			$where .=" AND name like '%".$keyword."%' ";
			$ext_page .= "keyword=$keyword|";
      $ext = "&keyword=$keyword";
		}
		
		
    $query = $DB->query("SELECT * FROM contact_order WHERE id<>0  $where ");
    $totals = intval($DB->num_rows($query));
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $nav = $func->paginate($totals, $n, $ext, $p);
    $table['link_action'] = $this->linkUrl . "&sub=manage";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"checkall\" id=\"checkall\" class=\"checkbox\" />|5%|center" , 
      'id' => "Order ID|10%|center" , 
			'customer' => "Khách hàng|30%|left" , 
      'info' => "Thông tin đăng ký|30%|left" , 
      'date_post' => "Ngày đăng ký |15%|center" , 
      'status' => "Trạng thái|10%|center" , 
      'action' => "Action|15%|center");
    $sql = "SELECT * FROM contact_order  WHERE id<>0 $where ORDER BY  id DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >" . $vnT->lang['no_have_order'] . "</div>";
    }
    $table['button'] = '<input type="button" name="btnDel" value=" ' . $vnT->lang['delete'] . ' " class="button" onclick="del_selected(\'' . $this->linkUrl . '&sub=del&ext=' . $ext_page . '\')">';
    $table_list = $func->ShowTable($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['keyword'] = $keyword;
		$data['list_status'] = List_Status($status,1,"onChange='submit()'");
    $data['err'] = $err;
    $data['nav'] = $nav;
    /*assign the array to a template variable*/
    $this->skin->assign('data', $data);
    $this->skin->parse("manage");
    return $this->skin->text("manage");
  }
  // end class
}
?>