<?php
/*================================================================================*\
|| 							Name code : order.php 		 			                 							# ||
||  				Copyright © 2007 by Thai Son - CMS vnTRUST                          # ||
\*================================================================================*/
/**
 * @version : 1.0
 * @date upgrade : 11/12/2007 by Thai Son
 **/
if (! defined('IN_vnT')) {
  die('Hacking attempt!');
}
$act = new sMain($sub);

class sMain
{
  var $html = "";
  var $output = "";
  var $base_url = "";

  function sMain ($sub)
  {
    global $vnT, $func, $DB, $vnT;
    //load lang
    $func->load_language("contact");
    include ("function_contact.php");
    if (isset($_GET['lang']))
      $lang = $_GET['lang'];
    else
      $lang = $func->get_lang_default();
    $this->linkUrl = "?mod=contact&act=contact_now&lang=" . $lang;
    switch ($sub) {
      case 'del':
        $this->do_Del();
      break;
      default:
        {
          $nd['f_title'] = "Danh sách cần phải liên hệ lại";
          $nd['content'] = $this->do_Manage();
        }
        ;
      break;
    }
    $nd['menu'] = getToolbar_Small("contact_now", $lang);
    $vnT->output .= $vnT->skin_acp->html_table($nd);
  }

  //================
  function do_Del ()
  {
    global $func, $DB, $conf;
    if (isset($_GET['id']))
      $id = $_GET['id'];
    else
      $id = 0;
    $del = 0;
    $qr = "";
    if ($id != 0) {
      $del = 1;
      $qr = " OR id='{$id}' ";
    }
    if (isset($_POST["del_id"]))
      $key = $_POST["del_id"];
    for ($i = 0; $i < count($key); $i ++) {
      $del = 1;
      $qr .= " OR id='{$key[$i]}' ";
    }
    if ($del) {
      $query = "DELETE FROM contact_now WHERE id=-1" . $qr;
      //print "query = ".$query."<br>";
      if ($ok = $DB->query($query)) {
        $mess = "Delete successfull";
      } else
        $mess = "Not found !";
      $url = $this->linkUrl;
      flush();
      echo $func->html_redirect($url, $mess);
      exit();
    } else
      $this->do_Manage();
  }

  //================
  function render_row ($row_info, $lang)
  {
    global $func, $DB, $conf, $vnT, $lang_acp, $a_unit;
    $row = $row_info;
    // Xu ly tung ROW
    $id = $row['id'];
    $row_id = "row_" . $id;
    $output['check_box'] = "<input type=\"checkbox\" name=\"del_id[]\" value=\"{$id}\" class=\"checkbox\" onclick=\"select_row('{$row_id}')\">";
    $link_del = "javascript:del_item('" . $this->linkUrl . "&sub=del&id={$id}')";
    $output['call_at'] = $row['time_call'] . ", " . $row['date_call'];
    // nguoi 
    $info_customer = "Họ tên : <b>" . $row['f_name'] . " " . $row['l_name'] . "</b> ";
    $info_customer .= "<br>Điện thoại : " . $row['phone'];
    $info_customer .= "<br><span class='font_small'>Email : " . $row['email'];
    $info_customer .= "<br>Quốc gia : " . $vnT->acp_array['country'][$row['country']];
    $output['info_customer'] = $info_customer;
    $output['note'] = $func->HTML($row['note']);
    $output['datesubmit'] = date("H:i, d/m/Y", $row['datesubmit']);
    switch ($row['status']) {
      case "0":
        $output['status'] = "<b class=font_err>Mới</b>";
      break;
      case "1":
        $output['status'] = "Đã liên lạc";
      break;
    }
    $output['action'] = "
		<input name=\"h_id[]\" type=\"hidden\" value=\"{$id}\" />
		<a href=\"{$link_del}\"><img src=\"{$vnT->dir_images}/delete.gif\"  alt=\"Delete \"></a>";
    return $output;
  }

  function do_Manage ()
  {
    global $vnT, $func, $DB, $conf;
    if ((isset($_GET['p'])) && (is_numeric($_GET['p'])))
      $p = $_GET['p'];
    else
      $p = 1;
      //update
    if (isset($_POST["do_action"])) {
      if (isset($_POST["del_id"]))
        $h_id = $_POST["del_id"];
      switch ($_POST["do_action"]) {
        case "do_edit":
          {
            $mess .= "- " . $vnT->lang['edit_success'] . " ID: <strong>";
            $str_mess = "";
            for ($i = 0; $i < count($h_id); $i ++) {
              $dup['status'] = 1;
              $ok = $DB->do_update("contact_now", $dup, "id={$h_id[$i]}");
              if ($ok) {
                $str_mess .= $h_id[$i] . ", ";
              }
            }
            $mess .= substr($str_mess, 0, - 2) . "</strong><br>";
            $err = $func->html_mess($mess);
          }
          ;
        break;
        case "do_hidden":
          ;
        break;
        case "do_display":
          ;
        break;
      }
    }
    $n = 50;
    $status = 0;
    if (isset($_GET['status']))
      $status = $_GET['status'];
    if (isset($_POST['status']))
      $status = $_POST['status'];
    $where = "where status=$status ";
    $query = $DB->query("SELECT * FROM contact_now $where  ");
    $totals = $DB->num_rows($query);
    $num_pages = ceil($totals / $n);
    if ($p > $num_pages)
      $p = $num_pages;
    if ($p < 1)
      $p = 1;
    $start = ($p - 1) * $n;
    $ext = "";
    if (! empty($n))
      $ext .= "&n={$n}";
    if (! empty($keyword))
      $ext .= "&keyword={$keyword}";
    $nav = $func->paginate($totals, $n, $ext, $p, $class = "pagelink");
    $table['link_action'] = $this->linkUrl . "&p={$p}";
    $table['title'] = array(
      'check_box' => "<input type=\"checkbox\" name=\"all\" class=\"checkbox\" onclick=\"javascript:checkall();\" />|5%|center" , 
      'call_at' => "Gọi lại lúc |15%|center" , 
      'info_customer' => "Khách hàng|30%|left" , 
      'note' => "Ghi chú |30%|left" , 
      'status' => "Trạng thái|15%|center" , 
      'action' => "Action|10%|center");
    $sql = "SELECT * FROM contact_now  $where ORDER BY   datesubmit DESC  LIMIT $start,$n";
    //print "sql = ".$sql."<br>";
    $result = $DB->query($sql);
    if ($DB->num_rows($result)) {
      $row = $DB->get_array($result);
      for ($i = 0; $i < count($row); $i ++) {
        $row[$i]['ext_page'] = $ext_page;
        $row_info = $this->render_row($row[$i], $lang);
        $row_field[$i] = $row_info;
        $row_field[$i]['stt'] = ($i + 1);
        $row_field[$i]['row_id'] = "row_" . $row[$i]['id'];
        $row_field[$i]['ext'] = "";
      }
      $table['row'] = $row_field;
    } else {
      $table['row'] = array();
      $table['extra'] = "<div align=center class=font_err >Chưa có liên lạc nào</div>";
    }
    $button .= "<input type=\"button\" name=\"btnEdit\" value=\"Sét trạng thái đã trả lời\" class=\"button1\" onclick=\"javascript:do_submit('do_edit')\">&nbsp;&nbsp;";
    $button .= "<input type=\"button\" name=\"btnDel\" value=\"Xóa các liên lạc  đã chọn\" class=\"button1\" onclick=\"javascript:del_selected('" . $this->linkUrl . "&sub=del')\">";
    $table['fName'] = "manage";
    $table['button'] = $button;
    $table_list = $func->ShowTable_Cus($table);
    $data['table_list'] = $table_list;
    $data['totals'] = $totals;
    $data['err'] = $err;
    $data['list_status'] = List_Status($status, "onChange='submit();'");
    $data['nav'] = $nav;
    return $this->html_manage($data);
  }

  function html_manage ($data)
  {
    global $func, $conf, $lang_acp;
    return <<<EOF

<br>
<form action="" method="post">
<table width="100%" border="0" cellspacing="2" cellpadding="2" class="tableborder" align=center>
  <tr>
    <td width="20%" ><strong>Tổng số :</strong> </td>
	  <td class="font_err">&nbsp;<strong>{$data['totals']}</strong></td>
  <tr>
    <td><strong>Trạng thái : </strong></td>
    <td >{$data['list_status']}</td></tr>
  </table>
</form>
{$data['err']}
<br />
{$data['table_list']}
<br />
<table width="100%"  border="0" align="center" cellspacing="1" cellpadding="1" class="bg_tab">
  <tr>
    <td  height="30">{$data['nav']}</td>
  </tr>
</table>
<br />
EOF;
  }
  // end class
}
?>