<!-- BEGIN: body --><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>[:: Admin ::]</title>
<meta name="author" content="www.thietkeweb.com | TRUST.vn"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="SHORTCUT ICON" href="vntrust.ico" type="image/x-icon" />
<link rel="icon" href="vntrust.ico" type="image/gif" >
<LINK href="{DIR_STYLE}/global.css" rel="stylesheet" type="text/css">
<link rel='stylesheet' href='{DIR_JS}/thickbox/thickbox.css' type='text/css' media='all' /> 

{EXT_STYLE}
<script language="javascript" >
	var ROOT = "{CONF.rooturl}";
	var DIR_IMAGE = "{DIR_IMAGE}";
</script>
<script language="javascript1.2" src="{DIR_JS}/jquery.js?v=1.8.3"></script>
<script language="javascript1.2" src="{DIR_JS}/core.js?v=1.0"></script>
<script language="javascript1.2" src="{DIR_JS}/admin/js_admin.js?v=1.0"></script>
<script type='text/javascript' src='{DIR_JS}/thickbox/thickbox.js?v=3.1'></script>
<script type="text/javascript" src="{DIR_JS}/jquery_validate/jquery.validate.min.js?v=1.11.1"></script>
<script id='ext_javascript'></script>
{EXT_HEAD}

</head>
<body >
<div id="vnt-wrap">
	<div id="vnt-header">
  	<div class="box-admin" >
    	<div class="info-user">
      {LANG.hello} : <span>{admininfo.username}</span><br>
      [<a href="?act=logout"><strong>{LANG.logout}</strong></a>] [<a href="?mod=admin&act=admin&sub=edit&id={admininfo.adminid}"><strong>{LANG.account}</strong></a>]
      </div>
    </div>
    <div class="box-help">
      <div class="info-help" >
      	<a href="?mod=main">{LANG.home}</a>
      	<a href="#Help" onclick="javascript:NewWindow('help/index.php','AdminCPHelp',1000, 600, 'yes','center');">{LANG.help_use}</a>
        <a href="http://www.thietkeweb.com/hoi-dap-thiet-ke-web.html" target="_blank" >{LANG.faqs}</a>
        <a href="http://www.thietkeweb.com/lien-he-thiet-ke-web.html" target="_blank">{LANG.contact}</a>
      </div>      
    </div>     
    <div class="iframe-mess"><marquee onmouseout="this.start()" onmouseover="this.stop()" scrollamount="3" scrolldelay="3" direction="left" behavior="scroll"><span class="font_marquee">{marquee_hotline}</span></marquee></div>
  </div>
  <div class="clear"></div>
	<div id="vnt-content">
  	<div id="vnt-menu">
    	 {BOX_LEFT}
    </div>
    <div id="vnt-main">
    	<div class="wrap-main">
    	{PAGE_CONTENT}
      </div>
    </div>
    <div class="clear">&nbsp;</div>
  </div>
  <div id="vnt-footer">
  	<div class="chose-skin">&nbsp;</div>
    <div class="copyright">
    	Copyright &copy; :: <strong>{CONF.indextitle}</strong>  :: [MADEBY]</b>
    </div>  
    
     
  </div>
</div>
<script type="text/javascript" src="{DIR_JS}/admin/jeip.js"></script>
<script type="text/javascript">
	function  quick_edit(obj,text_data){
		$( "#"+obj ).eip( "save.php", { select_text: true,data: text_data } );	
		$( "#"+obj ).trigger("click");
		$("#btn-"+ obj).hide();
	}
</script>

</body>
</html>      
<!-- END: body -->

<!-- BEGIN: box_main -->
<div id="box_main">
	<div class="vnt-title">
  	<h2><span class="{data.icon}">{data.f_title}</span></h2>
    <div class="vnt-lang">{data.row_lang}</div>
    <div class="clear"></div>
  </div>
  <div class="vnt-main">
  	{data.menu}
    <div class="box_content">{data.content}</div>
  </div>
  
</div>
     
<!-- END: box_main -->

<!-- BEGIN: box -->
<div class="postbox" id="{data.id}">
  <div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle"><span>{data.f_title}</span></h3>
  <div class="inside"> {data.content} </div>
</div>     
<!-- END: box -->

<!-- BEGIN: box_manage -->
<form action="{data.link_action}" method="post" name="manage" id="manage">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="bg_tbl">
   <tr>
  	<td >
    <table  border="0" cellspacing="2" cellpadding="2">
    	<tr>
           <td width="40" align="center"><img src="{DIR_IMAGE}/arr_top.gif" width="17" height="17"></td>
           <td>{data.button}</td>
         </tr>
     </table>
     </td>
   </tr>
   <tr>
     <td>
    <table cellspacing="1" class="adminlist" id="table_list" >
    	{data.list}
		</table>
   	</td>
   </tr>
    <tr>
      <td >
         <table  border="0" cellspacing="2" cellpadding="2">
           <tr>
           <td width="40" align="center"><img src="{DIR_IMAGE}/arr_bottom.gif" ></td>
           <td>{data.button}</td>
         </tr>
    	 </table>
     </td>
   </tr>
</table>
<input type="hidden" name="do_action" id="do_action" value="" >
</form> 
<!-- END: box_manage -->


<!-- BEGIN: box_redirect -->
<br><br><br><br><br>
<title>[:: Admin ::]</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="{DIR_STYLE}/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv='refresh' content='{data.time_ref}; url={data.url}' />
<table width="70%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
	<td height="30" bgcolor="#B84120" >&nbsp;<font color="#FFFFFF"><strong>{LANG.announce}</strong></font></td>
  </tr>
  <tr>
	<td bgcolor="#FFFFFF"  align=center>
	<table width="100%"  border="0" cellspacing="1" cellpadding="1">
	<tr>
	  <td height="50" align=center class="font_err"><strong>{data.mess}</strong></td>
	</tr>
	<tr>
	  <td align=center height="20"><img src="{DIR_IMAGE}/loading.gif"  /></td>
	</tr>
	<tr>
	  <td align=center class="font_err"><a  href='{data.url}'>({LANG.mess_redirect})</a></td>
	</tr>
	<tr>
	  <td align=center>&nbsp;</td>
	</tr>
	</table>
	</td>
  </tr>
  <tr>
	<td bgcolor="#B84120"  align="center" height="25" ><font color="#FFFFFF"><strong>.::[ Copyright &copy; 2008 {data.host} ]::.</strong></font></td>
  </tr>
</table>
<br><br><br><br><br>    
<!-- END: box_redirect -->