<!-- BEGIN: html_box_menu -->
<div class="logo-company"><a href="?mod=main"><img src="{DIR_IMAGE}/vinmus_logo.png" onerror="this.onerror=null; this.src='{DIR_IMAGE}/vinmus_logo.png'"  alt="Thiet ke web"  /></a></div>
<div class="menu-control"></div>
<ul id="admin-menu">
{data.box_menu}

</ul> 
 
<!-- END: html_box_menu -->

 
<!-- BEGIN: html_menu_item -->
<li class="menu-item">
  <div class="menu-top">          	
    <div class="menu-title waves-effect {data.class}"><h2>{data.f_title}</h2></div>
  </div>
  <div class="clear"></div>
  <div class="sub-menu" id="{data.group}" style="{data.style}" >         
  <ul >
    {data.list_menu}
  </ul>
  </div>
</li> 
<!-- END: html_menu_item -->

<!-- BEGIN: html_news -->
<div class="cms_version">© TRUST.vn CMS <span>[ v 6.0]</span></div>
<!-- END: html_news -->
 